Toshiba Solid State Drives
==========================

This is a list of all tested Toshiba solid state drive models and their MTBFs. See
more info on reliability test in the [README](https://github.com/linuxhw/EnterpriseDrive).

Contents
--------

1. [ SSD by Model  ](#ssd-by-model)
2. [ SSD by Family ](#ssd-by-family)

SSD by Model
------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days — avg. days per sample,
Err  — avg. errors per sample,
MTBF — avg. MTBF in years per sample.

| MFG       | Model              | Size   | Samples | Days  | Err   | MTBF   |
|-----------|--------------------|--------|---------|-------|-------|--------|
| Toshiba   | THNSNJ120PCSZ      | 120 GB | 2       | 2006  | 0     | 5.50   |
| Toshiba   | THNSNJ480PCSZ      | 480 GB | 1       | 1755  | 0     | 4.81   |
| Toshiba   | THNSNJ960PCSZ      | 960 GB | 42      | 1789  | 1     | 4.80   |
| Toshiba   | THNSN8480PCSE      | 480 GB | 1       | 1751  | 0     | 4.80   |
| Toshiba   | THNSN8240PCSE      | 240 GB | 2       | 1471  | 0     | 4.03   |
| Toshiba   | VX500              | 1 TB   | 6       | 1443  | 0     | 3.95   |
| Toshiba   | THNSN8960PCSE      | 960 GB | 32      | 1066  | 1     | 2.87   |
| Toshiba   | THNSNJ256GCSU      | 256 GB | 1       | 1036  | 0     | 2.84   |
| Toshiba   | THNSF8800CCSE      | 800 GB | 1       | 896   | 0     | 2.46   |
| Toshiba   | THNSF8200CCSE      | 200 GB | 20      | 914   | 1     | 2.27   |
| Toshiba   | TR150              | 960 GB | 1       | 1473  | 1     | 2.02   |
| Toshiba   | THNSF8240CCSE      | 240 GB | 10      | 736   | 0     | 2.02   |
| Toshiba   | THNSN81Q92CSE      | 1.9 TB | 7       | 719   | 0     | 1.97   |
| Toshiba   | THNSNJ800PCSZ      | 800 GB | 4       | 700   | 0     | 1.92   |
| Toshiba   | THNSF8400CCSE      | 400 GB | 12      | 508   | 0     | 1.39   |
| Toshiba   | THNSF8200CAME      | 200 GB | 3       | 447   | 0     | 1.23   |
| Toshiba   | THNSF81D60CSE      | 1.6 TB | 10      | 253   | 0     | 0.69   |
| Toshiba   | KHK61RSE960G       | 960 GB | 67      | 239   | 0     | 0.66   |
| Toshiba   | THNSFJ256GDNU      | 256 GB | 1       | 232   | 0     | 0.64   |
| Toshiba   | KHK61RSE1T92       | 1.9 TB | 33      | 119   | 0     | 0.33   |

SSD by Family
-------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days — avg. days per sample,
Err  — avg. errors per sample,
MTBF — avg. MTBF in years per sample.

| MFG       | Family                 | Models | Samples | Days  | Err   | MTBF   |
|-----------|------------------------|--------|---------|-------|-------|--------|
| Toshiba   | OCZ                    | 1      | 6       | 1443  | 0     | 3.95   |
| Toshiba   | HK4R Series SSD        | 4      | 42      | 1043  | 1     | 2.82   |
| Toshiba   | OCZ/Toshiba Trion SSDs | 1      | 1       | 1473  | 1     | 2.02   |
| Toshiba   | Unknown                | 12     | 205     | 683   | 1     | 1.83   |
| Toshiba   | HG6 Series SSD         | 2      | 2       | 634   | 0     | 1.74   |
