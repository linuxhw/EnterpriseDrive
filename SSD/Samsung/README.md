Samsung Solid State Drives
==========================

This is a list of all tested Samsung solid state drive models and their MTBFs. See
more info on reliability test in the [README](https://github.com/linuxhw/EnterpriseDrive).

Contents
--------

1. [ SSD by Model  ](#ssd-by-model)
2. [ SSD by Family ](#ssd-by-family)

SSD by Model
------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days — avg. days per sample,
Err  — avg. errors per sample,
MTBF — avg. MTBF in years per sample.

| MFG       | Model              | Size   | Samples | Days  | Err   | MTBF   |
|-----------|--------------------|--------|---------|-------|-------|--------|
| Samsung   | SSD 840 EVO        | 120 GB | 7       | 3119  | 0     | 8.55   |
| Samsung   | SSD 840 EVO        | 500 GB | 10      | 3120  | 1     | 7.89   |
| Samsung   | MZ7TE256HMHP-00000 | 256 GB | 1       | 2611  | 0     | 7.15   |
| Samsung   | MZ-5EA1000-0D3     | 100 GB | 1       | 2597  | 0     | 7.12   |
| Samsung   | SSD 830 Series     | 256 GB | 1       | 2440  | 0     | 6.69   |
| Samsung   | SSD 830 Series     | 512 GB | 12      | 3084  | 253   | 6.33   |
| Samsung   | MZ7LM240HCGR-00003 | 240 GB | 4       | 2197  | 0     | 6.02   |
| Samsung   | SSD 850 PRO        | 128 GB | 7       | 2133  | 0     | 5.85   |
| Samsung   | MZ7LM120HCFD-00003 | 120 GB | 4       | 2104  | 0     | 5.77   |
| Samsung   | MZ7KM200HAGR00D3   | 200 GB | 2       | 2062  | 0     | 5.65   |
| Samsung   | MZ7WD480HAGM-00003 | 480 GB | 7       | 2020  | 0     | 5.54   |
| Samsung   | MZ7LM480HCHP-0E003 | 480 GB | 7       | 2019  | 0     | 5.53   |
| Samsung   | MZ7TD512HAGM-00000 | 512 GB | 4       | 2275  | 1     | 5.33   |
| Samsung   | SSD 840 EVO        | 1 TB   | 21      | 2369  | 57    | 5.05   |
| Samsung   | MZ7WD480HCGM-00003 | 480 GB | 23      | 1999  | 45    | 5.03   |
| Samsung   | MZ7WD120HCFV-00003 | 120 GB | 1       | 1832  | 0     | 5.02   |
| Samsung   | MZ7KM960HAHP-00005 | 800 GB | 1       | 1822  | 0     | 4.99   |
| Samsung   | MZ7LM1T9HCJM-0E003 | 1.9 TB | 2       | 1785  | 0     | 4.89   |
| Samsung   | MZ7WD240HAFV-000D2 | 240 GB | 7       | 2083  | 5     | 4.88   |
| Samsung   | MZ7LM960HCHP-0E003 | 960 GB | 4       | 1778  | 0     | 4.87   |
| Samsung   | SSD 840 EVO        | 250 GB | 7       | 2673  | 7     | 4.74   |
| Samsung   | SSD 850 PRO        | 512 GB | 42      | 1766  | 1     | 4.70   |
| Samsung   | SSD 850 PRO        | 1 TB   | 47      | 2322  | 5     | 4.68   |
| Samsung   | MZ7KM960HAHP-00005 | 960 GB | 66      | 1681  | 0     | 4.61   |
| Samsung   | SSD 850 PRO        | 256 GB | 37      | 1734  | 1     | 4.58   |
| Samsung   | MZ7LM480HCHP-00003 | 480 GB | 74      | 1656  | 0     | 4.54   |
| Samsung   | MZ7PD480HAGM-000D3 | 480 GB | 1       | 1653  | 0     | 4.53   |
| Samsung   | MZ7GE960HMHP-00003 | 960 GB | 16      | 2115  | 142   | 4.38   |
| Samsung   | SSD 840 PRO Series | 128 GB | 17      | 1967  | 77    | 4.31   |
| Samsung   | MZ7KM480HAHP-00005 | 480 GB | 82      | 1552  | 0     | 4.25   |
| Samsung   | MZ7KM480HAHP-0E005 | 480 GB | 15      | 1498  | 0     | 4.11   |
| Samsung   | MZ7KM240HAGR-00005 | 240 GB | 19      | 1479  | 0     | 4.05   |
| Samsung   | MZ7KM120HAFD-00005 | 120 GB | 14      | 1476  | 0     | 4.05   |
| Samsung   | MZ7LM1T9HCJM00D3   | 1.9 TB | 4       | 1466  | 0     | 4.02   |
| Samsung   | SSD 840 Series     | 250 GB | 4       | 2709  | 269   | 4.01   |
| Samsung   | MCBQE25G5MPQ-0VAD3 | 25 GB  | 1       | 1456  | 0     | 3.99   |
| Samsung   | SSD 840 PRO Series | 512 GB | 10      | 1941  | 62    | 3.98   |
| Samsung   | MZ7KM120HAFD-0E005 | 120 GB | 2       | 1394  | 0     | 3.82   |
| Samsung   | MZ7LM120HCFD-00005 | 120 GB | 1       | 1375  | 0     | 3.77   |
| Samsung   | MZ7KM1T9HAJM-000NU | 1.9 TB | 1       | 1342  | 0     | 3.68   |
| Samsung   | MZ7LM960HCHP-00005 | 960 GB | 16      | 1333  | 0     | 3.65   |
| Samsung   | MZ7LN256HCHP-000L7 | 256 GB | 1       | 1330  | 0     | 3.65   |
| Samsung   | SSD 850 EVO        | 120 GB | 15      | 1391  | 15    | 3.58   |
| Samsung   | MZ7KM960HAHP-0E005 | 960 GB | 4       | 1302  | 0     | 3.57   |
| Samsung   | MZ7KM240HMHQ0D3    | 240 GB | 2       | 1295  | 0     | 3.55   |
| Samsung   | MZ7KM960HMJP0D3    | 960 GB | 120     | 1289  | 0     | 3.53   |
| Samsung   | MZ7LN512HMJP-00000 | 512 GB | 13      | 1414  | 3     | 3.49   |
| Samsung   | MZ7WD960HAGP-00003 | 960 GB | 3       | 1262  | 0     | 3.46   |
| Samsung   | MZ7GE480HMHP-00003 | 480 GB | 12      | 1956  | 165   | 3.45   |
| Samsung   | MZ7WD240HCFV-00003 | 240 GB | 2       | 1239  | 0     | 3.39   |
| Samsung   | SSD 850 PRO        | 2 TB   | 3       | 1223  | 0     | 3.35   |
| Samsung   | SSD 840 PRO Series | 256 GB | 19      | 2627  | 274   | 3.34   |
| Samsung   | SSD 850 EVO        | 500 GB | 118     | 1266  | 27    | 3.28   |
| Samsung   | SSD 750 EVO        | 120 GB | 2       | 1196  | 0     | 3.28   |
| Samsung   | MZ7LM3T8HMLP-00005 | 3.8 TB | 1       | 1176  | 0     | 3.22   |
| Samsung   | MZ7KM1T9HAJM-00005 | 1.9 TB | 50      | 1174  | 0     | 3.22   |
| Samsung   | SSD 850 EVO        | 2 TB   | 8       | 1163  | 0     | 3.19   |
| Samsung   | SSD 860 EVO M.2    | 2 TB   | 2       | 1123  | 0     | 3.08   |
| Samsung   | MZ7KM240HAGR-0E005 | 240 GB | 17      | 1671  | 23    | 3.07   |
| Samsung   | SSD 860 EVO M.2    | 1 TB   | 4       | 1101  | 0     | 3.02   |
| Samsung   | SSD 850 EVO        | 1 TB   | 196     | 1421  | 58    | 2.96   |
| Samsung   | SSD 850 EVO M.2    | 250 GB | 2       | 1077  | 0     | 2.95   |
| Samsung   | MZ7LM480HCHP-00005 | 480 GB | 25      | 1069  | 0     | 2.93   |
| Samsung   | SSD PM830 2.5" 7mm | 256 GB | 1       | 1044  | 0     | 2.86   |
| Samsung   | SSD 850 EVO M.2    | 500 GB | 5       | 1041  | 0     | 2.85   |
| Samsung   | MZ7LM1T9HCJM-00003 | 1.9 TB | 7       | 1040  | 0     | 2.85   |
| Samsung   | SSD 850 EVO        | 250 GB | 82      | 1139  | 35    | 2.81   |
| Samsung   | MZ7LM240HCGR-00005 | 240 GB | 4       | 1018  | 0     | 2.79   |
| Samsung   | MZ7KM960HAHP-0Z005 | 960 GB | 1       | 1004  | 0     | 2.75   |
| Samsung   | MZ7LM960HCHP-00003 | 960 GB | 31      | 987   | 0     | 2.70   |
| Samsung   | MZ7KM240HMHQ-00005 | 240 GB | 61      | 977   | 13    | 2.66   |
| Samsung   | MZ7KM1T9HMJP-00005 | 1.9 TB | 81      | 963   | 0     | 2.64   |
| Samsung   | SSD 750 EVO        | 250 GB | 4       | 933   | 0     | 2.56   |
| Samsung   | SSD 840 Series     | 120 GB | 7       | 1862  | 86    | 2.54   |
| Samsung   | MZ7KM480HMHQ-00005 | 480 GB | 151     | 908   | 0     | 2.49   |
| Samsung   | MZ7LM960HMJP0D3    | 960 GB | 42      | 903   | 1     | 2.38   |
| Samsung   | MZ7LM960HMJP-00005 | 960 GB | 293     | 1062  | 20    | 2.31   |
| Samsung   | MZ7LN256HMJP-00000 | 256 GB | 1       | 840   | 0     | 2.30   |
| Samsung   | SSD 860 EVO        | 4 TB   | 11      | 766   | 0     | 2.10   |
| Samsung   | SSD 860 PRO        | 256 GB | 56      | 761   | 0     | 2.09   |
| Samsung   | MZ7GE240HMGR-00003 | 240 GB | 3       | 907   | 119   | 2.08   |
| Samsung   | SSD 860 PRO        | 4 TB   | 14      | 755   | 0     | 2.07   |
| Samsung   | SSD PM851a 2.5 7mm | 1 TB   | 1       | 732   | 0     | 2.01   |
| Samsung   | MZ7KH3T8HALS-00005 | 3.8 TB | 7       | 727   | 0     | 1.99   |
| Samsung   | SSD 860 DCT 1.92TB | 1.9 TB | 18      | 711   | 0     | 1.95   |
| Samsung   | SSD 860 PRO        | 2 TB   | 141     | 703   | 0     | 1.93   |
| Samsung   | SSD 860 QVO        | 2 TB   | 25      | 691   | 0     | 1.89   |
| Samsung   | SSD 850 EVO mSATA  | 250 GB | 6       | 682   | 0     | 1.87   |
| Samsung   | MZ7LN256HCHP-000H1 | 256 GB | 1       | 678   | 0     | 1.86   |
| Samsung   | MZNLN512HMJP-00000 | 512 GB | 11      | 661   | 0     | 1.81   |
| Samsung   | MZ7LM960HMJP-000MV | 960 GB | 4       | 641   | 0     | 1.76   |
| Samsung   | SSD 860 EVO M.2    | 500 GB | 9       | 606   | 0     | 1.66   |
| Samsung   | MZ7KM960HMJP-00005 | 960 GB | 96      | 603   | 0     | 1.65   |
| Samsung   | SSD 860 EVO        | 2 TB   | 102     | 600   | 0     | 1.65   |
| Samsung   | SSD 883 DCT 1.92TB | 1.9 TB | 203     | 589   | 1     | 1.61   |
| Samsung   | SSD 860 PRO        | 1 TB   | 100     | 579   | 0     | 1.59   |
| Samsung   | MZ7LH960HAJR-000AZ | 960 GB | 4       | 571   | 0     | 1.57   |
| Samsung   | MZ7LM1T9HMJP-00005 | 1.9 TB | 106     | 895   | 10    | 1.56   |
| Samsung   | SSD 883 DCT        | 960 GB | 30      | 563   | 0     | 1.54   |
| Samsung   | MZ7LM1T9HMJP0D3    | 1.9 TB | 2       | 547   | 0     | 1.50   |
| Samsung   | MZ7LM480HMHQ-00005 | 480 GB | 68      | 1120  | 30    | 1.50   |
| Samsung   | SSD 845DC PRO      | 800 GB | 4       | 2179  | 3     | 1.49   |
| Samsung   | SSD 860 EVO mSATA  | 250 GB | 2       | 542   | 0     | 1.49   |
| Samsung   | MZ7KM480HMHQ-000MV | 480 GB | 37      | 539   | 0     | 1.48   |
| Samsung   | SSD 860 EVO        | 500 GB | 284     | 540   | 1     | 1.47   |
| Samsung   | MZ7LH240HAHQ0D3    | 240 GB | 6       | 534   | 0     | 1.46   |
| Samsung   | MZ7LM240HMHQ-00005 | 240 GB | 77      | 888   | 134   | 1.44   |
| Samsung   | MZ7LH960HAJR0D3    | 960 GB | 20      | 512   | 0     | 1.40   |
| Samsung   | MZ7KH240HAHQ-00005 | 240 GB | 54      | 512   | 0     | 1.40   |
| Samsung   | MZ7KH960HAJR-00005 | 960 GB | 57      | 508   | 0     | 1.39   |
| Samsung   | MZ7LH1T9HMLT-00005 | 1.9 TB | 472     | 501   | 0     | 1.37   |
| Samsung   | SSD 860 QVO        | 1 TB   | 47      | 476   | 0     | 1.30   |
| Samsung   | MZ7LH480HAHQ0D3    | 480 GB | 2       | 475   | 0     | 1.30   |
| Samsung   | SSD 850            | 120 GB | 2       | 474   | 0     | 1.30   |
| Samsung   | SSD 860 EVO        | 1 TB   | 347     | 468   | 1     | 1.28   |
| Samsung   | SSD 840 EVO 250... | 250 GB | 4       | 456   | 0     | 1.25   |
| Samsung   | MZ7KH1T9HAJR-00005 | 1.9 TB | 109     | 433   | 1     | 1.18   |
| Samsung   | SSD 860 EVO        | 250 GB | 113     | 426   | 1     | 1.16   |
| Samsung   | SSD 860 QVO        | 4 TB   | 6       | 417   | 0     | 1.14   |
| Samsung   | MZ7LM240HMHQ-00003 | 240 GB | 2       | 411   | 0     | 1.13   |
| Samsung   | MZ7KH480HAHQ0D3    | 480 GB | 17      | 399   | 0     | 1.10   |
| Samsung   | MZ7LH960HAJR-00005 | 960 GB | 838     | 390   | 0     | 1.07   |
| Samsung   | MZ7LH480HAHQ-000V3 | 480 GB | 5       | 390   | 0     | 1.07   |
| Samsung   | MZ7KM480HMHQ0D3    | 480 GB | 3       | 382   | 0     | 1.05   |
| Samsung   | MZ7WD480HMHP-00003 | 480 GB | 6       | 1987  | 86    | 1.00   |
| Samsung   | SSD 883 DCT        | 240 GB | 28      | 357   | 0     | 0.98   |
| Samsung   | MZ7LH480HAHQ-00005 | 480 GB | 66      | 352   | 0     | 0.96   |
| Samsung   | MZ7KH480HAHQ-00005 | 480 GB | 81      | 340   | 0     | 0.93   |
| Samsung   | SSD 883 DCT 3.84TB | 3.8 TB | 138     | 338   | 0     | 0.93   |
| Samsung   | MZ7LH1T9HALT0D3    | 1.9 TB | 35      | 338   | 0     | 0.93   |
| Samsung   | SSD 860 EVO M.2    | 250 GB | 1       | 328   | 0     | 0.90   |
| Samsung   | MZ7LH240HAHQ-00005 | 240 GB | 376     | 321   | 0     | 0.88   |
| Samsung   | MZ7LH7T6HMLA-00005 | 7.6 TB | 125     | 320   | 0     | 0.88   |
| Samsung   | MZ7KH960HAJR0D3    | 960 GB | 160     | 287   | 0     | 0.79   |
| Samsung   | SSD 883 DCT        | 480 GB | 7       | 278   | 0     | 0.76   |
| Samsung   | SSD 860 PRO        | 512 GB | 64      | 237   | 0     | 0.65   |
| Samsung   | SSD 870 QVO        | 8 TB   | 35      | 223   | 0     | 0.61   |
| Samsung   | MZ7LH3T8HMLT-00005 | 3.8 TB | 63      | 206   | 0     | 0.57   |
| Samsung   | MZ7WD960HMHP-00003 | 960 GB | 4       | 2167  | 43    | 0.55   |
| Samsung   | MZ7KM120HAFD-000FU | 120 GB | 1       | 195   | 0     | 0.54   |
| Samsung   | MZ7LM960HCHP-000MV | 960 GB | 6       | 192   | 0     | 0.53   |
| Samsung   | MZ7LH480HBHQ0D3    | 480 GB | 9       | 189   | 0     | 0.52   |
| Samsung   | MZNLH1T0HALB-00000 | 1 TB   | 10      | 167   | 0     | 0.46   |
| Samsung   | SSD 870 EVO        | 4 TB   | 2       | 165   | 0     | 0.45   |
| Samsung   | SSD 750 EVO        | 500 GB | 1       | 157   | 0     | 0.43   |
| Samsung   | SSD 870 EVO        | 250 GB | 2       | 136   | 0     | 0.37   |
| Samsung   | SSD 860 DCT        | 960 GB | 2       | 106   | 0     | 0.29   |
| Samsung   | SSD 870 QVO        | 1 TB   | 16      | 75    | 0     | 0.21   |
| Samsung   | SSD 870 EVO        | 500 GB | 8       | 69    | 0     | 0.19   |
| Samsung   | SSD 870 EVO        | 2 TB   | 44      | 46    | 1     | 0.12   |
| Samsung   | SSD 870 EVO        | 1 TB   | 45      | 42    | 1     | 0.12   |

SSD by Family
-------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days — avg. days per sample,
Err  — avg. errors per sample,
MTBF — avg. MTBF in years per sample.

| MFG       | Family                 | Models | Samples | Days  | Err   | MTBF   |
|-----------|------------------------|--------|---------|-------|-------|--------|
| Samsung   | Samsung based SSDs     | 133    | 6236    | 765   | 10    | 1.91   |
| Samsung   | Unknown                | 18     | 617     | 442   | 1     | 1.18   |
