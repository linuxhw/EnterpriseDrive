HPE Solid State Drives
======================

This is a list of all tested HPE solid state drive models and their MTBFs. See
more info on reliability test in the [README](https://github.com/linuxhw/EnterpriseDrive).

SSD by Model
------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days — avg. days per sample,
Err  — avg. errors per sample,
MTBF — avg. MTBF in years per sample.

| MFG       | Model              | Size   | Samples | Days  | Err   | MTBF   |
|-----------|--------------------|--------|---------|-------|-------|--------|
| HPE       | MK0800GCTZB        | 800 GB | 1       | 2321  | 0     | 6.36   |
| HPE       | MK0240GFDKQ        | 240 GB | 1       | 1803  | 0     | 4.94   |
| HPE       | MK001920GWCFB      | 1.9 TB | 1       | 1602  | 0     | 4.39   |
| HPE       | MK0200GEYKC        | 200 GB | 21      | 1370  | 0     | 3.75   |
| HPE       | LK0480GFJSK        | 480 GB | 1       | 1103  | 0     | 3.02   |
| HPE       | MK000240GWEZF      | 240 GB | 10      | 651   | 0     | 1.79   |
| HPE       | MK000960GWUGH      | 960 GB | 6       | 704   | 1     | 1.71   |
| HPE       | MR000240GWFLU      | 240 GB | 2       | 588   | 0     | 1.61   |
| HPE       | MK001920GWUGK      | 1.9 TB | 2       | 488   | 0     | 1.34   |
| HPE       | MK0400GEYKD        | 400 GB | 1       | 421   | 0     | 1.15   |
| HPE       | MK000960GXAXB      | 960 GB | 5       | 339   | 0     | 0.93   |
| HPE       | MK000480GWXFF      | 480 GB | 13      | 311   | 0     | 0.85   |
| HPE       | MK001920GWJPQ      | 1.9 TB | 18      | 555   | 6     | 0.85   |
| HPE       | VR000240GXBBL      | 240 GB | 2       | 264   | 0     | 0.72   |
| HPE       | VR000480GWFMD      | 480 GB | 1       | 154   | 0     | 0.42   |
| HPE       | MK000480GWUGF      | 480 GB | 2       | 69    | 0     | 0.19   |
| HPE       | MK001920GWTTL      | 1.9 TB | 1       | 56    | 0     | 0.15   |
| HPE       | LK1600GEYMV        | 1.6 TB | 1       | 28    | 0     | 0.08   |
