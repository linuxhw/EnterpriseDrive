Apacer Solid State Drives
=========================

This is a list of all tested Apacer solid state drive models and their MTBFs. See
more info on reliability test in the [README](https://github.com/linuxhw/EnterpriseDrive).

Contents
--------

1. [ SSD by Model  ](#ssd-by-model)
2. [ SSD by Family ](#ssd-by-family)

SSD by Model
------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days — avg. days per sample,
Err  — avg. errors per sample,
MTBF — avg. MTBF in years per sample.

| MFG       | Model              | Size   | Samples | Days  | Err   | MTBF   |
|-----------|--------------------|--------|---------|-------|-------|--------|
| Apacer    | AS510S             | 128 GB | 1       | 1444  | 0     | 3.96   |
| Apacer    | AS330              | 240 GB | 1       | 799   | 0     | 2.19   |
| Apacer    | AS340              | 480 GB | 1       | 348   | 0     | 0.96   |
| Apacer    | AS340              | 960 GB | 43      | 322   | 20    | 0.59   |
| Apacer    | 480GB SATA Flas... | 480 GB | 3       | 157   | 0     | 0.43   |

SSD by Family
-------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days — avg. days per sample,
Err  — avg. errors per sample,
MTBF — avg. MTBF in years per sample.

| MFG       | Family                 | Models | Samples | Days  | Err   | MTBF   |
|-----------|------------------------|--------|---------|-------|-------|--------|
| Apacer    | Unknown                | 2      | 2       | 1122  | 0     | 3.07   |
| Apacer    | AS340 SSDs             | 2      | 44      | 323   | 20    | 0.60   |
| Apacer    | SSDs                   | 1      | 3       | 157   | 0     | 0.43   |
