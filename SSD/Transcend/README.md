Transcend Solid State Drives
============================

This is a list of all tested Transcend solid state drive models and their MTBFs. See
more info on reliability test in the [README](https://github.com/linuxhw/EnterpriseDrive).

Contents
--------

1. [ SSD by Model  ](#ssd-by-model)
2. [ SSD by Family ](#ssd-by-family)

SSD by Model
------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days — avg. days per sample,
Err  — avg. errors per sample,
MTBF — avg. MTBF in years per sample.

| MFG       | Model              | Size   | Samples | Days  | Err   | MTBF   |
|-----------|--------------------|--------|---------|-------|-------|--------|
| Transcend | TS128GSSD340       | 128 GB | 1       | 2182  | 0     | 5.98   |
| Transcend | TS256GSSD370       | 256 GB | 1       | 596   | 0     | 1.63   |
| Transcend | TSMSSSD01-240GP    | 240 GB | 4       | 572   | 0     | 1.57   |
| Transcend | TS128GSSD370       | 128 GB | 3       | 286   | 0     | 0.79   |
| Transcend | TS480GSSD220S      | 480 GB | 4       | 320   | 2     | 0.63   |
| Transcend | TSMSSSD01-120GP    | 120 GB | 2       | 227   | 0     | 0.62   |
| Transcend | TS1TSSD370S        | 1 TB   | 1       | 296   | 3     | 0.20   |
| Transcend | TS128GSSD370S      | 128 GB | 7       | 66    | 0     | 0.18   |
| Transcend | TS2TSSD230S        | 2 TB   | 2       | 46    | 0     | 0.13   |
| Transcend | TS256GMTS800S      | 256 GB | 5       | 31    | 0     | 0.09   |
| Transcend | TS1TSSD230S        | 1 TB   | 1       | 28    | 0     | 0.08   |
| Transcend | TS128GMTS800S      | 128 GB | 5       | 18    | 0     | 0.05   |

SSD by Family
-------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days — avg. days per sample,
Err  — avg. errors per sample,
MTBF — avg. MTBF in years per sample.

| MFG       | Family                 | Models | Samples | Days  | Err   | MTBF   |
|-----------|------------------------|--------|---------|-------|-------|--------|
| Transcend | JMicron/Maxiotek ba... | 1      | 1       | 2182  | 0     | 5.98   |
| Transcend | Unknown                | 3      | 8       | 354   | 0     | 0.97   |
| Transcend | Silicon Motion base... | 8      | 27      | 140   | 1     | 0.32   |
