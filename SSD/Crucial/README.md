Crucial Solid State Drives
==========================

This is a list of all tested Crucial solid state drive models and their MTBFs. See
more info on reliability test in the [README](https://github.com/linuxhw/EnterpriseDrive).

Contents
--------

1. [ SSD by Model  ](#ssd-by-model)
2. [ SSD by Family ](#ssd-by-family)

SSD by Model
------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days — avg. days per sample,
Err  — avg. errors per sample,
MTBF — avg. MTBF in years per sample.

| MFG       | Model              | Size   | Samples | Days  | Err   | MTBF   |
|-----------|--------------------|--------|---------|-------|-------|--------|
| Crucial   | M4-CT256M4SSD2     | 256 GB | 1       | 3117  | 0     | 8.54   |
| Crucial   | M4-CT128M4SSD2     | 128 GB | 2       | 1898  | 0     | 5.20   |
| Crucial   | CT250MX200SSD1     | 250 GB | 11      | 1827  | 0     | 5.01   |
| Crucial   | CT512MX100SSD1     | 240 GB | 4       | 2010  | 1     | 4.40   |
| Crucial   | CT1024MX200SSD1    | 1 TB   | 23      | 1469  | 0     | 4.03   |
| Crucial   | CT500MX200SSD1     | 500 GB | 16      | 1434  | 0     | 3.93   |
| Crucial   | CT750MX300SSD1     | 752 GB | 16      | 1569  | 1     | 3.74   |
| Crucial   | CT480BX200SSD1     | 480 GB | 1       | 1240  | 0     | 3.40   |
| Crucial   | CT960M500SSD1      | 960 GB | 18      | 1863  | 7     | 2.37   |
| Crucial   | CT275MX300SSD1     | 275 GB | 40      | 1026  | 1     | 2.28   |
| Crucial   | CT525MX300SSD1     | 528 GB | 49      | 892   | 1     | 2.22   |
| Crucial   | CT256MX100SSD1     | 256 GB | 4       | 1508  | 503   | 2.10   |
| Crucial   | CT500BX100SSD1     | 500 GB | 2       | 723   | 0     | 1.98   |
| Crucial   | CT1050MX300SSD1    | 1 TB   | 36      | 1032  | 2     | 1.93   |
| Crucial   | CT2050MX300SSD1    | 2 TB   | 22      | 1227  | 22    | 1.45   |
| Crucial   | CT250BX100SSD1     | 250 GB | 1       | 507   | 0     | 1.39   |
| Crucial   | CT120BX500SSD1     | 120 GB | 38      | 460   | 0     | 1.26   |
| Crucial   | CT1000BX100SSD1    | 1 TB   | 1       | 400   | 0     | 1.10   |
| Crucial   | CT240BX500SSD1     | 240 GB | 30      | 316   | 0     | 0.87   |
| Crucial   | CT960BX500SSD1     | 960 GB | 8       | 295   | 0     | 0.81   |
| Crucial   | CT240M500SSD1      | 240 GB | 11      | 1997  | 258   | 0.79   |
| Crucial   | CT500MX500SSD1     | 500 GB | 121     | 327   | 3     | 0.73   |
| Crucial   | CT2000MX500SSD1    | 2 TB   | 132     | 303   | 4     | 0.69   |
| Crucial   | CT250MX500SSD1     | 250 GB | 61      | 259   | 1     | 0.65   |
| Crucial   | CT1000BX500SSD1    | 1 TB   | 20      | 236   | 1     | 0.60   |
| Crucial   | CT120BX100SSD1     | 120 GB | 1       | 209   | 0     | 0.57   |
| Crucial   | CT960BX200SSD1     | 960 GB | 1       | 181   | 0     | 0.50   |
| Crucial   | CT1000MX500SSD4    | 1 TB   | 3       | 177   | 0     | 0.49   |
| Crucial   | CT1000MX500SSD1    | 1 TB   | 327     | 183   | 3     | 0.42   |
| Crucial   | CT240BX200SSD1     | 240 GB | 1       | 133   | 0     | 0.37   |
| Crucial   | CT480BX500SSD1     | 480 GB | 24      | 125   | 0     | 0.34   |
| Crucial   | CT250MX500SSD4     | 250 GB | 4       | 117   | 0     | 0.32   |
| Crucial   | CT1024M550SSD1     | 1 TB   | 5       | 2364  | 188   | 0.30   |
| Crucial   | CT120M500SSD1      | 120 GB | 2       | 2010  | 523   | 0.14   |
| Crucial   | CT512M550SSD1      | 512 GB | 2       | 740   | 16    | 0.12   |
| Crucial   | CT2000BX500SSD1    | 2 TB   | 12      | 34    | 0     | 0.09   |
| Crucial   | M4-CT256M4SSD1     | 256 GB | 1       | 3129  | 1011  | 0.01   |
| Crucial   | CT128MX100SSD1     | 128 GB | 1       | 1045  | 1061  | 0.00   |

SSD by Family
-------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days — avg. days per sample,
Err  — avg. errors per sample,
MTBF — avg. MTBF in years per sample.

| MFG       | Family                 | Models | Samples | Days  | Err   | MTBF   |
|-----------|------------------------|--------|---------|-------|-------|--------|
| Crucial   | RealSSD m4/C400        | 1      | 2       | 1898  | 0     | 5.20   |
| Crucial   | RealSSD m4/C400/P400   | 2      | 2       | 3123  | 506   | 4.27   |
| Crucial   | BX/MX1/2/3/500, M5/... | 11     | 134     | 1198  | 18    | 2.44   |
| Crucial   | MX1/2/300, M5/600, ... | 1      | 4       | 1508  | 503   | 2.10   |
| Crucial   | Silicon Motion base... | 5      | 6       | 634   | 0     | 1.74   |
| Crucial   | BX/MX1/2/3/500, M5/... | 9      | 242     | 865   | 18    | 1.72   |
| Crucial   | MX500 SSDs             | 6      | 648     | 241   | 3     | 0.55   |
| Crucial   | SiliconMotion based... | 2      | 2       | 157   | 0     | 0.43   |
| Crucial   | Client SSDs            | 1      | 12      | 34    | 0     | 0.09   |
