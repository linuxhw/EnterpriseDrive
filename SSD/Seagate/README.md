Seagate Solid State Drives
==========================

This is a list of all tested Seagate solid state drive models and their MTBFs. See
more info on reliability test in the [README](https://github.com/linuxhw/EnterpriseDrive).

Contents
--------

1. [ SSD by Model  ](#ssd-by-model)
2. [ SSD by Family ](#ssd-by-family)

SSD by Model
------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days — avg. days per sample,
Err  — avg. errors per sample,
MTBF — avg. MTBF in years per sample.

| MFG       | Model              | Size   | Samples | Days  | Err   | MTBF   |
|-----------|--------------------|--------|---------|-------|-------|--------|
| Seagate   | XF1230-1A0480      | 480 GB | 14      | 864   | 0     | 2.37   |
| Seagate   | XF1230-1A0240      | 240 GB | 10      | 740   | 0     | 2.03   |
| Seagate   | XF1230-1A0960      | 960 GB | 2       | 734   | 0     | 2.01   |
| Seagate   | BarraCuda SSD Z... | 250 GB | 2       | 585   | 0     | 1.60   |
| Seagate   | XA480ME10063       | 480 GB | 10      | 564   | 0     | 1.55   |
| Seagate   | XA1920LE10063      | 1.9 TB | 20      | 549   | 0     | 1.51   |
| Seagate   | XA960ME10063       | 960 GB | 2       | 535   | 0     | 1.47   |
| Seagate   | XA1920ME10063      | 1.9 TB | 35      | 523   | 0     | 1.44   |
| Seagate   | XA240ME10003       | 240 GB | 3       | 522   | 0     | 1.43   |
| Seagate   | ST120FN0021        | 120 GB | 1       | 502   | 0     | 1.38   |
| Seagate   | XA480LE10063       | 480 GB | 14      | 367   | 0     | 1.01   |
| Seagate   | XA960LE10063       | 960 GB | 98      | 346   | 0     | 0.95   |
| Seagate   | BarraCuda 120 S... | 500 GB | 2       | 56    | 0     | 0.16   |

SSD by Family
-------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days — avg. days per sample,
Err  — avg. errors per sample,
MTBF — avg. MTBF in years per sample.

| MFG       | Family                 | Models | Samples | Days  | Err   | MTBF   |
|-----------|------------------------|--------|---------|-------|-------|--------|
| Seagate   | Nytro XF1230 SATA SSD  | 3      | 26      | 806   | 0     | 2.21   |
| Seagate   | Nytro SATA SSD         | 7      | 182     | 421   | 0     | 1.15   |
| Seagate   | Unknown                | 3      | 5       | 357   | 0     | 0.98   |
