Team Solid State Drives
=======================

This is a list of all tested Team solid state drive models and their MTBFs. See
more info on reliability test in the [README](https://github.com/linuxhw/EnterpriseDrive).

SSD by Model
------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days — avg. days per sample,
Err  — avg. errors per sample,
MTBF — avg. MTBF in years per sample.

| MFG       | Model              | Size   | Samples | Days  | Err   | MTBF   |
|-----------|--------------------|--------|---------|-------|-------|--------|
| Team      | T2535T120G         | 120 GB | 2       | 846   | 0     | 2.32   |
| Team      | T253X1960G         | 960 GB | 3       | 780   | 0     | 2.14   |
| Team      | T253X6001T         | 1 TB   | 4       | 137   | 0     | 0.38   |
| Team      | T253X1480G         | 480 GB | 1       | 118   | 0     | 0.33   |
| Team      | T253A3001T         | 1 TB   | 30      | 46    | 0     | 0.13   |
