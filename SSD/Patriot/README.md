Patriot Solid State Drives
==========================

This is a list of all tested Patriot solid state drive models and their MTBFs. See
more info on reliability test in the [README](https://github.com/linuxhw/EnterpriseDrive).

Contents
--------

1. [ SSD by Model  ](#ssd-by-model)
2. [ SSD by Family ](#ssd-by-family)

SSD by Model
------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days — avg. days per sample,
Err  — avg. errors per sample,
MTBF — avg. MTBF in years per sample.

| MFG       | Model              | Size   | Samples | Days  | Err   | MTBF   |
|-----------|--------------------|--------|---------|-------|-------|--------|
| Patriot   | Blast              | 240 GB | 1       | 1030  | 0     | 2.82   |
| Patriot   | P200               | 2 TB   | 5       | 510   | 0     | 1.40   |
| Patriot   | Burst              | 480 GB | 2       | 472   | 0     | 1.30   |
| Patriot   | Burst              | 960 GB | 1       | 423   | 0     | 1.16   |
| Patriot   | Burst              | 120 GB | 1       | 421   | 0     | 1.16   |
| Patriot   | Burst              | 240 GB | 10      | 327   | 0     | 0.90   |
| Patriot   | P210               | 1 TB   | 4       | 107   | 0     | 0.30   |
| Patriot   | P210               | 2 TB   | 2       | 31    | 0     | 0.09   |

SSD by Family
-------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days — avg. days per sample,
Err  — avg. errors per sample,
MTBF — avg. MTBF in years per sample.

| MFG       | Family                 | Models | Samples | Days  | Err   | MTBF   |
|-----------|------------------------|--------|---------|-------|-------|--------|
| Patriot   | Silicon Motion base... | 1      | 5       | 510   | 0     | 1.40   |
| Patriot   | Phison Driven SSDs     | 3      | 13      | 404   | 0     | 1.11   |
| Patriot   | Unknown                | 4      | 8       | 167   | 0     | 0.46   |
