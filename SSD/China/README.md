China Solid State Drives
========================

This is a list of all tested China solid state drive models and their MTBFs. See
more info on reliability test in the [README](https://github.com/linuxhw/EnterpriseDrive).

SSD by Model
------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days — avg. days per sample,
Err  — avg. errors per sample,
MTBF — avg. MTBF in years per sample.

| MFG       | Model              | Size   | Samples | Days  | Err   | MTBF   |
|-----------|--------------------|--------|---------|-------|-------|--------|
| China     | SATA SSD           | 256 GB | 2       | 421   | 0     | 1.15   |
| China     | 512GB QLC SATA SSD | 512 GB | 6       | 350   | 0     | 0.96   |
| China     | SH00R480GB         | 480 GB | 2       | 510   | 4     | 0.77   |
| China     | SATA SSD           | 240 GB | 5       | 180   | 0     | 0.49   |
| China     | SATA SSD           | 480 GB | 3       | 166   | 0     | 0.46   |
| China     | SATA SSD           | 1 TB   | 13      | 52    | 0     | 0.14   |
| China     | SATA SSD           | 120 GB | 3       | 40    | 0     | 0.11   |
| China     | OSSD120GBTSS2      | 120 GB | 1       | 700   | 18    | 0.10   |
