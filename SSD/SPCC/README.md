SPCC Solid State Drives
=======================

This is a list of all tested SPCC solid state drive models and their MTBFs. See
more info on reliability test in the [README](https://github.com/linuxhw/EnterpriseDrive).

Contents
--------

1. [ SSD by Model  ](#ssd-by-model)
2. [ SSD by Family ](#ssd-by-family)

SSD by Model
------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days — avg. days per sample,
Err  — avg. errors per sample,
MTBF — avg. MTBF in years per sample.

| MFG       | Model              | Size   | Samples | Days  | Err   | MTBF   |
|-----------|--------------------|--------|---------|-------|-------|--------|
| SPCC      | SSD                | 240 GB | 1       | 1351  | 0     | 3.70   |
| SPCC      | SSD                | 120 GB | 1       | 1231  | 0     | 3.37   |
| SPCC      | SSD                | 1 TB   | 1       | 378   | 0     | 1.04   |
| SPCC      | SSD                | 512 GB | 6       | 294   | 0     | 0.81   |
| SPCC      | SSD                | 256 GB | 3       | 178   | 0     | 0.49   |
| SPCC      | SSD                | 128 GB | 1       | 50    | 0     | 0.14   |
| SPCC      | SSD                | 480 GB | 2       | 311   | 518   | 0.10   |

SSD by Family
-------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days — avg. days per sample,
Err  — avg. errors per sample,
MTBF — avg. MTBF in years per sample.

| MFG       | Family                 | Models | Samples | Days  | Err   | MTBF   |
|-----------|------------------------|--------|---------|-------|-------|--------|
| SPCC      | Unknown                | 5      | 11      | 456   | 95    | 1.11   |
| SPCC      | Phison Driven OEM SSDs | 2      | 4       | 228   | 0     | 0.63   |
