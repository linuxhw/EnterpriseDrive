SuperMicro Solid State Drives
=============================

This is a list of all tested SuperMicro solid state drive models and their MTBFs. See
more info on reliability test in the [README](https://github.com/linuxhw/EnterpriseDrive).

SSD by Model
------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days — avg. days per sample,
Err  — avg. errors per sample,
MTBF — avg. MTBF in years per sample.

| MFG       | Model              | Size   | Samples | Days  | Err   | MTBF   |
|-----------|--------------------|--------|---------|-------|-------|--------|
| SuperM... | SSD                | 64 GB  | 27      | 930   | 1     | 2.48   |
| SuperM... | SSD                | 128 GB | 131     | 721   | 11    | 1.92   |
| SuperM... | SSD                | 16 GB  | 29      | 641   | 0     | 1.76   |
| SuperM... | SSD                | 32 GB  | 6       | 457   | 0     | 1.25   |
