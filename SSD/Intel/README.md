Intel Solid State Drives
========================

This is a list of all tested Intel solid state drive models and their MTBFs. See
more info on reliability test in the [README](https://github.com/linuxhw/EnterpriseDrive).

Contents
--------

1. [ SSD by Model  ](#ssd-by-model)
2. [ SSD by Family ](#ssd-by-family)

SSD by Model
------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days — avg. days per sample,
Err  — avg. errors per sample,
MTBF — avg. MTBF in years per sample.

| MFG       | Model              | Size   | Samples | Days  | Err   | MTBF   |
|-----------|--------------------|--------|---------|-------|-------|--------|
| Intel     | SSDSA2CW300G3      | 304 GB | 1       | 3503  | 0     | 9.60   |
| Intel     | SSDSA2SH032G1GN    | 32 GB  | 3       | 2924  | 0     | 8.01   |
| Intel     | SSDSC2BP240G4      | 240 GB | 11      | 2269  | 0     | 6.22   |
| Intel     | SSDSA2CW120G3      | 120 GB | 41      | 2813  | 53    | 6.15   |
| Intel     | VR0120GEJXL        | 120 GB | 3       | 2118  | 0     | 5.80   |
| Intel     | SSDSC2BB080G4      | 80 GB  | 4       | 2067  | 0     | 5.66   |
| Intel     | SSDSC2CW240A3      | 240 GB | 55      | 2171  | 1     | 5.56   |
| Intel     | SSDSC2BW180A3L     | 180 GB | 1       | 2007  | 0     | 5.50   |
| Intel     | SSDSA2BZ200G3      | 200 GB | 2       | 2530  | 1     | 5.32   |
| Intel     | SSDSC2CW480A3      | 400 GB | 12      | 2083  | 1     | 5.27   |
| Intel     | SSDSC2BA100G3      | 100 GB | 28      | 2021  | 1     | 5.23   |
| Intel     | SSDSC2BB800G4      | 800 GB | 7       | 1882  | 0     | 5.16   |
| Intel     | SSDSC2CW180A3      | 180 GB | 14      | 1940  | 1     | 5.13   |
| Intel     | SSDSC2BB480G4      | 480 GB | 55      | 1977  | 19    | 5.12   |
| Intel     | SSDSC2BX012T4      | 1.2 TB | 4       | 1807  | 0     | 4.95   |
| Intel     | SSDSC2BB120G4      | 120 GB | 17      | 1885  | 1     | 4.88   |
| Intel     | SSDSC2CT120A3      | 120 GB | 16      | 2688  | 2     | 4.85   |
| Intel     | SSDSC2CT240A4      | 240 GB | 2       | 2646  | 1     | 4.84   |
| Intel     | SSDSC2BA400G3C     | 400 GB | 2       | 1733  | 0     | 4.75   |
| Intel     | SSDSC2BB160G4      | 160 GB | 57      | 2007  | 1     | 4.64   |
| Intel     | SSDSC2BB300H4      | 304 GB | 3       | 1663  | 0     | 4.56   |
| Intel     | SSDSC2BA400G3      | 400 GB | 5       | 1660  | 0     | 4.55   |
| Intel     | SSDSC2CT080A4      | 80 GB  | 1       | 1655  | 0     | 4.54   |
| Intel     | SSDSC2BB800G6      | 800 GB | 68      | 1732  | 15    | 4.43   |
| Intel     | SSDSC2BB080G6      | 80 GB  | 1       | 1603  | 0     | 4.39   |
| Intel     | SSDSC2BB012T6      | 1.2 TB | 5       | 1850  | 1     | 4.38   |
| Intel     | SSDSC2BB120G6      | 120 GB | 6       | 1593  | 0     | 4.37   |
| Intel     | SSDSC2BB120G6K     | 120 GB | 24      | 1581  | 0     | 4.33   |
| Intel     | SSDSC2BB300G4      | 304 GB | 1       | 1568  | 0     | 4.30   |
| Intel     | SSDSC2BB240G6      | 240 GB | 19      | 1563  | 0     | 4.28   |
| Intel     | SSDSC2BB016T6      | 1.6 TB | 13      | 1560  | 0     | 4.27   |
| Intel     | SSDSC2BA200G3      | 200 GB | 15      | 1662  | 1     | 4.17   |
| Intel     | SSDSC2BX400G4      | 400 GB | 3       | 1520  | 0     | 4.17   |
| Intel     | SSDSC2BB240G4      | 240 GB | 43      | 1570  | 1     | 4.15   |
| Intel     | SSDSC2BW240A3      | 240 GB | 1       | 1489  | 0     | 4.08   |
| Intel     | SSDSA2BW600G3D     | 600 GB | 2       | 2200  | 1     | 4.02   |
| Intel     | SSDSC2BB480H4      | 480 GB | 9       | 1611  | 1     | 3.97   |
| Intel     | SSDSC2BB016T4      | 1.6 TB | 5       | 1634  | 1     | 3.94   |
| Intel     | SSDSC2BB480G6      | 480 GB | 33      | 1384  | 0     | 3.79   |
| Intel     | SSDSC2BB600G4      | 600 GB | 11      | 1378  | 0     | 3.78   |
| Intel     | SSDSC2BB120G7R     | 120 GB | 14      | 1339  | 0     | 3.67   |
| Intel     | SSDSC2BP480G4      | 480 GB | 1       | 1339  | 0     | 3.67   |
| Intel     | SSDSC2BX200G4      | 200 GB | 18      | 1334  | 0     | 3.66   |
| Intel     | SSDSC2BA200G4      | 200 GB | 111     | 1297  | 0     | 3.55   |
| Intel     | SSDSC2BB150G7      | 150 GB | 12      | 1279  | 0     | 3.51   |
| Intel     | SSDSC2BB800G7R     | 800 GB | 40      | 1574  | 53    | 3.41   |
| Intel     | SSDSC2BA800G4      | 800 GB | 12      | 1185  | 0     | 3.25   |
| Intel     | SSDSC2KB960G7R     | 960 GB | 10      | 1183  | 0     | 3.24   |
| Intel     | SSDSC2BX800G4      | 800 GB | 2       | 1142  | 0     | 3.13   |
| Intel     | SSDSC2BA400G4      | 400 GB | 20      | 1120  | 0     | 3.07   |
| Intel     | SSDSC2BA200G4C     | 200 GB | 5       | 1092  | 0     | 2.99   |
| Intel     | SSDSC2BX100G4      | 100 GB | 2       | 1090  | 0     | 2.99   |
| Intel     | SSDSC2BB240G7      | 240 GB | 21      | 1067  | 0     | 2.92   |
| Intel     | SSDSCKJB150G7      | 150 GB | 14      | 1088  | 1     | 2.63   |
| Intel     | SSDSC2BB480G7      | 480 GB | 107     | 1075  | 1     | 2.55   |
| Intel     | SSDSA2BW160G3L     | 160 GB | 1       | 914   | 0     | 2.50   |
| Intel     | SSDSC2KG480G7      | 480 GB | 54      | 1131  | 2     | 2.48   |
| Intel     | SSDSC2BX480G4      | 480 GB | 24      | 871   | 0     | 2.39   |
| Intel     | SSDSC2BX016T4      | 1.6 TB | 1       | 849   | 0     | 2.33   |
| Intel     | SSDSC2MH120A2      | 120 GB | 2       | 842   | 0     | 2.31   |
| Intel     | SSDSC2KG240G7R     | 240 GB | 25      | 877   | 83    | 2.25   |
| Intel     | SSDSA2CW160G3      | 160 GB | 1       | 1623  | 1     | 2.22   |
| Intel     | SSDSC2KG240G7      | 240 GB | 42      | 815   | 1     | 2.21   |
| Intel     | SSDMCEAC120B3      | 120 GB | 1       | 788   | 0     | 2.16   |
| Intel     | SSDSC2BB016T7      | 1.6 TB | 61      | 1477  | 4     | 2.12   |
| Intel     | SSDSC2BA012T4      | 1.2 TB | 1       | 745   | 0     | 2.04   |
| Intel     | SSDSC2KB240G7      | 240 GB | 64      | 830   | 8     | 2.04   |
| Intel     | SSDSC2KG960G7      | 960 GB | 46      | 1150  | 4     | 1.98   |
| Intel     | SSDSC2BB480G7R     | 480 GB | 24      | 747   | 1     | 1.95   |
| Intel     | SSDSC2BW120H6      | 120 GB | 4       | 814   | 3     | 1.80   |
| Intel     | SSDSC2BF120A4H     | 120 GB | 2       | 647   | 0     | 1.77   |
| Intel     | SSDSC2BW180A4      | 180 GB | 2       | 644   | 0     | 1.77   |
| Intel     | SSDSC2BB012T7      | 1.2 TB | 172     | 1087  | 3     | 1.62   |
| Intel     | SSDSC2BB800G7      | 800 GB | 1       | 1164  | 1     | 1.59   |
| Intel     | SSDSC2BB480G7O     | 480 GB | 3       | 1550  | 4     | 1.57   |
| Intel     | SSDSC2BW120A4      | 120 GB | 45      | 715   | 1     | 1.53   |
| Intel     | SSDSC2KB019T7      | 1.9 TB | 65      | 866   | 4     | 1.49   |
| Intel     | SSDSC2KB960G7      | 960 GB | 175     | 1188  | 12    | 1.45   |
| Intel     | SSDSC2KG019T7R     | 1.9 TB | 15      | 820   | 3     | 1.44   |
| Intel     | SSDSC2BW240A4      | 240 GB | 44      | 853   | 6     | 1.42   |
| Intel     | SSDSC2KG240G8R     | 240 GB | 1       | 508   | 0     | 1.39   |
| Intel     | SSDSC2KG240G8      | 240 GB | 168     | 503   | 0     | 1.38   |
| Intel     | SSDSC2BF120A4H SED | 120 GB | 2       | 497   | 0     | 1.36   |
| Intel     | SSDSC2KB480G7      | 480 GB | 41      | 587   | 1     | 1.36   |
| Intel     | SSDSC2KB240G8      | 240 GB | 688     | 516   | 2     | 1.36   |
| Intel     | SSDSC2KB480G8      | 480 GB | 268     | 499   | 1     | 1.33   |
| Intel     | SSDSC2BB960G7      | 960 GB | 34      | 909   | 2     | 1.33   |
| Intel     | SSDSC2BB800H4      | 800 GB | 6       | 473   | 0     | 1.30   |
| Intel     | SSDSA2BW120G3      | 120 GB | 1       | 2358  | 4     | 1.29   |
| Intel     | SSDSC2KB960G8      | 960 GB | 803     | 528   | 1     | 1.26   |
| Intel     | SSDSCKKB240G8      | 240 GB | 17      | 453   | 0     | 1.24   |
| Intel     | SSDSC2KB019T8      | 1.9 TB | 236     | 559   | 9     | 1.20   |
| Intel     | SSDSC2KG960G8      | 960 GB | 146     | 464   | 1     | 1.11   |
| Intel     | SSDSC2CW120A3      | 120 GB | 34      | 2108  | 810   | 1.11   |
| Intel     | SSDSC2KB480G8R     | 480 GB | 18      | 386   | 0     | 1.06   |
| Intel     | SSDSC2BW240H6      | 240 GB | 14      | 605   | 25    | 1.03   |
| Intel     | SSDSC2BW480H6      | 480 GB | 5       | 639   | 4     | 1.02   |
| Intel     | SSDSC2KB480G7R     | 480 GB | 6       | 367   | 0     | 1.01   |
| Intel     | SSDSC2KG019T8      | 1.9 TB | 214     | 392   | 1     | 0.94   |
| Intel     | SSDSC2KG480G8      | 480 GB | 239     | 376   | 1     | 0.93   |
| Intel     | SSDSC2KB019T7R     | 1.9 TB | 23      | 417   | 2     | 0.82   |
| Intel     | SSDSC2BW120A3F     | 120 GB | 20      | 2107  | 917   | 0.75   |
| Intel     | SSDSC2BW256H6      | 256 GB | 1       | 268   | 0     | 0.74   |
| Intel     | SSDSCKJB240G7      | 240 GB | 3       | 267   | 0     | 0.73   |
| Intel     | SSDSC2KB960G8R     | 960 GB | 7       | 267   | 0     | 0.73   |
| Intel     | SSDSC2BW080A4      | 80 GB  | 1       | 1589  | 5     | 0.73   |
| Intel     | SSDSC2KG019T7      | 1.9 TB | 1       | 243   | 0     | 0.67   |
| Intel     | SSDSC2KW128G8      | 128 GB | 9       | 219   | 2     | 0.57   |
| Intel     | SSDSA2M080G2GC     | 80 GB  | 1       | 2595  | 12    | 0.55   |
| Intel     | SSDSC2CT060A3      | 64 GB  | 1       | 786   | 3     | 0.54   |
| Intel     | SSDSC2KG960G8R     | 960 GB | 16      | 245   | 2     | 0.51   |
| Intel     | SSDSC2KG038T8      | 3.8 TB | 10      | 227   | 1     | 0.51   |
| Intel     | SSDSC2KB038T8      | 3.8 TB | 24      | 233   | 1     | 0.49   |
| Intel     | SSDSCKKI256G8      | 256 GB | 1       | 173   | 0     | 0.47   |
| Intel     | SSDSC2BF180A4      | 180 GB | 2       | 243   | 2     | 0.40   |
| Intel     | SSDSA2CT040G3      | 40 GB  | 1       | 125   | 0     | 0.34   |
| Intel     | SSDSC2BF240A4H     | 240 GB | 1       | 622   | 4     | 0.34   |
| Intel     | SSDSC2KW256G8      | 256 GB | 24      | 142   | 6     | 0.32   |
| Intel     | SSDSCKKW128G8      | 128 GB | 3       | 103   | 0     | 0.28   |
| Intel     | SSDSC2KG019T8R     | 1.9 TB | 25      | 100   | 0     | 0.27   |
| Intel     | SSDSC2KW512G8      | 512 GB | 6       | 123   | 3     | 0.26   |
| Intel     | SSDSCKKB960G8      | 960 GB | 1       | 72    | 0     | 0.20   |
| Intel     | SSDSC2BF240A4L     | 240 GB | 1       | 740   | 12    | 0.16   |
| Intel     | SSDSC2BB480G7K     | 480 GB | 1       | 48    | 0     | 0.13   |
| Intel     | SSDSC2BW480A4      | 480 GB | 3       | 514   | 24    | 0.09   |
| Intel     | SSDSC2KI128G8      | 128 GB | 1       | 56    | 1     | 0.08   |
| Intel     | SSDSC2KB240GZ      | 240 GB | 5       | 24    | 0     | 0.07   |
| Intel     | SSDSC2BA400G3I ... | 400 GB | 2       | 13    | 0     | 0.04   |
| Intel     | SSDSC2KW240H6      | 240 GB | 1       | 201   | 27    | 0.02   |
| Intel     | SSDSC2KW120H6      | 120 GB | 1       | 6     | 0     | 0.02   |
| Intel     | SSDSC2KF256H6 SATA | 256 GB | 1       | 169   | 42    | 0.01   |
| Intel     | SSDSC2BG200G4R     | 200 GB | 7       | 2303  | 1029  | 0.01   |
| Intel     | SSDSC2BA200G3R     | 200 GB | 21      | 2279  | 1029  | 0.01   |
| Intel     | SSDSC2BX016T4R     | 1.6 TB | 32      | 2003  | 1029  | 0.01   |
| Intel     | SSDSC1BG400G4R     | 400 GB | 16      | 1975  | 1028  | 0.01   |
| Intel     | SSDSC2CW060A3      | 64 GB  | 1       | 1920  | 1020  | 0.01   |
| Intel     | SSDSC2BB300G4T     | 304 GB | 2       | 1925  | 1028  | 0.01   |
| Intel     | SSDSC2BA200G3T     | 200 GB | 4       | 1733  | 1028  | 0.00   |
| Intel     | SSDSC2BX200G4R     | 200 GB | 21      | 1707  | 1028  | 0.00   |
| Intel     | SSDSC2BW120A3      | 120 GB | 1       | 1687  | 1019  | 0.00   |
| Intel     | SSDSC1BG200G4R     | 200 GB | 33      | 1528  | 1028  | 0.00   |
| Intel     | SSDSC2BB480G6R     | 480 GB | 1       | 1446  | 1028  | 0.00   |
| Intel     | SSDSC2BX400G4R     | 400 GB | 78      | 1361  | 1028  | 0.00   |
| Intel     | SSDSC2BA100G3T     | 100 GB | 3       | 1336  | 1028  | 0.00   |
| Intel     | SSDSC2BA800G4R     | 800 GB | 71      | 1243  | 1042  | 0.00   |
| Intel     | SSDSC2BA200G4R     | 200 GB | 12      | 1078  | 1027  | 0.00   |
| Intel     | SSDSC2KG960G8T     | 960 GB | 1       | 822   | 1026  | 0.00   |
| Intel     | SSDSC2BB800G4T     | 800 GB | 1       | 807   | 1027  | 0.00   |
| Intel     | SSDSC2BX800G4R     | 800 GB | 4       | 357   | 1027  | 0.00   |
| Intel     | SSDSC2KW480H6      | 480 GB | 1       | 115   | 393   | 0.00   |

SSD by Family
-------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days — avg. days per sample,
Err  — avg. errors per sample,
MTBF — avg. MTBF in years per sample.

| MFG       | Family                 | Models | Samples | Days  | Err   | MTBF   |
|-----------|------------------------|--------|---------|-------|-------|--------|
| Intel     | X25-E SSDs             | 1      | 3       | 2924  | 0     | 8.01   |
| Intel     | 320 Series SSDs        | 7      | 48      | 2672  | 45    | 5.75   |
| Intel     | 710 Series SSDs        | 1      | 2       | 2530  | 1     | 5.32   |
| Intel     | 330/335 Series SSDs    | 3      | 19      | 2583  | 2     | 4.62   |
| Intel     | 520 Series SSDs        | 9      | 139     | 2104  | 345   | 3.62   |
| Intel     | 730 and DC S35x0/36... | 50     | 980     | 1478  | 232   | 3.01   |
| Intel     | Dell Certified Inte... | 3      | 78      | 1277  | 27    | 3.01   |
| Intel     | 3710 Series SSDs       | 1      | 5       | 1092  | 0     | 2.99   |
| Intel     | 510 Series SSDs        | 1      | 2       | 842   | 0     | 2.31   |
| Intel     | 525 Series SSDs        | 1      | 1       | 788   | 0     | 2.16   |
| Intel     | S3520 Series SSDs      | 7      | 288     | 1141  | 3     | 1.73   |
| Intel     | 53x and Pro 1500/25... | 11     | 123     | 743   | 6     | 1.36   |
| Intel     | S4510/S4610/S4500/S... | 21     | 3303    | 568   | 3     | 1.30   |
| Intel     | Dell Certified Inte... | 10     | 146     | 504   | 15    | 1.20   |
| Intel     | Unknown                | 15     | 131     | 1634  | 691   | 1.07   |
| Intel     | X18-M/X25-M/X25-V G... | 1      | 1       | 2595  | 12    | 0.55   |
| Intel     | DC S3110 Series SSDs   | 1      | 1       | 173   | 0     | 0.47   |
| Intel     | 545s Series SSDs       | 4      | 42      | 153   | 4     | 0.36   |
| Intel     | 540 Series SSDs        | 3      | 3       | 108   | 140   | 0.01   |
