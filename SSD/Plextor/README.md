Plextor Solid State Drives
==========================

This is a list of all tested Plextor solid state drive models and their MTBFs. See
more info on reliability test in the [README](https://github.com/linuxhw/EnterpriseDrive).

Contents
--------

1. [ SSD by Model  ](#ssd-by-model)
2. [ SSD by Family ](#ssd-by-family)

SSD by Model
------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days — avg. days per sample,
Err  — avg. errors per sample,
MTBF — avg. MTBF in years per sample.

| MFG       | Model              | Size   | Samples | Days  | Err   | MTBF   |
|-----------|--------------------|--------|---------|-------|-------|--------|
| Plextor   | PX-128M5S          | 128 GB | 1       | 1971  | 0     | 5.40   |
| Plextor   | PH6-CE120-L1       | 120 GB | 1       | 674   | 0     | 1.85   |
| Plextor   | PX-256M6S+         | 256 GB | 2       | 527   | 0     | 1.44   |
| Plextor   | PX-128S3C          | 128 GB | 1       | 490   | 0     | 1.34   |

SSD by Family
-------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days — avg. days per sample,
Err  — avg. errors per sample,
MTBF — avg. MTBF in years per sample.

| MFG       | Family                 | Models | Samples | Days  | Err   | MTBF   |
|-----------|------------------------|--------|---------|-------|-------|--------|
| Plextor   | M3/M5/M6 Series SSDs   | 1      | 1       | 1971  | 0     | 5.40   |
| Plextor   | Unknown                | 2      | 2       | 582   | 0     | 1.60   |
| Plextor   | M3/M5/M6/M7 Series ... | 1      | 2       | 527   | 0     | 1.44   |
