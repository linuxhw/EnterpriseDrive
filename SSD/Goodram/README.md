Goodram Solid State Drives
==========================

This is a list of all tested Goodram solid state drive models and their MTBFs. See
more info on reliability test in the [README](https://github.com/linuxhw/EnterpriseDrive).

Contents
--------

1. [ SSD by Model  ](#ssd-by-model)
2. [ SSD by Family ](#ssd-by-family)

SSD by Model
------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days — avg. days per sample,
Err  — avg. errors per sample,
MTBF — avg. MTBF in years per sample.

| MFG       | Model              | Size   | Samples | Days  | Err   | MTBF   |
|-----------|--------------------|--------|---------|-------|-------|--------|
| Goodram   | SSD                | 240 GB | 4       | 962   | 0     | 2.64   |
| Goodram   | IR-SSDPR-S25A-240  | 240 GB | 3       | 577   | 0     | 1.58   |
| Goodram   | SSDPR-CX300-120    | 120 GB | 2       | 538   | 0     | 1.47   |
| Goodram   | IR-SSDPR-S25A-120  | 120 GB | 7       | 507   | 0     | 1.39   |
| Goodram   | SSDPR-CX400-256    | 256 GB | 1       | 465   | 0     | 1.28   |
| Goodram   | SSDPR-CX400-512    | 512 GB | 3       | 446   | 0     | 1.22   |

SSD by Family
-------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days — avg. days per sample,
Err  — avg. errors per sample,
MTBF — avg. MTBF in years per sample.

| MFG       | Family                 | Models | Samples | Days  | Err   | MTBF   |
|-----------|------------------------|--------|---------|-------|-------|--------|
| Goodram   | Phison Driven OEM SSDs | 1      | 4       | 962   | 0     | 2.64   |
| Goodram   | Unknown                | 1      | 2       | 538   | 0     | 1.47   |
| Goodram   | Phison Driven SSDs     | 4      | 14      | 506   | 0     | 1.39   |
