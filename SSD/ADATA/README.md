ADATA Solid State Drives
========================

This is a list of all tested ADATA solid state drive models and their MTBFs. See
more info on reliability test in the [README](https://github.com/linuxhw/EnterpriseDrive).

Contents
--------

1. [ SSD by Model  ](#ssd-by-model)
2. [ SSD by Family ](#ssd-by-family)

SSD by Model
------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days — avg. days per sample,
Err  — avg. errors per sample,
MTBF — avg. MTBF in years per sample.

| MFG       | Model              | Size   | Samples | Days  | Err   | MTBF   |
|-----------|--------------------|--------|---------|-------|-------|--------|
| ADATA     | SP600              | 128 GB | 1       | 2000  | 0     | 5.48   |
| ADATA     | SP900              | 256 GB | 2       | 2368  | 2     | 3.89   |
| ADATA     | SU800              | 128 GB | 2       | 836   | 0     | 2.29   |
| ADATA     | SP600NS34          | 128 GB | 1       | 772   | 0     | 2.12   |
| ADATA     | SP580              | 120 GB | 2       | 990   | 505   | 0.86   |
| ADATA     | SP550              | 240 GB | 2       | 295   | 0     | 0.81   |
| ADATA     | SP550              | 120 GB | 3       | 446   | 166   | 0.72   |
| ADATA     | SU630              | 960 GB | 1       | 676   | 2     | 0.62   |
| ADATA     | SU800              | 256 GB | 1       | 172   | 0     | 0.47   |
| ADATA     | SU750              | 512 GB | 4       | 163   | 0     | 0.45   |
| ADATA     | SU800              | 2 TB   | 4       | 250   | 1     | 0.40   |
| ADATA     | SU650              | 120 GB | 4       | 146   | 0     | 0.40   |
| ADATA     | SU800              | 1 TB   | 2       | 74    | 0     | 0.21   |
| ADATA     | SU800              | 512 GB | 1       | 342   | 4     | 0.19   |
| ADATA     | SU630 1.9TB        | 1.9 TB | 2       | 54    | 0     | 0.15   |
| ADATA     | SX900              | 512 GB | 2       | 2028  | 1019  | 0.01   |

SSD by Family
-------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days — avg. days per sample,
Err  — avg. errors per sample,
MTBF — avg. MTBF in years per sample.

| MFG       | Family                 | Models | Samples | Days  | Err   | MTBF   |
|-----------|------------------------|--------|---------|-------|-------|--------|
| ADATA     | JMicron/Maxiotek ba... | 1      | 1       | 2000  | 0     | 5.48   |
| ADATA     | SandForce Driven SSDs  | 1      | 2       | 2368  | 2     | 3.89   |
| ADATA     | Silicon Motion base... | 5      | 12      | 377   | 42    | 0.85   |
| ADATA     | Unknown                | 9      | 19      | 503   | 161   | 0.48   |
