SanDisk Solid State Drives
==========================

This is a list of all tested SanDisk solid state drive models and their MTBFs. See
more info on reliability test in the [README](https://github.com/linuxhw/EnterpriseDrive).

Contents
--------

1. [ SSD by Model  ](#ssd-by-model)
2. [ SSD by Family ](#ssd-by-family)

SSD by Model
------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days — avg. days per sample,
Err  — avg. errors per sample,
MTBF — avg. MTBF in years per sample.

| MFG       | Model              | Size   | Samples | Days  | Err   | MTBF   |
|-----------|--------------------|--------|---------|-------|-------|--------|
| SanDisk   | SDSSDP064G         | 64 GB  | 1       | 2475  | 0     | 6.78   |
| SanDisk   | SDSSDHII960G       | 960 GB | 1       | 1708  | 0     | 4.68   |
| SanDisk   | SDLF1DAR480G-1HHS  | 480 GB | 2       | 1517  | 0     | 4.16   |
| SanDisk   | X300 MSATA         | 256 GB | 1       | 1486  | 0     | 4.07   |
| SanDisk   | SD7UB2Q512G1022    | 512 GB | 7       | 1498  | 1     | 3.81   |
| SanDisk   | SDLF1CRM-017T-1HST | 1.7 TB | 4       | 1383  | 0     | 3.79   |
| SanDisk   | SDLF1DAR-960G-1HA2 | 960 GB | 9       | 1339  | 0     | 3.67   |
| SanDisk   | Ultra II           | 960 GB | 12      | 1236  | 0     | 3.39   |
| SanDisk   | SDLF1CRR019T-1HHS  | 1.9 TB | 30      | 1208  | 34    | 3.28   |
| SanDisk   | SD7UB2Q512G1122    | 512 GB | 6       | 1465  | 2     | 3.21   |
| SanDisk   | SD6SB1M064G1022I   | 64 GB  | 6       | 1119  | 0     | 3.07   |
| SanDisk   | SDSSDA960G         | 960 GB | 1       | 1074  | 0     | 2.94   |
| SanDisk   | SDSSDH3250G        | 250 GB | 1       | 881   | 0     | 2.41   |
| SanDisk   | Ultra II           | 480 GB | 15      | 892   | 1     | 2.36   |
| SanDisk   | SSD PLUS 240 GB    | 240 GB | 11      | 787   | 0     | 2.16   |
| SanDisk   | SDSSDA240G         | 240 GB | 2       | 768   | 0     | 2.11   |
| SanDisk   | SD7SB7S512G1122    | 512 GB | 1       | 762   | 0     | 2.09   |
| SanDisk   | SDSSDH31000G       | 1 TB   | 1       | 743   | 0     | 2.04   |
| SanDisk   | SD8SB8U256G1122    | 256 GB | 5       | 666   | 0     | 1.83   |
| SanDisk   | SSD PLUS 480 GB    | 480 GB | 11      | 777   | 2     | 1.73   |
| SanDisk   | SDLF1CRR-019T-1HA1 | 1.9 TB | 10      | 650   | 1     | 1.67   |
| SanDisk   | SDLFOCAM-800G-1HA1 | 800 GB | 1       | 577   | 0     | 1.58   |
| SanDisk   | SDSSDHP256G        | 256 GB | 1       | 541   | 0     | 1.48   |
| SanDisk   | SDSSDA480G         | 480 GB | 2       | 509   | 0     | 1.40   |
| SanDisk   | SDSSDH3 1T00       | 1 TB   | 3       | 425   | 0     | 1.17   |
| SanDisk   | SD6SB1M128G1022I   | 128 GB | 1       | 416   | 0     | 1.14   |
| SanDisk   | SSD PLUS           | 480 GB | 23      | 545   | 6     | 1.00   |
| SanDisk   | SDSSDA120G         | 120 GB | 3       | 317   | 0     | 0.87   |
| SanDisk   | pSSD               | 32 GB  | 1       | 314   | 0     | 0.86   |
| SanDisk   | SSD G5 BICS4       | 1 TB   | 5       | 322   | 1     | 0.80   |
| SanDisk   | SSD PLUS           | 240 GB | 19      | 283   | 1     | 0.74   |
| SanDisk   | SSD PLUS           | 120 GB | 24      | 260   | 1     | 0.65   |
| SanDisk   | SDSA5GK-016G-1006  | 16 GB  | 1       | 219   | 0     | 0.60   |
| SanDisk   | SDSSDH3 2T00       | 2 TB   | 6       | 209   | 1     | 0.51   |
| SanDisk   | SSD PLUS           | 1 TB   | 1       | 294   | 1     | 0.40   |
| SanDisk   | SDSSDX480GG25      | 480 GB | 1       | 2863  | 26    | 0.29   |
| SanDisk   | SDSSDH3 250G       | 250 GB | 1       | 99    | 0     | 0.27   |
| SanDisk   | SD8SB8U128G1122    | 128 GB | 1       | 261   | 2     | 0.24   |
| SanDisk   | SDSSDH3512G        | 512 GB | 2       | 82    | 0     | 0.23   |
| SanDisk   | SD7UB3Q256G1122    | 256 GB | 1       | 700   | 10    | 0.17   |
| SanDisk   | SD8SB8U-128G-1006  | 128 GB | 1       | 14    | 0     | 0.04   |
| SanDisk   | SD8SBAT256G1122    | 256 GB | 2       | 297   | 921   | 0.00   |

SSD by Family
-------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days — avg. days per sample,
Err  — avg. errors per sample,
MTBF — avg. MTBF in years per sample.

| MFG       | Family                 | Models | Samples | Days  | Err   | MTBF   |
|-----------|------------------------|--------|---------|-------|-------|--------|
| SanDisk   | SanDisk based SSDs     | 5      | 16      | 1423  | 2     | 3.36   |
| SanDisk   | SATA Cloudspeed Max... | 5      | 55      | 1152  | 19    | 3.12   |
| SanDisk   | SATA CS1K GEN1 ESS ... | 1      | 1       | 577   | 0     | 1.58   |
| SanDisk   | Marvell based SanDi... | 19     | 145     | 602   | 2     | 1.51   |
| SanDisk   | SandForce Driven SSDs  | 4      | 8       | 796   | 4     | 1.24   |
| SanDisk   | Unknown                | 8      | 12      | 473   | 154   | 1.16   |
