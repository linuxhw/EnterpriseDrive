Micron Solid State Drives
=========================

This is a list of all tested Micron solid state drive models and their MTBFs. See
more info on reliability test in the [README](https://github.com/linuxhw/EnterpriseDrive).

Contents
--------

1. [ SSD by Model  ](#ssd-by-model)
2. [ SSD by Family ](#ssd-by-family)

SSD by Model
------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days — avg. days per sample,
Err  — avg. errors per sample,
MTBF — avg. MTBF in years per sample.

| MFG       | Model              | Size   | Samples | Days  | Err   | MTBF   |
|-----------|--------------------|--------|---------|-------|-------|--------|
| Micron    | MTFDDAK512MAR-1... | 512 GB | 3       | 2870  | 0     | 7.86   |
| Micron    | MTFDDAK128MAR-1... | 128 GB | 3       | 2373  | 0     | 6.50   |
| Micron    | P400e-MTFDDAK40... | 400 GB | 3       | 1995  | 0     | 5.47   |
| Micron    | MTFDDAK128MAM-1J1  | 128 GB | 4       | 1983  | 0     | 5.43   |
| Micron    | M500DC_MTFDDAK1... | 120 GB | 2       | 1890  | 0     | 5.18   |
| Micron    | MTFDDAK256MAR-1... | 256 GB | 3       | 2481  | 1     | 5.13   |
| Micron    | 5100_MTFDDAK240TCC | 240 GB | 6       | 1452  | 1     | 3.51   |
| Micron    | 5100_MTFDDAK960TBY | 960 GB | 4       | 1044  | 0     | 2.86   |
| Micron    | M510DC_MTFDDAK4... | 480 GB | 3       | 854   | 0     | 2.34   |
| Micron    | M600_MTFDDAK1T0MBF | 1 TB   | 1       | 782   | 0     | 2.14   |
| Micron    | 5100_MTFDDAK240TCB | 240 GB | 28      | 960   | 2     | 2.04   |
| Micron    | 5100_MTFDDAK1T9TBY | 1.9 TB | 74      | 1272  | 20    | 2.01   |
| Micron    | 5100_MTFDDAK480TCC | 480 GB | 18      | 876   | 1     | 2.01   |
| Micron    | 5200_MTFDDAK1T9TDC | 1.9 TB | 10      | 873   | 2     | 1.94   |
| Micron    | 5200_MTFDDAK240TDN | 240 GB | 40      | 642   | 0     | 1.76   |
| Micron    | 5100_MTFDDAK960TCB | 960 GB | 128     | 960   | 3     | 1.73   |
| Micron    | 1100_MTFDDAK256TBN | 256 GB | 31      | 728   | 34    | 1.73   |
| Micron    | 5200_MTFDDAK480TDN | 480 GB | 17      | 625   | 0     | 1.71   |
| Micron    | 5100_MTFDDAK480TBY | 480 GB | 15      | 808   | 3     | 1.70   |
| Micron    | 1100_MTFDDAK512TBN | 512 GB | 38      | 989   | 53    | 1.69   |
| Micron    | 5200_MTFDDAK480TDC | 480 GB | 27      | 623   | 1     | 1.68   |
| Micron    | 1300_MTFDDAK256TDL | 256 GB | 31      | 642   | 1     | 1.67   |
| Micron    | MTFDDAK480TDN      | 480 GB | 38      | 604   | 1     | 1.62   |
| Micron    | M500DC_MTFDDAK8... | 800 GB | 27      | 601   | 1     | 1.62   |
| Micron    | 5200_MTFDDAK1T9TDD | 1.9 TB | 68      | 645   | 1     | 1.60   |
| Micron    | 5200_MTFDDAK3T8TDC | 3.8 TB | 29      | 566   | 1     | 1.50   |
| Micron    | 5200_MTFDDAK960TDD | 960 GB | 133     | 560   | 1     | 1.47   |
| Micron    | 5200_MTFDDAK960TDC | 960 GB | 45      | 567   | 1     | 1.46   |
| Micron    | MTFDDAK128MBF-1... | 128 GB | 13      | 526   | 0     | 1.44   |
| Micron    | MTFDDAK960TCB      | 960 GB | 12      | 617   | 1     | 1.36   |
| Micron    | 5200_MTFDDAK1T9TDN | 1.9 TB | 19      | 554   | 1     | 1.34   |
| Micron    | 1100_MTFDDAK2T0TBN | 2 TB   | 16      | 487   | 1     | 1.25   |
| Micron    | 1100_MTFDDAK1T0TBN | 1 TB   | 6       | 1224  | 146   | 1.20   |
| Micron    | 5210_MTFDDAK1T9QDE | 1.9 TB | 4       | 419   | 0     | 1.15   |
| Micron    | 5300_MTFDDAK960TDS | 960 GB | 139     | 426   | 1     | 1.14   |
| Micron    | 5100_MTFDDAK1T9TCB | 1.9 TB | 3       | 1328  | 694   | 1.11   |
| Micron    | 5200_MTFDDAK960TDN | 960 GB | 2       | 401   | 0     | 1.10   |
| Micron    | MTFDDAV240TCB      | 240 GB | 13      | 407   | 1     | 1.09   |
| Micron    | MTFDDAV480TDS-1... | 480 GB | 1       | 393   | 0     | 1.08   |
| Micron    | 5100_MTFDDAK1T9TCC | 1.9 TB | 17      | 422   | 1     | 1.07   |
| Micron    | 1300_MTFDDAK1T0TDL | 1 TB   | 9       | 414   | 1     | 1.03   |
| Micron    | 5100_MTFDDAV240TCB | 240 GB | 12      | 500   | 19    | 1.03   |
| Micron    | MTFDDAK480TDS-1... | 480 GB | 9       | 392   | 79    | 0.95   |
| Micron    | 5100_MTFDDAK960TCC | 960 GB | 9       | 422   | 1     | 0.95   |
| Micron    | 5300_MTFDDAK1T9TDT | 1.9 TB | 109     | 330   | 1     | 0.87   |
| Micron    | 5100_MTFDDAK480TCB | 480 GB | 24      | 961   | 140   | 0.83   |
| Micron    | 5300_MTFDDAK240TDS | 240 GB | 9       | 279   | 0     | 0.76   |
| Micron    | MTFDDAK480TDS      | 480 GB | 8       | 267   | 0     | 0.73   |
| Micron    | 1100 SATA          | 256 GB | 1       | 264   | 0     | 0.72   |
| Micron    | 5300_MTFDDAV240TDS | 240 GB | 6       | 238   | 0     | 0.65   |
| Micron    | 5300_MTFDDAK7T6TDS | 7.6 TB | 11      | 230   | 0     | 0.63   |
| Micron    | MTFDDAK256MAM-1K1  | 256 GB | 1       | 219   | 0     | 0.60   |
| Micron    | M510DC_MTFDDAK9... | 960 GB | 1       | 1953  | 8     | 0.59   |
| Micron    | 5300_MTFDDAK3T8TDS | 3.8 TB | 29      | 265   | 2     | 0.59   |
| Micron    | MTFDDAV240TDU      | 240 GB | 6       | 199   | 0     | 0.55   |
| Micron    | MTFDDAK960TDT      | 960 GB | 8       | 196   | 0     | 0.54   |
| Micron    | 5300_MTFDDAK480TDT | 480 GB | 4       | 194   | 0     | 0.53   |
| Micron    | 5300_MTFDDAK480TDS | 480 GB | 13      | 194   | 0     | 0.53   |
| Micron    | MTFDDAK960TDS      | 960 GB | 16      | 160   | 0     | 0.44   |
| Micron    | 5300_MTFDDAK960TDT | 960 GB | 36      | 151   | 0     | 0.41   |
| Micron    | 5210_MTFDDAK3T8QDE | 3.8 TB | 67      | 148   | 3     | 0.38   |
| Micron    | 1300_MTFDDAK512TDL | 512 GB | 116     | 138   | 0     | 0.38   |
| Micron    | 5300_MTFDDAK1T9TDS | 1.9 TB | 48      | 118   | 0     | 0.33   |
| Micron    | MTFDDAK1T9TDD      | 1.9 TB | 3       | 116   | 0     | 0.32   |
| Micron    | MTFDDAK960TDD      | 960 GB | 14      | 99    | 0     | 0.27   |
| Micron    | MTFDDAK960TDS-1... | 960 GB | 2       | 95    | 0     | 0.26   |
| Micron    | 5300_MTFDDAK3T8TDT | 3.8 TB | 16      | 122   | 3     | 0.26   |
| Micron    | MTFDDAV256TDL      | 256 GB | 5       | 143   | 2     | 0.23   |
| Micron    | MTFDDAK1T0TDL      | 1 TB   | 21      | 70    | 1     | 0.19   |
| Micron    | MTFDDAK480TDT      | 480 GB | 10      | 66    | 0     | 0.18   |
| Micron    | C400-MTFDDAC512MAM | 512 GB | 4       | 679   | 802   | 0.17   |
| Micron    | MTFDDAK512TBN      | 512 GB | 3       | 58    | 0     | 0.16   |
| Micron    | 5300_MTFDDAK240TDT | 240 GB | 7       | 7     | 0     | 0.02   |
| Micron    | M500_MTFDDAK960MAV | 960 GB | 1       | 2162  | 1032  | 0.01   |
| Micron    | MTFDDAK3T8TDS-1... | 3.8 TB | 2       | 1     | 0     | 0.00   |

SSD by Family
-------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days — avg. days per sample,
Err  — avg. errors per sample,
MTBF — avg. MTBF in years per sample.

| MFG       | Family                 | Models | Samples | Days  | Err   | MTBF   |
|-----------|------------------------|--------|---------|-------|-------|--------|
| Micron    | RealSSD m4/C400/P400   | 6      | 18      | 1891  | 179   | 4.53   |
| Micron    | BX/MX1/2/3/500, M5/... | 5      | 73      | 906   | 42    | 1.78   |
| Micron    | 5100 Pro / 5200 SSDs   | 19     | 592     | 697   | 10    | 1.53   |
| Micron    | BX/MX1/2/3/500, M5/... | 5      | 68      | 674   | 29    | 1.52   |
| Micron    | MX1/2/300, M5/600, ... | 3      | 20      | 557   | 1     | 1.46   |
| Micron    | 5100 Pro / 5200 / 5... | 13     | 420     | 572   | 5     | 1.24   |
| Micron    | Unknown                | 21     | 484     | 252   | 2     | 0.67   |
| Micron    | Client SSDs            | 3      | 39      | 231   | 1     | 0.61   |
