Kingston Solid State Drives
===========================

This is a list of all tested Kingston solid state drive models and their MTBFs. See
more info on reliability test in the [README](https://github.com/linuxhw/EnterpriseDrive).

Contents
--------

1. [ SSD by Model  ](#ssd-by-model)
2. [ SSD by Family ](#ssd-by-family)

SSD by Model
------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days — avg. days per sample,
Err  — avg. errors per sample,
MTBF — avg. MTBF in years per sample.

| MFG       | Model              | Size   | Samples | Days  | Err   | MTBF   |
|-----------|--------------------|--------|---------|-------|-------|--------|
| Kingston  | SVP100S264G        | 64 GB  | 1       | 3150  | 0     | 8.63   |
| Kingston  | SH103S3480G        | 480 GB | 1       | 1943  | 0     | 5.32   |
| Kingston  | SE50S3480G         | 480 GB | 5       | 1933  | 0     | 5.30   |
| Kingston  | SUV300S37A120G     | 120 GB | 1       | 1816  | 0     | 4.98   |
| Kingston  | SKC300S37A120G     | 120 GB | 2       | 1656  | 0     | 4.54   |
| Kingston  | SH103S3240G        | 240 GB | 2       | 2144  | 1     | 4.41   |
| Kingston  | SVP200S37A120G     | 120 GB | 1       | 1522  | 0     | 4.17   |
| Kingston  | SE50S37240G        | 240 GB | 1       | 1450  | 0     | 3.97   |
| Kingston  | SH103S3120G        | 120 GB | 10      | 1568  | 1     | 3.95   |
| Kingston  | SKC300S37A240G     | 240 GB | 12      | 1436  | 0     | 3.93   |
| Kingston  | SV300S37A240G      | 240 GB | 101     | 1601  | 9     | 3.78   |
| Kingston  | SE50S37100G        | 100 GB | 1       | 1285  | 0     | 3.52   |
| Kingston  | SKC400S37128G      | 128 GB | 4       | 1280  | 0     | 3.51   |
| Kingston  | SE50S3100G         | 100 GB | 3       | 1552  | 1     | 3.43   |
| Kingston  | SUV300S37A240G     | 240 GB | 2       | 1245  | 0     | 3.41   |
| Kingston  | SKC400S37512G      | 512 GB | 5       | 1022  | 0     | 2.80   |
| Kingston  | SV300S37A120G      | 120 GB | 38      | 1460  | 9     | 2.70   |
| Kingston  | SHFS37A480G        | 480 GB | 3       | 1378  | 3     | 2.69   |
| Kingston  | SKC400S371T        | 1 TB   | 10      | 945   | 0     | 2.59   |
| Kingston  | SKC400S37256G      | 256 GB | 1       | 902   | 0     | 2.47   |
| Kingston  | SEDC400S37960G     | 960 GB | 9       | 791   | 0     | 2.17   |
| Kingston  | SV300S37A480G      | 480 GB | 11      | 1097  | 39    | 2.01   |
| Kingston  | SUV500240G         | 240 GB | 4       | 678   | 0     | 1.86   |
| Kingston  | SHSS37A240G        | 240 GB | 3       | 661   | 0     | 1.81   |
| Kingston  | SUV400S37120G      | 120 GB | 12      | 662   | 3     | 1.73   |
| Kingston  | SA400M8120G        | 120 GB | 3       | 616   | 0     | 1.69   |
| Kingston  | SUV500M8480G       | 480 GB | 1       | 608   | 0     | 1.67   |
| Kingston  | SM2280S3G2120G     | 120 GB | 1       | 592   | 0     | 1.62   |
| Kingston  | SV300S37A60G       | 64 GB  | 3       | 1344  | 2     | 1.58   |
| Kingston  | SUV400S37240G      | 240 GB | 53      | 563   | 1     | 1.50   |
| Kingston  | SUV500960G         | 960 GB | 8       | 518   | 0     | 1.42   |
| Kingston  | SA400S37120G       | 120 GB | 42      | 522   | 1     | 1.35   |
| Kingston  | SEDC500M960G       | 960 GB | 14      | 476   | 1     | 1.25   |
| Kingston  | SA400S37480G       | 480 GB | 32      | 704   | 12    | 1.24   |
| Kingston  | SEDC500R960G       | 960 GB | 1       | 418   | 0     | 1.15   |
| Kingston  | SVP200S3120G       | 120 GB | 1       | 341   | 0     | 0.94   |
| Kingston  | SQ500S37480G       | 480 GB | 1       | 297   | 0     | 0.81   |
| Kingston  | SA400S37960G       | 960 GB | 8       | 286   | 0     | 0.78   |
| Kingston  | SEDC500R1920G      | 1.9 TB | 45      | 373   | 1     | 0.75   |
| Kingston  | SEDC500R480G       | 480 GB | 9       | 259   | 0     | 0.71   |
| Kingston  | SEDC500M1920G      | 1.9 TB | 36      | 280   | 1     | 0.70   |
| Kingston  | SHFS37A240G        | 240 GB | 1       | 248   | 0     | 0.68   |
| Kingston  | SEDC500M480G       | 480 GB | 10      | 255   | 1     | 0.64   |
| Kingston  | SA400S37240G       | 240 GB | 66      | 233   | 1     | 0.64   |
| Kingston  | SEDC450R480G       | 480 GB | 1       | 182   | 0     | 0.50   |
| Kingston  | SEDC400S37480G     | 480 GB | 1       | 166   | 0     | 0.46   |
| Kingston  | SEDC500R3840G      | 3.8 TB | 36      | 173   | 1     | 0.38   |
| Kingston  | SKC6001024G        | 1 TB   | 2       | 136   | 0     | 0.37   |
| Kingston  | SUV500M8240G       | 240 GB | 3       | 108   | 0     | 0.30   |
| Kingston  | SEDC500M3840G      | 3.8 TB | 6       | 89    | 1     | 0.20   |
| Kingston  | SKC6002048G        | 2 TB   | 1       | 117   | 1     | 0.16   |
| Kingston  | SKC300S37A480G     | 480 GB | 1       | 27    | 0     | 0.08   |
| Kingston  | SKC600256G         | 256 GB | 27      | 10    | 0     | 0.03   |
| Kingston  | SEDC450R1920G      | 1.9 TB | 4       | 8     | 0     | 0.02   |
| Kingston  | SHPM2280P2H-480G   | 480 GB | 3       | 1138  | 743   | 0.01   |
| Kingston  | SHPM2280P2H-240G   | 240 GB | 1       | 1297  | 1022  | 0.00   |

SSD by Family
-------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days — avg. days per sample,
Err  — avg. errors per sample,
MTBF — avg. MTBF in years per sample.

| MFG       | Family                 | Models | Samples | Days  | Err   | MTBF   |
|-----------|------------------------|--------|---------|-------|-------|--------|
| Kingston  | JMicron based SSDs     | 1      | 1       | 3150  | 0     | 8.63   |
| Kingston  | SandForce Driven SSDs  | 18     | 197     | 1518  | 9     | 3.45   |
| Kingston  | SSDNow UV400           | 2      | 65      | 581   | 1     | 1.54   |
| Kingston  | SSDNow UV400/500       | 4      | 16      | 487   | 0     | 1.33   |
| Kingston  | Phison Driven SSDs     | 24     | 349     | 412   | 2     | 1.00   |
| Kingston  | Unknown                | 4      | 6       | 933   | 542   | 0.41   |
| Kingston  | Silicon Motion base... | 3      | 30      | 22    | 1     | 0.06   |
