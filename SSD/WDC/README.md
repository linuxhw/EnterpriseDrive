WDC Solid State Drives
======================

This is a list of all tested WDC solid state drive models and their MTBFs. See
more info on reliability test in the [README](https://github.com/linuxhw/EnterpriseDrive).

Contents
--------

1. [ SSD by Model  ](#ssd-by-model)
2. [ SSD by Family ](#ssd-by-family)

SSD by Model
------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days — avg. days per sample,
Err  — avg. errors per sample,
MTBF — avg. MTBF in years per sample.

| MFG       | Model              | Size   | Samples | Days  | Err   | MTBF   |
|-----------|--------------------|--------|---------|-------|-------|--------|
| WDC       | WDS250G1B0A-00H9H0 | 250 GB | 1       | 1181  | 0     | 3.24   |
| WDC       | WDS100T1B0A-00H9H0 | 1 TB   | 27      | 898   | 0     | 2.46   |
| WDC       | WDS500G1B0A-00H9H0 | 500 GB | 2       | 881   | 0     | 2.41   |
| WDC       | WDS240G1G0A-00SS50 | 240 GB | 10      | 790   | 0     | 2.17   |
| WDC       | WDS100T2B0A        | 1 TB   | 9       | 730   | 0     | 2.00   |
| WDC       | HBS3A1996A7E6B1    | 960 GB | 2       | 577   | 0     | 1.58   |
| WDC       | WDS200T2B0A        | 2 TB   | 9       | 575   | 0     | 1.58   |
| WDC       | WDS120G1G0A-00SS50 | 120 GB | 7       | 569   | 0     | 1.56   |
| WDC       | WDS250G2B0A-00SM50 | 250 GB | 23      | 539   | 0     | 1.48   |
| WDC       | WDS100T2G0A-00JH30 | 1 TB   | 1       | 440   | 0     | 1.21   |
| WDC       | WDS480G2G0B-00EPW0 | 480 GB | 6       | 589   | 9     | 1.20   |
| WDC       | WDS100T2B0A-00SM50 | 1 TB   | 114     | 493   | 3     | 1.14   |
| WDC       | WDS500G2B0A-00SM50 | 500 GB | 4       | 404   | 0     | 1.11   |
| WDC       | WDS240G2G0B-00EPW0 | 240 GB | 1       | 402   | 0     | 1.10   |
| WDC       | WDS200T2B0A-00SM50 | 2 TB   | 33      | 403   | 1     | 1.08   |
| WDC       | WDS240G2G0A-00JH30 | 240 GB | 21      | 394   | 1     | 1.01   |
| WDC       | WDS400T2B0A-00SM50 | 4 TB   | 4       | 478   | 1     | 0.98   |
| WDC       | WDS200T1R0A-68A4W0 | 2 TB   | 15      | 209   | 0     | 0.58   |
| WDC       | WDS250G2B0A        | 250 GB | 2       | 176   | 0     | 0.48   |
| WDC       | WDS250G2B0B-00YS70 | 250 GB | 1       | 147   | 0     | 0.40   |
| WDC       | WDS100T1R0A-68A4W0 | 1 TB   | 3       | 120   | 0     | 0.33   |
| WDC       | WDS400T1R0A-68A4W0 | 4 TB   | 3       | 110   | 0     | 0.30   |
| WDC       | WDS250G2B0B        | 250 GB | 2       | 108   | 0     | 0.30   |
| WDC       | WDS100T2B0B-00YS70 | 1 TB   | 1       | 84    | 0     | 0.23   |
| WDC       | WDS500G1R0A-68A4W0 | 500 GB | 1       | 49    | 0     | 0.13   |

SSD by Family
-------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days — avg. days per sample,
Err  — avg. errors per sample,
MTBF — avg. MTBF in years per sample.

| MFG       | Family                 | Models | Samples | Days  | Err   | MTBF   |
|-----------|------------------------|--------|---------|-------|-------|--------|
| WDC       | Blue and Green SSDs    | 9      | 52      | 768   | 0     | 2.11   |
| WDC       | Blue / Red / Green ... | 13     | 241     | 462   | 2     | 1.15   |
| WDC       | Unknown                | 3      | 9       | 377   | 1     | 0.89   |
