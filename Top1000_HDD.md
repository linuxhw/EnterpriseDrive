Appendix 3: Top 1000 HDD Models
===============================

See more info on reliability test in the [README](https://github.com/linuxhw/EnterpriseDrive).

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days — avg. days per sample,
Err  — avg. errors per sample,
MTBF — avg. MTBF in years per sample.

| MFG       | Model              | Size   | Samples | Days  | Err   | MTBF   |
|-----------|--------------------|--------|---------|-------|-------|--------|
| WDC       | WD740ADFD-00NLR5   | 74 GB  | 1       | 4658  | 0     | 12.76  |
| Hitachi   | HDS725050KLA360    | 500 GB | 1       | 4456  | 0     | 12.21  |
| WDC       | WD5000AAKS-00A7B2  | 500 GB | 1       | 3899  | 0     | 10.68  |
| Hitachi   | HDS721010KLA33R... | 1 TB   | 1       | 3863  | 0     | 10.58  |
| WDC       | WD1002FBYS-01A6B0  | 1 TB   | 2       | 3719  | 0     | 10.19  |
| Seagate   | ST32500NSSUN250G   | 250 GB | 2       | 3676  | 0     | 10.07  |
| Maxtor    | STM3250310AS       | 250 GB | 2       | 3651  | 0     | 10.00  |
| WDC       | WD2502ABYS-01B7A0  | 256 GB | 3       | 3596  | 0     | 9.85   |
| Hitachi   | HUA7210SASUN1.0T   | 1 TB   | 16      | 3909  | 1     | 9.25   |
| Hitachi   | HUA722050CLA330    | 500 GB | 2       | 3243  | 0     | 8.89   |
| WDC       | WD1001FALS-00Y6A0  | 1 TB   | 3       | 3147  | 0     | 8.62   |
| Seagate   | ST3320620AS        | 320 GB | 3       | 4439  | 5     | 8.48   |
| HP        | GB0500EAFYL        | 500 GB | 2       | 2973  | 0     | 8.15   |
| WDC       | WD5000AAKS-00V6A0  | 500 GB | 1       | 2965  | 0     | 8.12   |
| WDC       | WD10EACS-00ZJB0    | 1 TB   | 1       | 2955  | 0     | 8.10   |
| Hitachi   | HDS721010KLA330    | 1 TB   | 16      | 3343  | 16    | 8.08   |
| WDC       | WD1500HLFS-01G6U4  | 150 GB | 4       | 2929  | 0     | 8.03   |
| WDC       | WD1002FBYS-05A6B0  | 1 TB   | 8       | 3346  | 1     | 7.99   |
| Hitachi   | HUA722020ALA330... | 2 TB   | 1       | 2886  | 0     | 7.91   |
| Samsung   | HD250HJ            | 250 GB | 1       | 2882  | 0     | 7.90   |
| Seagate   | ST32000645NS       | 2 TB   | 88      | 3111  | 15    | 7.89   |
| WDC       | WD10EZRX-00A8LB0   | 1 TB   | 1       | 2878  | 0     | 7.89   |
| Hitachi   | HDS721050CLA360    | 500 GB | 2       | 2828  | 0     | 7.75   |
| WDC       | WD5000AAKS-00E4A0  | 500 GB | 1       | 2827  | 0     | 7.75   |
| Hitachi   | HDS723020BLA642    | 2 TB   | 81      | 3052  | 1     | 7.75   |
| Hitachi   | HDS723030ALA640    | 3 TB   | 8       | 3041  | 22    | 7.73   |
| WDC       | WD10EARS-00MVWB0   | 1 TB   | 1       | 2778  | 0     | 7.61   |
| Seagate   | ST3320620A         | 320 GB | 2       | 2728  | 0     | 7.47   |
| WDC       | WD1002FBYS-50A6B1  | 1 TB   | 1       | 2722  | 0     | 7.46   |
| Toshiba   | DT01ABA300         | 3 TB   | 1       | 2720  | 0     | 7.45   |
| Hitachi   | HUA723020ALA640... | 2 TB   | 1       | 2643  | 0     | 7.24   |
| WDC       | WD1600JD-75HBB0    | 160 GB | 1       | 2640  | 0     | 7.23   |
| Hitachi   | HUA721010KLA330    | 1 TB   | 23      | 2692  | 1     | 7.17   |
| Hitachi   | HUA722010CLA330... | 1 TB   | 1       | 2591  | 0     | 7.10   |
| Hitachi   | HDS723020BLE640    | 2 TB   | 6       | 2982  | 1     | 7.07   |
| WDC       | WD2500AAJS-22L7A0  | 250 GB | 1       | 2570  | 0     | 7.04   |
| WDC       | WD10EALX-759BA1    | 1 TB   | 1       | 2562  | 0     | 7.02   |
| Samsung   | HD753LJ            | 752 GB | 1       | 2559  | 0     | 7.01   |
| WDC       | WD5003ABYX-01WERA2 | 500 GB | 2       | 2547  | 0     | 6.98   |
| Seagate   | ST3250620NS        | 250 GB | 4       | 2532  | 0     | 6.94   |
| HP        | GB0250EAFYK        | 250 GB | 2       | 2531  | 0     | 6.93   |
| WDC       | WD3200AAKS-61L9A0  | 320 GB | 1       | 2526  | 0     | 6.92   |
| WDC       | WD10EADS-00L5B1    | 1 TB   | 1       | 2526  | 0     | 6.92   |
| HP        | GB0500C4413        | 500 GB | 2       | 3306  | 1     | 6.90   |
| WDC       | WD1002FBYS-02A6B0  | 1 TB   | 16      | 2827  | 2     | 6.88   |
| Seagate   | ST2000VX000-1CU164 | 2 TB   | 1       | 2477  | 0     | 6.79   |
| Hitachi   | HDS722020ALA330    | 2 TB   | 34      | 2875  | 62    | 6.77   |
| WDC       | WD5000AACS-00G8B1  | 500 GB | 2       | 4300  | 3     | 6.73   |
| WDC       | WD6400AAKS-41H2B0  | 640 GB | 2       | 2433  | 0     | 6.67   |
| WDC       | WD7500AYYS-01RCA0  | 752 GB | 5       | 2785  | 1     | 6.66   |
| Seagate   | ST32000646NS       | 2 TB   | 2       | 2424  | 0     | 6.64   |
| WDC       | WD1500HLFS-01G6U0  | 150 GB | 1       | 2403  | 0     | 6.59   |
| Seagate   | ST3250410AS        | 250 GB | 4       | 3449  | 85    | 6.55   |
| WDC       | WD7501AALS-00J7B0  | 752 GB | 2       | 2359  | 0     | 6.46   |
| HP        | MB1000GCWCV        | 1 TB   | 3       | 2355  | 0     | 6.45   |
| Hitachi   | HDS721010CLA330    | 1 TB   | 1       | 2342  | 0     | 6.42   |
| WDC       | WD10EZEX-75ZF5A0   | 1 TB   | 1       | 2335  | 0     | 6.40   |
| HGST      | HUS724020ALE640    | 2 TB   | 1       | 2280  | 0     | 6.25   |
| WDC       | WD5003AZEX-00K1GA0 | 500 GB | 29      | 2406  | 1     | 6.22   |
| Seagate   | ST4000VN000-1H4168 | 4 TB   | 30      | 2342  | 1     | 6.17   |
| WDC       | WD7502AAEX-00Y9A0  | 752 GB | 4       | 2242  | 0     | 6.14   |
| HP        | GB1000EAMYC        | 1 TB   | 1       | 2228  | 0     | 6.11   |
| Seagate   | ST1000NC001-1DY162 | 1 TB   | 3       | 2222  | 0     | 6.09   |
| WDC       | WD1002FBYS-18W8B0  | 1 TB   | 10      | 2883  | 9     | 6.05   |
| WDC       | WD30EZRX-00SPEB0   | 3 TB   | 6       | 2569  | 2     | 5.96   |
| WDC       | WD1001FALS-00E8B0  | 1 TB   | 1       | 2171  | 0     | 5.95   |
| WDC       | WD5002AALX-00J37A0 | 500 GB | 11      | 2849  | 3     | 5.89   |
| HGST      | HDS724040ALE640    | 4 TB   | 6       | 2140  | 0     | 5.86   |
| Seagate   | ST3000VX000-1CU166 | 3 TB   | 26      | 2459  | 4     | 5.82   |
| Seagate   | ST3320620NS        | 320 GB | 4       | 2189  | 1     | 5.81   |
| Hitachi   | HUA723030ALA640    | 3 TB   | 32      | 2203  | 1     | 5.78   |
| Seagate   | ST2000NC000-1CX164 | 2 TB   | 1       | 2101  | 0     | 5.76   |
| Hitachi   | HDP725050GLA360    | 500 GB | 3       | 2095  | 0     | 5.74   |
| Seagate   | ST3000VX006-1HH166 | 3 TB   | 2       | 2069  | 0     | 5.67   |
| Seagate   | ST2000VN000-1HJ164 | 2 TB   | 2       | 2067  | 0     | 5.67   |
| Seagate   | ST3400620AS        | 400 GB | 1       | 2060  | 0     | 5.65   |
| Toshiba   | MD04ACA500         | 5 TB   | 2       | 2046  | 0     | 5.61   |
| Seagate   | ST2000VX000-1ES164 | 2 TB   | 1       | 2043  | 0     | 5.60   |
| WDC       | WD1600AAJS-08L7A0  | 160 GB | 1       | 2027  | 0     | 5.56   |
| WDC       | WD6001FSYZ-01SS7B0 | 6 TB   | 3       | 1989  | 0     | 5.45   |
| Seagate   | ST2000NM0033       | 2 TB   | 1       | 1988  | 0     | 5.45   |
| WDC       | WD20EARX-00PASB0   | 2 TB   | 4       | 1958  | 0     | 5.37   |
| Maxtor    | 6V160E0            | 160 GB | 2       | 1932  | 0     | 5.29   |
| Seagate   | ST500DM002-1BC142  | 500 GB | 2       | 1929  | 0     | 5.29   |
| WDC       | WD2004FBYZ-01YCBB0 | 2 TB   | 5       | 1902  | 0     | 5.21   |
| WDC       | WD2001FFSX-68JNUN0 | 2 TB   | 2       | 1899  | 0     | 5.20   |
| WDC       | WD30EZRX-00DC0B0   | 3 TB   | 8       | 2103  | 11    | 5.19   |
| Seagate   | ST6000DM001-1XY17Z | 6 TB   | 4       | 1888  | 0     | 5.17   |
| WDC       | WD10EZEX-22RKKA0   | 1 TB   | 4       | 2248  | 1     | 5.15   |
| HP        | MB1000GDUNU        | 1 TB   | 1       | 1877  | 0     | 5.14   |
| Seagate   | ST4000VN0001-1S... | 4 TB   | 9       | 2087  | 1     | 5.14   |
| Seagate   | ST91000640NS       | 1 TB   | 33      | 1990  | 1     | 5.14   |
| Seagate   | ST4000NM0033-9Z... | 4 TB   | 437     | 2259  | 106   | 5.09   |
| HGST      | HUS724020ALA640    | 2 TB   | 205     | 1921  | 10    | 5.08   |
| WDC       | WD30EFRX-68AX9N0   | 3 TB   | 17      | 2335  | 2     | 5.01   |
| WDC       | WD1002FAEX-007BA0  | 1 TB   | 2       | 1825  | 0     | 5.00   |
| WDC       | WD2500AAJS-00B4A0  | 250 GB | 1       | 1814  | 0     | 4.97   |
| WDC       | WD20EFRX-68AX9N0   | 2 TB   | 4       | 2789  | 4     | 4.96   |
| WDC       | WD2002FYPS-01U1B1  | 2 TB   | 17      | 2794  | 201   | 4.96   |
| Hitachi   | HUA723020ALA640    | 2 TB   | 22      | 1885  | 7     | 4.96   |
| HGST      | HUS726060ALE614    | 6 TB   | 147     | 1949  | 11    | 4.94   |
| Hitachi   | HDT725032VLA360    | 320 GB | 5       | 3607  | 7     | 4.88   |
| WDC       | WD1002FAEX-00Z3A0  | 1 TB   | 33      | 2086  | 42    | 4.87   |
| WDC       | WD2502ABYS-18B7A0  | 250 GB | 2       | 1777  | 0     | 4.87   |
| HP        | GB0160EAFJE        | 160 GB | 2       | 2903  | 513   | 4.82   |
| WDC       | WD5000BHTZ-04JCPV0 | 500 GB | 1       | 1758  | 0     | 4.82   |
| Toshiba   | MK0502TSKB         | 500 GB | 1       | 1752  | 0     | 4.80   |
| Toshiba   | MK1002TSKB         | 1 TB   | 10      | 1749  | 0     | 4.79   |
| WDC       | WD20EADS-00R6B0    | 2 TB   | 2       | 3212  | 6     | 4.77   |
| WDC       | WD20EZRX-00DC0B0   | 2 TB   | 7       | 1733  | 0     | 4.75   |
| Seagate   | ST3500630AS        | 500 GB | 1       | 1720  | 0     | 4.71   |
| WDC       | WD5003ABYZ-011FA0  | 500 GB | 15      | 1710  | 0     | 4.69   |
| WDC       | WD4001FFSX-68JNUN0 | 4 TB   | 12      | 2048  | 1     | 4.67   |
| Seagate   | ST3000DM001-1CH166 | 3 TB   | 20      | 1939  | 20    | 4.67   |
| WDC       | WD20EARS-00MVWB0   | 2 TB   | 7       | 2089  | 1     | 4.66   |
| Samsung   | HD103UJ            | 1 TB   | 26      | 2880  | 144   | 4.60   |
| WDC       | WD2002FAEX-007BA0  | 2 TB   | 59      | 1989  | 42    | 4.60   |
| HGST      | HMS5C4040BLE640    | 4 TB   | 17      | 1760  | 15    | 4.59   |
| Toshiba   | HDWE160            | 6 TB   | 74      | 1747  | 1     | 4.56   |
| WDC       | WD1002FBYS-18W8B1  | 1 TB   | 7       | 2404  | 1     | 4.53   |
| Seagate   | ST500NM0011 00W... | 500 GB | 2       | 1647  | 0     | 4.51   |
| WDC       | WD5003ABYX-23 0... | 500 GB | 2       | 1645  | 0     | 4.51   |
| Seagate   | ST2000NM0024-1H... | 2 TB   | 9       | 1645  | 0     | 4.51   |
| WDC       | WD5003AZEX-00S3DA0 | 500 GB | 16      | 1644  | 0     | 4.51   |
| Seagate   | ST3500630NS        | 500 GB | 9       | 1643  | 0     | 4.50   |
| WDC       | WD1000DHTZ-04N21V1 | 1 TB   | 6       | 2294  | 2     | 4.47   |
| WDC       | WD10EZEX-22BN5A0   | 1 TB   | 1       | 1630  | 0     | 4.47   |
| Hitachi   | HUA722010ALA330    | 1 TB   | 5       | 2547  | 5     | 4.45   |
| HGST      | HDN724040ALE640    | 4 TB   | 22      | 1721  | 92    | 4.44   |
| Seagate   | ST33000651AS       | 3 TB   | 4       | 2329  | 10    | 4.44   |
| WDC       | WD6402AAEX-00Y9A0  | 640 GB | 3       | 2411  | 677   | 4.43   |
| Hitachi   | HDT721050SLA360    | 500 GB | 1       | 1617  | 0     | 4.43   |
| Seagate   | ST9500620NS        | 500 GB | 27      | 1811  | 1     | 4.41   |
| WDC       | WD20EARX-00MMMB0   | 2 TB   | 2       | 2666  | 2     | 4.39   |
| Seagate   | ST6000NM0024-1H... | 6 TB   | 1675    | 1889  | 18    | 4.38   |
| WDC       | WD5000AAKX-003CA0  | 500 GB | 3       | 2165  | 3     | 4.36   |
| Seagate   | ST3000NM0033-9Z... | 3 TB   | 23      | 2185  | 67    | 4.34   |
| Seagate   | ST3000VX000-1ES166 | 3 TB   | 3       | 1583  | 0     | 4.34   |
| WDC       | WD4002FFWX-68TZ4N0 | 4 TB   | 4       | 1581  | 0     | 4.33   |
| Hitachi   | HUA722010CLA330    | 1 TB   | 55      | 2120  | 25    | 4.29   |
| WDC       | WD7500BPKX-00HPJT0 | 752 GB | 16      | 1885  | 2     | 4.28   |
| WDC       | WD1500HLHX-01JJPV0 | 150 GB | 1       | 1552  | 0     | 4.25   |
| WDC       | WD1000CHTZ-04JCPV1 | 1 TB   | 4       | 1542  | 0     | 4.22   |
| Seagate   | ST3000NC002-1DY166 | 3 TB   | 1       | 1518  | 0     | 4.16   |
| Seagate   | ST1000DM003-1ER162 | 1 TB   | 30      | 1508  | 0     | 4.13   |
| HGST      | HUS726030ALA610    | 3 TB   | 47      | 1529  | 1     | 4.13   |
| Hitachi   | HUA723020ALA641    | 2 TB   | 13      | 1504  | 0     | 4.12   |
| WDC       | WD10EZEX-00RKKA0   | 1 TB   | 7       | 1796  | 16    | 4.12   |
| WDC       | WD6002FRYZ-01WD5B0 | 6 TB   | 56      | 1578  | 6     | 4.10   |
| Seagate   | ST6000VX0001-1S... | 6 TB   | 2       | 1493  | 0     | 4.09   |
| WDC       | WD5000AADS-00S9B0  | 500 GB | 4       | 2749  | 4     | 4.09   |
| Seagate   | ST2000DM001-1ER164 | 2 TB   | 29      | 1695  | 211   | 4.09   |
| WDC       | WD30EZRX-00D8PB0   | 3 TB   | 15      | 1779  | 2     | 4.08   |
| WDC       | WD2003FYYS-05T8B0  | 2 TB   | 2       | 1487  | 0     | 4.08   |
| Seagate   | ST1000VX000-1CU162 | 1 TB   | 3       | 1485  | 0     | 4.07   |
| WDC       | WD10EALX-089BA0    | 1 TB   | 7       | 1879  | 2     | 4.06   |
| Seagate   | ST320DM000-1BC14C  | 320 GB | 2       | 1479  | 0     | 4.05   |
| WDC       | WD2500HHTZ-04N21V0 | 250 GB | 3       | 1473  | 0     | 4.04   |
| Seagate   | ST1000NM0033-9Z... | 1 TB   | 136     | 1741  | 83    | 4.03   |
| WDC       | WD2003FYYS-02W0B1  | 2 TB   | 36      | 1951  | 50    | 4.03   |
| WDC       | WD5000HHTZ-04N21V0 | 500 GB | 1       | 1468  | 0     | 4.02   |
| WDC       | WD800JD-60LUA0     | 80 GB  | 1       | 1465  | 0     | 4.01   |
| HP        | MB1000GCEHH        | 1 TB   | 4       | 2908  | 578   | 4.00   |
| Hitachi   | HUA722020ALA330    | 2 TB   | 6       | 1954  | 1     | 4.00   |
| WDC       | WD2003FYPS-02W0B1  | 2 TB   | 2       | 1454  | 0     | 3.99   |
| Seagate   | ST33000650NS       | 3 TB   | 19      | 2306  | 68    | 3.97   |
| WDC       | WD5000AZLX-07K2TA0 | 500 GB | 1       | 1444  | 0     | 3.96   |
| HGST      | HUS724040ALA640    | 4 TB   | 139     | 1524  | 2     | 3.95   |
| Hitachi   | HUA722020ALA331    | 2 TB   | 10      | 2351  | 203   | 3.93   |
| Hitachi   | HDS721010CLA332    | 1 TB   | 19      | 2820  | 396   | 3.93   |
| WDC       | WD4003FZEX-00Z4SA0 | 4 TB   | 14      | 1431  | 0     | 3.92   |
| Toshiba   | DT01ACA300         | 3 TB   | 77      | 1717  | 21    | 3.92   |
| WDC       | WD2000FYYZ-01UL1B3 | 2 TB   | 1       | 1427  | 0     | 3.91   |
| Samsung   | SP0812C            | 80 GB  | 1       | 1426  | 0     | 3.91   |
| WDC       | WD5002AALX-32Z3A0  | 500 GB | 7       | 1425  | 0     | 3.91   |
| WDC       | WD5000AAKX-75U6AA0 | 500 GB | 4       | 1417  | 0     | 3.88   |
| HGST      | HTE541010A9E680    | 1 TB   | 6       | 1414  | 0     | 3.87   |
| WDC       | WD20EADS-65R6B1    | 2 TB   | 1       | 2788  | 1     | 3.82   |
| WDC       | WD40PURX-64NZ6Y0   | 4 TB   | 4       | 1394  | 0     | 3.82   |
| HGST      | HUS724030ALA640    | 3 TB   | 37      | 1636  | 71    | 3.82   |
| HGST      | HUS724040ALE640    | 4 TB   | 22      | 1390  | 0     | 3.81   |
| Seagate   | ST1000DM003-1CH162 | 1 TB   | 17      | 1519  | 219   | 3.81   |
| WDC       | WD6002FFWX-68TZ4N0 | 6 TB   | 8       | 1609  | 44    | 3.80   |
| WDC       | WD101KRYZ-01JPDB0  | 10 TB  | 1       | 1384  | 0     | 3.79   |
| HGST      | HUH728080ALE600    | 8 TB   | 20      | 1382  | 0     | 3.79   |
| WDC       | WD5000BMVW-11AJGS2 | 500 GB | 2       | 1375  | 0     | 3.77   |
| Seagate   | ST9250610NS        | 250 GB | 8       | 1375  | 0     | 3.77   |
| WDC       | WD5000AUDX-61WNHY0 | 500 GB | 1       | 1373  | 0     | 3.76   |
| WDC       | WD1002FBYS-18A6B0  | 1 TB   | 7       | 2160  | 401   | 3.75   |
| WDC       | WD5000AAKX-60U6AA0 | 500 GB | 4       | 1494  | 1     | 3.75   |
| WDC       | WD5001AALS-00E3A0  | 500 GB | 6       | 2120  | 3     | 3.74   |
| Toshiba   | MG04ACA600E        | 6 TB   | 766     | 1435  | 15    | 3.73   |
| WDC       | WD4000FYYZ-03UL1B3 | 4 TB   | 2       | 1360  | 0     | 3.73   |
| WDC       | WD1600AAJS-00YZCA0 | 160 GB | 1       | 1356  | 0     | 3.72   |
| WDC       | WD5000AZLX-00CL5A0 | 500 GB | 2       | 1353  | 0     | 3.71   |
| WDC       | WD100PURZ-85W86Y0  | 10 TB  | 1       | 1347  | 0     | 3.69   |
| WDC       | WD1003FBYX-50Y7B1  | 1 TB   | 3       | 1335  | 0     | 3.66   |
| WDC       | WD1003FBYX-01Y7B1  | 1 TB   | 123     | 1599  | 7     | 3.64   |
| WDC       | WD5000AAKX-08ERMA0 | 500 GB | 3       | 1328  | 0     | 3.64   |
| WDC       | WD40EFRX-68WT0N0   | 4 TB   | 288     | 2022  | 17    | 3.63   |
| WDC       | WD1002FAEX-00Y9A0  | 1 TB   | 42      | 1495  | 8     | 3.63   |
| WDC       | WD10EZRX-00L4HB0   | 1 TB   | 6       | 1320  | 0     | 3.62   |
| Seagate   | ST4000NM0024-1H... | 4 TB   | 18      | 1585  | 88    | 3.61   |
| Seagate   | ST1000VX000-1ES162 | 1 TB   | 2       | 1315  | 0     | 3.60   |
| HGST      | HDN724030ALE640    | 3 TB   | 7       | 1299  | 0     | 3.56   |
| Hitachi   | HDS721075KLA330    | 752 GB | 1       | 3894  | 2     | 3.56   |
| WDC       | WD2000FYYZ-03UL1B2 | 2 TB   | 1       | 1292  | 0     | 3.54   |
| HGST      | HDN726060ALE614    | 6 TB   | 2       | 1289  | 0     | 3.53   |
| HGST      | HDN726040ALE614    | 4 TB   | 23      | 1287  | 0     | 3.53   |
| WDC       | WD1001FALS-00J7B0  | 1 TB   | 2       | 2317  | 4     | 3.53   |
| Seagate   | ST2000NM0033-9Z... | 2 TB   | 95      | 1621  | 104   | 3.52   |
| Seagate   | ST6000VN0041-2E... | 6 TB   | 10      | 1374  | 1     | 3.50   |
| WDC       | WD5003ABYX-01WERA0 | 500 GB | 20      | 1598  | 2     | 3.50   |
| Toshiba   | MG04ACA200EY       | 2 TB   | 7       | 1270  | 0     | 3.48   |
| Toshiba   | DT01ACA100         | 1 TB   | 37      | 1415  | 28    | 3.48   |
| WDC       | WD10EZEX-00MFCA0   | 1 TB   | 1       | 1262  | 0     | 3.46   |
| WDC       | WD5000BPKX-22HPJT0 | 500 GB | 5       | 1483  | 2     | 3.46   |
| Seagate   | ST2000DM001-1CH164 | 2 TB   | 17      | 1781  | 428   | 3.46   |
| WDC       | WD10EZEX-21WN4A0   | 1 TB   | 1       | 1260  | 0     | 3.45   |
| Samsung   | HD154UI            | 1.5 TB | 2       | 1879  | 1     | 3.44   |
| WDC       | WD10EFRX-68JCSN0   | 1 TB   | 7       | 2296  | 3     | 3.43   |
| WDC       | WD20NMVW-11W68S0   | 2 TB   | 1       | 1251  | 0     | 3.43   |
| WDC       | WD1003FZEX-00MK2A0 | 1 TB   | 89      | 1361  | 14    | 3.42   |
| Seagate   | ST1000NM0011       | 1 TB   | 27      | 1693  | 81    | 3.42   |
| WDC       | WD2004FBYZ-01YCBB1 | 2 TB   | 24      | 1333  | 1     | 3.41   |
| WDC       | WD5003ABYX-18WERA0 | 500 GB | 15      | 1348  | 1     | 3.39   |
| Toshiba   | MG04ACA600EY       | 6 TB   | 2       | 1236  | 0     | 3.39   |
| WDC       | WD30EFRX-68EUZN0   | 3 TB   | 45      | 1572  | 2     | 3.37   |
| Toshiba   | MG03ACA100         | 1 TB   | 49      | 1238  | 2     | 3.34   |
| Samsung   | HD103SJ            | 1 TB   | 23      | 2119  | 4     | 3.34   |
| Seagate   | ST8000AS0002-1N... | 8 TB   | 19      | 1405  | 56    | 3.34   |
| Maxtor    | STM3500630AS       | 500 GB | 2       | 1214  | 0     | 3.33   |
| WDC       | WD10SPZX-00Z10T0   | 1 TB   | 1       | 1213  | 0     | 3.32   |
| Seagate   | ST8000NM0055-1R... | 8 TB   | 928     | 1313  | 20    | 3.32   |
| HGST      | HUS726060ALE610    | 6 TB   | 23      | 1203  | 0     | 3.30   |
| Samsung   | HD161GJ            | 160 GB | 3       | 1200  | 0     | 3.29   |
| Seagate   | ST4000DM000-1F2168 | 4 TB   | 15      | 1304  | 150   | 3.28   |
| WDC       | WD2004FBYZ-01YCBB2 | 2 TB   | 1       | 1197  | 0     | 3.28   |
| Seagate   | ST8000DM005-2EH112 | 8 TB   | 215     | 1259  | 1     | 3.27   |
| HGST      | HDN721010ALE604    | 10 TB  | 2       | 1188  | 0     | 3.26   |
| MaxDig... | MD4000GBDS         | 4 TB   | 1       | 1187  | 0     | 3.25   |
| Seagate   | ST6000VX0023-2E... | 6 TB   | 17      | 1187  | 0     | 3.25   |
| WDC       | WD4002FYYZ-01B7CB0 | 4 TB   | 51      | 1332  | 3     | 3.25   |
| HGST      | HUS726020ALE614    | 2 TB   | 78      | 1212  | 1     | 3.24   |
| Seagate   | ST2000LM003 HN-... | 2 TB   | 16      | 1315  | 1     | 3.24   |
| WDC       | WD1004FBYZ-01YCBB1 | 1 TB   | 7       | 1179  | 0     | 3.23   |
| WDC       | WD5000AAKX-221CA1  | 500 GB | 2       | 1179  | 0     | 3.23   |
| Hitachi   | HDS722020ALA330... | 2 TB   | 34      | 1748  | 29    | 3.23   |
| Toshiba   | MQ01ABF050         | 500 GB | 1       | 1175  | 0     | 3.22   |
| WDC       | WD1002F9YZ-09H1JL1 | 1 TB   | 14      | 1355  | 2     | 3.22   |
| Seagate   | ST2000VN0001-1S... | 2 TB   | 4       | 1167  | 0     | 3.20   |
| WDC       | WD10EZEX-00UD2A0   | 1 TB   | 1       | 1166  | 0     | 3.20   |
| Seagate   | ST500NM0011        | 500 GB | 9       | 1964  | 5     | 3.19   |
| WDC       | WD10EZEX-00BN5A0   | 1 TB   | 7       | 1162  | 0     | 3.19   |
| Hitachi   | HDT725050VLA360    | 500 GB | 1       | 1157  | 0     | 3.17   |
| WDC       | WD10EZEX-75WN4A0   | 1 TB   | 4       | 1156  | 0     | 3.17   |
| HGST      | HUS722T1TALA600    | 1 TB   | 29      | 1154  | 0     | 3.16   |
| WDC       | WD1003FBYX-01Y7B0  | 1 TB   | 43      | 1690  | 12    | 3.15   |
| Seagate   | ST4000VX000-1F4168 | 4 TB   | 4       | 1150  | 0     | 3.15   |
| Seagate   | ST8000DM002-1YW112 | 8 TB   | 135     | 1214  | 2     | 3.15   |
| Toshiba   | MG04ACA300E        | 3 TB   | 1       | 1147  | 0     | 3.14   |
| WDC       | WD10EALS-00Z8A0    | 1 TB   | 7       | 2354  | 5     | 3.14   |
| WDC       | WD5000LPLX-00ZNTT0 | 500 GB | 2       | 1146  | 0     | 3.14   |
| HGST      | HUS726020ALE610    | 2 TB   | 11      | 1145  | 0     | 3.14   |
| Seagate   | ST1000NM0018-2F... | 1 TB   | 36      | 1168  | 1     | 3.14   |
| WDC       | WD3001FAEX-00MJRA0 | 3 TB   | 1       | 1140  | 0     | 3.13   |
| WDC       | WD10JFCX-68N6GN0   | 1 TB   | 27      | 1188  | 1     | 3.12   |
| WDC       | WD2500BHTZ-04JCPV1 | 250 GB | 2       | 1137  | 0     | 3.12   |
| HGST      | HUH728060ALE600    | 6 TB   | 3       | 1136  | 0     | 3.11   |
| WDC       | WD1003FBYX-18Y7B0  | 1 TB   | 7       | 1997  | 2     | 3.11   |
| Seagate   | ST3160318AS        | 160 GB | 1       | 2266  | 1     | 3.11   |
| Hitachi   | HDS721050CLA362    | 500 GB | 6       | 1905  | 459   | 3.10   |
| WDC       | WD20EZRX-00D8PB0   | 2 TB   | 10      | 1212  | 1     | 3.10   |
| WDC       | WD5000BMVW-11AJGS4 | 500 GB | 3       | 1129  | 0     | 3.10   |
| WDC       | WD3000HLHX-01JJPV0 | 304 GB | 6       | 1421  | 2     | 3.06   |
| WDC       | WD5000AAKX-00ERMA0 | 500 GB | 8       | 1419  | 2     | 3.05   |
| Hitachi   | HUA722010CLA630    | 1 TB   | 11      | 1396  | 105   | 3.02   |
| WDC       | WD5000BMVV-11GNWS0 | 500 GB | 1       | 1103  | 0     | 3.02   |
| WDC       | WD1003FBYX-12      | 1 TB   | 29      | 1172  | 1     | 3.00   |
| WDC       | WD2000FYYZ-01UL1B1 | 2 TB   | 35      | 1872  | 30    | 2.98   |
| WDC       | WD101KRYZ-01JPDB1  | 10 TB  | 8       | 1133  | 144   | 2.98   |
| WDC       | WD5000BHTZ-04JCPV1 | 500 GB | 6       | 1084  | 0     | 2.97   |
| WDC       | WD3000FYYZ-01UL1B2 | 3 TB   | 9       | 1718  | 3     | 2.95   |
| Toshiba   | DT01ACA200         | 2 TB   | 287     | 1125  | 18    | 2.95   |
| Seagate   | ST1000DM003-9YN162 | 1 TB   | 3       | 2428  | 732   | 2.95   |
| WDC       | WD101KFBX-68R56N0  | 10 TB  | 1       | 1075  | 0     | 2.95   |
| HPE       | MB0500GCEHE        | 500 GB | 6       | 1073  | 0     | 2.94   |
| WDC       | WD6002FRYZ-01WD5B1 | 6 TB   | 45      | 1072  | 20    | 2.94   |
| WDC       | WD10JPLX-00MBPT0   | 1 TB   | 4       | 1065  | 0     | 2.92   |
| HGST      | HUS726040ALA614    | 4 TB   | 19      | 1193  | 4     | 2.92   |
| WDC       | WD5000HHTZ-04N21V1 | 500 GB | 3       | 1064  | 0     | 2.92   |
| Seagate   | ST500DM002-1BD142  | 500 GB | 35      | 1539  | 57    | 2.91   |
| Seagate   | ST8000NM0205-2F... | 8 TB   | 31      | 1151  | 66    | 2.89   |
| HGST      | HUH721008ALE601    | 8 TB   | 6       | 1050  | 0     | 2.88   |
| WDC       | WD5000AAKX-001CA0  | 500 GB | 5       | 1580  | 1     | 2.88   |
| WDC       | WD1003FBYZ-010FB0  | 1 TB   | 62      | 1112  | 1     | 2.87   |
| Seagate   | ST3250310AS        | 250 GB | 3       | 2192  | 209   | 2.86   |
| Seagate   | ST4000DX001-1CE168 | 4 TB   | 2       | 1038  | 0     | 2.85   |
| WDC       | WD5003ABYX-01WERA1 | 500 GB | 18      | 1224  | 1     | 2.83   |
| WDC       | WD4004FZWX-00GBGB0 | 4 TB   | 22      | 1065  | 1     | 2.83   |
| WDC       | WD5002ABYS-02B1B0  | 500 GB | 5       | 2518  | 28    | 2.79   |
| WDC       | WD30EFRX-68N32N0   | 3 TB   | 4       | 1014  | 0     | 2.78   |
| WDC       | WD2000FYYZ-01UL1B2 | 2 TB   | 40      | 1439  | 17    | 2.77   |
| WDC       | WD7500BPKT-75PK4T0 | 752 GB | 2       | 1112  | 1     | 2.74   |
| Seagate   | ST1000NX0423       | 1 TB   | 1       | 997   | 0     | 2.73   |
| Toshiba   | MG03ACA400         | 4 TB   | 24      | 1455  | 3     | 2.73   |
| WDC       | WD60EFRX-68MYMN1   | 6 TB   | 27      | 1847  | 20    | 2.73   |
| WDC       | WD10EFRX-68PJCN0   | 1 TB   | 5       | 993   | 0     | 2.72   |
| Toshiba   | HDWD120            | 2 TB   | 9       | 993   | 0     | 2.72   |
| Hitachi   | HDS721032CLA362    | 320 GB | 3       | 1673  | 571   | 2.72   |
| WDC       | WD1502FAEX-007BA0  | 1.5 TB | 2       | 1534  | 5     | 2.72   |
| HGST      | HUS726040ALE610    | 4 TB   | 23      | 990   | 0     | 2.71   |
| Samsung   | HD204UI            | 2 TB   | 3       | 1876  | 3     | 2.71   |
| Seagate   | ST3000DM001-1ER166 | 3 TB   | 16      | 1437  | 459   | 2.71   |
| WDC       | WD10EALX-009BA0    | 1 TB   | 24      | 1688  | 65    | 2.71   |
| HGST      | HUS726040ALE614    | 4 TB   | 13      | 1068  | 3     | 2.71   |
| Seagate   | ST6000NM0235-2A... | 6 TB   | 9       | 984   | 0     | 2.70   |
| HGST      | HUH728080ALE604    | 8 TB   | 17      | 1020  | 10    | 2.69   |
| WDC       | WD5000AAKX-08U6AA0 | 500 GB | 5       | 980   | 0     | 2.69   |
| Seagate   | ST3500413AS        | 500 GB | 3       | 1621  | 22    | 2.68   |
| WDC       | WD800AAJS-00PSA0   | 80 GB  | 1       | 977   | 0     | 2.68   |
| WDC       | WD3003FZEX-00Z4SA0 | 3 TB   | 8       | 976   | 0     | 2.67   |
| WDC       | WD4000FYYZ-01UL1B2 | 4 TB   | 25      | 1902  | 89    | 2.67   |
| Seagate   | ST750LX003-1AC154  | 752 GB | 1       | 2921  | 2     | 2.67   |
| WDC       | WD40EZRZ-00WN9B0   | 4 TB   | 18      | 1175  | 13    | 2.67   |
| Seagate   | ST1000DM003-1SB10C | 1 TB   | 6       | 1230  | 1     | 2.66   |
| WDC       | WD5000AAKX-083CA1  | 500 GB | 2       | 1651  | 4     | 2.66   |
| WDC       | WD8002FRYZ-01FF2B0 | 8 TB   | 14      | 999   | 1     | 2.65   |
| Hitachi   | HUS724030ALE641    | 3 TB   | 13      | 1171  | 49    | 2.62   |
| WDC       | WD5003AZEX-00MK2A0 | 500 GB | 10      | 953   | 0     | 2.61   |
| MediaMax  | WL2000GSA6472E     | 2 TB   | 1       | 948   | 0     | 2.60   |
| HGST      | HTE721010A9E630    | 1 TB   | 2       | 1767  | 8     | 2.59   |
| Fujitsu   | MHW2120BH          | 120 GB | 1       | 946   | 0     | 2.59   |
| WDC       | WD2003FYYS-27Y2P0  | 2 TB   | 2       | 1374  | 2     | 2.59   |
| Seagate   | ST2000DM001-9YN164 | 2 TB   | 4       | 2324  | 260   | 2.59   |
| Seagate   | ST380815AS         | 80 GB  | 5       | 944   | 0     | 2.59   |
| Toshiba   | MG04ACA400EY       | 4 TB   | 12      | 940   | 0     | 2.58   |
| WDC       | WD2500AAJS-00VTA0  | 250 GB | 1       | 934   | 0     | 2.56   |
| Seagate   | ST6000VN0033-2E... | 6 TB   | 3       | 934   | 0     | 2.56   |
| Toshiba   | MG04ACA400N        | 4 TB   | 6       | 1201  | 2     | 2.55   |
| Toshiba   | MK2035GSS          | 200 GB | 1       | 927   | 0     | 2.54   |
| Seagate   | ST1000LM048-2E7172 | 1 TB   | 10      | 924   | 0     | 2.53   |
| Seagate   | ST1000DX001-1NS162 | 1 TB   | 6       | 1689  | 219   | 2.53   |
| WDC       | WD10EZEX-60M2NA0   | 1 TB   | 3       | 1242  | 339   | 2.53   |
| MediaMax  | WL500GSA3272       | 500 GB | 1       | 920   | 0     | 2.52   |
| WDC       | WD80EFZX-68UW8N0   | 8 TB   | 23      | 957   | 1     | 2.51   |
| Seagate   | ST32000644NS       | 2 TB   | 11      | 1525  | 11    | 2.51   |
| WDC       | WD40EFRX-68N32N0   | 4 TB   | 79      | 1007  | 1     | 2.50   |
| WDC       | WD10EZEX-22MFCA0   | 1 TB   | 2       | 911   | 0     | 2.50   |
| HGST      | HUS722T1TALA604    | 1 TB   | 124     | 923   | 10    | 2.50   |
| Toshiba   | HDWD105            | 500 GB | 5       | 907   | 0     | 2.49   |
| WDC       | WD10EZEX-00WN4A0   | 1 TB   | 6       | 906   | 0     | 2.48   |
| WDC       | WD2003FZEX-00Z4SA0 | 2 TB   | 14      | 1086  | 1     | 2.47   |
| Seagate   | ST1000NM0055-1V... | 1 TB   | 31      | 1019  | 2     | 2.45   |
| HGST      | HUH721212ALN600    | 12 TB  | 11      | 1003  | 17    | 2.45   |
| Hitachi   | HDP725025GLA380    | 250 GB | 2       | 4193  | 6     | 2.44   |
| WDC       | WD20EFRX-68EUZN0   | 2 TB   | 48      | 1137  | 10    | 2.43   |
| WDC       | WD2000F9YZ-09N20L1 | 2 TB   | 4       | 886   | 0     | 2.43   |
| WDC       | WD2000FYYZ-05UL1B0 | 2 TB   | 11      | 927   | 1     | 2.42   |
| HGST      | HUH721010ALN600    | 10 TB  | 16      | 882   | 0     | 2.42   |
| WDC       | WD2003FYYS-02W0B0  | 2 TB   | 8       | 1482  | 8     | 2.41   |
| HGST      | HUS726020ALA610    | 2 TB   | 133     | 936   | 19    | 2.39   |
| Seagate   | ST12000VN0007-2... | 12 TB  | 8       | 871   | 0     | 2.39   |
| WDC       | WD1005FBYZ-01YCBB2 | 1 TB   | 21      | 920   | 1     | 2.39   |
| Samsung   | HD103SI            | 1 TB   | 3       | 3325  | 806   | 2.38   |
| HGST      | HUH721010ALE604    | 10 TB  | 111     | 869   | 0     | 2.38   |
| Toshiba   | DT01ACA050         | 500 GB | 19      | 1005  | 129   | 2.36   |
| WDC       | WD10EZEX-60ZF5A0   | 1 TB   | 5       | 1753  | 7     | 2.36   |
| HPE       | MM1000GFJTE        | 1 TB   | 4       | 862   | 0     | 2.36   |
| Seagate   | ST10000NM0016-1... | 10 TB  | 1507    | 1335  | 66    | 2.36   |
| Seagate   | ST1000DM005 HD1... | 1 TB   | 4       | 1986  | 4     | 2.35   |
| Toshiba   | MG03ACA200         | 2 TB   | 7       | 920   | 288   | 2.35   |
| Seagate   | ST12000NM0007-2... | 12 TB  | 140     | 870   | 7     | 2.33   |
| Seagate   | ST3400620NS        | 400 GB | 1       | 1695  | 1     | 2.32   |
| WDC       | WD5003AZEX-00K3CA0 | 500 GB | 3       | 846   | 0     | 2.32   |
| WDC       | WD4002FYYZ-01B7CB1 | 4 TB   | 45      | 873   | 3     | 2.31   |
| HGST      | HUS726040ALA610    | 4 TB   | 68      | 863   | 1     | 2.29   |
| Seagate   | ST3750640AS        | 752 GB | 1       | 2504  | 2     | 2.29   |
| WDC       | WD50EFRX-68L0BN1   | 5 TB   | 6       | 1538  | 118   | 2.27   |
| HPE       | MB0500EBNCR        | 500 GB | 5       | 1525  | 18    | 2.26   |
| Seagate   | ST1000VN002-2EY102 | 1 TB   | 4       | 817   | 0     | 2.24   |
| WDC       | WD40EZRZ-22GXCB0   | 4 TB   | 16      | 813   | 0     | 2.23   |
| Seagate   | ST3250318AS        | 250 GB | 3       | 1375  | 17    | 2.22   |
| WDC       | WD5002ABYS-18B1B0  | 500 GB | 1       | 808   | 0     | 2.22   |
| Seagate   | ST2000DM006-2DM164 | 2 TB   | 40      | 840   | 27    | 2.21   |
| HP        | MB2000GCEHK        | 2 TB   | 2       | 1625  | 1051  | 2.20   |
| Samsung   | HD502HJ            | 500 GB | 2       | 800   | 0     | 2.19   |
| Seagate   | ST2000NM0125-1Y... | 2 TB   | 13      | 800   | 0     | 2.19   |
| Seagate   | ST500DM009-2F110A  | 500 GB | 1       | 799   | 0     | 2.19   |
| Seagate   | ST3120022A         | 120 GB | 1       | 3197  | 3     | 2.19   |
| HPE       | MM1000GBKAL        | 1 TB   | 21      | 1099  | 1     | 2.19   |
| HGST      | HUH728060ALE604    | 6 TB   | 14      | 1198  | 20    | 2.18   |
| WDC       | WD1003FZEX-00K3CA0 | 1 TB   | 207     | 816   | 3     | 2.18   |
| Toshiba   | HDWL120            | 2 TB   | 4       | 793   | 0     | 2.17   |
| Seagate   | ST10000NM0196-2... | 10 TB  | 198     | 869   | 37    | 2.17   |
| Seagate   | ST31000NSSUN1.0T   | 1 TB   | 1       | 1574  | 1     | 2.16   |
| WDC       | WD2000F9YZ-09N20L0 | 2 TB   | 2       | 1354  | 3     | 2.15   |
| Seagate   | ST3500418AS        | 500 GB | 7       | 789   | 2     | 2.15   |
| WDC       | WD800JD-00HKA0     | 80 GB  | 1       | 776   | 0     | 2.13   |
| WDC       | WD4000F9YZ-09N20L0 | 4 TB   | 4       | 775   | 0     | 2.12   |
| HPE       | MM2000GEFRA        | 2 TB   | 241     | 824   | 1     | 2.11   |
| WDC       | WD2005FBYZ-01YCBB2 | 2 TB   | 40      | 770   | 0     | 2.11   |
| HGST      | HUS726060ALA640    | 6 TB   | 46      | 779   | 1     | 2.11   |
| WDC       | WD4000FYYZ-01UL1B3 | 4 TB   | 23      | 1115  | 16    | 2.11   |
| WDC       | WD1005FBYZ-01YCBB1 | 1 TB   | 3       | 979   | 1     | 2.10   |
| Seagate   | ST3500312CS        | 500 GB | 10      | 964   | 2     | 2.10   |
| Hitachi   | HUS724040ALE640    | 4 TB   | 22      | 797   | 1     | 2.08   |
| HGST      | HTS721010A9E630    | 1 TB   | 5       | 1455  | 605   | 2.07   |
| HGST      | HUH721008ALE600    | 8 TB   | 46      | 752   | 0     | 2.06   |
| Hitachi   | HDE721050SLA330    | 500 GB | 2       | 3732  | 4     | 2.05   |
| HGST      | HUH721212ALN604    | 12 TB  | 2050    | 770   | 2     | 2.03   |
| WDC       | WD5000AZLX-08K2TA0 | 500 GB | 3       | 733   | 0     | 2.01   |
| Hitachi   | HTS723216L9SA60    | 160 GB | 1       | 727   | 0     | 1.99   |
| WDC       | WD100EFAX-68LHPN0  | 10 TB  | 53      | 733   | 1     | 1.99   |
| Seagate   | ST6000NM0115-1Y... | 6 TB   | 127     | 791   | 14    | 1.99   |
| Seagate   | ST4000NC001-1FS168 | 4 TB   | 6       | 722   | 0     | 1.98   |
| Seagate   | ST500DM005 HD502HJ | 500 GB | 1       | 716   | 0     | 1.96   |
| Seagate   | ST2000NM0018-2F... | 2 TB   | 1       | 716   | 0     | 1.96   |
| Seagate   | ST6000DM003-2CY186 | 6 TB   | 3       | 715   | 0     | 1.96   |
| Seagate   | ST12000DM0007-2... | 12 TB  | 5       | 712   | 0     | 1.95   |
| Seagate   | ST2000LM007-1R8174 | 2 TB   | 38      | 963   | 210   | 1.93   |
| Seagate   | ST33000651NS       | 3 TB   | 1       | 704   | 0     | 1.93   |
| Seagate   | ST1000LM024 HN-... | 1 TB   | 3       | 1027  | 4     | 1.92   |
| Seagate   | ST10000DM0004-2... | 10 TB  | 7       | 990   | 87    | 1.92   |
| Toshiba   | HDWA130            | 3 TB   | 1       | 699   | 0     | 1.92   |
| HGST      | HUS726T6TALE6L4    | 6 TB   | 191     | 710   | 22    | 1.91   |
| WDC       | WD30PURZ-85GU6Y0   | 3 TB   | 2       | 696   | 0     | 1.91   |
| Seagate   | ST8000VN0022-2E... | 8 TB   | 27      | 697   | 39    | 1.91   |
| Seagate   | ST4000NM0035-1V... | 4 TB   | 173     | 715   | 6     | 1.91   |
| WDC       | WD1003FBYX-01Y7B2  | 1 TB   | 1       | 695   | 0     | 1.91   |
| WDC       | WD2003FZEX-00SRLA0 | 2 TB   | 65      | 698   | 1     | 1.89   |
| HGST      | HUS726T4TALN6L4    | 4 TB   | 20      | 688   | 0     | 1.89   |
| Seagate   | ST1000DM003-1SB102 | 1 TB   | 7       | 970   | 21    | 1.88   |
| Seagate   | ST1000NM0008-2F... | 1 TB   | 57      | 683   | 0     | 1.87   |
| WDC       | WD5000AAKX-22ERMA0 | 500 GB | 1       | 680   | 0     | 1.86   |
| Seagate   | ST10000NE0004-1... | 10 TB  | 15      | 1217  | 373   | 1.86   |
| WDC       | WD2002FYPS-02W3B0  | 2 TB   | 1       | 679   | 0     | 1.86   |
| Seagate   | ST2000NM0008-2F... | 2 TB   | 73      | 696   | 72    | 1.85   |
| Seagate   | ST1000VX005-2EZ102 | 1 TB   | 3       | 672   | 0     | 1.84   |
| Seagate   | ST10000NM0086-2... | 10 TB  | 101     | 734   | 12    | 1.84   |
| WDC       | WD40EZRZ-75GXCB0   | 4 TB   | 3       | 668   | 0     | 1.83   |
| WDC       | WD30PURX-64P6ZY0   | 3 TB   | 1       | 667   | 0     | 1.83   |
| Toshiba   | MD04ACA400         | 4 TB   | 440     | 681   | 3     | 1.83   |
| Seagate   | ST10000NM0156-2... | 10 TB  | 37      | 743   | 6     | 1.83   |
| WDC       | WD5000AZLX-35K2TA0 | 500 GB | 3       | 664   | 0     | 1.82   |
| WDC       | WD15EARS-00MVWB0   | 1.5 TB | 4       | 2054  | 324   | 1.81   |
| Seagate   | ST2000NM0055-1V... | 2 TB   | 21      | 900   | 2     | 1.79   |
| WDC       | WD10EFRX-68FYTN0   | 1 TB   | 17      | 712   | 2     | 1.79   |
| Seagate   | ST14000VN0008-2... | 14 TB  | 12      | 653   | 0     | 1.79   |
| Seagate   | ST2000VX003-1HH164 | 2 TB   | 3       | 652   | 0     | 1.79   |
| Seagate   | ST10000VN0004-1... | 10 TB  | 30      | 808   | 110   | 1.78   |
| Toshiba   | MG05ACA800E        | 8 TB   | 666     | 683   | 1     | 1.78   |
| MediaMax  | WL4000GSA6472      | 4 TB   | 2       | 649   | 0     | 1.78   |
| Toshiba   | HDWR21C            | 12 TB  | 5       | 649   | 0     | 1.78   |
| WDC       | WD2500AAJS-08L7A0  | 250 GB | 1       | 1944  | 2     | 1.78   |
| Seagate   | ST2000NX0253       | 2 TB   | 49      | 832   | 16    | 1.77   |
| Hitachi   | HDS723030ALA640... | 3 TB   | 2       | 975   | 1     | 1.77   |
| Toshiba   | MG07ACA14TE        | 14 TB  | 46      | 662   | 1     | 1.76   |
| Seagate   | ST2000DL003-9VT166 | 2 TB   | 6       | 1354  | 366   | 1.75   |
| Samsung   | HE103SJ            | 1 TB   | 1       | 637   | 0     | 1.75   |
| Toshiba   | HDWT140            | 4 TB   | 2       | 634   | 0     | 1.74   |
| Seagate   | ST4000LM024-2AN17V | 4 TB   | 47      | 665   | 18    | 1.73   |
| WDC       | WD2002FFSX-68PF8N0 | 2 TB   | 13      | 627   | 0     | 1.72   |
| HP        | MB2000EAZNL        | 2 TB   | 2       | 809   | 2     | 1.72   |
| Toshiba   | HDWU130            | 3 TB   | 8       | 715   | 8     | 1.71   |
| WDC       | WD10JMVW-11AJGS3   | 1 TB   | 1       | 624   | 0     | 1.71   |
| WDC       | WD30EZRZ-00Z5HB0   | 3 TB   | 8       | 657   | 1     | 1.71   |
| Seagate   | ST12000NM0017-2... | 12 TB  | 2       | 621   | 0     | 1.70   |
| WDC       | WD8003FFBX-68B9AN0 | 8 TB   | 3       | 616   | 0     | 1.69   |
| Hitachi   | HDE721010SLA330    | 1 TB   | 4       | 1982  | 3     | 1.69   |
| WDC       | WD80EFAX-68KNBN0   | 8 TB   | 5       | 614   | 0     | 1.68   |
| WDC       | WD3200AAJS-08L7A0  | 320 GB | 1       | 612   | 0     | 1.68   |
| WDC       | WD121KRYZ-01W0RB0  | 12 TB  | 8       | 812   | 1     | 1.68   |
| Seagate   | ST4000LM016-1N2170 | 4 TB   | 3       | 609   | 0     | 1.67   |
| HGST      | HUS722T2TALA604    | 2 TB   | 84      | 621   | 1     | 1.66   |
| WDC       | WD10EURX-63UY4Y0   | 1 TB   | 2       | 606   | 0     | 1.66   |
| Seagate   | ST4000NM0245-1Z... | 4 TB   | 29      | 841   | 22    | 1.66   |
| WDC       | WD40EZRZ-00GXCB0   | 4 TB   | 40      | 621   | 29    | 1.65   |
| Toshiba   | HDWD130            | 3 TB   | 8       | 621   | 2     | 1.65   |
| Seagate   | ST3000VX010-2E3166 | 3 TB   | 5       | 595   | 0     | 1.63   |
| Seagate   | ST6000VX001-2BD186 | 6 TB   | 4       | 595   | 0     | 1.63   |
| HGST      | HUH721212ALE604    | 12 TB  | 175     | 598   | 1     | 1.62   |
| Seagate   | ST2000LM015-2E8174 | 2 TB   | 15      | 717   | 133   | 1.62   |
| WDC       | WD5000LPVX-22V0TT0 | 500 GB | 3       | 583   | 0     | 1.60   |
| WDC       | WD4000FYYZ-01UL1B1 | 4 TB   | 10      | 1633  | 5     | 1.60   |
| Seagate   | ST2000NX0403       | 2 TB   | 7       | 611   | 1     | 1.60   |
| Seagate   | ST2000NX0423       | 2 TB   | 60      | 587   | 1     | 1.59   |
| Toshiba   | HDWE140            | 4 TB   | 42      | 595   | 43    | 1.58   |
| WDC       | WD5000AAKS-00V1A0  | 500 GB | 1       | 2881  | 4     | 1.58   |
| WDC       | WD4003FFBX-68MU3N0 | 4 TB   | 33      | 573   | 0     | 1.57   |
| WDC       | WD10EZRZ-00HTKB0   | 1 TB   | 3       | 564   | 0     | 1.55   |
| HPE       | MB004000GWFWB      | 4 TB   | 15      | 564   | 0     | 1.55   |
| HGST      | HUH721212ALE600    | 12 TB  | 64      | 559   | 0     | 1.53   |
| WDC       | WD5000AAKS-65YGA0  | 500 GB | 1       | 556   | 0     | 1.52   |
| Seagate   | ST8000VX0022-2E... | 8 TB   | 4       | 758   | 9     | 1.52   |
| WDC       | WD82PURZ-85TEUY0   | 8 TB   | 19      | 553   | 0     | 1.52   |
| WDC       | WD2003FYYS-05T9B0  | 2 TB   | 4       | 609   | 1     | 1.51   |
| Seagate   | ST3000DM007-1WY10G | 3 TB   | 3       | 549   | 0     | 1.50   |
| WDC       | WD10EZEX-60WN4A0   | 1 TB   | 1       | 545   | 0     | 1.49   |
| WDC       | WD50EFRX-68MYMN1   | 5 TB   | 2       | 2167  | 4     | 1.49   |
| Toshiba   | HDWN160            | 6 TB   | 10      | 640   | 3     | 1.47   |
| HGST      | HUH721010ALE600    | 10 TB  | 219     | 539   | 1     | 1.47   |
| Seagate   | ST8000DM0004-1Z... | 8 TB   | 10      | 956   | 317   | 1.46   |
| HGST      | HUS726T4TALE6L4    | 4 TB   | 62      | 531   | 0     | 1.46   |
| HGST      | HTS725050A7E630    | 500 GB | 3       | 1123  | 1015  | 1.46   |
| Seagate   | ST1000DL002-9TT153 | 1 TB   | 2       | 877   | 509   | 1.45   |
| WDC       | WD10EZEX-08WN4A0   | 1 TB   | 35      | 527   | 0     | 1.45   |
| WDC       | WD5000AZLX-22JKKA0 | 500 GB | 3       | 840   | 2     | 1.45   |
| Toshiba   | MG04ACA400E        | 4 TB   | 69      | 532   | 32    | 1.44   |
| WDC       | WD10JPVX-08JC3T5   | 1 TB   | 1       | 523   | 0     | 1.43   |
| Seagate   | ST2000NM0011       | 2 TB   | 14      | 1590  | 143   | 1.43   |
| Toshiba   | MG06ACA10TEY       | 10 TB  | 70      | 541   | 1     | 1.43   |
| Seagate   | ST12000NM0008-2... | 12 TB  | 163     | 521   | 4     | 1.41   |
| Seagate   | ST16000NE000-2R... | 16 TB  | 2       | 511   | 0     | 1.40   |
| WDC       | WD8003FRYZ-01JPDB1 | 8 TB   | 5       | 510   | 0     | 1.40   |
| HGST      | HUS728T8TALE6L4    | 8 TB   | 49      | 507   | 1     | 1.38   |
| WDC       | WD3000FYYZ-01UL1B3 | 3 TB   | 5       | 499   | 0     | 1.37   |
| HGST      | HDN728080ALE604    | 8 TB   | 2       | 766   | 16    | 1.36   |
| WDC       | WD10EZRX-00D8PB0   | 1 TB   | 1       | 490   | 0     | 1.34   |
| WDC       | WD5000LPCX-24VHAT0 | 500 GB | 2       | 483   | 0     | 1.32   |
| Seagate   | ST1000NM0011 81... | 1 TB   | 1       | 2394  | 4     | 1.31   |
| WDC       | WD60PURZ-85ZUFY1   | 6 TB   | 6       | 477   | 0     | 1.31   |
| Samsung   | HD322GJ            | 320 GB | 1       | 477   | 0     | 1.31   |
| WDC       | WD60EFRX-68L0BN1   | 6 TB   | 182     | 1159  | 6     | 1.31   |
| Seagate   | ST3750640NS        | 752 GB | 13      | 1262  | 802   | 1.29   |
| Seagate   | ST10000NM0568-2... | 10 TB  | 32      | 468   | 0     | 1.28   |
| WDC       | WD10JMVW-11AJGS4   | 1 TB   | 1       | 465   | 0     | 1.28   |
| Seagate   | ST2000DM005-2CW102 | 2 TB   | 1       | 459   | 0     | 1.26   |
| Seagate   | ST3500514NS        | 500 GB | 4       | 1140  | 10    | 1.26   |
| Seagate   | ST1000NX0313       | 1 TB   | 32      | 1167  | 23    | 1.25   |
| WDC       | WD3200AAKS-00SBA0  | 320 GB | 1       | 2733  | 5     | 1.25   |
| WDC       | WD40EFAX-68JH4N0   | 4 TB   | 38      | 464   | 1     | 1.24   |
| WDC       | WD121KFBX-68EF5N0  | 12 TB  | 7       | 451   | 0     | 1.24   |
| HGST      | HUH721008ALE604    | 8 TB   | 22      | 445   | 0     | 1.22   |
| Seagate   | ST4000NM0115-1Y... | 4 TB   | 87      | 453   | 1     | 1.22   |
| WDC       | WD4005FZBX-00K5WB0 | 4 TB   | 8       | 445   | 0     | 1.22   |
| WDC       | WD3200AAJS-40VWA1  | 320 GB | 1       | 443   | 0     | 1.21   |
| Seagate   | ST10000NM0146-2... | 10 TB  | 1       | 441   | 0     | 1.21   |
| Toshiba   | MG07ACA12TEY       | 12 TB  | 52      | 449   | 2     | 1.21   |
| WDC       | WD20PURX-64P6ZY0   | 2 TB   | 1       | 440   | 0     | 1.21   |
| WDC       | WD1005FBYZ-01YCBB3 | 1 TB   | 1       | 438   | 0     | 1.20   |
| Seagate   | ST31000524AS       | 1 TB   | 6       | 1739  | 759   | 1.20   |
| HGST      | HUS726T6TALE6L1    | 6 TB   | 12      | 436   | 0     | 1.20   |
| WDC       | WD800JD-60LSA5     | 80 GB  | 1       | 434   | 0     | 1.19   |
| HPE       | MB001000GWFGF      | 1 TB   | 1       | 431   | 0     | 1.18   |
| Toshiba   | MG06ACA800EY       | 8 TB   | 15      | 430   | 0     | 1.18   |
| Samsung   | HD080HJ            | 80 GB  | 2       | 1964  | 841   | 1.17   |
| WDC       | WD5000AAKS-00UU3A0 | 500 GB | 2       | 1646  | 4     | 1.16   |
| Seagate   | ST2000DX002-2DV164 | 2 TB   | 4       | 424   | 0     | 1.16   |
| Toshiba   | HDWD110            | 1 TB   | 74      | 440   | 49    | 1.16   |
| Seagate   | ST1000DM010-2EP102 | 1 TB   | 63      | 463   | 22    | 1.16   |
| Toshiba   | MG06ACA10TE        | 10 TB  | 123     | 427   | 1     | 1.16   |
| Seagate   | ST10000VE0008-2... | 10 TB  | 4       | 422   | 0     | 1.16   |
| WDC       | WD3000F9YZ-09N20L1 | 3 TB   | 1       | 419   | 0     | 1.15   |
| WDC       | WD1501FASS-00U0B0  | 1.5 TB | 1       | 3755  | 8     | 1.14   |
| Toshiba   | MG03ACA300         | 3 TB   | 2       | 750   | 6     | 1.14   |
| Toshiba   | MD04ACA50D         | 5 TB   | 1       | 416   | 0     | 1.14   |
| Toshiba   | MG04ACA200N        | 2 TB   | 4       | 458   | 1     | 1.14   |
| Samsung   | HD501LJ            | 500 GB | 3       | 2183  | 676   | 1.14   |
| Seagate   | ST3250312AS        | 250 GB | 3       | 777   | 4     | 1.14   |
| HGST      | HUS726T4TALA6L4    | 4 TB   | 137     | 415   | 5     | 1.13   |
| Hitachi   | HUS724040ALE641    | 4 TB   | 14      | 551   | 2     | 1.13   |
| Seagate   | ST8000DM004-2CX188 | 8 TB   | 20      | 496   | 18    | 1.12   |
| Toshiba   | MG04ACA200E        | 2 TB   | 34      | 406   | 0     | 1.11   |
| WDC       | WD1503FYYS-02W0B0  | 1.5 TB | 2       | 3213  | 7     | 1.10   |
| Seagate   | ST10000VN0008-2... | 10 TB  | 25      | 400   | 0     | 1.10   |
| Seagate   | ST1000LM049-2GH172 | 1 TB   | 4       | 398   | 0     | 1.09   |
| Toshiba   | HDWQ140            | 4 TB   | 11      | 395   | 0     | 1.08   |
| WDC       | WD10EZEX-75WN4A1   | 1 TB   | 2       | 392   | 0     | 1.08   |
| WDC       | WD5000AZLX-00JKKA0 | 500 GB | 2       | 392   | 0     | 1.08   |
| WDC       | WD60EFAX-68SHWN0   | 6 TB   | 4       | 442   | 1     | 1.07   |
| WDC       | WD4003FRYZ-01F0DB0 | 4 TB   | 29      | 391   | 0     | 1.07   |
| WDC       | WD40EZAZ-00ZGHB0   | 4 TB   | 2       | 388   | 0     | 1.06   |
| WDC       | WD20EZRZ-00Z5HB0   | 2 TB   | 11      | 506   | 99    | 1.06   |
| WDC       | WD20PURZ-85GU6Y0   | 2 TB   | 7       | 386   | 0     | 1.06   |
| HP        | MB2000GCWDA        | 2 TB   | 2       | 383   | 0     | 1.05   |
| Seagate   | ST6000NM021A-2R... | 6 TB   | 84      | 401   | 13    | 1.05   |
| Hitachi   | HTS542512K9SA00    | 120 GB | 1       | 1508  | 3     | 1.03   |
| Seagate   | ST6000NE0021-2E... | 6 TB   | 3       | 1114  | 144   | 1.03   |
| WDC       | WUH721414ALE604    | 14 TB  | 1998    | 376   | 1     | 1.03   |
| HGST      | HUS728T8TALE6L0    | 8 TB   | 15      | 370   | 0     | 1.01   |
| WDC       | WD3200BPVT-22JJ5T0 | 320 GB | 2       | 369   | 0     | 1.01   |
| WDC       | WD60EZRZ-00GZ5B1   | 6 TB   | 2       | 367   | 0     | 1.01   |
| Toshiba   | HDWR160            | 6 TB   | 1       | 364   | 0     | 1.00   |
| Toshiba   | MG06ACA600E        | 6 TB   | 10      | 363   | 0     | 1.00   |
| Seagate   | ST3250823AS        | 250 GB | 1       | 4725  | 12    | 1.00   |
| Seagate   | ST4000NM0033       | 4 TB   | 4       | 362   | 0     | 0.99   |
| Toshiba   | MG07ACA12TE        | 12 TB  | 104     | 374   | 1     | 0.99   |
| WDC       | WD5000LPCX-00VHAT0 | 500 GB | 2       | 361   | 0     | 0.99   |
| Seagate   | ST5000LM000-2AN170 | 5 TB   | 44      | 473   | 34    | 0.99   |
| WDC       | WD5000AZLX-00K2TA0 | 500 GB | 1       | 358   | 0     | 0.98   |
| WDC       | WD2005FBYZ-01YCBB3 | 2 TB   | 5       | 351   | 0     | 0.96   |
| Seagate   | ST3000DM008-2DM166 | 3 TB   | 6       | 699   | 185   | 0.94   |
| Seagate   | ST14000NM001G-2... | 14 TB  | 16      | 343   | 0     | 0.94   |
| WDC       | WD3000F9YZ-09N20L0 | 3 TB   | 1       | 1714  | 4     | 0.94   |
| WDC       | WD10EALS-002BA0    | 1 TB   | 2       | 2394  | 6     | 0.94   |
| WDC       | WD6003FZBX-00GXAB0 | 6 TB   | 7       | 337   | 0     | 0.92   |
| WDC       | WD20SPZX-00UA7T0   | 2 TB   | 5       | 336   | 0     | 0.92   |
| Seagate   | ST3500411SV        | 500 GB | 1       | 335   | 0     | 0.92   |
| HGST      | HUS726T4TALA6L1    | 4 TB   | 79      | 340   | 1     | 0.91   |
| Seagate   | ST500LT012-1DG142  | 500 GB | 3       | 330   | 0     | 0.91   |
| Hitachi   | HDS721050DLE630    | 500 GB | 4       | 808   | 420   | 0.90   |
| HPE       | MB8000GFECR        | 8 TB   | 56      | 353   | 28    | 0.90   |
| Seagate   | ST4000DM004-2CV104 | 4 TB   | 14      | 350   | 2     | 0.90   |
| WDC       | WD4000FYYZ-01UL1B0 | 4 TB   | 20      | 2195  | 175   | 0.90   |
| Maxtor    | STM3500320AS       | 500 GB | 2       | 514   | 617   | 0.89   |
| WDC       | WD5000LPLX-08ZNTT0 | 500 GB | 2       | 323   | 0     | 0.89   |
| Seagate   | ST4000VN008-2DR166 | 4 TB   | 85      | 337   | 80    | 0.89   |
| WDC       | WD20EZAZ-00GGJB0   | 2 TB   | 5       | 320   | 0     | 0.88   |
| Seagate   | ST8000NE001-2M7101 | 8 TB   | 1       | 317   | 0     | 0.87   |
| Seagate   | ST10000VX0004-1... | 10 TB  | 18      | 317   | 0     | 0.87   |
| Seagate   | ST10000NM0478-2... | 10 TB  | 28      | 336   | 4     | 0.87   |
| Toshiba   | MK2002TSKB         | 2 TB   | 1       | 2835  | 8     | 0.86   |
| Toshiba   | MQ01ACF050         | 500 GB | 5       | 500   | 2     | 0.86   |
| Seagate   | ST8000VN004-2M2101 | 8 TB   | 196     | 325   | 31    | 0.85   |
| HPE       | MB0500EBZQA        | 500 GB | 1       | 2783  | 8     | 0.85   |
| Seagate   | ST8000VE000-2P6101 | 8 TB   | 1       | 307   | 0     | 0.84   |
| Seagate   | ST31000528AS       | 1 TB   | 4       | 777   | 351   | 0.84   |
| Toshiba   | MG04ACA100NY       | 1 TB   | 7       | 359   | 12    | 0.84   |
| WDC       | WD7500BPVT-22HXZT3 | 752 GB | 1       | 301   | 0     | 0.83   |
| WDC       | WD800BEVS-22RST0   | 80 GB  | 1       | 299   | 0     | 0.82   |
| WDC       | WD101EFAX-68LDBN0  | 10 TB  | 10      | 299   | 0     | 0.82   |
| Seagate   | ST9500420ASG       | 500 GB | 4       | 713   | 19    | 0.81   |
| WDC       | WD6003FFBX-68MU3N0 | 6 TB   | 9       | 295   | 0     | 0.81   |
| Seagate   | ST2000LX001-1RG174 | 2 TB   | 4       | 295   | 0     | 0.81   |
| Seagate   | ST8000VX004-2M1101 | 8 TB   | 5       | 295   | 416   | 0.79   |
| Seagate   | ST9750420AS        | 752 GB | 1       | 285   | 0     | 0.78   |
| Seagate   | ST1000NX0443       | 1 TB   | 22      | 281   | 0     | 0.77   |
| Seagate   | ST31000524NS       | 1 TB   | 20      | 1090  | 107   | 0.77   |
| Seagate   | ST2000NM000A-2J... | 2 TB   | 1       | 281   | 0     | 0.77   |
| WDC       | WD6003FZBX-00K5WB0 | 6 TB   | 13      | 281   | 0     | 0.77   |
| HPE       | MB012000GWDFE      | 12 TB  | 35      | 281   | 0     | 0.77   |
| Seagate   | ST18000NM000J-2... | 18 TB  | 11      | 276   | 0     | 0.76   |
| Seagate   | ST10000DM0004-1... | 10 TB  | 7       | 482   | 453   | 0.76   |
| WDC       | WD1200JS-00NCB1    | 120 GB | 1       | 3027  | 10    | 0.75   |
| Seagate   | ST16000NM001G-2... | 16 TB  | 57      | 273   | 0     | 0.75   |
| Hitachi   | HDS721025CLA382    | 250 GB | 1       | 2454  | 8     | 0.75   |
| WDC       | WD5001AALS-00L3B2  | 500 GB | 2       | 3341  | 515   | 0.74   |
| WDC       | WD3000FYYZ-01UL1B1 | 3 TB   | 1       | 2435  | 8     | 0.74   |
| Seagate   | ST32000542AS       | 2 TB   | 1       | 264   | 0     | 0.73   |
| Seagate   | ST12000NM0248-2... | 12 TB  | 2       | 263   | 0     | 0.72   |
| WDC       | WD102KFBX-68M95N0  | 10 TB  | 32      | 262   | 0     | 0.72   |
| Seagate   | ST4000NM000A-2H... | 4 TB   | 10      | 301   | 1     | 0.71   |
| Seagate   | ST3160815AS        | 160 GB | 3       | 2011  | 183   | 0.71   |
| WDC       | WUS721010ALE6L4    | 10 TB  | 21      | 257   | 11    | 0.70   |
| Hitachi   | HTS545032B9A300    | 320 GB | 1       | 1024  | 3     | 0.70   |
| Seagate   | ST31000524NS 45... | 1 TB   | 2       | 682   | 2     | 0.70   |
| Toshiba   | MG04ACA400NY       | 4 TB   | 3       | 252   | 0     | 0.69   |
| HPE       | MB008000GWRTC      | 8 TB   | 1       | 252   | 0     | 0.69   |
| WDC       | WD10EARS-00Y5B1    | 1 TB   | 4       | 1203  | 697   | 0.69   |
| WDC       | WD3200AAKS-22B3A0  | 320 GB | 1       | 250   | 0     | 0.69   |
| Seagate   | ST2000VN004-2E4164 | 2 TB   | 25      | 247   | 0     | 0.68   |
| WDC       | WD1000DHTZ-04N21V0 | 1 TB   | 11      | 328   | 1     | 0.68   |
| WDC       | WD1001FALS-00J7B1  | 1 TB   | 1       | 246   | 0     | 0.67   |
| WDC       | WD10TMVW-11ZSMS1   | 1 TB   | 1       | 492   | 1     | 0.67   |
| HP        | VB0250EAVER        | 250 GB | 3       | 418   | 205   | 0.67   |
| WDC       | WD10EADS-11P8B1    | 1 TB   | 1       | 2191  | 8     | 0.67   |
| WDC       | WD2003FYYS-70W0B0  | 2 TB   | 1       | 240   | 0     | 0.66   |
| WDC       | WD5000AAKX-603CA0  | 500 GB | 1       | 239   | 0     | 0.66   |
| Seagate   | ST3320418AS        | 320 GB | 1       | 239   | 0     | 0.66   |
| Toshiba   | HDWG11A            | 10 TB  | 1       | 235   | 0     | 0.65   |
| WDC       | WD15NMVW-11W68S0   | 1.5 TB | 1       | 233   | 0     | 0.64   |
| Seagate   | ST8000NM000A-2K... | 8 TB   | 51      | 240   | 1     | 0.63   |
| WDC       | WD5000AZRZ-00HTKB0 | 500 GB | 2       | 227   | 0     | 0.62   |
| WDC       | WD141KFGX-68FH9N0  | 14 TB  | 12      | 224   | 0     | 0.62   |
| HGST      | HUH721212ALE601    | 12 TB  | 49      | 221   | 0     | 0.61   |
| Apple     | HDD HTS541010A9... | 1 TB   | 1       | 220   | 0     | 0.60   |
| Samsung   | HD252HJ            | 250 GB | 1       | 2827  | 12    | 0.60   |
| Seagate   | ST2000DM008-2FR102 | 2 TB   | 74      | 249   | 32    | 0.59   |
| Toshiba   | MG04ACA200NY       | 2 TB   | 4       | 215   | 0     | 0.59   |
| Seagate   | ST3160812AS        | 160 GB | 1       | 211   | 0     | 0.58   |
| WDC       | WD8004FRYZ-01VAEB0 | 8 TB   | 27      | 210   | 0     | 0.58   |
| Seagate   | ST1000NX0423 00... | 1 TB   | 1       | 207   | 0     | 0.57   |
| WDC       | WD6003FRYZ-01F0DB0 | 6 TB   | 27      | 204   | 0     | 0.56   |
| WDC       | WD7500BPKT-00PK4T0 | 752 GB | 1       | 1623  | 7     | 0.56   |
| Toshiba   | MK1252GSX          | 120 GB | 1       | 199   | 0     | 0.55   |
| Seagate   | ST10000NM001G-2... | 10 TB  | 36      | 198   | 0     | 0.54   |
| Toshiba   | HDWG180            | 8 TB   | 8       | 197   | 0     | 0.54   |
| Seagate   | ST12000NM0128-2... | 12 TB  | 18      | 197   | 1     | 0.53   |
| WDC       | WD10JPVX-22JC3T0   | 1 TB   | 2       | 191   | 0     | 0.52   |
| HPE       | MB010000GWRTK      | 10 TB  | 24      | 190   | 0     | 0.52   |
| Toshiba   | MQ01ABB200         | 2 TB   | 1       | 1702  | 8     | 0.52   |
| HP        | MB1000EAMZE        | 1 TB   | 2       | 185   | 0     | 0.51   |
| WDC       | WD62PURZ-85B3AY0   | 6 TB   | 3       | 182   | 0     | 0.50   |
| WDC       | WUH721414ALE6L1    | 14 TB  | 8       | 169   | 0     | 0.46   |
| WDC       | WUH721414ALE6L4    | 14 TB  | 94      | 168   | 1     | 0.46   |
| Seagate   | ST3000NM0005-1V... | 3 TB   | 1       | 166   | 0     | 0.46   |
| Seagate   | ST1000NX0343       | 1 TB   | 2       | 1561  | 10    | 0.45   |
| Toshiba   | HDWL110            | 1 TB   | 1       | 163   | 0     | 0.45   |
| WDC       | WD5000AZLX-75K2TA0 | 500 GB | 1       | 162   | 0     | 0.44   |
| Toshiba   | MQ04UBF100         | 1 TB   | 4       | 161   | 0     | 0.44   |
| Seagate   | ST1000NX0303       | 1 TB   | 3       | 650   | 4     | 0.42   |
| Toshiba   | MQ01ACF032         | 320 GB | 1       | 151   | 0     | 0.41   |
| Maxtor    | STM3200827AS       | 200 GB | 1       | 1203  | 7     | 0.41   |
| WDC       | WD20SPZX-21UA7T0   | 2 TB   | 2       | 150   | 0     | 0.41   |
| WDC       | WD20NPVX-00EA4T0   | 2 TB   | 4       | 1441  | 15    | 0.40   |
| Toshiba   | HDWN180            | 8 TB   | 1       | 146   | 0     | 0.40   |
| WDC       | WD20SPZX-75UA7T1   | 2 TB   | 2       | 143   | 0     | 0.39   |
| Seagate   | ST4000NM002A-2H... | 4 TB   | 65      | 143   | 0     | 0.39   |
| WDC       | WD50EZRZ-00GZ5B1   | 5 TB   | 1       | 141   | 0     | 0.39   |
| Apple     | HDD HTS545050A7... | 500 GB | 1       | 141   | 0     | 0.39   |
| HP        | MB2000EBZQC        | 2 TB   | 1       | 1269  | 8     | 0.39   |
| WDC       | WD40NMZW-11GX6S1   | 4 TB   | 4       | 137   | 0     | 0.38   |
| Seagate   | ST250DM000-1BD141  | 250 GB | 3       | 750   | 44    | 0.37   |
| Seagate   | ST6000VN001-2BB186 | 6 TB   | 8       | 136   | 0     | 0.37   |
| Hitachi   | HDT725025VLA380... | 250 GB | 1       | 135   | 0     | 0.37   |
| Samsung   | HM641JI            | 640 GB | 1       | 132   | 0     | 0.36   |
| Seagate   | ST12000NM001G-2... | 12 TB  | 43      | 132   | 0     | 0.36   |
| Hitachi   | HDS721010DLE630    | 1 TB   | 8       | 1613  | 1436  | 0.34   |
| WDC       | WD20PURZ-85AKKY0   | 2 TB   | 1       | 121   | 0     | 0.33   |
| WDC       | WD2001FASS-00W2B0  | 2 TB   | 3       | 1796  | 15    | 0.33   |
| HP        | GB0750C4414        | 752 GB | 2       | 3668  | 152   | 0.33   |
| Seagate   | ST10000VN0008-2... | 10 TB  | 19      | 118   | 0     | 0.32   |
| Toshiba   | MQ01ABD100         | 1 TB   | 3       | 553   | 1093  | 0.32   |
| WDC       | WD10EURX-63C57Y0   | 1 TB   | 1       | 1027  | 8     | 0.31   |
| Seagate   | ST1000VM002-1CT162 | 1 TB   | 1       | 113   | 0     | 0.31   |
| Hitachi   | HTS725016A9A364    | 160 GB | 2       | 1326  | 507   | 0.30   |
| Toshiba   | MG06ACA800E        | 8 TB   | 86      | 108   | 0     | 0.30   |
| WDC       | WD30EFAX-68JH4N0   | 3 TB   | 2       | 249   | 4     | 0.30   |
| WDC       | WD10JPVT-00A1YT0   | 1 TB   | 1       | 952   | 8     | 0.29   |
| Toshiba   | HDWR180            | 8 TB   | 9       | 104   | 0     | 0.29   |
| WDC       | WD1003FBYX-20Y7B0  | 1 TB   | 2       | 102   | 0     | 0.28   |
| WDC       | WD2500AAKX-00ERMA0 | 250 GB | 2       | 507   | 4     | 0.28   |
| WDC       | WD40EFAX-68JH4N1   | 4 TB   | 4       | 100   | 0     | 0.28   |
| WDC       | WD20EZAZ-00L9GB0   | 2 TB   | 3       | 91    | 0     | 0.25   |
| WDC       | WD3200AVVS-56L2B0  | 320 GB | 4       | 91    | 0     | 0.25   |
| Toshiba   | MG08ACA16TEY       | 16 TB  | 4       | 90    | 0     | 0.25   |
| WDC       | WD1600AAJS-22L7A0  | 160 GB | 1       | 807   | 8     | 0.25   |
| WDC       | WD10SMZW-11Y0TS0   | 1 TB   | 2       | 88    | 0     | 0.24   |
| WDC       | WUH721818ALE604    | 18 TB  | 759     | 87    | 1     | 0.24   |
| Seagate   | ST8000NM012A-2K... | 8 TB   | 73      | 86    | 0     | 0.24   |
| HPE       | MB008000GWJRT      | 8 TB   | 4       | 83    | 0     | 0.23   |
| WDC       | WD20EFAX-68B2RN1   | 2 TB   | 4       | 82    | 0     | 0.23   |
| WDC       | WD1600BEKT-00F3T0  | 160 GB | 2       | 1098  | 146   | 0.22   |
| WDC       | WD2500BEKT-60A25T1 | 250 GB | 1       | 399   | 4     | 0.22   |
| WDC       | WD4000F9YZ-76N20L1 | 4 TB   | 1       | 76    | 0     | 0.21   |
| Seagate   | ST9500325AS        | 500 GB | 2       | 1360  | 69    | 0.20   |
| Seagate   | ST32000641AS       | 2 TB   | 1       | 2235  | 29    | 0.20   |
| Seagate   | ST9160412AS        | 160 GB | 1       | 73    | 0     | 0.20   |
| WDC       | WUH721818ALE6L4    | 18 TB  | 201     | 72    | 0     | 0.20   |
| WDC       | WD102KRYZ-01A5AB0  | 10 TB  | 1       | 70    | 0     | 0.19   |
| Toshiba   | MQ01UBD050         | 500 GB | 1       | 69    | 0     | 0.19   |
| Seagate   | ST980811AS         | 80 GB  | 1       | 348   | 4     | 0.19   |
| Toshiba   | MQ01UBD100         | 1 TB   | 2       | 142   | 8     | 0.19   |
| Toshiba   | MQ04ABF100         | 1 TB   | 2       | 267   | 1010  | 0.18   |
| Seagate   | ST2000NC001-1DY164 | 2 TB   | 1       | 1641  | 24    | 0.18   |
| WDC       | WD5000LPLX-60ZNTT2 | 500 GB | 1       | 63    | 0     | 0.17   |
| Toshiba   | HDWD240            | 4 TB   | 3       | 61    | 0     | 0.17   |
| WDC       | WD50NDZW-11MR8S1   | 5 TB   | 2       | 60    | 0     | 0.17   |
| Seagate   | ST1000LM035-1RK172 | 1 TB   | 1       | 60    | 0     | 0.16   |
| Hitachi   | HTS725050A9A362    | 500 GB | 1       | 868   | 14    | 0.16   |
| HGST      | HUH728080ALN600    | 8 TB   | 2       | 96    | 2     | 0.16   |
| WDC       | WD102PURZ-85BXPY0  | 10 TB  | 10      | 56    | 0     | 0.15   |
| Hitachi   | HTS545016B9A300    | 160 GB | 1       | 55    | 0     | 0.15   |
| Hitachi   | HTS545025B9A300    | 250 GB | 1       | 219   | 3     | 0.15   |
| WDC       | WD20NMVW-11EDZS7   | 2 TB   | 1       | 54    | 0     | 0.15   |
| WDC       | WD60EFAX-68JH4N1   | 6 TB   | 2       | 51    | 0     | 0.14   |
| HPE       | MB006000GWKGR      | 6 TB   | 1       | 51    | 0     | 0.14   |
| WDC       | WD5000AAKS-00TMA0  | 500 GB | 1       | 49    | 0     | 0.14   |
| Seagate   | ST12000NM003G-2... | 12 TB  | 4       | 49    | 0     | 0.14   |
| WDC       | WD80EFBX-68AZZN0   | 8 TB   | 3       | 46    | 0     | 0.13   |
| WDC       | WD10SPZX-21Z10T0   | 1 TB   | 2       | 44    | 0     | 0.12   |
| Seagate   | ST31000340NS       | 1 TB   | 3       | 730   | 354   | 0.12   |
| WDC       | WD20EZRX-19DC0B0   | 2 TB   | 1       | 1541  | 35    | 0.12   |
| Seagate   | ST3000DM001-9YN166 | 3 TB   | 6       | 1993  | 1022  | 0.12   |
| Seagate   | ST8000VN0002-1Z... | 8 TB   | 1       | 1522  | 35    | 0.12   |
| WDC       | WD80EMAZ-00WJTA0   | 8 TB   | 1       | 38    | 0     | 0.11   |
| Seagate   | ST3640323AS        | 640 GB | 1       | 960   | 24    | 0.11   |
| WDC       | WUH721816ALE6L4    | 16 TB  | 20      | 36    | 0     | 0.10   |
| HPE       | MB006000GWWQT      | 6 TB   | 1       | 36    | 0     | 0.10   |
| Hitachi   | HTS721060G9SA00    | 64 GB  | 1       | 2330  | 66    | 0.10   |
| HP        | VB0160EAVEQ        | 160 GB | 1       | 513   | 14    | 0.09   |
| WDC       | WD40PURZ-85TTDY0   | 4 TB   | 2       | 33    | 0     | 0.09   |
| Toshiba   | MG08ACA16TE        | 16 TB  | 6       | 33    | 0     | 0.09   |
| Toshiba   | HDWF180            | 8 TB   | 2       | 33    | 0     | 0.09   |
| Seagate   | ST5000NM0024-1H... | 5 TB   | 1       | 1636  | 49    | 0.09   |
| WDC       | WD2502ABYS-02B7A0  | 256 GB | 1       | 2202  | 67    | 0.09   |
| Seagate   | ST16000NM003G-2... | 16 TB  | 4       | 30    | 0     | 0.08   |
| WDC       | WD20EFAX-68FB5N0   | 2 TB   | 4       | 30    | 0     | 0.08   |
| WDC       | WD4000FDYZ-27YA5B0 | 4 TB   | 5       | 711   | 115   | 0.08   |
| WDC       | WD10EZEX-00BBHA0   | 1 TB   | 2       | 28    | 0     | 0.08   |
| Apple     | HDD ST1000DM003    | 1 TB   | 1       | 254   | 8     | 0.08   |
| HGST      | HTS541050A9E680    | 500 GB | 1       | 26    | 0     | 0.07   |
| Seagate   | ST6000NM002A-2K... | 6 TB   | 1       | 25    | 0     | 0.07   |
| Toshiba   | MG08ACA16TA        | 16 TB  | 35      | 23    | 0     | 0.07   |
| HP        | MB1000EBZQB        | 1 TB   | 1       | 2073  | 86    | 0.07   |
| Toshiba   | MK5061GSY          | 500 GB | 1       | 22    | 0     | 0.06   |
| HP        | MB1000EBNCF        | 1 TB   | 1       | 1398  | 73    | 0.05   |
| Maxtor    | STM3160215AS       | 160 GB | 1       | 2646  | 142   | 0.05   |
| Seagate   | ST16000NM005G-2... | 16 TB  | 6       | 18    | 0     | 0.05   |
| Seagate   | ST4000VX007-2DT166 | 4 TB   | 1       | 17    | 0     | 0.05   |
| WDC       | WD3200AAKS-00L9A0  | 320 GB | 1       | 3625  | 242   | 0.04   |
| WDC       | WD60EZRX-00MVLB1   | 6 TB   | 1       | 1373  | 105   | 0.04   |
| Toshiba   | HDWR11A            | 10 TB  | 1       | 11    | 0     | 0.03   |
| Seagate   | ST16000VN001-2R... | 16 TB  | 1       | 10    | 0     | 0.03   |
| WDC       | WD20SMZW-11JW8S1   | 2 TB   | 1       | 10    | 0     | 0.03   |
| Seagate   | ST1000NC000-1CX162 | 1 TB   | 1       | 2229  | 224   | 0.03   |
| Seagate   | ST3160813AS        | 160 GB | 1       | 1350  | 139   | 0.03   |
| Hitachi   | HDT722525DLA380    | 250 GB | 1       | 3645  | 398   | 0.03   |
| WDC       | WD5000AZLX-60K2TA0 | 500 GB | 2       | 8     | 0     | 0.02   |
| HGST      | HTS725050B7E630    | 500 GB | 4       | 8     | 0     | 0.02   |
| Seagate   | ST18000NE000-2Y... | 18 TB  | 1       | 8     | 0     | 0.02   |
| WDC       | WD10EVVS-63M5B0    | 1 TB   | 1       | 73    | 8     | 0.02   |
| Seagate   | ST9320325AS        | 320 GB | 1       | 348   | 42    | 0.02   |
| Hitachi   | HDS721010CLA632    | 1 TB   | 1       | 960   | 118   | 0.02   |
| Toshiba   | MG08ADA800E        | 8 TB   | 5       | 7     | 0     | 0.02   |
| Seagate   | ST16000NM001J-2... | 16 TB  | 1       | 6     | 0     | 0.02   |
| WDC       | WD40PURX-64GVNY0   | 4 TB   | 1       | 6     | 0     | 0.02   |
| Seagate   | ST2000DM001-1E6164 | 2 TB   | 1       | 6     | 0     | 0.02   |
| Samsung   | HD502HI            | 500 GB | 3       | 3726  | 807   | 0.02   |
| WDC       | WD20SMZW-11JW8S0   | 2 TB   | 1       | 5     | 0     | 0.01   |
| Seagate   | ST3750330NS        | 752 GB | 1       | 170   | 36    | 0.01   |
| Seagate   | ST31500341AS       | 1.5 TB | 1       | 2761  | 670   | 0.01   |
| WDC       | WD101EFBX-68B0AN0  | 10 TB  | 1       | 3     | 0     | 0.01   |
| Seagate   | ST3160211AS        | 160 GB | 1       | 1802  | 497   | 0.01   |
| Seagate   | ST1000VM002-1ET162 | 1 TB   | 1       | 113   | 32    | 0.01   |
| Samsung   | HD322HJ            | 320 GB | 1       | 3471  | 1015  | 0.01   |
| HGST      | HTS541010A9E680    | 1 TB   | 1       | 888   | 259   | 0.01   |
| HP        | GJ0250EAGSQ        | 250 GB | 2       | 3080  | 1028  | 0.01   |
| Seagate   | ST31000524NS 43... | 1 TB   | 1       | 2718  | 1138  | 0.01   |
| Hitachi   | HDS721010CLA630    | 1 TB   | 2       | 1527  | 699   | 0.01   |
| Seagate   | ST3000VN007-2AH16M | 3 TB   | 3       | 1     | 0     | 0.01   |
| WDC       | WD1600JS-75NCB2    | 160 GB | 1       | 2854  | 1502  | 0.01   |
| MediaMax  | WL4000GSA6472E     | 4.9 TB | 1       | 1617  | 1054  | 0.00   |
| WDC       | WD5000AADS-00M2B0  | 500 GB | 1       | 2968  | 2023  | 0.00   |
| Seagate   | ST500LT012-9WS142  | 500 GB | 3       | 1335  | 1067  | 0.00   |
| HGST      | HTS541075A9E680    | 752 GB | 1       | 1224  | 1027  | 0.00   |
| Samsung   | SP2004C            | 200 GB | 1       | 1212  | 1105  | 0.00   |
| WDC       | WD2500BEVS-60UST0  | 250 GB | 1       | 400   | 413   | 0.00   |
| Seagate   | ST4000DM000-2AE166 | 4 TB   | 1       | 998   | 1060  | 0.00   |
| Seagate   | ST9120817AS        | 120 GB | 1       | 3408  | 3854  | 0.00   |
| HGST      | HTS725032A7E630    | 320 GB | 1       | 461   | 538   | 0.00   |
| WDC       | WD2003FYPS-27Y2B0  | 2 TB   | 1       | 1747  | 2073  | 0.00   |
| Hitachi   | HDS5C3020ALA632    | 2 TB   | 1       | 1638  | 2016  | 0.00   |
| Seagate   | ST3200827AS        | 200 GB | 1       | 1371  | 2268  | 0.00   |
| WDC       | WD10JMVW-11S5XS0   | 1 TB   | 1       | 540   | 1016  | 0.00   |
| HGST      | HTS545050A7E680    | 500 GB | 1       | 362   | 1023  | 0.00   |
| Seagate   | ST3250820AS        | 250 GB | 1       | 957   | 3053  | 0.00   |
| WDC       | WD2500AAKX-001CA0  | 250 GB | 1       | 449   | 1509  | 0.00   |
| Seagate   | ST3500320NS        | 500 GB | 2       | 585   | 2573  | 0.00   |
| Seagate   | ST9160821AS        | 160 GB | 1       | 262   | 1011  | 0.00   |
| Seagate   | ST1000LM010-9YH146 | 1 TB   | 1       | 61    | 1087  | 0.00   |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
