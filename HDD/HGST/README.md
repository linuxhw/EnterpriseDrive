HGST Hard Drives
================

This is a list of all tested HGST hard drive models and their MTBFs. See more
info on reliability test in the [README](https://github.com/linuxhw/EnterpriseDrive).

Contents
--------

1. [ HDD by Model  ](#hdd-by-model)
2. [ HDD by Family ](#hdd-by-family)

HDD by Model
------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days — avg. days per sample,
Err  — avg. errors per sample,
MTBF — avg. MTBF in years per sample.

| MFG       | Model              | Size   | Samples | Days  | Err   | MTBF   |
|-----------|--------------------|--------|---------|-------|-------|--------|
| HGST      | HUS724020ALE640    | 2 TB   | 1       | 2280  | 0     | 6.25   |
| HGST      | HDS724040ALE640    | 4 TB   | 6       | 2140  | 0     | 5.86   |
| HGST      | HUS724020ALA640    | 2 TB   | 205     | 1921  | 10    | 5.08   |
| HGST      | HUS726060ALE614    | 6 TB   | 147     | 1949  | 11    | 4.94   |
| HGST      | HMS5C4040BLE640    | 4 TB   | 17      | 1760  | 15    | 4.59   |
| HGST      | HDN724040ALE640    | 4 TB   | 22      | 1721  | 92    | 4.44   |
| HGST      | HUS726030ALA610    | 3 TB   | 47      | 1529  | 1     | 4.13   |
| HGST      | HUS724040ALA640    | 4 TB   | 139     | 1524  | 2     | 3.95   |
| HGST      | HTE541010A9E680    | 1 TB   | 6       | 1414  | 0     | 3.87   |
| HGST      | HUS724030ALA640    | 3 TB   | 37      | 1636  | 71    | 3.82   |
| HGST      | HUS724040ALE640    | 4 TB   | 22      | 1390  | 0     | 3.81   |
| HGST      | HUH728080ALE600    | 8 TB   | 20      | 1382  | 0     | 3.79   |
| HGST      | HDN724030ALE640    | 3 TB   | 7       | 1299  | 0     | 3.56   |
| HGST      | HDN726060ALE614    | 6 TB   | 2       | 1289  | 0     | 3.53   |
| HGST      | HDN726040ALE614    | 4 TB   | 23      | 1287  | 0     | 3.53   |
| HGST      | HUS726060ALE610    | 6 TB   | 23      | 1203  | 0     | 3.30   |
| HGST      | HDN721010ALE604    | 10 TB  | 2       | 1188  | 0     | 3.26   |
| HGST      | HUS726020ALE614    | 2 TB   | 78      | 1212  | 1     | 3.24   |
| HGST      | HUS722T1TALA600    | 1 TB   | 29      | 1154  | 0     | 3.16   |
| HGST      | HUS726020ALE610    | 2 TB   | 11      | 1145  | 0     | 3.14   |
| HGST      | HUH728060ALE600    | 6 TB   | 3       | 1136  | 0     | 3.11   |
| HGST      | HUS726040ALA614    | 4 TB   | 19      | 1193  | 4     | 2.92   |
| HGST      | HUH721008ALE601    | 8 TB   | 6       | 1050  | 0     | 2.88   |
| HGST      | HUS726040ALE610    | 4 TB   | 23      | 990   | 0     | 2.71   |
| HGST      | HUS726040ALE614    | 4 TB   | 13      | 1068  | 3     | 2.71   |
| HGST      | HUH728080ALE604    | 8 TB   | 17      | 1020  | 10    | 2.69   |
| HGST      | HTE721010A9E630    | 1 TB   | 2       | 1767  | 8     | 2.59   |
| HGST      | HUS722T1TALA604    | 1 TB   | 124     | 923   | 10    | 2.50   |
| HGST      | HUH721212ALN600    | 12 TB  | 11      | 1003  | 17    | 2.45   |
| HGST      | HUH721010ALN600    | 10 TB  | 16      | 882   | 0     | 2.42   |
| HGST      | HUS726020ALA610    | 2 TB   | 133     | 936   | 19    | 2.39   |
| HGST      | HUH721010ALE604    | 10 TB  | 111     | 869   | 0     | 2.38   |
| HGST      | HUS726040ALA610    | 4 TB   | 68      | 863   | 1     | 2.29   |
| HGST      | HUH728060ALE604    | 6 TB   | 14      | 1198  | 20    | 2.18   |
| HGST      | HUS726060ALA640    | 6 TB   | 46      | 779   | 1     | 2.11   |
| HGST      | HTS721010A9E630    | 1 TB   | 5       | 1455  | 605   | 2.07   |
| HGST      | HUH721008ALE600    | 8 TB   | 46      | 752   | 0     | 2.06   |
| HGST      | HUH721212ALN604    | 12 TB  | 2050    | 770   | 2     | 2.03   |
| HGST      | HUS726T6TALE6L4    | 6 TB   | 191     | 710   | 22    | 1.91   |
| HGST      | HUS726T4TALN6L4    | 4 TB   | 20      | 688   | 0     | 1.89   |
| HGST      | HUS722T2TALA604    | 2 TB   | 84      | 621   | 1     | 1.66   |
| HGST      | HUH721212ALE604    | 12 TB  | 175     | 598   | 1     | 1.62   |
| HGST      | HUH721212ALE600    | 12 TB  | 64      | 559   | 0     | 1.53   |
| HGST      | HUH721010ALE600    | 10 TB  | 219     | 539   | 1     | 1.47   |
| HGST      | HUS726T4TALE6L4    | 4 TB   | 62      | 531   | 0     | 1.46   |
| HGST      | HTS725050A7E630    | 500 GB | 3       | 1123  | 1015  | 1.46   |
| HGST      | HUS728T8TALE6L4    | 8 TB   | 49      | 507   | 1     | 1.38   |
| HGST      | HDN728080ALE604    | 8 TB   | 2       | 766   | 16    | 1.36   |
| HGST      | HUH721008ALE604    | 8 TB   | 22      | 445   | 0     | 1.22   |
| HGST      | HUS726T6TALE6L1    | 6 TB   | 12      | 436   | 0     | 1.20   |
| HGST      | HUS726T4TALA6L4    | 4 TB   | 137     | 415   | 5     | 1.13   |
| HGST      | HUS728T8TALE6L0    | 8 TB   | 15      | 370   | 0     | 1.01   |
| HGST      | HUS726T4TALA6L1    | 4 TB   | 79      | 340   | 1     | 0.91   |
| HGST      | HUH721212ALE601    | 12 TB  | 49      | 221   | 0     | 0.61   |
| HGST      | HUH728080ALN600    | 8 TB   | 2       | 96    | 2     | 0.16   |
| HGST      | HTS541050A9E680    | 500 GB | 1       | 26    | 0     | 0.07   |
| HGST      | HTS725050B7E630    | 500 GB | 4       | 8     | 0     | 0.02   |
| HGST      | HTS541010A9E680    | 1 TB   | 1       | 888   | 259   | 0.01   |
| HGST      | HTS541075A9E680    | 752 GB | 1       | 1224  | 1027  | 0.00   |
| HGST      | HTS725032A7E630    | 320 GB | 1       | 461   | 538   | 0.00   |
| HGST      | HTS545050A7E680    | 500 GB | 1       | 362   | 1023  | 0.00   |

HDD by Family
-------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days — avg. days per sample,
Err  — avg. errors per sample,
MTBF — avg. MTBF in years per sample.

| MFG       | Family                 | Models | Samples | Days  | Err   | MTBF   |
|-----------|------------------------|--------|---------|-------|-------|--------|
| HGST      | Deskstar 7K4000        | 1      | 6       | 2140  | 0     | 5.86   |
| HGST      | MegaScale 4000         | 1      | 17      | 1760  | 15    | 4.59   |
| HGST      | Ultrastar 7K4000       | 5      | 404     | 1730  | 13    | 4.51   |
| HGST      | Deskstar NAS           | 6      | 58      | 1432  | 36    | 3.79   |
| HGST      | Ultrastar 7K6000       | 11     | 582     | 1288  | 8     | 3.35   |
| HGST      | Ultrastar He8          | 5      | 56      | 1167  | 8     | 2.89   |
| HGST      | Travelstar 5K1000      | 4      | 9       | 1180  | 143   | 2.59   |
| HGST      | Ultrastar 7K2          | 3      | 237     | 844   | 6     | 2.28   |
| HGST      | Travelstar 7K1000      | 2      | 7       | 1544  | 434   | 2.22   |
| HGST      | Ultrastar He6          | 1      | 46      | 779   | 1     | 2.11   |
| HGST      | Ultrastar DC HC520 ... | 5      | 2349    | 741   | 2     | 1.96   |
| HGST      | Ultrastar He10         | 6      | 420     | 665   | 1     | 1.82   |
| HGST      | Ultrastar HC310/320    | 3      | 302     | 641   | 14    | 1.73   |
| HGST      | Travelstar Z7K500      | 2      | 4       | 958   | 896   | 1.09   |
| HGST      | Ultrastar DC HC310     | 3      | 228     | 390   | 3     | 1.06   |
| HGST      | Ultrastar DC HC320     | 1      | 15      | 370   | 0     | 1.01   |
| HGST      | Travelstar Z7K500.B    | 1      | 4       | 8     | 0     | 0.02   |
| HGST      | Travelstar Z5K500      | 1      | 1       | 362   | 1023  | 0.00   |
