Seagate Hard Drives
===================

This is a list of all tested Seagate hard drive models and their MTBFs. See more
info on reliability test in the [README](https://github.com/linuxhw/EnterpriseDrive).

Contents
--------

1. [ HDD by Model  ](#hdd-by-model)
2. [ HDD by Family ](#hdd-by-family)

HDD by Model
------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days — avg. days per sample,
Err  — avg. errors per sample,
MTBF — avg. MTBF in years per sample.

| MFG       | Model              | Size   | Samples | Days  | Err   | MTBF   |
|-----------|--------------------|--------|---------|-------|-------|--------|
| Seagate   | ST32500NSSUN250G   | 250 GB | 2       | 3676  | 0     | 10.07  |
| Seagate   | ST3320620AS        | 320 GB | 3       | 4439  | 5     | 8.48   |
| Seagate   | ST32000645NS       | 2 TB   | 88      | 3111  | 15    | 7.89   |
| Seagate   | ST3320620A         | 320 GB | 2       | 2728  | 0     | 7.47   |
| Seagate   | ST3250620NS        | 250 GB | 4       | 2532  | 0     | 6.94   |
| Seagate   | ST2000VX000-1CU164 | 2 TB   | 1       | 2477  | 0     | 6.79   |
| Seagate   | ST32000646NS       | 2 TB   | 2       | 2424  | 0     | 6.64   |
| Seagate   | ST3250410AS        | 250 GB | 4       | 3449  | 85    | 6.55   |
| Seagate   | ST4000VN000-1H4168 | 4 TB   | 30      | 2342  | 1     | 6.17   |
| Seagate   | ST1000NC001-1DY162 | 1 TB   | 3       | 2222  | 0     | 6.09   |
| Seagate   | ST3000VX000-1CU166 | 3 TB   | 26      | 2459  | 4     | 5.82   |
| Seagate   | ST3320620NS        | 320 GB | 4       | 2189  | 1     | 5.81   |
| Seagate   | ST2000NC000-1CX164 | 2 TB   | 1       | 2101  | 0     | 5.76   |
| Seagate   | ST3000VX006-1HH166 | 3 TB   | 2       | 2069  | 0     | 5.67   |
| Seagate   | ST2000VN000-1HJ164 | 2 TB   | 2       | 2067  | 0     | 5.67   |
| Seagate   | ST3400620AS        | 400 GB | 1       | 2060  | 0     | 5.65   |
| Seagate   | ST2000VX000-1ES164 | 2 TB   | 1       | 2043  | 0     | 5.60   |
| Seagate   | ST2000NM0033       | 2 TB   | 1       | 1988  | 0     | 5.45   |
| Seagate   | ST500DM002-1BC142  | 500 GB | 2       | 1929  | 0     | 5.29   |
| Seagate   | ST6000DM001-1XY17Z | 6 TB   | 4       | 1888  | 0     | 5.17   |
| Seagate   | ST4000VN0001-1S... | 4 TB   | 9       | 2087  | 1     | 5.14   |
| Seagate   | ST91000640NS       | 1 TB   | 33      | 1990  | 1     | 5.14   |
| Seagate   | ST4000NM0033-9Z... | 4 TB   | 437     | 2259  | 106   | 5.09   |
| Seagate   | ST3500630AS        | 500 GB | 1       | 1720  | 0     | 4.71   |
| Seagate   | ST3000DM001-1CH166 | 3 TB   | 20      | 1939  | 20    | 4.67   |
| Seagate   | ST500NM0011 00W... | 500 GB | 2       | 1647  | 0     | 4.51   |
| Seagate   | ST2000NM0024-1H... | 2 TB   | 9       | 1645  | 0     | 4.51   |
| Seagate   | ST3500630NS        | 500 GB | 9       | 1643  | 0     | 4.50   |
| Seagate   | ST33000651AS       | 3 TB   | 4       | 2329  | 10    | 4.44   |
| Seagate   | ST9500620NS        | 500 GB | 27      | 1811  | 1     | 4.41   |
| Seagate   | ST6000NM0024-1H... | 6 TB   | 1675    | 1889  | 18    | 4.38   |
| Seagate   | ST3000NM0033-9Z... | 3 TB   | 23      | 2185  | 67    | 4.34   |
| Seagate   | ST3000VX000-1ES166 | 3 TB   | 3       | 1583  | 0     | 4.34   |
| Seagate   | ST3000NC002-1DY166 | 3 TB   | 1       | 1518  | 0     | 4.16   |
| Seagate   | ST1000DM003-1ER162 | 1 TB   | 30      | 1508  | 0     | 4.13   |
| Seagate   | ST6000VX0001-1S... | 6 TB   | 2       | 1493  | 0     | 4.09   |
| Seagate   | ST2000DM001-1ER164 | 2 TB   | 29      | 1695  | 211   | 4.09   |
| Seagate   | ST1000VX000-1CU162 | 1 TB   | 3       | 1485  | 0     | 4.07   |
| Seagate   | ST320DM000-1BC14C  | 320 GB | 2       | 1479  | 0     | 4.05   |
| Seagate   | ST1000NM0033-9Z... | 1 TB   | 136     | 1741  | 83    | 4.03   |
| Seagate   | ST33000650NS       | 3 TB   | 19      | 2306  | 68    | 3.97   |
| Seagate   | ST1000DM003-1CH162 | 1 TB   | 17      | 1519  | 219   | 3.81   |
| Seagate   | ST9250610NS        | 250 GB | 8       | 1375  | 0     | 3.77   |
| Seagate   | ST4000NM0024-1H... | 4 TB   | 18      | 1585  | 88    | 3.61   |
| Seagate   | ST1000VX000-1ES162 | 1 TB   | 2       | 1315  | 0     | 3.60   |
| Seagate   | ST2000NM0033-9Z... | 2 TB   | 95      | 1621  | 104   | 3.52   |
| Seagate   | ST6000VN0041-2E... | 6 TB   | 10      | 1374  | 1     | 3.50   |
| Seagate   | ST2000DM001-1CH164 | 2 TB   | 17      | 1781  | 428   | 3.46   |
| Seagate   | ST1000NM0011       | 1 TB   | 27      | 1693  | 81    | 3.42   |
| Seagate   | ST8000AS0002-1N... | 8 TB   | 19      | 1405  | 56    | 3.34   |
| Seagate   | ST8000NM0055-1R... | 8 TB   | 928     | 1313  | 20    | 3.32   |
| Seagate   | ST4000DM000-1F2168 | 4 TB   | 15      | 1304  | 150   | 3.28   |
| Seagate   | ST8000DM005-2EH112 | 8 TB   | 215     | 1259  | 1     | 3.27   |
| Seagate   | ST6000VX0023-2E... | 6 TB   | 17      | 1187  | 0     | 3.25   |
| Seagate   | ST2000LM003 HN-... | 2 TB   | 16      | 1315  | 1     | 3.24   |
| Seagate   | ST2000VN0001-1S... | 2 TB   | 4       | 1167  | 0     | 3.20   |
| Seagate   | ST500NM0011        | 500 GB | 9       | 1964  | 5     | 3.19   |
| Seagate   | ST4000VX000-1F4168 | 4 TB   | 4       | 1150  | 0     | 3.15   |
| Seagate   | ST8000DM002-1YW112 | 8 TB   | 135     | 1214  | 2     | 3.15   |
| Seagate   | ST1000NM0018-2F... | 1 TB   | 36      | 1168  | 1     | 3.14   |
| Seagate   | ST3160318AS        | 160 GB | 1       | 2266  | 1     | 3.11   |
| Seagate   | ST1000DM003-9YN162 | 1 TB   | 3       | 2428  | 732   | 2.95   |
| Seagate   | ST500DM002-1BD142  | 500 GB | 35      | 1539  | 57    | 2.91   |
| Seagate   | ST8000NM0205-2F... | 8 TB   | 31      | 1151  | 66    | 2.89   |
| Seagate   | ST3250310AS        | 250 GB | 3       | 2192  | 209   | 2.86   |
| Seagate   | ST4000DX001-1CE168 | 4 TB   | 2       | 1038  | 0     | 2.85   |
| Seagate   | ST1000NX0423       | 1 TB   | 1       | 997   | 0     | 2.73   |
| Seagate   | ST3000DM001-1ER166 | 3 TB   | 16      | 1437  | 459   | 2.71   |
| Seagate   | ST6000NM0235-2A... | 6 TB   | 9       | 984   | 0     | 2.70   |
| Seagate   | ST3500413AS        | 500 GB | 3       | 1621  | 22    | 2.68   |
| Seagate   | ST750LX003-1AC154  | 752 GB | 1       | 2921  | 2     | 2.67   |
| Seagate   | ST1000DM003-1SB10C | 1 TB   | 6       | 1230  | 1     | 2.66   |
| Seagate   | ST2000DM001-9YN164 | 2 TB   | 4       | 2324  | 260   | 2.59   |
| Seagate   | ST380815AS         | 80 GB  | 5       | 944   | 0     | 2.59   |
| Seagate   | ST6000VN0033-2E... | 6 TB   | 3       | 934   | 0     | 2.56   |
| Seagate   | ST1000LM048-2E7172 | 1 TB   | 10      | 924   | 0     | 2.53   |
| Seagate   | ST1000DX001-1NS162 | 1 TB   | 6       | 1689  | 219   | 2.53   |
| Seagate   | ST32000644NS       | 2 TB   | 11      | 1525  | 11    | 2.51   |
| Seagate   | ST1000NM0055-1V... | 1 TB   | 31      | 1019  | 2     | 2.45   |
| Seagate   | ST12000VN0007-2... | 12 TB  | 8       | 871   | 0     | 2.39   |
| Seagate   | ST10000NM0016-1... | 10 TB  | 1507    | 1335  | 66    | 2.36   |
| Seagate   | ST1000DM005 HD1... | 1 TB   | 4       | 1986  | 4     | 2.35   |
| Seagate   | ST12000NM0007-2... | 12 TB  | 140     | 870   | 7     | 2.33   |
| Seagate   | ST3400620NS        | 400 GB | 1       | 1695  | 1     | 2.32   |
| Seagate   | ST3750640AS        | 752 GB | 1       | 2504  | 2     | 2.29   |
| Seagate   | ST1000VN002-2EY102 | 1 TB   | 4       | 817   | 0     | 2.24   |
| Seagate   | ST3250318AS        | 250 GB | 3       | 1375  | 17    | 2.22   |
| Seagate   | ST2000DM006-2DM164 | 2 TB   | 40      | 840   | 27    | 2.21   |
| Seagate   | ST2000NM0125-1Y... | 2 TB   | 13      | 800   | 0     | 2.19   |
| Seagate   | ST500DM009-2F110A  | 500 GB | 1       | 799   | 0     | 2.19   |
| Seagate   | ST3120022A         | 120 GB | 1       | 3197  | 3     | 2.19   |
| Seagate   | ST10000NM0196-2... | 10 TB  | 198     | 869   | 37    | 2.17   |
| Seagate   | ST31000NSSUN1.0T   | 1 TB   | 1       | 1574  | 1     | 2.16   |
| Seagate   | ST3500418AS        | 500 GB | 7       | 789   | 2     | 2.15   |
| Seagate   | ST3500312CS        | 500 GB | 10      | 964   | 2     | 2.10   |
| Seagate   | ST6000NM0115-1Y... | 6 TB   | 127     | 791   | 14    | 1.99   |
| Seagate   | ST4000NC001-1FS168 | 4 TB   | 6       | 722   | 0     | 1.98   |
| Seagate   | ST500DM005 HD502HJ | 500 GB | 1       | 716   | 0     | 1.96   |
| Seagate   | ST2000NM0018-2F... | 2 TB   | 1       | 716   | 0     | 1.96   |
| Seagate   | ST6000DM003-2CY186 | 6 TB   | 3       | 715   | 0     | 1.96   |
| Seagate   | ST12000DM0007-2... | 12 TB  | 5       | 712   | 0     | 1.95   |
| Seagate   | ST2000LM007-1R8174 | 2 TB   | 38      | 963   | 210   | 1.93   |
| Seagate   | ST33000651NS       | 3 TB   | 1       | 704   | 0     | 1.93   |
| Seagate   | ST1000LM024 HN-... | 1 TB   | 3       | 1027  | 4     | 1.92   |
| Seagate   | ST10000DM0004-2... | 10 TB  | 7       | 990   | 87    | 1.92   |
| Seagate   | ST8000VN0022-2E... | 8 TB   | 27      | 697   | 39    | 1.91   |
| Seagate   | ST4000NM0035-1V... | 4 TB   | 173     | 715   | 6     | 1.91   |
| Seagate   | ST1000DM003-1SB102 | 1 TB   | 7       | 970   | 21    | 1.88   |
| Seagate   | ST1000NM0008-2F... | 1 TB   | 57      | 683   | 0     | 1.87   |
| Seagate   | ST10000NE0004-1... | 10 TB  | 15      | 1217  | 373   | 1.86   |
| Seagate   | ST2000NM0008-2F... | 2 TB   | 73      | 696   | 72    | 1.85   |
| Seagate   | ST1000VX005-2EZ102 | 1 TB   | 3       | 672   | 0     | 1.84   |
| Seagate   | ST10000NM0086-2... | 10 TB  | 101     | 734   | 12    | 1.84   |
| Seagate   | ST10000NM0156-2... | 10 TB  | 37      | 743   | 6     | 1.83   |
| Seagate   | ST2000NM0055-1V... | 2 TB   | 21      | 900   | 2     | 1.79   |
| Seagate   | ST14000VN0008-2... | 14 TB  | 12      | 653   | 0     | 1.79   |
| Seagate   | ST2000VX003-1HH164 | 2 TB   | 3       | 652   | 0     | 1.79   |
| Seagate   | ST10000VN0004-1... | 10 TB  | 30      | 808   | 110   | 1.78   |
| Seagate   | ST2000NX0253       | 2 TB   | 49      | 832   | 16    | 1.77   |
| Seagate   | ST2000DL003-9VT166 | 2 TB   | 6       | 1354  | 366   | 1.75   |
| Seagate   | ST4000LM024-2AN17V | 4 TB   | 47      | 665   | 18    | 1.73   |
| Seagate   | ST12000NM0017-2... | 12 TB  | 2       | 621   | 0     | 1.70   |
| Seagate   | ST4000LM016-1N2170 | 4 TB   | 3       | 609   | 0     | 1.67   |
| Seagate   | ST4000NM0245-1Z... | 4 TB   | 29      | 841   | 22    | 1.66   |
| Seagate   | ST3000VX010-2E3166 | 3 TB   | 5       | 595   | 0     | 1.63   |
| Seagate   | ST6000VX001-2BD186 | 6 TB   | 4       | 595   | 0     | 1.63   |
| Seagate   | ST2000LM015-2E8174 | 2 TB   | 15      | 717   | 133   | 1.62   |
| Seagate   | ST2000NX0403       | 2 TB   | 7       | 611   | 1     | 1.60   |
| Seagate   | ST2000NX0423       | 2 TB   | 60      | 587   | 1     | 1.59   |
| Seagate   | ST8000VX0022-2E... | 8 TB   | 4       | 758   | 9     | 1.52   |
| Seagate   | ST3000DM007-1WY10G | 3 TB   | 3       | 549   | 0     | 1.50   |
| Seagate   | ST8000DM0004-1Z... | 8 TB   | 10      | 956   | 317   | 1.46   |
| Seagate   | ST1000DL002-9TT153 | 1 TB   | 2       | 877   | 509   | 1.45   |
| Seagate   | ST2000NM0011       | 2 TB   | 14      | 1590  | 143   | 1.43   |
| Seagate   | ST12000NM0008-2... | 12 TB  | 163     | 521   | 4     | 1.41   |
| Seagate   | ST16000NE000-2R... | 16 TB  | 2       | 511   | 0     | 1.40   |
| Seagate   | ST1000NM0011 81... | 1 TB   | 1       | 2394  | 4     | 1.31   |
| Seagate   | ST3750640NS        | 752 GB | 13      | 1262  | 802   | 1.29   |
| Seagate   | ST10000NM0568-2... | 10 TB  | 32      | 468   | 0     | 1.28   |
| Seagate   | ST2000DM005-2CW102 | 2 TB   | 1       | 459   | 0     | 1.26   |
| Seagate   | ST3500514NS        | 500 GB | 4       | 1140  | 10    | 1.26   |
| Seagate   | ST1000NX0313       | 1 TB   | 32      | 1167  | 23    | 1.25   |
| Seagate   | ST4000NM0115-1Y... | 4 TB   | 87      | 453   | 1     | 1.22   |
| Seagate   | ST10000NM0146-2... | 10 TB  | 1       | 441   | 0     | 1.21   |
| Seagate   | ST31000524AS       | 1 TB   | 6       | 1739  | 759   | 1.20   |
| Seagate   | ST2000DX002-2DV164 | 2 TB   | 4       | 424   | 0     | 1.16   |
| Seagate   | ST1000DM010-2EP102 | 1 TB   | 63      | 463   | 22    | 1.16   |
| Seagate   | ST10000VE0008-2... | 10 TB  | 4       | 422   | 0     | 1.16   |
| Seagate   | ST3250312AS        | 250 GB | 3       | 777   | 4     | 1.14   |
| Seagate   | ST8000DM004-2CX188 | 8 TB   | 20      | 496   | 18    | 1.12   |
| Seagate   | ST10000VN0008-2... | 10 TB  | 25      | 400   | 0     | 1.10   |
| Seagate   | ST1000LM049-2GH172 | 1 TB   | 4       | 398   | 0     | 1.09   |
| Seagate   | ST6000NM021A-2R... | 6 TB   | 84      | 401   | 13    | 1.05   |
| Seagate   | ST6000NE0021-2E... | 6 TB   | 3       | 1114  | 144   | 1.03   |
| Seagate   | ST3250823AS        | 250 GB | 1       | 4725  | 12    | 1.00   |
| Seagate   | ST4000NM0033       | 4 TB   | 4       | 362   | 0     | 0.99   |
| Seagate   | ST5000LM000-2AN170 | 5 TB   | 44      | 473   | 34    | 0.99   |
| Seagate   | ST3000DM008-2DM166 | 3 TB   | 6       | 699   | 185   | 0.94   |
| Seagate   | ST14000NM001G-2... | 14 TB  | 16      | 343   | 0     | 0.94   |
| Seagate   | ST3500411SV        | 500 GB | 1       | 335   | 0     | 0.92   |
| Seagate   | ST500LT012-1DG142  | 500 GB | 3       | 330   | 0     | 0.91   |
| Seagate   | ST4000DM004-2CV104 | 4 TB   | 14      | 350   | 2     | 0.90   |
| Seagate   | ST4000VN008-2DR166 | 4 TB   | 85      | 337   | 80    | 0.89   |
| Seagate   | ST8000NE001-2M7101 | 8 TB   | 1       | 317   | 0     | 0.87   |
| Seagate   | ST10000VX0004-1... | 10 TB  | 18      | 317   | 0     | 0.87   |
| Seagate   | ST10000NM0478-2... | 10 TB  | 28      | 336   | 4     | 0.87   |
| Seagate   | ST8000VN004-2M2101 | 8 TB   | 196     | 325   | 31    | 0.85   |
| Seagate   | ST8000VE000-2P6101 | 8 TB   | 1       | 307   | 0     | 0.84   |
| Seagate   | ST31000528AS       | 1 TB   | 4       | 777   | 351   | 0.84   |
| Seagate   | ST9500420ASG       | 500 GB | 4       | 713   | 19    | 0.81   |
| Seagate   | ST2000LX001-1RG174 | 2 TB   | 4       | 295   | 0     | 0.81   |
| Seagate   | ST8000VX004-2M1101 | 8 TB   | 5       | 295   | 416   | 0.79   |
| Seagate   | ST9750420AS        | 752 GB | 1       | 285   | 0     | 0.78   |
| Seagate   | ST1000NX0443       | 1 TB   | 22      | 281   | 0     | 0.77   |
| Seagate   | ST31000524NS       | 1 TB   | 20      | 1090  | 107   | 0.77   |
| Seagate   | ST2000NM000A-2J... | 2 TB   | 1       | 281   | 0     | 0.77   |
| Seagate   | ST18000NM000J-2... | 18 TB  | 11      | 276   | 0     | 0.76   |
| Seagate   | ST10000DM0004-1... | 10 TB  | 7       | 482   | 453   | 0.76   |
| Seagate   | ST16000NM001G-2... | 16 TB  | 57      | 273   | 0     | 0.75   |
| Seagate   | ST32000542AS       | 2 TB   | 1       | 264   | 0     | 0.73   |
| Seagate   | ST12000NM0248-2... | 12 TB  | 2       | 263   | 0     | 0.72   |
| Seagate   | ST4000NM000A-2H... | 4 TB   | 10      | 301   | 1     | 0.71   |
| Seagate   | ST3160815AS        | 160 GB | 3       | 2011  | 183   | 0.71   |
| Seagate   | ST31000524NS 45... | 1 TB   | 2       | 682   | 2     | 0.70   |
| Seagate   | ST2000VN004-2E4164 | 2 TB   | 25      | 247   | 0     | 0.68   |
| Seagate   | ST3320418AS        | 320 GB | 1       | 239   | 0     | 0.66   |
| Seagate   | ST8000NM000A-2K... | 8 TB   | 51      | 240   | 1     | 0.63   |
| Seagate   | ST2000DM008-2FR102 | 2 TB   | 74      | 249   | 32    | 0.59   |
| Seagate   | ST3160812AS        | 160 GB | 1       | 211   | 0     | 0.58   |
| Seagate   | ST1000NX0423 00... | 1 TB   | 1       | 207   | 0     | 0.57   |
| Seagate   | ST10000NM001G-2... | 10 TB  | 36      | 198   | 0     | 0.54   |
| Seagate   | ST12000NM0128-2... | 12 TB  | 18      | 197   | 1     | 0.53   |
| Seagate   | ST3000NM0005-1V... | 3 TB   | 1       | 166   | 0     | 0.46   |
| Seagate   | ST1000NX0343       | 1 TB   | 2       | 1561  | 10    | 0.45   |
| Seagate   | ST1000NX0303       | 1 TB   | 3       | 650   | 4     | 0.42   |
| Seagate   | ST4000NM002A-2H... | 4 TB   | 65      | 143   | 0     | 0.39   |
| Seagate   | ST250DM000-1BD141  | 250 GB | 3       | 750   | 44    | 0.37   |
| Seagate   | ST6000VN001-2BB186 | 6 TB   | 8       | 136   | 0     | 0.37   |
| Seagate   | ST12000NM001G-2... | 12 TB  | 43      | 132   | 0     | 0.36   |
| Seagate   | ST10000VN0008-2... | 10 TB  | 19      | 118   | 0     | 0.32   |
| Seagate   | ST1000VM002-1CT162 | 1 TB   | 1       | 113   | 0     | 0.31   |
| Seagate   | ST8000NM012A-2K... | 8 TB   | 73      | 86    | 0     | 0.24   |
| Seagate   | ST9500325AS        | 500 GB | 2       | 1360  | 69    | 0.20   |
| Seagate   | ST32000641AS       | 2 TB   | 1       | 2235  | 29    | 0.20   |
| Seagate   | ST9160412AS        | 160 GB | 1       | 73    | 0     | 0.20   |
| Seagate   | ST980811AS         | 80 GB  | 1       | 348   | 4     | 0.19   |
| Seagate   | ST2000NC001-1DY164 | 2 TB   | 1       | 1641  | 24    | 0.18   |
| Seagate   | ST1000LM035-1RK172 | 1 TB   | 1       | 60    | 0     | 0.16   |
| Seagate   | ST12000NM003G-2... | 12 TB  | 4       | 49    | 0     | 0.14   |
| Seagate   | ST31000340NS       | 1 TB   | 3       | 730   | 354   | 0.12   |
| Seagate   | ST3000DM001-9YN166 | 3 TB   | 6       | 1993  | 1022  | 0.12   |
| Seagate   | ST8000VN0002-1Z... | 8 TB   | 1       | 1522  | 35    | 0.12   |
| Seagate   | ST3640323AS        | 640 GB | 1       | 960   | 24    | 0.11   |
| Seagate   | ST5000NM0024-1H... | 5 TB   | 1       | 1636  | 49    | 0.09   |
| Seagate   | ST16000NM003G-2... | 16 TB  | 4       | 30    | 0     | 0.08   |
| Seagate   | ST6000NM002A-2K... | 6 TB   | 1       | 25    | 0     | 0.07   |
| Seagate   | ST16000NM005G-2... | 16 TB  | 6       | 18    | 0     | 0.05   |
| Seagate   | ST4000VX007-2DT166 | 4 TB   | 1       | 17    | 0     | 0.05   |
| Seagate   | ST16000VN001-2R... | 16 TB  | 1       | 10    | 0     | 0.03   |
| Seagate   | ST1000NC000-1CX162 | 1 TB   | 1       | 2229  | 224   | 0.03   |
| Seagate   | ST3160813AS        | 160 GB | 1       | 1350  | 139   | 0.03   |
| Seagate   | ST18000NE000-2Y... | 18 TB  | 1       | 8     | 0     | 0.02   |
| Seagate   | ST9320325AS        | 320 GB | 1       | 348   | 42    | 0.02   |
| Seagate   | ST16000NM001J-2... | 16 TB  | 1       | 6     | 0     | 0.02   |
| Seagate   | ST2000DM001-1E6164 | 2 TB   | 1       | 6     | 0     | 0.02   |
| Seagate   | ST3750330NS        | 752 GB | 1       | 170   | 36    | 0.01   |
| Seagate   | ST31500341AS       | 1.5 TB | 1       | 2761  | 670   | 0.01   |
| Seagate   | ST3160211AS        | 160 GB | 1       | 1802  | 497   | 0.01   |
| Seagate   | ST1000VM002-1ET162 | 1 TB   | 1       | 113   | 32    | 0.01   |
| Seagate   | ST31000524NS 43... | 1 TB   | 1       | 2718  | 1138  | 0.01   |
| Seagate   | ST3000VN007-2AH16M | 3 TB   | 3       | 1     | 0     | 0.01   |
| Seagate   | ST500LT012-9WS142  | 500 GB | 3       | 1335  | 1067  | 0.00   |
| Seagate   | ST4000DM000-2AE166 | 4 TB   | 1       | 998   | 1060  | 0.00   |
| Seagate   | ST9120817AS        | 120 GB | 1       | 3408  | 3854  | 0.00   |
| Seagate   | ST3200827AS        | 200 GB | 1       | 1371  | 2268  | 0.00   |
| Seagate   | ST3250820AS        | 250 GB | 1       | 957   | 3053  | 0.00   |
| Seagate   | ST3500320NS        | 500 GB | 2       | 585   | 2573  | 0.00   |
| Seagate   | ST9160821AS        | 160 GB | 1       | 262   | 1011  | 0.00   |
| Seagate   | ST1000LM010-9YH146 | 1 TB   | 1       | 61    | 1087  | 0.00   |

HDD by Family
-------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days — avg. days per sample,
Err  — avg. errors per sample,
MTBF — avg. MTBF in years per sample.

| MFG       | Family                 | Models | Samples | Days  | Err   | MTBF   |
|-----------|------------------------|--------|---------|-------|-------|--------|
| Seagate   | Constellation ES.2 ... | 3      | 108     | 2947  | 24    | 7.14   |
| Seagate   | Constellation ES.2     | 1      | 2       | 2424  | 0     | 6.64   |
| Seagate   | NAS HDD                | 3      | 33      | 2301  | 2     | 5.96   |
| Seagate   | Constellation.2 (SATA) | 3      | 68      | 1847  | 1     | 4.69   |
| Seagate   | Surveillance           | 11     | 50      | 1874  | 2     | 4.66   |
| Seagate   | Constellation ES.3     | 5      | 692     | 2067  | 100   | 4.64   |
| Seagate   | Enterprise NAS HDD     | 2      | 13      | 1804  | 1     | 4.55   |
| Seagate   | Barracuda 7200.10      | 10     | 24      | 2381  | 191   | 4.29   |
| Seagate   | Constellation CS       | 5      | 7       | 2022  | 36    | 4.06   |
| Seagate   | Barracuda ES           | 6      | 33      | 1791  | 316   | 3.96   |
| Seagate   | Barracuda XT           | 2      | 5       | 2310  | 14    | 3.59   |
| Seagate   | Barracuda 7200.14 (AF) | 16     | 198     | 1606  | 185   | 3.40   |
| Seagate   | Archive HDD            | 1      | 19      | 1405  | 56    | 3.34   |
| Seagate   | Barracuda Pro          | 1      | 215     | 1259  | 1     | 3.27   |
| Seagate   | SpinPoint M9T          | 1      | 16      | 1315  | 1     | 3.24   |
| Seagate   | Desktop HDD.15         | 4      | 155     | 1239  | 23    | 3.19   |
| Seagate   | Enterprise Capacity... | 18     | 4860    | 1431  | 33    | 3.17   |
| Seagate   | Constellation ES (S... | 3      | 50      | 1713  | 85    | 2.82   |
| Seagate   | Momentus XT (AF)       | 1      | 1       | 2921  | 2     | 2.67   |
| Seagate   | Desktop SSHD           | 2      | 8       | 1527  | 164   | 2.61   |
| Seagate   | Exos X12               | 2      | 142     | 866   | 7     | 2.32   |
| Seagate   | Barracuda SpinPoint F3 | 2      | 5       | 1732  | 4     | 2.28   |
| Seagate   | Skyhawk                | 6      | 35      | 848   | 61    | 2.26   |
| Seagate   | Barracuda 7200.7 an... | 1      | 1       | 3197  | 3     | 2.19   |
| Seagate   | Seagate Enterprise     | 1      | 198     | 869   | 37    | 2.17   |
| Seagate   | Sun Internal           | 1      | 1       | 1574  | 1     | 2.16   |
| Seagate   | Pipeline HD 5900.2     | 1      | 10      | 964   | 2     | 2.10   |
| Seagate   | Terascale              | 1      | 6       | 722   | 0     | 1.98   |
| Seagate   | Constellation ES       | 4      | 6       | 1628  | 191   | 1.96   |
| Seagate   | SpinPoint M8 (AF)      | 1      | 3       | 1027  | 4     | 1.92   |
| Seagate   | Mobile HDD             | 2      | 39      | 940   | 204   | 1.89   |
| Seagate   | Barracuda 3.5          | 2      | 54      | 713   | 20    | 1.87   |
| Seagate   | Exos 7E2               | 9      | 147     | 907   | 11    | 1.80   |
| Seagate   | Barracuda Compute      | 2      | 6       | 632   | 0     | 1.73   |
| Seagate   | Barracuda 7200.12      | 8      | 28      | 1175  | 218   | 1.70   |
| Seagate   | Barracuda Green (AF)   | 2      | 8       | 1234  | 402   | 1.68   |
| Seagate   | Exos 7E2000            | 2      | 67      | 590   | 1     | 1.59   |
| Seagate   | IronWolf Pro           | 5      | 22      | 1043  | 274   | 1.58   |
| Seagate   | Barracuda 2.5 5400     | 4      | 116     | 622   | 37    | 1.50   |
| Seagate   | BarraCuda Pro          | 1      | 10      | 956   | 317   | 1.46   |
| Seagate   | Constellation ES (S... | 3      | 35      | 1232  | 66    | 1.37   |
| Seagate   | Exos X14               | 5      | 243     | 467   | 3     | 1.26   |
| Seagate   | BarraCuda 3.5 (CMR)    | 2      | 12      | 578   | 264   | 1.25   |
| Seagate   | FireCuda 3.5           | 1      | 4       | 424   | 0     | 1.16   |
| Seagate   | Barracuda Pro Compute  | 1      | 4       | 398   | 0     | 1.09   |
| Seagate   | IronWolf               | 15     | 456     | 416   | 38    | 1.08   |
| Seagate   | Barracuda 7200.8       | 1      | 1       | 4725  | 12    | 1.00   |
| Seagate   | BarraCuda 3.5          | 7      | 172     | 406   | 34    | 0.94   |
| Seagate   | Exos 7E8               | 10     | 354     | 378   | 11    | 0.94   |
| Seagate   | SkyHawk                | 3      | 23      | 334   | 0     | 0.92   |
| Seagate   | Laptop HDD             | 3      | 9       | 758   | 356   | 0.86   |
| Seagate   | FireCuda 2.5           | 1      | 4       | 295   | 0     | 0.81   |
| Seagate   | Momentus 7200.5        | 1      | 1       | 285   | 0     | 0.78   |
| Seagate   | Barracuda LP           | 1      | 1       | 264   | 0     | 0.73   |
| Seagate   | Exos X18               | 2      | 12      | 253   | 0     | 0.69   |
| Seagate   | Momentus 7200.4        | 2      | 5       | 585   | 15    | 0.69   |
| Seagate   | Exos X16               | 9      | 172     | 225   | 0     | 0.62   |
| Seagate   | Barracuda 7200.9       | 3      | 3       | 1128  | 922   | 0.20   |
| Seagate   | Video 3.5 HDD          | 2      | 2       | 113   | 16    | 0.16   |
| Seagate   | Momentus 5400.6        | 2      | 3       | 1023  | 60    | 0.14   |
| Seagate   | Momentus 5400.3        | 2      | 2       | 305   | 508   | 0.10   |
| Seagate   | Barracuda ES.2         | 3      | 6       | 588   | 1041  | 0.06   |
| Seagate   | Barracuda 7200.11      | 3      | 3       | 1690  | 278   | 0.05   |
| Seagate   | Momentus 5400.4        | 1      | 1       | 3408  | 3854  | 0.00   |
| Seagate   | FreePlay               | 1      | 1       | 61    | 1087  | 0.00   |
