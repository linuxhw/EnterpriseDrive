Hitachi Hard Drives
===================

This is a list of all tested Hitachi hard drive models and their MTBFs. See more
info on reliability test in the [README](https://github.com/linuxhw/EnterpriseDrive).

Contents
--------

1. [ HDD by Model  ](#hdd-by-model)
2. [ HDD by Family ](#hdd-by-family)

HDD by Model
------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days — avg. days per sample,
Err  — avg. errors per sample,
MTBF — avg. MTBF in years per sample.

| MFG       | Model              | Size   | Samples | Days  | Err   | MTBF   |
|-----------|--------------------|--------|---------|-------|-------|--------|
| Hitachi   | HDS725050KLA360    | 500 GB | 1       | 4456  | 0     | 12.21  |
| Hitachi   | HDS721010KLA33R... | 1 TB   | 1       | 3863  | 0     | 10.58  |
| Hitachi   | HUA7210SASUN1.0T   | 1 TB   | 16      | 3909  | 1     | 9.25   |
| Hitachi   | HUA722050CLA330    | 500 GB | 2       | 3243  | 0     | 8.89   |
| Hitachi   | HDS721010KLA330    | 1 TB   | 16      | 3343  | 16    | 8.08   |
| Hitachi   | HUA722020ALA330... | 2 TB   | 1       | 2886  | 0     | 7.91   |
| Hitachi   | HDS721050CLA360    | 500 GB | 2       | 2828  | 0     | 7.75   |
| Hitachi   | HDS723020BLA642    | 2 TB   | 81      | 3052  | 1     | 7.75   |
| Hitachi   | HDS723030ALA640    | 3 TB   | 8       | 3041  | 22    | 7.73   |
| Hitachi   | HUA723020ALA640... | 2 TB   | 1       | 2643  | 0     | 7.24   |
| Hitachi   | HUA721010KLA330    | 1 TB   | 23      | 2692  | 1     | 7.17   |
| Hitachi   | HUA722010CLA330... | 1 TB   | 1       | 2591  | 0     | 7.10   |
| Hitachi   | HDS723020BLE640    | 2 TB   | 6       | 2982  | 1     | 7.07   |
| Hitachi   | HDS722020ALA330    | 2 TB   | 34      | 2875  | 62    | 6.77   |
| Hitachi   | HDS721010CLA330    | 1 TB   | 1       | 2342  | 0     | 6.42   |
| Hitachi   | HUA723030ALA640    | 3 TB   | 32      | 2203  | 1     | 5.78   |
| Hitachi   | HDP725050GLA360    | 500 GB | 3       | 2095  | 0     | 5.74   |
| Hitachi   | HUA723020ALA640    | 2 TB   | 22      | 1885  | 7     | 4.96   |
| Hitachi   | HDT725032VLA360    | 320 GB | 5       | 3607  | 7     | 4.88   |
| Hitachi   | HUA722010ALA330    | 1 TB   | 5       | 2547  | 5     | 4.45   |
| Hitachi   | HDT721050SLA360    | 500 GB | 1       | 1617  | 0     | 4.43   |
| Hitachi   | HUA722010CLA330    | 1 TB   | 55      | 2120  | 25    | 4.29   |
| Hitachi   | HUA723020ALA641    | 2 TB   | 13      | 1504  | 0     | 4.12   |
| Hitachi   | HUA722020ALA330    | 2 TB   | 6       | 1954  | 1     | 4.00   |
| Hitachi   | HUA722020ALA331    | 2 TB   | 10      | 2351  | 203   | 3.93   |
| Hitachi   | HDS721010CLA332    | 1 TB   | 19      | 2820  | 396   | 3.93   |
| Hitachi   | HDS721075KLA330    | 752 GB | 1       | 3894  | 2     | 3.56   |
| Hitachi   | HDS722020ALA330... | 2 TB   | 34      | 1748  | 29    | 3.23   |
| Hitachi   | HDT725050VLA360    | 500 GB | 1       | 1157  | 0     | 3.17   |
| Hitachi   | HDS721050CLA362    | 500 GB | 6       | 1905  | 459   | 3.10   |
| Hitachi   | HUA722010CLA630    | 1 TB   | 11      | 1396  | 105   | 3.02   |
| Hitachi   | HDS721032CLA362    | 320 GB | 3       | 1673  | 571   | 2.72   |
| Hitachi   | HUS724030ALE641    | 3 TB   | 13      | 1171  | 49    | 2.62   |
| Hitachi   | HDP725025GLA380    | 250 GB | 2       | 4193  | 6     | 2.44   |
| Hitachi   | HUS724040ALE640    | 4 TB   | 22      | 797   | 1     | 2.08   |
| Hitachi   | HDE721050SLA330    | 500 GB | 2       | 3732  | 4     | 2.05   |
| Hitachi   | HTS723216L9SA60    | 160 GB | 1       | 727   | 0     | 1.99   |
| Hitachi   | HDS723030ALA640... | 3 TB   | 2       | 975   | 1     | 1.77   |
| Hitachi   | HDE721010SLA330    | 1 TB   | 4       | 1982  | 3     | 1.69   |
| Hitachi   | HUS724040ALE641    | 4 TB   | 14      | 551   | 2     | 1.13   |
| Hitachi   | HTS542512K9SA00    | 120 GB | 1       | 1508  | 3     | 1.03   |
| Hitachi   | HDS721050DLE630    | 500 GB | 4       | 808   | 420   | 0.90   |
| Hitachi   | HDS721025CLA382    | 250 GB | 1       | 2454  | 8     | 0.75   |
| Hitachi   | HTS545032B9A300    | 320 GB | 1       | 1024  | 3     | 0.70   |
| Hitachi   | HDT725025VLA380... | 250 GB | 1       | 135   | 0     | 0.37   |
| Hitachi   | HDS721010DLE630    | 1 TB   | 8       | 1613  | 1436  | 0.34   |
| Hitachi   | HTS725016A9A364    | 160 GB | 2       | 1326  | 507   | 0.30   |
| Hitachi   | HTS725050A9A362    | 500 GB | 1       | 868   | 14    | 0.16   |
| Hitachi   | HTS545016B9A300    | 160 GB | 1       | 55    | 0     | 0.15   |
| Hitachi   | HTS545025B9A300    | 250 GB | 1       | 219   | 3     | 0.15   |
| Hitachi   | HTS721060G9SA00    | 64 GB  | 1       | 2330  | 66    | 0.10   |
| Hitachi   | HDT722525DLA380    | 250 GB | 1       | 3645  | 398   | 0.03   |
| Hitachi   | HDS721010CLA632    | 1 TB   | 1       | 960   | 118   | 0.02   |
| Hitachi   | HDS721010CLA630    | 1 TB   | 2       | 1527  | 699   | 0.01   |
| Hitachi   | HDS5C3020ALA632    | 2 TB   | 1       | 1638  | 2016  | 0.00   |

HDD by Family
-------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days — avg. days per sample,
Err  — avg. errors per sample,
MTBF — avg. MTBF in years per sample.

| MFG       | Family                 | Models | Samples | Days  | Err   | MTBF   |
|-----------|------------------------|--------|---------|-------|-------|--------|
| Hitachi   | Deskstar 7K500         | 1      | 1       | 4456  | 0     | 12.21  |
| Hitachi   | Sun Original           | 1      | 16      | 3909  | 1     | 9.25   |
| Hitachi   | Deskstar 7K1000        | 3      | 18      | 3403  | 15    | 7.97   |
| Hitachi   | Deskstar 7K3000        | 5      | 98      | 3000  | 3     | 7.58   |
| Hitachi   | Ultrastar A7K1000      | 1      | 23      | 2692  | 1     | 7.17   |
| Hitachi   | Ultrastar 7K3000       | 3      | 67      | 1963  | 3     | 5.19   |
| Hitachi   | Deskstar 7K2000        | 2      | 68      | 2311  | 45    | 5.00   |
| Hitachi   | Deskstar 7K1000.B      | 1      | 1       | 1617  | 0     | 4.43   |
| Hitachi   | Deskstar P7K500        | 2      | 5       | 2934  | 3     | 4.42   |
| Hitachi   | Ultrastar A7K2000      | 8      | 91      | 2108  | 51    | 4.26   |
| Hitachi   | Deskstar T7K500        | 3      | 7       | 2761  | 5     | 3.99   |
| Hitachi   | Deskstar 7K1000.C      | 8      | 35      | 2414  | 386   | 3.55   |
| Hitachi   | Travelstar 7K320       | 1      | 1       | 727   | 0     | 1.99   |
| Hitachi   | Ultrastar 7K4000       | 3      | 49      | 826   | 14    | 1.95   |
| Hitachi   | Deskstar E7K1000       | 2      | 6       | 2565  | 3     | 1.81   |
| Hitachi   | Travelstar 5K250       | 1      | 1       | 1508  | 3     | 1.03   |
| Hitachi   | Deskstar 7K1000.D      | 2      | 12      | 1345  | 1097  | 0.53   |
| Hitachi   | Travelstar 5K500.B     | 3      | 3       | 433   | 2     | 0.33   |
| Hitachi   | Travelstar 7K500       | 2      | 3       | 1173  | 343   | 0.25   |
| Hitachi   | Travelstar 7K100       | 1      | 1       | 2330  | 66    | 0.10   |
| Hitachi   | Deskstar T7K250        | 1      | 1       | 3645  | 398   | 0.03   |
| Hitachi   | Deskstar 5K3000        | 1      | 1       | 1638  | 2016  | 0.00   |
