WDC Hard Drives
===============

This is a list of all tested WDC hard drive models and their MTBFs. See more
info on reliability test in the [README](https://github.com/linuxhw/EnterpriseDrive).

Contents
--------

1. [ HDD by Model  ](#hdd-by-model)
2. [ HDD by Family ](#hdd-by-family)

HDD by Model
------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days — avg. days per sample,
Err  — avg. errors per sample,
MTBF — avg. MTBF in years per sample.

| MFG       | Model              | Size   | Samples | Days  | Err   | MTBF   |
|-----------|--------------------|--------|---------|-------|-------|--------|
| WDC       | WD740ADFD-00NLR5   | 74 GB  | 1       | 4658  | 0     | 12.76  |
| WDC       | WD5000AAKS-00A7B2  | 500 GB | 1       | 3899  | 0     | 10.68  |
| WDC       | WD1002FBYS-01A6B0  | 1 TB   | 2       | 3719  | 0     | 10.19  |
| WDC       | WD2502ABYS-01B7A0  | 256 GB | 3       | 3596  | 0     | 9.85   |
| WDC       | WD1001FALS-00Y6A0  | 1 TB   | 3       | 3147  | 0     | 8.62   |
| WDC       | WD5000AAKS-00V6A0  | 500 GB | 1       | 2965  | 0     | 8.12   |
| WDC       | WD10EACS-00ZJB0    | 1 TB   | 1       | 2955  | 0     | 8.10   |
| WDC       | WD1500HLFS-01G6U4  | 150 GB | 4       | 2929  | 0     | 8.03   |
| WDC       | WD1002FBYS-05A6B0  | 1 TB   | 8       | 3346  | 1     | 7.99   |
| WDC       | WD10EZRX-00A8LB0   | 1 TB   | 1       | 2878  | 0     | 7.89   |
| WDC       | WD5000AAKS-00E4A0  | 500 GB | 1       | 2827  | 0     | 7.75   |
| WDC       | WD10EARS-00MVWB0   | 1 TB   | 1       | 2778  | 0     | 7.61   |
| WDC       | WD1002FBYS-50A6B1  | 1 TB   | 1       | 2722  | 0     | 7.46   |
| WDC       | WD1600JD-75HBB0    | 160 GB | 1       | 2640  | 0     | 7.23   |
| WDC       | WD2500AAJS-22L7A0  | 250 GB | 1       | 2570  | 0     | 7.04   |
| WDC       | WD10EALX-759BA1    | 1 TB   | 1       | 2562  | 0     | 7.02   |
| WDC       | WD5003ABYX-01WERA2 | 500 GB | 2       | 2547  | 0     | 6.98   |
| WDC       | WD3200AAKS-61L9A0  | 320 GB | 1       | 2526  | 0     | 6.92   |
| WDC       | WD10EADS-00L5B1    | 1 TB   | 1       | 2526  | 0     | 6.92   |
| WDC       | WD1002FBYS-02A6B0  | 1 TB   | 16      | 2827  | 2     | 6.88   |
| WDC       | WD5000AACS-00G8B1  | 500 GB | 2       | 4300  | 3     | 6.73   |
| WDC       | WD6400AAKS-41H2B0  | 640 GB | 2       | 2433  | 0     | 6.67   |
| WDC       | WD7500AYYS-01RCA0  | 752 GB | 5       | 2785  | 1     | 6.66   |
| WDC       | WD1500HLFS-01G6U0  | 150 GB | 1       | 2403  | 0     | 6.59   |
| WDC       | WD7501AALS-00J7B0  | 752 GB | 2       | 2359  | 0     | 6.46   |
| WDC       | WD10EZEX-75ZF5A0   | 1 TB   | 1       | 2335  | 0     | 6.40   |
| WDC       | WD5003AZEX-00K1GA0 | 500 GB | 29      | 2406  | 1     | 6.22   |
| WDC       | WD7502AAEX-00Y9A0  | 752 GB | 4       | 2242  | 0     | 6.14   |
| WDC       | WD1002FBYS-18W8B0  | 1 TB   | 10      | 2883  | 9     | 6.05   |
| WDC       | WD30EZRX-00SPEB0   | 3 TB   | 6       | 2569  | 2     | 5.96   |
| WDC       | WD1001FALS-00E8B0  | 1 TB   | 1       | 2171  | 0     | 5.95   |
| WDC       | WD5002AALX-00J37A0 | 500 GB | 11      | 2849  | 3     | 5.89   |
| WDC       | WD1600AAJS-08L7A0  | 160 GB | 1       | 2027  | 0     | 5.56   |
| WDC       | WD6001FSYZ-01SS7B0 | 6 TB   | 3       | 1989  | 0     | 5.45   |
| WDC       | WD20EARX-00PASB0   | 2 TB   | 4       | 1958  | 0     | 5.37   |
| WDC       | WD2004FBYZ-01YCBB0 | 2 TB   | 5       | 1902  | 0     | 5.21   |
| WDC       | WD2001FFSX-68JNUN0 | 2 TB   | 2       | 1899  | 0     | 5.20   |
| WDC       | WD30EZRX-00DC0B0   | 3 TB   | 8       | 2103  | 11    | 5.19   |
| WDC       | WD10EZEX-22RKKA0   | 1 TB   | 4       | 2248  | 1     | 5.15   |
| WDC       | WD30EFRX-68AX9N0   | 3 TB   | 17      | 2335  | 2     | 5.01   |
| WDC       | WD1002FAEX-007BA0  | 1 TB   | 2       | 1825  | 0     | 5.00   |
| WDC       | WD2500AAJS-00B4A0  | 250 GB | 1       | 1814  | 0     | 4.97   |
| WDC       | WD20EFRX-68AX9N0   | 2 TB   | 4       | 2789  | 4     | 4.96   |
| WDC       | WD2002FYPS-01U1B1  | 2 TB   | 17      | 2794  | 201   | 4.96   |
| WDC       | WD1002FAEX-00Z3A0  | 1 TB   | 33      | 2086  | 42    | 4.87   |
| WDC       | WD2502ABYS-18B7A0  | 250 GB | 2       | 1777  | 0     | 4.87   |
| WDC       | WD5000BHTZ-04JCPV0 | 500 GB | 1       | 1758  | 0     | 4.82   |
| WDC       | WD20EADS-00R6B0    | 2 TB   | 2       | 3212  | 6     | 4.77   |
| WDC       | WD20EZRX-00DC0B0   | 2 TB   | 7       | 1733  | 0     | 4.75   |
| WDC       | WD5003ABYZ-011FA0  | 500 GB | 15      | 1710  | 0     | 4.69   |
| WDC       | WD4001FFSX-68JNUN0 | 4 TB   | 12      | 2048  | 1     | 4.67   |
| WDC       | WD20EARS-00MVWB0   | 2 TB   | 7       | 2089  | 1     | 4.66   |
| WDC       | WD2002FAEX-007BA0  | 2 TB   | 59      | 1989  | 42    | 4.60   |
| WDC       | WD1002FBYS-18W8B1  | 1 TB   | 7       | 2404  | 1     | 4.53   |
| WDC       | WD5003ABYX-23 0... | 500 GB | 2       | 1645  | 0     | 4.51   |
| WDC       | WD5003AZEX-00S3DA0 | 500 GB | 16      | 1644  | 0     | 4.51   |
| WDC       | WD1000DHTZ-04N21V1 | 1 TB   | 6       | 2294  | 2     | 4.47   |
| WDC       | WD10EZEX-22BN5A0   | 1 TB   | 1       | 1630  | 0     | 4.47   |
| WDC       | WD6402AAEX-00Y9A0  | 640 GB | 3       | 2411  | 677   | 4.43   |
| WDC       | WD20EARX-00MMMB0   | 2 TB   | 2       | 2666  | 2     | 4.39   |
| WDC       | WD5000AAKX-003CA0  | 500 GB | 3       | 2165  | 3     | 4.36   |
| WDC       | WD4002FFWX-68TZ4N0 | 4 TB   | 4       | 1581  | 0     | 4.33   |
| WDC       | WD7500BPKX-00HPJT0 | 752 GB | 16      | 1885  | 2     | 4.28   |
| WDC       | WD1500HLHX-01JJPV0 | 150 GB | 1       | 1552  | 0     | 4.25   |
| WDC       | WD1000CHTZ-04JCPV1 | 1 TB   | 4       | 1542  | 0     | 4.22   |
| WDC       | WD10EZEX-00RKKA0   | 1 TB   | 7       | 1796  | 16    | 4.12   |
| WDC       | WD6002FRYZ-01WD5B0 | 6 TB   | 56      | 1578  | 6     | 4.10   |
| WDC       | WD5000AADS-00S9B0  | 500 GB | 4       | 2749  | 4     | 4.09   |
| WDC       | WD30EZRX-00D8PB0   | 3 TB   | 15      | 1779  | 2     | 4.08   |
| WDC       | WD2003FYYS-05T8B0  | 2 TB   | 2       | 1487  | 0     | 4.08   |
| WDC       | WD10EALX-089BA0    | 1 TB   | 7       | 1879  | 2     | 4.06   |
| WDC       | WD2500HHTZ-04N21V0 | 250 GB | 3       | 1473  | 0     | 4.04   |
| WDC       | WD2003FYYS-02W0B1  | 2 TB   | 36      | 1951  | 50    | 4.03   |
| WDC       | WD5000HHTZ-04N21V0 | 500 GB | 1       | 1468  | 0     | 4.02   |
| WDC       | WD800JD-60LUA0     | 80 GB  | 1       | 1465  | 0     | 4.01   |
| WDC       | WD2003FYPS-02W0B1  | 2 TB   | 2       | 1454  | 0     | 3.99   |
| WDC       | WD5000AZLX-07K2TA0 | 500 GB | 1       | 1444  | 0     | 3.96   |
| WDC       | WD4003FZEX-00Z4SA0 | 4 TB   | 14      | 1431  | 0     | 3.92   |
| WDC       | WD2000FYYZ-01UL1B3 | 2 TB   | 1       | 1427  | 0     | 3.91   |
| WDC       | WD5002AALX-32Z3A0  | 500 GB | 7       | 1425  | 0     | 3.91   |
| WDC       | WD5000AAKX-75U6AA0 | 500 GB | 4       | 1417  | 0     | 3.88   |
| WDC       | WD20EADS-65R6B1    | 2 TB   | 1       | 2788  | 1     | 3.82   |
| WDC       | WD40PURX-64NZ6Y0   | 4 TB   | 4       | 1394  | 0     | 3.82   |
| WDC       | WD6002FFWX-68TZ4N0 | 6 TB   | 8       | 1609  | 44    | 3.80   |
| WDC       | WD101KRYZ-01JPDB0  | 10 TB  | 1       | 1384  | 0     | 3.79   |
| WDC       | WD5000BMVW-11AJGS2 | 500 GB | 2       | 1375  | 0     | 3.77   |
| WDC       | WD5000AUDX-61WNHY0 | 500 GB | 1       | 1373  | 0     | 3.76   |
| WDC       | WD1002FBYS-18A6B0  | 1 TB   | 7       | 2160  | 401   | 3.75   |
| WDC       | WD5000AAKX-60U6AA0 | 500 GB | 4       | 1494  | 1     | 3.75   |
| WDC       | WD5001AALS-00E3A0  | 500 GB | 6       | 2120  | 3     | 3.74   |
| WDC       | WD4000FYYZ-03UL1B3 | 4 TB   | 2       | 1360  | 0     | 3.73   |
| WDC       | WD1600AAJS-00YZCA0 | 160 GB | 1       | 1356  | 0     | 3.72   |
| WDC       | WD5000AZLX-00CL5A0 | 500 GB | 2       | 1353  | 0     | 3.71   |
| WDC       | WD100PURZ-85W86Y0  | 10 TB  | 1       | 1347  | 0     | 3.69   |
| WDC       | WD1003FBYX-50Y7B1  | 1 TB   | 3       | 1335  | 0     | 3.66   |
| WDC       | WD1003FBYX-01Y7B1  | 1 TB   | 123     | 1599  | 7     | 3.64   |
| WDC       | WD5000AAKX-08ERMA0 | 500 GB | 3       | 1328  | 0     | 3.64   |
| WDC       | WD40EFRX-68WT0N0   | 4 TB   | 288     | 2022  | 17    | 3.63   |
| WDC       | WD1002FAEX-00Y9A0  | 1 TB   | 42      | 1495  | 8     | 3.63   |
| WDC       | WD10EZRX-00L4HB0   | 1 TB   | 6       | 1320  | 0     | 3.62   |
| WDC       | WD2000FYYZ-03UL1B2 | 2 TB   | 1       | 1292  | 0     | 3.54   |
| WDC       | WD1001FALS-00J7B0  | 1 TB   | 2       | 2317  | 4     | 3.53   |
| WDC       | WD5003ABYX-01WERA0 | 500 GB | 20      | 1598  | 2     | 3.50   |
| WDC       | WD10EZEX-00MFCA0   | 1 TB   | 1       | 1262  | 0     | 3.46   |
| WDC       | WD5000BPKX-22HPJT0 | 500 GB | 5       | 1483  | 2     | 3.46   |
| WDC       | WD10EZEX-21WN4A0   | 1 TB   | 1       | 1260  | 0     | 3.45   |
| WDC       | WD10EFRX-68JCSN0   | 1 TB   | 7       | 2296  | 3     | 3.43   |
| WDC       | WD20NMVW-11W68S0   | 2 TB   | 1       | 1251  | 0     | 3.43   |
| WDC       | WD1003FZEX-00MK2A0 | 1 TB   | 89      | 1361  | 14    | 3.42   |
| WDC       | WD2004FBYZ-01YCBB1 | 2 TB   | 24      | 1333  | 1     | 3.41   |
| WDC       | WD5003ABYX-18WERA0 | 500 GB | 15      | 1348  | 1     | 3.39   |
| WDC       | WD30EFRX-68EUZN0   | 3 TB   | 45      | 1572  | 2     | 3.37   |
| WDC       | WD10SPZX-00Z10T0   | 1 TB   | 1       | 1213  | 0     | 3.32   |
| WDC       | WD2004FBYZ-01YCBB2 | 2 TB   | 1       | 1197  | 0     | 3.28   |
| WDC       | WD4002FYYZ-01B7CB0 | 4 TB   | 51      | 1332  | 3     | 3.25   |
| WDC       | WD1004FBYZ-01YCBB1 | 1 TB   | 7       | 1179  | 0     | 3.23   |
| WDC       | WD5000AAKX-221CA1  | 500 GB | 2       | 1179  | 0     | 3.23   |
| WDC       | WD1002F9YZ-09H1JL1 | 1 TB   | 14      | 1355  | 2     | 3.22   |
| WDC       | WD10EZEX-00UD2A0   | 1 TB   | 1       | 1166  | 0     | 3.20   |
| WDC       | WD10EZEX-00BN5A0   | 1 TB   | 7       | 1162  | 0     | 3.19   |
| WDC       | WD10EZEX-75WN4A0   | 1 TB   | 4       | 1156  | 0     | 3.17   |
| WDC       | WD1003FBYX-01Y7B0  | 1 TB   | 43      | 1690  | 12    | 3.15   |
| WDC       | WD10EALS-00Z8A0    | 1 TB   | 7       | 2354  | 5     | 3.14   |
| WDC       | WD5000LPLX-00ZNTT0 | 500 GB | 2       | 1146  | 0     | 3.14   |
| WDC       | WD3001FAEX-00MJRA0 | 3 TB   | 1       | 1140  | 0     | 3.13   |
| WDC       | WD10JFCX-68N6GN0   | 1 TB   | 27      | 1188  | 1     | 3.12   |
| WDC       | WD2500BHTZ-04JCPV1 | 250 GB | 2       | 1137  | 0     | 3.12   |
| WDC       | WD1003FBYX-18Y7B0  | 1 TB   | 7       | 1997  | 2     | 3.11   |
| WDC       | WD20EZRX-00D8PB0   | 2 TB   | 10      | 1212  | 1     | 3.10   |
| WDC       | WD5000BMVW-11AJGS4 | 500 GB | 3       | 1129  | 0     | 3.10   |
| WDC       | WD3000HLHX-01JJPV0 | 304 GB | 6       | 1421  | 2     | 3.06   |
| WDC       | WD5000AAKX-00ERMA0 | 500 GB | 8       | 1419  | 2     | 3.05   |
| WDC       | WD5000BMVV-11GNWS0 | 500 GB | 1       | 1103  | 0     | 3.02   |
| WDC       | WD1003FBYX-12      | 1 TB   | 29      | 1172  | 1     | 3.00   |
| WDC       | WD2000FYYZ-01UL1B1 | 2 TB   | 35      | 1872  | 30    | 2.98   |
| WDC       | WD101KRYZ-01JPDB1  | 10 TB  | 8       | 1133  | 144   | 2.98   |
| WDC       | WD5000BHTZ-04JCPV1 | 500 GB | 6       | 1084  | 0     | 2.97   |
| WDC       | WD3000FYYZ-01UL1B2 | 3 TB   | 9       | 1718  | 3     | 2.95   |
| WDC       | WD101KFBX-68R56N0  | 10 TB  | 1       | 1075  | 0     | 2.95   |
| WDC       | WD6002FRYZ-01WD5B1 | 6 TB   | 45      | 1072  | 20    | 2.94   |
| WDC       | WD10JPLX-00MBPT0   | 1 TB   | 4       | 1065  | 0     | 2.92   |
| WDC       | WD5000HHTZ-04N21V1 | 500 GB | 3       | 1064  | 0     | 2.92   |
| WDC       | WD5000AAKX-001CA0  | 500 GB | 5       | 1580  | 1     | 2.88   |
| WDC       | WD1003FBYZ-010FB0  | 1 TB   | 62      | 1112  | 1     | 2.87   |
| WDC       | WD5003ABYX-01WERA1 | 500 GB | 18      | 1224  | 1     | 2.83   |
| WDC       | WD4004FZWX-00GBGB0 | 4 TB   | 22      | 1065  | 1     | 2.83   |
| WDC       | WD5002ABYS-02B1B0  | 500 GB | 5       | 2518  | 28    | 2.79   |
| WDC       | WD30EFRX-68N32N0   | 3 TB   | 4       | 1014  | 0     | 2.78   |
| WDC       | WD2000FYYZ-01UL1B2 | 2 TB   | 40      | 1439  | 17    | 2.77   |
| WDC       | WD7500BPKT-75PK4T0 | 752 GB | 2       | 1112  | 1     | 2.74   |
| WDC       | WD60EFRX-68MYMN1   | 6 TB   | 27      | 1847  | 20    | 2.73   |
| WDC       | WD10EFRX-68PJCN0   | 1 TB   | 5       | 993   | 0     | 2.72   |
| WDC       | WD1502FAEX-007BA0  | 1.5 TB | 2       | 1534  | 5     | 2.72   |
| WDC       | WD10EALX-009BA0    | 1 TB   | 24      | 1688  | 65    | 2.71   |
| WDC       | WD5000AAKX-08U6AA0 | 500 GB | 5       | 980   | 0     | 2.69   |
| WDC       | WD800AAJS-00PSA0   | 80 GB  | 1       | 977   | 0     | 2.68   |
| WDC       | WD3003FZEX-00Z4SA0 | 3 TB   | 8       | 976   | 0     | 2.67   |
| WDC       | WD4000FYYZ-01UL1B2 | 4 TB   | 25      | 1902  | 89    | 2.67   |
| WDC       | WD40EZRZ-00WN9B0   | 4 TB   | 18      | 1175  | 13    | 2.67   |
| WDC       | WD5000AAKX-083CA1  | 500 GB | 2       | 1651  | 4     | 2.66   |
| WDC       | WD8002FRYZ-01FF2B0 | 8 TB   | 14      | 999   | 1     | 2.65   |
| WDC       | WD5003AZEX-00MK2A0 | 500 GB | 10      | 953   | 0     | 2.61   |
| WDC       | WD2003FYYS-27Y2P0  | 2 TB   | 2       | 1374  | 2     | 2.59   |
| WDC       | WD2500AAJS-00VTA0  | 250 GB | 1       | 934   | 0     | 2.56   |
| WDC       | WD10EZEX-60M2NA0   | 1 TB   | 3       | 1242  | 339   | 2.53   |
| WDC       | WD80EFZX-68UW8N0   | 8 TB   | 23      | 957   | 1     | 2.51   |
| WDC       | WD40EFRX-68N32N0   | 4 TB   | 79      | 1007  | 1     | 2.50   |
| WDC       | WD10EZEX-22MFCA0   | 1 TB   | 2       | 911   | 0     | 2.50   |
| WDC       | WD10EZEX-00WN4A0   | 1 TB   | 6       | 906   | 0     | 2.48   |
| WDC       | WD2003FZEX-00Z4SA0 | 2 TB   | 14      | 1086  | 1     | 2.47   |
| WDC       | WD20EFRX-68EUZN0   | 2 TB   | 48      | 1137  | 10    | 2.43   |
| WDC       | WD2000F9YZ-09N20L1 | 2 TB   | 4       | 886   | 0     | 2.43   |
| WDC       | WD2000FYYZ-05UL1B0 | 2 TB   | 11      | 927   | 1     | 2.42   |
| WDC       | WD2003FYYS-02W0B0  | 2 TB   | 8       | 1482  | 8     | 2.41   |
| WDC       | WD1005FBYZ-01YCBB2 | 1 TB   | 21      | 920   | 1     | 2.39   |
| WDC       | WD10EZEX-60ZF5A0   | 1 TB   | 5       | 1753  | 7     | 2.36   |
| WDC       | WD5003AZEX-00K3CA0 | 500 GB | 3       | 846   | 0     | 2.32   |
| WDC       | WD4002FYYZ-01B7CB1 | 4 TB   | 45      | 873   | 3     | 2.31   |
| WDC       | WD50EFRX-68L0BN1   | 5 TB   | 6       | 1538  | 118   | 2.27   |
| WDC       | WD40EZRZ-22GXCB0   | 4 TB   | 16      | 813   | 0     | 2.23   |
| WDC       | WD5002ABYS-18B1B0  | 500 GB | 1       | 808   | 0     | 2.22   |
| WDC       | WD1003FZEX-00K3CA0 | 1 TB   | 207     | 816   | 3     | 2.18   |
| WDC       | WD2000F9YZ-09N20L0 | 2 TB   | 2       | 1354  | 3     | 2.15   |
| WDC       | WD800JD-00HKA0     | 80 GB  | 1       | 776   | 0     | 2.13   |
| WDC       | WD4000F9YZ-09N20L0 | 4 TB   | 4       | 775   | 0     | 2.12   |
| WDC       | WD2005FBYZ-01YCBB2 | 2 TB   | 40      | 770   | 0     | 2.11   |
| WDC       | WD4000FYYZ-01UL1B3 | 4 TB   | 23      | 1115  | 16    | 2.11   |
| WDC       | WD1005FBYZ-01YCBB1 | 1 TB   | 3       | 979   | 1     | 2.10   |
| WDC       | WD5000AZLX-08K2TA0 | 500 GB | 3       | 733   | 0     | 2.01   |
| WDC       | WD100EFAX-68LHPN0  | 10 TB  | 53      | 733   | 1     | 1.99   |
| WDC       | WD30PURZ-85GU6Y0   | 3 TB   | 2       | 696   | 0     | 1.91   |
| WDC       | WD1003FBYX-01Y7B2  | 1 TB   | 1       | 695   | 0     | 1.91   |
| WDC       | WD2003FZEX-00SRLA0 | 2 TB   | 65      | 698   | 1     | 1.89   |
| WDC       | WD5000AAKX-22ERMA0 | 500 GB | 1       | 680   | 0     | 1.86   |
| WDC       | WD2002FYPS-02W3B0  | 2 TB   | 1       | 679   | 0     | 1.86   |
| WDC       | WD40EZRZ-75GXCB0   | 4 TB   | 3       | 668   | 0     | 1.83   |
| WDC       | WD30PURX-64P6ZY0   | 3 TB   | 1       | 667   | 0     | 1.83   |
| WDC       | WD5000AZLX-35K2TA0 | 500 GB | 3       | 664   | 0     | 1.82   |
| WDC       | WD15EARS-00MVWB0   | 1.5 TB | 4       | 2054  | 324   | 1.81   |
| WDC       | WD10EFRX-68FYTN0   | 1 TB   | 17      | 712   | 2     | 1.79   |
| WDC       | WD2500AAJS-08L7A0  | 250 GB | 1       | 1944  | 2     | 1.78   |
| WDC       | WD2002FFSX-68PF8N0 | 2 TB   | 13      | 627   | 0     | 1.72   |
| WDC       | WD10JMVW-11AJGS3   | 1 TB   | 1       | 624   | 0     | 1.71   |
| WDC       | WD30EZRZ-00Z5HB0   | 3 TB   | 8       | 657   | 1     | 1.71   |
| WDC       | WD8003FFBX-68B9AN0 | 8 TB   | 3       | 616   | 0     | 1.69   |
| WDC       | WD80EFAX-68KNBN0   | 8 TB   | 5       | 614   | 0     | 1.68   |
| WDC       | WD3200AAJS-08L7A0  | 320 GB | 1       | 612   | 0     | 1.68   |
| WDC       | WD121KRYZ-01W0RB0  | 12 TB  | 8       | 812   | 1     | 1.68   |
| WDC       | WD10EURX-63UY4Y0   | 1 TB   | 2       | 606   | 0     | 1.66   |
| WDC       | WD40EZRZ-00GXCB0   | 4 TB   | 40      | 621   | 29    | 1.65   |
| WDC       | WD5000LPVX-22V0TT0 | 500 GB | 3       | 583   | 0     | 1.60   |
| WDC       | WD4000FYYZ-01UL1B1 | 4 TB   | 10      | 1633  | 5     | 1.60   |
| WDC       | WD5000AAKS-00V1A0  | 500 GB | 1       | 2881  | 4     | 1.58   |
| WDC       | WD4003FFBX-68MU3N0 | 4 TB   | 33      | 573   | 0     | 1.57   |
| WDC       | WD10EZRZ-00HTKB0   | 1 TB   | 3       | 564   | 0     | 1.55   |
| WDC       | WD5000AAKS-65YGA0  | 500 GB | 1       | 556   | 0     | 1.52   |
| WDC       | WD82PURZ-85TEUY0   | 8 TB   | 19      | 553   | 0     | 1.52   |
| WDC       | WD2003FYYS-05T9B0  | 2 TB   | 4       | 609   | 1     | 1.51   |
| WDC       | WD10EZEX-60WN4A0   | 1 TB   | 1       | 545   | 0     | 1.49   |
| WDC       | WD50EFRX-68MYMN1   | 5 TB   | 2       | 2167  | 4     | 1.49   |
| WDC       | WD10EZEX-08WN4A0   | 1 TB   | 35      | 527   | 0     | 1.45   |
| WDC       | WD5000AZLX-22JKKA0 | 500 GB | 3       | 840   | 2     | 1.45   |
| WDC       | WD10JPVX-08JC3T5   | 1 TB   | 1       | 523   | 0     | 1.43   |
| WDC       | WD8003FRYZ-01JPDB1 | 8 TB   | 5       | 510   | 0     | 1.40   |
| WDC       | WD3000FYYZ-01UL1B3 | 3 TB   | 5       | 499   | 0     | 1.37   |
| WDC       | WD10EZRX-00D8PB0   | 1 TB   | 1       | 490   | 0     | 1.34   |
| WDC       | WD5000LPCX-24VHAT0 | 500 GB | 2       | 483   | 0     | 1.32   |
| WDC       | WD60PURZ-85ZUFY1   | 6 TB   | 6       | 477   | 0     | 1.31   |
| WDC       | WD60EFRX-68L0BN1   | 6 TB   | 182     | 1159  | 6     | 1.31   |
| WDC       | WD10JMVW-11AJGS4   | 1 TB   | 1       | 465   | 0     | 1.28   |
| WDC       | WD3200AAKS-00SBA0  | 320 GB | 1       | 2733  | 5     | 1.25   |
| WDC       | WD40EFAX-68JH4N0   | 4 TB   | 38      | 464   | 1     | 1.24   |
| WDC       | WD121KFBX-68EF5N0  | 12 TB  | 7       | 451   | 0     | 1.24   |
| WDC       | WD4005FZBX-00K5WB0 | 4 TB   | 8       | 445   | 0     | 1.22   |
| WDC       | WD3200AAJS-40VWA1  | 320 GB | 1       | 443   | 0     | 1.21   |
| WDC       | WD20PURX-64P6ZY0   | 2 TB   | 1       | 440   | 0     | 1.21   |
| WDC       | WD1005FBYZ-01YCBB3 | 1 TB   | 1       | 438   | 0     | 1.20   |
| WDC       | WD800JD-60LSA5     | 80 GB  | 1       | 434   | 0     | 1.19   |
| WDC       | WD5000AAKS-00UU3A0 | 500 GB | 2       | 1646  | 4     | 1.16   |
| WDC       | WD3000F9YZ-09N20L1 | 3 TB   | 1       | 419   | 0     | 1.15   |
| WDC       | WD1501FASS-00U0B0  | 1.5 TB | 1       | 3755  | 8     | 1.14   |
| WDC       | WD1503FYYS-02W0B0  | 1.5 TB | 2       | 3213  | 7     | 1.10   |
| WDC       | WD10EZEX-75WN4A1   | 1 TB   | 2       | 392   | 0     | 1.08   |
| WDC       | WD5000AZLX-00JKKA0 | 500 GB | 2       | 392   | 0     | 1.08   |
| WDC       | WD60EFAX-68SHWN0   | 6 TB   | 4       | 442   | 1     | 1.07   |
| WDC       | WD4003FRYZ-01F0DB0 | 4 TB   | 29      | 391   | 0     | 1.07   |
| WDC       | WD40EZAZ-00ZGHB0   | 4 TB   | 2       | 388   | 0     | 1.06   |
| WDC       | WD20EZRZ-00Z5HB0   | 2 TB   | 11      | 506   | 99    | 1.06   |
| WDC       | WD20PURZ-85GU6Y0   | 2 TB   | 7       | 386   | 0     | 1.06   |
| WDC       | WUH721414ALE604    | 14 TB  | 1998    | 376   | 1     | 1.03   |
| WDC       | WD3200BPVT-22JJ5T0 | 320 GB | 2       | 369   | 0     | 1.01   |
| WDC       | WD60EZRZ-00GZ5B1   | 6 TB   | 2       | 367   | 0     | 1.01   |
| WDC       | WD5000LPCX-00VHAT0 | 500 GB | 2       | 361   | 0     | 0.99   |
| WDC       | WD5000AZLX-00K2TA0 | 500 GB | 1       | 358   | 0     | 0.98   |
| WDC       | WD2005FBYZ-01YCBB3 | 2 TB   | 5       | 351   | 0     | 0.96   |
| WDC       | WD3000F9YZ-09N20L0 | 3 TB   | 1       | 1714  | 4     | 0.94   |
| WDC       | WD10EALS-002BA0    | 1 TB   | 2       | 2394  | 6     | 0.94   |
| WDC       | WD6003FZBX-00GXAB0 | 6 TB   | 7       | 337   | 0     | 0.92   |
| WDC       | WD20SPZX-00UA7T0   | 2 TB   | 5       | 336   | 0     | 0.92   |
| WDC       | WD4000FYYZ-01UL1B0 | 4 TB   | 20      | 2195  | 175   | 0.90   |
| WDC       | WD5000LPLX-08ZNTT0 | 500 GB | 2       | 323   | 0     | 0.89   |
| WDC       | WD20EZAZ-00GGJB0   | 2 TB   | 5       | 320   | 0     | 0.88   |
| WDC       | WD7500BPVT-22HXZT3 | 752 GB | 1       | 301   | 0     | 0.83   |
| WDC       | WD800BEVS-22RST0   | 80 GB  | 1       | 299   | 0     | 0.82   |
| WDC       | WD101EFAX-68LDBN0  | 10 TB  | 10      | 299   | 0     | 0.82   |
| WDC       | WD6003FFBX-68MU3N0 | 6 TB   | 9       | 295   | 0     | 0.81   |
| WDC       | WD6003FZBX-00K5WB0 | 6 TB   | 13      | 281   | 0     | 0.77   |
| WDC       | WD1200JS-00NCB1    | 120 GB | 1       | 3027  | 10    | 0.75   |
| WDC       | WD5001AALS-00L3B2  | 500 GB | 2       | 3341  | 515   | 0.74   |
| WDC       | WD3000FYYZ-01UL1B1 | 3 TB   | 1       | 2435  | 8     | 0.74   |
| WDC       | WD102KFBX-68M95N0  | 10 TB  | 32      | 262   | 0     | 0.72   |
| WDC       | WUS721010ALE6L4    | 10 TB  | 21      | 257   | 11    | 0.70   |
| WDC       | WD10EARS-00Y5B1    | 1 TB   | 4       | 1203  | 697   | 0.69   |
| WDC       | WD3200AAKS-22B3A0  | 320 GB | 1       | 250   | 0     | 0.69   |
| WDC       | WD1000DHTZ-04N21V0 | 1 TB   | 11      | 328   | 1     | 0.68   |
| WDC       | WD1001FALS-00J7B1  | 1 TB   | 1       | 246   | 0     | 0.67   |
| WDC       | WD10TMVW-11ZSMS1   | 1 TB   | 1       | 492   | 1     | 0.67   |
| WDC       | WD10EADS-11P8B1    | 1 TB   | 1       | 2191  | 8     | 0.67   |
| WDC       | WD2003FYYS-70W0B0  | 2 TB   | 1       | 240   | 0     | 0.66   |
| WDC       | WD5000AAKX-603CA0  | 500 GB | 1       | 239   | 0     | 0.66   |
| WDC       | WD15NMVW-11W68S0   | 1.5 TB | 1       | 233   | 0     | 0.64   |
| WDC       | WD5000AZRZ-00HTKB0 | 500 GB | 2       | 227   | 0     | 0.62   |
| WDC       | WD141KFGX-68FH9N0  | 14 TB  | 12      | 224   | 0     | 0.62   |
| WDC       | WD8004FRYZ-01VAEB0 | 8 TB   | 27      | 210   | 0     | 0.58   |
| WDC       | WD6003FRYZ-01F0DB0 | 6 TB   | 27      | 204   | 0     | 0.56   |
| WDC       | WD7500BPKT-00PK4T0 | 752 GB | 1       | 1623  | 7     | 0.56   |
| WDC       | WD10JPVX-22JC3T0   | 1 TB   | 2       | 191   | 0     | 0.52   |
| WDC       | WD62PURZ-85B3AY0   | 6 TB   | 3       | 182   | 0     | 0.50   |
| WDC       | WUH721414ALE6L1    | 14 TB  | 8       | 169   | 0     | 0.46   |
| WDC       | WUH721414ALE6L4    | 14 TB  | 94      | 168   | 1     | 0.46   |
| WDC       | WD5000AZLX-75K2TA0 | 500 GB | 1       | 162   | 0     | 0.44   |
| WDC       | WD20SPZX-21UA7T0   | 2 TB   | 2       | 150   | 0     | 0.41   |
| WDC       | WD20NPVX-00EA4T0   | 2 TB   | 4       | 1441  | 15    | 0.40   |
| WDC       | WD20SPZX-75UA7T1   | 2 TB   | 2       | 143   | 0     | 0.39   |
| WDC       | WD50EZRZ-00GZ5B1   | 5 TB   | 1       | 141   | 0     | 0.39   |
| WDC       | WD40NMZW-11GX6S1   | 4 TB   | 4       | 137   | 0     | 0.38   |
| WDC       | WD20PURZ-85AKKY0   | 2 TB   | 1       | 121   | 0     | 0.33   |
| WDC       | WD2001FASS-00W2B0  | 2 TB   | 3       | 1796  | 15    | 0.33   |
| WDC       | WD10EURX-63C57Y0   | 1 TB   | 1       | 1027  | 8     | 0.31   |
| WDC       | WD30EFAX-68JH4N0   | 3 TB   | 2       | 249   | 4     | 0.30   |
| WDC       | WD10JPVT-00A1YT0   | 1 TB   | 1       | 952   | 8     | 0.29   |
| WDC       | WD1003FBYX-20Y7B0  | 1 TB   | 2       | 102   | 0     | 0.28   |
| WDC       | WD2500AAKX-00ERMA0 | 250 GB | 2       | 507   | 4     | 0.28   |
| WDC       | WD40EFAX-68JH4N1   | 4 TB   | 4       | 100   | 0     | 0.28   |
| WDC       | WD20EZAZ-00L9GB0   | 2 TB   | 3       | 91    | 0     | 0.25   |
| WDC       | WD3200AVVS-56L2B0  | 320 GB | 4       | 91    | 0     | 0.25   |
| WDC       | WD1600AAJS-22L7A0  | 160 GB | 1       | 807   | 8     | 0.25   |
| WDC       | WD10SMZW-11Y0TS0   | 1 TB   | 2       | 88    | 0     | 0.24   |
| WDC       | WUH721818ALE604    | 18 TB  | 759     | 87    | 1     | 0.24   |
| WDC       | WD20EFAX-68B2RN1   | 2 TB   | 4       | 82    | 0     | 0.23   |
| WDC       | WD1600BEKT-00F3T0  | 160 GB | 2       | 1098  | 146   | 0.22   |
| WDC       | WD2500BEKT-60A25T1 | 250 GB | 1       | 399   | 4     | 0.22   |
| WDC       | WD4000F9YZ-76N20L1 | 4 TB   | 1       | 76    | 0     | 0.21   |
| WDC       | WUH721818ALE6L4    | 18 TB  | 201     | 72    | 0     | 0.20   |
| WDC       | WD102KRYZ-01A5AB0  | 10 TB  | 1       | 70    | 0     | 0.19   |
| WDC       | WD5000LPLX-60ZNTT2 | 500 GB | 1       | 63    | 0     | 0.17   |
| WDC       | WD50NDZW-11MR8S1   | 5 TB   | 2       | 60    | 0     | 0.17   |
| WDC       | WD102PURZ-85BXPY0  | 10 TB  | 10      | 56    | 0     | 0.15   |
| WDC       | WD20NMVW-11EDZS7   | 2 TB   | 1       | 54    | 0     | 0.15   |
| WDC       | WD60EFAX-68JH4N1   | 6 TB   | 2       | 51    | 0     | 0.14   |
| WDC       | WD5000AAKS-00TMA0  | 500 GB | 1       | 49    | 0     | 0.14   |
| WDC       | WD80EFBX-68AZZN0   | 8 TB   | 3       | 46    | 0     | 0.13   |
| WDC       | WD10SPZX-21Z10T0   | 1 TB   | 2       | 44    | 0     | 0.12   |
| WDC       | WD20EZRX-19DC0B0   | 2 TB   | 1       | 1541  | 35    | 0.12   |
| WDC       | WD80EMAZ-00WJTA0   | 8 TB   | 1       | 38    | 0     | 0.11   |
| WDC       | WUH721816ALE6L4    | 16 TB  | 20      | 36    | 0     | 0.10   |
| WDC       | WD40PURZ-85TTDY0   | 4 TB   | 2       | 33    | 0     | 0.09   |
| WDC       | WD2502ABYS-02B7A0  | 256 GB | 1       | 2202  | 67    | 0.09   |
| WDC       | WD20EFAX-68FB5N0   | 2 TB   | 4       | 30    | 0     | 0.08   |
| WDC       | WD4000FDYZ-27YA5B0 | 4 TB   | 5       | 711   | 115   | 0.08   |
| WDC       | WD10EZEX-00BBHA0   | 1 TB   | 2       | 28    | 0     | 0.08   |
| WDC       | WD3200AAKS-00L9A0  | 320 GB | 1       | 3625  | 242   | 0.04   |
| WDC       | WD60EZRX-00MVLB1   | 6 TB   | 1       | 1373  | 105   | 0.04   |
| WDC       | WD20SMZW-11JW8S1   | 2 TB   | 1       | 10    | 0     | 0.03   |
| WDC       | WD5000AZLX-60K2TA0 | 500 GB | 2       | 8     | 0     | 0.02   |
| WDC       | WD10EVVS-63M5B0    | 1 TB   | 1       | 73    | 8     | 0.02   |
| WDC       | WD40PURX-64GVNY0   | 4 TB   | 1       | 6     | 0     | 0.02   |
| WDC       | WD20SMZW-11JW8S0   | 2 TB   | 1       | 5     | 0     | 0.01   |
| WDC       | WD101EFBX-68B0AN0  | 10 TB  | 1       | 3     | 0     | 0.01   |
| WDC       | WD1600JS-75NCB2    | 160 GB | 1       | 2854  | 1502  | 0.01   |
| WDC       | WD5000AADS-00M2B0  | 500 GB | 1       | 2968  | 2023  | 0.00   |
| WDC       | WD2500BEVS-60UST0  | 250 GB | 1       | 400   | 413   | 0.00   |
| WDC       | WD2003FYPS-27Y2B0  | 2 TB   | 1       | 1747  | 2073  | 0.00   |
| WDC       | WD10JMVW-11S5XS0   | 1 TB   | 1       | 540   | 1016  | 0.00   |
| WDC       | WD2500AAKX-001CA0  | 250 GB | 1       | 449   | 1509  | 0.00   |

HDD by Family
-------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days — avg. days per sample,
Err  — avg. errors per sample,
MTBF — avg. MTBF in years per sample.

| MFG       | Family                 | Models | Samples | Days  | Err   | MTBF   |
|-----------|------------------------|--------|---------|-------|-------|--------|
| WDC       | Raptor                 | 1      | 1       | 4658  | 0     | 12.76  |
| WDC       | RE2                    | 1      | 5       | 2785  | 1     | 6.66   |
| WDC       | RE3                    | 12     | 63      | 2744  | 50    | 5.97   |
| WDC       | Caviar Black           | 22     | 242     | 1947  | 31    | 4.52   |
| WDC       | RE4-GP                 | 4      | 21      | 2516  | 261   | 4.49   |
| WDC       | Green                  | 12     | 62      | 1783  | 5     | 4.26   |
| WDC       | Caviar Green           | 12     | 29      | 2409  | 212   | 3.76   |
| WDC       | Black Mobile           | 6      | 30      | 1495  | 1     | 3.52   |
| WDC       | VelociRaptor           | 13     | 49      | 1374  | 1     | 3.39   |
| WDC       | RE4                    | 21     | 397     | 1502  | 9     | 3.36   |
| WDC       | Caviar Blue            | 24     | 33      | 1976  | 10    | 3.32   |
| WDC       | Se                     | 7      | 27      | 1131  | 2     | 2.59   |
| WDC       | Red                    | 26     | 907     | 1400  | 9     | 2.58   |
| WDC       | Caviar SE              | 6      | 6       | 1866  | 252   | 2.55   |
| WDC       | RE                     | 18     | 225     | 1542  | 38    | 2.55   |
| WDC       | Black                  | 11     | 448     | 921   | 4     | 2.41   |
| WDC       | Gold                   | 18     | 387     | 918   | 7     | 2.39   |
| WDC       | Blue                   | 55     | 288     | 979   | 24    | 2.25   |
| WDC       | Red Pro                | 12     | 136     | 695   | 3     | 1.79   |
| WDC       | Purple                 | 14     | 61      | 537   | 0     | 1.47   |
| WDC       | Elements / My Passport | 12     | 20      | 575   | 51    | 1.47   |
| WDC       | Scorpio Black          | 4      | 6       | 1074  | 51    | 1.12   |
| WDC       | Ultrastar DC HC530     | 2      | 2092    | 367   | 1     | 1.00   |
| WDC       | AV-GP                  | 5      | 9       | 450   | 2     | 0.94   |
| WDC       | Blue Mobile            | 12     | 24      | 330   | 0     | 0.91   |
| WDC       | Ultrastar DC HC330     | 1      | 21      | 257   | 11    | 0.70   |
| WDC       | Scorpio Blue           | 5      | 6       | 448   | 71    | 0.66   |
| WDC       | Shrek LT 2.5           | 1      | 1       | 233   | 0     | 0.64   |
| WDC       | Ultrastar DC HC500     | 1      | 8       | 169   | 0     | 0.46   |
| WDC       | Green Mobile           | 1      | 4       | 1441  | 15    | 0.40   |
| WDC       | Ultrastar DC HC550     | 3      | 980     | 83    | 1     | 0.23   |
| WDC       | HGST Ultrastar He10    | 1      | 1       | 38    | 0     | 0.11   |
| WDC       | Red Plus               | 2      | 4       | 35    | 0     | 0.10   |
