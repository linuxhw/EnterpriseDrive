HPE Hard Drives
===============

This is a list of all tested HPE hard drive models and their MTBFs. See more
info on reliability test in the [README](https://github.com/linuxhw/EnterpriseDrive).

Contents
--------

1. [ HDD by Model  ](#hdd-by-model)
2. [ HDD by Family ](#hdd-by-family)

HDD by Model
------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days — avg. days per sample,
Err  — avg. errors per sample,
MTBF — avg. MTBF in years per sample.

| MFG       | Model              | Size   | Samples | Days  | Err   | MTBF   |
|-----------|--------------------|--------|---------|-------|-------|--------|
| HPE       | MB0500GCEHE        | 500 GB | 6       | 1073  | 0     | 2.94   |
| HPE       | MM1000GFJTE        | 1 TB   | 4       | 862   | 0     | 2.36   |
| HPE       | MB0500EBNCR        | 500 GB | 5       | 1525  | 18    | 2.26   |
| HPE       | MM1000GBKAL        | 1 TB   | 21      | 1099  | 1     | 2.19   |
| HPE       | MM2000GEFRA        | 2 TB   | 241     | 824   | 1     | 2.11   |
| HPE       | MB004000GWFWB      | 4 TB   | 15      | 564   | 0     | 1.55   |
| HPE       | MB001000GWFGF      | 1 TB   | 1       | 431   | 0     | 1.18   |
| HPE       | MB8000GFECR        | 8 TB   | 56      | 353   | 28    | 0.90   |
| HPE       | MB0500EBZQA        | 500 GB | 1       | 2783  | 8     | 0.85   |
| HPE       | MB012000GWDFE      | 12 TB  | 35      | 281   | 0     | 0.77   |
| HPE       | MB008000GWRTC      | 8 TB   | 1       | 252   | 0     | 0.69   |
| HPE       | MB010000GWRTK      | 10 TB  | 24      | 190   | 0     | 0.52   |
| HPE       | MB008000GWJRT      | 8 TB   | 4       | 83    | 0     | 0.23   |
| HPE       | MB006000GWKGR      | 6 TB   | 1       | 51    | 0     | 0.14   |
| HPE       | MB006000GWWQT      | 6 TB   | 1       | 36    | 0     | 0.10   |

HDD by Family
-------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days — avg. days per sample,
Err  — avg. errors per sample,
MTBF — avg. MTBF in years per sample.

| MFG       | Family                 | Models | Samples | Days  | Err   | MTBF   |
|-----------|------------------------|--------|---------|-------|-------|--------|
| HPE       | WDC Enterprise         | 2      | 11      | 1278  | 8     | 2.63   |
| HPE       | Seagate Constellati... | 1      | 21      | 1099  | 1     | 2.19   |
| HPE       | Proliant HardDrive     | 9      | 303     | 731   | 1     | 1.89   |
| HPE       | Seagate Enterprise     | 3      | 81      | 335   | 19    | 0.79   |
