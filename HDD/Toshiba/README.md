Toshiba Hard Drives
===================

This is a list of all tested Toshiba hard drive models and their MTBFs. See more
info on reliability test in the [README](https://github.com/linuxhw/EnterpriseDrive).

Contents
--------

1. [ HDD by Model  ](#hdd-by-model)
2. [ HDD by Family ](#hdd-by-family)

HDD by Model
------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days — avg. days per sample,
Err  — avg. errors per sample,
MTBF — avg. MTBF in years per sample.

| MFG       | Model              | Size   | Samples | Days  | Err   | MTBF   |
|-----------|--------------------|--------|---------|-------|-------|--------|
| Toshiba   | DT01ABA300         | 3 TB   | 1       | 2720  | 0     | 7.45   |
| Toshiba   | MD04ACA500         | 5 TB   | 2       | 2046  | 0     | 5.61   |
| Toshiba   | MK0502TSKB         | 500 GB | 1       | 1752  | 0     | 4.80   |
| Toshiba   | MK1002TSKB         | 1 TB   | 10      | 1749  | 0     | 4.79   |
| Toshiba   | HDWE160            | 6 TB   | 74      | 1747  | 1     | 4.56   |
| Toshiba   | DT01ACA300         | 3 TB   | 77      | 1717  | 21    | 3.92   |
| Toshiba   | MG04ACA600E        | 6 TB   | 766     | 1435  | 15    | 3.73   |
| Toshiba   | MG04ACA200EY       | 2 TB   | 7       | 1270  | 0     | 3.48   |
| Toshiba   | DT01ACA100         | 1 TB   | 37      | 1415  | 28    | 3.48   |
| Toshiba   | MG04ACA600EY       | 6 TB   | 2       | 1236  | 0     | 3.39   |
| Toshiba   | MG03ACA100         | 1 TB   | 49      | 1238  | 2     | 3.34   |
| Toshiba   | MQ01ABF050         | 500 GB | 1       | 1175  | 0     | 3.22   |
| Toshiba   | MG04ACA300E        | 3 TB   | 1       | 1147  | 0     | 3.14   |
| Toshiba   | DT01ACA200         | 2 TB   | 287     | 1125  | 18    | 2.95   |
| Toshiba   | MG03ACA400         | 4 TB   | 24      | 1455  | 3     | 2.73   |
| Toshiba   | HDWD120            | 2 TB   | 9       | 993   | 0     | 2.72   |
| Toshiba   | MG04ACA400EY       | 4 TB   | 12      | 940   | 0     | 2.58   |
| Toshiba   | MG04ACA400N        | 4 TB   | 6       | 1201  | 2     | 2.55   |
| Toshiba   | MK2035GSS          | 200 GB | 1       | 927   | 0     | 2.54   |
| Toshiba   | HDWD105            | 500 GB | 5       | 907   | 0     | 2.49   |
| Toshiba   | DT01ACA050         | 500 GB | 19      | 1005  | 129   | 2.36   |
| Toshiba   | MG03ACA200         | 2 TB   | 7       | 920   | 288   | 2.35   |
| Toshiba   | HDWL120            | 2 TB   | 4       | 793   | 0     | 2.17   |
| Toshiba   | HDWA130            | 3 TB   | 1       | 699   | 0     | 1.92   |
| Toshiba   | MD04ACA400         | 4 TB   | 440     | 681   | 3     | 1.83   |
| Toshiba   | MG05ACA800E        | 8 TB   | 666     | 683   | 1     | 1.78   |
| Toshiba   | HDWR21C            | 12 TB  | 5       | 649   | 0     | 1.78   |
| Toshiba   | MG07ACA14TE        | 14 TB  | 46      | 662   | 1     | 1.76   |
| Toshiba   | HDWT140            | 4 TB   | 2       | 634   | 0     | 1.74   |
| Toshiba   | HDWU130            | 3 TB   | 8       | 715   | 8     | 1.71   |
| Toshiba   | HDWD130            | 3 TB   | 8       | 621   | 2     | 1.65   |
| Toshiba   | HDWE140            | 4 TB   | 42      | 595   | 43    | 1.58   |
| Toshiba   | HDWN160            | 6 TB   | 10      | 640   | 3     | 1.47   |
| Toshiba   | MG04ACA400E        | 4 TB   | 69      | 532   | 32    | 1.44   |
| Toshiba   | MG06ACA10TEY       | 10 TB  | 70      | 541   | 1     | 1.43   |
| Toshiba   | MG07ACA12TEY       | 12 TB  | 52      | 449   | 2     | 1.21   |
| Toshiba   | MG06ACA800EY       | 8 TB   | 15      | 430   | 0     | 1.18   |
| Toshiba   | HDWD110            | 1 TB   | 74      | 440   | 49    | 1.16   |
| Toshiba   | MG06ACA10TE        | 10 TB  | 123     | 427   | 1     | 1.16   |
| Toshiba   | MG03ACA300         | 3 TB   | 2       | 750   | 6     | 1.14   |
| Toshiba   | MD04ACA50D         | 5 TB   | 1       | 416   | 0     | 1.14   |
| Toshiba   | MG04ACA200N        | 2 TB   | 4       | 458   | 1     | 1.14   |
| Toshiba   | MG04ACA200E        | 2 TB   | 34      | 406   | 0     | 1.11   |
| Toshiba   | HDWQ140            | 4 TB   | 11      | 395   | 0     | 1.08   |
| Toshiba   | HDWR160            | 6 TB   | 1       | 364   | 0     | 1.00   |
| Toshiba   | MG06ACA600E        | 6 TB   | 10      | 363   | 0     | 1.00   |
| Toshiba   | MG07ACA12TE        | 12 TB  | 104     | 374   | 1     | 0.99   |
| Toshiba   | MK2002TSKB         | 2 TB   | 1       | 2835  | 8     | 0.86   |
| Toshiba   | MQ01ACF050         | 500 GB | 5       | 500   | 2     | 0.86   |
| Toshiba   | MG04ACA100NY       | 1 TB   | 7       | 359   | 12    | 0.84   |
| Toshiba   | MG04ACA400NY       | 4 TB   | 3       | 252   | 0     | 0.69   |
| Toshiba   | HDWG11A            | 10 TB  | 1       | 235   | 0     | 0.65   |
| Toshiba   | MG04ACA200NY       | 2 TB   | 4       | 215   | 0     | 0.59   |
| Toshiba   | MK1252GSX          | 120 GB | 1       | 199   | 0     | 0.55   |
| Toshiba   | HDWG180            | 8 TB   | 8       | 197   | 0     | 0.54   |
| Toshiba   | MQ01ABB200         | 2 TB   | 1       | 1702  | 8     | 0.52   |
| Toshiba   | HDWL110            | 1 TB   | 1       | 163   | 0     | 0.45   |
| Toshiba   | MQ04UBF100         | 1 TB   | 4       | 161   | 0     | 0.44   |
| Toshiba   | MQ01ACF032         | 320 GB | 1       | 151   | 0     | 0.41   |
| Toshiba   | HDWN180            | 8 TB   | 1       | 146   | 0     | 0.40   |
| Toshiba   | MQ01ABD100         | 1 TB   | 3       | 553   | 1093  | 0.32   |
| Toshiba   | MG06ACA800E        | 8 TB   | 86      | 108   | 0     | 0.30   |
| Toshiba   | HDWR180            | 8 TB   | 9       | 104   | 0     | 0.29   |
| Toshiba   | MG08ACA16TEY       | 16 TB  | 4       | 90    | 0     | 0.25   |
| Toshiba   | MQ01UBD050         | 500 GB | 1       | 69    | 0     | 0.19   |
| Toshiba   | MQ01UBD100         | 1 TB   | 2       | 142   | 8     | 0.19   |
| Toshiba   | MQ04ABF100         | 1 TB   | 2       | 267   | 1010  | 0.18   |
| Toshiba   | HDWD240            | 4 TB   | 3       | 61    | 0     | 0.17   |
| Toshiba   | MG08ACA16TE        | 16 TB  | 6       | 33    | 0     | 0.09   |
| Toshiba   | HDWF180            | 8 TB   | 2       | 33    | 0     | 0.09   |
| Toshiba   | MG08ACA16TA        | 16 TB  | 35      | 23    | 0     | 0.07   |
| Toshiba   | MK5061GSY          | 500 GB | 1       | 22    | 0     | 0.06   |
| Toshiba   | HDWR11A            | 10 TB  | 1       | 11    | 0     | 0.03   |
| Toshiba   | MG08ADA800E        | 8 TB   | 5       | 7     | 0     | 0.02   |

HDD by Family
-------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days — avg. days per sample,
Err  — avg. errors per sample,
MTBF — avg. MTBF in years per sample.

| MFG       | Family                 | Models | Samples | Days  | Err   | MTBF   |
|-----------|------------------------|--------|---------|-------|-------|--------|
| Toshiba   | 3.5" DT01ABA Deskto... | 1      | 1       | 2720  | 0     | 7.45   |
| Toshiba   | 3.5" HDD MK.002TSKB    | 2      | 11      | 1848  | 1     | 4.44   |
| Toshiba   | 3.5" MG04ACA Enterp... | 7      | 891     | 1317  | 16    | 3.43   |
| Toshiba   | 2.5" HDD MQ01ABF       | 1      | 1       | 1175  | 0     | 3.22   |
| Toshiba   | 3.5" HDD DT01ACA       | 4      | 420     | 1254  | 25    | 3.15   |
| Toshiba   | X300                   | 7      | 134     | 1186  | 14    | 3.11   |
| Toshiba   | 3.5" MG03ACAxxx(Y) ... | 4      | 82      | 1263  | 27    | 3.03   |
| Toshiba   | S300                   | 2      | 3       | 1006  | 0     | 2.76   |
| Toshiba   | 2.5" HDD               | 1      | 1       | 927   | 0     | 2.54   |
| Toshiba   | 3.5" HDD E300          | 1      | 1       | 699   | 0     | 1.92   |
| Toshiba   | 3.5" MD04ACA Enterp... | 2      | 442     | 687   | 3     | 1.85   |
| Toshiba   | L200                   | 2      | 5       | 667   | 0     | 1.83   |
| Toshiba   | MG05ACA Enterprise ... | 1      | 666     | 683   | 1     | 1.78   |
| Toshiba   | V300                   | 1      | 8       | 715   | 8     | 1.71   |
| Toshiba   | MG04ACA Enterprise HDD | 4      | 17      | 627   | 1     | 1.43   |
| Toshiba   | P300                   | 5      | 99      | 517   | 37    | 1.38   |
| Toshiba   | MG07ACA Enterprise ... | 3      | 202     | 459   | 1     | 1.22   |
| Toshiba   | Toshiba Client HDD     | 1      | 1       | 416   | 0     | 1.14   |
| Toshiba   | N300 NAS HDD           | 5      | 31      | 410   | 1     | 1.03   |
| Toshiba   | MG06ACA Enterprise ... | 5      | 304     | 361   | 1     | 0.97   |
| Toshiba   | Toshiba Enterprise     | 1      | 7       | 359   | 12    | 0.84   |
| Toshiba   | 2.5" HDD MQ01ACF       | 2      | 6       | 442   | 2     | 0.79   |
| Toshiba   | 2.5" HDD MK..52GSX     | 1      | 1       | 199   | 0     | 0.55   |
| Toshiba   | 2.5" HDD MQ01ABB       | 1      | 1       | 1702  | 8     | 0.52   |
| Toshiba   | 2.5" HDD MQ04UBF       | 1      | 4       | 161   | 0     | 0.44   |
| Toshiba   | 2.5" HDD MQ01ABD       | 1      | 3       | 553   | 1093  | 0.32   |
| Toshiba   | 2.5" HDD MQ01UBD       | 2      | 3       | 118   | 6     | 0.19   |
| Toshiba   | 2.5" HDD MQ04ABF       | 1      | 2       | 267   | 1010  | 0.18   |
| Toshiba   | MG08ACA Enterprise ... | 3      | 45      | 31    | 0     | 0.09   |
| Toshiba   | 2.5" HDD MK..61GSY[N]  | 1      | 1       | 22    | 0     | 0.06   |
| Toshiba   | MG08                   | 1      | 5       | 7     | 0     | 0.02   |
