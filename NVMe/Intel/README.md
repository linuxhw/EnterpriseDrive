Intel NVMe Drives
=================

This is a list of all tested Intel NVMe drive models and their MTBFs. See more
info on reliability test in the [README](https://github.com/linuxhw/EnterpriseDrive).

NVME by Model
------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days — avg. days per sample,
Err  — avg. errors per sample,
MTBF — avg. MTBF in years per sample.

| MFG       | Model              | Size   | Samples | Days  | Err   | MTBF   |
|-----------|--------------------|--------|---------|-------|-------|--------|
| Intel     | SSDPEDME016T4      | 1.6 TB | 1       | 1754  | 0     | 4.81   |
| Intel     | SSDPEDME020T4      | 2 TB   | 2       | 1696  | 0     | 4.65   |
| Intel     | SSDPEDME020T4D ... | 2 TB   | 2       | 1608  | 0     | 4.41   |
| Intel     | SSDPEDME400G4      | 400 GB | 19      | 1600  | 0     | 4.38   |
| Intel     | SSDPEDMW012T4      | 1.2 TB | 1       | 1576  | 0     | 4.32   |
| Intel     | SSDPE2MX012T4      | 1.2 TB | 4       | 1433  | 0     | 3.93   |
| Intel     | SSDPEDMX400G4      | 400 GB | 10      | 1432  | 0     | 3.93   |
| Intel     | SSDPEDME800G4      | 800 GB | 1       | 1416  | 0     | 3.88   |
| Intel     | SSDPEDMD400G4      | 400 GB | 210     | 1359  | 0     | 3.73   |
| Intel     | SSDPEDMD020T4D ... | 2 TB   | 5       | 1323  | 0     | 3.62   |
| Intel     | SSDPEDMD016T4      | 1.6 TB | 17      | 1319  | 0     | 3.62   |
| Intel     | SSDPE2KE016T7      | 1.6 TB | 2       | 1280  | 0     | 3.51   |
| Intel     | SSDPEDMX020T7      | 2 TB   | 46      | 1287  | 5     | 3.46   |
| Intel     | SSDPEDMW400G4      | 400 GB | 10      | 1229  | 0     | 3.37   |
| Intel     | SSDPEDMX012T7      | 1.2 TB | 3       | 1228  | 0     | 3.36   |
| Intel     | SSDPEDMD800G4      | 800 GB | 270     | 1189  | 0     | 3.26   |
| Intel     | SSDPE2KE020T7      | 2 TB   | 1       | 1180  | 0     | 3.23   |
| Intel     | SSDPE2MD400G4      | 400 GB | 11      | 1156  | 0     | 3.17   |
| Intel     | SSDPE2ME020T4      | 2 TB   | 2       | 1134  | 0     | 3.11   |
| Intel     | SSDPEK1W120GA      | 118 GB | 1       | 1054  | 0     | 2.89   |
| Intel     | SSDPEDME016T4S     | 1.6 TB | 4       | 1035  | 0     | 2.84   |
| Intel     | SSDPE2MX012T7      | 1.2 TB | 19      | 953   | 0     | 2.61   |
| Intel     | SSDPEDMD020T4      | 2 TB   | 1       | 897   | 0     | 2.46   |
| Intel     | SSDPE2MD400G4L     | 400 GB | 4       | 895   | 0     | 2.45   |
| Intel     | SSDPEDKX040T7      | 4 TB   | 45      | 890   | 23    | 2.33   |
| Intel     | SSDPE2ME012T4      | 1.2 TB | 7       | 828   | 0     | 2.27   |
| Intel     | SSDPEKKW256G7      | 256 GB | 17      | 836   | 1     | 2.24   |
| Intel     | SSDPE2MX450G7      | 450 GB | 44      | 812   | 0     | 2.23   |
| Intel     | SSDPE2KE076T8      | 7.6 TB | 2       | 778   | 0     | 2.13   |
| Intel     | SSDPEDMW800G4      | 800 GB | 7       | 774   | 0     | 2.12   |
| Intel     | SSDPE21K375GA      | 375 GB | 30      | 773   | 0     | 2.12   |
| Intel     | SSDPEDKE020T7      | 2 TB   | 25      | 770   | 0     | 2.11   |
| Intel     | SSDPEDMD016T4K     | 1.6 TB | 2       | 764   | 0     | 2.09   |
| Intel     | SSDPE2KX040T8      | 4 TB   | 60      | 762   | 18    | 2.05   |
| Intel     | VO0400KEFJB        | 400 GB | 4       | 743   | 0     | 2.04   |
| Intel     | SSDPEKKW512G7      | 512 GB | 1       | 728   | 0     | 2.00   |
| Intel     | MT0800KEXUU        | 800 GB | 18      | 713   | 0     | 1.95   |
| Intel     | SSDPED1D480GA      | 480 GB | 52      | 710   | 0     | 1.95   |
| Intel     | SSDPEKKF256G8 NVMe | 256 GB | 1       | 673   | 0     | 1.84   |
| Intel     | SSDPED1D280GA      | 280 GB | 21      | 720   | 1     | 1.83   |
| Intel     | SSDPE2KX020T7      | 2 TB   | 1       | 611   | 0     | 1.68   |
| Intel     | SSDPEDKE040T7      | 4 TB   | 1       | 604   | 0     | 1.66   |
| Intel     | SSDPEL1D380GA      | 384 GB | 6       | 562   | 0     | 1.54   |
| Intel     | SSDPEKKA256G7      | 256 GB | 4       | 557   | 0     | 1.53   |
| Intel     | SSDPE2KX080T8      | 8 TB   | 14      | 556   | 0     | 1.53   |
| Intel     | SSDPEKKW256G8      | 256 GB | 1       | 548   | 0     | 1.50   |
| Intel     | SSDPED1K375GA      | 375 GB | 77      | 535   | 0     | 1.47   |
| Intel     | SSDPE21D480GA      | 480 GB | 107     | 491   | 0     | 1.35   |
| Intel     | SSDPED1D960GAY     | 960 GB | 6       | 460   | 0     | 1.26   |
| Intel     | SSDPED1K750GA      | 752 GB | 135     | 456   | 0     | 1.25   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 164     | 439   | 0     | 1.21   |
| Intel     | SSDPE21K750GA      | 752 GB | 66      | 430   | 0     | 1.18   |
| Intel     | SSDPE2KX040T7      | 4 TB   | 4       | 405   | 0     | 1.11   |
| Intel     | SSDPE2KE032T8      | 3.2 TB | 29      | 404   | 0     | 1.11   |
| Intel     | SSDPE21K100GA      | 100 GB | 5       | 387   | 0     | 1.06   |
| Intel     | SSDPE2KX010T8      | 1 TB   | 133     | 384   | 0     | 1.05   |
| Intel     | SSDPEL1K100GA      | 100 GB | 11      | 368   | 0     | 1.01   |
| Intel     | SSDPEKKW512G8      | 512 GB | 9       | 367   | 0     | 1.01   |
| Intel     | SSDPEKKA512G8      | 512 GB | 23      | 352   | 0     | 0.97   |
| Intel     | SSDPE2MD016T4      | 1.6 TB | 4       | 349   | 0     | 0.96   |
| Intel     | SSDPEKNW020T8      | 2 TB   | 5       | 344   | 0     | 0.95   |
| Intel     | SSDPE2ME400G4      | 400 GB | 7       | 285   | 0     | 0.78   |
| Intel     | SSDPECKE064T7ES    | 3.2 TB | 2       | 273   | 0     | 0.75   |
| Intel     | SSDPEKKW010T8      | 1 TB   | 2       | 270   | 0     | 0.74   |
| Intel     | EO000375KWJUC      | 375 GB | 2       | 242   | 0     | 0.66   |
| Intel     | SSDPE2NU076T8      | 7.6 TB | 3       | 236   | 0     | 0.65   |
| Intel     | SSDPE2NV076T8      | 7.6 TB | 2       | 223   | 0     | 0.61   |
| Intel     | SSDPEKNW010T8      | 1 TB   | 8       | 210   | 0     | 0.58   |
| Intel     | SSDPE2KX010T7      | 1 TB   | 8       | 186   | 0     | 0.51   |
| Intel     | SSDPECME016T4      | 800 GB | 8       | 175   | 0     | 0.48   |
| Intel     | SSDPE2KE016T8      | 1.6 TB | 28      | 174   | 0     | 0.48   |
| Intel     | SSDPE2MD800G4      | 800 GB | 1       | 130   | 0     | 0.36   |
| Intel     | SSDPEKNW512G8      | 512 GB | 4       | 69    | 0     | 0.19   |
| Intel     | MDTPED1K750GA      | 752 GB | 2       | 41    | 0     | 0.11   |
| Intel     | SSDPF2KX076TZ      | 7.6 TB | 5       | 15    | 0     | 0.04   |
| Intel     | SSDPELKX010T8      | 1 TB   | 2       | 9     | 0     | 0.03   |
