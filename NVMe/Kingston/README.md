Kingston NVMe Drives
====================

This is a list of all tested Kingston NVMe drive models and their MTBFs. See more
info on reliability test in the [README](https://github.com/linuxhw/EnterpriseDrive).

NVME by Model
------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days — avg. days per sample,
Err  — avg. errors per sample,
MTBF — avg. MTBF in years per sample.

| MFG       | Model              | Size   | Samples | Days  | Err   | MTBF   |
|-----------|--------------------|--------|---------|-------|-------|--------|
| Kingston  | SKC1000960G        | 960 GB | 3       | 738   | 0     | 2.02   |
| Kingston  | SKC2000M81000G     | 1 TB   | 2       | 518   | 0     | 1.42   |
| Kingston  | SEDC1000M1920G     | 1.9 TB | 2       | 307   | 0     | 0.84   |
| Kingston  | SEDC1000M3840G     | 3.8 TB | 6       | 183   | 0     | 0.50   |
| Kingston  | SKC2500M8250G      | 250 GB | 5       | 161   | 0     | 0.44   |
| Kingston  | SA2000M81000G      | 1 TB   | 8       | 154   | 0     | 0.42   |
| Kingston  | SKC2500M81000G     | 1 TB   | 8       | 122   | 0     | 0.34   |
| Kingston  | SA2000M8500G       | 500 GB | 4       | 111   | 0     | 0.31   |
| Kingston  | SA2000M8250G       | 250 GB | 1       | 101   | 0     | 0.28   |
| Kingston  | SEDC1000M960G      | 960 GB | 2       | 100   | 0     | 0.27   |
| Kingston  | SKC2500M8500G      | 500 GB | 3       | 35    | 0     | 0.10   |
| Kingston  | SKC2500M82000G     | 2 TB   | 2       | 3     | 0     | 0.01   |
