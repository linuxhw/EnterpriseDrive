WDC NVMe Drives
===============

This is a list of all tested WDC NVMe drive models and their MTBFs. See more
info on reliability test in the [README](https://github.com/linuxhw/EnterpriseDrive).

NVME by Model
------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days — avg. days per sample,
Err  — avg. errors per sample,
MTBF — avg. MTBF in years per sample.

| MFG       | Model              | Size   | Samples | Days  | Err   | MTBF   |
|-----------|--------------------|--------|---------|-------|-------|--------|
| WDC       | PC SN720 SDAPNT... | 1 TB   | 1       | 824   | 0     | 2.26   |
| WDC       | WUS3BA196C7P3E3    | 960 GB | 83      | 513   | 0     | 1.41   |
| WDC       | WUS3BA138C7P3E3    | 3.8 TB | 20      | 503   | 0     | 1.38   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 178     | 479   | 0     | 1.31   |
| WDC       | CL SN720 SDAQNT... | 1 TB   | 46      | 383   | 0     | 1.05   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 152     | 375   | 0     | 1.03   |
| WDC       | CL SN720 SDAQNT... | 512 GB | 96      | 375   | 9     | 0.99   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 534     | 361   | 0     | 0.99   |
| WDC       | WDS250G3X0C-00SJG0 | 250 GB | 4       | 309   | 0     | 0.85   |
| WDC       | PC SN720 SDAPNT... | 256 GB | 15      | 296   | 0     | 0.81   |
| WDC       | WDS500G3X0C-00SJG0 | 500 GB | 23      | 252   | 0     | 0.69   |
| WDC       | WDS500G3XHC-00SJG0 | 500 GB | 6       | 239   | 0     | 0.66   |
| WDC       | WUS4BB038D7P3E1    | 3.8 TB | 71      | 198   | 0     | 0.54   |
| WDC       | WUS4BB038D7P3E3    | 3.8 TB | 37      | 197   | 0     | 0.54   |
| WDC       | WUS4BB076D7P3E1    | 7.6 TB | 12      | 191   | 0     | 0.53   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 365     | 165   | 0     | 0.45   |
| WDC       | WDS250G2B0C-00PXH0 | 250 GB | 3       | 153   | 0     | 0.42   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 465     | 144   | 0     | 0.40   |
| WDC       | WDS100T3X0C-00SJG0 | 1 TB   | 17      | 128   | 0     | 0.35   |
| WDC       | PC SN520 SDAPNU... | 128 GB | 2       | 126   | 0     | 0.35   |
| WDC       | WUS3BA119C7P3E3    | 1.9 TB | 7       | 107   | 0     | 0.29   |
| WDC       | PC SN530 NVMe      | 512 GB | 1       | 103   | 0     | 0.28   |
| WDC       | WUS4BB019D7P3E3    | 1.9 TB | 54      | 69    | 0     | 0.19   |
| WDC       | PC SN520 SDAPNU... | 256 GB | 1       | 47    | 0     | 0.13   |
| WDC       | WUS4BB096D7P3E4    | 960 GB | 4       | 43    | 0     | 0.12   |
| WDC       | WDS500G2B0C-00PXH0 | 500 GB | 1       | 24    | 0     | 0.07   |
| WDC       | WUS4BB038D7P3E4    | 3.8 TB | 6       | 24    | 0     | 0.07   |
| WDC       | WUS4CB032D7P3E4    | 3.2 TB | 5       | 9     | 0     | 0.03   |
| WDC       | PC SN730 SDBQNT... | 256 GB | 1       | 4     | 0     | 0.01   |
