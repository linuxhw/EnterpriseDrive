Micron NVMe Drives
==================

This is a list of all tested Micron NVMe drive models and their MTBFs. See more
info on reliability test in the [README](https://github.com/linuxhw/EnterpriseDrive).

NVME by Model
------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days — avg. days per sample,
Err  — avg. errors per sample,
MTBF — avg. MTBF in years per sample.

| MFG       | Model              | Size   | Samples | Days  | Err   | MTBF   |
|-----------|--------------------|--------|---------|-------|-------|--------|
| Micron    | MTFDHAL1T2MCF-1... | 1.2 TB | 1       | 1120  | 0     | 3.07   |
| Micron    | 2200S NVMe         | 256 GB | 1       | 333   | 0     | 0.91   |
| Micron    | 9300_MTFDHAL3T2TDR | 3.2 TB | 30      | 245   | 0     | 0.67   |
| Micron    | 9300_MTFDHAL7T6TDP | 7.6 TB | 33      | 166   | 0     | 0.46   |
| Micron    | 2200_MTFDHBA512TCK | 512 GB | 58      | 125   | 0     | 0.34   |
| Micron    | 7300_MTFDHBE960TDF | 960 GB | 4       | 115   | 0     | 0.32   |
| Micron    | 2200_MTFDHBA256TCK | 256 GB | 18      | 89    | 0     | 0.25   |
| Micron    | 7300_MTFDHBE1T6TDG | 1.6 TB | 2       | 64    | 0     | 0.18   |
| Micron    | 7300_MTFDHBE6T4TDG | 6.4 TB | 24      | 34    | 0     | 0.09   |
| Micron    | MTFDHBA1T0TDV-1... | 1 TB   | 1       | 21    | 0     | 0.06   |
| Micron    | 7300_MTFDHBG1T9TDF | 1.9 TB | 8       | 3     | 0     | 0.01   |
