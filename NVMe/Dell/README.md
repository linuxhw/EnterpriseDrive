Dell NVMe Drives
================

This is a list of all tested Dell NVMe drive models and their MTBFs. See more
info on reliability test in the [README](https://github.com/linuxhw/EnterpriseDrive).

NVME by Model
------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days — avg. days per sample,
Err  — avg. errors per sample,
MTBF — avg. MTBF in years per sample.

| MFG       | Model              | Size   | Samples | Days  | Err   | MTBF   |
|-----------|--------------------|--------|---------|-------|-------|--------|
| Dell      | Express Flash N... | 800 GB | 1       | 1865  | 0     | 5.11   |
| Dell      | Express Flash N... | 3.2 TB | 1       | 1428  | 0     | 3.91   |
| Dell      | Express Flash N... | 2 TB   | 14      | 1155  | 0     | 3.16   |
| Dell      | Express Flash N... | 1.6 TB | 19      | 920   | 54    | 2.37   |
| Dell      | Express Flash N... | 1.6 TB | 4       | 631   | 0     | 1.73   |
| Dell      | Express Flash N... | 3.2 TB | 4       | 538   | 0     | 1.47   |
| Dell      | Express Flash P... | 6.4 TB | 1       | 527   | 0     | 1.45   |
| Dell      | Express Flash P... | 800 GB | 8       | 517   | 0     | 1.42   |
| Dell      | Express Flash N... | 1.6 TB | 115     | 407   | 0     | 1.12   |
| Dell      | Express Flash P... | 1.6 TB | 4       | 327   | 0     | 0.90   |
| Dell      | Express Flash P... | 1.6 TB | 12      | 277   | 0     | 0.76   |
| Dell      | Express Flash P... | 1.6 TB | 1       | 259   | 0     | 0.71   |
| Dell      | Express Flash C... | 960 GB | 97      | 142   | 0     | 0.39   |
| Dell      | Ent NVMe P5600 ... | 1.6 TB | 8       | 108   | 0     | 0.30   |
| Dell      | Express Flash P... | 1.6 TB | 4       | 92    | 0     | 0.25   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 180     | 54    | 0     | 0.15   |
| Dell      | Express Flash N... | 6.4 TB | 4       | 39    | 0     | 0.11   |
