Toshiba NVMe Drives
===================

This is a list of all tested Toshiba NVMe drive models and their MTBFs. See more
info on reliability test in the [README](https://github.com/linuxhw/EnterpriseDrive).

NVME by Model
------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days — avg. days per sample,
Err  — avg. errors per sample,
MTBF — avg. MTBF in years per sample.

| MFG       | Model              | Size   | Samples | Days  | Err   | MTBF   |
|-----------|--------------------|--------|---------|-------|-------|--------|
| Toshiba   | THNSN5512GPU7      | 512 GB | 14      | 1021  | 0     | 2.80   |
| Toshiba   | KXG50ZNV256G       | 256 GB | 2       | 759   | 0     | 2.08   |
| Toshiba   | KXG50ZNV1T02       | 1 TB   | 6       | 685   | 0     | 1.88   |
| Toshiba   | KXG50ZNV512G       | 512 GB | 18      | 614   | 0     | 1.68   |
| Toshiba   | KXD51RUE960G       | 960 GB | 4       | 466   | 0     | 1.28   |
| Toshiba   | KXG60ZNV1T02       | 1 TB   | 38      | 289   | 0     | 0.79   |
| Toshiba   | KXD51RUE3T84       | 3.8 TB | 1       | 234   | 0     | 0.64   |
| Toshiba   | KXD51RUE1T92       | 1.9 TB | 10      | 194   | 0     | 0.53   |
| Toshiba   | KXG60ZNV512G       | 512 GB | 45      | 169   | 0     | 0.46   |
| Toshiba   | KXG60PNV2T04       | 2 TB   | 8       | 146   | 0     | 0.40   |
| Toshiba   | KXG60ZNV256G       | 256 GB | 55      | 86    | 0     | 0.24   |
| Toshiba   | KXG6AZNV512G       | 512 GB | 12      | 27    | 0     | 0.07   |
