Samsung NVMe Drives
===================

This is a list of all tested Samsung NVMe drive models and their MTBFs. See more
info on reliability test in the [README](https://github.com/linuxhw/EnterpriseDrive).

NVME by Model
------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days — avg. days per sample,
Err  — avg. errors per sample,
MTBF — avg. MTBF in years per sample.

| MFG       | Model              | Size   | Samples | Days  | Err   | MTBF   |
|-----------|--------------------|--------|---------|-------|-------|--------|
| Samsung   | SSD 950 PRO        | 512 GB | 6       | 1392  | 0     | 3.82   |
| Samsung   | MZVPV512HDGL-00000 | 512 GB | 3       | 1281  | 0     | 3.51   |
| Samsung   | MZ1LV480HCHP-00003 | 480 GB | 2       | 1263  | 0     | 3.46   |
| Samsung   | MZVLW512HMJP-000H1 | 512 GB | 2       | 1041  | 0     | 2.85   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | 87      | 1007  | 12    | 2.70   |
| Samsung   | SSD 960 EVO        | 500 GB | 6       | 922   | 0     | 2.53   |
| Samsung   | SSD 960 PRO        | 2 TB   | 1       | 921   | 0     | 2.52   |
| Samsung   | SSD 960 PRO        | 1 TB   | 10      | 845   | 0     | 2.32   |
| Samsung   | MZPLL3T2HMLS-00003 | 3.2 TB | 6       | 715   | 0     | 1.96   |
| Samsung   | MZVKW512HMJP-00000 | 512 GB | 5       | 858   | 202   | 1.90   |
| Samsung   | MZPLL6T4HMLA-00005 | 6.4 TB | 6       | 658   | 0     | 1.81   |
| Samsung   | MZQLW1T9HMJP-00003 | 1.9 TB | 2       | 639   | 0     | 1.75   |
| Samsung   | MZQLW960HMJP-00003 | 960 GB | 18      | 992   | 144   | 1.74   |
| Samsung   | MZWLL6T4HMLA-00005 | 6.4 TB | 2       | 629   | 0     | 1.72   |
| Samsung   | MZPLL6T4HMLS-00003 | 6.4 TB | 16      | 603   | 0     | 1.65   |
| Samsung   | SSD 983 DCT        | 960 GB | 2       | 591   | 0     | 1.62   |
| Samsung   | SSD 970 EVO        | 500 GB | 2       | 588   | 0     | 1.61   |
| Samsung   | SSD 960 EVO        | 250 GB | 2       | 495   | 0     | 1.36   |
| Samsung   | MZQLB3T8HALS-000AZ | 3.8 TB | 24      | 484   | 0     | 1.33   |
| Samsung   | MZWLL800HEHP-00003 | 800 GB | 14      | 757   | 4     | 1.30   |
| Samsung   | MZPLL3T2HAJQ-00005 | 3.2 TB | 24      | 474   | 0     | 1.30   |
| Samsung   | MZPLL6T4HMLS-000MV | 6.4 TB | 4       | 465   | 0     | 1.27   |
| Samsung   | MZ1LB960HAJQ-00007 | 960 GB | 28      | 447   | 0     | 1.23   |
| Samsung   | MZWLJ15THALA-00007 | 15.... | 2       | 429   | 0     | 1.18   |
| Samsung   | MZWLL12THMLA-00005 | 12.... | 2       | 422   | 0     | 1.16   |
| Samsung   | MZWLL1T6HAJQ-00005 | 1.6 TB | 10      | 420   | 0     | 1.15   |
| Samsung   | MZQLW1T9HMJP-000AZ | 1.9 TB | 7       | 435   | 1     | 1.11   |
| Samsung   | SSD 960 PRO        | 512 GB | 2       | 362   | 0     | 0.99   |
| Samsung   | MZPLL1T6HAJQ-00005 | 1.6 TB | 3       | 357   | 0     | 0.98   |
| Samsung   | MZVLB512HAJQ-00000 | 512 GB | 27      | 359   | 1     | 0.93   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 470     | 333   | 0     | 0.91   |
| Samsung   | VO001920KWVMT 1... | 1.9 TB | 36      | 320   | 0     | 0.88   |
| Samsung   | MZWLL1T6HEHP-00003 | 1.6 TB | 8       | 318   | 0     | 0.87   |
| Samsung   | SSD 970 PRO        | 1 TB   | 38      | 316   | 0     | 0.87   |
| Samsung   | MZSLW1T0HMLH-000L1 | 1 TB   | 2       | 312   | 0     | 0.85   |
| Samsung   | MZPJB480HMGC-0BW07 | 480 GB | 15      | 310   | 0     | 0.85   |
| Samsung   | MZVLB1T0HALR-00000 | 1 TB   | 27      | 306   | 1     | 0.82   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 124     | 304   | 1     | 0.82   |
| Samsung   | MZVLW128HEGR-000L1 | 128 GB | 1       | 282   | 0     | 0.77   |
| Samsung   | SSD 970 EVO Plus   | 1 TB   | 45      | 279   | 0     | 0.77   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 668     | 276   | 1     | 0.76   |
| Samsung   | SSD 970 PRO        | 512 GB | 83      | 266   | 0     | 0.73   |
| Samsung   | SSD 970 EVO        | 250 GB | 2       | 360   | 1     | 0.62   |
| Samsung   | MZWLJ7T6HALA-00007 | 7.6 TB | 2       | 226   | 0     | 0.62   |
| Samsung   | MZVLQ512HALU-00000 | 512 GB | 2       | 216   | 0     | 0.59   |
| Samsung   | MZ1LB3T8HMLA-00007 | 3.8 TB | 12      | 225   | 1     | 0.58   |
| Samsung   | SSD 970 EVO Plus   | 500 GB | 20      | 192   | 0     | 0.53   |
| Samsung   | MZVLW256HEHP-000H1 | 256 GB | 1       | 191   | 0     | 0.52   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | 106     | 182   | 0     | 0.50   |
| Samsung   | MZPLJ6T4HALA-00007 | 6.4 TB | 37      | 174   | 0     | 0.48   |
| Samsung   | MZPJB960HMGC-0BW07 | 960 GB | 11      | 161   | 0     | 0.44   |
| Samsung   | MZ1LB1T9HALS-00007 | 1.9 TB | 3       | 158   | 0     | 0.43   |
| Samsung   | MZPLJ3T2HBJR-00007 | 3.2 TB | 4       | 157   | 0     | 0.43   |
| Samsung   | MZ4LB3T8HALS-00003 | 3.8 TB | 6       | 140   | 0     | 0.38   |
| Samsung   | SSD 970 EVO        | 1 TB   | 7       | 339   | 2     | 0.32   |
| Samsung   | SSD 970 EVO Plus   | 2 TB   | 8       | 113   | 0     | 0.31   |
| Samsung   | SSD 970 EVO Plus   | 250 GB | 22      | 108   | 0     | 0.30   |
| Samsung   | MZQL2960HCJR-00A07 | 960 GB | 2       | 96    | 0     | 0.26   |
| Samsung   | MZVLB1T0HBLR-00000 | 1 TB   | 44      | 95    | 0     | 0.26   |
| Samsung   | MZPLJ3T2HBJR-000H3 | 3.2 TB | 6       | 87    | 0     | 0.24   |
| Samsung   | MZVLB256HAHQ-00000 | 256 GB | 14      | 84    | 0     | 0.23   |
| Samsung   | MZQLB1T9HAJR-000AZ | 1.9 TB | 4       | 80    | 0     | 0.22   |
| Samsung   | MT001600KWHAC 1... | 1.6 TB | 1       | 77    | 0     | 0.21   |
| Samsung   | MZVLB512HBJQ-00000 | 512 GB | 15      | 69    | 0     | 0.19   |
| Samsung   | MZVLB256HBHQ-00000 | 256 GB | 6       | 62    | 0     | 0.17   |
| Samsung   | MZPLJ1T6HBJR-000H3 | 1.6 TB | 8       | 56    | 0     | 0.15   |
| Samsung   | SSD 980            | 500 GB | 3       | 51    | 0     | 0.14   |
| Samsung   | MZVLB256HAHQ-000L7 | 256 GB | 3       | 43    | 0     | 0.12   |
| Samsung   | MZQL23T8HCLS-00... | 3.8 TB | 1       | 43    | 0     | 0.12   |
| Samsung   | SSD 980 PRO        | 250 GB | 2       | 37    | 0     | 0.10   |
| Samsung   | MZVL21T0HCLR-00B00 | 1 TB   | 6       | 28    | 0     | 0.08   |
| Samsung   | MZVLB256HBHQ-000L7 | 256 GB | 3       | 2     | 0     | 0.01   |
| Samsung   | MZVLW128HEGR-00000 | 128 GB | 2       | 1     | 0     | 0.00   |
| Samsung   | MZVLW1T0HMLH-000L7 | 1 TB   | 1       | 0     | 0     | 0.00   |
| Samsung   | SSD 980            | 250 GB | 4       | 0     | 0     | 0.00   |
| Samsung   | SSD 980            | 1 TB   | 2       | 0     | 0     | 0.00   |
