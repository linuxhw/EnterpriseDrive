Appendix 6: Top 1000 NVMe Models
================================

See more info on reliability test in the [README](https://github.com/linuxhw/EnterpriseDrive).

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days — avg. days per sample,
Err  — avg. errors per sample,
MTBF — avg. MTBF in years per sample.

| MFG       | Model              | Size   | Samples | Days  | Err   | MTBF   |
|-----------|--------------------|--------|---------|-------|-------|--------|
| Dell      | Express Flash N... | 800 GB | 1       | 1865  | 0     | 5.11   |
| Intel     | SSDPEDME016T4      | 1.6 TB | 1       | 1754  | 0     | 4.81   |
| Intel     | SSDPEDME020T4      | 2 TB   | 2       | 1696  | 0     | 4.65   |
| Intel     | SSDPEDME020T4D ... | 2 TB   | 2       | 1608  | 0     | 4.41   |
| Intel     | SSDPEDME400G4      | 400 GB | 19      | 1600  | 0     | 4.38   |
| Intel     | SSDPEDMW012T4      | 1.2 TB | 1       | 1576  | 0     | 4.32   |
| Intel     | SSDPE2MX012T4      | 1.2 TB | 4       | 1433  | 0     | 3.93   |
| Intel     | SSDPEDMX400G4      | 400 GB | 10      | 1432  | 0     | 3.93   |
| Dell      | Express Flash N... | 3.2 TB | 1       | 1428  | 0     | 3.91   |
| Intel     | SSDPEDME800G4      | 800 GB | 1       | 1416  | 0     | 3.88   |
| Samsung   | SSD 950 PRO        | 512 GB | 6       | 1392  | 0     | 3.82   |
| Intel     | SSDPEDMD400G4      | 400 GB | 210     | 1359  | 0     | 3.73   |
| Intel     | SSDPEDMD020T4D ... | 2 TB   | 5       | 1323  | 0     | 3.62   |
| Intel     | SSDPEDMD016T4      | 1.6 TB | 17      | 1319  | 0     | 3.62   |
| Samsung   | MZVPV512HDGL-00000 | 512 GB | 3       | 1281  | 0     | 3.51   |
| Intel     | SSDPE2KE016T7      | 1.6 TB | 2       | 1280  | 0     | 3.51   |
| Samsung   | MZ1LV480HCHP-00003 | 480 GB | 2       | 1263  | 0     | 3.46   |
| Intel     | SSDPEDMX020T7      | 2 TB   | 46      | 1287  | 5     | 3.46   |
| Intel     | SSDPEDMW400G4      | 400 GB | 10      | 1229  | 0     | 3.37   |
| Intel     | SSDPEDMX012T7      | 1.2 TB | 3       | 1228  | 0     | 3.36   |
| Intel     | SSDPEDMD800G4      | 800 GB | 270     | 1189  | 0     | 3.26   |
| Intel     | SSDPE2KE020T7      | 2 TB   | 1       | 1180  | 0     | 3.23   |
| Intel     | SSDPE2MD400G4      | 400 GB | 11      | 1156  | 0     | 3.17   |
| Dell      | Express Flash N... | 2 TB   | 14      | 1155  | 0     | 3.16   |
| Intel     | SSDPE2ME020T4      | 2 TB   | 2       | 1134  | 0     | 3.11   |
| Micron    | MTFDHAL1T2MCF-1... | 1.2 TB | 1       | 1120  | 0     | 3.07   |
| Intel     | SSDPEK1W120GA      | 118 GB | 1       | 1054  | 0     | 2.89   |
| Samsung   | MZVLW512HMJP-000H1 | 512 GB | 2       | 1041  | 0     | 2.85   |
| Intel     | SSDPEDME016T4S     | 1.6 TB | 4       | 1035  | 0     | 2.84   |
| Toshiba   | THNSN5512GPU7      | 512 GB | 14      | 1021  | 0     | 2.80   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | 87      | 1007  | 12    | 2.70   |
| Intel     | SSDPE2MX012T7      | 1.2 TB | 19      | 953   | 0     | 2.61   |
| Samsung   | SSD 960 EVO        | 500 GB | 6       | 922   | 0     | 2.53   |
| Samsung   | SSD 960 PRO        | 2 TB   | 1       | 921   | 0     | 2.52   |
| Intel     | SSDPEDMD020T4      | 2 TB   | 1       | 897   | 0     | 2.46   |
| Intel     | SSDPE2MD400G4L     | 400 GB | 4       | 895   | 0     | 2.45   |
| Dell      | Express Flash N... | 1.6 TB | 19      | 920   | 54    | 2.37   |
| Intel     | SSDPEDKX040T7      | 4 TB   | 45      | 890   | 23    | 2.33   |
| Samsung   | SSD 960 PRO        | 1 TB   | 10      | 845   | 0     | 2.32   |
| Intel     | SSDPE2ME012T4      | 1.2 TB | 7       | 828   | 0     | 2.27   |
| WDC       | PC SN720 SDAPNT... | 1 TB   | 1       | 824   | 0     | 2.26   |
| Intel     | SSDPEKKW256G7      | 256 GB | 17      | 836   | 1     | 2.24   |
| Intel     | SSDPE2MX450G7      | 450 GB | 44      | 812   | 0     | 2.23   |
| SPCC      | M.2 PCIe SSD       | 1 TB   | 1       | 789   | 0     | 2.16   |
| Intel     | SSDPE2KE076T8      | 7.6 TB | 2       | 778   | 0     | 2.13   |
| Intel     | SSDPEDMW800G4      | 800 GB | 7       | 774   | 0     | 2.12   |
| Intel     | SSDPE21K375GA      | 375 GB | 30      | 773   | 0     | 2.12   |
| Intel     | SSDPEDKE020T7      | 2 TB   | 25      | 770   | 0     | 2.11   |
| Intel     | SSDPEDMD016T4K     | 1.6 TB | 2       | 764   | 0     | 2.09   |
| Toshiba   | KXG50ZNV256G       | 256 GB | 2       | 759   | 0     | 2.08   |
| Intel     | SSDPE2KX040T8      | 4 TB   | 60      | 762   | 18    | 2.05   |
| Intel     | VO0400KEFJB        | 400 GB | 4       | 743   | 0     | 2.04   |
| Huawei    | HWE36P43016M000N   | 1.6 TB | 2       | 741   | 0     | 2.03   |
| Kingston  | SKC1000960G        | 960 GB | 3       | 738   | 0     | 2.02   |
| Intel     | SSDPEKKW512G7      | 512 GB | 1       | 728   | 0     | 2.00   |
| Samsung   | MZPLL3T2HMLS-00003 | 3.2 TB | 6       | 715   | 0     | 1.96   |
| Intel     | MT0800KEXUU        | 800 GB | 18      | 713   | 0     | 1.95   |
| Intel     | SSDPED1D480GA      | 480 GB | 52      | 710   | 0     | 1.95   |
| Samsung   | MZVKW512HMJP-00000 | 512 GB | 5       | 858   | 202   | 1.90   |
| Toshiba   | KXG50ZNV1T02       | 1 TB   | 6       | 685   | 0     | 1.88   |
| Intel     | SSDPEKKF256G8 NVMe | 256 GB | 1       | 673   | 0     | 1.84   |
| Intel     | SSDPED1D280GA      | 280 GB | 21      | 720   | 1     | 1.83   |
| Samsung   | MZPLL6T4HMLA-00005 | 6.4 TB | 6       | 658   | 0     | 1.81   |
| Samsung   | MZQLW1T9HMJP-00003 | 1.9 TB | 2       | 639   | 0     | 1.75   |
| Samsung   | MZQLW960HMJP-00003 | 960 GB | 18      | 992   | 144   | 1.74   |
| Dell      | Express Flash N... | 1.6 TB | 4       | 631   | 0     | 1.73   |
| Samsung   | MZWLL6T4HMLA-00005 | 6.4 TB | 2       | 629   | 0     | 1.72   |
| Toshiba   | KXG50ZNV512G       | 512 GB | 18      | 614   | 0     | 1.68   |
| Intel     | SSDPE2KX020T7      | 2 TB   | 1       | 611   | 0     | 1.68   |
| Intel     | SSDPEDKE040T7      | 4 TB   | 1       | 604   | 0     | 1.66   |
| Samsung   | MZPLL6T4HMLS-00003 | 6.4 TB | 16      | 603   | 0     | 1.65   |
| Samsung   | SSD 983 DCT        | 960 GB | 2       | 591   | 0     | 1.62   |
| PNY       | CS3030 500GB SSD   | 500 GB | 2       | 589   | 0     | 1.62   |
| Samsung   | SSD 970 EVO        | 500 GB | 2       | 588   | 0     | 1.61   |
| Intel     | SSDPEL1D380GA      | 384 GB | 6       | 562   | 0     | 1.54   |
| Intel     | SSDPEKKA256G7      | 256 GB | 4       | 557   | 0     | 1.53   |
| Intel     | SSDPE2KX080T8      | 8 TB   | 14      | 556   | 0     | 1.53   |
| Intel     | SSDPEKKW256G8      | 256 GB | 1       | 548   | 0     | 1.50   |
| Dell      | Express Flash N... | 3.2 TB | 4       | 538   | 0     | 1.47   |
| Intel     | SSDPED1K375GA      | 375 GB | 77      | 535   | 0     | 1.47   |
| Corsair   | Force MP600        | 1 TB   | 1       | 531   | 0     | 1.46   |
| Dell      | Express Flash P... | 6.4 TB | 1       | 527   | 0     | 1.45   |
| Kingston  | SKC2000M81000G     | 1 TB   | 2       | 518   | 0     | 1.42   |
| Dell      | Express Flash P... | 800 GB | 8       | 517   | 0     | 1.42   |
| WDC       | WUS3BA196C7P3E3    | 960 GB | 83      | 513   | 0     | 1.41   |
| WDC       | WUS3BA138C7P3E3    | 3.8 TB | 20      | 503   | 0     | 1.38   |
| Samsung   | SSD 960 EVO        | 250 GB | 2       | 495   | 0     | 1.36   |
| Smartbuy  | m.2 PS5013T-2280T  | 128 GB | 1       | 493   | 0     | 1.35   |
| Intel     | SSDPE21D480GA      | 480 GB | 107     | 491   | 0     | 1.35   |
| Samsung   | MZQLB3T8HALS-000AZ | 3.8 TB | 24      | 484   | 0     | 1.33   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 178     | 479   | 0     | 1.31   |
| Samsung   | MZWLL800HEHP-00003 | 800 GB | 14      | 757   | 4     | 1.30   |
| Samsung   | MZPLL3T2HAJQ-00005 | 3.2 TB | 24      | 474   | 0     | 1.30   |
| Toshiba   | KXD51RUE960G       | 960 GB | 4       | 466   | 0     | 1.28   |
| Samsung   | MZPLL6T4HMLS-000MV | 6.4 TB | 4       | 465   | 0     | 1.27   |
| Intel     | SSDPED1D960GAY     | 960 GB | 6       | 460   | 0     | 1.26   |
| Intel     | SSDPED1K750GA      | 752 GB | 135     | 456   | 0     | 1.25   |
| Samsung   | MZ1LB960HAJQ-00007 | 960 GB | 28      | 447   | 0     | 1.23   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 164     | 439   | 0     | 1.21   |
| Wester... | WUS3BA196C7P3E3    | 960 GB | 83      | 439   | 0     | 1.20   |
| Intel     | SSDPE21K750GA      | 752 GB | 66      | 430   | 0     | 1.18   |
| Samsung   | MZWLJ15THALA-00007 | 15.... | 2       | 429   | 0     | 1.18   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 178     | 428   | 0     | 1.17   |
| Wester... | WUS3BA119C7P3E3    | 960 GB | 1       | 424   | 0     | 1.16   |
| Wester... | WUS3BA138C7P3E3    | 3.8 TB | 20      | 424   | 0     | 1.16   |
| Samsung   | MZWLL12THMLA-00005 | 12.... | 2       | 422   | 0     | 1.16   |
| Samsung   | MZWLL1T6HAJQ-00005 | 1.6 TB | 10      | 420   | 0     | 1.15   |
| Dell      | Express Flash N... | 1.6 TB | 115     | 407   | 0     | 1.12   |
| Samsung   | MZQLW1T9HMJP-000AZ | 1.9 TB | 7       | 435   | 1     | 1.11   |
| Intel     | SSDPE2KX040T7      | 4 TB   | 4       | 405   | 0     | 1.11   |
| Intel     | SSDPE2KE032T8      | 3.2 TB | 29      | 404   | 0     | 1.11   |
| Corsair   | Force MP510 1.9TB  | 1.9 TB | 12      | 391   | 0     | 1.07   |
| Intel     | SSDPE21K100GA      | 100 GB | 5       | 387   | 0     | 1.06   |
| Intel     | SSDPE2KX010T8      | 1 TB   | 133     | 384   | 0     | 1.05   |
| WDC       | CL SN720 SDAQNT... | 1 TB   | 46      | 383   | 0     | 1.05   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 152     | 375   | 0     | 1.03   |
| Intel     | SSDPEL1K100GA      | 100 GB | 11      | 368   | 0     | 1.01   |
| Intel     | SSDPEKKW512G8      | 512 GB | 9       | 367   | 0     | 1.01   |
| WDC       | CL SN720 SDAQNT... | 512 GB | 96      | 375   | 9     | 0.99   |
| Samsung   | SSD 960 PRO        | 512 GB | 2       | 362   | 0     | 0.99   |
| Phison    | PCIe SSD           | 256 GB | 1       | 362   | 0     | 0.99   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 534     | 361   | 0     | 0.99   |
| Samsung   | MZPLL1T6HAJQ-00005 | 1.6 TB | 3       | 357   | 0     | 0.98   |
| Intel     | SSDPEKKA512G8      | 512 GB | 23      | 352   | 0     | 0.97   |
| Intel     | SSDPE2MD016T4      | 1.6 TB | 4       | 349   | 0     | 0.96   |
| Intel     | SSDPEKNW020T8      | 2 TB   | 5       | 344   | 0     | 0.95   |
| ADATA     | SX8200PNP          | 1 TB   | 1       | 341   | 0     | 0.94   |
| Samsung   | MZVLB512HAJQ-00000 | 512 GB | 27      | 359   | 1     | 0.93   |
| Phison    | Sabrent Rocket Q   | 1 TB   | 3       | 333   | 0     | 0.91   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 470     | 333   | 0     | 0.91   |
| Micron    | 2200S NVMe         | 256 GB | 1       | 333   | 0     | 0.91   |
| HP        | SSD EX950          | 2 TB   | 10      | 332   | 0     | 0.91   |
| Phison    | Viper M.2 VPN100   | 2 TB   | 7       | 330   | 0     | 0.91   |
| Dell      | Express Flash P... | 1.6 TB | 4       | 327   | 0     | 0.90   |
| Crucial   | CT250P2SSD8        | 250 GB | 1       | 326   | 0     | 0.89   |
| Samsung   | VO001920KWVMT 1... | 1.9 TB | 36      | 320   | 0     | 0.88   |
| Samsung   | MZWLL1T6HEHP-00003 | 1.6 TB | 8       | 318   | 0     | 0.87   |
| Samsung   | SSD 970 PRO        | 1 TB   | 38      | 316   | 0     | 0.87   |
| Samsung   | MZSLW1T0HMLH-000L1 | 1 TB   | 2       | 312   | 0     | 0.85   |
| Samsung   | MZPJB480HMGC-0BW07 | 480 GB | 15      | 310   | 0     | 0.85   |
| WDC       | WDS250G3X0C-00SJG0 | 250 GB | 4       | 309   | 0     | 0.85   |
| Kingston  | SEDC1000M1920G     | 1.9 TB | 2       | 307   | 0     | 0.84   |
| Samsung   | MZVLB1T0HALR-00000 | 1 TB   | 27      | 306   | 1     | 0.82   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 124     | 304   | 1     | 0.82   |
| WDC       | PC SN720 SDAPNT... | 256 GB | 15      | 296   | 0     | 0.81   |
| HGST      | HUSMR7638BDP3Y1    | 3.8 TB | 24      | 290   | 0     | 0.80   |
| Toshiba   | KXG60ZNV1T02       | 1 TB   | 38      | 289   | 0     | 0.79   |
| Seagate   | FireCuda 520 SS... | 500 GB | 10      | 287   | 0     | 0.79   |
| Intel     | SSDPE2ME400G4      | 400 GB | 7       | 285   | 0     | 0.78   |
| Samsung   | MZVLW128HEGR-000L1 | 128 GB | 1       | 282   | 0     | 0.77   |
| Samsung   | SSD 970 EVO Plus   | 1 TB   | 45      | 279   | 0     | 0.77   |
| Dell      | Express Flash P... | 1.6 TB | 12      | 277   | 0     | 0.76   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 668     | 276   | 1     | 0.76   |
| Intel     | SSDPECKE064T7ES    | 3.2 TB | 2       | 273   | 0     | 0.75   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 443     | 273   | 0     | 0.75   |
| Intel     | SSDPEKKW010T8      | 1 TB   | 2       | 270   | 0     | 0.74   |
| Samsung   | SSD 970 PRO        | 512 GB | 83      | 266   | 0     | 0.73   |
| ADATA     | SX8200PNP          | 256 GB | 2       | 264   | 0     | 0.72   |
| Dell      | Express Flash P... | 1.6 TB | 1       | 259   | 0     | 0.71   |
| Gigabyte  | GP-GSM2NE3100TNTD  | 1 TB   | 10      | 254   | 0     | 0.70   |
| WDC       | WDS500G3X0C-00SJG0 | 500 GB | 23      | 252   | 0     | 0.69   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 152     | 246   | 0     | 0.68   |
| Micron    | 9300_MTFDHAL3T2TDR | 3.2 TB | 30      | 245   | 0     | 0.67   |
| Intel     | EO000375KWJUC      | 375 GB | 2       | 242   | 0     | 0.66   |
| WDC       | WDS500G3XHC-00SJG0 | 500 GB | 6       | 239   | 0     | 0.66   |
| Intel     | SSDPE2NU076T8      | 7.6 TB | 3       | 236   | 0     | 0.65   |
| Toshiba   | KXD51RUE3T84       | 3.8 TB | 1       | 234   | 0     | 0.64   |
| Samsung   | SSD 970 EVO        | 250 GB | 2       | 360   | 1     | 0.62   |
| Samsung   | MZWLJ7T6HALA-00007 | 7.6 TB | 2       | 226   | 0     | 0.62   |
| Intel     | SSDPE2NV076T8      | 7.6 TB | 2       | 223   | 0     | 0.61   |
| Samsung   | MZVLQ512HALU-00000 | 512 GB | 2       | 216   | 0     | 0.59   |
| Samsung   | MZ1LB3T8HMLA-00007 | 3.8 TB | 12      | 225   | 1     | 0.58   |
| Intel     | SSDPEKNW010T8      | 1 TB   | 8       | 210   | 0     | 0.58   |
| WDC       | WUS4BB038D7P3E1    | 3.8 TB | 71      | 198   | 0     | 0.54   |
| WDC       | WUS4BB038D7P3E3    | 3.8 TB | 37      | 197   | 0     | 0.54   |
| Patriot   | P300               | 256 GB | 2       | 196   | 0     | 0.54   |
| Toshiba   | KXD51RUE1T92       | 1.9 TB | 10      | 194   | 0     | 0.53   |
| SK hynix  | PC601 NVMe         | 256 GB | 1       | 192   | 0     | 0.53   |
| Samsung   | SSD 970 EVO Plus   | 500 GB | 20      | 192   | 0     | 0.53   |
| WDC       | WUS4BB076D7P3E1    | 7.6 TB | 12      | 191   | 0     | 0.53   |
| Samsung   | MZVLW256HEHP-000H1 | 256 GB | 1       | 191   | 0     | 0.52   |
| Intel     | SSDPE2KX010T7      | 1 TB   | 8       | 186   | 0     | 0.51   |
| Kingston  | SEDC1000M3840G     | 3.8 TB | 6       | 183   | 0     | 0.50   |
| HP        | SSD EX950          | 512 GB | 7       | 183   | 0     | 0.50   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | 106     | 182   | 0     | 0.50   |
| Silico... | NVME SSD           | 512 GB | 1       | 177   | 0     | 0.49   |
| Intel     | SSDPECME016T4      | 800 GB | 8       | 175   | 0     | 0.48   |
| Samsung   | MZPLJ6T4HALA-00007 | 6.4 TB | 37      | 174   | 0     | 0.48   |
| Intel     | SSDPE2KE016T8      | 1.6 TB | 28      | 174   | 0     | 0.48   |
| Toshiba   | KXG60ZNV512G       | 512 GB | 45      | 169   | 0     | 0.46   |
| XPG       | SPECTRIX S40G      | 1 TB   | 1       | 168   | 0     | 0.46   |
| Micron    | 9300_MTFDHAL7T6TDP | 7.6 TB | 33      | 166   | 0     | 0.46   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 365     | 165   | 0     | 0.45   |
| Crucial   | CT500P1SSD8        | 500 GB | 2       | 163   | 0     | 0.45   |
| Patriot   | P300               | 128 GB | 4       | 163   | 0     | 0.45   |
| Wester... | WUS4BB038D7P3E3    | 3.8 TB | 13      | 162   | 0     | 0.44   |
| Samsung   | MZPJB960HMGC-0BW07 | 960 GB | 11      | 161   | 0     | 0.44   |
| Kingston  | SKC2500M8250G      | 250 GB | 5       | 161   | 0     | 0.44   |
| Samsung   | MZ1LB1T9HALS-00007 | 1.9 TB | 3       | 158   | 0     | 0.43   |
| Samsung   | MZPLJ3T2HBJR-00007 | 3.2 TB | 4       | 157   | 0     | 0.43   |
| Kingston  | SA2000M81000G      | 1 TB   | 8       | 154   | 0     | 0.42   |
| WDC       | WDS250G2B0C-00PXH0 | 250 GB | 3       | 153   | 0     | 0.42   |
| Toshiba   | KXG60PNV2T04       | 2 TB   | 8       | 146   | 0     | 0.40   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 465     | 144   | 0     | 0.40   |
| Dell      | Express Flash C... | 960 GB | 97      | 142   | 0     | 0.39   |
| Samsung   | MZ4LB3T8HALS-00003 | 3.8 TB | 6       | 140   | 0     | 0.38   |
| Intel     | SSDPE2MD800G4      | 800 GB | 1       | 130   | 0     | 0.36   |
| WDC       | WDS100T3X0C-00SJG0 | 1 TB   | 17      | 128   | 0     | 0.35   |
| WDC       | PC SN520 SDAPNU... | 128 GB | 2       | 126   | 0     | 0.35   |
| Micron    | 2200_MTFDHBA512TCK | 512 GB | 58      | 125   | 0     | 0.34   |
| Kingston  | SKC2500M81000G     | 1 TB   | 8       | 122   | 0     | 0.34   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 193     | 117   | 0     | 0.32   |
| Samsung   | SSD 970 EVO        | 1 TB   | 7       | 339   | 2     | 0.32   |
| Micron    | 7300_MTFDHBE960TDF | 960 GB | 4       | 115   | 0     | 0.32   |
| Wester... | WUS4BB076D7P3E1    | 7.6 TB | 12      | 114   | 0     | 0.31   |
| Samsung   | SSD 970 EVO Plus   | 2 TB   | 8       | 113   | 0     | 0.31   |
| Kingston  | SA2000M8500G       | 500 GB | 4       | 111   | 0     | 0.31   |
| ADATA     | SX8200PNP          | 512 GB | 5       | 111   | 0     | 0.30   |
| KIOXIA    | KCD6XLUL1T92       | 1.9 TB | 10      | 109   | 0     | 0.30   |
| Samsung   | SSD 970 EVO Plus   | 250 GB | 22      | 108   | 0     | 0.30   |
| Huawei    | HWE52P431T6M002N   | 1.6 TB | 2       | 108   | 0     | 0.30   |
| Dell      | Ent NVMe P5600 ... | 1.6 TB | 8       | 108   | 0     | 0.30   |
| WDC       | WUS3BA119C7P3E3    | 1.9 TB | 7       | 107   | 0     | 0.29   |
| WDC       | PC SN530 NVMe      | 512 GB | 1       | 103   | 0     | 0.28   |
| Kingston  | SA2000M8250G       | 250 GB | 1       | 101   | 0     | 0.28   |
| Kingston  | SEDC1000M960G      | 960 GB | 2       | 100   | 0     | 0.27   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 200     | 97    | 0     | 0.27   |
| Samsung   | MZQL2960HCJR-00A07 | 960 GB | 2       | 96    | 0     | 0.26   |
| Samsung   | MZVLB1T0HBLR-00000 | 1 TB   | 44      | 95    | 0     | 0.26   |
| Dell      | Express Flash P... | 1.6 TB | 4       | 92    | 0     | 0.25   |
| Micron    | 2200_MTFDHBA256TCK | 256 GB | 18      | 89    | 0     | 0.25   |
| Samsung   | MZPLJ3T2HBJR-000H3 | 3.2 TB | 6       | 87    | 0     | 0.24   |
| Toshiba   | KXG60ZNV256G       | 256 GB | 55      | 86    | 0     | 0.24   |
| Samsung   | MZVLB256HAHQ-00000 | 256 GB | 14      | 84    | 0     | 0.23   |
| SK hynix  | VO000960KXAVL      | 960 GB | 2       | 80    | 0     | 0.22   |
| Samsung   | MZQLB1T9HAJR-000AZ | 1.9 TB | 4       | 80    | 0     | 0.22   |
| Samsung   | MT001600KWHAC 1... | 1.6 TB | 1       | 77    | 0     | 0.21   |
| Wester... | WUS4BB038D7P3E1    | 3.8 TB | 71      | 76    | 0     | 0.21   |
| SK hynix  | BC501 HFM512GDJ... | 512 GB | 1       | 73    | 0     | 0.20   |
| WDC       | WUS4BB019D7P3E3    | 1.9 TB | 54      | 69    | 0     | 0.19   |
| Intel     | SSDPEKNW512G8      | 512 GB | 4       | 69    | 0     | 0.19   |
| Samsung   | MZVLB512HBJQ-00000 | 512 GB | 15      | 69    | 0     | 0.19   |
| Micron    | 7300_MTFDHBE1T6TDG | 1.6 TB | 2       | 64    | 0     | 0.18   |
| Samsung   | MZVLB256HBHQ-00000 | 256 GB | 6       | 62    | 0     | 0.17   |
| Samsung   | MZPLJ1T6HBJR-000H3 | 1.6 TB | 8       | 56    | 0     | 0.15   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 180     | 54    | 0     | 0.15   |
| Corsair   | MP400              | 1 TB   | 20      | 53    | 0     | 0.15   |
| Samsung   | SSD 980            | 500 GB | 3       | 51    | 0     | 0.14   |
| Huawei    | HWE56P43800M005N   | 800 GB | 3       | 48    | 0     | 0.13   |
| WDC       | PC SN520 SDAPNU... | 256 GB | 1       | 47    | 0     | 0.13   |
| SK hynix  | PC601 HFS256GD9... | 256 GB | 1       | 46    | 0     | 0.13   |
| ADATA     | SX8200PNP          | 2 TB   | 2       | 45    | 0     | 0.13   |
| Team      | TM8FP6128G         | 128 GB | 1       | 45    | 0     | 0.13   |
| WDC       | WUS4BB096D7P3E4    | 960 GB | 4       | 43    | 0     | 0.12   |
| Samsung   | MZVLB256HAHQ-000L7 | 256 GB | 3       | 43    | 0     | 0.12   |
| Samsung   | MZQL23T8HCLS-00... | 3.8 TB | 1       | 43    | 0     | 0.12   |
| Intel     | MDTPED1K750GA      | 752 GB | 2       | 41    | 0     | 0.11   |
| Dell      | Express Flash N... | 6.4 TB | 4       | 39    | 0     | 0.11   |
| Samsung   | SSD 980 PRO        | 250 GB | 2       | 37    | 0     | 0.10   |
| Kingston  | SKC2500M8500G      | 500 GB | 3       | 35    | 0     | 0.10   |
| Micron    | 7300_MTFDHBE6T4TDG | 6.4 TB | 24      | 34    | 0     | 0.09   |
| Silico... | NV900-1T           | 1 TB   | 1       | 30    | 0     | 0.08   |
| ADATA     | SX6000LNP          | 512 GB | 1       | 30    | 0     | 0.08   |
| Samsung   | MZVL21T0HCLR-00B00 | 1 TB   | 6       | 28    | 0     | 0.08   |
| Toshiba   | KXG6AZNV512G       | 512 GB | 12      | 27    | 0     | 0.07   |
| WDC       | WDS500G2B0C-00PXH0 | 500 GB | 1       | 24    | 0     | 0.07   |
| WDC       | WUS4BB038D7P3E4    | 3.8 TB | 6       | 24    | 0     | 0.07   |
| ADATA     | SX6000PNP          | 256 GB | 2       | 22    | 0     | 0.06   |
| Micron    | MTFDHBA1T0TDV-1... | 1 TB   | 1       | 21    | 0     | 0.06   |
| Crucial   | CT1000P5SSD8       | 1 TB   | 4       | 17    | 0     | 0.05   |
| Intel     | SSDPF2KX076TZ      | 7.6 TB | 5       | 15    | 0     | 0.04   |
| Wester... | WUS4BB096D7P3E4    | 960 GB | 4       | 13    | 0     | 0.04   |
| Wester... | WUS4CB032D7P3E4    | 3.2 TB | 4       | 11    | 0     | 0.03   |
| WDC       | WUS4CB032D7P3E4    | 3.2 TB | 5       | 9     | 0     | 0.03   |
| Intel     | SSDPELKX010T8      | 1 TB   | 2       | 9     | 0     | 0.03   |
| SK hynix  | BC501A NVMe        | 128 GB | 3       | 7     | 0     | 0.02   |
| KIOXIA    | KCD61LUL3T84       | 3.8 TB | 20      | 5     | 0     | 0.01   |
| Crucial   | CT500P5SSD8        | 500 GB | 5       | 4     | 0     | 0.01   |
| ADATA     | SX6000NP           | 128 GB | 1       | 615   | 142   | 0.01   |
| WDC       | PC SN730 SDBQNT... | 256 GB | 1       | 4     | 0     | 0.01   |
| Micron    | 7300_MTFDHBG1T9TDF | 1.9 TB | 8       | 3     | 0     | 0.01   |
| Kingston  | SKC2500M82000G     | 2 TB   | 2       | 3     | 0     | 0.01   |
| Samsung   | MZVLB256HBHQ-000L7 | 256 GB | 3       | 2     | 0     | 0.01   |
| Samsung   | MZVLW128HEGR-00000 | 128 GB | 2       | 1     | 0     | 0.00   |
| Samsung   | MZVLW1T0HMLH-000L7 | 1 TB   | 1       | 0     | 0     | 0.00   |
| Samsung   | SSD 980            | 250 GB | 4       | 0     | 0     | 0.00   |
| Samsung   | SSD 980            | 1 TB   | 2       | 0     | 0     | 0.00   |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
|           |                    |        |         |       |       |        |
