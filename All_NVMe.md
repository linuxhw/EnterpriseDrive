Appendix 5: All NVMe Samples
============================

This is a list of all tested NVMe samples and their MTBFs. See more info on
reliability test in the README. See HDD samples MTBFs in the Appendix 1 (All_HDD.md)
and SSD samples MTBFs in the Appendix 2 (All_SSD.md).

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days — avg. days per sample,
Err  — avg. errors per sample,
MTBF — avg. MTBF in years per sample.

| MFG       | Model              | Size   | Drive ID     | Days  | Err   | MTBF   |
|-----------|--------------------|--------|--------------|-------|-------|--------|
| Intel     | SSDPEDMD400G4      | 400 GB | B83AFDA75128 | 2022  | 0     | 5.54   |
| Intel     | SSDPEDMD400G4      | 400 GB | BE70D19440D1 | 2022  | 0     | 5.54   |
| Intel     | SSDPEDMD400G4      | 400 GB | CA5EC06F1F73 | 2022  | 0     | 5.54   |
| Intel     | SSDPEDMD400G4      | 400 GB | 3F0065DDCB68 | 2020  | 0     | 5.53   |
| Intel     | SSDPEDMD400G4      | 400 GB | CDE819D7733B | 2020  | 0     | 5.53   |
| Intel     | SSDPEDMD400G4      | 400 GB | ED71DBA2EE9A | 2020  | 0     | 5.53   |
| Intel     | SSDPEDMD400G4      | 400 GB | 6D29744C9258 | 2019  | 0     | 5.53   |
| Intel     | SSDPEDMD400G4      | 400 GB | 3FA5CB5D15C4 | 2018  | 0     | 5.53   |
| Intel     | SSDPEDMD400G4      | 400 GB | 7EE5A493DB09 | 2018  | 0     | 5.53   |
| Intel     | SSDPEDMD400G4      | 400 GB | 2F5CA4312253 | 2017  | 0     | 5.53   |
| Intel     | SSDPEDMD400G4      | 400 GB | 61D3D3CC1600 | 2017  | 0     | 5.53   |
| Intel     | SSDPEDMD400G4      | 400 GB | 98A14B920346 | 2017  | 0     | 5.53   |
| Intel     | SSDPEDMD400G4      | 400 GB | FE4AFDC5CA09 | 2017  | 0     | 5.53   |
| Intel     | SSDPEDMD400G4      | 400 GB | 261572B4780D | 2014  | 0     | 5.52   |
| Intel     | SSDPEDMD400G4      | 400 GB | 9CFB7386E0E8 | 2014  | 0     | 5.52   |
| Intel     | SSDPEDMD400G4      | 400 GB | F83B6CE9FFF1 | 2014  | 0     | 5.52   |
| Intel     | SSDPEDMD400G4      | 400 GB | DD1B6FCD7A2E | 1977  | 0     | 5.42   |
| Intel     | SSDPEDMD400G4      | 400 GB | 7E012EE8022D | 1969  | 0     | 5.39   |
| Intel     | SSDPEDMD400G4      | 400 GB | 1E74418D5224 | 1968  | 0     | 5.39   |
| Intel     | SSDPEDMD016T4      | 1.6 TB | 77195838147E | 1937  | 0     | 5.31   |
| Intel     | SSDPEDMD016T4      | 1.6 TB | F44A32119A0F | 1937  | 0     | 5.31   |
| Intel     | SSDPEDMD400G4      | 400 GB | 00F4CC5452AF | 1936  | 0     | 5.31   |
| Intel     | SSDPEDMD400G4      | 400 GB | 46764674863F | 1934  | 0     | 5.30   |
| Intel     | SSDPEDMD400G4      | 400 GB | A112EB690CED | 1928  | 0     | 5.28   |
| Samsung   | MZVPV512HDGL-00000 | 512 GB | FAF89E03C1D6 | 1901  | 0     | 5.21   |
| Intel     | SSDPEDMD400G4      | 400 GB | 108924DD29AF | 1890  | 0     | 5.18   |
| Intel     | SSDPEDMD400G4      | 400 GB | 4764B53C1776 | 1878  | 0     | 5.15   |
| Intel     | SSDPEDMD400G4      | 400 GB | A395B452EFF6 | 1878  | 0     | 5.15   |
| Dell      | Express Flash N... | 800 GB | 51792EC7F87C | 1865  | 0     | 5.11   |
| Intel     | SSDPEDMD400G4      | 400 GB | 1BCCAEE8CC90 | 1831  | 0     | 5.02   |
| Intel     | SSDPEDMD400G4      | 400 GB | B69599BF6D97 | 1829  | 0     | 5.01   |
| Intel     | SSDPEDME400G4      | 400 GB | D88C7A0AB178 | 1822  | 0     | 4.99   |
| Toshiba   | THNSN5512GPU7      | 512 GB | EBD6BB98CF63 | 1810  | 0     | 4.96   |
| Intel     | SSDPEDME400G4      | 400 GB | 8C89882F3255 | 1806  | 0     | 4.95   |
| Intel     | SSDPEDME400G4      | 400 GB | 4174EFE2CBE4 | 1802  | 0     | 4.94   |
| Intel     | SSDPEDMD400G4      | 400 GB | C7DDFEDE924C | 1800  | 0     | 4.93   |
| Intel     | SSDPEDME400G4      | 400 GB | ED72279F6077 | 1793  | 0     | 4.91   |
| Intel     | SSDPEDMD400G4      | 400 GB | AE5C2415B4E0 | 1790  | 0     | 4.90   |
| Intel     | SSDPEDMD400G4      | 400 GB | 3C353CB30A59 | 1787  | 0     | 4.90   |
| Intel     | SSDPEDMD400G4      | 400 GB | FB2A985484C3 | 1777  | 0     | 4.87   |
| Samsung   | MZVPV512HDGL-00000 | 512 GB | 35C287C51AB0 | 1773  | 0     | 4.86   |
| Intel     | SSDPEDMD400G4      | 400 GB | 08DBB25A6166 | 1769  | 0     | 4.85   |
| Intel     | SSDPEDMW400G4      | 400 GB | 846278BDA652 | 1768  | 0     | 4.85   |
| Intel     | SSDPEDMD400G4      | 400 GB | D478C053CF68 | 1764  | 0     | 4.83   |
| Intel     | SSDPEDMD400G4      | 400 GB | A2F8CAB17529 | 1759  | 0     | 4.82   |
| Intel     | SSDPEDME016T4      | 1.6 TB | 21AC1C0D27CD | 1754  | 0     | 4.81   |
| Intel     | SSDPEDME020T4      | 2 TB   | 2A4F0C770AF6 | 1754  | 0     | 4.81   |
| Intel     | SSDPEDMD400G4      | 400 GB | B334318C207C | 1735  | 0     | 4.75   |
| Intel     | SSDPEDMD400G4      | 400 GB | 94BFEC51D81D | 1730  | 0     | 4.74   |
| Intel     | SSDPEDMD800G4      | 800 GB | C3F37E912926 | 1726  | 0     | 4.73   |
| Intel     | SSDPEDMD800G4      | 800 GB | F334B1C49AC8 | 1712  | 0     | 4.69   |
| Intel     | SSDPEDMD400G4      | 400 GB | C411762387C8 | 1712  | 0     | 4.69   |
| Intel     | SSDPEDMD400G4      | 400 GB | 67D2966DA3C2 | 1701  | 0     | 4.66   |
| Intel     | SSDPEDMD400G4      | 400 GB | EA2A31F8D6AB | 1701  | 0     | 4.66   |
| Intel     | SSDPEDMD400G4      | 400 GB | E06F564692E5 | 1697  | 0     | 4.65   |
| Intel     | SSDPEDMD400G4      | 400 GB | FD2F57CB760B | 1696  | 0     | 4.65   |
| Intel     | SSDPEDMD400G4      | 400 GB | 0B96D6703529 | 1683  | 0     | 4.61   |
| Intel     | SSDPEDMD800G4      | 800 GB | 1FC954E76E2D | 1671  | 0     | 4.58   |
| Intel     | SSDPEDMW400G4      | 400 GB | B18E616856A8 | 1670  | 0     | 4.58   |
| Intel     | SSDPEDMD400G4      | 400 GB | 129F9B95D565 | 1667  | 0     | 4.57   |
| Intel     | SSDPEDMD800G4      | 800 GB | 03C994BCBB5F | 1659  | 0     | 4.55   |
| Intel     | SSDPEDMD800G4      | 800 GB | 67069A716DBD | 1657  | 0     | 4.54   |
| Intel     | SSDPEDMD400G4      | 400 GB | CCC29E11CCEF | 1655  | 0     | 4.54   |
| Intel     | SSDPEDMD400G4      | 400 GB | 17C5EE1A1E92 | 1651  | 0     | 4.53   |
| Intel     | SSDPEDMD400G4      | 400 GB | 526D06A3FB78 | 1647  | 0     | 4.51   |
| Samsung   | SSD 950 PRO        | 512 GB | DC7A9EC22FDA | 1647  | 0     | 4.51   |
| Intel     | SSDPEDMD800G4      | 800 GB | 9A5DAAC59320 | 1646  | 0     | 4.51   |
| Intel     | SSDPEDMD800G4      | 800 GB | 1BDC080DA416 | 1645  | 0     | 4.51   |
| Intel     | SSDPEDME400G4      | 400 GB | 8A16414D071F | 1643  | 0     | 4.50   |
| Intel     | SSDPEDMD400G4      | 400 GB | 7783749EF57F | 1642  | 0     | 4.50   |
| Intel     | SSDPEDME020T4      | 2 TB   | ADE49E8D8170 | 1638  | 0     | 4.49   |
| Intel     | SSDPEDMX400G4      | 400 GB | DBD2252CDB2B | 1635  | 0     | 4.48   |
| Intel     | SSDPEDMD800G4      | 800 GB | 2E509718A77D | 1633  | 0     | 4.47   |
| Samsung   | SSD 950 PRO        | 512 GB | 58980E251488 | 1627  | 0     | 4.46   |
| Intel     | SSDPEDMD400G4      | 400 GB | F40269E3C652 | 1627  | 0     | 4.46   |
| Intel     | SSDPEDMD800G4      | 800 GB | 58C65FE743A4 | 1625  | 0     | 4.45   |
| Intel     | SSDPEDMD800G4      | 800 GB | 69A856B7FAB6 | 1625  | 0     | 4.45   |
| Intel     | SSDPEDME020T4D ... | 2 TB   | 3DFD003D98DF | 1625  | 0     | 4.45   |
| Intel     | SSDPEDMD400G4      | 400 GB | 733EC993F44E | 1624  | 0     | 4.45   |
| Intel     | SSDPEDMD800G4      | 800 GB | C56519D3E0F2 | 1617  | 0     | 4.43   |
| Intel     | SSDPEDMW400G4      | 400 GB | 75A1AA7709F1 | 1614  | 0     | 4.42   |
| Intel     | SSDPEDMD400G4      | 400 GB | FE5440F9BDF2 | 1613  | 0     | 4.42   |
| Intel     | SSDPEDMD400G4      | 400 GB | 00AE0A7D7793 | 1611  | 0     | 4.42   |
| Intel     | SSDPEDME016T4S     | 1.6 TB | ECE77999956B | 1611  | 0     | 4.42   |
| Intel     | SSDPEDMX020T7      | 2 TB   | 2587249B0C3D | 1610  | 0     | 4.41   |
| Intel     | SSDPEDME400G4      | 400 GB | F03878A4DE48 | 1599  | 0     | 4.38   |
| Intel     | SSDPEDME400G4      | 400 GB | 84B13E2D3B44 | 1599  | 0     | 4.38   |
| Intel     | SSDPEDME400G4      | 400 GB | 9099DA14C0B8 | 1599  | 0     | 4.38   |
| Intel     | SSDPEDME400G4      | 400 GB | 35D14AA4B880 | 1593  | 0     | 4.37   |
| Intel     | SSDPEDMX020T7      | 2 TB   | 5B18EC08CFD8 | 1591  | 0     | 4.36   |
| Intel     | SSDPEDMX020T7      | 2 TB   | 837FAC86A8ED | 1591  | 0     | 4.36   |
| Intel     | SSDPEDMD400G4      | 400 GB | D2F980462F2B | 1591  | 0     | 4.36   |
| Intel     | SSDPEDME020T4D ... | 2 TB   | 3DC27115D79F | 1591  | 0     | 4.36   |
| Intel     | SSDPEDMD400G4      | 400 GB | 2F83B0132BB7 | 1591  | 0     | 4.36   |
| Intel     | SSDPEDMD400G4      | 400 GB | 43576D2DF765 | 1590  | 0     | 4.36   |
| Intel     | SSDPEDMX020T7      | 2 TB   | FDBBB0BDF91C | 1590  | 0     | 4.36   |
| Intel     | SSDPEDMD400G4      | 400 GB | 87916B02E99B | 1589  | 0     | 4.35   |
| Intel     | SSDPEDMD800G4      | 800 GB | 3F5ECE4428F5 | 1582  | 0     | 4.34   |
| Intel     | SSDPEDME400G4      | 400 GB | 48A2470007AA | 1581  | 0     | 4.33   |
| Intel     | SSDPEDME400G4      | 400 GB | 3D05667FD127 | 1579  | 0     | 4.33   |
| Intel     | SSDPEDME400G4      | 400 GB | DFCDA0741129 | 1577  | 0     | 4.32   |
| Intel     | SSDPEDME400G4      | 400 GB | C829EAC70F34 | 1577  | 0     | 4.32   |
| Intel     | SSDPEDME400G4      | 400 GB | AE51F903C70D | 1576  | 0     | 4.32   |
| Intel     | SSDPEDME400G4      | 400 GB | 88413009A3CE | 1576  | 0     | 4.32   |
| Intel     | SSDPEDMD400G4      | 400 GB | 519E913CFFD6 | 1576  | 0     | 4.32   |
| Intel     | SSDPEDMW012T4      | 1.2 TB | 0008AB2EC577 | 1576  | 0     | 4.32   |
| Intel     | SSDPEDMD800G4      | 800 GB | 390EE13F7B75 | 1573  | 0     | 4.31   |
| Intel     | SSDPEDMD400G4      | 400 GB | 3D76EE789680 | 1568  | 0     | 4.30   |
| Intel     | SSDPEDMD400G4      | 400 GB | B33CC666CEA7 | 1567  | 0     | 4.29   |
| Intel     | SSDPEDMD800G4      | 800 GB | A95C200977BF | 1566  | 0     | 4.29   |
| Intel     | SSDPEDMD800G4      | 800 GB | AC2E71D37387 | 1565  | 0     | 4.29   |
| Intel     | SSDPEDMD400G4      | 400 GB | C960C6FBC709 | 1552  | 0     | 4.25   |
| Intel     | SSDPEDMD400G4      | 400 GB | E409E48298FC | 1551  | 0     | 4.25   |
| Intel     | SSDPEDMD800G4      | 800 GB | 231374B5B2B6 | 1551  | 0     | 4.25   |
| Intel     | SSDPEDMD800G4      | 800 GB | 8985C2E1E6E4 | 1551  | 0     | 4.25   |
| Intel     | SSDPEDMD020T4D ... | 2 TB   | 10EAC04FB0D2 | 1547  | 0     | 4.24   |
| Intel     | SSDPEDMD400G4      | 400 GB | 96F418B859EB | 1545  | 0     | 4.24   |
| Intel     | SSDPEDMD400G4      | 400 GB | 5847378A4461 | 1541  | 0     | 4.22   |
| Intel     | SSDPEDME400G4      | 400 GB | B294D3A62067 | 1540  | 0     | 4.22   |
| Intel     | SSDPEDMD800G4      | 800 GB | 1568DCDCC888 | 1536  | 0     | 4.21   |
| Intel     | SSDPEDMD800G4      | 800 GB | 88FD92B25152 | 1533  | 0     | 4.20   |
| Intel     | SSDPEDMD800G4      | 800 GB | CAE2F2DFFDBC | 1532  | 0     | 4.20   |
| Intel     | SSDPEDMD800G4      | 800 GB | 303E8BEB193D | 1531  | 0     | 4.20   |
| Intel     | SSDPEDMD800G4      | 800 GB | 35E23F917B88 | 1531  | 0     | 4.20   |
| Intel     | SSDPEDMD800G4      | 800 GB | BEC9D2B17A8A | 1531  | 0     | 4.20   |
| Samsung   | MZQLW960HMJP-00003 | 960 GB | 05C08994F8CA | 1530  | 0     | 4.19   |
| Intel     | SSDPEDMD800G4      | 800 GB | 4317AB43E24A | 1528  | 0     | 4.19   |
| Intel     | SSDPEDMD800G4      | 800 GB | 490839E91082 | 1525  | 0     | 4.18   |
| Intel     | SSDPEDMD800G4      | 800 GB | 53855229C0CC | 1525  | 0     | 4.18   |
| Intel     | SSDPEDMD800G4      | 800 GB | 944D1368025C | 1525  | 0     | 4.18   |
| Intel     | SSDPEDMD800G4      | 800 GB | C111D75BE6D1 | 1525  | 0     | 4.18   |
| Intel     | SSDPEDMD800G4      | 800 GB | 990E80299D96 | 1525  | 0     | 4.18   |
| Intel     | SSDPEDMD400G4      | 400 GB | 6C9AF2D1120F | 1525  | 0     | 4.18   |
| Intel     | SSDPEDMX020T7      | 2 TB   | C091574E9271 | 1519  | 0     | 4.16   |
| Intel     | SSDPEDMD400G4      | 400 GB | A4A488FE8462 | 1518  | 0     | 4.16   |
| Intel     | SSDPEDMD400G4      | 400 GB | E0816AD2CC68 | 1518  | 0     | 4.16   |
| Intel     | SSDPEDMD400G4      | 400 GB | 575B88529F2D | 1517  | 0     | 4.16   |
| Intel     | SSDPEDMD400G4      | 400 GB | 762B8302935D | 1517  | 0     | 4.16   |
| Intel     | SSDPEDMD400G4      | 400 GB | 8742DBC696D4 | 1517  | 0     | 4.16   |
| Intel     | SSDPEDMD800G4      | 800 GB | 14CA3620B252 | 1512  | 0     | 4.14   |
| Intel     | SSDPEDMD800G4      | 800 GB | 559BCA7EA778 | 1512  | 0     | 4.14   |
| Intel     | SSDPEDMD800G4      | 800 GB | DCF37E5F2867 | 1512  | 0     | 4.14   |
| Intel     | SSDPEDMD400G4      | 400 GB | 7EEE48D40650 | 1509  | 0     | 4.14   |
| Intel     | SSDPEDMD400G4      | 400 GB | 1494C8837445 | 1509  | 0     | 4.13   |
| Intel     | SSDPEDMD400G4      | 400 GB | 9CBDE0EF1780 | 1503  | 0     | 4.12   |
| Intel     | SSDPEDMD400G4      | 400 GB | BEBB615A0E9C | 1500  | 0     | 4.11   |
| Intel     | SSDPEDMD400G4      | 400 GB | 6AA1DDB03C1C | 1500  | 0     | 4.11   |
| Intel     | SSDPEDMD800G4      | 800 GB | 3FCE284AEEC7 | 1496  | 0     | 4.10   |
| Intel     | SSDPEDMX020T7      | 2 TB   | 2ADEC4EA72A0 | 1494  | 0     | 4.10   |
| Intel     | SSDPEDMD400G4      | 400 GB | 446694356EF3 | 1494  | 0     | 4.09   |
| Intel     | SSDPEDMD400G4      | 400 GB | 47C2C63A6C5D | 1494  | 0     | 4.09   |
| Intel     | SSDPEDMD400G4      | 400 GB | 90D75672909E | 1494  | 0     | 4.09   |
| Intel     | SSDPEDMD800G4      | 800 GB | 966504752B6A | 1492  | 0     | 4.09   |
| Toshiba   | THNSN5512GPU7      | 512 GB | C2BF973C446C | 1492  | 0     | 4.09   |
| Intel     | SSDPEDMX020T7      | 2 TB   | 28AD658F2642 | 1491  | 0     | 4.09   |
| Intel     | SSDPEDMD800G4      | 800 GB | 7B58EA396F0D | 1489  | 0     | 4.08   |
| Intel     | SSDPEDMD800G4      | 800 GB | FAA8A0E5151A | 1489  | 0     | 4.08   |
| Intel     | SSDPEDMD800G4      | 800 GB | FF1F68042315 | 1489  | 0     | 4.08   |
| Intel     | SSDPEDMD400G4      | 400 GB | C8B06F5D23A9 | 1486  | 0     | 4.07   |
| Intel     | SSDPEDMD800G4      | 800 GB | 6B52E1F5F5F8 | 1486  | 0     | 4.07   |
| Intel     | SSDPEDMD800G4      | 800 GB | B15361781264 | 1486  | 0     | 4.07   |
| Intel     | SSDPEDMD800G4      | 800 GB | B75EA0AFB1FD | 1485  | 0     | 4.07   |
| Intel     | SSDPEDMD400G4      | 400 GB | CD5117E8190E | 1485  | 0     | 4.07   |
| Intel     | SSDPEDMD800G4      | 800 GB | D9D4B26CB2B3 | 1485  | 0     | 4.07   |
| Intel     | SSDPEDMD800G4      | 800 GB | 04E2556B9F4C | 1485  | 0     | 4.07   |
| Intel     | SSDPEDMD800G4      | 800 GB | C7987F5E3508 | 1484  | 0     | 4.07   |
| Intel     | SSDPEDMD800G4      | 800 GB | 5891E7B99D59 | 1484  | 0     | 4.07   |
| Intel     | SSDPEDMD400G4      | 400 GB | 88FA2028AF38 | 1482  | 0     | 4.06   |
| Intel     | SSDPEDMD400G4      | 400 GB | DB5C0247B0D9 | 1479  | 0     | 4.05   |
| Samsung   | SSD 950 PRO        | 512 GB | AC638F701D70 | 1475  | 0     | 4.04   |
| Intel     | SSDPEDMD800G4      | 800 GB | C7579C542259 | 1474  | 0     | 4.04   |
| Intel     | SSDPEDMD400G4      | 400 GB | 3CFB38973953 | 1474  | 0     | 4.04   |
| Intel     | SSDPEDMD400G4      | 400 GB | 7D7D31754CAB | 1473  | 0     | 4.04   |
| Intel     | SSDPEDMD016T4      | 1.6 TB | F101EC265662 | 1473  | 0     | 4.04   |
| Intel     | SSDPEDMD400G4      | 400 GB | EB7118D96403 | 1471  | 0     | 4.03   |
| Intel     | SSDPEDMD400G4      | 400 GB | CD86D04D4FA9 | 1471  | 0     | 4.03   |
| Intel     | SSDPEDMD400G4      | 400 GB | 33E67700E517 | 1470  | 0     | 4.03   |
| Intel     | SSDPEDMD400G4      | 400 GB | 79921C31E432 | 1470  | 0     | 4.03   |
| Intel     | SSDPEDMD400G4      | 400 GB | B9C7A3069F9C | 1470  | 0     | 4.03   |
| Intel     | SSDPEDMD400G4      | 400 GB | C794AB7FAE45 | 1470  | 0     | 4.03   |
| Intel     | SSDPEDMD800G4      | 800 GB | 810D00639476 | 1460  | 0     | 4.00   |
| Intel     | SSDPEDME400G4      | 400 GB | 11B5C9D5583F | 1459  | 0     | 4.00   |
| Intel     | SSDPEDMX020T7      | 2 TB   | 04558A92AA8E | 1455  | 0     | 3.99   |
| Intel     | SSDPEDMX020T7      | 2 TB   | 46D0500AE243 | 1454  | 0     | 3.99   |
| Intel     | SSDPEDMD400G4      | 400 GB | 8164888EE8FC | 1454  | 0     | 3.98   |
| Intel     | SSDPE2MX012T4      | 1.2 TB | 544C677098FD | 1453  | 0     | 3.98   |
| Intel     | SSDPE2MX012T4      | 1.2 TB | DC313BECD994 | 1453  | 0     | 3.98   |
| Intel     | SSDPEDMX020T7      | 2 TB   | 22DFDD16AB36 | 1452  | 0     | 3.98   |
| Intel     | SSDPEDMX020T7      | 2 TB   | C10FAC44D723 | 1452  | 0     | 3.98   |
| Intel     | SSDPEDMD800G4      | 800 GB | 3AB70D221FF8 | 1448  | 0     | 3.97   |
| Samsung   | MZQLW960HMJP-00003 | 960 GB | 321BC1330BD2 | 1443  | 0     | 3.95   |
| Toshiba   | THNSN5512GPU7      | 512 GB | 7AB37DC384B4 | 1439  | 0     | 3.94   |
| Intel     | SSDPEDMD016T4      | 1.6 TB | CA195B3AE38C | 1439  | 0     | 3.94   |
| Intel     | SSDPEDMD800G4      | 800 GB | 9CB62F1A47C6 | 1435  | 0     | 3.93   |
| Intel     | SSDPEDMX020T7      | 2 TB   | 792A6FFEDC74 | 1434  | 0     | 3.93   |
| Intel     | SSDPEDMX020T7      | 2 TB   | 4979A5256EBA | 1434  | 0     | 3.93   |
| Intel     | SSDPE2ME012T4      | 1.2 TB | 64F2941E458A | 1433  | 0     | 3.93   |
| Intel     | SSDPE2ME012T4      | 1.2 TB | 9E9DEB0FEF9C | 1433  | 0     | 3.93   |
| Intel     | SSDPEDMD400G4      | 400 GB | 969CD5BD5165 | 1433  | 0     | 3.93   |
| Intel     | SSDPEDMD400G4      | 400 GB | 68238BD5EEE9 | 1433  | 0     | 3.93   |
| Intel     | SSDPEDMD800G4      | 800 GB | 083848E60F9D | 1431  | 0     | 3.92   |
| Intel     | SSDPEDMD400G4      | 400 GB | EA32841A189A | 1430  | 0     | 3.92   |
| Intel     | SSDPEDMD400G4      | 400 GB | AB965DBE62D4 | 1430  | 0     | 3.92   |
| Samsung   | MZQLW960HMJP-00003 | 960 GB | 349B63A055D1 | 1428  | 0     | 3.91   |
| Intel     | SSDPEDMD400G4      | 400 GB | CF280DB36DE1 | 1428  | 0     | 3.91   |
| Dell      | Express Flash N... | 3.2 TB | F3E8993E2F74 | 1428  | 0     | 3.91   |
| Intel     | SSDPEDMX400G4      | 400 GB | 1AF6E1FF855E | 1427  | 0     | 3.91   |
| Intel     | SSDPEDMX400G4      | 400 GB | 7F6B1DD82560 | 1427  | 0     | 3.91   |
| Intel     | SSDPEDMD400G4      | 400 GB | 13243FD132A9 | 1426  | 0     | 3.91   |
| Intel     | SSDPEDMD400G4      | 400 GB | A03D7B8EF79D | 1426  | 0     | 3.91   |
| Intel     | SSDPEDMD400G4      | 400 GB | BBB1450170C1 | 1426  | 0     | 3.91   |
| Intel     | SSDPEDMD400G4      | 400 GB | F56F3F807B19 | 1426  | 0     | 3.91   |
| Intel     | SSDPEDMD800G4      | 800 GB | 6EA8804E8594 | 1425  | 0     | 3.91   |
| Intel     | SSDPEDMX400G4      | 400 GB | 609CA23AF258 | 1423  | 0     | 3.90   |
| Intel     | SSDPEDMD800G4      | 800 GB | 9E1EE234E82B | 1422  | 0     | 3.90   |
| Intel     | SSDPEDMX020T7      | 2 TB   | 6BE7D53CC529 | 1420  | 0     | 3.89   |
| Intel     | SSDPEDME400G4      | 400 GB | 2CE6E2519DD8 | 1418  | 0     | 3.89   |
| Intel     | SSDPEDMX020T7      | 2 TB   | 3572DDEF3C57 | 1418  | 0     | 3.89   |
| Intel     | SSDPEDME800G4      | 800 GB | 0BE1AED053E7 | 1416  | 0     | 3.88   |
| Intel     | SSDPEDMX400G4      | 400 GB | 85A01A4074F5 | 1416  | 0     | 3.88   |
| Intel     | SSDPEDMX400G4      | 400 GB | 85E19C705EDB | 1416  | 0     | 3.88   |
| Intel     | SSDPEDMX020T7      | 2 TB   | 55375FE232B2 | 1415  | 0     | 3.88   |
| Intel     | SSDPEDMX400G4      | 400 GB | 611E38F2A639 | 1414  | 0     | 3.88   |
| Intel     | SSDPEDMX400G4      | 400 GB | 9E4D5ADA16E2 | 1414  | 0     | 3.88   |
| Intel     | SSDPE2MX012T4      | 1.2 TB | 283B7B306501 | 1412  | 0     | 3.87   |
| Intel     | SSDPE2MX012T4      | 1.2 TB | 6068E80DE69C | 1412  | 0     | 3.87   |
| Intel     | SSDPEDMD020T4D ... | 2 TB   | F23E4D0F62A6 | 1412  | 0     | 3.87   |
| Intel     | SSDPEDMD800G4      | 800 GB | 5A669ABFF31C | 1411  | 0     | 3.87   |
| Intel     | SSDPEDMD800G4      | 800 GB | 6A7F6F51849C | 1411  | 0     | 3.87   |
| Intel     | SSDPEDMD800G4      | 800 GB | 7A2C7B86DDEE | 1410  | 0     | 3.87   |
| Intel     | SSDPEDMD800G4      | 800 GB | C6086C68D863 | 1410  | 0     | 3.87   |
| Intel     | SSDPEDMD800G4      | 800 GB | 854E9D777731 | 1410  | 0     | 3.86   |
| Intel     | SSDPE2MX450G7      | 450 GB | 04234EF3C5E1 | 1408  | 0     | 3.86   |
| Intel     | SSDPEDMX020T7      | 2 TB   | B1955BD28F9D | 1405  | 0     | 3.85   |
| Intel     | SSDPEDMX020T7      | 2 TB   | EC26DC71230A | 1401  | 0     | 3.84   |
| Intel     | SSDPEDMD800G4      | 800 GB | DF9082ED2C00 | 1399  | 0     | 3.84   |
| Intel     | SSDPEDMD400G4      | 400 GB | EFF721FBCBC1 | 1398  | 0     | 3.83   |
| Intel     | SSDPE2MX450G7      | 450 GB | 2CA988FD1696 | 1394  | 0     | 3.82   |
| Intel     | SSDPEDMD800G4      | 800 GB | 4455E2AA5BD6 | 1389  | 0     | 3.81   |
| Intel     | SSDPEDMD800G4      | 800 GB | 8C0D3D7A7E44 | 1388  | 0     | 3.80   |
| Intel     | SSDPEDMD800G4      | 800 GB | 7990ACCE7621 | 1386  | 0     | 3.80   |
| Intel     | SSDPEDMD400G4      | 400 GB | B4098210986D | 1386  | 0     | 3.80   |
| Intel     | SSDPE2MD400G4      | 400 GB | 47D84A9C781E | 1386  | 0     | 3.80   |
| Intel     | SSDPEDMD016T4      | 1.6 TB | 577284EB4B10 | 1385  | 0     | 3.80   |
| Intel     | SSDPE2MD400G4      | 400 GB | 955752458D2C | 1385  | 0     | 3.80   |
| Intel     | SSDPEDMX020T7      | 2 TB   | 61FA35FCCD95 | 1385  | 0     | 3.80   |
| Intel     | SSDPEDMX020T7      | 2 TB   | E35980A7FDBF | 1382  | 0     | 3.79   |
| Intel     | SSDPEDMD800G4      | 800 GB | D35B46C353DF | 1382  | 0     | 3.79   |
| Intel     | SSDPEDME016T4S     | 1.6 TB | BD61499935D5 | 1381  | 0     | 3.78   |
| Intel     | SSDPEDMD800G4      | 800 GB | 00C26F66EA4B | 1378  | 0     | 3.78   |
| Intel     | SSDPE2MD400G4      | 400 GB | 07FA7378CE66 | 1377  | 0     | 3.77   |
| Intel     | SSDPEDMD400G4      | 400 GB | 5A7467ECA790 | 1377  | 0     | 3.77   |
| Intel     | SSDPEDMD400G4      | 400 GB | 822122FD34F0 | 1377  | 0     | 3.77   |
| Intel     | SSDPEDMX400G4      | 400 GB | 6B1E3D5E2DA7 | 1376  | 0     | 3.77   |
| Intel     | SSDPEDMX400G4      | 400 GB | 91F339D2A5ED | 1376  | 0     | 3.77   |
| Intel     | SSDPEDMD400G4      | 400 GB | 858BC2604739 | 1375  | 0     | 3.77   |
| Intel     | SSDPEDMD800G4      | 800 GB | 8020051E2331 | 1375  | 0     | 3.77   |
| Intel     | SSDPEDMD400G4      | 400 GB | 98261D04D965 | 1374  | 0     | 3.76   |
| Intel     | SSDPEDMX020T7      | 2 TB   | 606EB4B6B10A | 1373  | 0     | 3.76   |
| Intel     | SSDPEDMD400G4      | 400 GB | 76BB04AD5ACF | 1373  | 0     | 3.76   |
| Intel     | SSDPE2MD400G4      | 400 GB | 0F9D7E614286 | 1369  | 0     | 3.75   |
| Intel     | SSDPEDMD400G4      | 400 GB | B6BA56D4C230 | 1367  | 0     | 3.75   |
| Intel     | SSDPEDMD400G4      | 400 GB | BCD2C2F615F3 | 1367  | 0     | 3.75   |
| Intel     | SSDPEDMD400G4      | 400 GB | 29ADB0AC8FA2 | 1367  | 0     | 3.75   |
| Intel     | SSDPEDMD400G4      | 400 GB | 8A2A2C4BEEEC | 1367  | 0     | 3.75   |
| Intel     | SSDPEDMX020T7      | 2 TB   | C655B1D6D359 | 1364  | 0     | 3.74   |
| Intel     | SSDPEDMD400G4      | 400 GB | 155326E76895 | 1363  | 0     | 3.74   |
| Intel     | SSDPEDMD400G4      | 400 GB | 91B457A30A73 | 1363  | 0     | 3.74   |
| Intel     | SSDPEDMD016T4      | 1.6 TB | DCDBC9FA1F6E | 1360  | 0     | 3.73   |
| Intel     | SSDPEDMX012T7      | 1.2 TB | A0605E74AD77 | 1359  | 0     | 3.72   |
| Intel     | SSDPEDMD400G4      | 400 GB | A33F03166BCF | 1359  | 0     | 3.72   |
| Intel     | SSDPEDMD400G4      | 400 GB | AA0D81B073C5 | 1359  | 0     | 3.72   |
| Intel     | SSDPEDMD800G4      | 800 GB | 43003232FF3C | 1357  | 0     | 3.72   |
| Intel     | SSDPEDMD800G4      | 800 GB | 8E46C17CCE61 | 1357  | 0     | 3.72   |
| Intel     | SSDPEDMD800G4      | 800 GB | E407E3363396 | 1357  | 0     | 3.72   |
| Intel     | SSDPEDMD800G4      | 800 GB | 7B203FAFDF3F | 1356  | 0     | 3.72   |
| Intel     | SSDPEDMD800G4      | 800 GB | 4DF906430EC5 | 1356  | 0     | 3.72   |
| Intel     | SSDPEDMD800G4      | 800 GB | AC7A9E730C40 | 1355  | 0     | 3.71   |
| Intel     | SSDPEDMD800G4      | 800 GB | 204536BB532A | 1355  | 0     | 3.71   |
| Intel     | SSDPEDMD800G4      | 800 GB | 67D5F21969A2 | 1355  | 0     | 3.71   |
| Intel     | SSDPEDMD400G4      | 400 GB | 04E75413F1A7 | 1355  | 0     | 3.71   |
| Intel     | SSDPEDMD800G4      | 800 GB | 9B6BAB4D117E | 1354  | 0     | 3.71   |
| Intel     | SSDPE2MX012T7      | 1.2 TB | 3868698C4A5C | 1353  | 0     | 3.71   |
| Intel     | SSDPE2MX012T7      | 1.2 TB | D82A13BC6B3D | 1353  | 0     | 3.71   |
| Intel     | SSDPEDMD016T4      | 1.6 TB | 63A2A6B1BF8D | 1347  | 0     | 3.69   |
| Samsung   | SSD 950 PRO        | 512 GB | E4E5A40DCB28 | 1344  | 0     | 3.68   |
| Samsung   | SSD 950 PRO        | 512 GB | 0CCF7569407F | 1344  | 0     | 3.68   |
| Intel     | SSDPEDMD400G4      | 400 GB | 15CE13CE15B4 | 1343  | 0     | 3.68   |
| Intel     | SSDPEDMD400G4      | 400 GB | 6A4DCAA36E09 | 1341  | 0     | 3.68   |
| Intel     | SSDPEDMD016T4      | 1.6 TB | EFB7D11BAB2A | 1337  | 0     | 3.66   |
| Intel     | SSDPEDMD800G4      | 800 GB | DA461399520E | 1336  | 0     | 3.66   |
| Intel     | SSDPEDMD400G4      | 400 GB | 70EA4257F8BA | 1335  | 0     | 3.66   |
| Intel     | SSDPEDMD400G4      | 400 GB | 316B72ED4BCF | 1333  | 0     | 3.65   |
| Intel     | SSDPE2MX450G7      | 450 GB | 0ECB24FBFB6F | 1329  | 0     | 3.64   |
| Intel     | SSDPEDMX020T7      | 2 TB   | EF0E40D79F2C | 1328  | 0     | 3.64   |
| Intel     | SSDPEDMD400G4      | 400 GB | 1028AFDA7C4A | 1326  | 0     | 3.63   |
| Intel     | SSDPEDMD800G4      | 800 GB | 73AEBF1F3F08 | 1325  | 0     | 3.63   |
| Intel     | SSDPEDMD800G4      | 800 GB | A9F1BB6D41A4 | 1325  | 0     | 3.63   |
| Intel     | SSDPEDMD800G4      | 800 GB | BC72F20C9B9E | 1325  | 0     | 3.63   |
| Intel     | SSDPEDMD800G4      | 800 GB | B82913EDD9A2 | 1322  | 0     | 3.62   |
| Intel     | SSDPEDMD800G4      | 800 GB | 2281B4A6E066 | 1315  | 0     | 3.60   |
| Intel     | SSDPEDMD800G4      | 800 GB | D7D56E41A012 | 1315  | 0     | 3.60   |
| Intel     | SSDPE2KE016T7      | 1.6 TB | 78AAA9759B63 | 1313  | 0     | 3.60   |
| Intel     | SSDPE2MX012T7      | 1.2 TB | 7B23597FD8AA | 1312  | 0     | 3.60   |
| Intel     | SSDPE2MX012T7      | 1.2 TB | BF7BBA8F3BD6 | 1312  | 0     | 3.60   |
| Intel     | SSDPEDMD400G4      | 400 GB | 00FD6B44F57A | 1301  | 0     | 3.57   |
| Intel     | SSDPEDMD400G4      | 400 GB | 2CB9A6763D9C | 1301  | 0     | 3.57   |
| Intel     | SSDPEDMD400G4      | 400 GB | 5F76964C85DE | 1300  | 0     | 3.56   |
| Intel     | SSDPEDMD400G4      | 400 GB | B0E658976583 | 1300  | 0     | 3.56   |
| Intel     | SSDPEDMD400G4      | 400 GB | 9D71DD146FDF | 1299  | 0     | 3.56   |
| Intel     | SSDPEDMD800G4      | 800 GB | 4E694C4A170A | 1297  | 0     | 3.55   |
| Intel     | SSDPEDMD800G4      | 800 GB | 7BB553439513 | 1297  | 0     | 3.55   |
| Intel     | SSDPEDMD800G4      | 800 GB | C03397C01D5A | 1297  | 0     | 3.55   |
| Intel     | SSDPEDMD400G4      | 400 GB | 51EF3E8F0C87 | 1296  | 0     | 3.55   |
| Intel     | SSDPEDMD800G4      | 800 GB | 443E078C3396 | 1296  | 0     | 3.55   |
| Intel     | SSDPEDMD800G4      | 800 GB | 4C7AF02EA9D8 | 1296  | 0     | 3.55   |
| Intel     | SSDPEDMD800G4      | 800 GB | AD88343C3357 | 1296  | 0     | 3.55   |
| Intel     | SSDPEDMD800G4      | 800 GB | FFA240DE0131 | 1296  | 0     | 3.55   |
| Intel     | SSDPEDMD800G4      | 800 GB | 2B36F8045704 | 1294  | 0     | 3.55   |
| Intel     | SSDPEDMW800G4      | 800 GB | B4D63DAD1AFA | 1294  | 0     | 3.55   |
| Intel     | SSDPEDMD400G4      | 400 GB | BA974FB7706C | 1292  | 0     | 3.54   |
| Intel     | SSDPEDMD400G4      | 400 GB | FFCD2626392C | 1292  | 0     | 3.54   |
| Intel     | SSDPEDMD800G4      | 800 GB | B733EB0A1357 | 1289  | 0     | 3.53   |
| Intel     | SSDPEDMD800G4      | 800 GB | D795F393A994 | 1289  | 0     | 3.53   |
| Intel     | SSDPEDMD400G4      | 400 GB | F24BB32D0340 | 1289  | 0     | 3.53   |
| Intel     | SSDPEDMX020T7      | 2 TB   | B7B99FB7A6E8 | 1287  | 0     | 3.53   |
| Intel     | SSDPEDMD800G4      | 800 GB | CC57DD282315 | 1287  | 0     | 3.53   |
| Intel     | SSDPEDMD400G4      | 400 GB | F78CAEC40C40 | 1283  | 0     | 3.52   |
| Intel     | SSDPEDMD400G4      | 400 GB | DC6E912BB4DD | 1282  | 0     | 3.51   |
| Intel     | SSDPEDMD400G4      | 400 GB | 97D5EDA068E2 | 1282  | 0     | 3.51   |
| Intel     | SSDPEDMD400G4      | 400 GB | 927B670A0BBB | 1282  | 0     | 3.51   |
| Intel     | SSDPEDKE020T7      | 2 TB   | 9B356B57952D | 1281  | 0     | 3.51   |
| Intel     | SSDPE2MX450G7      | 450 GB | 418B57B633C8 | 1280  | 0     | 3.51   |
| Intel     | SSDPEKKW256G7      | 256 GB | AC43F99CD86F | 1280  | 0     | 3.51   |
| Intel     | SSDPEDMD800G4      | 800 GB | 904D19E82AC7 | 1278  | 0     | 3.50   |
| Intel     | SSDPEDMW800G4      | 800 GB | 171F9BC0CA50 | 1277  | 0     | 3.50   |
| Intel     | SSDPEDMD020T4D ... | 2 TB   | D389D93F980C | 1277  | 0     | 3.50   |
| Intel     | SSDPE2MX450G7      | 450 GB | 35827F8197CB | 1277  | 0     | 3.50   |
| Intel     | SSDPEDMD800G4      | 800 GB | E9FBFD6A8254 | 1275  | 0     | 3.49   |
| Intel     | SSDPEDMD800G4      | 800 GB | 159BF35EA03D | 1275  | 0     | 3.49   |
| Intel     | SSDPEDMD800G4      | 800 GB | 080DF86C9F8C | 1274  | 0     | 3.49   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | 2A7E54A1E64F | 1273  | 0     | 3.49   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | 5B97BD072487 | 1273  | 0     | 3.49   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | DA77585C98C3 | 1273  | 0     | 3.49   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | 56BEE896D9B4 | 1273  | 0     | 3.49   |
| Intel     | SSDPEDMD800G4      | 800 GB | B170894277FF | 1272  | 0     | 3.49   |
| Intel     | SSDPE2MX450G7      | 450 GB | D78D1F5A202E | 1269  | 0     | 3.48   |
| Intel     | SSDPEDMX020T7      | 2 TB   | C8FB43893562 | 1267  | 0     | 3.47   |
| Samsung   | MZ1LV480HCHP-00003 | 480 GB | 277A2B8BFCF1 | 1267  | 0     | 3.47   |
| Intel     | SSDPEDMX020T7      | 2 TB   | 99C0C154F5AA | 1263  | 0     | 3.46   |
| Intel     | SSDPEDMD020T4D ... | 2 TB   | 12A61F7EA85E | 1263  | 0     | 3.46   |
| Intel     | SSDPEDMD400G4      | 400 GB | 8D48FC32E697 | 1261  | 0     | 3.46   |
| Intel     | SSDPEKKW256G7      | 256 GB | D7BAE5372A38 | 1260  | 0     | 3.45   |
| Samsung   | MZ1LV480HCHP-00003 | 480 GB | 8CDBE2D568D8 | 1260  | 0     | 3.45   |
| Intel     | SSDPEDMD800G4      | 800 GB | 490AC0B17D7D | 1258  | 0     | 3.45   |
| Samsung   | MZQLW960HMJP-00003 | 960 GB | A05F7856297F | 1258  | 0     | 3.45   |
| Intel     | SSDPEDMD800G4      | 800 GB | A07B42E450CB | 1258  | 0     | 3.45   |
| Intel     | SSDPEDMD800G4      | 800 GB | 3F66B8DA513A | 1257  | 0     | 3.45   |
| Intel     | SSDPEDMD800G4      | 800 GB | BDDC88CF9394 | 1257  | 0     | 3.45   |
| Intel     | SSDPEDME400G4      | 400 GB | 9F0F7855A9F7 | 1254  | 0     | 3.44   |
| Intel     | SSDPEDMW800G4      | 800 GB | DAF4052DD60F | 1254  | 0     | 3.44   |
| Intel     | SSDPEDMX012T7      | 1.2 TB | EA738F489276 | 1250  | 0     | 3.43   |
| Intel     | SSDPE2MX450G7      | 450 GB | 16362022026F | 1250  | 0     | 3.43   |
| Intel     | SSDPEDMD400G4      | 400 GB | 19121FE2FFAD | 1249  | 0     | 3.42   |
| Intel     | SSDPEDMD400G4      | 400 GB | A610BFF6A40C | 1249  | 0     | 3.42   |
| Intel     | SSDPEDMD400G4      | 400 GB | 0CCD9A225229 | 1247  | 0     | 3.42   |
| Intel     | SSDPEDMD400G4      | 400 GB | A64F9B020C6D | 1247  | 0     | 3.42   |
| Intel     | SSDPEDMD400G4      | 400 GB | BD999E0139B0 | 1247  | 0     | 3.42   |
| Intel     | SSDPE2KE016T7      | 1.6 TB | 5CDB83708061 | 1247  | 0     | 3.42   |
| Intel     | SSDPEDMD400G4      | 400 GB | 32FEB3694C0A | 1245  | 0     | 3.41   |
| Intel     | SSDPEDMD400G4      | 400 GB | 7B1CB260E613 | 1245  | 0     | 3.41   |
| Intel     | SSDPEDMD800G4      | 800 GB | 211F7A8F5F41 | 1245  | 0     | 3.41   |
| Intel     | SSDPEDMD400G4      | 400 GB | 43C7491777FB | 1245  | 0     | 3.41   |
| Intel     | SSDPEDMD400G4      | 400 GB | 9E263049BEE1 | 1245  | 0     | 3.41   |
| Intel     | SSDPEDMD400G4      | 400 GB | 923BD77F9AF0 | 1245  | 0     | 3.41   |
| Intel     | SSDPE2MX450G7      | 450 GB | DE2BEC677C05 | 1244  | 0     | 3.41   |
| Intel     | SSDPEDMD400G4      | 400 GB | 0BD082DBEE83 | 1243  | 0     | 3.41   |
| Intel     | SSDPEDMD400G4      | 400 GB | 39CD5D329746 | 1243  | 0     | 3.41   |
| Intel     | SSDPEDMD400G4      | 400 GB | 2BFE167E992D | 1240  | 0     | 3.40   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | 2ADCD2CEF024 | 1239  | 0     | 3.40   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | 3E718041CEBA | 1239  | 0     | 3.40   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | 106AEA750973 | 1239  | 0     | 3.39   |
| Intel     | SSDPEDMD800G4      | 800 GB | EADEDDC251D2 | 1238  | 0     | 3.39   |
| Intel     | SSDPEDMD800G4      | 800 GB | 65A3943913AE | 1236  | 0     | 3.39   |
| Intel     | SSDPEDMD800G4      | 800 GB | CFF12FDBCDC7 | 1236  | 0     | 3.39   |
| Dell      | Express Flash N... | 1.6 TB | A8A3365B1EDE | 1235  | 0     | 3.39   |
| Intel     | SSDPEDMD400G4      | 400 GB | 1EC4A820021E | 1235  | 0     | 3.39   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | 4CFE5EFC10A2 | 1234  | 0     | 3.38   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | FCDF319AD456 | 1234  | 0     | 3.38   |
| Intel     | SSDPEDMD800G4      | 800 GB | 81A99F947193 | 1234  | 0     | 3.38   |
| Intel     | SSDPEDMD800G4      | 800 GB | A34FCEA14FBD | 1234  | 0     | 3.38   |
| Intel     | SSDPEDMD800G4      | 800 GB | A474662691AA | 1234  | 0     | 3.38   |
| Intel     | SSDPEDMD800G4      | 800 GB | C709EDBB7E34 | 1234  | 0     | 3.38   |
| Intel     | SSDPEDMD800G4      | 800 GB | 3AF1FA2DE974 | 1233  | 0     | 3.38   |
| Intel     | SSDPEDMD800G4      | 800 GB | 8876A921D77A | 1233  | 0     | 3.38   |
| Intel     | SSDPEDMD800G4      | 800 GB | 17603B2110E5 | 1233  | 0     | 3.38   |
| Intel     | SSDPEDMD800G4      | 800 GB | AA908A36082C | 1230  | 0     | 3.37   |
| Intel     | SSDPEDMD400G4      | 400 GB | 45E17B98DA1C | 1228  | 0     | 3.37   |
| Intel     | SSDPEDMD800G4      | 800 GB | 10024A008465 | 1228  | 0     | 3.36   |
| Intel     | SSDPEDMD400G4      | 400 GB | BF9EE71F8E97 | 1224  | 0     | 3.36   |
| Intel     | SSDPED1D480GA      | 480 GB | D2A5A273083F | 1223  | 0     | 3.35   |
| Intel     | SSDPEDMD400G4      | 400 GB | 1F10A65B9955 | 1222  | 0     | 3.35   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | 0C49A9821A49 | 1221  | 0     | 3.35   |
| Intel     | SSDPEDMX020T7      | 2 TB   | 78F6EF25425E | 1219  | 0     | 3.34   |
| Intel     | SSDPEDMD800G4      | 800 GB | CF62E379F840 | 1218  | 0     | 3.34   |
| Intel     | SSDPEDMX020T7      | 2 TB   | 9A3F54E0A431 | 1218  | 0     | 3.34   |
| Intel     | SSDPEDMD400G4      | 400 GB | 760BB202502C | 1218  | 0     | 3.34   |
| Intel     | SSDPEDMD400G4      | 400 GB | D86C50DC1284 | 1218  | 0     | 3.34   |
| Intel     | SSDPEDMD800G4      | 800 GB | B408D398878B | 1216  | 0     | 3.33   |
| Intel     | SSDPEKKW256G7      | 256 GB | C13034B08D21 | 1214  | 0     | 3.33   |
| Intel     | SSDPEDMD800G4      | 800 GB | 980951A17450 | 1214  | 0     | 3.33   |
| Intel     | SSDPEDMD400G4      | 400 GB | 4BC2205D7862 | 1213  | 0     | 3.33   |
| Intel     | SSDPEDMD400G4      | 400 GB | C11B104C678B | 1213  | 0     | 3.33   |
| Intel     | SSDPEDMD400G4      | 400 GB | 265F11135D56 | 1213  | 0     | 3.33   |
| Intel     | SSDPED1D280GA      | 280 GB | 5E4A0ACA0490 | 1213  | 0     | 3.33   |
| Intel     | SSDPEDMD800G4      | 800 GB | 78899D2CB43F | 1212  | 0     | 3.32   |
| Intel     | SSDPEDMD800G4      | 800 GB | A4F7D10F0551 | 1212  | 0     | 3.32   |
| Intel     | SSDPEDKE020T7      | 2 TB   | 82937DC8D18B | 1212  | 0     | 3.32   |
| Intel     | SSDPEDMD800G4      | 800 GB | C6AF6770B1CD | 1212  | 0     | 3.32   |
| Intel     | SSDPEDMD800G4      | 800 GB | 7D7A3B204580 | 1211  | 0     | 3.32   |
| Intel     | SSDPEDMD400G4      | 400 GB | 36792CE46505 | 1211  | 0     | 3.32   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | 7618E9B6B8FF | 1207  | 0     | 3.31   |
| Intel     | SSDPEDMD400G4      | 400 GB | 95E3C0AB1B02 | 1207  | 0     | 3.31   |
| Intel     | SSDPEDMD400G4      | 400 GB | 4BF60A970DC1 | 1207  | 0     | 3.31   |
| Intel     | SSDPEKKW256G7      | 256 GB | C01BA7F46982 | 1206  | 0     | 3.31   |
| Intel     | SSDPEDMD400G4      | 400 GB | A9BDEF637A85 | 1206  | 0     | 3.31   |
| Intel     | SSDPED1D480GA      | 480 GB | E644C8441694 | 1206  | 0     | 3.30   |
| Intel     | SSDPEDMD400G4      | 400 GB | B35D61308AFA | 1206  | 0     | 3.30   |
| Intel     | SSDPEDMD400G4      | 400 GB | 05C0FACC56BF | 1204  | 0     | 3.30   |
| Intel     | SSDPEDMD400G4      | 400 GB | 0DFA6220BDBB | 1203  | 0     | 3.30   |
| Intel     | SSDPEDMD800G4      | 800 GB | AABC0E477F62 | 1198  | 0     | 3.28   |
| Intel     | SSDPEDMD400G4      | 400 GB | 397FCF4460D2 | 1198  | 0     | 3.28   |
| Intel     | SSDPEDMX020T7      | 2 TB   | 30A5ED9B8A97 | 1197  | 0     | 3.28   |
| Intel     | SSDPEDMD400G4      | 400 GB | CB0BDC0C2650 | 1194  | 0     | 3.27   |
| Intel     | SSDPEDMD400G4      | 400 GB | 8F93A25CC16F | 1193  | 0     | 3.27   |
| Intel     | SSDPED1D480GA      | 480 GB | 8BC28B9ED4F8 | 1192  | 0     | 3.27   |
| Samsung   | SSD 960 EVO        | 500 GB | 7EA943529CF0 | 1191  | 0     | 3.26   |
| Dell      | Express Flash N... | 2 TB   | 0E19DBE5059A | 1190  | 0     | 3.26   |
| Intel     | SSDPEDMD800G4      | 800 GB | 1BC8D7CB9317 | 1189  | 0     | 3.26   |
| Toshiba   | THNSN5512GPU7      | 512 GB | A4C4D1C887E3 | 1186  | 0     | 3.25   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | D88A32F67C08 | 1185  | 0     | 3.25   |
| Intel     | SSDPEDMX020T7      | 2 TB   | 1535C31D0F8B | 1183  | 0     | 3.24   |
| Samsung   | SSD 960 EVO        | 500 GB | 7500AF54FCC3 | 1182  | 0     | 3.24   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | CD189194B8D2 | 1181  | 0     | 3.24   |
| Intel     | SSDPEDMD800G4      | 800 GB | D3EF0E4F932B | 1181  | 0     | 3.24   |
| Intel     | SSDPE2KE020T7      | 2 TB   | ADB95430E54C | 1180  | 0     | 3.23   |
| Intel     | SSDPEDKX040T7      | 4 TB   | 40B9700DA6C8 | 1180  | 0     | 3.23   |
| Dell      | Express Flash N... | 2 TB   | FB2F009429D6 | 1180  | 0     | 3.23   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | DC192532831F | 1179  | 0     | 3.23   |
| Intel     | SSDPEDMD800G4      | 800 GB | 5C8465DFC43C | 1177  | 0     | 3.23   |
| Intel     | SSDPEDMX020T7      | 2 TB   | F74246B2C26E | 1175  | 0     | 3.22   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | 14D9BB26A262 | 1174  | 0     | 3.22   |
| Dell      | Express Flash N... | 2 TB   | 973353DC546E | 1173  | 0     | 3.21   |
| Samsung   | MZ1LB960HAJQ-00007 | 960 GB | 738503362BC4 | 1172  | 0     | 3.21   |
| Intel     | SSDPEDMD800G4      | 800 GB | F52B04351D2F | 1172  | 0     | 3.21   |
| Intel     | SSDPEDMD800G4      | 800 GB | D07389FEB667 | 1170  | 0     | 3.21   |
| Intel     | SSDPEDMD800G4      | 800 GB | 6602C2B54130 | 1170  | 0     | 3.21   |
| Intel     | SSDPE2MX450G7      | 450 GB | A3C1E09A8B45 | 1169  | 0     | 3.20   |
| Intel     | SSDPE2MX450G7      | 450 GB | 397F6CB5C89E | 1169  | 0     | 3.20   |
| Intel     | SSDPEDMX020T7      | 2 TB   | BAE708C244B2 | 1166  | 0     | 3.20   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | 7D488949752F | 1166  | 0     | 3.20   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | EED8E14D3B86 | 1166  | 0     | 3.20   |
| Intel     | SSDPEDMD800G4      | 800 GB | DB99B84014F6 | 1164  | 0     | 3.19   |
| Dell      | Express Flash N... | 2 TB   | 97ACF85A4011 | 1164  | 0     | 3.19   |
| Intel     | SSDPEDMD800G4      | 800 GB | 5EFEBD7832A8 | 1163  | 0     | 3.19   |
| Dell      | Express Flash N... | 1.6 TB | 94734038D22A | 1163  | 0     | 3.19   |
| Dell      | Express Flash N... | 2 TB   | F4CC9A919309 | 1163  | 0     | 3.19   |
| Intel     | SSDPEDMX020T7      | 2 TB   | 9EAF358D5D6E | 1163  | 0     | 3.19   |
| Intel     | SSDPEDMX020T7      | 2 TB   | 97BD6085BEF7 | 1162  | 0     | 3.19   |
| Dell      | Express Flash N... | 2 TB   | DCA816EE81C3 | 1161  | 0     | 3.18   |
| Dell      | Express Flash N... | 2 TB   | ADFBF60E48D4 | 1161  | 0     | 3.18   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | 1A330F24753F | 1161  | 0     | 3.18   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | 3A61D52906C9 | 1161  | 0     | 3.18   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | BD291903C93D | 1161  | 0     | 3.18   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | 9B755FF49EC0 | 1160  | 0     | 3.18   |
| Samsung   | SSD 960 EVO        | 500 GB | 53C0AC2A16CF | 1158  | 0     | 3.18   |
| Intel     | SSDPEDMD016T4      | 1.6 TB | 65C23BF6A9F3 | 1158  | 0     | 3.17   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | 198A751ADA6E | 1158  | 0     | 3.17   |
| Intel     | SSDPE2MD400G4      | 400 GB | 40C935EF5FBD | 1157  | 0     | 3.17   |
| Intel     | SSDPE2MD400G4      | 400 GB | 5ACEB1975670 | 1157  | 0     | 3.17   |
| Intel     | SSDPED1K375GA      | 375 GB | 4062675EC36D | 1156  | 0     | 3.17   |
| Intel     | SSDPEDMD016T4      | 1.6 TB | E4DA8F59A54D | 1154  | 0     | 3.16   |
| Intel     | SSDPEDMD016T4      | 1.6 TB | 14886D890417 | 1153  | 0     | 3.16   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | CBE5C45550C8 | 1153  | 0     | 3.16   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | C84192B695F3 | 1152  | 0     | 3.16   |
| Intel     | SSDPEDMD016T4      | 1.6 TB | 677DF6E4B0CF | 1151  | 0     | 3.16   |
| Intel     | SSDPEDMX020T7      | 2 TB   | 19EA02418172 | 1151  | 0     | 3.15   |
| Intel     | SSDPEDMD016T4      | 1.6 TB | 71720844C89B | 1151  | 0     | 3.15   |
| Intel     | SSDPEDKX040T7      | 4 TB   | FB0AB6EEA440 | 1150  | 0     | 3.15   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | 8104391639FF | 1150  | 0     | 3.15   |
| Intel     | SSDPEDMD016T4      | 1.6 TB | E2A6F1440E0D | 1149  | 0     | 3.15   |
| Dell      | Express Flash N... | 2 TB   | FF38BAAB763A | 1149  | 0     | 3.15   |
| Dell      | Express Flash N... | 1.6 TB | 1608F888D62C | 1149  | 0     | 3.15   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | 7AFA4BDA77C4 | 1148  | 0     | 3.15   |
| Dell      | Express Flash N... | 2 TB   | CF4D6055B4DD | 1148  | 0     | 3.15   |
| Dell      | Express Flash N... | 2 TB   | FD51F7A48FDA | 1147  | 0     | 3.15   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | B44B594186B8 | 1147  | 0     | 3.15   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | 5734ED13C159 | 1147  | 0     | 3.14   |
| Dell      | Express Flash N... | 2 TB   | C6690BF17B55 | 1146  | 0     | 3.14   |
| Intel     | SSDPEDMD800G4      | 800 GB | 2D0E57B14374 | 1146  | 0     | 3.14   |
| Intel     | SSDPEDMD800G4      | 800 GB | 45C3F0AD5510 | 1146  | 0     | 3.14   |
| Intel     | SSDPEDMD800G4      | 800 GB | 196C42776F55 | 1142  | 0     | 3.13   |
| Intel     | SSDPEDMW400G4      | 400 GB | CFAFAC13EF45 | 1142  | 0     | 3.13   |
| Intel     | SSDPEDMW400G4      | 400 GB | BDA79FD0DD25 | 1142  | 0     | 3.13   |
| Intel     | SSDPEDMW400G4      | 400 GB | C5E365CF5856 | 1141  | 0     | 3.13   |
| Intel     | SSDPE2MD400G4      | 400 GB | BEF97C57D94C | 1140  | 0     | 3.12   |
| Intel     | SSDPED1K375GA      | 375 GB | 14A98FC5B7A8 | 1137  | 0     | 3.12   |
| Intel     | SSDPEDMD800G4      | 800 GB | D1AC5048F85E | 1137  | 0     | 3.12   |
| Intel     | SSDPEDMW400G4      | 400 GB | 8980C65862C9 | 1137  | 0     | 3.12   |
| Intel     | SSDPEDMD800G4      | 800 GB | 45C3D0FE4428 | 1135  | 0     | 3.11   |
| Intel     | SSDPEDMD800G4      | 800 GB | 72364592C6B3 | 1135  | 0     | 3.11   |
| Intel     | SSDPEDMW400G4      | 400 GB | C64AFA9F99F5 | 1135  | 0     | 3.11   |
| Intel     | SSDPEDMD800G4      | 800 GB | 23FAF0412D25 | 1135  | 0     | 3.11   |
| Intel     | SSDPEDMD800G4      | 800 GB | 7D37888EDB0D | 1135  | 0     | 3.11   |
| Intel     | SSDPEDMD800G4      | 800 GB | DE92608AB1EB | 1135  | 0     | 3.11   |
| Intel     | SSDPE2ME020T4      | 2 TB   | 1DE4AE988B45 | 1135  | 0     | 3.11   |
| Intel     | SSDPEDMD800G4      | 800 GB | 4298ABE8E37E | 1135  | 0     | 3.11   |
| Intel     | SSDPEDMD800G4      | 800 GB | 9C2FB9293D47 | 1135  | 0     | 3.11   |
| Intel     | SSDPEDMD800G4      | 800 GB | C45E64C4D66F | 1134  | 0     | 3.11   |
| Intel     | SSDPE2MX450G7      | 450 GB | 0A3639E9A3EB | 1134  | 0     | 3.11   |
| Intel     | SSDPEDMD800G4      | 800 GB | 2679AA3B6D39 | 1134  | 0     | 3.11   |
| Intel     | SSDPEDMD800G4      | 800 GB | D372DC6D90A3 | 1134  | 0     | 3.11   |
| Intel     | SSDPEDMD800G4      | 800 GB | AEB9B68212DE | 1134  | 0     | 3.11   |
| Intel     | SSDPEDMD400G4      | 400 GB | 0EC6935E298F | 1134  | 0     | 3.11   |
| Intel     | SSDPE2ME020T4      | 2 TB   | 49182C9677AF | 1132  | 0     | 3.10   |
| Intel     | SSDPEDMX020T7      | 2 TB   | 84A5794AF11B | 1132  | 0     | 3.10   |
| Intel     | SSDPEDMD800G4      | 800 GB | BD3D20BBE74B | 1132  | 0     | 3.10   |
| Intel     | SSDPEKKW256G7      | 256 GB | 0846D1AAA0E1 | 1132  | 0     | 3.10   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | 32ECF22E9565 | 1132  | 0     | 3.10   |
| Intel     | SSDPEDMD400G4      | 400 GB | B30A3A4C622B | 1132  | 0     | 3.10   |
| Intel     | SSDPED1D480GA      | 480 GB | C519BAE09C3E | 1132  | 0     | 3.10   |
| Intel     | SSDPE2MD400G4      | 400 GB | 87C4EC2731B3 | 1129  | 0     | 3.09   |
| Dell      | Express Flash N... | 2 TB   | 9ECA12900D7F | 1127  | 0     | 3.09   |
| Dell      | Express Flash N... | 2 TB   | C8AB20244F67 | 1127  | 0     | 3.09   |
| Intel     | SSDPEDMD800G4      | 800 GB | EE472D7E831D | 1127  | 0     | 3.09   |
| Dell      | Express Flash N... | 2 TB   | 008D33EF78BF | 1127  | 0     | 3.09   |
| Intel     | SSDPEDMD800G4      | 800 GB | 5ACAC524874A | 1126  | 0     | 3.09   |
| Intel     | SSDPED1K375GA      | 375 GB | 841067E94ECB | 1126  | 0     | 3.09   |
| Intel     | SSDPEDMD800G4      | 800 GB | 808F98AC59E5 | 1125  | 0     | 3.08   |
| Intel     | SSDPEDMW400G4      | 400 GB | FEE125E9EC43 | 1122  | 0     | 3.07   |
| Micron    | MTFDHAL1T2MCF-1... | 1.2 TB | B8C60344ABE9 | 1120  | 0     | 3.07   |
| Intel     | SSDPEDMD800G4      | 800 GB | F00BF614DA32 | 1119  | 0     | 3.07   |
| Intel     | SSDPEDMD800G4      | 800 GB | EE4A250C864E | 1118  | 0     | 3.06   |
| Intel     | SSDPEDMD016T4      | 1.6 TB | CF11FE0B0C20 | 1115  | 0     | 3.06   |
| Intel     | SSDPEDMX020T7      | 2 TB   | 2D096DC87190 | 1114  | 0     | 3.05   |
| Intel     | SSDPEDMD020T4D ... | 2 TB   | 2A481C175E03 | 1114  | 0     | 3.05   |
| Dell      | Express Flash N... | 1.6 TB | 641896A423A9 | 1114  | 0     | 3.05   |
| Intel     | SSDPEDKX040T7      | 4 TB   | 66394C970A86 | 1114  | 0     | 3.05   |
| Intel     | SSDPEDMD800G4      | 800 GB | 22EBAD071670 | 1113  | 0     | 3.05   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | 80995635F36F | 1113  | 0     | 3.05   |
| Intel     | SSDPE2MX450G7      | 450 GB | 020B5D27D7E5 | 1113  | 0     | 3.05   |
| Intel     | SSDPEDKX040T7      | 4 TB   | E5C25D2C0AD6 | 1112  | 0     | 3.05   |
| Intel     | SSDPEDKX040T7      | 4 TB   | AD38D54CD9B6 | 1111  | 0     | 3.05   |
| Intel     | SSDPEDMD800G4      | 800 GB | E915218EDA51 | 1110  | 0     | 3.04   |
| Intel     | SSDPEDMD800G4      | 800 GB | 2DBBE7CBE1E5 | 1110  | 0     | 3.04   |
| Intel     | SSDPEDMD800G4      | 800 GB | 60D2A543F5D3 | 1110  | 0     | 3.04   |
| Intel     | SSDPEDMD800G4      | 800 GB | 2BD3D43A2DF0 | 1109  | 0     | 3.04   |
| Intel     | SSDPEDMD400G4      | 400 GB | 279D3F5A2BFF | 1106  | 0     | 3.03   |
| Intel     | SSDPEDMD800G4      | 800 GB | 8E5BD0F6AC00 | 1106  | 0     | 3.03   |
| Samsung   | MZVKW512HMJP-00000 | 512 GB | 327069DB522D | 1105  | 0     | 3.03   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | FC420F8DE189 | 1105  | 0     | 3.03   |
| Intel     | SSDPEDMD800G4      | 800 GB | BEC21C18563F | 1105  | 0     | 3.03   |
| Intel     | SSDPEDMD800G4      | 800 GB | 1FF28FF915E9 | 1104  | 0     | 3.03   |
| Intel     | SSDPEDMD800G4      | 800 GB | 9B6D0F6FD3E3 | 1104  | 0     | 3.03   |
| Intel     | SSDPEDMD800G4      | 800 GB | 51C80C68CF56 | 1104  | 0     | 3.02   |
| Intel     | SSDPEDMD800G4      | 800 GB | 7CDC6D3055FB | 1104  | 0     | 3.02   |
| Intel     | SSDPEDMD016T4      | 1.6 TB | 997C6C7B749E | 1103  | 0     | 3.02   |
| Intel     | SSDPE2MX012T7      | 1.2 TB | 389E63A9C63C | 1101  | 0     | 3.02   |
| Intel     | SSDPE2MX012T7      | 1.2 TB | CBECFEFB949D | 1101  | 0     | 3.02   |
| Intel     | SSDPEDMD800G4      | 800 GB | 1328C9AE0075 | 1101  | 0     | 3.02   |
| Intel     | SSDPEDMD800G4      | 800 GB | 7FB0E8875D4C | 1101  | 0     | 3.02   |
| Intel     | SSDPEDKX040T7      | 4 TB   | C250FCB24973 | 1098  | 0     | 3.01   |
| Intel     | SSDPE2KX040T8      | 4 TB   | 04EEC079E147 | 1096  | 0     | 3.00   |
| Intel     | SSDPE2KX040T8      | 4 TB   | DF44B64A4133 | 1096  | 0     | 3.00   |
| Intel     | SSDPE2KX040T8      | 4 TB   | 8748546BF8A3 | 1096  | 0     | 3.00   |
| Intel     | SSDPE2KX040T8      | 4 TB   | 4D6D5FE4311B | 1095  | 0     | 3.00   |
| Intel     | SSDPEDMD800G4      | 800 GB | F728AA2FE47D | 1095  | 0     | 3.00   |
| Intel     | SSDPE2MX012T7      | 1.2 TB | 59A983DE601F | 1093  | 0     | 3.00   |
| Intel     | SSDPE2MX012T7      | 1.2 TB | D19C5D17B544 | 1093  | 0     | 3.00   |
| Intel     | SSDPEDMD800G4      | 800 GB | 925BA38A8E36 | 1093  | 0     | 3.00   |
| Intel     | SSDPE2MX012T7      | 1.2 TB | 6EABE37D14DA | 1092  | 0     | 2.99   |
| Intel     | SSDPED1D280GA      | 280 GB | 458E7415FC77 | 1091  | 0     | 2.99   |
| Intel     | SSDPEDMD800G4      | 800 GB | 4617D63D2221 | 1090  | 0     | 2.99   |
| Intel     | SSDPEDMD800G4      | 800 GB | 9251BDB10402 | 1090  | 0     | 2.99   |
| Intel     | SSDPE2KX040T8      | 4 TB   | 3C3B146EC798 | 1087  | 0     | 2.98   |
| Intel     | SSDPEDMD800G4      | 800 GB | 9406364C5CEC | 1086  | 0     | 2.98   |
| Toshiba   | THNSN5512GPU7      | 512 GB | 02F6D93801B3 | 1084  | 0     | 2.97   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | C3A8CEF9A410 | 1083  | 0     | 2.97   |
| Intel     | SSDPE2KX040T8      | 4 TB   | 10870D528EAD | 1081  | 0     | 2.96   |
| Intel     | SSDPE2KX040T8      | 4 TB   | FB642A59AAF9 | 1081  | 0     | 2.96   |
| Intel     | SSDPEDMD016T4      | 1.6 TB | BFBE5361D92C | 1080  | 0     | 2.96   |
| Intel     | SSDPE2MX012T7      | 1.2 TB | 90F13FC21866 | 1080  | 0     | 2.96   |
| Intel     | SSDPEDMX020T7      | 2 TB   | 97A16DD0EF41 | 1079  | 0     | 2.96   |
| Intel     | SSDPEDMX020T7      | 2 TB   | 54E2CA29017E | 1079  | 0     | 2.96   |
| Intel     | SSDPE2MX012T7      | 1.2 TB | 4472A9650C37 | 1078  | 0     | 2.95   |
| Intel     | SSDPEDKX040T7      | 4 TB   | 53E5B1D7652B | 1078  | 0     | 2.95   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | 6705F91177E3 | 1078  | 0     | 2.95   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | 75D88A6765E7 | 1078  | 0     | 2.95   |
| Samsung   | MZVKW512HMJP-00000 | 512 GB | 2B28D54A0CF0 | 1078  | 0     | 2.95   |
| Intel     | SSDPEDMX020T7      | 2 TB   | F8556544C460 | 1077  | 0     | 2.95   |
| Intel     | SSDPEDKE020T7      | 2 TB   | A3700C1DC405 | 1076  | 0     | 2.95   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | 02E54760FA31 | 1075  | 0     | 2.95   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | FADC88F36928 | 1075  | 0     | 2.95   |
| Intel     | SSDPEDMX012T7      | 1.2 TB | 25F019834001 | 1074  | 0     | 2.94   |
| Intel     | SSDPEDKX040T7      | 4 TB   | 7DD55B5E157D | 1074  | 0     | 2.94   |
| Intel     | SSDPE2KX040T8      | 4 TB   | 1302D17DF44E | 1072  | 0     | 2.94   |
| Intel     | SSDPEDMD800G4      | 800 GB | 101FA3287283 | 1072  | 0     | 2.94   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | E4795B5A7122 | 1072  | 0     | 2.94   |
| Intel     | SSDPE2MX012T7      | 1.2 TB | A6ABF94B71DD | 1070  | 0     | 2.93   |
| Intel     | SSDPE2KX040T8      | 4 TB   | 82B710F9A23F | 1067  | 0     | 2.93   |
| Intel     | SSDPEDKX040T7      | 4 TB   | 1280C2D4847B | 1067  | 0     | 2.92   |
| Samsung   | MZQLW960HMJP-00003 | 960 GB | 837A5851129F | 1066  | 0     | 2.92   |
| Intel     | SSDPEDMD800G4      | 800 GB | 2B5AB23E9999 | 1064  | 0     | 2.92   |
| Intel     | SSDPED1D480GA      | 480 GB | 181F2B5AB343 | 1062  | 0     | 2.91   |
| Intel     | SSDPEDMD800G4      | 800 GB | 67F3B7B9290F | 1061  | 0     | 2.91   |
| Intel     | SSDPEDKX040T7      | 4 TB   | 60C27904A72B | 1060  | 0     | 2.91   |
| Dell      | Express Flash N... | 1.6 TB | 6EF73D6CF852 | 1060  | 0     | 2.90   |
| Intel     | SSDPEDMD800G4      | 800 GB | 0E2698D0698F | 1057  | 0     | 2.90   |
| Intel     | SSDPEDMD800G4      | 800 GB | 4D6B5C0F7017 | 1057  | 0     | 2.90   |
| Intel     | SSDPEDMD800G4      | 800 GB | 660AE544DD70 | 1057  | 0     | 2.90   |
| Samsung   | MZVLW512HMJP-000H1 | 512 GB | E7786BFF2AC9 | 1056  | 0     | 2.89   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | 35602A5D35E4 | 1055  | 0     | 2.89   |
| Intel     | SSDPEDKX040T7      | 4 TB   | 5282A68E62F1 | 1055  | 0     | 2.89   |
| Intel     | SSDPEK1W120GA      | 118 GB | 7A7F5B15B49C | 1054  | 0     | 2.89   |
| Intel     | SSDPEDMD400G4      | 400 GB | BBEB0DB72E9A | 1053  | 0     | 2.89   |
| Intel     | SSDPEDMD800G4      | 800 GB | 62966C0FC7CB | 1053  | 0     | 2.89   |
| Intel     | SSDPEDMD800G4      | 800 GB | 6FCFF30D0521 | 1053  | 0     | 2.89   |
| Intel     | SSDPEDMD800G4      | 800 GB | EDC812973B98 | 1053  | 0     | 2.89   |
| Samsung   | SSD 970 EVO        | 500 GB | 73699C3819A0 | 1052  | 0     | 2.88   |
| Samsung   | SSD 960 EVO        | 500 GB | 6B96649DAC09 | 1051  | 0     | 2.88   |
| Intel     | SSDPEDMD800G4      | 800 GB | 75FA0C095106 | 1050  | 0     | 2.88   |
| Dell      | Express Flash N... | 1.6 TB | 055AE808E354 | 1050  | 0     | 2.88   |
| Intel     | SSDPEDMD800G4      | 800 GB | 5001AF747420 | 1050  | 0     | 2.88   |
| Intel     | SSDPEDMD800G4      | 800 GB | AD8C1855765B | 1050  | 0     | 2.88   |
| Intel     | SSDPEDMD800G4      | 800 GB | DB7DE8D2E969 | 1050  | 0     | 2.88   |
| Intel     | SSDPEDMD800G4      | 800 GB | 0CA236AEF703 | 1047  | 0     | 2.87   |
| Dell      | Express Flash N... | 1.6 TB | AC2A74F3603E | 1041  | 0     | 2.85   |
| Intel     | SSDPEDMD800G4      | 800 GB | 6B560AB7A394 | 1040  | 0     | 2.85   |
| Intel     | SSDPEDMD400G4      | 400 GB | D0AA026F0E60 | 1040  | 0     | 2.85   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | 2ADEBEB1E750 | 1040  | 0     | 2.85   |
| Intel     | SSDPEDMD400G4      | 400 GB | ACA28EE9D63B | 1040  | 0     | 2.85   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | 812843320BFC | 1039  | 0     | 2.85   |
| Intel     | SSDPEDMD800G4      | 800 GB | 2E426DEBFA8F | 1039  | 0     | 2.85   |
| Intel     | SSDPEDMD800G4      | 800 GB | 730829D4C90C | 1039  | 0     | 2.85   |
| Intel     | SSDPEDMD800G4      | 800 GB | 83411E69A7C0 | 1039  | 0     | 2.85   |
| Intel     | SSDPEDMD800G4      | 800 GB | 821CCED54F59 | 1038  | 0     | 2.85   |
| Intel     | SSDPEDMD800G4      | 800 GB | F6C664D7C092 | 1038  | 0     | 2.85   |
| Intel     | SSDPE2MD400G4L     | 400 GB | EB3EAFD35073 | 1038  | 0     | 2.85   |
| Intel     | SSDPEDMD800G4      | 800 GB | 2A760355CB09 | 1038  | 0     | 2.84   |
| Intel     | SSDPEDMD800G4      | 800 GB | 8AC11D6F5CD9 | 1038  | 0     | 2.84   |
| Intel     | SSDPE2MX450G7      | 450 GB | 27A3AC113247 | 1036  | 0     | 2.84   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | 215FBEF447EF | 1035  | 0     | 2.84   |
| Intel     | SSDPEDMD800G4      | 800 GB | F0233AA68DF1 | 1035  | 0     | 2.84   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | B0F937AFA706 | 1034  | 0     | 2.83   |
| Intel     | SSDPED1D280GA      | 280 GB | 700DC0B7035B | 1034  | 0     | 2.83   |
| Intel     | SSDPEDMD800G4      | 800 GB | C2839523ACF5 | 1033  | 0     | 2.83   |
| Intel     | SSDPEDMD800G4      | 800 GB | 25B150952273 | 1033  | 0     | 2.83   |
| Intel     | SSDPE21K375GA      | 375 GB | 67CA9AA3BD59 | 1033  | 0     | 2.83   |
| Intel     | SSDPE21K375GA      | 375 GB | F810312A49B7 | 1033  | 0     | 2.83   |
| Intel     | SSDPEDMD400G4      | 400 GB | D69A8258C206 | 1033  | 0     | 2.83   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | 9146750E432D | 1032  | 0     | 2.83   |
| Intel     | SSDPED1D280GA      | 280 GB | BF0051BE361A | 1030  | 0     | 2.82   |
| Intel     | SSDPEDMX020T7      | 2 TB   | 94E34C435028 | 1030  | 0     | 2.82   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | 921639C49470 | 1028  | 0     | 2.82   |
| Intel     | SSDPEDKX040T7      | 4 TB   | CBDCE7A83666 | 1027  | 0     | 2.82   |
| Intel     | SSDPEDMD800G4      | 800 GB | AC2DDC999C87 | 1027  | 0     | 2.82   |
| Intel     | SSDPE2MX450G7      | 450 GB | 1F770F7426B7 | 1027  | 0     | 2.81   |
| Intel     | SSDPEDMD800G4      | 800 GB | 6CE45553EAB3 | 1026  | 0     | 2.81   |
| Samsung   | MZVLW512HMJP-000H1 | 512 GB | FCEFC31259BA | 1025  | 0     | 2.81   |
| Intel     | SSDPEDMD800G4      | 800 GB | 3B3F2A3F1703 | 1023  | 0     | 2.81   |
| Intel     | SSDPEDMD800G4      | 800 GB | 95B1FE0205BF | 1023  | 0     | 2.81   |
| Intel     | SSDPEDMD800G4      | 800 GB | 645CA7A3AFC9 | 1023  | 0     | 2.81   |
| Intel     | SSDPEDMD800G4      | 800 GB | 4F12924BAF73 | 1022  | 0     | 2.80   |
| Intel     | SSDPE21K375GA      | 375 GB | 000D23D5182F | 1022  | 0     | 2.80   |
| Intel     | SSDPE21K375GA      | 375 GB | 22B5A0C5BA5F | 1022  | 0     | 2.80   |
| Intel     | SSDPE21K375GA      | 375 GB | 39C774C3CA95 | 1022  | 0     | 2.80   |
| Samsung   | SSD 960 PRO        | 1 TB   | E203E7D849EA | 1022  | 0     | 2.80   |
| Intel     | MT0800KEXUU        | 800 GB | B54F75A39309 | 1022  | 0     | 2.80   |
| Intel     | SSDPEDMD800G4      | 800 GB | 3C0CF173F070 | 1022  | 0     | 2.80   |
| Intel     | SSDPEDMD800G4      | 800 GB | A26FE84B4302 | 1022  | 0     | 2.80   |
| Intel     | SSDPEDMD800G4      | 800 GB | E3555B3D6A0F | 1022  | 0     | 2.80   |
| Intel     | SSDPEDMD800G4      | 800 GB | F05220D07811 | 1022  | 0     | 2.80   |
| Intel     | SSDPE21K375GA      | 375 GB | 3C1B054F4776 | 1021  | 0     | 2.80   |
| Intel     | SSDPE21K375GA      | 375 GB | 753AF5563E4F | 1020  | 0     | 2.80   |
| Intel     | SSDPE21K375GA      | 375 GB | 976054069C87 | 1020  | 0     | 2.80   |
| Intel     | SSDPE21K375GA      | 375 GB | 9C3877BD523D | 1020  | 0     | 2.80   |
| Intel     | SSDPE21K375GA      | 375 GB | BBEBE6D263DD | 1020  | 0     | 2.80   |
| Intel     | SSDPEDMD800G4      | 800 GB | 2FF5BF9C45B9 | 1019  | 0     | 2.79   |
| Intel     | SSDPEDMD800G4      | 800 GB | ADA067E9A394 | 1019  | 0     | 2.79   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | 416EA2A2670C | 1018  | 0     | 2.79   |
| Intel     | SSDPE2MX450G7      | 450 GB | 5D0DC140A7FF | 1018  | 0     | 2.79   |
| Intel     | SSDPE2MX450G7      | 450 GB | 98D697C1D241 | 1018  | 0     | 2.79   |
| Intel     | SSDPE21K375GA      | 375 GB | 3B3347B0381C | 1018  | 0     | 2.79   |
| Intel     | SSDPE21K375GA      | 375 GB | B01AFF864AE3 | 1018  | 0     | 2.79   |
| Intel     | SSDPE21K375GA      | 375 GB | E1CFC10B00FA | 1018  | 0     | 2.79   |
| Intel     | SSDPE21K375GA      | 375 GB | 0B2CE4F33B01 | 1017  | 0     | 2.79   |
| Intel     | SSDPE21K375GA      | 375 GB | 4E4BDD4A1360 | 1017  | 0     | 2.79   |
| Intel     | SSDPE21K375GA      | 375 GB | 91245CCD9ECE | 1017  | 0     | 2.79   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | 9788C24A25BD | 1016  | 0     | 2.79   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | 86D0387B9971 | 1016  | 0     | 2.78   |
| Intel     | SSDPE21K375GA      | 375 GB | 9272EF48DDD1 | 1016  | 0     | 2.78   |
| Intel     | SSDPEDMD800G4      | 800 GB | 6B4DF3B4A4E5 | 1013  | 0     | 2.78   |
| Intel     | SSDPEDMD800G4      | 800 GB | 360F0AB8D19C | 1012  | 0     | 2.77   |
| Intel     | SSDPEDMD800G4      | 800 GB | 82358B91EB99 | 1012  | 0     | 2.77   |
| Intel     | SSDPEDMD800G4      | 800 GB | 9E380C926248 | 1012  | 0     | 2.77   |
| Intel     | SSDPEDMD800G4      | 800 GB | ED223F3E347C | 1012  | 0     | 2.77   |
| Intel     | SSDPE21K375GA      | 375 GB | 7557C471B4DF | 1012  | 0     | 2.77   |
| Intel     | SSDPE21K375GA      | 375 GB | 417F01A1BF92 | 1012  | 0     | 2.77   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | 93CF507F476D | 1011  | 0     | 2.77   |
| Intel     | SSDPEDMD800G4      | 800 GB | 799A9E5E5FCA | 1011  | 0     | 2.77   |
| Intel     | SSDPEDMD800G4      | 800 GB | 8857FACC972F | 1011  | 0     | 2.77   |
| Intel     | SSDPEDMD800G4      | 800 GB | E728BB2D2B11 | 1011  | 0     | 2.77   |
| Toshiba   | THNSN5512GPU7      | 512 GB | D22F16F82F44 | 1011  | 0     | 2.77   |
| Intel     | SSDPE21K375GA      | 375 GB | FA1178016CAA | 1011  | 0     | 2.77   |
| Intel     | SSDPE2MX012T7      | 1.2 TB | 134F738D0752 | 1010  | 0     | 2.77   |
| Intel     | SSDPE2MX012T7      | 1.2 TB | DBD0B94DB389 | 1010  | 0     | 2.77   |
| Intel     | SSDPEDMD800G4      | 800 GB | F006AC331180 | 1010  | 0     | 2.77   |
| Intel     | SSDPED1D480GA      | 480 GB | 6DD605193869 | 1009  | 0     | 2.77   |
| Intel     | SSDPEDMD400G4      | 400 GB | AFCFAA64278E | 1009  | 0     | 2.77   |
| Intel     | SSDPEDMD800G4      | 800 GB | 63C3F2CD6C44 | 1009  | 0     | 2.76   |
| Intel     | SSDPEDMD800G4      | 800 GB | F9B4D17A12C4 | 1009  | 0     | 2.76   |
| Intel     | SSDPEDMD800G4      | 800 GB | 18DEACE3B4A3 | 1008  | 0     | 2.76   |
| Intel     | SSDPEDMD800G4      | 800 GB | 540EB3F3DF59 | 1008  | 0     | 2.76   |
| Intel     | SSDPEDMD800G4      | 800 GB | A53BDCE68193 | 1008  | 0     | 2.76   |
| Intel     | SSDPEDMD800G4      | 800 GB | BF6D3B672E10 | 1008  | 0     | 2.76   |
| Intel     | SSDPEDMD800G4      | 800 GB | 666BD20DEED8 | 1008  | 0     | 2.76   |
| Intel     | SSDPEDMD800G4      | 800 GB | 793F244311EB | 1008  | 0     | 2.76   |
| Intel     | SSDPEDMX020T7      | 2 TB   | E03C1DD4121A | 1007  | 0     | 2.76   |
| Intel     | SSDPED1D480GA      | 480 GB | 67C035F147DC | 1007  | 0     | 2.76   |
| Intel     | SSDPEDMD800G4      | 800 GB | ADC04C0D9552 | 1005  | 0     | 2.76   |
| Intel     | SSDPEDMD800G4      | 800 GB | FA4C0C607557 | 1004  | 0     | 2.75   |
| Intel     | SSDPEDMD800G4      | 800 GB | 12F6E40E2065 | 1004  | 0     | 2.75   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | A19F66CFEE0D | 1003  | 0     | 2.75   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | 91D2954EFA79 | 1003  | 0     | 2.75   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | C36EE79DE8F3 | 999   | 0     | 2.74   |
| Intel     | SSDPEDMD800G4      | 800 GB | 870BB96E8B73 | 993   | 0     | 2.72   |
| Toshiba   | KXG50ZNV512G       | 512 GB | 32926FBE5755 | 992   | 0     | 2.72   |
| Intel     | SSDPEDMD800G4      | 800 GB | 8D45ADCA77E5 | 992   | 0     | 2.72   |
| Intel     | SSDPEDMD800G4      | 800 GB | 75DB3168056B | 992   | 0     | 2.72   |
| Intel     | SSDPEDMD800G4      | 800 GB | 17B594E32488 | 991   | 0     | 2.72   |
| Intel     | SSDPEDMD800G4      | 800 GB | 06BF322A52B4 | 991   | 0     | 2.72   |
| Intel     | SSDPEDMD800G4      | 800 GB | B08ABD87C060 | 991   | 0     | 2.72   |
| Intel     | SSDPEDMD800G4      | 800 GB | EF226A4C6392 | 991   | 0     | 2.72   |
| Samsung   | SSD 970 PRO        | 512 GB | 075FF841F1C4 | 990   | 0     | 2.71   |
| Intel     | SSDPED1K375GA      | 375 GB | 3EC26DA1424A | 990   | 0     | 2.71   |
| Samsung   | SSD 970 PRO        | 512 GB | DFE11AA6889C | 989   | 0     | 2.71   |
| Intel     | SSDPE2KX040T8      | 4 TB   | 5B42692437CA | 988   | 0     | 2.71   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | BA66ADC31FDC | 987   | 0     | 2.71   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | 4F0E23684E06 | 986   | 0     | 2.70   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | 86D96C09A2C9 | 984   | 0     | 2.70   |
| Toshiba   | KXG50ZNV512G       | 512 GB | 23296697CF73 | 984   | 0     | 2.70   |
| Intel     | SSDPEDMD800G4      | 800 GB | 56056DB1744C | 982   | 0     | 2.69   |
| Intel     | SSDPEDMD800G4      | 800 GB | BD6D594A9DF5 | 982   | 0     | 2.69   |
| Intel     | SSDPEDMD400G4      | 400 GB | AFDE07FC4A01 | 981   | 0     | 2.69   |
| Intel     | SSDPEDMD800G4      | 800 GB | 1579210229B1 | 980   | 0     | 2.69   |
| Intel     | SSDPED1K375GA      | 375 GB | A92FA992C6C7 | 979   | 0     | 2.68   |
| Intel     | SSDPEDMD800G4      | 800 GB | 30E3D750DD4C | 978   | 0     | 2.68   |
| Intel     | SSDPEDMD800G4      | 800 GB | 5D16DCE210C2 | 978   | 0     | 2.68   |
| Intel     | SSDPEDMD800G4      | 800 GB | 91FFEBD74886 | 978   | 0     | 2.68   |
| Intel     | SSDPEDKE020T7      | 2 TB   | 4259217B98B5 | 976   | 0     | 2.67   |
| Toshiba   | KXG50ZNV512G       | 512 GB | 4C536B56E028 | 974   | 0     | 2.67   |
| Intel     | SSDPE2MD400G4      | 400 GB | 17039D97FC62 | 973   | 0     | 2.67   |
| Intel     | SSDPED1D280GA      | 280 GB | 23375EC468D8 | 973   | 0     | 2.67   |
| Intel     | SSDPED1K375GA      | 375 GB | FF5534DAC9E2 | 971   | 0     | 2.66   |
| Intel     | SSDPEDMD800G4      | 800 GB | 2A44CEBC1846 | 971   | 0     | 2.66   |
| Intel     | SSDPEDMD800G4      | 800 GB | BDC36DE789B8 | 971   | 0     | 2.66   |
| Intel     | SSDPED1D280GA      | 280 GB | E376876255B7 | 970   | 0     | 2.66   |
| Intel     | SSDPEDKE020T7      | 2 TB   | 7A8DD8EF072E | 969   | 0     | 2.66   |
| Intel     | SSDPEDMD800G4      | 800 GB | 5BC945EB3028 | 969   | 0     | 2.66   |
| Intel     | SSDPEDMD800G4      | 800 GB | 7DB2D2F4CA81 | 969   | 0     | 2.66   |
| Intel     | SSDPED1K375GA      | 375 GB | ECD256A90A39 | 969   | 0     | 2.66   |
| Intel     | SSDPE2KX040T8      | 4 TB   | E629E43F104B | 969   | 0     | 2.66   |
| Samsung   | SSD 960 PRO        | 1 TB   | 7D08D2CEFB6F | 969   | 0     | 2.66   |
| Intel     | SSDPED1D280GA      | 280 GB | B85330B524F6 | 968   | 0     | 2.65   |
| Intel     | SSDPED1K375GA      | 375 GB | CC24F7B018BA | 967   | 0     | 2.65   |
| Intel     | SSDPEDKX040T7      | 4 TB   | F99198B5FC47 | 966   | 0     | 2.65   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | 4E12A40AAE53 | 965   | 0     | 2.65   |
| Samsung   | SSD 970 PRO        | 512 GB | 24A048A5A0C3 | 962   | 0     | 2.64   |
| Intel     | SSDPED1D480GA      | 480 GB | 7980CC43B632 | 961   | 0     | 2.63   |
| Intel     | SSDPE2KX040T8      | 4 TB   | 38CC30A6934F | 960   | 0     | 2.63   |
| Intel     | SSDPED1D480GA      | 480 GB | 01F849BFAA29 | 960   | 0     | 2.63   |
| Intel     | SSDPED1K375GA      | 375 GB | 56F31352C8DD | 957   | 0     | 2.62   |
| Intel     | SSDPEDMD800G4      | 800 GB | F3EACE39B9E7 | 957   | 0     | 2.62   |
| Intel     | SSDPED1D480GA      | 480 GB | 578CD3ABC0AB | 954   | 0     | 2.62   |
| Intel     | SSDPED1D480GA      | 480 GB | 760BBF194220 | 953   | 0     | 2.61   |
| Intel     | SSDPED1D480GA      | 480 GB | E4157880D810 | 953   | 0     | 2.61   |
| Intel     | SSDPED1D480GA      | 480 GB | FE441AF32048 | 953   | 0     | 2.61   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | C648507D8B22 | 951   | 0     | 2.61   |
| Intel     | SSDPEDKX040T7      | 4 TB   | E86307D80950 | 949   | 0     | 2.60   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | CD80E0329333 | 947   | 0     | 2.59   |
| Intel     | SSDPEDMD800G4      | 800 GB | 5C8C5715014A | 946   | 0     | 2.59   |
| Intel     | SSDPEDMD800G4      | 800 GB | 615AF43ECEAE | 946   | 0     | 2.59   |
| Intel     | SSDPEDMD800G4      | 800 GB | BE109AA3E75A | 946   | 0     | 2.59   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | 745CCB41533D | 946   | 0     | 2.59   |
| Intel     | SSDPE2MX450G7      | 450 GB | 1937622CA946 | 945   | 0     | 2.59   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | E89FE98F0CEF | 945   | 0     | 2.59   |
| Intel     | SSDPEDMD800G4      | 800 GB | D5C14AEFBBC1 | 943   | 0     | 2.58   |
| Intel     | SSDPEDMD800G4      | 800 GB | 0AD73A544D17 | 943   | 0     | 2.58   |
| Intel     | SSDPEDMD800G4      | 800 GB | 388ADE2152A4 | 943   | 0     | 2.58   |
| Intel     | SSDPEDMD800G4      | 800 GB | 38B3E2882A99 | 943   | 0     | 2.58   |
| Intel     | SSDPED1K750GA      | 752 GB | 68EDC490D3E6 | 939   | 0     | 2.57   |
| Toshiba   | THNSN5512GPU7      | 512 GB | 99401C565AEC | 934   | 0     | 2.56   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | 415E64835DD0 | 934   | 0     | 2.56   |
| Toshiba   | KXG50ZNV512G       | 512 GB | CB9DDB0BB233 | 934   | 0     | 2.56   |
| Toshiba   | THNSN5512GPU7      | 512 GB | 02D34E6ACB7F | 932   | 0     | 2.56   |
| Intel     | SSDPEDMD800G4      | 800 GB | 842565E27D56 | 928   | 0     | 2.54   |
| Samsung   | SSD 970 PRO        | 512 GB | 5BE85ADDDFDB | 926   | 0     | 2.54   |
| Intel     | SSDPE2KX040T8      | 4 TB   | 55E96F4EE2BC | 925   | 0     | 2.54   |
| Intel     | SSDPEDMX020T7      | 2 TB   | 3D05566CC678 | 923   | 0     | 2.53   |
| Intel     | SSDPEDMD400G4      | 400 GB | 95B95734EBBE | 923   | 0     | 2.53   |
| Samsung   | SSD 960 PRO        | 2 TB   | 25B103451B64 | 921   | 0     | 2.52   |
| Toshiba   | KXG50ZNV512G       | 512 GB | 0429AE8061CF | 920   | 0     | 2.52   |
| Dell      | Express Flash N... | 1.6 TB | 45C85ABC1A3D | 920   | 0     | 2.52   |
| Intel     | SSDPED1K375GA      | 375 GB | EBAF8A253583 | 920   | 0     | 2.52   |
| Intel     | SSDPEKKA256G7      | 256 GB | 0F58385AE900 | 919   | 0     | 2.52   |
| Intel     | SSDPEDMD800G4      | 800 GB | AB73F5E2E068 | 918   | 0     | 2.52   |
| Samsung   | SSD 950 PRO        | 512 GB | 81DC796C63A0 | 917   | 0     | 2.51   |
| Samsung   | MZWLL800HEHP-00003 | 800 GB | 8122CF9E72E2 | 916   | 0     | 2.51   |
| Intel     | SSDPEDKE020T7      | 2 TB   | E9FAE1E57B35 | 914   | 0     | 2.51   |
| Intel     | SSDPE2MX450G7      | 450 GB | A5AF1ED63684 | 914   | 0     | 2.51   |
| Intel     | SSDPEDMD800G4      | 800 GB | EDFF0E4E8CA6 | 913   | 0     | 2.50   |
| Intel     | SSDPEDKE020T7      | 2 TB   | E601710775E6 | 913   | 0     | 2.50   |
| Intel     | SSDPEDKX040T7      | 4 TB   | 9C8753F6B9C6 | 912   | 0     | 2.50   |
| Intel     | SSDPEDMD800G4      | 800 GB | 52654F3D9C78 | 912   | 0     | 2.50   |
| Intel     | SSDPEDMD800G4      | 800 GB | 267BAB458253 | 910   | 0     | 2.49   |
| Intel     | SSDPEDMD800G4      | 800 GB | 064C6C5B230E | 910   | 0     | 2.49   |
| Intel     | SSDPEDMD800G4      | 800 GB | 76F8FA81ED7E | 908   | 0     | 2.49   |
| Intel     | SSDPEDMD800G4      | 800 GB | 76661A2F66F5 | 908   | 0     | 2.49   |
| Samsung   | SSD 960 PRO        | 1 TB   | 572547BE913D | 906   | 0     | 2.48   |
| Intel     | SSDPEDKX040T7      | 4 TB   | 077495D67D0C | 906   | 0     | 2.48   |
| Intel     | SSDPEDKX040T7      | 4 TB   | 546922B4A1F1 | 904   | 0     | 2.48   |
| Intel     | SSDPEDMD400G4      | 400 GB | A06356A167F0 | 904   | 0     | 2.48   |
| Intel     | SSDPEDKX040T7      | 4 TB   | DE6FFED9C4DE | 903   | 0     | 2.47   |
| Intel     | SSDPEDKX040T7      | 4 TB   | 0C16E8A45953 | 902   | 0     | 2.47   |
| Intel     | SSDPEDMD800G4      | 800 GB | 1FB0F7CFA1A7 | 902   | 0     | 2.47   |
| Intel     | SSDPEDKX040T7      | 4 TB   | C56F7F8C6AAD | 902   | 0     | 2.47   |
| Intel     | SSDPEDKE020T7      | 2 TB   | D9E1418BE04E | 901   | 0     | 2.47   |
| Toshiba   | THNSN5512GPU7      | 512 GB | 8F9376E1AD51 | 901   | 0     | 2.47   |
| Intel     | SSDPEDMD800G4      | 800 GB | 341A530CC265 | 900   | 0     | 2.47   |
| Intel     | SSDPE2MX450G7      | 450 GB | 5970BD275548 | 900   | 0     | 2.47   |
| Intel     | SSDPE2KX040T8      | 4 TB   | 17DCE181182F | 899   | 0     | 2.46   |
| Intel     | SSDPEDMD400G4      | 400 GB | FA1E1E1A2820 | 899   | 0     | 2.46   |
| Intel     | SSDPEDMD400G4      | 400 GB | FF8FFF0B0137 | 899   | 0     | 2.46   |
| Intel     | SSDPE2KX040T8      | 4 TB   | 5B0FD7CD73FB | 899   | 0     | 2.46   |
| Intel     | SSDPE2KX040T8      | 4 TB   | 79381C6D78FE | 898   | 0     | 2.46   |
| Intel     | SSDPEDMD400G4      | 400 GB | 5BC518D4A471 | 898   | 0     | 2.46   |
| Intel     | SSDPEDMD800G4      | 800 GB | 5CE131319409 | 898   | 0     | 2.46   |
| Intel     | SSDPEDMD400G4      | 400 GB | 58B852BC971B | 898   | 0     | 2.46   |
| Intel     | SSDPEDMD800G4      | 800 GB | AE128FE03AF0 | 897   | 0     | 2.46   |
| Intel     | SSDPEDMD800G4      | 800 GB | C9C832AC4FD3 | 897   | 0     | 2.46   |
| Intel     | SSDPEDMD020T4      | 2 TB   | 0F69AA65FCF5 | 897   | 0     | 2.46   |
| Samsung   | MZPLL6T4HMLA-00005 | 6.4 TB | 4C9F6B85135E | 896   | 0     | 2.46   |
| Intel     | SSDPE2MX450G7      | 450 GB | 71DA3095B6B2 | 896   | 0     | 2.46   |
| Intel     | SSDPE2MX450G7      | 450 GB | B77C0ED36780 | 896   | 0     | 2.46   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | 297D0E0FA206 | 894   | 0     | 2.45   |
| Intel     | SSDPEDKE020T7      | 2 TB   | 6DF6837D253D | 894   | 0     | 2.45   |
| Intel     | SSDPEDKE020T7      | 2 TB   | E9F96CDAE52E | 894   | 0     | 2.45   |
| Intel     | SSDPEDMD800G4      | 800 GB | 88E911B7BF0F | 894   | 0     | 2.45   |
| Samsung   | SSD 970 PRO        | 512 GB | 68F045B9DE85 | 893   | 0     | 2.45   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | F97BEEC5C85F | 888   | 0     | 2.43   |
| Intel     | SSDPEDMD016T4K     | 1.6 TB | 6CB96237D7BF | 888   | 0     | 2.43   |
| Toshiba   | THNSN5512GPU7      | 512 GB | 26DD5A08DF85 | 886   | 0     | 2.43   |
| Intel     | SSDPEDMD800G4      | 800 GB | 71594007168C | 885   | 0     | 2.42   |
| Intel     | SSDPE2MX450G7      | 450 GB | F95F71957832 | 883   | 0     | 2.42   |
| Intel     | SSDPEDKX040T7      | 4 TB   | 6485F7E80EAA | 882   | 0     | 2.42   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | FCC1EFA3D837 | 882   | 0     | 2.42   |
| Intel     | SSDPED1K375GA      | 375 GB | 4F6BC434ED48 | 881   | 0     | 2.42   |
| Intel     | SSDPEDMD400G4      | 400 GB | 0258F8E45C95 | 879   | 0     | 2.41   |
| Intel     | SSDPEDMD800G4      | 800 GB | 2A981CC9C680 | 879   | 0     | 2.41   |
| Samsung   | MZ1LB960HAJQ-00007 | 960 GB | A69298C66E60 | 878   | 0     | 2.41   |
| Samsung   | MZPLL3T2HMLS-00003 | 3.2 TB | BE33A36D4113 | 875   | 0     | 2.40   |
| Intel     | SSDPEDMD400G4      | 400 GB | 6DECB98A3CD4 | 875   | 0     | 2.40   |
| Intel     | SSDPEDMD400G4      | 400 GB | 0E5410DCD8EA | 875   | 0     | 2.40   |
| Dell      | Express Flash P... | 1.6 TB | E3E0F4A96A04 | 874   | 0     | 2.40   |
| Intel     | MT0800KEXUU        | 800 GB | 6244EB38D564 | 874   | 0     | 2.39   |
| Intel     | SSDPEDMD400G4      | 400 GB | A7DD480105E6 | 873   | 0     | 2.39   |
| Intel     | SSDPED1K375GA      | 375 GB | 4032D2ADE660 | 873   | 0     | 2.39   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | 51E0C6E256BA | 871   | 0     | 2.39   |
| Intel     | SSDPE2MX450G7      | 450 GB | 3E8EE0052D0E | 869   | 0     | 2.38   |
| Intel     | SSDPED1D480GA      | 480 GB | 9E38A7C5D901 | 869   | 0     | 2.38   |
| Intel     | SSDPE2MX450G7      | 450 GB | 88B09D7AFAC2 | 867   | 0     | 2.38   |
| Intel     | SSDPE2MX450G7      | 450 GB | E72FCD23A2EC | 867   | 0     | 2.38   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | 48A5E86C5A02 | 866   | 0     | 2.38   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | E8F8AB62B019 | 866   | 0     | 2.38   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | 162F351D0BEE | 866   | 0     | 2.37   |
| Samsung   | MZQLW960HMJP-00003 | 960 GB | F6840E2121C5 | 1733  | 1     | 2.37   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | 58EE37B411B2 | 866   | 0     | 2.37   |
| Intel     | MT0800KEXUU        | 800 GB | 20CC7A11EC37 | 865   | 0     | 2.37   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | 194FAE8E3CE0 | 865   | 0     | 2.37   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | D5A40D690951 | 865   | 0     | 2.37   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | B0D031B2743E | 861   | 0     | 2.36   |
| Samsung   | SSD 960 PRO        | 1 TB   | A508637ED2CB | 860   | 0     | 2.36   |
| Intel     | SSDPEDMD400G4      | 400 GB | 47F9AAB3FC7E | 857   | 0     | 2.35   |
| Intel     | SSDPEDKX040T7      | 4 TB   | 98A90F464AFC | 856   | 0     | 2.35   |
| Intel     | SSDPEDKX040T7      | 4 TB   | AD6EE5F87891 | 855   | 0     | 2.34   |
| Intel     | SSDPE2KX040T8      | 4 TB   | F2D761B0A450 | 855   | 0     | 2.34   |
| Intel     | SSDPE2KX010T8      | 1 TB   | 4B839D9C0808 | 854   | 0     | 2.34   |
| Intel     | SSDPED1K375GA      | 375 GB | F1BEDC6BB750 | 854   | 0     | 2.34   |
| Intel     | MT0800KEXUU        | 800 GB | 90F22C61E5F9 | 850   | 0     | 2.33   |
| Intel     | SSDPE2MD400G4L     | 400 GB | EDF986ABC1CD | 850   | 0     | 2.33   |
| Intel     | SSDPE2KX040T8      | 4 TB   | 8FE9A738753F | 849   | 0     | 2.33   |
| Intel     | SSDPED1D480GA      | 480 GB | 14B2BF5FDC6C | 849   | 0     | 2.33   |
| Samsung   | MZWLL800HEHP-00003 | 800 GB | 9F711B58D361 | 849   | 0     | 2.33   |
| Intel     | SSDPED1K375GA      | 375 GB | 3776D2FE4D96 | 849   | 0     | 2.33   |
| Samsung   | MZPLL6T4HMLA-00005 | 6.4 TB | 060DA8462DCC | 848   | 0     | 2.32   |
| Toshiba   | THNSN5512GPU7      | 512 GB | 93B32EE0A00C | 848   | 0     | 2.32   |
| Samsung   | SSD 960 PRO        | 1 TB   | 6EB9F882455B | 848   | 0     | 2.32   |
| Samsung   | SSD 970 PRO        | 512 GB | 56A81E5DA35C | 847   | 0     | 2.32   |
| Intel     | SSDPE2KX010T8      | 1 TB   | FA8F42A555D0 | 847   | 0     | 2.32   |
| Intel     | SSDPE2MD400G4L     | 400 GB | E7802DCBE43C | 846   | 0     | 2.32   |
| Intel     | SSDPE2MD400G4L     | 400 GB | 4A2860A215F2 | 846   | 0     | 2.32   |
| Intel     | SSDPEDKX040T7      | 4 TB   | B0FD211C7BBA | 844   | 0     | 2.31   |
| Toshiba   | THNSN5512GPU7      | 512 GB | BA28B368E6A3 | 843   | 0     | 2.31   |
| Intel     | SSDPE2KX040T8      | 4 TB   | 89D248E3FA6E | 842   | 0     | 2.31   |
| Intel     | MT0800KEXUU        | 800 GB | FF7E63B28635 | 842   | 0     | 2.31   |
| Intel     | SSDPE2MD400G4      | 400 GB | 11DFAF1BF6B2 | 841   | 0     | 2.31   |
| Toshiba   | KXG50ZNV256G       | 256 GB | 62CDF713C1BF | 841   | 0     | 2.31   |
| Intel     | SSDPED1K375GA      | 375 GB | 2F95672E52E3 | 840   | 0     | 2.30   |
| Intel     | SSDPEDKX040T7      | 4 TB   | 8D6B31CC515B | 840   | 0     | 2.30   |
| Intel     | SSDPE2KX010T8      | 1 TB   | 80E01C03CEF2 | 840   | 0     | 2.30   |
| Intel     | SSDPED1K375GA      | 375 GB | 357C3C366FFC | 840   | 0     | 2.30   |
| Intel     | SSDPEDMX020T7      | 2 TB   | 846291AFC8CE | 840   | 0     | 2.30   |
| Intel     | SSDPED1K375GA      | 375 GB | BD0F105640DA | 839   | 0     | 2.30   |
| WDC       | CL SN720 SDAQNT... | 1 TB   | F49567863900 | 838   | 0     | 2.30   |
| Intel     | SSDPE2MX450G7      | 450 GB | B5FEC68EA2A9 | 838   | 0     | 2.30   |
| Intel     | SSDPE2KX080T8      | 8 TB   | 29D76792D78E | 836   | 0     | 2.29   |
| Intel     | SSDPED1K375GA      | 375 GB | E23897157A2F | 833   | 0     | 2.28   |
| Intel     | SSDPEDKX040T7      | 4 TB   | 1B7D207CAD01 | 833   | 0     | 2.28   |
| Intel     | SSDPE21D480GA      | 480 GB | E4D8A0160571 | 832   | 0     | 2.28   |
| Intel     | SSDPE21D480GA      | 480 GB | C62327AC9453 | 831   | 0     | 2.28   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 08B6FCF9031C | 830   | 0     | 2.28   |
| WDC       | CL SN720 SDAQNT... | 1 TB   | 17F86DEAF8AC | 829   | 0     | 2.27   |
| Intel     | MT0800KEXUU        | 800 GB | 483FA37ECE93 | 829   | 0     | 2.27   |
| Samsung   | SSD 970 PRO        | 512 GB | 3BF585A2E1FE | 829   | 0     | 2.27   |
| Intel     | SSDPEDMD400G4      | 400 GB | 74154003E97E | 828   | 0     | 2.27   |
| Intel     | SSDPE2KE016T8      | 1.6 TB | B1ABD2E6883A | 828   | 0     | 2.27   |
| Intel     | SSDPE2MX450G7      | 450 GB | 1C2BC7C7E4DD | 828   | 0     | 2.27   |
| Intel     | SSDPEDKX040T7      | 4 TB   | E8BEA05CE56B | 828   | 0     | 2.27   |
| Samsung   | SSD 960 PRO        | 1 TB   | 2032A92CD784 | 827   | 0     | 2.27   |
| Samsung   | SSD 970 PRO        | 512 GB | CB46452E3FF3 | 827   | 0     | 2.27   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | BE0556D58D6F | 827   | 0     | 2.27   |
| Intel     | SSDPE2KX010T8      | 1 TB   | FA133D46140B | 825   | 0     | 2.26   |
| Samsung   | SSD 970 PRO        | 512 GB | 3126135F9D30 | 825   | 0     | 2.26   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | 89DE585ACCF5 | 824   | 0     | 2.26   |
| Intel     | SSDPE2MX450G7      | 450 GB | D6A9BCAB088A | 824   | 0     | 2.26   |
| WDC       | PC SN720 SDAPNT... | 1 TB   | FEDA00F7319B | 824   | 0     | 2.26   |
| Intel     | SSDPE2KX040T8      | 4 TB   | FA95DBE43307 | 823   | 0     | 2.26   |
| WDC       | CL SN720 SDAQNT... | 512 GB | 0ADFE456AF58 | 823   | 0     | 2.26   |
| Intel     | MT0800KEXUU        | 800 GB | BD057315ED48 | 823   | 0     | 2.26   |
| Intel     | SSDPEDKX040T7      | 4 TB   | 32665FA0C8D4 | 822   | 0     | 2.25   |
| WDC       | CL SN720 SDAQNT... | 512 GB | 60C530EE525F | 822   | 0     | 2.25   |
| Samsung   | MZPLL3T2HMLS-00003 | 3.2 TB | D59F506149B5 | 821   | 0     | 2.25   |
| Intel     | SSDPEDKX040T7      | 4 TB   | 03992A38F7F8 | 821   | 0     | 2.25   |
| Samsung   | SSD 960 PRO        | 1 TB   | 6B660BF7493A | 818   | 0     | 2.24   |
| Kingston  | SKC1000960G        | 960 GB | D6CC00EE2894 | 816   | 0     | 2.24   |
| Dell      | Express Flash N... | 1.6 TB | D80A63E77EE6 | 815   | 0     | 2.23   |
| Intel     | SSDPEDKX040T7      | 4 TB   | 5E2ABA1A3814 | 814   | 0     | 2.23   |
| Intel     | SSDPED1K375GA      | 375 GB | EA0D87234140 | 814   | 0     | 2.23   |
| Intel     | SSDPEKKW256G7      | 256 GB | 5B6624F91B82 | 811   | 0     | 2.22   |
| Intel     | SSDPED1K375GA      | 375 GB | B514430A9AF3 | 811   | 0     | 2.22   |
| Intel     | SSDPE21D480GA      | 480 GB | 59F275594063 | 810   | 0     | 2.22   |
| Intel     | SSDPE21D480GA      | 480 GB | B064605268EE | 810   | 0     | 2.22   |
| Samsung   | MZQLW1T9HMJP-00003 | 1.9 TB | 5A858D9A355F | 810   | 0     | 2.22   |
| Intel     | SSDPED1D480GA      | 480 GB | F82801EDC9BD | 810   | 0     | 2.22   |
| Intel     | SSDPED1K750GA      | 752 GB | 691766BF8FF9 | 810   | 0     | 2.22   |
| Intel     | SSDPE2MX450G7      | 450 GB | 3A7077D06845 | 808   | 0     | 2.21   |
| Samsung   | SSD 960 EVO        | 500 GB | D6C629D325EA | 807   | 0     | 2.21   |
| Toshiba   | KXG50ZNV512G       | 512 GB | 735F79D0401E | 807   | 0     | 2.21   |
| Intel     | SSDPE2MD400G4      | 400 GB | 79A172BCB33C | 806   | 0     | 2.21   |
| Dell      | Express Flash N... | 1.6 TB | 4307085C8DA7 | 805   | 0     | 2.21   |
| Samsung   | SSD 960 PRO        | 1 TB   | 6357C609C2AA | 805   | 0     | 2.21   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | 174E2E35A244 | 804   | 0     | 2.21   |
| Intel     | SSDPEDKX040T7      | 4 TB   | C1BD43A0C9A8 | 804   | 0     | 2.20   |
| Intel     | SSDPED1K750GA      | 752 GB | 205B56D0958E | 804   | 0     | 2.20   |
| Samsung   | MZWLL800HEHP-00003 | 800 GB | 27F926C35E1D | 801   | 0     | 2.20   |
| Intel     | SSDPE2KX040T8      | 4 TB   | 0ABA9A1968AF | 801   | 0     | 2.20   |
| Intel     | SSDPE2KX040T8      | 4 TB   | A41113DB58E6 | 801   | 0     | 2.20   |
| Intel     | SSDPE2KE016T8      | 1.6 TB | D66893FF9915 | 801   | 0     | 2.20   |
| Intel     | SSDPE2KX040T8      | 4 TB   | 13A84DFE61F8 | 801   | 0     | 2.20   |
| Intel     | SSDPEDKX040T7      | 4 TB   | 5B17970EFF0B | 801   | 0     | 2.20   |
| Samsung   | MZPLL6T4HMLS-00003 | 6.4 TB | C4035163C95F | 801   | 0     | 2.20   |
| Samsung   | SSD 970 PRO        | 1 TB   | 226B5D5DAD81 | 800   | 0     | 2.19   |
| Samsung   | SSD 970 PRO        | 1 TB   | 9BEBFBF9400D | 800   | 0     | 2.19   |
| Intel     | SSDPEDKE020T7      | 2 TB   | E67BED7FEA7A | 799   | 0     | 2.19   |
| Intel     | VO0400KEFJB        | 400 GB | D858C324536E | 799   | 0     | 2.19   |
| Intel     | MT0800KEXUU        | 800 GB | 456C739CB496 | 797   | 0     | 2.19   |
| Intel     | SSDPEDMD400G4      | 400 GB | D9B844A5AD4F | 797   | 0     | 2.18   |
| Intel     | SSDPEDMX020T7      | 2 TB   | FA9338FD133C | 797   | 0     | 2.18   |
| Intel     | SSDPE2KE016T8      | 1.6 TB | 9545A075AD86 | 796   | 0     | 2.18   |
| Intel     | SSDPEDMD400G4      | 400 GB | E99A72CA1104 | 792   | 0     | 2.17   |
| Intel     | SSDPEDKX040T7      | 4 TB   | DFFCC265605F | 792   | 0     | 2.17   |
| Intel     | SSDPEDMD400G4      | 400 GB | 0216F1429E1E | 792   | 0     | 2.17   |
| Samsung   | MZPLL6T4HMLS-00003 | 6.4 TB | A8CF205BA041 | 791   | 0     | 2.17   |
| Intel     | SSDPE2KX040T8      | 4 TB   | C8C83D1E012E | 791   | 0     | 2.17   |
| Intel     | SSDPE2KX080T8      | 8 TB   | 4854F1274D5E | 790   | 0     | 2.17   |
| SPCC      | M.2 PCIe SSD       | 1 TB   | EE52F5D9AAF9 | 789   | 0     | 2.16   |
| Dell      | Express Flash N... | 1.6 TB | 0B72EC83E113 | 788   | 0     | 2.16   |
| Intel     | SSDPED1K750GA      | 752 GB | 55260956991D | 788   | 0     | 2.16   |
| Samsung   | MZPLL6T4HMLA-00005 | 6.4 TB | FB957B19D2A7 | 787   | 0     | 2.16   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | 17BB9A2CF72F | 787   | 0     | 2.16   |
| Samsung   | MZPLL3T2HMLS-00003 | 3.2 TB | F696D29DE386 | 787   | 0     | 2.16   |
| Intel     | VO0400KEFJB        | 400 GB | 830E8E29F92F | 787   | 0     | 2.16   |
| Intel     | SSDPE2KX040T8      | 4 TB   | D269E035A589 | 785   | 0     | 2.15   |
| Intel     | SSDPE2KE076T8      | 7.6 TB | 5871AEBE31BC | 784   | 0     | 2.15   |
| Intel     | SSDPE2KX040T8      | 4 TB   | 04F1CA4309E2 | 784   | 0     | 2.15   |
| Intel     | SSDPE2KX040T8      | 4 TB   | 5DFEE0D6B3D4 | 784   | 0     | 2.15   |
| Intel     | SSDPED1K750GA      | 752 GB | F2B0CC79E862 | 783   | 0     | 2.15   |
| Intel     | SSDPEDMD400G4      | 400 GB | BE2DB2235FDF | 782   | 0     | 2.14   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 3FCDF4A6B579 | 782   | 0     | 2.14   |
| Intel     | SSDPE2KX040T8      | 4 TB   | 29DB1F21AD26 | 782   | 0     | 2.14   |
| Intel     | SSDPE2KX040T8      | 4 TB   | 65AA6AE6D3C2 | 782   | 0     | 2.14   |
| Intel     | SSDPE2KX040T8      | 4 TB   | 0297801AB4C6 | 778   | 0     | 2.13   |
| Intel     | SSDPE2KX040T8      | 4 TB   | AC8E1EE20732 | 778   | 0     | 2.13   |
| Intel     | SSDPEKKW256G7      | 256 GB | 630CD019F2AB | 778   | 0     | 2.13   |
| Samsung   | SSD 960 PRO        | 1 TB   | 36133BCC8909 | 777   | 0     | 2.13   |
| Intel     | SSDPED1D480GA      | 480 GB | F9BA23AA1E87 | 775   | 0     | 2.13   |
| Intel     | SSDPEDMD400G4      | 400 GB | 0D5009BD2CE6 | 775   | 0     | 2.13   |
| Intel     | SSDPED1D480GA      | 480 GB | 1F9CF99A8362 | 775   | 0     | 2.12   |
| Intel     | SSDPEDKX040T7      | 4 TB   | 3CBFD96CBAEC | 773   | 0     | 2.12   |
| Intel     | SSDPED1K750GA      | 752 GB | 28BDFC809F73 | 773   | 0     | 2.12   |
| Intel     | SSDPED1K750GA      | 752 GB | 5ABEB8E63563 | 772   | 0     | 2.12   |
| Samsung   | MZPLL6T4HMLA-00005 | 6.4 TB | 7CC94528B2FA | 772   | 0     | 2.12   |
| Intel     | SSDPEDKX040T7      | 4 TB   | 5C4A9BF0CD4F | 771   | 0     | 2.11   |
| Intel     | SSDPE2KE076T8      | 7.6 TB | 8BB5C3BD29EC | 771   | 0     | 2.11   |
| Intel     | SSDPEDKX040T7      | 4 TB   | 49DCD1AA5C34 | 770   | 0     | 2.11   |
| Dell      | Express Flash N... | 1.6 TB | 2DB251BE5234 | 769   | 0     | 2.11   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | CDFF0DA1732F | 767   | 0     | 2.10   |
| Huawei    | HWE36P43016M000N   | 1.6 TB | AB97B65146C7 | 767   | 0     | 2.10   |
| Dell      | Express Flash N... | 1.6 TB | 23F4D03712FB | 766   | 0     | 2.10   |
| Intel     | SSDPEDMD400G4      | 400 GB | F3E893777CA2 | 766   | 0     | 2.10   |
| Intel     | SSDPEDMD400G4      | 400 GB | 8A91DF9B4FF9 | 766   | 0     | 2.10   |
| Intel     | SSDPED1D280GA      | 280 GB | 876D2C84863D | 766   | 0     | 2.10   |
| Intel     | SSDPEKKW256G7      | 256 GB | 4809E40A509C | 765   | 0     | 2.10   |
| Intel     | SSDPEDMD400G4      | 400 GB | 6DDB05ED2F11 | 764   | 0     | 2.10   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | 936E25D1AA34 | 764   | 0     | 2.09   |
| Intel     | SSDPE2KX080T8      | 8 TB   | 40B9440AD76C | 764   | 0     | 2.09   |
| Intel     | SSDPED1D480GA      | 480 GB | 269BD9253A42 | 764   | 0     | 2.09   |
| Intel     | SSDPED1D480GA      | 480 GB | 14C1070BF806 | 763   | 0     | 2.09   |
| Intel     | SSDPEDMD400G4      | 400 GB | 4571E71B772E | 763   | 0     | 2.09   |
| Intel     | SSDPE2KX040T8      | 4 TB   | DD2FD8651429 | 762   | 0     | 2.09   |
| Dell      | Express Flash N... | 1.6 TB | 67512E538B39 | 761   | 0     | 2.09   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | 564723F221FB | 759   | 0     | 2.08   |
| Intel     | SSDPED1K750GA      | 752 GB | FAAB00A325D1 | 759   | 0     | 2.08   |
| Intel     | SSDPEDMD400G4      | 400 GB | BB99C1E66C92 | 759   | 0     | 2.08   |
| Dell      | Express Flash N... | 1.6 TB | 01399FFCF105 | 759   | 0     | 2.08   |
| Intel     | SSDPED1D280GA      | 280 GB | 9445ACA62220 | 759   | 0     | 2.08   |
| Intel     | SSDPED1D280GA      | 280 GB | C3ADC5535979 | 759   | 0     | 2.08   |
| Intel     | SSDPEDKE020T7      | 2 TB   | 3E4F919DBAD3 | 758   | 0     | 2.08   |
| Intel     | SSDPEDKE020T7      | 2 TB   | 1E72C2349FA4 | 758   | 0     | 2.08   |
| WDC       | CL SN720 SDAQNT... | 512 GB | 1EB48E703972 | 758   | 0     | 2.08   |
| Toshiba   | KXG50ZNV1T02       | 1 TB   | C60A97AA0ADD | 757   | 0     | 2.07   |
| Intel     | SSDPE2KX040T8      | 4 TB   | 07385D5F914F | 755   | 0     | 2.07   |
| Intel     | SSDPE2KX040T8      | 4 TB   | CD5C2DFA64A5 | 755   | 0     | 2.07   |
| Intel     | SSDPED1K750GA      | 752 GB | BF2CF219241D | 754   | 0     | 2.07   |
| Toshiba   | KXG50ZNV1T02       | 1 TB   | BB524EA5E7A7 | 754   | 0     | 2.07   |
| Toshiba   | KXG50ZNV1T02       | 1 TB   | 450006878CD9 | 754   | 0     | 2.07   |
| Intel     | SSDPEDKE020T7      | 2 TB   | 7DE4D9497E83 | 754   | 0     | 2.07   |
| Intel     | SSDPE2KX080T8      | 8 TB   | B1F5AD77A1AD | 753   | 0     | 2.06   |
| Intel     | SSDPEDKE020T7      | 2 TB   | 360150099788 | 753   | 0     | 2.06   |
| Intel     | SSDPEDKE020T7      | 2 TB   | 8A6E79B0B4B2 | 753   | 0     | 2.06   |
| Intel     | SSDPE2KX010T8      | 1 TB   | 1979197F436A | 752   | 0     | 2.06   |
| Dell      | Express Flash N... | 1.6 TB | AD7FD94D85E8 | 751   | 0     | 2.06   |
| Intel     | SSDPEDMD400G4      | 400 GB | 876244ACB272 | 751   | 0     | 2.06   |
| Intel     | SSDPEDMD400G4      | 400 GB | AB4571C96298 | 751   | 0     | 2.06   |
| Intel     | MT0800KEXUU        | 800 GB | DE808107140F | 751   | 0     | 2.06   |
| Dell      | Express Flash N... | 1.6 TB | E385C8A7D387 | 750   | 0     | 2.06   |
| Intel     | MT0800KEXUU        | 800 GB | 2CED18A86A44 | 750   | 0     | 2.05   |
| Samsung   | MZVLB512HAJQ-00000 | 512 GB | F5C7196D1AD6 | 747   | 0     | 2.05   |
| Intel     | SSDPE2KX010T8      | 1 TB   | F469F60FC62C | 747   | 0     | 2.05   |
| Samsung   | SSD 970 EVO Plus   | 1 TB   | 8442633E41BC | 746   | 0     | 2.04   |
| Intel     | SSDPE2KX080T8      | 8 TB   | D30291B2A05A | 743   | 0     | 2.04   |
| Dell      | Express Flash N... | 1.6 TB | 462492ABAFBE | 743   | 0     | 2.04   |
| Intel     | SSDPE2KX040T8      | 4 TB   | 0767D4D6F65E | 741   | 0     | 2.03   |
| Kingston  | SKC1000960G        | 960 GB | 0D6D2BB12EA2 | 741   | 0     | 2.03   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 2894DB4F5A40 | 741   | 0     | 2.03   |
| Intel     | SSDPE2KX010T8      | 1 TB   | 6604D7229881 | 739   | 0     | 2.03   |
| Intel     | SSDPE2KX010T8      | 1 TB   | 4E1A8376A7C0 | 739   | 0     | 2.03   |
| Intel     | SSDPE2KX010T8      | 1 TB   | AB59958E3AEA | 739   | 0     | 2.03   |
| Intel     | SSDPE21D480GA      | 480 GB | D6A398048D11 | 738   | 0     | 2.02   |
| Intel     | VO0400KEFJB        | 400 GB | B5D37D5645EB | 738   | 0     | 2.02   |
| Intel     | SSDPEDKX040T7      | 4 TB   | 79F363F9078C | 736   | 0     | 2.02   |
| WDC       | CL SN720 SDAQNT... | 512 GB | 0FED026FB154 | 735   | 0     | 2.01   |
| Intel     | SSDPEKKW256G7      | 256 GB | 19DB01ADBB49 | 735   | 0     | 2.01   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 233288317B45 | 734   | 0     | 2.01   |
| Intel     | SSDPEDKX040T7      | 4 TB   | 51EA51429C62 | 733   | 0     | 2.01   |
| Intel     | SSDPED1D280GA      | 280 GB | 4B37B1593B77 | 733   | 0     | 2.01   |
| WDC       | CL SN720 SDAQNT... | 512 GB | 9523BCF69C37 | 733   | 0     | 2.01   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | B6C0A694707B | 732   | 0     | 2.01   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 5BEFD7485D24 | 732   | 0     | 2.01   |
| Intel     | SSDPE2KX040T8      | 4 TB   | F15C9FAE99C8 | 729   | 0     | 2.00   |
| Intel     | SSDPEKKW512G7      | 512 GB | D458C72B1C76 | 728   | 0     | 2.00   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 87AFAFC26665 | 727   | 0     | 1.99   |
| Intel     | SSDPE2KE016T8      | 1.6 TB | 6BF02E7353A6 | 727   | 0     | 1.99   |
| Dell      | Express Flash N... | 1.6 TB | 56F9DF816CDC | 727   | 0     | 1.99   |
| Intel     | SSDPE2KX010T8      | 1 TB   | 89645D718503 | 726   | 0     | 1.99   |
| Intel     | SSDPED1K375GA      | 375 GB | 8EF7AB0FC2E4 | 726   | 0     | 1.99   |
| Intel     | SSDPE2KX040T8      | 4 TB   | 2DBB7717965C | 725   | 0     | 1.99   |
| Intel     | SSDPED1K375GA      | 375 GB | C395820AE4C2 | 725   | 0     | 1.99   |
| Intel     | SSDPE2KX010T8      | 1 TB   | 7A79963FF75D | 725   | 0     | 1.99   |
| Samsung   | MZWLL800HEHP-00003 | 800 GB | 265D0D26C474 | 725   | 0     | 1.99   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 695930F26F04 | 724   | 0     | 1.98   |
| Intel     | SSDPE21D480GA      | 480 GB | 4EB6C199E394 | 724   | 0     | 1.98   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 01E7AFBB6C9F | 723   | 0     | 1.98   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | C372F7AB7A58 | 723   | 0     | 1.98   |
| Samsung   | MZWLL800HEHP-00003 | 800 GB | 059F765E2755 | 723   | 0     | 1.98   |
| Intel     | MT0800KEXUU        | 800 GB | 3851881E9ED9 | 721   | 0     | 1.98   |
| Intel     | SSDPED1K375GA      | 375 GB | 1364AA79AFCA | 720   | 0     | 1.97   |
| Intel     | SSDPE21D480GA      | 480 GB | D74FA494FBBC | 719   | 0     | 1.97   |
| WDC       | CL SN720 SDAQNT... | 1 TB   | 576D7C0C0998 | 718   | 0     | 1.97   |
| Intel     | SSDPE2ME012T4      | 1.2 TB | CCA93768E6D8 | 717   | 0     | 1.97   |
| Intel     | SSDPED1K375GA      | 375 GB | D2AD2DAB8996 | 716   | 0     | 1.96   |
| Huawei    | HWE36P43016M000N   | 1.6 TB | 0EB462305F3D | 714   | 0     | 1.96   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 8B316DDF55B1 | 714   | 0     | 1.96   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | FBEC6FC297C8 | 714   | 0     | 1.96   |
| Intel     | SSDPE2KX080T8      | 8 TB   | 58E6422879CD | 713   | 0     | 1.95   |
| Intel     | SSDPED1D480GA      | 480 GB | C7D83753AF1A | 713   | 0     | 1.95   |
| Intel     | SSDPE21D480GA      | 480 GB | 7E384FB5B774 | 710   | 0     | 1.95   |
| Intel     | SSDPE21D480GA      | 480 GB | D4E251A17A38 | 710   | 0     | 1.95   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 0BA0AE39B4EA | 709   | 0     | 1.94   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 9EFFD7ED1B18 | 709   | 0     | 1.94   |
| Intel     | SSDPE2KX040T8      | 4 TB   | B2C80422504F | 709   | 0     | 1.94   |
| Intel     | SSDPEKKW256G7      | 256 GB | 70DBB7FE0F53 | 708   | 0     | 1.94   |
| Samsung   | SSD 970 EVO Plus   | 1 TB   | A6098559EB6D | 708   | 0     | 1.94   |
| Intel     | SSDPED1D480GA      | 480 GB | 12E5CA3F4094 | 708   | 0     | 1.94   |
| Intel     | SSDPED1D480GA      | 480 GB | B47599102DF9 | 708   | 0     | 1.94   |
| Intel     | SSDPED1D480GA      | 480 GB | CD8DA54C8603 | 708   | 0     | 1.94   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 7E739BC16A19 | 707   | 0     | 1.94   |
| Intel     | SSDPE21D480GA      | 480 GB | 6D6560615FA7 | 707   | 0     | 1.94   |
| Intel     | SSDPEKKW256G7      | 256 GB | 4839A94BB36E | 705   | 0     | 1.93   |
| Intel     | SSDPED1D480GA      | 480 GB | 8C38331A3783 | 705   | 0     | 1.93   |
| Intel     | SSDPED1D480GA      | 480 GB | F3DEF7B7E635 | 705   | 0     | 1.93   |
| Intel     | SSDPED1D480GA      | 480 GB | 0EDF944508DC | 705   | 0     | 1.93   |
| Intel     | SSDPED1D480GA      | 480 GB | 1DE06FA74A89 | 705   | 0     | 1.93   |
| Intel     | SSDPED1K375GA      | 375 GB | E9900445814E | 705   | 0     | 1.93   |
| Intel     | SSDPE2KX010T8      | 1 TB   | DC997AFC6C04 | 705   | 0     | 1.93   |
| Intel     | MT0800KEXUU        | 800 GB | 2605DC7371A1 | 704   | 0     | 1.93   |
| Intel     | SSDPE2KX040T8      | 4 TB   | 68A71518C093 | 703   | 0     | 1.93   |
| Intel     | MT0800KEXUU        | 800 GB | 84B963285E6B | 703   | 0     | 1.93   |
| Samsung   | MZPLL6T4HMLS-00003 | 6.4 TB | 6189506EA155 | 703   | 0     | 1.93   |
| Intel     | SSDPED1K750GA      | 752 GB | E7726053F087 | 702   | 0     | 1.93   |
| Intel     | SSDPE21D480GA      | 480 GB | DD7AF21CEACB | 701   | 0     | 1.92   |
| WDC       | CL SN720 SDAQNT... | 512 GB | DE7D9BAF5DB7 | 701   | 0     | 1.92   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 57A13774161D | 701   | 0     | 1.92   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 449D03302C77 | 699   | 0     | 1.92   |
| Intel     | SSDPEDKX040T7      | 4 TB   | 57C80C774E8D | 699   | 0     | 1.92   |
| Intel     | SSDPE21D480GA      | 480 GB | 9D63FF5BD88B | 698   | 0     | 1.91   |
| Samsung   | MZQLB3T8HALS-000AZ | 3.8 TB | 6F828F2ACB43 | 698   | 0     | 1.91   |
| Samsung   | MZQLB3T8HALS-000AZ | 3.8 TB | A49EFB015771 | 698   | 0     | 1.91   |
| WDC       | CL SN720 SDAQNT... | 512 GB | 433D9959D74D | 697   | 0     | 1.91   |
| Intel     | SSDPED1K750GA      | 752 GB | 0F13040998D7 | 697   | 0     | 1.91   |
| Samsung   | SSD 960 EVO        | 250 GB | D680BA0E41C7 | 697   | 0     | 1.91   |
| Intel     | SSDPEDMD400G4      | 400 GB | 21F31322964C | 697   | 0     | 1.91   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | D36FD88FC2B7 | 697   | 0     | 1.91   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | E5CB0BA344A0 | 697   | 0     | 1.91   |
| Intel     | SSDPE2ME012T4      | 1.2 TB | 941E29EA5C85 | 696   | 0     | 1.91   |
| WDC       | CL SN720 SDAQNT... | 512 GB | 9F6302D5670C | 696   | 0     | 1.91   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | FD194BA89906 | 695   | 0     | 1.91   |
| WDC       | CL SN720 SDAQNT... | 1 TB   | 493725C3CF0E | 695   | 0     | 1.91   |
| Intel     | SSDPED1K750GA      | 752 GB | F57CE4375C70 | 695   | 0     | 1.90   |
| Intel     | SSDPED1K375GA      | 375 GB | CDE8B858AF77 | 694   | 0     | 1.90   |
| Samsung   | MZVLB512HAJQ-00000 | 512 GB | 877C8FFE453A | 693   | 0     | 1.90   |
| Intel     | SSDPED1K750GA      | 752 GB | 248D388FB428 | 692   | 0     | 1.90   |
| Samsung   | MZQLB3T8HALS-000AZ | 3.8 TB | 0ABDF78560B4 | 691   | 0     | 1.89   |
| Samsung   | MZQLB3T8HALS-000AZ | 3.8 TB | 676286C64B4B | 691   | 0     | 1.89   |
| Samsung   | MZQLB3T8HALS-000AZ | 3.8 TB | 32E5BD591ED2 | 691   | 0     | 1.89   |
| WDC       | CL SN720 SDAQNT... | 512 GB | B763E3E35161 | 691   | 0     | 1.89   |
| Intel     | SSDPED1D480GA      | 480 GB | 4883627C3980 | 690   | 0     | 1.89   |
| Samsung   | MZVLB1T0HALR-00000 | 1 TB   | CC42978411EF | 690   | 0     | 1.89   |
| Intel     | SSDPE21D480GA      | 480 GB | 1BAC2B118574 | 689   | 0     | 1.89   |
| Intel     | SSDPE21D480GA      | 480 GB | 31B5DBF64204 | 689   | 0     | 1.89   |
| Intel     | SSDPE21D480GA      | 480 GB | 4E00970419E9 | 689   | 0     | 1.89   |
| Intel     | SSDPE21D480GA      | 480 GB | 4EB1857C204D | 689   | 0     | 1.89   |
| Intel     | SSDPE21D480GA      | 480 GB | 5098096B6DF0 | 689   | 0     | 1.89   |
| Intel     | SSDPE21D480GA      | 480 GB | 760A53EC4212 | 689   | 0     | 1.89   |
| Intel     | SSDPE21D480GA      | 480 GB | 81AACA157BB2 | 689   | 0     | 1.89   |
| Intel     | SSDPE21D480GA      | 480 GB | EEB04F3CE254 | 689   | 0     | 1.89   |
| Intel     | SSDPE21D480GA      | 480 GB | FC84D644F8C6 | 689   | 0     | 1.89   |
| Intel     | SSDPED1D480GA      | 480 GB | AF23ADF46396 | 687   | 0     | 1.88   |
| Samsung   | MZPLL3T2HAJQ-00005 | 3.2 TB | 67FC4BA49683 | 686   | 0     | 1.88   |
| Intel     | SSDPED1K375GA      | 375 GB | 551B16828CFE | 686   | 0     | 1.88   |
| Intel     | SSDPE21D480GA      | 480 GB | 6EEE41638144 | 685   | 0     | 1.88   |
| Intel     | SSDPE21D480GA      | 480 GB | FD561D644DD5 | 685   | 0     | 1.88   |
| Samsung   | MZQLB3T8HALS-000AZ | 3.8 TB | 45439F8F3678 | 684   | 0     | 1.88   |
| Intel     | SSDPE21D480GA      | 480 GB | CE6C7B0B6CE2 | 681   | 0     | 1.87   |
| Intel     | SSDPEDKX040T7      | 4 TB   | 404E7A671E69 | 681   | 0     | 1.87   |
| Toshiba   | KXG50ZNV1T02       | 1 TB   | 160F528B662A | 680   | 0     | 1.86   |
| Samsung   | MZQLW960HMJP-00003 | 960 GB | 56DA382C04B9 | 1360  | 1     | 1.86   |
| Intel     | SSDPE21D480GA      | 480 GB | B12976AB4A46 | 679   | 0     | 1.86   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | FAF79546B7E0 | 679   | 0     | 1.86   |
| Intel     | SSDPED1K750GA      | 752 GB | D4DB731C0F8D | 679   | 0     | 1.86   |
| Toshiba   | KXG50ZNV256G       | 256 GB | B0645E82C45E | 678   | 0     | 1.86   |
| Intel     | SSDPE2KX040T8      | 4 TB   | C7B20AA746F6 | 678   | 0     | 1.86   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | C84FF6300D48 | 678   | 0     | 1.86   |
| Intel     | SSDPE2KX010T8      | 1 TB   | 6748DE091FD5 | 678   | 0     | 1.86   |
| Intel     | SSDPE2KX010T8      | 1 TB   | 7668DF5368D8 | 678   | 0     | 1.86   |
| Samsung   | MZWLL800HEHP-00003 | 800 GB | 34E169C05AD2 | 677   | 0     | 1.86   |
| Intel     | SSDPEDKE020T7      | 2 TB   | F8227DAAEC3D | 676   | 0     | 1.85   |
| Intel     | SSDPEKNW010T8      | 1 TB   | FEFFF8686E28 | 676   | 0     | 1.85   |
| Intel     | SSDPED1K750GA      | 752 GB | 5114D57BADEE | 675   | 0     | 1.85   |
| Intel     | SSDPE2KX040T8      | 4 TB   | 803FF87D2271 | 675   | 0     | 1.85   |
| Intel     | SSDPE21D480GA      | 480 GB | 53C763BFF05D | 675   | 0     | 1.85   |
| Intel     | SSDPE21D480GA      | 480 GB | 4FB0A8E9DDBB | 675   | 0     | 1.85   |
| Intel     | SSDPE21D480GA      | 480 GB | 911A8DDC4FAD | 675   | 0     | 1.85   |
| Intel     | SSDPE21D480GA      | 480 GB | 92C677B8A525 | 675   | 0     | 1.85   |
| Intel     | SSDPED1D480GA      | 480 GB | 34E592A11168 | 675   | 0     | 1.85   |
| Intel     | SSDPED1D480GA      | 480 GB | 51794E987D04 | 675   | 0     | 1.85   |
| Intel     | SSDPED1D480GA      | 480 GB | D7B3D5547C42 | 675   | 0     | 1.85   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 5346CA554DB3 | 674   | 0     | 1.85   |
| Intel     | SSDPE21D480GA      | 480 GB | 080349EE871E | 673   | 0     | 1.84   |
| Intel     | SSDPE21D480GA      | 480 GB | 5D5171C7D97C | 673   | 0     | 1.84   |
| Intel     | SSDPE21D480GA      | 480 GB | 87C61ECD0B18 | 673   | 0     | 1.84   |
| Intel     | SSDPEKKF256G8 NVMe | 256 GB | A3B155F2BBA6 | 673   | 0     | 1.84   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | 80137AE52B75 | 673   | 0     | 1.84   |
| WDC       | WUS3BA196C7P3E3    | 800 GB | 9713A6FE5B4E | 673   | 0     | 1.84   |
| WDC       | WUS3BA196C7P3E3    | 800 GB | CBA8A3E5AA0F | 673   | 0     | 1.84   |
| Samsung   | MZ1LB960HAJQ-00007 | 960 GB | B8A64EA182C8 | 670   | 0     | 1.84   |
| Intel     | SSDPE21D480GA      | 480 GB | 00A97F992A6E | 670   | 0     | 1.84   |
| Intel     | SSDPE21D480GA      | 480 GB | 4A2471CD5011 | 670   | 0     | 1.84   |
| Intel     | SSDPE21D480GA      | 480 GB | 703966944D29 | 670   | 0     | 1.84   |
| Intel     | SSDPE21D480GA      | 480 GB | C53B6CD4F2A4 | 670   | 0     | 1.84   |
| Intel     | SSDPE21D480GA      | 480 GB | F52CED6D6316 | 670   | 0     | 1.84   |
| Intel     | SSDPE21D480GA      | 480 GB | FB8C9029A328 | 670   | 0     | 1.84   |
| Intel     | SSDPE21D480GA      | 480 GB | 13302D2B6B73 | 670   | 0     | 1.84   |
| Intel     | SSDPE21D480GA      | 480 GB | F9A6C9973342 | 670   | 0     | 1.84   |
| PNY       | CS3030 500GB SSD   | 500 GB | 840457B32FE1 | 670   | 0     | 1.84   |
| Samsung   | MZPLL3T2HAJQ-00005 | 3.2 TB | 16F1974A7EB7 | 669   | 0     | 1.83   |
| Intel     | SSDPED1K750GA      | 752 GB | 8B2DB9D7F227 | 668   | 0     | 1.83   |
| Intel     | SSDPED1K750GA      | 752 GB | A52059DA7FAA | 668   | 0     | 1.83   |
| Intel     | SSDPED1K750GA      | 752 GB | A6F9B74B3C60 | 668   | 0     | 1.83   |
| Intel     | SSDPED1K750GA      | 752 GB | FD4CBAF76815 | 668   | 0     | 1.83   |
| Intel     | SSDPEKKW256G7      | 256 GB | 84EC81A658D5 | 667   | 0     | 1.83   |
| Intel     | SSDPE21D480GA      | 480 GB | 49D9ACF1807F | 667   | 0     | 1.83   |
| Intel     | SSDPE21D480GA      | 480 GB | 9F998E281AA4 | 667   | 0     | 1.83   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 8B488045066A | 666   | 0     | 1.83   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | A5F662A802A3 | 666   | 0     | 1.83   |
| Intel     | SSDPE2KX040T8      | 4 TB   | 3954D46E3A46 | 665   | 0     | 1.82   |
| Intel     | SSDPE2KX040T8      | 4 TB   | 960CAB17A164 | 665   | 0     | 1.82   |
| Intel     | SSDPE2KX040T8      | 4 TB   | E1F05BF06470 | 665   | 0     | 1.82   |
| Intel     | SSDPED1K750GA      | 752 GB | 28CF9BAD919D | 665   | 0     | 1.82   |
| WDC       | CL SN720 SDAQNT... | 512 GB | 34410C532D0C | 665   | 0     | 1.82   |
| Intel     | SSDPE21D480GA      | 480 GB | 33E2FFA18EC1 | 665   | 0     | 1.82   |
| Dell      | Express Flash P... | 800 GB | CAAD2BA3019B | 665   | 0     | 1.82   |
| Intel     | SSDPE21D480GA      | 480 GB | E21C13885832 | 665   | 0     | 1.82   |
| Intel     | SSDPE21D480GA      | 480 GB | FCA0ACF5F75F | 665   | 0     | 1.82   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | EB60594E6ECF | 664   | 0     | 1.82   |
| Intel     | SSDPED1K750GA      | 752 GB | 0199968698AF | 664   | 0     | 1.82   |
| Intel     | SSDPED1K750GA      | 752 GB | FAA5BDB0C60A | 664   | 0     | 1.82   |
| Intel     | SSDPE2MX450G7      | 450 GB | 9162053DA4A6 | 664   | 0     | 1.82   |
| Samsung   | MZQLB3T8HALS-000AZ | 3.8 TB | 7FD8131970F5 | 663   | 0     | 1.82   |
| Intel     | SSDPED1K750GA      | 752 GB | 0B736B63ABFA | 663   | 0     | 1.82   |
| Samsung   | MZPLL6T4HMLS-00003 | 6.4 TB | A767E1016B53 | 663   | 0     | 1.82   |
| Toshiba   | KXG50ZNV512G       | 512 GB | 85D305B6594E | 662   | 0     | 1.81   |
| WDC       | CL SN720 SDAQNT... | 512 GB | C4B413849900 | 661   | 0     | 1.81   |
| Intel     | SSDPE2KX040T8      | 4 TB   | 9FD513EB5318 | 661   | 0     | 1.81   |
| Intel     | SSDPE2KX040T8      | 4 TB   | C737EDD6037A | 661   | 0     | 1.81   |
| Intel     | SSDPE2KX040T8      | 4 TB   | F749F2441977 | 661   | 0     | 1.81   |
| Intel     | SSDPED1K750GA      | 752 GB | 2EFC77BB16DB | 660   | 0     | 1.81   |
| Samsung   | SSD 970 EVO Plus   | 500 GB | FBA5A1F6C69B | 660   | 0     | 1.81   |
| Samsung   | MZPLL3T2HMLS-00003 | 3.2 TB | F7EF787CDAA0 | 659   | 0     | 1.81   |
| Intel     | SSDPEKKW256G7      | 256 GB | 6D019831FEDF | 659   | 0     | 1.81   |
| Samsung   | SSD 970 EVO Plus   | 500 GB | 25F209E022E0 | 659   | 0     | 1.81   |
| Intel     | SSDPED1K750GA      | 752 GB | 84965F4EFA5F | 657   | 0     | 1.80   |
| Kingston  | SKC1000960G        | 960 GB | B10F2564CEEC | 657   | 0     | 1.80   |
| Samsung   | MZQLB3T8HALS-000AZ | 3.8 TB | 44CA9C33A077 | 657   | 0     | 1.80   |
| Intel     | SSDPEKKW256G7      | 256 GB | 35E6F487EC3F | 656   | 0     | 1.80   |
| Samsung   | MZPLL6T4HMLS-00003 | 6.4 TB | 4BE7242220CB | 656   | 0     | 1.80   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 7CF38199B31B | 656   | 0     | 1.80   |
| Intel     | SSDPED1K750GA      | 752 GB | 16A98E0DAAE9 | 655   | 0     | 1.79   |
| Intel     | SSDPED1K750GA      | 752 GB | 4C4B29D46E11 | 655   | 0     | 1.79   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | 9EA251B8A95B | 654   | 0     | 1.79   |
| Intel     | SSDPED1D480GA      | 480 GB | 1E90DCA0AE70 | 654   | 0     | 1.79   |
| Intel     | SSDPE2KX080T8      | 8 TB   | A9DA75738B06 | 654   | 0     | 1.79   |
| Intel     | SSDPE2KX040T8      | 4 TB   | 9D4A35ACF752 | 653   | 0     | 1.79   |
| Intel     | SSDPED1K750GA      | 752 GB | 1C09C18B3C8B | 653   | 0     | 1.79   |
| Toshiba   | KXG60ZNV512G       | 512 GB | 8EB2C4037E86 | 652   | 0     | 1.79   |
| WDC       | CL SN720 SDAQNT... | 512 GB | E730ACE03864 | 652   | 0     | 1.79   |
| Intel     | SSDPEDMD400G4      | 400 GB | 44D06E3D6CBF | 652   | 0     | 1.79   |
| Intel     | SSDPEDMD400G4      | 400 GB | 9E40781E9962 | 652   | 0     | 1.79   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | B10AC7A4574C | 651   | 0     | 1.79   |
| Intel     | SSDPEDME016T4S     | 1.6 TB | 5F5B0EEF15EB | 651   | 0     | 1.79   |
| WDC       | WUS3BA196C7P3E3    | 800 GB | 8082AAB9A643 | 651   | 0     | 1.79   |
| WDC       | WUS3BA196C7P3E3    | 800 GB | F73A62CAE486 | 651   | 0     | 1.79   |
| Intel     | SSDPEDKX040T7      | 4 TB   | 219ABBA075C2 | 651   | 0     | 1.78   |
| Intel     | SSDPEDMD800G4      | 800 GB | B6469330FAAA | 650   | 0     | 1.78   |
| Intel     | VO0400KEFJB        | 400 GB | DD118B1F5252 | 650   | 0     | 1.78   |
| Samsung   | MZPLL6T4HMLS-00003 | 6.4 TB | EE6DA3DD57A4 | 649   | 0     | 1.78   |
| Samsung   | MZPLL6T4HMLS-00003 | 6.4 TB | 84182F71C073 | 649   | 0     | 1.78   |
| Intel     | SSDPED1K750GA      | 752 GB | 89373C7291DC | 649   | 0     | 1.78   |
| Samsung   | SSD 970 PRO        | 512 GB | 1D6946F60E5F | 648   | 0     | 1.78   |
| WDC       | WUS3BA196C7P3E3    | 800 GB | C8FED79351C7 | 648   | 0     | 1.78   |
| WDC       | WUS3BA196C7P3E3    | 800 GB | EDCC3A641370 | 648   | 0     | 1.78   |
| Intel     | SSDPE2MX450G7      | 450 GB | AA4CD4188E8D | 648   | 0     | 1.78   |
| WDC       | WUS3BA196C7P3E3    | 800 GB | 9D1C4425AC2C | 648   | 0     | 1.78   |
| WDC       | WUS3BA196C7P3E3    | 800 GB | EF0E989423A6 | 648   | 0     | 1.78   |
| Intel     | SSDPED1K750GA      | 752 GB | 40B8EB5EC88A | 648   | 0     | 1.78   |
| Intel     | SSDPED1K750GA      | 752 GB | C61B6BA936D1 | 648   | 0     | 1.78   |
| Samsung   | MZ1LB960HAJQ-00007 | 960 GB | 10AC00A9D962 | 646   | 0     | 1.77   |
| Intel     | SSDPEDMD400G4      | 400 GB | 029132A74480 | 646   | 0     | 1.77   |
| WDC       | CL SN720 SDAQNT... | 512 GB | A4623BC4802D | 646   | 0     | 1.77   |
| WDC       | CL SN720 SDAQNT... | 512 GB | 99964411F1EB | 645   | 0     | 1.77   |
| WDC       | CL SN720 SDAQNT... | 512 GB | AC5D31EDE86D | 644   | 0     | 1.77   |
| Samsung   | MZVKW512HMJP-00000 | 512 GB | 9B8A0B99F9F6 | 644   | 0     | 1.77   |
| Samsung   | MZVKW512HMJP-00000 | 512 GB | BC3B90D4F9D4 | 644   | 0     | 1.76   |
| WDC       | CL SN720 SDAQNT... | 512 GB | CEFF1817D700 | 643   | 0     | 1.76   |
| Samsung   | MZPLL3T2HAJQ-00005 | 3.2 TB | B70517DFE354 | 643   | 0     | 1.76   |
| Intel     | SSDPED1K375GA      | 375 GB | 2E5611B13268 | 643   | 0     | 1.76   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | BCBDCE1CD239 | 642   | 0     | 1.76   |
| Intel     | SSDPED1K750GA      | 752 GB | 15DE8F0DACAB | 641   | 0     | 1.76   |
| Samsung   | MZPLL6T4HMLS-00003 | 6.4 TB | EA036B19378A | 640   | 0     | 1.75   |
| Intel     | SSDPEDMD016T4K     | 1.6 TB | AD8456A37E88 | 640   | 0     | 1.75   |
| Intel     | SSDPEDMD400G4      | 400 GB | 54BB0507C140 | 639   | 0     | 1.75   |
| Intel     | SSDPEDMD400G4      | 400 GB | 2A89D0864CA6 | 638   | 0     | 1.75   |
| Intel     | SSDPED1D480GA      | 480 GB | 7D6ACD552C4E | 638   | 0     | 1.75   |
| Intel     | SSDPED1K750GA      | 752 GB | 21ECDC55545D | 637   | 0     | 1.75   |
| Intel     | SSDPEDMD400G4      | 400 GB | AE6B7960745F | 637   | 0     | 1.75   |
| WDC       | WUS3BA196C7P3E3    | 800 GB | 78EA71B21412 | 637   | 0     | 1.75   |
| WDC       | WUS3BA196C7P3E3    | 800 GB | B41B94C22814 | 637   | 0     | 1.75   |
| WDC       | WUS3BA196C7P3E3    | 800 GB | EA8847015051 | 637   | 0     | 1.75   |
| Intel     | SSDPED1K750GA      | 752 GB | F16E58CD5643 | 637   | 0     | 1.75   |
| WDC       | WUS3BA196C7P3E3    | 800 GB | 2881DDB73C6E | 636   | 0     | 1.74   |
| WDC       | WUS3BA196C7P3E3    | 800 GB | 4D73C5133F92 | 636   | 0     | 1.74   |
| WDC       | WUS3BA196C7P3E3    | 800 GB | 56CCAD1C7155 | 636   | 0     | 1.74   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 4A304B535E8B | 635   | 0     | 1.74   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | EDF1FCD0D118 | 635   | 0     | 1.74   |
| WDC       | WUS3BA196C7P3E3    | 800 GB | 6F9AFC9ABE11 | 634   | 0     | 1.74   |
| WDC       | WUS3BA196C7P3E3    | 800 GB | 7C8B5D98C28A | 634   | 0     | 1.74   |
| WDC       | WUS3BA196C7P3E3    | 800 GB | CF9FCAFA2455 | 634   | 0     | 1.74   |
| Samsung   | MZPLL6T4HMLS-00003 | 6.4 TB | 04989BA379AF | 634   | 0     | 1.74   |
| Toshiba   | KXG60ZNV1T02       | 1 TB   | 2E61BA38DE09 | 634   | 0     | 1.74   |
| Toshiba   | KXG60ZNV1T02       | 1 TB   | A354761B4D45 | 634   | 0     | 1.74   |
| WDC       | WUS3BA196C7P3E3    | 800 GB | 59D030F292B0 | 634   | 0     | 1.74   |
| Intel     | SSDPED1K750GA      | 752 GB | C2EC39AE0031 | 633   | 0     | 1.74   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 90C66053EE8B | 633   | 0     | 1.74   |
| WDC       | WUS3BA196C7P3E3    | 800 GB | 2885CE10F94D | 633   | 0     | 1.74   |
| Samsung   | MZ1LB960HAJQ-00007 | 960 GB | 8CB71F61F29B | 633   | 0     | 1.74   |
| Dell      | Express Flash P... | 800 GB | CA9DAF5F720A | 633   | 0     | 1.74   |
| Dell      | Express Flash N... | 1.6 TB | 49C7D05F7073 | 632   | 0     | 1.73   |
| Intel     | SSDPE2ME012T4      | 1.2 TB | F81BC622785C | 632   | 0     | 1.73   |
| WDC       | WUS3BA196C7P3E3    | 800 GB | 97E00B8645B6 | 631   | 0     | 1.73   |
| WDC       | WUS3BA196C7P3E3    | 960 GB | 89A2E87B5B44 | 631   | 0     | 1.73   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | CA4602308DC9 | 631   | 0     | 1.73   |
| Intel     | SSDPED1D480GA      | 480 GB | 50282E1ADDBF | 631   | 0     | 1.73   |
| Intel     | SSDPED1D480GA      | 480 GB | 68A6EEC689A0 | 631   | 0     | 1.73   |
| Intel     | SSDPE21D480GA      | 480 GB | 0F1E720D2F7F | 631   | 0     | 1.73   |
| Intel     | SSDPE21D480GA      | 480 GB | 6EE2301C750B | 631   | 0     | 1.73   |
| Intel     | SSDPE21D480GA      | 480 GB | 7D7DA3170FE7 | 631   | 0     | 1.73   |
| Intel     | SSDPE21D480GA      | 480 GB | C939DE2EA6C3 | 631   | 0     | 1.73   |
| Intel     | SSDPE21D480GA      | 480 GB | E78FD637C820 | 631   | 0     | 1.73   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | FD6EC4D9A7F7 | 631   | 0     | 1.73   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | F4AE3DC3B490 | 630   | 0     | 1.73   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 4F98700BC543 | 630   | 0     | 1.73   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 9A1478928F61 | 630   | 0     | 1.73   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 179034CDD611 | 629   | 0     | 1.73   |
| Intel     | MT0800KEXUU        | 800 GB | 26A8196F1742 | 629   | 0     | 1.73   |
| WDC       | CL SN720 SDAQNT... | 512 GB | 36B3A894B5D0 | 629   | 0     | 1.73   |
| Intel     | SSDPE21D480GA      | 480 GB | 08E711BE7662 | 629   | 0     | 1.72   |
| Intel     | SSDPE21D480GA      | 480 GB | 36FB5CED7C6A | 629   | 0     | 1.72   |
| Intel     | SSDPE21D480GA      | 480 GB | A2D184427385 | 629   | 0     | 1.72   |
| Samsung   | MZWLL6T4HMLA-00005 | 6.4 TB | 4C316B32ED7C | 629   | 0     | 1.72   |
| Samsung   | MZWLL6T4HMLA-00005 | 6.4 TB | 6330755333AA | 629   | 0     | 1.72   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | F8E07B7488B5 | 629   | 0     | 1.72   |
| Intel     | SSDPED1K750GA      | 752 GB | B1FA8DB30D5C | 629   | 0     | 1.72   |
| WDC       | CL SN720 SDAQNT... | 512 GB | 74B4AF9F6ED0 | 628   | 0     | 1.72   |
| Intel     | SSDPED1K750GA      | 752 GB | C9FA1DA36133 | 627   | 0     | 1.72   |
| Intel     | SSDPE2KX020T8      | 2 TB   | DC6FD6956C1F | 627   | 0     | 1.72   |
| Intel     | SSDPE2KE016T8      | 1.6 TB | B03117885E23 | 625   | 0     | 1.71   |
| Intel     | SSDPE2KX040T8      | 4 TB   | 11A2CFEBA15E | 624   | 0     | 1.71   |
| Intel     | SSDPE2KX040T8      | 4 TB   | 43670F1385C6 | 624   | 0     | 1.71   |
| Intel     | SSDPE2KX040T8      | 4 TB   | 745EF1CE705E | 624   | 0     | 1.71   |
| Intel     | SSDPE2KX040T8      | 4 TB   | 7FA61EA23C9F | 624   | 0     | 1.71   |
| Intel     | SSDPE2KX040T8      | 4 TB   | EACB31A1F20D | 624   | 0     | 1.71   |
| Intel     | SSDPE2KX040T8      | 4 TB   | FE71D5AE21BC | 624   | 0     | 1.71   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 942A0DC68DD4 | 624   | 0     | 1.71   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 1CE92C43517B | 624   | 0     | 1.71   |
| Intel     | SSDPED1K750GA      | 752 GB | CDEB0FBBD2A8 | 622   | 0     | 1.71   |
| Samsung   | MZ1LB960HAJQ-00007 | 960 GB | 85E950CA4EDC | 622   | 0     | 1.71   |
| Toshiba   | KXG50ZNV1T02       | 1 TB   | 5240C0E57F0F | 621   | 0     | 1.70   |
| Samsung   | SSD 960 PRO        | 1 TB   | B035241BEC53 | 620   | 0     | 1.70   |
| Intel     | SSDPE21D480GA      | 480 GB | 302A29FA7029 | 619   | 0     | 1.70   |
| Intel     | SSDPE21D480GA      | 480 GB | 337500538D52 | 619   | 0     | 1.70   |
| Intel     | SSDPE21D480GA      | 480 GB | 226531684F17 | 618   | 0     | 1.70   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | 021CF0DD742A | 618   | 0     | 1.70   |
| Intel     | SSDPED1D480GA      | 480 GB | 23FEF448109E | 618   | 0     | 1.69   |
| Intel     | SSDPED1D480GA      | 480 GB | C4AC7454A7D0 | 618   | 0     | 1.69   |
| WDC       | CL SN720 SDAQNT... | 1 TB   | E28E066389A8 | 617   | 0     | 1.69   |
| Dell      | Express Flash P... | 800 GB | 4648F5C7BBC9 | 617   | 0     | 1.69   |
| Intel     | SSDPEKKW256G7      | 256 GB | 9C8D0670B098 | 617   | 0     | 1.69   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 394B548CDE3C | 616   | 0     | 1.69   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 81D5C9B7912F | 614   | 0     | 1.68   |
| Intel     | SSDPE2MX450G7      | 450 GB | 55132655467B | 614   | 0     | 1.68   |
| Samsung   | MZPLL6T4HMLS-00003 | 6.4 TB | E4DF2BF681A2 | 613   | 0     | 1.68   |
| Samsung   | MZ1LB960HAJQ-00007 | 960 GB | 35ABC3AEAC65 | 613   | 0     | 1.68   |
| Samsung   | MZPLL6T4HMLS-00003 | 6.4 TB | EAFD480493D3 | 613   | 0     | 1.68   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 34F718108ACF | 612   | 0     | 1.68   |
| Intel     | SSDPE21K375GA      | 375 GB | A24AA0B66ED1 | 611   | 0     | 1.68   |
| Intel     | SSDPE2KX020T7      | 2 TB   | A548F7A5DE93 | 611   | 0     | 1.68   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 0CD889EE4A76 | 610   | 0     | 1.67   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | C75EDCE2C9E0 | 609   | 0     | 1.67   |
| Samsung   | MZPLL3T2HAJQ-00005 | 3.2 TB | 09BAA4A5AC37 | 609   | 0     | 1.67   |
| Intel     | SSDPEDKE020T7      | 2 TB   | 1A16CB58BB6F | 609   | 0     | 1.67   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 5EB8DE69DD61 | 608   | 0     | 1.67   |
| Intel     | SSDPE2KX020T8      | 2 TB   | AC4D791EE4B3 | 608   | 0     | 1.67   |
| Intel     | SSDPE21K375GA      | 375 GB | 4E7DEB53BAC6 | 606   | 0     | 1.66   |
| Intel     | SSDPED1K750GA      | 752 GB | 59C2C7D44E48 | 606   | 0     | 1.66   |
| Samsung   | SSD 970 PRO        | 512 GB | E25F1E2973DA | 606   | 0     | 1.66   |
| Intel     | SSDPE2KX080T8      | 8 TB   | 28BC02078F15 | 606   | 0     | 1.66   |
| Samsung   | SSD 970 PRO        | 1 TB   | 56E7FA507E24 | 605   | 0     | 1.66   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | C4ED9FFD019C | 605   | 0     | 1.66   |
| Samsung   | SSD 970 PRO        | 1 TB   | E8533893A38C | 605   | 0     | 1.66   |
| Intel     | SSDPE21K375GA      | 375 GB | A874079AADAD | 605   | 0     | 1.66   |
| Intel     | SSDPEDKE040T7      | 4 TB   | 8E2C9FBC35FC | 604   | 0     | 1.66   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | BF0F98FCEA31 | 603   | 0     | 1.65   |
| Intel     | SSDPED1K750GA      | 752 GB | CDE6CD9B7510 | 603   | 0     | 1.65   |
| WDC       | WUS3BA196C7P3E3    | 960 GB | 0A8F7504DCE8 | 603   | 0     | 1.65   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 0ACF0C177F33 | 602   | 0     | 1.65   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | F94C19211EE7 | 602   | 0     | 1.65   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 6C9C3872A1CB | 602   | 0     | 1.65   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 1460B77861C7 | 602   | 0     | 1.65   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 4AF9C5B2A80E | 601   | 0     | 1.65   |
| Intel     | SSDPE2KX020T8      | 2 TB   | FA8B41B72777 | 601   | 0     | 1.65   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 198006C3C117 | 601   | 0     | 1.65   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 81C5A9D52A4B | 601   | 0     | 1.65   |
| Intel     | SSDPE21D480GA      | 480 GB | 6B5F09C52FE8 | 601   | 0     | 1.65   |
| Intel     | SSDPE2KX010T8      | 1 TB   | D635C9188538 | 600   | 0     | 1.65   |
| Intel     | SSDPE2KX010T8      | 1 TB   | ED0CD937EB80 | 600   | 0     | 1.65   |
| WDC       | CL SN720 SDAQNT... | 1 TB   | 92BCB0EA2C0C | 600   | 0     | 1.64   |
| Samsung   | SSD 983 DCT        | 960 GB | 1076545A916D | 599   | 0     | 1.64   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 69F511930A4C | 599   | 0     | 1.64   |
| WDC       | WUS3BA196C7P3E3    | 800 GB | 5C475BCD6F1E | 598   | 0     | 1.64   |
| WDC       | WUS3BA196C7P3E3    | 800 GB | BEA407468060 | 598   | 0     | 1.64   |
| Intel     | SSDPED1K750GA      | 752 GB | 501651A990A6 | 597   | 0     | 1.64   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 719EED3C91BA | 596   | 0     | 1.64   |
| WDC       | WUS3BA196C7P3E3    | 960 GB | 7468262286F4 | 596   | 0     | 1.64   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 37ED7370BB78 | 596   | 0     | 1.63   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | CA8F48981645 | 596   | 0     | 1.63   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 24F2101EF07C | 595   | 0     | 1.63   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | DA255AA4C754 | 595   | 0     | 1.63   |
| Samsung   | SSD 970 PRO        | 1 TB   | 9740539AEDD2 | 595   | 0     | 1.63   |
| Intel     | SSDPED1K750GA      | 752 GB | EE01AB9F758E | 595   | 0     | 1.63   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 427624934742 | 595   | 0     | 1.63   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 7AF0D06456C6 | 595   | 0     | 1.63   |
| Intel     | SSDPE2KX020T8      | 2 TB   | E6897044A18C | 595   | 0     | 1.63   |
| Intel     | SSDPE2KX020T8      | 2 TB   | EAD69C544235 | 595   | 0     | 1.63   |
| Intel     | SSDPED1K375GA      | 375 GB | 4A97FEB1A2BA | 594   | 0     | 1.63   |
| Dell      | Express Flash N... | 1.6 TB | 8C741AA4B802 | 594   | 0     | 1.63   |
| Intel     | SSDPE21D480GA      | 480 GB | 008A45024F53 | 594   | 0     | 1.63   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 08B135F7F875 | 594   | 0     | 1.63   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 95A062E58B30 | 594   | 0     | 1.63   |
| WDC       | WUS3BA196C7P3E3    | 800 GB | 4BA2FA796BAA | 594   | 0     | 1.63   |
| Intel     | SSDPE21D480GA      | 480 GB | CFFACE3AE354 | 593   | 0     | 1.63   |
| Intel     | SSDPE21D480GA      | 480 GB | DB52FA50D3A8 | 593   | 0     | 1.63   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 03A78299A75B | 593   | 0     | 1.63   |
| Intel     | SSDPE21D480GA      | 480 GB | E4D0FD79C011 | 593   | 0     | 1.62   |
| Dell      | Express Flash N... | 1.6 TB | 1C09FA5C48CE | 592   | 0     | 1.62   |
| WDC       | WDS500G3X0C-00SJG0 | 500 GB | D8C8467D6532 | 592   | 0     | 1.62   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | D8E5432F6DF6 | 591   | 0     | 1.62   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 112042C9EC29 | 591   | 0     | 1.62   |
| Toshiba   | KXG60ZNV512G       | 512 GB | 2D2EF480E227 | 591   | 0     | 1.62   |
| Intel     | SSDPE21D480GA      | 480 GB | 13BDBB780C3A | 590   | 0     | 1.62   |
| Intel     | SSDPE21D480GA      | 480 GB | 1704380956B8 | 590   | 0     | 1.62   |
| Intel     | SSDPE21D480GA      | 480 GB | 1D54E21A4AE5 | 590   | 0     | 1.62   |
| Intel     | SSDPE21D480GA      | 480 GB | 3895E2ABC7D9 | 590   | 0     | 1.62   |
| Intel     | SSDPE21D480GA      | 480 GB | 490FA872ADFF | 590   | 0     | 1.62   |
| Intel     | SSDPE21D480GA      | 480 GB | 6514F480C274 | 590   | 0     | 1.62   |
| Intel     | SSDPE21D480GA      | 480 GB | FD570A2E0AB4 | 590   | 0     | 1.62   |
| Intel     | SSDPED1K750GA      | 752 GB | 38585A7DA093 | 590   | 0     | 1.62   |
| Intel     | SSDPED1K750GA      | 752 GB | 58507A739C16 | 590   | 0     | 1.62   |
| Intel     | SSDPED1K750GA      | 752 GB | EF16D590901B | 590   | 0     | 1.62   |
| Intel     | SSDPED1K750GA      | 752 GB | F9C63668CFDE | 590   | 0     | 1.62   |
| Intel     | SSDPEL1K100GA      | 100 GB | 2830BE65606F | 590   | 0     | 1.62   |
| WDC       | WUS3BA196C7P3E3    | 800 GB | BCDD303C03CD | 589   | 0     | 1.62   |
| Intel     | SSDPED1K750GA      | 752 GB | 364AACD963AE | 589   | 0     | 1.62   |
| Intel     | SSDPED1K750GA      | 752 GB | 84A88B7C4CA8 | 589   | 0     | 1.62   |
| Dell      | Express Flash N... | 1.6 TB | 3672E8B184A0 | 589   | 0     | 1.62   |
| Dell      | Express Flash N... | 1.6 TB | 780694C253DE | 589   | 0     | 1.62   |
| Dell      | Express Flash N... | 1.6 TB | 91203604A5B8 | 589   | 0     | 1.62   |
| Dell      | Express Flash N... | 1.6 TB | D40B58E03943 | 589   | 0     | 1.62   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 000A1A64C680 | 589   | 0     | 1.61   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 1F6F29BE546B | 588   | 0     | 1.61   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 91FD54A1A913 | 588   | 0     | 1.61   |
| Intel     | SSDPED1K750GA      | 752 GB | 102DA79DF7B9 | 588   | 0     | 1.61   |
| Intel     | SSDPED1K750GA      | 752 GB | FB35041D32F6 | 588   | 0     | 1.61   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 66B5B62FE721 | 588   | 0     | 1.61   |
| WDC       | CL SN720 SDAQNT... | 1 TB   | 72006C0674AC | 588   | 0     | 1.61   |
| Intel     | SSDPED1K375GA      | 375 GB | B940E2482E43 | 587   | 0     | 1.61   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 718541DBA369 | 587   | 0     | 1.61   |
| Intel     | SSDPED1K375GA      | 375 GB | 6FAEA677C615 | 587   | 0     | 1.61   |
| Intel     | SSDPED1K750GA      | 752 GB | 102DFB4E77E7 | 587   | 0     | 1.61   |
| Intel     | SSDPED1K750GA      | 752 GB | 11E68EAE14FD | 587   | 0     | 1.61   |
| Intel     | SSDPED1K750GA      | 752 GB | 851DD8DEED2C | 587   | 0     | 1.61   |
| Intel     | SSDPED1K750GA      | 752 GB | 87DA4E967956 | 587   | 0     | 1.61   |
| Intel     | SSDPED1K750GA      | 752 GB | B34F01EF7578 | 586   | 0     | 1.61   |
| Intel     | SSDPE2KX010T8      | 1 TB   | DB05A9F351C7 | 586   | 0     | 1.61   |
| Intel     | SSDPE21D480GA      | 480 GB | AD0BB5727CE3 | 586   | 0     | 1.61   |
| Intel     | SSDPE2KE032T8      | 3.2 TB | 07C5A39A60B9 | 586   | 0     | 1.61   |
| Intel     | SSDPEDMD800G4      | 800 GB | 6C1276468602 | 585   | 0     | 1.60   |
| Intel     | SSDPE2MX450G7      | 450 GB | A7CA6BE8EFE2 | 585   | 0     | 1.60   |
| Intel     | SSDPED1K375GA      | 375 GB | 0AC5166971DD | 585   | 0     | 1.60   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | C6B40DD8B7EC | 585   | 0     | 1.60   |
| WDC       | CL SN720 SDAQNT... | 512 GB | CDE88E710918 | 585   | 0     | 1.60   |
| Samsung   | SSD 970 PRO        | 1 TB   | 365DF7201E66 | 584   | 0     | 1.60   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 8344489735A0 | 583   | 0     | 1.60   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | E2EF8D00297B | 583   | 0     | 1.60   |
| Samsung   | SSD 983 DCT        | 960 GB | B7EC8C0F16D1 | 583   | 0     | 1.60   |
| Intel     | SSDPED1K750GA      | 752 GB | 0152D926F564 | 583   | 0     | 1.60   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 84C25246C40A | 582   | 0     | 1.60   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | A78E052AB54C | 582   | 0     | 1.60   |
| Intel     | SSDPE21D480GA      | 480 GB | 3B1952C73E3A | 582   | 0     | 1.60   |
| Intel     | SSDPE21D480GA      | 480 GB | 76D848AA7D8E | 582   | 0     | 1.60   |
| Intel     | SSDPE21D480GA      | 480 GB | E74EE4A05341 | 582   | 0     | 1.60   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | A5F88F729A3D | 582   | 0     | 1.59   |
| Intel     | SSDPED1K375GA      | 375 GB | 7FE78437403A | 581   | 0     | 1.59   |
| Intel     | SSDPED1K375GA      | 375 GB | 87B34195F419 | 581   | 0     | 1.59   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 6482FCF3BD82 | 581   | 0     | 1.59   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | CE42BAAC1B5C | 580   | 0     | 1.59   |
| Samsung   | MZVLB1T0HALR-00000 | 1 TB   | 80AFC1763875 | 579   | 0     | 1.59   |
| Samsung   | MZQLW960HMJP-00003 | 960 GB | F84E16149778 | 579   | 0     | 1.59   |
| Samsung   | SSD 970 EVO Plus   | 500 GB | F08B29D23658 | 579   | 0     | 1.59   |
| Samsung   | MZPLL3T2HAJQ-00005 | 3.2 TB | 35E151FC80A1 | 578   | 0     | 1.59   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 134205A9D736 | 577   | 0     | 1.58   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | B930DB98217A | 577   | 0     | 1.58   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 4F0A4DCF8DBB | 576   | 0     | 1.58   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | B06719B7F1CF | 576   | 0     | 1.58   |
| Intel     | SSDPED1K750GA      | 752 GB | 707955959BEB | 576   | 0     | 1.58   |
| Intel     | SSDPED1K375GA      | 375 GB | 02E5CDE5E061 | 576   | 0     | 1.58   |
| WDC       | WUS3BA196C7P3E3    | 960 GB | 97A62C4A0DF9 | 576   | 0     | 1.58   |
| Intel     | SSDPEDKX040T7      | 4 TB   | 32513DFC1BA2 | 576   | 0     | 1.58   |
| Intel     | SSDPE2KX010T8      | 1 TB   | 352A417A7DFB | 575   | 0     | 1.58   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 1DAD6BD365F6 | 575   | 0     | 1.58   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | C86598490B51 | 575   | 0     | 1.58   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | F91D95990C8D | 575   | 0     | 1.58   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | AE81ECF18F46 | 575   | 0     | 1.58   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | EDBAB1F03535 | 575   | 0     | 1.58   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | EA889B51D39B | 574   | 0     | 1.58   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 285A2B20584A | 574   | 0     | 1.57   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | FF77292C7F67 | 574   | 0     | 1.57   |
| Samsung   | MZPLL3T2HMLS-00003 | 3.2 TB | BEF1ABC5B3CC | 574   | 0     | 1.57   |
| Dell      | Express Flash C... | 960 GB | 2E6AB0A8F9F2 | 574   | 0     | 1.57   |
| Samsung   | MZVLB1T0HALR-00000 | 1 TB   | A3DDB49DA73F | 573   | 0     | 1.57   |
| Samsung   | MZPLL3T2HMLS-00003 | 3.2 TB | 8D330DDFA4DA | 573   | 0     | 1.57   |
| Intel     | SSDPE2KX010T8      | 1 TB   | 33DE5236DBF5 | 572   | 0     | 1.57   |
| Intel     | SSDPED1D480GA      | 480 GB | F96F9DEED5D5 | 572   | 0     | 1.57   |
| Dell      | Express Flash N... | 1.6 TB | 9F47181A3931 | 572   | 0     | 1.57   |
| Intel     | SSDPEL1D380GA      | 384 GB | 7AE98C6496E4 | 572   | 0     | 1.57   |
| Intel     | SSDPE2MX450G7      | 450 GB | DB1B3B18F79E | 572   | 0     | 1.57   |
| Intel     | SSDPEL1D380GA      | 384 GB | AE7902F1274B | 571   | 0     | 1.56   |
| Samsung   | MZWLL800HEHP-00003 | 800 GB | 8A998261ED6A | 570   | 0     | 1.56   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 397F813F71DB | 570   | 0     | 1.56   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 1533DE0301EB | 570   | 0     | 1.56   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | BD4AFC7FF435 | 570   | 0     | 1.56   |
| WDC       | WUS3BA196C7P3E3    | 960 GB | 9CFDDB395C56 | 569   | 0     | 1.56   |
| Samsung   | MZQLW960HMJP-00003 | 960 GB | 04B3E35E4B94 | 569   | 0     | 1.56   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 094C1690FA6B | 569   | 0     | 1.56   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 3E6BD0B5933E | 569   | 0     | 1.56   |
| Intel     | SSDPE2KX040T7      | 4 TB   | 228D960379EB | 569   | 0     | 1.56   |
| Intel     | SSDPEL1D380GA      | 384 GB | 32FED8CC4F36 | 569   | 0     | 1.56   |
| Intel     | SSDPEL1D380GA      | 384 GB | F5D12C545513 | 569   | 0     | 1.56   |
| Intel     | SSDPED1K750GA      | 752 GB | EB245AE86ABE | 568   | 0     | 1.56   |
| WDC       | CL SN720 SDAQNT... | 512 GB | 27FC5A99D954 | 568   | 0     | 1.56   |
| Intel     | SSDPEL1D380GA      | 384 GB | 9338B0BB2A03 | 568   | 0     | 1.56   |
| Samsung   | MZPLL3T2HAJQ-00005 | 3.2 TB | 91D3B3252B8D | 567   | 0     | 1.56   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 1CD657F94DEE | 567   | 0     | 1.55   |
| Intel     | SSDPE2ME012T4      | 1.2 TB | C6FA638BB213 | 566   | 0     | 1.55   |
| Dell      | Express Flash C... | 960 GB | 28801DF80829 | 565   | 0     | 1.55   |
| Samsung   | SSD 970 EVO Plus   | 1 TB   | 9F6476D8C502 | 565   | 0     | 1.55   |
| Samsung   | SSD 970 EVO Plus   | 1 TB   | 3A0658810F7A | 565   | 0     | 1.55   |
| Samsung   | MZVLB512HAJQ-00000 | 512 GB | FC3A4A0A91C3 | 564   | 0     | 1.55   |
| WDC       | CL SN720 SDAQNT... | 512 GB | 27CFFA16FD7F | 564   | 0     | 1.55   |
| Dell      | Express Flash C... | 960 GB | 365DC079D291 | 564   | 0     | 1.55   |
| Intel     | SSDPEDKE020T7      | 2 TB   | 27E3BE96CCAB | 563   | 0     | 1.54   |
| Dell      | Express Flash N... | 1.6 TB | 702E152EAF6E | 563   | 0     | 1.54   |
| WDC       | WDS500G3X0C-00SJG0 | 500 GB | E04F183CB8E2 | 563   | 0     | 1.54   |
| WDC       | WUS3BA196C7P3E3    | 960 GB | 8FD095700B4C | 563   | 0     | 1.54   |
| Intel     | SSDPED1K375GA      | 375 GB | 035987A850BE | 562   | 0     | 1.54   |
| Intel     | SSDPEL1K100GA      | 100 GB | 2CE48C8F8BE3 | 562   | 0     | 1.54   |
| Samsung   | MZ1LB960HAJQ-00007 | 960 GB | E94A278725A3 | 562   | 0     | 1.54   |
| Kingston  | SKC2000M81000G     | 1 TB   | ED5481CC63AE | 562   | 0     | 1.54   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | F8E91117E428 | 561   | 0     | 1.54   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 253B1921B2B2 | 561   | 0     | 1.54   |
| WDC       | WUS3BA196C7P3E3    | 960 GB | 03F9CC625716 | 561   | 0     | 1.54   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 8361513381C3 | 561   | 0     | 1.54   |
| Intel     | SSDPEL1K100GA      | 100 GB | 9A28F66960F5 | 560   | 0     | 1.54   |
| Dell      | Express Flash N... | 1.6 TB | D55B3A73C563 | 560   | 0     | 1.54   |
| Samsung   | MZPLL3T2HAJQ-00005 | 3.2 TB | E2D57D6C4B57 | 559   | 0     | 1.53   |
| Intel     | SSDPED1K375GA      | 375 GB | 3244CA1A4314 | 559   | 0     | 1.53   |
| WDC       | CL SN720 SDAQNT... | 512 GB | DF4E2FF990FB | 559   | 0     | 1.53   |
| Samsung   | MZVLB512HAJQ-00000 | 512 GB | 1A02CBB48CB9 | 558   | 0     | 1.53   |
| WDC       | CL SN720 SDAQNT... | 512 GB | 430006A68440 | 558   | 0     | 1.53   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | DCF22568CDD3 | 556   | 0     | 1.52   |
| Samsung   | MZQLW960HMJP-00003 | 960 GB | B46A173AB526 | 555   | 0     | 1.52   |
| WDC       | CL SN720 SDAQNT... | 1 TB   | 882D02296475 | 555   | 0     | 1.52   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 5E847EF5996E | 555   | 0     | 1.52   |
| WDC       | CL SN720 SDAQNT... | 512 GB | BEDA36FF2C28 | 554   | 0     | 1.52   |
| Dell      | Express Flash N... | 1.6 TB | 301A8BD9A9E9 | 554   | 0     | 1.52   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 9E58A6109C27 | 554   | 0     | 1.52   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 367428335774 | 553   | 0     | 1.52   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 639FF8EBF8B4 | 553   | 0     | 1.52   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | C3225D700173 | 553   | 0     | 1.52   |
| Samsung   | MZVLB512HAJQ-00000 | 512 GB | D6D60D52B173 | 553   | 0     | 1.52   |
| WDC       | CL SN720 SDAQNT... | 512 GB | 2B2909B85D2F | 553   | 0     | 1.52   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 9495D7749ADD | 553   | 0     | 1.52   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | C52EF5B4BC3B | 553   | 0     | 1.52   |
| Intel     | SSDPE2KX040T7      | 4 TB   | BCBA49DF3E5A | 552   | 0     | 1.51   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | C332055999C7 | 552   | 0     | 1.51   |
| Wester... | WUS3BA196C7P3E3    | 800 GB | 6F9AFC9ABE11 | 551   | 0     | 1.51   |
| WDC       | WUS3BA138C7P3E3    | 3.8 TB | 8FBCA56F1794 | 550   | 0     | 1.51   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 10CECFB3F911 | 550   | 0     | 1.51   |
| Samsung   | MZ1LB960HAJQ-00007 | 960 GB | FB04D1CEB7FB | 550   | 0     | 1.51   |
| Intel     | SSDPE2KX010T8      | 1 TB   | 13A015B498AC | 550   | 0     | 1.51   |
| WDC       | WUS3BA138C7P3E3    | 3.8 TB | B1EE82A3550C | 550   | 0     | 1.51   |
| WDC       | WUS3BA196C7P3E3    | 960 GB | 89F9219138BA | 549   | 0     | 1.51   |
| WDC       | CL SN720 SDAQNT... | 512 GB | 6DDA7374E995 | 549   | 0     | 1.51   |
| Intel     | SSDPED1K375GA      | 375 GB | 8383DDF426C2 | 549   | 0     | 1.51   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 28938565F999 | 549   | 0     | 1.51   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 47F916FD85AC | 549   | 0     | 1.51   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 9B55F61E80F9 | 549   | 0     | 1.50   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | BBF7EBA0413E | 549   | 0     | 1.50   |
| WDC       | PC SN720 SDAPNT... | 256 GB | 7A3440E5D828 | 549   | 0     | 1.50   |
| Intel     | SSDPE2KX040T8      | 4 TB   | A7B5BAA29A1A | 548   | 0     | 1.50   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 528F13EF3D6B | 548   | 0     | 1.50   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | 0E0EFE051DB6 | 548   | 0     | 1.50   |
| Intel     | SSDPEKKW256G8      | 256 GB | EF0C0567ACC1 | 548   | 0     | 1.50   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 66B65502F307 | 548   | 0     | 1.50   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | AD4C8C6D62D5 | 548   | 0     | 1.50   |
| Toshiba   | KXG50ZNV1T02       | 1 TB   | CF414CA33A27 | 548   | 0     | 1.50   |
| Intel     | SSDPED1K375GA      | 375 GB | 861F471CD330 | 548   | 0     | 1.50   |
| Toshiba   | KXG50ZNV512G       | 512 GB | 1063F3DBD51A | 548   | 0     | 1.50   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | C1AC0F3F0379 | 547   | 0     | 1.50   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 56B1D6BBF7AB | 547   | 0     | 1.50   |
| Intel     | SSDPED1K750GA      | 752 GB | 0237A25498CB | 546   | 0     | 1.50   |
| Dell      | Express Flash N... | 1.6 TB | 9F024064B32A | 546   | 0     | 1.50   |
| Samsung   | MZVLB512HAJQ-00000 | 512 GB | C404DD8FC3A0 | 546   | 0     | 1.50   |
| WDC       | WUS3BA138C7P3E3    | 3.8 TB | B4EC0D1ED904 | 546   | 0     | 1.50   |
| Dell      | Express Flash C... | 960 GB | DCE676D7F246 | 546   | 0     | 1.50   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 0D1F538E3527 | 545   | 0     | 1.50   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 58F9F7C6A165 | 545   | 0     | 1.49   |
| Dell      | Express Flash N... | 1.6 TB | 657E1ED963AD | 545   | 0     | 1.49   |
| Dell      | Express Flash N... | 1.6 TB | AA0940153B43 | 545   | 0     | 1.49   |
| Dell      | Express Flash N... | 1.6 TB | 3DDC5B475E20 | 545   | 0     | 1.49   |
| Dell      | Express Flash N... | 1.6 TB | 8C014494684B | 545   | 0     | 1.49   |
| Dell      | Express Flash N... | 1.6 TB | E2953A94E81C | 545   | 0     | 1.49   |
| Corsair   | Force MP510 1.9TB  | 1.9 TB | E2AF94325ADF | 545   | 0     | 1.49   |
| Intel     | SSDPE2KE032T8      | 3.2 TB | 64C658FFF067 | 545   | 0     | 1.49   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 9662985719CC | 544   | 0     | 1.49   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | B13E82236666 | 544   | 0     | 1.49   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 06DB750EBB29 | 544   | 0     | 1.49   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 7D483CA0F397 | 544   | 0     | 1.49   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | E7D769F68519 | 544   | 0     | 1.49   |
| Samsung   | MZVLB512HAJQ-00000 | 512 GB | 2FA1A49A1911 | 543   | 0     | 1.49   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 39351657609E | 543   | 0     | 1.49   |
| Micron    | 9300_MTFDHAL3T2TDR | 3.2 TB | 31CF08CB936B | 543   | 0     | 1.49   |
| Dell      | Express Flash N... | 3.2 TB | DFE4F1549670 | 543   | 0     | 1.49   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 388B2AD4A366 | 543   | 0     | 1.49   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 8517A408DFB8 | 543   | 0     | 1.49   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | D918F51FB993 | 543   | 0     | 1.49   |
| Dell      | Express Flash N... | 3.2 TB | 9FC590BC6059 | 542   | 0     | 1.49   |
| Intel     | SSDPEKKW010T8      | 1 TB   | 6875F8F98748 | 540   | 0     | 1.48   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 0FE54BD697A6 | 539   | 0     | 1.48   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | D7399502D6E9 | 539   | 0     | 1.48   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | FC2EB57ED55C | 539   | 0     | 1.48   |
| WDC       | WUS3BA138C7P3E3    | 3.8 TB | 1FF7EC5CD064 | 538   | 0     | 1.48   |
| WDC       | WUS3BA138C7P3E3    | 3.8 TB | E2A138ADFC67 | 538   | 0     | 1.48   |
| WDC       | WUS3BA138C7P3E3    | 3.8 TB | 72D17EA5169B | 538   | 0     | 1.48   |
| Dell      | Express Flash N... | 3.2 TB | 9074797E91CD | 538   | 0     | 1.47   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | 480508ADA1D7 | 537   | 0     | 1.47   |
| WDC       | CL SN720 SDAQNT... | 512 GB | 90E740108280 | 537   | 0     | 1.47   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 9E17A50EA859 | 536   | 0     | 1.47   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | E8DF6164ED46 | 536   | 0     | 1.47   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | EE40D3A3A269 | 536   | 0     | 1.47   |
| Intel     | SSDPED1K750GA      | 752 GB | C2E878C9C371 | 536   | 0     | 1.47   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 47D8D40DBB15 | 535   | 0     | 1.47   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | DFF08FFF692D | 535   | 0     | 1.47   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 1748D6E99E56 | 535   | 0     | 1.47   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 3A80BD312A46 | 535   | 0     | 1.47   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 1CF62923E1BE | 535   | 0     | 1.47   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | CD020E9FF96A | 535   | 0     | 1.47   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 0B4937643A99 | 534   | 0     | 1.47   |
| Intel     | SSDPEKKA256G7      | 256 GB | 3570E86690BB | 534   | 0     | 1.47   |
| Samsung   | SSD 970 PRO        | 1 TB   | C48D63EE1891 | 534   | 0     | 1.46   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 02E9455D4E66 | 533   | 0     | 1.46   |
| Intel     | SSDPED1K750GA      | 752 GB | 551EF3BF4363 | 533   | 0     | 1.46   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 24003B8431F2 | 532   | 0     | 1.46   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | F9BCD15500C8 | 532   | 0     | 1.46   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 8BB2B993E2B5 | 532   | 0     | 1.46   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 264487D28CA3 | 532   | 0     | 1.46   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 8C81A9A125F6 | 531   | 0     | 1.46   |
| Corsair   | Force MP600        | 1 TB   | 4D61E68FC26F | 531   | 0     | 1.46   |
| WDC       | WDS500G3X0C-00SJG0 | 500 GB | 05FBDB9B0DDE | 531   | 0     | 1.46   |
| Intel     | SSDPED1D280GA      | 280 GB | 622CED62DB19 | 531   | 0     | 1.46   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 04909FE358BB | 531   | 0     | 1.46   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 9F92A2FBD06C | 531   | 0     | 1.45   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | AF9B5F839DE8 | 531   | 0     | 1.45   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | B3882BFB92A1 | 531   | 0     | 1.45   |
| Dell      | Express Flash N... | 1.6 TB | B411612C2622 | 530   | 0     | 1.45   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 0753E2C42F7D | 530   | 0     | 1.45   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 81E51D649FA5 | 530   | 0     | 1.45   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | D94728550E73 | 530   | 0     | 1.45   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | E79D20349517 | 529   | 0     | 1.45   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | DDA94E177C93 | 529   | 0     | 1.45   |
| Dell      | Express Flash N... | 3.2 TB | CE43038ACAF1 | 529   | 0     | 1.45   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 11C6C0D5159E | 529   | 0     | 1.45   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 25726BD1D634 | 529   | 0     | 1.45   |
| WDC       | WUS3BA138C7P3E3    | 3.8 TB | 81A053D9CADB | 528   | 0     | 1.45   |
| Intel     | SSDPED1K750GA      | 752 GB | 73B02D53FA82 | 528   | 0     | 1.45   |
| Samsung   | MZPLL3T2HAJQ-00005 | 3.2 TB | 28E05C557A76 | 528   | 0     | 1.45   |
| Intel     | SSDPEDMD400G4      | 400 GB | 1C3A354BCEEA | 528   | 0     | 1.45   |
| Dell      | Express Flash C... | 960 GB | 288965D205DE | 528   | 0     | 1.45   |
| Samsung   | MZPLL3T2HAJQ-00005 | 3.2 TB | DD8D8686281C | 528   | 0     | 1.45   |
| WDC       | WUS3BA138C7P3E3    | 3.8 TB | 2653058AFA7D | 527   | 0     | 1.45   |
| WDC       | WUS3BA138C7P3E3    | 3.8 TB | 74F006E44DA1 | 527   | 0     | 1.45   |
| Kingston  | SA2000M81000G      | 1 TB   | 69C8CA9AE9A8 | 527   | 0     | 1.45   |
| Dell      | Express Flash P... | 6.4 TB | C990ACE0C8EF | 527   | 0     | 1.45   |
| Intel     | SSDPED1D480GA      | 480 GB | F299437D0FB6 | 527   | 0     | 1.44   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | DE8E00C3E6BF | 527   | 0     | 1.44   |
| Intel     | SSDPED1K750GA      | 752 GB | 3A9C19722528 | 526   | 0     | 1.44   |
| Intel     | SSDPED1K750GA      | 752 GB | 83CA3A961745 | 526   | 0     | 1.44   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 24B3FB820D89 | 525   | 0     | 1.44   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 8693CC786A55 | 525   | 0     | 1.44   |
| WDC       | CL SN720 SDAQNT... | 512 GB | 219C05A9BD96 | 524   | 0     | 1.44   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 03F62E11CBA2 | 524   | 0     | 1.44   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 7F509EE33B82 | 524   | 0     | 1.44   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | B406DD0961B7 | 524   | 0     | 1.44   |
| WDC       | WUS3BA138C7P3E3    | 3.8 TB | 5348C23D90AD | 524   | 0     | 1.44   |
| WDC       | WUS3BA138C7P3E3    | 3.8 TB | DF8879C7965C | 524   | 0     | 1.44   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 11C3630EA3B2 | 524   | 0     | 1.44   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | B895FAC763B1 | 524   | 0     | 1.44   |
| Intel     | SSDPEL1D380GA      | 384 GB | C7306B116747 | 524   | 0     | 1.44   |
| WDC       | WUS3BA138C7P3E3    | 3.8 TB | A5BA4ACCDC4D | 523   | 0     | 1.43   |
| WDC       | CL SN720 SDAQNT... | 512 GB | 3AFF16D8D5AD | 522   | 0     | 1.43   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | FD9FEB4B2CC6 | 521   | 0     | 1.43   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 101FA47E19D2 | 521   | 0     | 1.43   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 3A11CB9719DF | 521   | 0     | 1.43   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 5BBDFC193B93 | 521   | 0     | 1.43   |
| Intel     | SSDPEKKA512G8      | 512 GB | 9496B5DB5CC9 | 520   | 0     | 1.43   |
| Dell      | Express Flash N... | 1.6 TB | C83E40738814 | 520   | 0     | 1.43   |
| WDC       | CL SN720 SDAQNT... | 512 GB | 12ACE245D952 | 519   | 0     | 1.42   |
| WDC       | CL SN720 SDAQNT... | 1 TB   | 24FEE8A3B97D | 518   | 0     | 1.42   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 3EFE2F3E6C9C | 518   | 0     | 1.42   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 4B5793800134 | 518   | 0     | 1.42   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 81C745AFD0E7 | 518   | 0     | 1.42   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | B5AFB5C9C430 | 518   | 0     | 1.42   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | B752B379A6A7 | 518   | 0     | 1.42   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | C1AB07CBA62E | 518   | 0     | 1.42   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | CFF6895C7EEF | 518   | 0     | 1.42   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | E958B8D5883D | 518   | 0     | 1.42   |
| WDC       | WUS3BA138C7P3E3    | 3.8 TB | 30BF0C8326C1 | 518   | 0     | 1.42   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 31DF5FD5FB56 | 517   | 0     | 1.42   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 3DD0FE3B5DF9 | 517   | 0     | 1.42   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 467677EA1210 | 517   | 0     | 1.42   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 5C290EA51A46 | 517   | 0     | 1.42   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 934BE33CCACC | 517   | 0     | 1.42   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | A4C013D1DF66 | 517   | 0     | 1.42   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | C9BFA74F1467 | 517   | 0     | 1.42   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | F97A79B57C6B | 517   | 0     | 1.42   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 3A59C4D442CA | 517   | 0     | 1.42   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 24387343F84C | 517   | 0     | 1.42   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 3A3D800273B5 | 517   | 0     | 1.42   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 467AAA6EFC80 | 517   | 0     | 1.42   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 99713676E102 | 517   | 0     | 1.42   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | B9F7036E3BB3 | 517   | 0     | 1.42   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | DADC816EF494 | 517   | 0     | 1.42   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | ECDDC3C1EBB6 | 517   | 0     | 1.42   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | F298EA42A07E | 517   | 0     | 1.42   |
| Intel     | SSDPEDKE020T7      | 2 TB   | A7833473CDB4 | 517   | 0     | 1.42   |
| Intel     | SSDPEDKE020T7      | 2 TB   | 7FC839F6C03D | 516   | 0     | 1.42   |
| Intel     | SSDPEDKE020T7      | 2 TB   | 997A9D83ADA5 | 516   | 0     | 1.42   |
| WDC       | WDS500G3X0C-00SJG0 | 500 GB | 9038E71FACDA | 516   | 0     | 1.42   |
| WDC       | WUS3BA196C7P3E3    | 960 GB | 77D7920C1E21 | 516   | 0     | 1.41   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 76B5ABEF5DC9 | 516   | 0     | 1.41   |
| Samsung   | MZPLL6T4HMLS-00003 | 6.4 TB | B5B3E6ACDAB4 | 515   | 0     | 1.41   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 4E797D393278 | 515   | 0     | 1.41   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 853132E6ED1E | 515   | 0     | 1.41   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 2A82FD13ADE6 | 515   | 0     | 1.41   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | EBEDA1CB99ED | 514   | 0     | 1.41   |
| Intel     | SSDPE21K750GA      | 752 GB | 18AE40343B16 | 514   | 0     | 1.41   |
| Intel     | SSDPE21K750GA      | 752 GB | A7DE12DE3C96 | 514   | 0     | 1.41   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 84154DC382B0 | 514   | 0     | 1.41   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | C6092E986231 | 514   | 0     | 1.41   |
| Dell      | Express Flash N... | 1.6 TB | F4EBECA4A210 | 514   | 0     | 1.41   |
| Intel     | SSDPE21K750GA      | 752 GB | 25383511BDDE | 514   | 0     | 1.41   |
| Dell      | Express Flash P... | 800 GB | D525AE0F8E25 | 514   | 0     | 1.41   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 6D7EDFE4252A | 513   | 0     | 1.41   |
| Intel     | SSDPE21K750GA      | 752 GB | 2EB56C870E49 | 513   | 0     | 1.41   |
| Intel     | SSDPE21K750GA      | 752 GB | 34FB1725D312 | 513   | 0     | 1.41   |
| Intel     | SSDPE21K750GA      | 752 GB | 6F11874B1D2A | 513   | 0     | 1.41   |
| Intel     | SSDPE21K750GA      | 752 GB | 890CBD25E18D | 513   | 0     | 1.41   |
| Intel     | SSDPE21K750GA      | 752 GB | A24607B848C2 | 513   | 0     | 1.41   |
| Intel     | SSDPE21K750GA      | 752 GB | 1A66D6FB592E | 513   | 0     | 1.41   |
| Intel     | SSDPE21K750GA      | 752 GB | 1D77DBACC553 | 513   | 0     | 1.41   |
| Intel     | SSDPE21K750GA      | 752 GB | 4ECA5A63FD36 | 513   | 0     | 1.41   |
| Intel     | SSDPE21K750GA      | 752 GB | DEDEC0678855 | 513   | 0     | 1.41   |
| Intel     | SSDPE21K750GA      | 752 GB | 2D4B8AE37568 | 512   | 0     | 1.40   |
| Intel     | SSDPE21K750GA      | 752 GB | 91C4F7A6A9B7 | 512   | 0     | 1.40   |
| Intel     | SSDPE21K750GA      | 752 GB | C65F99C9D51E | 512   | 0     | 1.40   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 07E52D27479B | 512   | 0     | 1.40   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | F750ACDAF8ED | 512   | 0     | 1.40   |
| Samsung   | MZVLB1T0HALR-00000 | 1 TB   | 50BD483AD23E | 512   | 0     | 1.40   |
| Samsung   | MZPLL6T4HMLS-000MV | 6.4 TB | 0334B049B355 | 511   | 0     | 1.40   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 0C264D3C199A | 511   | 0     | 1.40   |
| WDC       | WUS3BA196C7P3E3    | 960 GB | 0983012B5560 | 511   | 0     | 1.40   |
| Intel     | SSDPEKKA512G8      | 512 GB | E86AF88102A2 | 511   | 0     | 1.40   |
| Intel     | SSDPED1K375GA      | 375 GB | E1941F7CCA59 | 510   | 0     | 1.40   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 02535824B9BF | 510   | 0     | 1.40   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 0CC1F3372400 | 510   | 0     | 1.40   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 182E33256E4C | 510   | 0     | 1.40   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 3CEC2629939C | 510   | 0     | 1.40   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 5297BA7083AF | 510   | 0     | 1.40   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 714B70A00BFC | 510   | 0     | 1.40   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 7C154D6CB8F6 | 510   | 0     | 1.40   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | E7AE7087C5E8 | 510   | 0     | 1.40   |
| WDC       | WUS3BA196C7P3E3    | 960 GB | 2D4B5BBD4554 | 510   | 0     | 1.40   |
| PNY       | CS3030 500GB SSD   | 500 GB | 015A2331285C | 509   | 0     | 1.40   |
| Intel     | SSDPED1D960GAY     | 960 GB | 6058B4468DD8 | 509   | 0     | 1.39   |
| Intel     | SSDPE2KX010T8      | 1 TB   | F8F266A46680 | 508   | 0     | 1.39   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 422C6EF848BA | 508   | 0     | 1.39   |
| Samsung   | MZWLL800HEHP-00003 | 800 GB | 404B89789C6B | 508   | 0     | 1.39   |
| Intel     | SSDPE21K750GA      | 752 GB | C52683D73B2E | 508   | 0     | 1.39   |
| Intel     | SSDPE21K750GA      | 752 GB | C2E25EF71DFB | 508   | 0     | 1.39   |
| Intel     | SSDPE21K750GA      | 752 GB | E98E73782E26 | 508   | 0     | 1.39   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 1519A60F85A6 | 508   | 0     | 1.39   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | D0C692BE0844 | 508   | 0     | 1.39   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | F2F372E8BFB9 | 508   | 0     | 1.39   |
| Intel     | SSDPEDMD800G4      | 800 GB | 5133CC875EDB | 507   | 0     | 1.39   |
| Toshiba   | KXG60ZNV512G       | 512 GB | 210898BA3B10 | 507   | 0     | 1.39   |
| Intel     | SSDPEDMD800G4      | 800 GB | 9A761A59E9DC | 507   | 0     | 1.39   |
| Samsung   | MZQLW1T9HMJP-000AZ | 1.9 TB | 94929CC45D65 | 507   | 0     | 1.39   |
| Samsung   | SSD 970 PRO        | 512 GB | E50DDCEED26C | 506   | 0     | 1.39   |
| Intel     | SSDPEDMD800G4      | 800 GB | 037AB4BD6FE6 | 506   | 0     | 1.39   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 6FE516A47DB2 | 505   | 0     | 1.39   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | A368953AADBC | 505   | 0     | 1.39   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 58E444EFC4CE | 505   | 0     | 1.39   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 0524473379A6 | 505   | 0     | 1.38   |
| Dell      | Express Flash N... | 1.6 TB | B4C6DFCAB89A | 504   | 0     | 1.38   |
| WDC       | WUS3BA196C7P3E3    | 960 GB | E94F6B19387B | 504   | 0     | 1.38   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 4A4719564CC1 | 504   | 0     | 1.38   |
| Samsung   | MZVLB1T0HALR-00000 | 1 TB   | CCC2AE45F39B | 504   | 0     | 1.38   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 348BB7725191 | 503   | 0     | 1.38   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | F87E90F02CAF | 503   | 0     | 1.38   |
| Samsung   | MZVLB512HAJQ-00000 | 512 GB | 8B00D335F694 | 503   | 0     | 1.38   |
| Samsung   | MZVLB512HAJQ-00000 | 512 GB | 53193FFFF0A4 | 503   | 0     | 1.38   |
| Samsung   | MZWLL800HEHP-00003 | 800 GB | FF5D179BF6B0 | 503   | 0     | 1.38   |
| Wester... | WUS3BA196C7P3E3    | 800 GB | 4BA2FA796BAA | 503   | 0     | 1.38   |
| Wester... | WUS3BA196C7P3E3    | 800 GB | 5C475BCD6F1E | 503   | 0     | 1.38   |
| Wester... | WUS3BA196C7P3E3    | 800 GB | BCDD303C03CD | 503   | 0     | 1.38   |
| Wester... | WUS3BA196C7P3E3    | 800 GB | BEA407468060 | 503   | 0     | 1.38   |
| Intel     | SSDPEKKA512G8      | 512 GB | 0D205024CC93 | 502   | 0     | 1.38   |
| Dell      | Express Flash N... | 1.6 TB | 1C12B3E9B149 | 502   | 0     | 1.38   |
| Dell      | Express Flash N... | 1.6 TB | 9EEA45AC28D4 | 502   | 0     | 1.38   |
| Dell      | Express Flash N... | 1.6 TB | EBA1CBD25777 | 502   | 0     | 1.38   |
| Samsung   | MZPJB480HMGC-0BW07 | 480 GB | 512F392C78A9 | 502   | 0     | 1.38   |
| WDC       | CL SN720 SDAQNT... | 1 TB   | 7D39515D9658 | 501   | 0     | 1.38   |
| Samsung   | SSD 970 PRO        | 512 GB | E94A87E0E736 | 501   | 0     | 1.37   |
| Dell      | Express Flash N... | 1.6 TB | B2A958340308 | 501   | 0     | 1.37   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 34D8248DBD5B | 501   | 0     | 1.37   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 507669984B4F | 501   | 0     | 1.37   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | C36B78CC69B3 | 501   | 0     | 1.37   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 4613669E727F | 500   | 0     | 1.37   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 5ABC88BBD9B7 | 500   | 0     | 1.37   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 8A8B84CC9004 | 500   | 0     | 1.37   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | DEA689AB7308 | 500   | 0     | 1.37   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 9F11732CE922 | 500   | 0     | 1.37   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 3E119950D361 | 500   | 0     | 1.37   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 44B20C61AD9E | 500   | 0     | 1.37   |
| Intel     | SSDPE2KX010T8      | 1 TB   | 2F13E0CC22E6 | 500   | 0     | 1.37   |
| WDC       | CL SN720 SDAQNT... | 1 TB   | 2F38F6B4024B | 500   | 0     | 1.37   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | CC151E0E2E39 | 499   | 0     | 1.37   |
| WDC       | CL SN720 SDAQNT... | 1 TB   | A3A1D67A3F24 | 499   | 0     | 1.37   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 2293F586CE8A | 499   | 0     | 1.37   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 3AE409409462 | 499   | 0     | 1.37   |
| Samsung   | SSD 970 EVO Plus   | 1 TB   | F8D1BDC2FDB2 | 499   | 0     | 1.37   |
| WDC       | WDS500G3X0C-00SJG0 | 500 GB | 7ED5390D338C | 499   | 0     | 1.37   |
| Samsung   | SSD 970 EVO Plus   | 1 TB   | 51CE2E4E5015 | 498   | 0     | 1.37   |
| Toshiba   | THNSN5512GPU7      | 512 GB | 949C832AEAF6 | 498   | 0     | 1.37   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | FCDBBA09892D | 498   | 0     | 1.37   |
| WDC       | CL SN720 SDAQNT... | 1 TB   | E910509B6E46 | 498   | 0     | 1.37   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 0C4C8812619A | 498   | 0     | 1.36   |
| WDC       | WDS500G3X0C-00SJG0 | 500 GB | FBF0B5B3711E | 497   | 0     | 1.36   |
| Intel     | SSDPEDME016T4S     | 1.6 TB | 2E88302B6607 | 497   | 0     | 1.36   |
| Dell      | Express Flash C... | 960 GB | 68836870CCEE | 497   | 0     | 1.36   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 781FF8DFBC9C | 497   | 0     | 1.36   |
| Intel     | SSDPE2KX010T8      | 1 TB   | 79AD670B5F68 | 496   | 0     | 1.36   |
| Intel     | SSDPE2KX010T8      | 1 TB   | 1135958296A6 | 496   | 0     | 1.36   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 2DEA6852EB97 | 495   | 0     | 1.36   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 70B9153F0B12 | 495   | 0     | 1.36   |
| Intel     | SSDPE21K750GA      | 752 GB | 3BC00BE0F350 | 495   | 0     | 1.36   |
| Intel     | SSDPE21K750GA      | 752 GB | 55E9D24CBD62 | 495   | 0     | 1.36   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 03E2F139D038 | 495   | 0     | 1.36   |
| WDC       | CL SN720 SDAQNT... | 1 TB   | 18FF630FC40E | 495   | 0     | 1.36   |
| Intel     | SSDPED1K750GA      | 752 GB | DA3D69601224 | 495   | 0     | 1.36   |
| Intel     | SSDPED1K750GA      | 752 GB | 19BA693A3433 | 495   | 0     | 1.36   |
| Intel     | SSDPE2KX040T7      | 4 TB   | 0FAEFE72E070 | 495   | 0     | 1.36   |
| WDC       | CL SN720 SDAQNT... | 1 TB   | F06A4C01B073 | 494   | 0     | 1.36   |
| Samsung   | MZQLB3T8HALS-000AZ | 3.8 TB | BABF5BB0581E | 494   | 0     | 1.36   |
| Intel     | SSDPE2KX010T8      | 1 TB   | F2451316BBE7 | 494   | 0     | 1.36   |
| Samsung   | MZQLB3T8HALS-000AZ | 3.8 TB | B0E3D06E07EA | 494   | 0     | 1.36   |
| Samsung   | MZQLB3T8HALS-000AZ | 3.8 TB | 209215E4954B | 494   | 0     | 1.35   |
| Samsung   | MZQLB3T8HALS-000AZ | 3.8 TB | 336ECD0015FE | 494   | 0     | 1.35   |
| Samsung   | MZQLB3T8HALS-000AZ | 3.8 TB | 6A0FDD384188 | 494   | 0     | 1.35   |
| Samsung   | MZQLB3T8HALS-000AZ | 3.8 TB | 6A6E7BAC082D | 494   | 0     | 1.35   |
| Samsung   | MZQLB3T8HALS-000AZ | 3.8 TB | 7275A66235F2 | 494   | 0     | 1.35   |
| Samsung   | MZQLB3T8HALS-000AZ | 3.8 TB | C0228DF1FA2F | 494   | 0     | 1.35   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 1BA564C8B2B2 | 494   | 0     | 1.35   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | C871CE121A2B | 494   | 0     | 1.35   |
| Toshiba   | KXD51RUE960G       | 960 GB | 9A5904E9DF7C | 493   | 0     | 1.35   |
| Toshiba   | KXD51RUE960G       | 960 GB | B0D7A66A41D2 | 493   | 0     | 1.35   |
| Intel     | SSDPED1K750GA      | 752 GB | 6C95D0944C3C | 493   | 0     | 1.35   |
| Smartbuy  | m.2 PS5013T-2280T  | 128 GB | 3EA2C44651DE | 493   | 0     | 1.35   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 027DBD958AEA | 493   | 0     | 1.35   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 0B5C169B30B1 | 493   | 0     | 1.35   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 0F5AD9D8DB6F | 493   | 0     | 1.35   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 3D8EAC436351 | 493   | 0     | 1.35   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 89AD76504AB3 | 493   | 0     | 1.35   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | C35C95028507 | 493   | 0     | 1.35   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | D3BD96F8660C | 493   | 0     | 1.35   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | D605D0490F1D | 493   | 0     | 1.35   |
| Intel     | SSDPE2KX010T8      | 1 TB   | 4F63EDEFFFE1 | 492   | 0     | 1.35   |
| WDC       | WUS3BA196C7P3E3    | 960 GB | A9C98BAA2216 | 492   | 0     | 1.35   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 25A6E2440BE7 | 492   | 0     | 1.35   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 7DBF40DCC544 | 492   | 0     | 1.35   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 938DCCF9E0B2 | 492   | 0     | 1.35   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 0DE4108ADBBA | 492   | 0     | 1.35   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 659C8A515AEC | 492   | 0     | 1.35   |
| WDC       | WDS500G3X0C-00SJG0 | 500 GB | 62814AD4D5E8 | 491   | 0     | 1.35   |
| Intel     | SSDPE21K750GA      | 752 GB | 2E93ED6719FA | 491   | 0     | 1.35   |
| Intel     | SSDPE21K750GA      | 752 GB | AA5EF3736199 | 491   | 0     | 1.35   |
| Intel     | SSDPE21K750GA      | 752 GB | AB59544486A8 | 491   | 0     | 1.35   |
| Intel     | SSDPE21K750GA      | 752 GB | CB5C190CBB1B | 491   | 0     | 1.35   |
| Intel     | SSDPE2KX010T8      | 1 TB   | 4161C7804ADC | 491   | 0     | 1.35   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | BCB451AD261F | 491   | 0     | 1.35   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | FACBDCA61636 | 491   | 0     | 1.35   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 4EAB8F2CB57E | 491   | 0     | 1.35   |
| Toshiba   | KXG60ZNV512G       | 512 GB | E5276E5C5E02 | 490   | 0     | 1.34   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 0F9D8097E85A | 490   | 0     | 1.34   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 53F07AAAF9CC | 490   | 0     | 1.34   |
| Samsung   | MZ1LB960HAJQ-00007 | 960 GB | 23C218E9738F | 490   | 0     | 1.34   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 19138FFEF40E | 490   | 0     | 1.34   |
| Wester... | WUS3BA196C7P3E3    | 800 GB | 7C8B5D98C28A | 490   | 0     | 1.34   |
| Wester... | WUS3BA196C7P3E3    | 800 GB | 9713A6FE5B4E | 490   | 0     | 1.34   |
| Wester... | WUS3BA196C7P3E3    | 800 GB | CBA8A3E5AA0F | 490   | 0     | 1.34   |
| Wester... | WUS3BA196C7P3E3    | 800 GB | CF9FCAFA2455 | 490   | 0     | 1.34   |
| Wester... | WUS3BA196C7P3E3    | 800 GB | 2881DDB73C6E | 490   | 0     | 1.34   |
| Wester... | WUS3BA196C7P3E3    | 800 GB | 2885CE10F94D | 490   | 0     | 1.34   |
| Wester... | WUS3BA196C7P3E3    | 800 GB | 4D73C5133F92 | 490   | 0     | 1.34   |
| Wester... | WUS3BA196C7P3E3    | 800 GB | 56CCAD1C7155 | 490   | 0     | 1.34   |
| Wester... | WUS3BA196C7P3E3    | 800 GB | 59D030F292B0 | 490   | 0     | 1.34   |
| Wester... | WUS3BA196C7P3E3    | 800 GB | 78EA71B21412 | 490   | 0     | 1.34   |
| Wester... | WUS3BA196C7P3E3    | 800 GB | 8082AAB9A643 | 490   | 0     | 1.34   |
| Wester... | WUS3BA196C7P3E3    | 800 GB | 97E00B8645B6 | 490   | 0     | 1.34   |
| Wester... | WUS3BA196C7P3E3    | 800 GB | 9D1C4425AC2C | 490   | 0     | 1.34   |
| Wester... | WUS3BA196C7P3E3    | 800 GB | B41B94C22814 | 490   | 0     | 1.34   |
| Wester... | WUS3BA196C7P3E3    | 800 GB | C8FED79351C7 | 490   | 0     | 1.34   |
| Wester... | WUS3BA196C7P3E3    | 800 GB | EA8847015051 | 490   | 0     | 1.34   |
| Wester... | WUS3BA196C7P3E3    | 800 GB | EDCC3A641370 | 490   | 0     | 1.34   |
| Wester... | WUS3BA196C7P3E3    | 800 GB | EF0E989423A6 | 490   | 0     | 1.34   |
| Wester... | WUS3BA196C7P3E3    | 800 GB | F73A62CAE486 | 490   | 0     | 1.34   |
| WDC       | CL SN720 SDAQNT... | 1 TB   | 36CFFEC0FFDD | 490   | 0     | 1.34   |
| Samsung   | SSD 960 PRO        | 512 GB | 65F77290FBF0 | 489   | 0     | 1.34   |
| WDC       | WUS3BA138C7P3E3    | 3.8 TB | 6F8630A0213D | 489   | 0     | 1.34   |
| Samsung   | SSD 970 EVO Plus   | 1 TB   | 265E3F0F6A76 | 488   | 0     | 1.34   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 45D77A9C9B11 | 488   | 0     | 1.34   |
| WDC       | WUS3BA196C7P3E3    | 960 GB | 2278F4545132 | 488   | 0     | 1.34   |
| WDC       | WUS3BA196C7P3E3    | 960 GB | BB20F93A7283 | 488   | 0     | 1.34   |
| Dell      | Express Flash C... | 960 GB | 1FE6BBD72232 | 488   | 0     | 1.34   |
| Samsung   | MZ1LB960HAJQ-00007 | 960 GB | EF0887557EC5 | 488   | 0     | 1.34   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 2D3BCD25ACC9 | 488   | 0     | 1.34   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 4B7A1E7F9301 | 488   | 0     | 1.34   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | CB4D7376F7EB | 488   | 0     | 1.34   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 96E52B17DDEE | 488   | 0     | 1.34   |
| Intel     | SSDPE2KX010T8      | 1 TB   | 45DFD7C48998 | 488   | 0     | 1.34   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 23A154661840 | 487   | 0     | 1.34   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 47CC9C486586 | 487   | 0     | 1.34   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | CBA4D3A6CB87 | 487   | 0     | 1.34   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | BF9642EAA131 | 487   | 0     | 1.34   |
| Dell      | Express Flash N... | 1.6 TB | 73412CF01A9A | 487   | 0     | 1.33   |
| Dell      | Express Flash N... | 1.6 TB | F0225AAC7F77 | 487   | 0     | 1.33   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | C15458E6A8DB | 487   | 0     | 1.33   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 8E61458CAE5F | 486   | 0     | 1.33   |
| Intel     | SSDPE2KX020T8      | 2 TB   | EE19226BD1CC | 486   | 0     | 1.33   |
| Intel     | SSDPED1K750GA      | 752 GB | AF483D04722C | 486   | 0     | 1.33   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 8245278067C7 | 486   | 0     | 1.33   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | A414269227E9 | 486   | 0     | 1.33   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | E7CF90A55AC8 | 486   | 0     | 1.33   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 8901B8C8CD34 | 485   | 0     | 1.33   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | CA4602308DC9 | 485   | 0     | 1.33   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 4385FEBD9766 | 485   | 0     | 1.33   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | C41D76DF11EA | 485   | 0     | 1.33   |
| Intel     | SSDPE2KX010T8      | 1 TB   | 69FD5440B8E5 | 485   | 0     | 1.33   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 44FC2A4664D1 | 484   | 0     | 1.33   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 7F02C8F88C2E | 484   | 0     | 1.33   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | B09C917C6428 | 484   | 0     | 1.33   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | D36B49B90449 | 484   | 0     | 1.33   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 754B381F0FB6 | 484   | 0     | 1.33   |
| Intel     | SSDPE2KX020T8      | 2 TB   | BCAAECF45ED0 | 484   | 0     | 1.33   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | FB3AB8FE5E4F | 484   | 0     | 1.33   |
| Dell      | Express Flash N... | 1.6 TB | 5F8C55AE6A51 | 484   | 0     | 1.33   |
| Dell      | Express Flash N... | 1.6 TB | AE475C29B0E6 | 484   | 0     | 1.33   |
| Dell      | Express Flash N... | 1.6 TB | F02BC989E71F | 484   | 0     | 1.33   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 006F2B072480 | 483   | 0     | 1.33   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 006F2B072480 | 483   | 0     | 1.33   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | BCCDF4678C9A | 483   | 0     | 1.32   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | E68B97AFD34E | 483   | 0     | 1.32   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 88AF3D280292 | 482   | 0     | 1.32   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 0CD889EE4A76 | 482   | 0     | 1.32   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 88AF3D280292 | 482   | 0     | 1.32   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | BF0F98FCEA31 | 482   | 0     | 1.32   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 2798DF3922DF | 482   | 0     | 1.32   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 55509F8E9D55 | 482   | 0     | 1.32   |
| Toshiba   | KXG60ZNV1T02       | 1 TB   | 3D3AD08E57E3 | 481   | 0     | 1.32   |
| Toshiba   | KXG60ZNV1T02       | 1 TB   | 55CC79C29072 | 481   | 0     | 1.32   |
| Toshiba   | KXG60ZNV1T02       | 1 TB   | 875EE1015D9B | 481   | 0     | 1.32   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | B8D0CA3324BD | 481   | 0     | 1.32   |
| Intel     | SSDPEKKW512G8      | 512 GB | D98A11256BCC | 481   | 0     | 1.32   |
| Samsung   | MZPLL1T6HAJQ-00005 | 1.6 TB | C61DC03936F3 | 481   | 0     | 1.32   |
| Samsung   | MZPLL1T6HAJQ-00005 | 1.6 TB | FC19C19F4607 | 481   | 0     | 1.32   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | FE9ABA25C744 | 481   | 0     | 1.32   |
| WDC       | CL SN720 SDAQNT... | 512 GB | C0C2B11A1B72 | 481   | 0     | 1.32   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 7F313818A123 | 480   | 0     | 1.32   |
| Samsung   | SSD 970 EVO Plus   | 1 TB   | 27DB48899890 | 480   | 0     | 1.32   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 5406FD7086BD | 480   | 0     | 1.32   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | A8745A052DA0 | 480   | 0     | 1.32   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | EA3FF5627FB2 | 480   | 0     | 1.32   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 075ABF7A5E9B | 480   | 0     | 1.32   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 436FF10CE709 | 480   | 0     | 1.32   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 5ED05AAE9304 | 480   | 0     | 1.32   |
| Intel     | SSDPE2KX020T8      | 2 TB   | EBEFE6C84B92 | 480   | 0     | 1.32   |
| Intel     | SSDPE2KX020T8      | 2 TB   | D28F70E1EAD7 | 480   | 0     | 1.32   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 3CF0AFFE36D3 | 479   | 0     | 1.31   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 9F4DE5327E2B | 479   | 0     | 1.31   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | AD9FCB456CDF | 479   | 0     | 1.31   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | F58946AB41E9 | 479   | 0     | 1.31   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 8E6CE1859709 | 479   | 0     | 1.31   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 253912976FFD | 479   | 0     | 1.31   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 380526E487DD | 479   | 0     | 1.31   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 41CE42317BC2 | 479   | 0     | 1.31   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 6D2D0441F156 | 479   | 0     | 1.31   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 959A72490011 | 479   | 0     | 1.31   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | B42EF80ADC34 | 479   | 0     | 1.31   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | BF171897D803 | 479   | 0     | 1.31   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | EFF535E288AF | 479   | 0     | 1.31   |
| Dell      | Express Flash N... | 1.6 TB | 1E9E9F67DE74 | 478   | 0     | 1.31   |
| Dell      | Express Flash N... | 1.6 TB | DF42BA3915AE | 478   | 0     | 1.31   |
| WDC       | WUS3BA138C7P3E3    | 3.8 TB | 850BA24CEE34 | 477   | 0     | 1.31   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | BDBD7E6A4FCC | 477   | 0     | 1.31   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | D865A87894BF | 477   | 0     | 1.31   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 4276F393B7FE | 477   | 0     | 1.31   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 7175E2FD8D5B | 477   | 0     | 1.31   |
| Intel     | SSDPED1K375GA      | 375 GB | 91CFC927ACB8 | 477   | 0     | 1.31   |
| WDC       | WUS3BA138C7P3E3    | 3.8 TB | 49A3AEC57276 | 476   | 0     | 1.31   |
| Corsair   | Force MP510 1.9TB  | 1.9 TB | 513F2E9BE5B9 | 476   | 0     | 1.31   |
| Samsung   | SSD 970 EVO Plus   | 1 TB   | 66EF1098FE93 | 476   | 0     | 1.31   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 84CC48CBEDCA | 476   | 0     | 1.30   |
| WDC       | WUS3BA196C7P3E3    | 960 GB | 631B0679179F | 475   | 0     | 1.30   |
| Wester... | WUS3BA196C7P3E3    | 960 GB | 631B0679179F | 475   | 0     | 1.30   |
| Samsung   | MZ1LB960HAJQ-00007 | 960 GB | F1D8B6944386 | 475   | 0     | 1.30   |
| WDC       | CL SN720 SDAQNT... | 512 GB | 6C95C5E220F1 | 475   | 0     | 1.30   |
| Kingston  | SKC2000M81000G     | 1 TB   | FA12066D9CA6 | 475   | 0     | 1.30   |
| WDC       | WUS3BA196C7P3E3    | 960 GB | 26F88B2D884F | 475   | 0     | 1.30   |
| WDC       | WUS3BA196C7P3E3    | 960 GB | 5E1327AFC457 | 475   | 0     | 1.30   |
| WDC       | WUS3BA196C7P3E3    | 960 GB | 70D5A0C9C33B | 475   | 0     | 1.30   |
| WDC       | WUS3BA196C7P3E3    | 960 GB | 93FDFA50DBAA | 475   | 0     | 1.30   |
| WDC       | WUS3BA196C7P3E3    | 960 GB | 9AE9B362ECC4 | 475   | 0     | 1.30   |
| WDC       | WUS3BA196C7P3E3    | 960 GB | A06BD6DFE2ED | 475   | 0     | 1.30   |
| WDC       | WUS3BA196C7P3E3    | 960 GB | CBB0D491614B | 475   | 0     | 1.30   |
| WDC       | WUS3BA196C7P3E3    | 960 GB | D0E34B544D01 | 475   | 0     | 1.30   |
| WDC       | WUS3BA196C7P3E3    | 960 GB | D3B5A272DE1E | 475   | 0     | 1.30   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | E1A63B111AF3 | 475   | 0     | 1.30   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 397F813F71DB | 475   | 0     | 1.30   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | E1A63B111AF3 | 475   | 0     | 1.30   |
| Dell      | Express Flash N... | 1.6 TB | 78CD990CA994 | 474   | 0     | 1.30   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | BAA0B7E4725D | 474   | 0     | 1.30   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 719EED3C91BA | 474   | 0     | 1.30   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | BAA0B7E4725D | 474   | 0     | 1.30   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 4091604B3E6D | 474   | 0     | 1.30   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | F317C9E82058 | 474   | 0     | 1.30   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 69A4DF5D817F | 474   | 0     | 1.30   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 69A4DF5D817F | 474   | 0     | 1.30   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 9A1478928F61 | 474   | 0     | 1.30   |
| Intel     | SSDPED1D280GA      | 280 GB | DEEE3F280B80 | 474   | 0     | 1.30   |
| Micron    | 9300_MTFDHAL3T2TDR | 3.2 TB | 1BF4B5DD3585 | 473   | 0     | 1.30   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 7B0DA37140BE | 473   | 0     | 1.30   |
| Corsair   | Force MP510 1.9TB  | 1.9 TB | 352B227A37B2 | 473   | 0     | 1.30   |
| Dell      | Express Flash N... | 1.6 TB | DC6598242902 | 473   | 0     | 1.30   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 459DCE9712C4 | 473   | 0     | 1.30   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | D95CD09B5D51 | 473   | 0     | 1.30   |
| Samsung   | MZPLL6T4HMLS-000MV | 6.4 TB | 72A255AF88C8 | 472   | 0     | 1.30   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 2D3FF219BD5B | 472   | 0     | 1.30   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 93A3D170E031 | 472   | 0     | 1.30   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 334A481BE6D6 | 472   | 0     | 1.30   |
| Samsung   | MZVLB512HAJQ-00000 | 512 GB | 5C4EE66ACD4C | 472   | 0     | 1.29   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 3526673AF20B | 472   | 0     | 1.29   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 99652E3778FB | 472   | 0     | 1.29   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 2549C818780E | 472   | 0     | 1.29   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | B929CCF1B7D7 | 472   | 0     | 1.29   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 1854CB75A64F | 471   | 0     | 1.29   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 95ABDC5D0E4F | 471   | 0     | 1.29   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | B2503C760509 | 471   | 0     | 1.29   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | CB45F8ECF27A | 471   | 0     | 1.29   |
| Micron    | 9300_MTFDHAL3T2TDR | 3.2 TB | 1B7B15211B4F | 471   | 0     | 1.29   |
| Dell      | Express Flash N... | 1.6 TB | 9E2DFF022337 | 471   | 0     | 1.29   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 95F2CAA96182 | 470   | 0     | 1.29   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 856446D08344 | 470   | 0     | 1.29   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 9C858EBE0AFA | 470   | 0     | 1.29   |
| Intel     | SSDPE2KX010T8      | 1 TB   | DD44821C47D0 | 470   | 0     | 1.29   |
| Dell      | Express Flash N... | 1.6 TB | 6BB41DA1A7B7 | 470   | 0     | 1.29   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 7B548139ABEF | 470   | 0     | 1.29   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | C9F72B51B939 | 470   | 0     | 1.29   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | DF33A48C8AE3 | 469   | 0     | 1.29   |
| Samsung   | SSD 970 PRO        | 1 TB   | 0EC0035E8F5D | 469   | 0     | 1.29   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 7AEA354E6C73 | 469   | 0     | 1.29   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 4788E1D2A0C6 | 468   | 0     | 1.28   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 4788E1D2A0C6 | 468   | 0     | 1.28   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 9139E0470B02 | 468   | 0     | 1.28   |
| Intel     | SSDPED1D480GA      | 480 GB | 2BC0654304E8 | 468   | 0     | 1.28   |
| Dell      | Express Flash N... | 1.6 TB | D1CCEF826D77 | 467   | 0     | 1.28   |
| Samsung   | MZ1LB1T9HALS-00007 | 1.9 TB | 934B029158C7 | 467   | 0     | 1.28   |
| Samsung   | MZ1LB960HAJQ-00007 | 960 GB | 56F80947A146 | 467   | 0     | 1.28   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 669506FEDEB0 | 467   | 0     | 1.28   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 669506FEDEB0 | 467   | 0     | 1.28   |
| Dell      | Express Flash C... | 960 GB | 38BC02C1A622 | 467   | 0     | 1.28   |
| Samsung   | MZQLW1T9HMJP-00003 | 1.9 TB | 5810512EE7F4 | 467   | 0     | 1.28   |
| Dell      | Express Flash N... | 1.6 TB | 32C27E8941BA | 467   | 0     | 1.28   |
| Intel     | SSDPED1D960GAY     | 960 GB | EDDBDD8C9D47 | 467   | 0     | 1.28   |
| Samsung   | SSD 970 EVO Plus   | 1 TB   | 4E0BB03D09C9 | 467   | 0     | 1.28   |
| Intel     | SSDPE2KX010T8      | 1 TB   | AF7E0AB9FEF1 | 466   | 0     | 1.28   |
| Dell      | Express Flash C... | 960 GB | 1E9031534E1A | 466   | 0     | 1.28   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 09F1A3300BAE | 466   | 0     | 1.28   |
| Intel     | SSDPE21K750GA      | 752 GB | 9D64CF7ECB8F | 466   | 0     | 1.28   |
| Intel     | SSDPE2KX010T8      | 1 TB   | F22615C03E6A | 465   | 0     | 1.28   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 16876CD27FEF | 465   | 0     | 1.27   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 5F1998D4C59B | 465   | 0     | 1.27   |
| Intel     | SSDPE2KE032T8      | 3.2 TB | A3612F61CBA8 | 465   | 0     | 1.27   |
| Samsung   | MZ1LB960HAJQ-00007 | 960 GB | 0BBCE0EF2129 | 465   | 0     | 1.27   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 2E5293FF9DC5 | 465   | 0     | 1.27   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 37B4C19DAACB | 465   | 0     | 1.27   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | B488358DFE95 | 465   | 0     | 1.27   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | C86375304B2B | 465   | 0     | 1.27   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | A044250C36E2 | 464   | 0     | 1.27   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | F46EF09C1F21 | 464   | 0     | 1.27   |
| Dell      | Express Flash N... | 1.6 TB | 3C232A6C55F6 | 464   | 0     | 1.27   |
| Dell      | Express Flash N... | 1.6 TB | D9C054216803 | 464   | 0     | 1.27   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | 900EF2310B01 | 464   | 0     | 1.27   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 70DB09C8A242 | 464   | 0     | 1.27   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 05BB039B55FA | 463   | 0     | 1.27   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 05BB039B55FA | 463   | 0     | 1.27   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | B67117E5D3FB | 463   | 0     | 1.27   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | EA889B51D39B | 463   | 0     | 1.27   |
| WDC       | WUS3BA196C7P3E3    | 960 GB | A30D64B422F1 | 463   | 0     | 1.27   |
| Wester... | WUS3BA196C7P3E3    | 960 GB | A30D64B422F1 | 463   | 0     | 1.27   |
| Samsung   | MZPJB480HMGC-0BW07 | 480 GB | 13CF846E9ABC | 463   | 0     | 1.27   |
| Intel     | SSDPE2KE016T8      | 1.6 TB | 001AA449951C | 463   | 0     | 1.27   |
| Intel     | SSDPE2KE016T8      | 1.6 TB | 753ADF198DC4 | 463   | 0     | 1.27   |
| Samsung   | SSD 970 PRO        | 1 TB   | D273406D6DE3 | 462   | 0     | 1.27   |
| WDC       | WUS3BA196C7P3E3    | 960 GB | E90D44BA4024 | 462   | 0     | 1.27   |
| Wester... | WUS3BA196C7P3E3    | 960 GB | E90D44BA4024 | 462   | 0     | 1.27   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 134205A9D736 | 462   | 0     | 1.27   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 39351657609E | 462   | 0     | 1.27   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | B930DB98217A | 462   | 0     | 1.27   |
| Toshiba   | KXG60ZNV512G       | 512 GB | 1228DE8957CC | 461   | 0     | 1.26   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 6F86FA523E54 | 461   | 0     | 1.26   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 897DD3D66245 | 461   | 0     | 1.26   |
| Dell      | Express Flash N... | 1.6 TB | ADCCE88B8C15 | 461   | 0     | 1.26   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 856DE1FB4B26 | 461   | 0     | 1.26   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 7706AF7A1F1A | 460   | 0     | 1.26   |
| Dell      | Express Flash N... | 1.6 TB | 722EE7E771DB | 460   | 0     | 1.26   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 465CD1EC429E | 459   | 0     | 1.26   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | BD26EF9461D2 | 459   | 0     | 1.26   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 465CD1EC429E | 459   | 0     | 1.26   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | BD26EF9461D2 | 459   | 0     | 1.26   |
| Intel     | SSDPE2KX020T8      | 2 TB   | DD0717FC603C | 459   | 0     | 1.26   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 671BB2861C17 | 459   | 0     | 1.26   |
| Toshiba   | KXG60ZNV1T02       | 1 TB   | 70FC5C6B15C0 | 458   | 0     | 1.26   |
| Samsung   | MZVLB1T0HALR-00000 | 1 TB   | A29800206B06 | 458   | 0     | 1.26   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 4611C6DB1B03 | 458   | 0     | 1.26   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 56334F9141FB | 458   | 0     | 1.26   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | E90F2DE12E6E | 458   | 0     | 1.26   |
| Intel     | SSDPED1D960GAY     | 960 GB | FB1276A18769 | 457   | 0     | 1.25   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 8119BB499572 | 457   | 0     | 1.25   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 0524473379A6 | 457   | 0     | 1.25   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | D0597A8FFF9A | 457   | 0     | 1.25   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | D0597A8FFF9A | 457   | 0     | 1.25   |
| Intel     | SSDPEKKW512G8      | 512 GB | 2D89B94B30B0 | 457   | 0     | 1.25   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | A63CC36B02F6 | 457   | 0     | 1.25   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | BE8852352A59 | 457   | 0     | 1.25   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 69F511930A4C | 457   | 0     | 1.25   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | A63CC36B02F6 | 457   | 0     | 1.25   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | BE8852352A59 | 457   | 0     | 1.25   |
| Intel     | SSDPEDMD400G4      | 400 GB | B587BC0CC641 | 457   | 0     | 1.25   |
| Samsung   | MZPLL6T4HMLS-00003 | 6.4 TB | 837FE2BC56AC | 456   | 0     | 1.25   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 679648C2ABF2 | 456   | 0     | 1.25   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | C6347B1DD388 | 456   | 0     | 1.25   |
| Intel     | SSDPED1D960GAY     | 960 GB | 95CFD2E55167 | 456   | 0     | 1.25   |
| Intel     | SSDPE2KX020T8      | 2 TB   | F754BA8AEF00 | 455   | 0     | 1.25   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 10CDEA0D0C04 | 455   | 0     | 1.25   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 274F9FE764DD | 455   | 0     | 1.25   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 3596C34377B2 | 455   | 0     | 1.25   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 40F0867104D5 | 455   | 0     | 1.25   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 746C0462DA70 | 455   | 0     | 1.25   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 74B394DDA9C2 | 455   | 0     | 1.25   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 7682020DBB0D | 455   | 0     | 1.25   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 85C6853C592B | 455   | 0     | 1.25   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 8848107F7707 | 455   | 0     | 1.25   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 93484817D6F3 | 455   | 0     | 1.25   |
| Intel     | SSDPE2KX020T8      | 2 TB   | A6C07EADC080 | 455   | 0     | 1.25   |
| Intel     | SSDPE2KX020T8      | 2 TB   | ADF40FBB54AF | 455   | 0     | 1.25   |
| Intel     | SSDPE2KX020T8      | 2 TB   | B86663AB4944 | 455   | 0     | 1.25   |
| Intel     | SSDPE2KX020T8      | 2 TB   | B8C5BDD93E5F | 455   | 0     | 1.25   |
| Intel     | SSDPE2KX020T8      | 2 TB   | BB22DBDB8DBD | 455   | 0     | 1.25   |
| Intel     | SSDPE2KX020T8      | 2 TB   | C5028410518B | 455   | 0     | 1.25   |
| Intel     | SSDPE2KX020T8      | 2 TB   | D563E15ADD2C | 455   | 0     | 1.25   |
| Intel     | SSDPE2KX020T8      | 2 TB   | D6F9A9C1AE8B | 455   | 0     | 1.25   |
| Intel     | SSDPE2KX020T8      | 2 TB   | E695125FE9D6 | 455   | 0     | 1.25   |
| Corsair   | Force MP510 1.9TB  | 1.9 TB | DC7A786E26C8 | 455   | 0     | 1.25   |
| Toshiba   | KXD51RUE960G       | 960 GB | 051C5753E7C7 | 455   | 0     | 1.25   |
| Samsung   | MZPLL3T2HAJQ-00005 | 3.2 TB | 4A7C3081444D | 455   | 0     | 1.25   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 58CC3C9D2D51 | 454   | 0     | 1.25   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | D28676489616 | 454   | 0     | 1.25   |
| Intel     | SSDPED1D960GAY     | 960 GB | E28CC91E9425 | 454   | 0     | 1.25   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 983FD118CE7C | 454   | 0     | 1.24   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 0734905E4A42 | 454   | 0     | 1.24   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 2F2FD2B0137D | 454   | 0     | 1.24   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 56906D8020A8 | 454   | 0     | 1.24   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 5ADE2EC0839A | 454   | 0     | 1.24   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 82F82C5F8726 | 454   | 0     | 1.24   |
| Intel     | SSDPE2KX020T8      | 2 TB   | A6244338B07B | 454   | 0     | 1.24   |
| Intel     | SSDPE2KX020T8      | 2 TB   | A7FDF97F853B | 454   | 0     | 1.24   |
| Intel     | SSDPE2KX020T8      | 2 TB   | B13EBAA80EA9 | 454   | 0     | 1.24   |
| Intel     | SSDPE2KX020T8      | 2 TB   | BB70948EA805 | 454   | 0     | 1.24   |
| Intel     | SSDPE2KX020T8      | 2 TB   | CFC0051AF678 | 454   | 0     | 1.24   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 0172B5F31E80 | 454   | 0     | 1.24   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 09A1775E20AE | 454   | 0     | 1.24   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 564B36835D5A | 454   | 0     | 1.24   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 6194189A233E | 454   | 0     | 1.24   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 64EECD264F62 | 454   | 0     | 1.24   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 6D2507007B9C | 454   | 0     | 1.24   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 7D1FB2B4E06B | 454   | 0     | 1.24   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 8D686CE14CB4 | 454   | 0     | 1.24   |
| Intel     | SSDPE2KX020T8      | 2 TB   | F2BA646EA850 | 454   | 0     | 1.24   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 168848062747 | 454   | 0     | 1.24   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 8FFBADF23981 | 454   | 0     | 1.24   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 675FFB36EE34 | 454   | 0     | 1.24   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 4095BEF4872D | 453   | 0     | 1.24   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | C7626E6DAAE8 | 453   | 0     | 1.24   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 133D5B56D0FA | 453   | 0     | 1.24   |
| Intel     | SSDPE21K750GA      | 752 GB | 8540D85590A2 | 452   | 0     | 1.24   |
| Samsung   | SSD 970 EVO Plus   | 500 GB | 7CAB86A227AC | 452   | 0     | 1.24   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 5BF83FBA7C8B | 452   | 0     | 1.24   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | 046EA5BE653D | 451   | 0     | 1.24   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | AB19017AB4A5 | 451   | 0     | 1.24   |
| Intel     | SSDPE2KE032T8      | 3.2 TB | 8BE41912835F | 451   | 0     | 1.24   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 9AFCD5D893D7 | 450   | 0     | 1.24   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 9AFCD5D893D7 | 450   | 0     | 1.24   |
| WDC       | WUS3BA196C7P3E3    | 960 GB | BEB6D917DBD0 | 450   | 0     | 1.23   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 5288006FD32C | 450   | 0     | 1.23   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 8409B0BCB1C2 | 450   | 0     | 1.23   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 5288006FD32C | 450   | 0     | 1.23   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 8409B0BCB1C2 | 450   | 0     | 1.23   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | F5D570948020 | 450   | 0     | 1.23   |
| Samsung   | SSD 970 PRO        | 512 GB | 3E7FC0652B59 | 450   | 0     | 1.23   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 5B718689BEBD | 450   | 0     | 1.23   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 7C4E46CF092C | 450   | 0     | 1.23   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 93616BA39091 | 450   | 0     | 1.23   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | A9B60E8029AD | 450   | 0     | 1.23   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | CA69F5D31F17 | 450   | 0     | 1.23   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | CAB0CD668989 | 450   | 0     | 1.23   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 64F5C4CEC3C9 | 449   | 0     | 1.23   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 0B9773E8A54A | 449   | 0     | 1.23   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | A3586A299A8D | 449   | 0     | 1.23   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | D28E1517ACF7 | 449   | 0     | 1.23   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 741CBF638AE4 | 449   | 0     | 1.23   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 7D2A1FF892A0 | 449   | 0     | 1.23   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | C57BAA98D16F | 449   | 0     | 1.23   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 98F1423D457F | 449   | 0     | 1.23   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | C4A375F811A4 | 449   | 0     | 1.23   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | B79C26FAF8C9 | 449   | 0     | 1.23   |
| Intel     | SSDPEDKX040T7      | 4 TB   | B3A11D144C9D | 448   | 0     | 1.23   |
| Samsung   | SSD 970 PRO        | 512 GB | 0DCC2D8AB935 | 448   | 0     | 1.23   |
| Toshiba   | KXG60ZNV1T02       | 1 TB   | 1796CB5AED9A | 448   | 0     | 1.23   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | E6B89D9B7C1F | 448   | 0     | 1.23   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | DE6DFFAAD59F | 448   | 0     | 1.23   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | F23F350EB250 | 448   | 0     | 1.23   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 5C867CC1CF65 | 447   | 0     | 1.23   |
| Intel     | SSDPE21K100GA      | 100 GB | AA611781C660 | 447   | 0     | 1.23   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 1A6DEB44096B | 447   | 0     | 1.23   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 3FB481963D62 | 447   | 0     | 1.23   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 7E0C421CFB57 | 447   | 0     | 1.23   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | C69748E05C8F | 447   | 0     | 1.23   |
| Intel     | SSDPE21K750GA      | 752 GB | FCD0BA18FF2A | 447   | 0     | 1.22   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 3AF549A3A9E3 | 447   | 0     | 1.22   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 4B9B93BABE47 | 447   | 0     | 1.22   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | A6F57104E67A | 447   | 0     | 1.22   |
| WDC       | CL SN720 SDAQNT... | 1 TB   | 9FABA2F75046 | 447   | 0     | 1.22   |
| Dell      | Express Flash P... | 800 GB | 3112E1391569 | 446   | 0     | 1.22   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 014D91363BB8 | 446   | 0     | 1.22   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 2793A357F3DD | 446   | 0     | 1.22   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 385D0F0C384A | 446   | 0     | 1.22   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 4611E640CFE2 | 446   | 0     | 1.22   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | C0667433CF15 | 446   | 0     | 1.22   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | C2B09B40D5E5 | 446   | 0     | 1.22   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | C7F1EFDB60B6 | 446   | 0     | 1.22   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | CF1141AD9283 | 446   | 0     | 1.22   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | D5C0B16D78AB | 446   | 0     | 1.22   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | EA91A2EC543B | 446   | 0     | 1.22   |
| Corsair   | Force MP510 1.9TB  | 1.9 TB | 82BD626D38C5 | 446   | 0     | 1.22   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | C42EB91792DE | 446   | 0     | 1.22   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 588C87CECB54 | 446   | 0     | 1.22   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | F85F1F8514E9 | 446   | 0     | 1.22   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 9F7F057406A0 | 446   | 0     | 1.22   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | D3A55AB7FA2B | 446   | 0     | 1.22   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | 5D0285C5C9F0 | 446   | 0     | 1.22   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | BEC3FA9DD95C | 446   | 0     | 1.22   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | B91B65AB162A | 445   | 0     | 1.22   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | BA2E0E8AEC4F | 445   | 0     | 1.22   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 8B3790B7AB47 | 445   | 0     | 1.22   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | CD1EC70D88DE | 445   | 0     | 1.22   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 7DE90D125D0D | 445   | 0     | 1.22   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 82B14471360F | 445   | 0     | 1.22   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | AEBD18D86F50 | 445   | 0     | 1.22   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | B335B7C7F72D | 445   | 0     | 1.22   |
| Samsung   | MZQLW1T9HMJP-000AZ | 1.9 TB | B40DB2D1F6B2 | 445   | 0     | 1.22   |
| Intel     | SSDPED1D280GA      | 280 GB | 2F60B15E2960 | 445   | 0     | 1.22   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | C2A0B4592719 | 445   | 0     | 1.22   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | A9D7D0C40DB5 | 444   | 0     | 1.22   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 1CA8854FF2AE | 444   | 0     | 1.22   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 57D44AD566D1 | 444   | 0     | 1.22   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 7BCC3B36C351 | 444   | 0     | 1.22   |
| Samsung   | SSD 970 PRO        | 1 TB   | 50CA8548414F | 444   | 0     | 1.22   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 1FAF31E3A7E2 | 444   | 0     | 1.22   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 99EB1FA4B649 | 444   | 0     | 1.22   |
| Intel     | SSDPED1D480GA      | 480 GB | 0BE3944B5AAA | 444   | 0     | 1.22   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 014EE23876FE | 444   | 0     | 1.22   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 1712839C974C | 444   | 0     | 1.22   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 56903949469A | 444   | 0     | 1.22   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 701576CCBD96 | 444   | 0     | 1.22   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | D158DEA4E7AA | 444   | 0     | 1.22   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | D1F77716C714 | 444   | 0     | 1.22   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 10D4A31F1E63 | 444   | 0     | 1.22   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 96D8BBC4944D | 444   | 0     | 1.22   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 36AF08DD0CF7 | 444   | 0     | 1.22   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 5A6F81638143 | 444   | 0     | 1.22   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 6DC82B3077E6 | 444   | 0     | 1.22   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | A490A3EC6145 | 444   | 0     | 1.22   |
| Intel     | SSDPE2KX010T8      | 1 TB   | 61055D6B80E3 | 444   | 0     | 1.22   |
| Intel     | SSDPE2KX010T8      | 1 TB   | 678463A3463D | 444   | 0     | 1.22   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 945064E113B0 | 443   | 0     | 1.22   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 73B62C6B2D31 | 443   | 0     | 1.22   |
| Dell      | Express Flash N... | 1.6 TB | ADCB897B296E | 443   | 0     | 1.22   |
| Dell      | Express Flash N... | 1.6 TB | BFAC36F17F06 | 443   | 0     | 1.22   |
| Samsung   | MZPLL6T4HMLS-000MV | 6.4 TB | E998751F2D53 | 443   | 0     | 1.22   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 20DEC9203A5B | 443   | 0     | 1.22   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 46F5E5E5F167 | 443   | 0     | 1.22   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 4C0046371AC5 | 443   | 0     | 1.22   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | B17953D97174 | 443   | 0     | 1.22   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | C9F660CAEB5E | 443   | 0     | 1.22   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | F70F0DFBBEFB | 443   | 0     | 1.22   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | CB39801AB10F | 443   | 0     | 1.21   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | D242846C3C4A | 443   | 0     | 1.21   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 015B485512FD | 443   | 0     | 1.21   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 607382813DD5 | 443   | 0     | 1.21   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 6B4A12F8C01A | 443   | 0     | 1.21   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | D42B2C418C46 | 443   | 0     | 1.21   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 4231CFF336E4 | 443   | 0     | 1.21   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 7E626729AC29 | 443   | 0     | 1.21   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | C0363174612A | 443   | 0     | 1.21   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | E61D3D249774 | 443   | 0     | 1.21   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 56930BBF5897 | 443   | 0     | 1.21   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | FB877ABCFAE0 | 443   | 0     | 1.21   |
| Intel     | SSDPE2KX010T8      | 1 TB   | E956740347ED | 443   | 0     | 1.21   |
| Intel     | SSDPEL1K100GA      | 100 GB | 9228297E6571 | 443   | 0     | 1.21   |
| Samsung   | SSD 970 EVO Plus   | 1 TB   | 5A4D64ADFEFE | 443   | 0     | 1.21   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 24BF88BA983C | 443   | 0     | 1.21   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 3EAEBA041BA5 | 442   | 0     | 1.21   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 5169EC331D2F | 442   | 0     | 1.21   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 888DAA16DC5B | 442   | 0     | 1.21   |
| WDC       | PC SN720 SDAPNT... | 256 GB | 5B3CB3E1B766 | 442   | 0     | 1.21   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 6E34D5120E27 | 442   | 0     | 1.21   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 7B5FCB6D3028 | 442   | 0     | 1.21   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 9E7902E542FC | 442   | 0     | 1.21   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 812F525291AC | 442   | 0     | 1.21   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 8F7B9A01B7AD | 442   | 0     | 1.21   |
| Intel     | SSDPE2KX010T8      | 1 TB   | 73A3EAA8EDA3 | 442   | 0     | 1.21   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 95658413C7B4 | 442   | 0     | 1.21   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | AB922323CF2A | 442   | 0     | 1.21   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | DEB4A496BDA7 | 442   | 0     | 1.21   |
| Dell      | Express Flash N... | 1.6 TB | 2B46F5957B89 | 441   | 0     | 1.21   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 1F51E05E39AF | 441   | 0     | 1.21   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | CE0078532630 | 441   | 0     | 1.21   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 61B92499A6E1 | 441   | 0     | 1.21   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 145D470EEAC2 | 441   | 0     | 1.21   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 22C2AA02A37B | 441   | 0     | 1.21   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 69BFAD101A05 | 441   | 0     | 1.21   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 95E1E1ED834D | 441   | 0     | 1.21   |
| Samsung   | MZPJB480HMGC-0BW07 | 480 GB | 2E232E01A675 | 441   | 0     | 1.21   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 222B89B7CFCF | 441   | 0     | 1.21   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 63355E05CBEB | 441   | 0     | 1.21   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | D88E7C67686E | 441   | 0     | 1.21   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | E78082FC9A58 | 441   | 0     | 1.21   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 77468BA81BCA | 441   | 0     | 1.21   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 84838FA94794 | 441   | 0     | 1.21   |
| WDC       | CL SN720 SDAQNT... | 1 TB   | 1758F0E49990 | 440   | 0     | 1.21   |
| WDC       | WUS3BA138C7P3E3    | 3.8 TB | CAD55484DE7F | 440   | 0     | 1.21   |
| Wester... | WUS3BA138C7P3E3    | 3.8 TB | CAD55484DE7F | 440   | 0     | 1.21   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 4B6F4F41BC76 | 440   | 0     | 1.21   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | B4937C0CB60F | 440   | 0     | 1.21   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | FBF3B70FA942 | 440   | 0     | 1.21   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 75512DC70FF4 | 440   | 0     | 1.21   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 02A4ACAEB5DD | 440   | 0     | 1.21   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 2539CBAA93FE | 440   | 0     | 1.21   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 3FBF5E194F64 | 440   | 0     | 1.21   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 6A416E154D85 | 440   | 0     | 1.21   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 921BE6AF33E5 | 440   | 0     | 1.21   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 980CEB7F8EEB | 440   | 0     | 1.21   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 9BD38B395038 | 440   | 0     | 1.21   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | A9C7E9715A55 | 440   | 0     | 1.21   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | D9A07C7437D0 | 440   | 0     | 1.21   |
| Intel     | SSDPE2KX010T8      | 1 TB   | 4F750F01E429 | 440   | 0     | 1.21   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 585BCEFB1C6B | 440   | 0     | 1.21   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | D340D9236645 | 440   | 0     | 1.21   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | FDEB188B3ED9 | 440   | 0     | 1.21   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 7DD346A439E1 | 440   | 0     | 1.21   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 4383D166220E | 440   | 0     | 1.21   |
| Intel     | SSDPE2KX020T8      | 2 TB   | C6B5ECE3A01F | 440   | 0     | 1.21   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 743AA942CB4B | 439   | 0     | 1.21   |
| Samsung   | SSD 970 EVO Plus   | 1 TB   | 8455A03FFAE6 | 439   | 0     | 1.20   |
| Dell      | Express Flash N... | 1.6 TB | 6C3392879A47 | 439   | 0     | 1.20   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 49150766C804 | 439   | 0     | 1.20   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 179A4034D0BC | 439   | 0     | 1.20   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 4AB1FB61E1E5 | 439   | 0     | 1.20   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 7D24FCB49AFD | 439   | 0     | 1.20   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | BAAF60EBED75 | 439   | 0     | 1.20   |
| Intel     | SSDPEKKA512G8      | 512 GB | 5F95360A2FF3 | 439   | 0     | 1.20   |
| Intel     | SSDPE21K750GA      | 752 GB | FB8E16DD6B70 | 439   | 0     | 1.20   |
| WDC       | CL SN720 SDAQNT... | 1 TB   | 3D76B1050AD4 | 439   | 0     | 1.20   |
| Samsung   | MZPLL3T2HAJQ-00005 | 3.2 TB | 94E8345678F3 | 438   | 0     | 1.20   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 34C34CC5F810 | 438   | 0     | 1.20   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 669B8842036D | 438   | 0     | 1.20   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 7A741193D51D | 438   | 0     | 1.20   |
| Intel     | MT0800KEXUU        | 800 GB | 797B6A0DF543 | 438   | 0     | 1.20   |
| Intel     | SSDPEKKW512G8      | 512 GB | 0012BF859D7E | 437   | 0     | 1.20   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | D8AE18AC08B4 | 437   | 0     | 1.20   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | C6661A657941 | 437   | 0     | 1.20   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | E8978B7043C2 | 437   | 0     | 1.20   |
| WDC       | CL SN720 SDAQNT... | 512 GB | 376BE6388BCC | 437   | 0     | 1.20   |
| Intel     | SSDPEKNW010T8      | 1 TB   | 568561D42B50 | 437   | 0     | 1.20   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 05BA3FF3399B | 436   | 0     | 1.20   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 292152A17CE8 | 436   | 0     | 1.20   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 2E43D975A7BD | 436   | 0     | 1.20   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 67F5E6C78303 | 436   | 0     | 1.20   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | DF8ECD6ACBE9 | 436   | 0     | 1.20   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | E7547E8BCF0A | 436   | 0     | 1.20   |
| WDC       | CL SN720 SDAQNT... | 512 GB | 90B1AEB40E1B | 436   | 0     | 1.20   |
| Wester... | WUS3BA138C7P3E3    | 3.8 TB | 2653058AFA7D | 436   | 0     | 1.20   |
| Wester... | WUS3BA138C7P3E3    | 3.8 TB | 49A3AEC57276 | 436   | 0     | 1.20   |
| Wester... | WUS3BA138C7P3E3    | 3.8 TB | 72D17EA5169B | 436   | 0     | 1.20   |
| Wester... | WUS3BA138C7P3E3    | 3.8 TB | 74F006E44DA1 | 436   | 0     | 1.20   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 65A0F09A6B50 | 436   | 0     | 1.20   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | BD5AD67ED8E0 | 436   | 0     | 1.20   |
| Samsung   | MZWLJ15THALA-00007 | 15.... | 55BA6A6E14FB | 436   | 0     | 1.20   |
| Intel     | SSDPE2KX010T8      | 1 TB   | EAAAF8D158DE | 436   | 0     | 1.19   |
| Intel     | SSDPE2KX010T8      | 1 TB   | 5D2480594F2C | 436   | 0     | 1.19   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 4597D3F0990A | 436   | 0     | 1.19   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | F8F5CBF2A256 | 436   | 0     | 1.19   |
| Dell      | Express Flash N... | 1.6 TB | 95D27C0B62D5 | 435   | 0     | 1.19   |
| Dell      | Express Flash N... | 1.6 TB | BE2EF46D07E5 | 435   | 0     | 1.19   |
| Dell      | Express Flash N... | 1.6 TB | 82CBE2A376AB | 435   | 0     | 1.19   |
| Toshiba   | KXG60ZNV1T02       | 1 TB   | 0653677D815D | 435   | 0     | 1.19   |
| Intel     | SSDPE2KX010T8      | 1 TB   | D212E5F1D306 | 435   | 0     | 1.19   |
| Intel     | SSDPE2KX010T8      | 1 TB   | EDFE3F76145F | 435   | 0     | 1.19   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 940FC4C293BE | 435   | 0     | 1.19   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | B93BC98A84D1 | 435   | 0     | 1.19   |
| WDC       | WDS250G3X0C-00SJG0 | 250 GB | 82CF5C3C6DF4 | 435   | 0     | 1.19   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 803C236520C7 | 435   | 0     | 1.19   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 979B9C1FEB19 | 435   | 0     | 1.19   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | DCC3F123DEB8 | 434   | 0     | 1.19   |
| Intel     | SSDPE21K750GA      | 752 GB | 780CE595C1CC | 434   | 0     | 1.19   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 1E329730A21F | 434   | 0     | 1.19   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | BDD0D3D69937 | 434   | 0     | 1.19   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | E9BA40CEBD8A | 434   | 0     | 1.19   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 0A6F7B8E9F79 | 434   | 0     | 1.19   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 6F1EFC9BC4D6 | 434   | 0     | 1.19   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 984CDF288CB0 | 434   | 0     | 1.19   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | CE3BF7B1BC03 | 434   | 0     | 1.19   |
| Dell      | Express Flash N... | 1.6 TB | 0D00F2436837 | 433   | 0     | 1.19   |
| Dell      | Express Flash N... | 1.6 TB | 52672E8EDBDD | 433   | 0     | 1.19   |
| Dell      | Express Flash N... | 1.6 TB | 996988C9088E | 433   | 0     | 1.19   |
| Dell      | Express Flash N... | 1.6 TB | AABAB453952E | 433   | 0     | 1.19   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 05AB11D96F45 | 433   | 0     | 1.19   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 3EF193AF62CA | 433   | 0     | 1.19   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 4851CB416FD0 | 433   | 0     | 1.19   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 621569B82C05 | 433   | 0     | 1.19   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | A42235B039C7 | 433   | 0     | 1.19   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | CAF56E40231D | 433   | 0     | 1.19   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | CB6049FACE70 | 433   | 0     | 1.19   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | E9021C3FDF45 | 433   | 0     | 1.19   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | ED29EEE44A36 | 433   | 0     | 1.19   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | F24C2C61E698 | 433   | 0     | 1.19   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 05AB11D96F45 | 433   | 0     | 1.19   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 3EF193AF62CA | 433   | 0     | 1.19   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 4851CB416FD0 | 433   | 0     | 1.19   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 621569B82C05 | 433   | 0     | 1.19   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | A42235B039C7 | 433   | 0     | 1.19   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | CAF56E40231D | 433   | 0     | 1.19   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | CB6049FACE70 | 433   | 0     | 1.19   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | E9021C3FDF45 | 433   | 0     | 1.19   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | ED29EEE44A36 | 433   | 0     | 1.19   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | F24C2C61E698 | 433   | 0     | 1.19   |
| Intel     | SSDPED1K375GA      | 375 GB | 5127C10875D8 | 433   | 0     | 1.19   |
| Toshiba   | THNSN5512GPU7      | 512 GB | 125E4F7110C8 | 433   | 0     | 1.19   |
| Intel     | SSDPE2KX010T8      | 1 TB   | FE5EC96C3944 | 433   | 0     | 1.19   |
| Samsung   | MZQLW1T9HMJP-000AZ | 1.9 TB | 8B55D8938EDB | 432   | 0     | 1.19   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 9F11732CE922 | 432   | 0     | 1.19   |
| Samsung   | MZPLL6T4HMLS-000MV | 6.4 TB | 0DF003972FB4 | 432   | 0     | 1.19   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | D84D9B7F22D8 | 432   | 0     | 1.19   |
| Intel     | SSDPED1K750GA      | 752 GB | 40D314F46878 | 432   | 0     | 1.18   |
| Toshiba   | KXG50ZNV512G       | 512 GB | 73D444DE6583 | 432   | 0     | 1.18   |
| Toshiba   | KXG50ZNV512G       | 512 GB | AFD5351A2DAD | 432   | 0     | 1.18   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 756B5BF288EE | 432   | 0     | 1.18   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | BE40A7434F19 | 432   | 0     | 1.18   |
| WDC       | PC SN720 SDAPNT... | 256 GB | 4A442F672455 | 432   | 0     | 1.18   |
| WDC       | CL SN720 SDAQNT... | 512 GB | FEEF5075EFD9 | 432   | 0     | 1.18   |
| WDC       | PC SN720 SDAPNT... | 256 GB | 14B63D1AA218 | 432   | 0     | 1.18   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 10E1A66706E0 | 431   | 0     | 1.18   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | DBC066248450 | 431   | 0     | 1.18   |
| WDC       | PC SN720 SDAPNT... | 256 GB | C2651F2AB82B | 431   | 0     | 1.18   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 969781805494 | 431   | 0     | 1.18   |
| Intel     | SSDPE2KX020T8      | 2 TB   | FDED7D137D31 | 431   | 0     | 1.18   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 016374102579 | 431   | 0     | 1.18   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 7C1F504097DB | 431   | 0     | 1.18   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | 5AE28DA790B9 | 431   | 0     | 1.18   |
| WDC       | CL SN720 SDAQNT... | 512 GB | 788010A4B933 | 430   | 0     | 1.18   |
| WDC       | WUS3BA196C7P3E3    | 960 GB | 942C90A72531 | 430   | 0     | 1.18   |
| Intel     | SSDPE21K750GA      | 752 GB | 125927620110 | 430   | 0     | 1.18   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 491DAEFF1C2B | 430   | 0     | 1.18   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | CE6ACD1A8B4C | 430   | 0     | 1.18   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 0C36CCA1279F | 430   | 0     | 1.18   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 36438D3F951E | 430   | 0     | 1.18   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | D9F5D57C8BA8 | 430   | 0     | 1.18   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | DE00E5D6CF5A | 430   | 0     | 1.18   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 27870D6C1532 | 429   | 0     | 1.18   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 5867E8B24C83 | 429   | 0     | 1.18   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 664ECC26F254 | 429   | 0     | 1.18   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 894A2768B296 | 429   | 0     | 1.18   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 9C5CD3C2A19D | 429   | 0     | 1.18   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | B8FE7FE5A2BA | 429   | 0     | 1.18   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | BF6449218C4D | 429   | 0     | 1.18   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | CEA8552018D3 | 429   | 0     | 1.18   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | E64FFF07A251 | 429   | 0     | 1.18   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | F7D0607334A9 | 429   | 0     | 1.18   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 27870D6C1532 | 429   | 0     | 1.18   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 5867E8B24C83 | 429   | 0     | 1.18   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 664ECC26F254 | 429   | 0     | 1.18   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 894A2768B296 | 429   | 0     | 1.18   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 9C5CD3C2A19D | 429   | 0     | 1.18   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | B8FE7FE5A2BA | 429   | 0     | 1.18   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | BF6449218C4D | 429   | 0     | 1.18   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | CEA8552018D3 | 429   | 0     | 1.18   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | E64FFF07A251 | 429   | 0     | 1.18   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | F7D0607334A9 | 429   | 0     | 1.18   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | D884B84525BB | 429   | 0     | 1.18   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | FF7E5EA39E04 | 429   | 0     | 1.18   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 6C1AA841F2C7 | 429   | 0     | 1.18   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 818A81260135 | 429   | 0     | 1.18   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 97696E0A395F | 429   | 0     | 1.18   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | AFAAA9477709 | 429   | 0     | 1.18   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | CC8BAA65ACA5 | 429   | 0     | 1.18   |
| Samsung   | SSD 970 EVO Plus   | 1 TB   | 971FD9B03DAA | 429   | 0     | 1.18   |
| WDC       | CL SN720 SDAQNT... | 1 TB   | 9B504FCB7E30 | 429   | 0     | 1.18   |
| Intel     | SSDPE2KE032T8      | 3.2 TB | 37B1CEE3800C | 429   | 0     | 1.18   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 1117FF1D0D5F | 429   | 0     | 1.18   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 1E2D400C1FAB | 429   | 0     | 1.18   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 7BECEEAD11B7 | 429   | 0     | 1.18   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 7E49B5566AEF | 429   | 0     | 1.18   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 81E3294FE28F | 429   | 0     | 1.18   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 86AA519D0938 | 429   | 0     | 1.18   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 8A2C48C71250 | 429   | 0     | 1.18   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 8EC8DE44396C | 429   | 0     | 1.18   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | A8B60F457DD6 | 429   | 0     | 1.18   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 1117FF1D0D5F | 429   | 0     | 1.18   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 1E2D400C1FAB | 429   | 0     | 1.18   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 7BECEEAD11B7 | 429   | 0     | 1.18   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 7E49B5566AEF | 429   | 0     | 1.18   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 81E3294FE28F | 429   | 0     | 1.18   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 86AA519D0938 | 429   | 0     | 1.18   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 8A2C48C71250 | 429   | 0     | 1.18   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 8EC8DE44396C | 429   | 0     | 1.18   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | A8B60F457DD6 | 429   | 0     | 1.18   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | D8E5432F6DF6 | 429   | 0     | 1.18   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 052AC6B2B89E | 429   | 0     | 1.18   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 07D67B461726 | 429   | 0     | 1.18   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 3EC3893CFBC6 | 429   | 0     | 1.18   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 44E4B51B1A41 | 429   | 0     | 1.18   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 6327BE07CB8B | 429   | 0     | 1.18   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 8D81D6683854 | 429   | 0     | 1.18   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 95F34E87B02C | 429   | 0     | 1.18   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 9F7961AF895D | 429   | 0     | 1.18   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | C8E250A728F7 | 429   | 0     | 1.18   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | E89E675F3C71 | 429   | 0     | 1.18   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 052AC6B2B89E | 429   | 0     | 1.18   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 07D67B461726 | 429   | 0     | 1.18   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 3EC3893CFBC6 | 429   | 0     | 1.18   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 44E4B51B1A41 | 429   | 0     | 1.18   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 6327BE07CB8B | 429   | 0     | 1.18   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 8D81D6683854 | 429   | 0     | 1.18   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 95F34E87B02C | 429   | 0     | 1.18   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 9F7961AF895D | 429   | 0     | 1.18   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | C8E250A728F7 | 429   | 0     | 1.18   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | E89E675F3C71 | 429   | 0     | 1.18   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 917FB31E6DB7 | 428   | 0     | 1.18   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | A22D0507F002 | 428   | 0     | 1.18   |
| Samsung   | SSD 970 EVO Plus   | 1 TB   | 22E18DBD4BA7 | 428   | 0     | 1.18   |
| Samsung   | MZPLL3T2HAJQ-00005 | 3.2 TB | 55665ACD17D4 | 428   | 0     | 1.17   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 48F10105F316 | 428   | 0     | 1.17   |
| Samsung   | MZ1LB960HAJQ-00007 | 960 GB | 2B8CECCA0D8D | 428   | 0     | 1.17   |
| Dell      | Express Flash N... | 1.6 TB | 039B4B9D6D92 | 428   | 0     | 1.17   |
| WDC       | CL SN720 SDAQNT... | 512 GB | 8A5D010F4EE1 | 428   | 0     | 1.17   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | FB9529C788C3 | 428   | 0     | 1.17   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 115405C6DB2B | 428   | 0     | 1.17   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 4BB14A472C09 | 428   | 0     | 1.17   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 509BBECD05C4 | 428   | 0     | 1.17   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 665EFEA9FAC5 | 428   | 0     | 1.17   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 9553708C8E6E | 428   | 0     | 1.17   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | C40C0199293D | 428   | 0     | 1.17   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | C78345156938 | 428   | 0     | 1.17   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | D0F0E409CAAC | 428   | 0     | 1.17   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | E952D742B569 | 428   | 0     | 1.17   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | EE7A5F3ABB30 | 428   | 0     | 1.17   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | F1DE45A1A14E | 427   | 0     | 1.17   |
| Samsung   | SSD 970 PRO        | 1 TB   | E837A556AB9D | 427   | 0     | 1.17   |
| Samsung   | MZPLL3T2HAJQ-00005 | 3.2 TB | 4C2EF3A2F60A | 427   | 0     | 1.17   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | EA521935E508 | 427   | 0     | 1.17   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 4785A4B15D2C | 427   | 0     | 1.17   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 03CE5072F904 | 427   | 0     | 1.17   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 7BE62BC8C507 | 427   | 0     | 1.17   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 4785A4B15D2C | 427   | 0     | 1.17   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 87703AAAFB14 | 427   | 0     | 1.17   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 9D94081A54B8 | 427   | 0     | 1.17   |
| Samsung   | MZPLL6T4HMLS-00003 | 6.4 TB | D647B581F3BE | 427   | 0     | 1.17   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 194DD5D2428D | 427   | 0     | 1.17   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | BF6C77C5DA12 | 427   | 0     | 1.17   |
| Toshiba   | KXG60ZNV1T02       | 1 TB   | 8519F3B354C8 | 426   | 0     | 1.17   |
| Dell      | Express Flash N... | 1.6 TB | 2D5EB6FBF945 | 426   | 0     | 1.17   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | DDAA0E546FC4 | 426   | 0     | 1.17   |
| Intel     | SSDPE2MX012T7      | 1.2 TB | 2F26EBC78CC8 | 426   | 0     | 1.17   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | E625441E9538 | 426   | 0     | 1.17   |
| Samsung   | MZWLL1T6HAJQ-00005 | 1.6 TB | 8FD6FC9CD970 | 426   | 0     | 1.17   |
| Samsung   | MZPLL3T2HAJQ-00005 | 3.2 TB | 802582281FA9 | 426   | 0     | 1.17   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 03D8BDDB52BE | 426   | 0     | 1.17   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 1AF274C0B2D6 | 426   | 0     | 1.17   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 1F2A1CE80755 | 426   | 0     | 1.17   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 4EF0BCFE29FD | 426   | 0     | 1.17   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | B1B324808F24 | 426   | 0     | 1.17   |
| Samsung   | SSD 970 PRO        | 1 TB   | A2267E2D5AD7 | 425   | 0     | 1.17   |
| Intel     | SSDPE2KX010T8      | 1 TB   | E0CC0F6AA4E1 | 425   | 0     | 1.17   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 7DCCE291FAD3 | 425   | 0     | 1.17   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 9342B94EFD41 | 425   | 0     | 1.17   |
| Intel     | SSDPE21K750GA      | 752 GB | 5BA14ACDEFF5 | 425   | 0     | 1.17   |
| Intel     | SSDPE21K750GA      | 752 GB | 302D39AC0B8A | 425   | 0     | 1.16   |
| Intel     | SSDPE21K750GA      | 752 GB | 90F667115292 | 425   | 0     | 1.16   |
| Intel     | SSDPE21K750GA      | 752 GB | D9EFEA752C1F | 425   | 0     | 1.16   |
| Intel     | SSDPE21K750GA      | 752 GB | EA4D4C61DFD7 | 425   | 0     | 1.16   |
| Samsung   | MZPLL3T2HAJQ-00005 | 3.2 TB | D0E32A271CFE | 425   | 0     | 1.16   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 3D07ABBF4789 | 424   | 0     | 1.16   |
| Intel     | SSDPE2KX020T8      | 2 TB   | F544AD5E2175 | 424   | 0     | 1.16   |
| WDC       | WUS3BA119C7P3E3    | 960 GB | A4AD2AC4B141 | 424   | 0     | 1.16   |
| Wester... | WUS3BA119C7P3E3    | 960 GB | A4AD2AC4B141 | 424   | 0     | 1.16   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 812B5C2BB15E | 424   | 0     | 1.16   |
| Intel     | SSDPE2KX020T8      | 2 TB   | FF7C9A8A4A42 | 424   | 0     | 1.16   |
| WDC       | WUS3BA196C7P3E3    | 960 GB | 1D6DE17F8670 | 424   | 0     | 1.16   |
| WDC       | WUS3BA196C7P3E3    | 960 GB | 317AE9A8BAE3 | 424   | 0     | 1.16   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 6D7C281D5CA8 | 424   | 0     | 1.16   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 9763361F27A0 | 424   | 0     | 1.16   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 9D36BED36894 | 424   | 0     | 1.16   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | C4A3D42DF461 | 424   | 0     | 1.16   |
| Wester... | WUS3BA196C7P3E3    | 960 GB | 1D6DE17F8670 | 424   | 0     | 1.16   |
| Wester... | WUS3BA196C7P3E3    | 960 GB | 317AE9A8BAE3 | 424   | 0     | 1.16   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 08B135F7F875 | 424   | 0     | 1.16   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 6D7C281D5CA8 | 424   | 0     | 1.16   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 8E6CE1859709 | 424   | 0     | 1.16   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 9763361F27A0 | 424   | 0     | 1.16   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 9D36BED36894 | 424   | 0     | 1.16   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | C4A3D42DF461 | 424   | 0     | 1.16   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | CC151E0E2E39 | 424   | 0     | 1.16   |
| Toshiba   | KXG60ZNV512G       | 512 GB | 70532BCF3353 | 424   | 0     | 1.16   |
| WDC       | CL SN720 SDAQNT... | 512 GB | 1C81175E4D6E | 424   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 011F41CAE762 | 424   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 218538ABB067 | 424   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 2D2585895556 | 424   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 46F8AE06F120 | 424   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 551E032D7922 | 424   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 5A0638527CC2 | 424   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 625EC9B3A9FC | 424   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 793F03D5BF59 | 424   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | E782FA426B39 | 424   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | F9FCCA6D64B8 | 424   | 0     | 1.16   |
| WDC       | CL SN720 SDAQNT... | 512 GB | 64610725A2F4 | 424   | 0     | 1.16   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | E800FDC98B93 | 424   | 0     | 1.16   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | E9B245037835 | 424   | 0     | 1.16   |
| Samsung   | MZPLL3T2HAJQ-00005 | 3.2 TB | 1C5182FD601E | 424   | 0     | 1.16   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 684F6CA391E9 | 424   | 0     | 1.16   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | A43FF1CCFD3E | 424   | 0     | 1.16   |
| Toshiba   | KXG60ZNV1T02       | 1 TB   | 24DD49AECD68 | 424   | 0     | 1.16   |
| Toshiba   | KXG60ZNV512G       | 512 GB | 0C68EA23EBD4 | 424   | 0     | 1.16   |
| Toshiba   | KXG60ZNV512G       | 512 GB | 57C09BE343C1 | 424   | 0     | 1.16   |
| Samsung   | SSD 970 PRO        | 1 TB   | 5D0B0072D502 | 424   | 0     | 1.16   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 5A07320DDC24 | 423   | 0     | 1.16   |
| Intel     | SSDPED1D480GA      | 480 GB | C5A59F7F4E8C | 423   | 0     | 1.16   |
| Samsung   | MZ1LB960HAJQ-00007 | 960 GB | 1B70235BF146 | 423   | 0     | 1.16   |
| Samsung   | MZ1LB960HAJQ-00007 | 960 GB | 22AF4645A58B | 423   | 0     | 1.16   |
| Samsung   | MZQLW1T9HMJP-000AZ | 1.9 TB | C6C74C5D3330 | 423   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 8132E4689B8B | 423   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | CA90AA57D0A4 | 423   | 0     | 1.16   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 666EAEDA4D3D | 423   | 0     | 1.16   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | FB842F7ADE04 | 423   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 116511CD808B | 423   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 2FC4908A7267 | 423   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 38B8876903D0 | 423   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 513CBC096F70 | 423   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 5B1E9EBAF3A2 | 423   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 6B799681D88F | 423   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | BAC54F0D8927 | 423   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | C9191A767798 | 423   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | D47E0528C6B4 | 423   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | FE187D28451F | 423   | 0     | 1.16   |
| WDC       | CL SN720 SDAQNT... | 1 TB   | 187439F53FE8 | 423   | 0     | 1.16   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 9367A706978E | 423   | 0     | 1.16   |
| Samsung   | MZQLW1T9HMJP-000AZ | 1.9 TB | 22CC119AB539 | 423   | 0     | 1.16   |
| Dell      | Express Flash P... | 800 GB | 050B73A82388 | 423   | 0     | 1.16   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 45BAFEBA7789 | 423   | 0     | 1.16   |
| Intel     | SSDPED1K750GA      | 752 GB | D32CAE5F4954 | 423   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 03F2FCA4929A | 423   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 615C56AD9540 | 423   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 9D2EB8966459 | 423   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | A45C213786DA | 423   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | E30567DE6A7F | 423   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | F45B9691A8D6 | 423   | 0     | 1.16   |
| Intel     | SSDPE2KX010T8      | 1 TB   | 33DA0DE2C250 | 423   | 0     | 1.16   |
| Toshiba   | KXG50ZNV512G       | 512 GB | 37E849F50313 | 423   | 0     | 1.16   |
| Toshiba   | KXG50ZNV512G       | 512 GB | 43137DEC8B9C | 423   | 0     | 1.16   |
| Intel     | SSDPE2KX010T8      | 1 TB   | 5C570DAA917C | 423   | 0     | 1.16   |
| Samsung   | MZPJB480HMGC-0BW07 | 480 GB | 080CEF5FC82F | 423   | 0     | 1.16   |
| Samsung   | MZPLL3T2HAJQ-00005 | 3.2 TB | 110A8E7BF9B9 | 423   | 0     | 1.16   |
| Samsung   | MZWLJ15THALA-00007 | 15.... | 4BD921EC0018 | 423   | 0     | 1.16   |
| Intel     | SSDPE2KX010T8      | 1 TB   | 9AFEB8E3C916 | 423   | 0     | 1.16   |
| Intel     | SSDPED1K750GA      | 752 GB | FD01B5F41078 | 423   | 0     | 1.16   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 3D15897A839D | 423   | 0     | 1.16   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 6675B5AFE965 | 423   | 0     | 1.16   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 883C1B67C1D2 | 423   | 0     | 1.16   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | A90E410EEE0F | 423   | 0     | 1.16   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | C45A5BDBF5BA | 423   | 0     | 1.16   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | C4E4A9A4AE5B | 423   | 0     | 1.16   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | D3207E84454F | 423   | 0     | 1.16   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | EA47CC0D1AED | 423   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 103573840F45 | 423   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 6B716A05761F | 423   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 6C424254457A | 423   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 89D773C11E41 | 423   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | AA794F51AAC5 | 423   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | CABD8C4635BF | 423   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | DC35136D1DC7 | 423   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | E989E3B67A73 | 423   | 0     | 1.16   |
| Dell      | Express Flash N... | 1.6 TB | 083C3E983D3A | 423   | 0     | 1.16   |
| Dell      | Express Flash N... | 1.6 TB | 0BEB01F2535E | 423   | 0     | 1.16   |
| Dell      | Express Flash N... | 1.6 TB | 5CEA7AB85ABC | 423   | 0     | 1.16   |
| Dell      | Express Flash N... | 1.6 TB | 76918010D852 | 423   | 0     | 1.16   |
| Intel     | SSDPED1K750GA      | 752 GB | 94E932E8D8A3 | 423   | 0     | 1.16   |
| Samsung   | MZVLB1T0HALR-00000 | 1 TB   | 6C91657F8426 | 423   | 0     | 1.16   |
| Intel     | SSDPED1D280GA      | 280 GB | EDA84ED2AE09 | 423   | 0     | 1.16   |
| Toshiba   | KXG50ZNV512G       | 512 GB | 5C3D58545750 | 423   | 0     | 1.16   |
| Toshiba   | KXG50ZNV512G       | 512 GB | 970F81F2459F | 423   | 0     | 1.16   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 37AE35208421 | 422   | 0     | 1.16   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 284DCC14500F | 422   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 2FCF4E52EB08 | 422   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | BE2F08E611E0 | 422   | 0     | 1.16   |
| WDC       | CL SN720 SDAQNT... | 1 TB   | CA1523599896 | 422   | 0     | 1.16   |
| Intel     | SSDPE2KX010T8      | 1 TB   | 677E78C7A0E8 | 422   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | AF32A9799879 | 422   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | E0D4D89F6F51 | 422   | 0     | 1.16   |
| Intel     | SSDPE2KE032T8      | 3.2 TB | 6F9A209221AB | 422   | 0     | 1.16   |
| Intel     | SSDPE2KX010T8      | 1 TB   | A3CBA0CD0032 | 422   | 0     | 1.16   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 15F0447BC653 | 422   | 0     | 1.16   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 6D64D97A2B35 | 422   | 0     | 1.16   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 814902D95D35 | 422   | 0     | 1.16   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | AC7B1FEF18F7 | 422   | 0     | 1.16   |
| WDC       | CL SN720 SDAQNT... | 512 GB | 8C44A2952D6D | 422   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 636FAAB99F1E | 422   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | CB6282FE5807 | 422   | 0     | 1.16   |
| Dell      | Express Flash C... | 960 GB | 99EBB7EDED91 | 422   | 0     | 1.16   |
| Dell      | Express Flash N... | 1.6 TB | 015079C88F62 | 422   | 0     | 1.16   |
| Intel     | SSDPED1K375GA      | 375 GB | B3BC520A0D62 | 422   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 01F7146DBB0D | 422   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 9B0674F4BC93 | 422   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | C23D2C6C8549 | 422   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | E0059E2247BF | 422   | 0     | 1.16   |
| Wester... | WUS3BA138C7P3E3    | 3.8 TB | 5348C23D90AD | 422   | 0     | 1.16   |
| Wester... | WUS3BA138C7P3E3    | 3.8 TB | 8FBCA56F1794 | 422   | 0     | 1.16   |
| Wester... | WUS3BA138C7P3E3    | 3.8 TB | B1EE82A3550C | 422   | 0     | 1.16   |
| Wester... | WUS3BA138C7P3E3    | 3.8 TB | DF8879C7965C | 422   | 0     | 1.16   |
| Intel     | SSDPE2MX012T7      | 1.2 TB | B00D7DD8F2CF | 422   | 0     | 1.16   |
| Samsung   | MZPLL3T2HAJQ-00005 | 3.2 TB | 773249547F9E | 422   | 0     | 1.16   |
| Samsung   | SSD 970 PRO        | 1 TB   | F15D352A5414 | 422   | 0     | 1.16   |
| Wester... | WUS3BA138C7P3E3    | 3.8 TB | 1FF7EC5CD064 | 422   | 0     | 1.16   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | BA56547203AE | 422   | 0     | 1.16   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 06FE7F1530BE | 422   | 0     | 1.16   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 4097EE60BF62 | 422   | 0     | 1.16   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | F4030F60F8DB | 422   | 0     | 1.16   |
| Intel     | SSDPEKNW020T8      | 2 TB   | 4C0310082FB7 | 422   | 0     | 1.16   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 32921CC487FE | 422   | 0     | 1.16   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 69823B483F83 | 422   | 0     | 1.16   |
| Samsung   | SSD 970 EVO Plus   | 1 TB   | 3DA27F73ACBA | 422   | 0     | 1.16   |
| WDC       | CL SN720 SDAQNT... | 512 GB | 6C8AF49B1361 | 422   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 0E2B2DAA456E | 422   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 0E711749B2BD | 422   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 490AEFAF3B54 | 422   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 51FD14060B91 | 422   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 5BE595FC21EC | 422   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 911BBDD70B4D | 422   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | AA15589C64D3 | 422   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | AA47BB9B29E5 | 422   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | BAF7FBD5AE7B | 422   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | CD579E4C6622 | 422   | 0     | 1.16   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 04582D215223 | 422   | 0     | 1.16   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 15D00065C69C | 422   | 0     | 1.16   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 484B614D3E94 | 422   | 0     | 1.16   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 4BD8C922D381 | 422   | 0     | 1.16   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 8393FD53E86A | 422   | 0     | 1.16   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | F9140F0CF3A9 | 422   | 0     | 1.16   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | FB3AC54A3052 | 422   | 0     | 1.16   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | FB3EF2C6EBC9 | 422   | 0     | 1.16   |
| Intel     | SSDPED1K750GA      | 752 GB | DD978395A31F | 422   | 0     | 1.16   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 689AD38A8282 | 422   | 0     | 1.16   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | D4DFC657BF9D | 422   | 0     | 1.16   |
| Toshiba   | KXG60ZNV1T02       | 1 TB   | BA9F60D80AFE | 422   | 0     | 1.16   |
| Toshiba   | KXG60ZNV1T02       | 1 TB   | D5775628128C | 422   | 0     | 1.16   |
| Intel     | SSDPE2KX010T8      | 1 TB   | 3ED1DC189545 | 422   | 0     | 1.16   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 6811AFDE8FAC | 422   | 0     | 1.16   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 744F744EBE2B | 422   | 0     | 1.16   |
| WDC       | CL SN720 SDAQNT... | 512 GB | C97AA805C539 | 422   | 0     | 1.16   |
| WDC       | WUS3BA138C7P3E3    | 3.8 TB | ACE29F33B82B | 422   | 0     | 1.16   |
| WDC       | WUS3BA138C7P3E3    | 3.8 TB | DDC0AB76A1A1 | 422   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 029218B1F466 | 422   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 03A3AE01ECB7 | 422   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 21BA42AEAF12 | 422   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 43E9CD3BA4D8 | 422   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 4B521A47308D | 422   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 6235CB9CFD65 | 422   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | B4ADE2746552 | 422   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | C1D0944C783B | 422   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | DE644285BB6B | 422   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | F4DBE89AC6CB | 422   | 0     | 1.16   |
| Wester... | WUS3BA138C7P3E3    | 3.8 TB | A5BA4ACCDC4D | 422   | 0     | 1.16   |
| Wester... | WUS3BA138C7P3E3    | 3.8 TB | ACE29F33B82B | 422   | 0     | 1.16   |
| Wester... | WUS3BA138C7P3E3    | 3.8 TB | DDC0AB76A1A1 | 422   | 0     | 1.16   |
| WDC       | WUS3BA196C7P3E3    | 960 GB | 9CE7C2B1001D | 422   | 0     | 1.16   |
| WDC       | WUS3BA196C7P3E3    | 960 GB | D30FB5EA3473 | 422   | 0     | 1.16   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 1FB9BA8FFB68 | 422   | 0     | 1.16   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 2D94B10D0C2B | 422   | 0     | 1.16   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 3E64C8D326F0 | 422   | 0     | 1.16   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | AD74DE88DAD7 | 422   | 0     | 1.16   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | EBB3C649958B | 422   | 0     | 1.16   |
| Wester... | WUS3BA196C7P3E3    | 960 GB | 9CE7C2B1001D | 422   | 0     | 1.16   |
| Wester... | WUS3BA196C7P3E3    | 960 GB | A9C98BAA2216 | 422   | 0     | 1.16   |
| Wester... | WUS3BA196C7P3E3    | 960 GB | D30FB5EA3473 | 422   | 0     | 1.16   |
| Wester... | WUS3BA196C7P3E3    | 960 GB | E94F6B19387B | 422   | 0     | 1.16   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 1FB9BA8FFB68 | 422   | 0     | 1.16   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 2D94B10D0C2B | 422   | 0     | 1.16   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 34F718108ACF | 422   | 0     | 1.16   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 3E64C8D326F0 | 422   | 0     | 1.16   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | AD74DE88DAD7 | 422   | 0     | 1.16   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | EBB3C649958B | 422   | 0     | 1.16   |
| Corsair   | Force MP510 1.9TB  | 1.9 TB | 4167F28C9F68 | 422   | 0     | 1.16   |
| Dell      | Express Flash P... | 1.6 TB | C2C7F2A303BE | 422   | 0     | 1.16   |
| WDC       | WUS3BA196C7P3E3    | 960 GB | 1EFDDDEB23D2 | 422   | 0     | 1.16   |
| WDC       | WUS3BA196C7P3E3    | 960 GB | 4A7C57C7CBB9 | 422   | 0     | 1.16   |
| WDC       | WUS3BA196C7P3E3    | 960 GB | 5665EFF4B813 | 422   | 0     | 1.16   |
| WDC       | WUS3BA196C7P3E3    | 960 GB | B94E01AA77FB | 422   | 0     | 1.16   |
| WDC       | WUS3BA196C7P3E3    | 960 GB | D4E35A75068F | 422   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 1ACE21FBEB19 | 422   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 2C754690E106 | 422   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 32AA9F771A86 | 422   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 41016EDBBF5D | 422   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 59EA4FE09956 | 422   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 8F58743BC9AD | 422   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | A4EC9D9CD6F5 | 422   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | B33BFE3F426D | 422   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | D8486F771051 | 422   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | F37FF1387F79 | 422   | 0     | 1.16   |
| Wester... | WUS3BA196C7P3E3    | 960 GB | 03F9CC625716 | 422   | 0     | 1.16   |
| Wester... | WUS3BA196C7P3E3    | 960 GB | 1EFDDDEB23D2 | 422   | 0     | 1.16   |
| Wester... | WUS3BA196C7P3E3    | 960 GB | 2D4B5BBD4554 | 422   | 0     | 1.16   |
| Wester... | WUS3BA196C7P3E3    | 960 GB | 4A7C57C7CBB9 | 422   | 0     | 1.16   |
| Wester... | WUS3BA196C7P3E3    | 960 GB | 5665EFF4B813 | 422   | 0     | 1.16   |
| Wester... | WUS3BA196C7P3E3    | 960 GB | B94E01AA77FB | 422   | 0     | 1.16   |
| Wester... | WUS3BA196C7P3E3    | 960 GB | D4E35A75068F | 422   | 0     | 1.16   |
| Samsung   | MZWLL12THMLA-00005 | 12.... | 1D032E2398C6 | 422   | 0     | 1.16   |
| Samsung   | MZWLL12THMLA-00005 | 12.... | 4816C8C6CD59 | 422   | 0     | 1.16   |
| Samsung   | MZVLB512HAJQ-00000 | 512 GB | 12A79C0BA416 | 422   | 0     | 1.16   |
| WDC       | PC SN720 SDAPNT... | 256 GB | 996829787603 | 422   | 0     | 1.16   |
| Toshiba   | KXG60ZNV1T02       | 1 TB   | 6128A5F4B942 | 421   | 0     | 1.16   |
| Toshiba   | KXG60ZNV1T02       | 1 TB   | 624E90EF4B88 | 421   | 0     | 1.16   |
| Toshiba   | KXG60ZNV1T02       | 1 TB   | A80944313FCE | 421   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 0104E78B083F | 421   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 8A8030EC67D9 | 421   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 90642055B4DC | 421   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | A0EDD928C370 | 421   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | A54DC18C92A0 | 421   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | B2CE8F6FABD5 | 421   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | BB643535DF3F | 421   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | C7F61F956BD0 | 421   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | EA032A184992 | 421   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | FC2336F2584F | 421   | 0     | 1.16   |
| WDC       | CL SN720 SDAQNT... | 1 TB   | A7A71A6555F5 | 421   | 0     | 1.16   |
| Intel     | SSDPED1K750GA      | 752 GB | 073A1368CF21 | 421   | 0     | 1.16   |
| Intel     | SSDPED1K750GA      | 752 GB | 76DEAEFC9232 | 421   | 0     | 1.16   |
| Intel     | SSDPED1K750GA      | 752 GB | A5B010DFAA6C | 421   | 0     | 1.16   |
| Intel     | SSDPEKKA512G8      | 512 GB | 98EDBAC92E81 | 421   | 0     | 1.16   |
| Intel     | SSDPEKKW512G8      | 512 GB | EE9BBD29AFE6 | 421   | 0     | 1.16   |
| Intel     | SSDPED1K375GA      | 375 GB | 386C72A5587E | 421   | 0     | 1.16   |
| Intel     | SSDPED1K750GA      | 752 GB | 095E6CA63447 | 421   | 0     | 1.16   |
| Intel     | SSDPED1K750GA      | 752 GB | A43009A8ADDC | 421   | 0     | 1.16   |
| Intel     | SSDPED1K750GA      | 752 GB | CE8EDBF896FB | 421   | 0     | 1.16   |
| Phison    | Sabrent Rocket Q   | 2 TB   | 3F8B12E93D1A | 421   | 0     | 1.16   |
| Intel     | SSDPE2KX010T8      | 1 TB   | 3227056BFA93 | 421   | 0     | 1.16   |
| Intel     | SSDPED1K375GA      | 375 GB | 504B29C28308 | 421   | 0     | 1.16   |
| Intel     | SSDPED1K375GA      | 375 GB | 53356B2097BB | 421   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 0AE4D4733DA8 | 421   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 24B156874470 | 421   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 46435A5354CF | 421   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 5EE91A58DD7D | 421   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 80B1310F939D | 421   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 8DAF26117CFA | 421   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | B34686B79E9E | 421   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | B82E87ACBDC7 | 421   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | B8636643BCF0 | 421   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | BD3B1B467B90 | 421   | 0     | 1.16   |
| Intel     | SSDPE2KX010T8      | 1 TB   | 1D3CCC4DCB6D | 421   | 0     | 1.16   |
| Dell      | Express Flash P... | 1.6 TB | 7762DE5BE4CC | 421   | 0     | 1.16   |
| Intel     | SSDPE2KX010T8      | 1 TB   | 551B8F2D0738 | 421   | 0     | 1.16   |
| WDC       | WUS3BA196C7P3E3    | 960 GB | 71F7D2C38180 | 421   | 0     | 1.16   |
| WDC       | WUS3BA196C7P3E3    | 960 GB | 83DDBD4A04F2 | 421   | 0     | 1.16   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 1B13260437F8 | 421   | 0     | 1.16   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 334902D16328 | 421   | 0     | 1.16   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 3E7E435C7FC2 | 421   | 0     | 1.16   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 4EFC3EF886B5 | 421   | 0     | 1.16   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 765FF2D68CF3 | 421   | 0     | 1.16   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | A6AF0C943519 | 421   | 0     | 1.16   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | C13AC3359923 | 421   | 0     | 1.16   |
| Wester... | WUS3BA196C7P3E3    | 960 GB | 71F7D2C38180 | 421   | 0     | 1.16   |
| Wester... | WUS3BA196C7P3E3    | 960 GB | 83DDBD4A04F2 | 421   | 0     | 1.16   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 1B13260437F8 | 421   | 0     | 1.16   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 334902D16328 | 421   | 0     | 1.16   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 3E7E435C7FC2 | 421   | 0     | 1.16   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 4EFC3EF886B5 | 421   | 0     | 1.16   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 765FF2D68CF3 | 421   | 0     | 1.16   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | A6AF0C943519 | 421   | 0     | 1.16   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | C13AC3359923 | 421   | 0     | 1.16   |
| Intel     | SSDPE2KE032T8      | 3.2 TB | AA351DC380DC | 421   | 0     | 1.16   |
| Intel     | SSDPE2KX010T8      | 1 TB   | DD25707CE559 | 421   | 0     | 1.16   |
| Intel     | SSDPED1D280GA      | 280 GB | 3435B53BFA5A | 421   | 0     | 1.16   |
| Intel     | SSDPED1D280GA      | 280 GB | AC5C5B607D07 | 421   | 0     | 1.16   |
| Intel     | SSDPEDKE020T7      | 2 TB   | 061B82A0BF45 | 421   | 0     | 1.16   |
| Micron    | 9300_MTFDHAL3T2TDR | 3.2 TB | C2D7B40CE093 | 421   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 15CCB719C42A | 421   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 6157E0D89DFC | 421   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 75710A2D82F0 | 421   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 8998814DCA3F | 421   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | D675A8D19FC5 | 421   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | DB1D4A612C4A | 421   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | DC077334B828 | 421   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | DF9D11F36123 | 421   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | E2B879FAF8CF | 421   | 0     | 1.16   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | E74A1C415317 | 421   | 0     | 1.16   |
| Intel     | SSDPE2KE032T8      | 3.2 TB | E65C00E9CA94 | 421   | 0     | 1.16   |
| Intel     | SSDPED1D480GA      | 480 GB | 3717CCB4F86F | 421   | 0     | 1.16   |
| Micron    | 9300_MTFDHAL3T2TDR | 3.2 TB | 48C6B80C2624 | 421   | 0     | 1.16   |
| WDC       | WUS3BA196C7P3E3    | 960 GB | 987E41B9B41E | 421   | 0     | 1.16   |
| WDC       | WUS3BA196C7P3E3    | 960 GB | E3B2F5E234EE | 421   | 0     | 1.16   |
| Wester... | WUS3BA196C7P3E3    | 960 GB | 89A2E87B5B44 | 421   | 0     | 1.16   |
| Wester... | WUS3BA196C7P3E3    | 960 GB | 987E41B9B41E | 421   | 0     | 1.16   |
| Wester... | WUS3BA196C7P3E3    | 960 GB | E3B2F5E234EE | 421   | 0     | 1.16   |
| Intel     | SSDPE2KE032T8      | 3.2 TB | 0E376782BDB8 | 421   | 0     | 1.16   |
| Intel     | SSDPE2KE032T8      | 3.2 TB | D05B1D07E534 | 421   | 0     | 1.16   |
| Intel     | SSDPED1K750GA      | 752 GB | 6AB4D2A0DE51 | 421   | 0     | 1.16   |
| WDC       | CL SN720 SDAQNT... | 1 TB   | 0B26F30F1722 | 421   | 0     | 1.16   |
| WDC       | CL SN720 SDAQNT... | 512 GB | 2CDCD1DEB0AF | 421   | 0     | 1.15   |
| HP        | SSD EX950          | 2 TB   | 25880204C22A | 421   | 0     | 1.15   |
| Intel     | SSDPE21K750GA      | 752 GB | 42A8CA29E571 | 421   | 0     | 1.15   |
| Intel     | SSDPE21K750GA      | 752 GB | 956146AAAAF3 | 421   | 0     | 1.15   |
| Intel     | SSDPE21K750GA      | 752 GB | 9F2DA57A4FF4 | 421   | 0     | 1.15   |
| Intel     | SSDPE21K750GA      | 752 GB | C6DF053E7116 | 421   | 0     | 1.15   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 8E2BFC334936 | 421   | 0     | 1.15   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | A0A9CD6ED9EF | 421   | 0     | 1.15   |
| Toshiba   | KXD51RUE960G       | 960 GB | 56AE1DCAF3C9 | 421   | 0     | 1.15   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 45E1B5019DDD | 421   | 0     | 1.15   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 4113B57491F6 | 421   | 0     | 1.15   |
| Intel     | SSDPE2KX020T8      | 2 TB   | DA911BFD5161 | 421   | 0     | 1.15   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 4D6B9E4D4D12 | 421   | 0     | 1.15   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 90F4B2DE1FB4 | 421   | 0     | 1.15   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | A8F6F3E2A9F0 | 421   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 0FE8CBDAD65B | 421   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 15D480FF5ACC | 421   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 1B33F83E9B4D | 421   | 0     | 1.15   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 036D818EB27A | 421   | 0     | 1.15   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 59DCD5A1B3C8 | 421   | 0     | 1.15   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | A606C0436897 | 421   | 0     | 1.15   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | B6B16726D626 | 421   | 0     | 1.15   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | E368265FDF30 | 421   | 0     | 1.15   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | EB6C7A04DD7F | 421   | 0     | 1.15   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | FA124DAF7D29 | 421   | 0     | 1.15   |
| Intel     | SSDPE2KE032T8      | 3.2 TB | 20072AA2296A | 421   | 0     | 1.15   |
| Intel     | SSDPE2KE032T8      | 3.2 TB | 2F34C2C759F8 | 421   | 0     | 1.15   |
| WDC       | CL SN720 SDAQNT... | 1 TB   | 7933ED0ECF64 | 421   | 0     | 1.15   |
| Wester... | WUS3BA138C7P3E3    | 3.8 TB | 6F8630A0213D | 421   | 0     | 1.15   |
| Wester... | WUS3BA138C7P3E3    | 3.8 TB | 81A053D9CADB | 421   | 0     | 1.15   |
| Wester... | WUS3BA138C7P3E3    | 3.8 TB | 850BA24CEE34 | 421   | 0     | 1.15   |
| Wester... | WUS3BA138C7P3E3    | 3.8 TB | B4EC0D1ED904 | 421   | 0     | 1.15   |
| Wester... | WUS3BA138C7P3E3    | 3.8 TB | E2A138ADFC67 | 421   | 0     | 1.15   |
| Samsung   | MZPLL3T2HAJQ-00005 | 3.2 TB | 062F49823028 | 421   | 0     | 1.15   |
| WDC       | PC SN720 SDAPNT... | 256 GB | 00D97B426106 | 421   | 0     | 1.15   |
| Intel     | SSDPED1D480GA      | 480 GB | 088AF3E76A4A | 421   | 0     | 1.15   |
| Samsung   | SSD 970 PRO        | 1 TB   | 7604B6A16099 | 421   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 206D35C62D1C | 421   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | AEE3D8A090BE | 421   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | BE539568069F | 421   | 0     | 1.15   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 2D8A1F49EC1C | 421   | 0     | 1.15   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 5FD77AD54CF8 | 421   | 0     | 1.15   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 6320A4C90486 | 421   | 0     | 1.15   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 6FA03F65F67A | 421   | 0     | 1.15   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 822DEAE50894 | 421   | 0     | 1.15   |
| Dell      | Express Flash N... | 1.6 TB | 715F0CBDA5F9 | 421   | 0     | 1.15   |
| Intel     | SSDPED1K375GA      | 375 GB | 65846EF5997F | 421   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 6986ABC8638B | 421   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 9F698E554C80 | 421   | 0     | 1.15   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 01719C788FB4 | 421   | 0     | 1.15   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 3A5484CFCE1C | 421   | 0     | 1.15   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 462324121322 | 421   | 0     | 1.15   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 5CC131769F08 | 421   | 0     | 1.15   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 6910A32B80B5 | 421   | 0     | 1.15   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | CB97E97E025F | 421   | 0     | 1.15   |
| Intel     | SSDPE21K750GA      | 752 GB | A0570820BADA | 421   | 0     | 1.15   |
| Intel     | SSDPE2KE032T8      | 3.2 TB | A0D2F1B20249 | 421   | 0     | 1.15   |
| Intel     | SSDPE2KE032T8      | 3.2 TB | A7A210747FD8 | 421   | 0     | 1.15   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 83FF5D7F43F3 | 421   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 0AC92167311D | 421   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 0ACF690D08DD | 421   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 43C9AF9B6CD1 | 421   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 953D74E4A5C9 | 421   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 96AEFAA9AF7A | 421   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 9A75C02FC323 | 421   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 9EBBA68694B8 | 421   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | B4C7062E25AA | 421   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | F1FBA528D8B4 | 421   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | FF4F1E152B13 | 421   | 0     | 1.15   |
| Intel     | SSDPEKKA512G8      | 512 GB | 191C15D155BA | 420   | 0     | 1.15   |
| Samsung   | MZ1LB960HAJQ-00007 | 960 GB | E02A2E824783 | 420   | 0     | 1.15   |
| Samsung   | MZ1LB960HAJQ-00007 | 960 GB | E70726A53A35 | 420   | 0     | 1.15   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 28C5D5C7FCB6 | 420   | 0     | 1.15   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 28C5D5C7FCB6 | 420   | 0     | 1.15   |
| Dell      | Express Flash N... | 1.6 TB | 0B91227AD422 | 420   | 0     | 1.15   |
| Dell      | Express Flash N... | 1.6 TB | 62F76B95F20C | 420   | 0     | 1.15   |
| Dell      | Express Flash N... | 1.6 TB | BF352E265141 | 420   | 0     | 1.15   |
| Dell      | Express Flash N... | 1.6 TB | C8B14ECD9DF9 | 420   | 0     | 1.15   |
| Dell      | Express Flash N... | 1.6 TB | FA23C8F0BDF5 | 420   | 0     | 1.15   |
| Intel     | SSDPEKKA256G7      | 256 GB | 741546F2C8BE | 420   | 0     | 1.15   |
| WDC       | CL SN720 SDAQNT... | 512 GB | 77781D7DEA28 | 420   | 0     | 1.15   |
| Intel     | SSDPED1K375GA      | 375 GB | C775BD1F4167 | 420   | 0     | 1.15   |
| Intel     | SSDPED1K375GA      | 375 GB | FC113A6C9F37 | 420   | 0     | 1.15   |
| Intel     | SSDPED1D960GAY     | 960 GB | 6DE197960864 | 420   | 0     | 1.15   |
| Samsung   | MZ1LB960HAJQ-00007 | 960 GB | EDF6176CD413 | 420   | 0     | 1.15   |
| WDC       | CL SN720 SDAQNT... | 1 TB   | 69596A13984B | 420   | 0     | 1.15   |
| WDC       | WUS3BA196C7P3E3    | 960 GB | 9CA2B8A66EAF | 420   | 0     | 1.15   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 326D702F9672 | 420   | 0     | 1.15   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 32AECF1A0681 | 420   | 0     | 1.15   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | A402BEFFECFC | 420   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 1BC872BD0248 | 420   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 1CA53AFF3B33 | 420   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 206FABC8B692 | 420   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 7A4071293EB7 | 420   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 8BD64948050C | 420   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | ADBB5D6A23FF | 420   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | D59F917F2A52 | 420   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | DF798323621D | 420   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | E2A74B97FBA7 | 420   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | F7A7E1422074 | 420   | 0     | 1.15   |
| Wester... | WUS3BA196C7P3E3    | 960 GB | 89F9219138BA | 420   | 0     | 1.15   |
| Wester... | WUS3BA196C7P3E3    | 960 GB | 8FD095700B4C | 420   | 0     | 1.15   |
| Wester... | WUS3BA196C7P3E3    | 960 GB | 9CA2B8A66EAF | 420   | 0     | 1.15   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 326D702F9672 | 420   | 0     | 1.15   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 32AECF1A0681 | 420   | 0     | 1.15   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | A402BEFFECFC | 420   | 0     | 1.15   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | FD6EC4D9A7F7 | 420   | 0     | 1.15   |
| Dell      | Express Flash N... | 1.6 TB | 1635F808C003 | 420   | 0     | 1.15   |
| Dell      | Express Flash N... | 1.6 TB | 25239374D0C8 | 420   | 0     | 1.15   |
| Dell      | Express Flash N... | 1.6 TB | 7C65D421BD27 | 420   | 0     | 1.15   |
| Dell      | Express Flash N... | 1.6 TB | 859D8C413D11 | 420   | 0     | 1.15   |
| Dell      | Express Flash N... | 1.6 TB | D6ACE34EE210 | 420   | 0     | 1.15   |
| Dell      | Express Flash N... | 1.6 TB | F871E09514F2 | 420   | 0     | 1.15   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 210D8C00AABD | 420   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 1DE9808AA91C | 420   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 267526C188A6 | 420   | 0     | 1.15   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 0394312D4F9D | 420   | 0     | 1.15   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 0D13F13D5405 | 420   | 0     | 1.15   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 1B0A64C19B3B | 420   | 0     | 1.15   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 22C8E3E1DA10 | 420   | 0     | 1.15   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 23496F89D653 | 420   | 0     | 1.15   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 2483C687E555 | 420   | 0     | 1.15   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 2F28EFCF1531 | 420   | 0     | 1.15   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 39CFD03652C2 | 420   | 0     | 1.15   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 40ED4C55BD49 | 420   | 0     | 1.15   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 565788F089AA | 420   | 0     | 1.15   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 5805AE7E574D | 420   | 0     | 1.15   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 7A7CBC6CF5E8 | 420   | 0     | 1.15   |
| Intel     | SSDPE2KX020T8      | 2 TB   | AEEF9ABDEC20 | 420   | 0     | 1.15   |
| Intel     | SSDPE2KX020T8      | 2 TB   | BA1FAD01E2B2 | 420   | 0     | 1.15   |
| Intel     | SSDPE2KX020T8      | 2 TB   | C09230ADA625 | 420   | 0     | 1.15   |
| Intel     | SSDPE2KX020T8      | 2 TB   | C0B35A41BF6C | 420   | 0     | 1.15   |
| Intel     | SSDPE2KX020T8      | 2 TB   | FBD2C82AF106 | 420   | 0     | 1.15   |
| Intel     | SSDPEKKA512G8      | 512 GB | 129D2A2C5F96 | 420   | 0     | 1.15   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 3793609CE364 | 420   | 0     | 1.15   |
| WDC       | WUS3BA196C7P3E3    | 960 GB | F8C86507635E | 420   | 0     | 1.15   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 031E2AC29CF0 | 420   | 0     | 1.15   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 37AE4AC946B9 | 420   | 0     | 1.15   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 41B53AC8F3A0 | 420   | 0     | 1.15   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 68856E7A384F | 420   | 0     | 1.15   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 8602CA6DA3C5 | 420   | 0     | 1.15   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | BBA91B13F333 | 420   | 0     | 1.15   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | BEB15444D678 | 420   | 0     | 1.15   |
| Wester... | WUS3BA196C7P3E3    | 960 GB | 7468262286F4 | 420   | 0     | 1.15   |
| Wester... | WUS3BA196C7P3E3    | 960 GB | 9CFDDB395C56 | 420   | 0     | 1.15   |
| Wester... | WUS3BA196C7P3E3    | 960 GB | F8C86507635E | 420   | 0     | 1.15   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 031E2AC29CF0 | 420   | 0     | 1.15   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 37AE4AC946B9 | 420   | 0     | 1.15   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 41B53AC8F3A0 | 420   | 0     | 1.15   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 68856E7A384F | 420   | 0     | 1.15   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 8602CA6DA3C5 | 420   | 0     | 1.15   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | BBA91B13F333 | 420   | 0     | 1.15   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | BEB15444D678 | 420   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 0EDB55E17455 | 420   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 1933B5CF5D7C | 420   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 2DFD42D7E81C | 420   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 359F35111DB2 | 420   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 52199F05F4ED | 420   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 5F1F105E7979 | 420   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 5FE02B31148E | 420   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 73F61372FB7D | 420   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 748FFEDDC88E | 420   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 77E4E75AA416 | 420   | 0     | 1.15   |
| Intel     | SSDPEKKA512G8      | 512 GB | FF9E9A3DBED5 | 420   | 0     | 1.15   |
| Intel     | SSDPEKNW020T8      | 2 TB   | 6417E08762BD | 420   | 0     | 1.15   |
| WDC       | CL SN720 SDAQNT... | 1 TB   | 91E422166FD9 | 420   | 0     | 1.15   |
| WDC       | PC SN720 SDAPNT... | 256 GB | 70C1EA50E240 | 420   | 0     | 1.15   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 8367E704DC42 | 420   | 0     | 1.15   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | AD977A09E357 | 420   | 0     | 1.15   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | AF694D8A0DBC | 420   | 0     | 1.15   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | F47C12C61C6F | 420   | 0     | 1.15   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 094C1690FA6B | 420   | 0     | 1.15   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 11C6C0D5159E | 420   | 0     | 1.15   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 25726BD1D634 | 420   | 0     | 1.15   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 28938565F999 | 420   | 0     | 1.15   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 3E6BD0B5933E | 420   | 0     | 1.15   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 47D8D40DBB15 | 420   | 0     | 1.15   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 47F916FD85AC | 420   | 0     | 1.15   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 8367E704DC42 | 420   | 0     | 1.15   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | AD977A09E357 | 420   | 0     | 1.15   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | AF694D8A0DBC | 420   | 0     | 1.15   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | DFF08FFF692D | 420   | 0     | 1.15   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | F47C12C61C6F | 420   | 0     | 1.15   |
| Dell      | Express Flash N... | 1.6 TB | E0C672AB0167 | 420   | 0     | 1.15   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 0E3C2DB1EB32 | 420   | 0     | 1.15   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | CEAC704449A7 | 420   | 0     | 1.15   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 39BF43FA8E4E | 420   | 0     | 1.15   |
| WDC       | CL SN720 SDAQNT... | 512 GB | 96921571D959 | 420   | 0     | 1.15   |
| WDC       | CL SN720 SDAQNT... | 512 GB | A812CC679B0C | 420   | 0     | 1.15   |
| Intel     | SSDPEKKA512G8      | 512 GB | AE5FCCB6DED9 | 420   | 0     | 1.15   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 01183161E660 | 420   | 0     | 1.15   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 1BDADC16E64D | 420   | 0     | 1.15   |
| Intel     | SSDPE2KX020T8      | 2 TB   | A56278D80392 | 420   | 0     | 1.15   |
| Intel     | SSDPE2KX020T8      | 2 TB   | F1E5EF372B5D | 420   | 0     | 1.15   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 24B75F64A530 | 420   | 0     | 1.15   |
| Samsung   | MZPLL6T4HMLS-00003 | 6.4 TB | 0A63E17B00A0 | 420   | 0     | 1.15   |
| WDC       | WUS3BA196C7P3E3    | 960 GB | DB82A37B3283 | 420   | 0     | 1.15   |
| Wester... | WUS3BA196C7P3E3    | 960 GB | 0A8F7504DCE8 | 420   | 0     | 1.15   |
| Wester... | WUS3BA196C7P3E3    | 960 GB | 97A62C4A0DF9 | 420   | 0     | 1.15   |
| Wester... | WUS3BA196C7P3E3    | 960 GB | DB82A37B3283 | 420   | 0     | 1.15   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 2D3BCD25ACC9 | 420   | 0     | 1.15   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 422C6EF848BA | 420   | 0     | 1.15   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 4B7A1E7F9301 | 420   | 0     | 1.15   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | CB4D7376F7EB | 420   | 0     | 1.15   |
| Dell      | Express Flash N... | 1.6 TB | C9D0CA29666B | 420   | 0     | 1.15   |
| Intel     | SSDPEDKE020T7      | 2 TB   | 6A7DAC27771F | 420   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 1004ED5C3096 | 420   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 168B436E5445 | 420   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 3345E07E271D | 420   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 36C6F183A29D | 420   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 4351809CD49E | 420   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 7B0E0F15D3CA | 420   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 844D74DD0800 | 420   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 9FA3633824CB | 420   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | A0421CA3D5A4 | 420   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | C6AB3A3FFCD8 | 420   | 0     | 1.15   |
| Corsair   | Force MP510 1.9TB  | 1.9 TB | 82B780D89FAA | 420   | 0     | 1.15   |
| Intel     | SSDPED1D280GA      | 280 GB | A8336BFF5D02 | 420   | 0     | 1.15   |
| Phison    | Viper M.2 VPN100   | 512 GB | E29FDE54A21E | 420   | 0     | 1.15   |
| WDC       | CL SN720 SDAQNT... | 1 TB   | A52B332E7939 | 420   | 0     | 1.15   |
| WDC       | CL SN720 SDAQNT... | 512 GB | 9137110A4A91 | 420   | 0     | 1.15   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 45ED7EF519FF | 420   | 0     | 1.15   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | A724E7571860 | 420   | 0     | 1.15   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | FA08DF7331A8 | 420   | 0     | 1.15   |
| Intel     | SSDPED1K750GA      | 752 GB | 0FD6D552954A | 420   | 0     | 1.15   |
| Dell      | Express Flash N... | 1.6 TB | 192AC02C907F | 420   | 0     | 1.15   |
| Dell      | Express Flash N... | 1.6 TB | 2D020B8E0C24 | 420   | 0     | 1.15   |
| Dell      | Express Flash N... | 1.6 TB | 432939179AB7 | 420   | 0     | 1.15   |
| Dell      | Express Flash N... | 1.6 TB | 61E42AAC8365 | 420   | 0     | 1.15   |
| Dell      | Express Flash N... | 1.6 TB | BA1C27DC0A79 | 420   | 0     | 1.15   |
| Dell      | Express Flash N... | 1.6 TB | C234C6D40045 | 420   | 0     | 1.15   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 1E2C595D04FC | 420   | 0     | 1.15   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | AC7A092EE7C3 | 420   | 0     | 1.15   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | C0DD3B979244 | 420   | 0     | 1.15   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | CD73D3D644F4 | 420   | 0     | 1.15   |
| Intel     | SSDPE2KX080T8      | 8 TB   | 17CA9FB1A3B4 | 420   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 4B91AC82CC2A | 420   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 9526B8AA6F5C | 420   | 0     | 1.15   |
| Intel     | SSDPE21K750GA      | 752 GB | F342925CC5DC | 420   | 0     | 1.15   |
| Intel     | SSDPED1K750GA      | 752 GB | 09949AB8345C | 420   | 0     | 1.15   |
| Intel     | SSDPED1K750GA      | 752 GB | AC3A39E6181E | 420   | 0     | 1.15   |
| Intel     | SSDPED1K750GA      | 752 GB | E1AEF76210D0 | 420   | 0     | 1.15   |
| Samsung   | MZPLL3T2HAJQ-00005 | 3.2 TB | 39C6AECF257B | 420   | 0     | 1.15   |
| Intel     | SSDPED1K750GA      | 752 GB | A8EDE4A5E74C | 420   | 0     | 1.15   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 2AB9C5EC7D75 | 420   | 0     | 1.15   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 7F2651273ED7 | 420   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 659C0DD3C6CD | 420   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | C30B7FA6AA37 | 420   | 0     | 1.15   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 1748D6E99E56 | 420   | 0     | 1.15   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 2AB9C5EC7D75 | 420   | 0     | 1.15   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 3A80BD312A46 | 420   | 0     | 1.15   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 7F2651273ED7 | 420   | 0     | 1.15   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 84C25246C40A | 420   | 0     | 1.15   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 853132E6ED1E | 420   | 0     | 1.15   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 9B55F61E80F9 | 420   | 0     | 1.15   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | A78E052AB54C | 420   | 0     | 1.15   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | BBF7EBA0413E | 420   | 0     | 1.15   |
| Intel     | SSDPE2KE032T8      | 3.2 TB | A15ACCE99C5E | 419   | 0     | 1.15   |
| Intel     | SSDPE2KE032T8      | 3.2 TB | ABBF0D3AF9F6 | 419   | 0     | 1.15   |
| Intel     | SSDPED1K750GA      | 752 GB | 109E9925C834 | 419   | 0     | 1.15   |
| Intel     | SSDPED1K750GA      | 752 GB | 3F234058605C | 419   | 0     | 1.15   |
| Intel     | SSDPED1K750GA      | 752 GB | 58D70D3D4BA4 | 419   | 0     | 1.15   |
| Intel     | SSDPED1K750GA      | 752 GB | AF181EDBAD98 | 419   | 0     | 1.15   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | FF567FC90310 | 419   | 0     | 1.15   |
| Toshiba   | KXG50ZNV512G       | 512 GB | 9E20B4BBE7DE | 419   | 0     | 1.15   |
| Toshiba   | KXG50ZNV512G       | 512 GB | BF84B2145E64 | 419   | 0     | 1.15   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 4E797D393278 | 419   | 0     | 1.15   |
| Intel     | SSDPED1K375GA      | 375 GB | 805BD0BC6241 | 419   | 0     | 1.15   |
| Intel     | SSDPEKKW512G8      | 512 GB | FC61814C1177 | 419   | 0     | 1.15   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 599A176B1CE5 | 419   | 0     | 1.15   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 95EC5A0F527A | 419   | 0     | 1.15   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | E5A055DFBBFE | 419   | 0     | 1.15   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | FCCAFE8ABA10 | 419   | 0     | 1.15   |
| WDC       | CL SN720 SDAQNT... | 512 GB | 44D2F7F3A5E1 | 419   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 1496AB90A734 | 419   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 3BA37672ED33 | 419   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 47A2B61EF776 | 419   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 4D25F8AF6D57 | 419   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 5953E11026C9 | 419   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 5D0E27280B4C | 419   | 0     | 1.15   |
| Intel     | SSDPED1D480GA      | 480 GB | 35B5D4A37A2D | 419   | 0     | 1.15   |
| Intel     | SSDPEKKA512G8      | 512 GB | 11F329E88A50 | 419   | 0     | 1.15   |
| WDC       | CL SN720 SDAQNT... | 1 TB   | B52E4E13F5F1 | 419   | 0     | 1.15   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 252446A56885 | 419   | 0     | 1.15   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 43013AF1DD80 | 419   | 0     | 1.15   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 5D40950FEED8 | 419   | 0     | 1.15   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 69070D82F053 | 419   | 0     | 1.15   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | C1B7D87EFC1A | 419   | 0     | 1.15   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | E8CD35771774 | 419   | 0     | 1.15   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 179034CDD611 | 419   | 0     | 1.15   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 1CF62923E1BE | 419   | 0     | 1.15   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 252446A56885 | 419   | 0     | 1.15   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 43013AF1DD80 | 419   | 0     | 1.15   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 5D40950FEED8 | 419   | 0     | 1.15   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 69070D82F053 | 419   | 0     | 1.15   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 6C9C3872A1CB | 419   | 0     | 1.15   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | C1B7D87EFC1A | 419   | 0     | 1.15   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | CD020E9FF96A | 419   | 0     | 1.15   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | E8CD35771774 | 419   | 0     | 1.15   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 7204BABBB35E | 419   | 0     | 1.15   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 99731B8F8D15 | 419   | 0     | 1.15   |
| Intel     | SSDPE2KX020T8      | 2 TB   | DEECD289DB86 | 419   | 0     | 1.15   |
| Intel     | SSDPE2KX020T8      | 2 TB   | E22C545A91DF | 419   | 0     | 1.15   |
| Intel     | SSDPEKKA512G8      | 512 GB | 969AF2CF397A | 419   | 0     | 1.15   |
| Intel     | SSDPE2KX010T8      | 1 TB   | 93D4E8C4E0FB | 419   | 0     | 1.15   |
| Intel     | SSDPE2KX010T8      | 1 TB   | 95520B5F1C20 | 419   | 0     | 1.15   |
| Intel     | SSDPE2KX010T8      | 1 TB   | EFF44B0DF5FA | 419   | 0     | 1.15   |
| Dell      | Express Flash N... | 1.6 TB | 0E5D7CE02DBB | 419   | 0     | 1.15   |
| Dell      | Express Flash N... | 1.6 TB | 1B4002F62ACB | 419   | 0     | 1.15   |
| Dell      | Express Flash N... | 1.6 TB | 49F48879692F | 419   | 0     | 1.15   |
| Intel     | SSDPE2KX010T8      | 1 TB   | 1B07F236C7D8 | 419   | 0     | 1.15   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 0ACF0C177F33 | 419   | 0     | 1.15   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 1DAD6BD365F6 | 419   | 0     | 1.15   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 37ED7370BB78 | 419   | 0     | 1.15   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 528F13EF3D6B | 419   | 0     | 1.15   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | C86598490B51 | 419   | 0     | 1.15   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | CA8F48981645 | 419   | 0     | 1.15   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | F94C19211EE7 | 419   | 0     | 1.15   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 473448966935 | 419   | 0     | 1.15   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | FFE5346F44F5 | 419   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 07D53C57E54D | 419   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 0E8C10DDC17A | 419   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 203531414DBE | 419   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 278A5FEA93EF | 419   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | A7562FA74D89 | 419   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | C82958836495 | 419   | 0     | 1.15   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 117745BEB822 | 419   | 0     | 1.15   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 1E8FC0EBCFB2 | 419   | 0     | 1.15   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 2507E1375AA4 | 419   | 0     | 1.15   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 31C728075293 | 419   | 0     | 1.15   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 441BF472CA70 | 419   | 0     | 1.15   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 5DA1509E6595 | 419   | 0     | 1.15   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 69FD8DBDB547 | 419   | 0     | 1.15   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 6B0359045A56 | 419   | 0     | 1.15   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 838949D524BD | 419   | 0     | 1.15   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 9221F78497B7 | 419   | 0     | 1.15   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 96218E687975 | 419   | 0     | 1.15   |
| Intel     | SSDPE2KX020T8      | 2 TB   | B200B5962EC0 | 419   | 0     | 1.15   |
| Intel     | SSDPE2KX020T8      | 2 TB   | C210FD5CBF77 | 419   | 0     | 1.15   |
| Intel     | SSDPE2KX020T8      | 2 TB   | D56377081740 | 419   | 0     | 1.15   |
| Intel     | SSDPE2KX020T8      | 2 TB   | DBA035C023FD | 419   | 0     | 1.15   |
| Intel     | SSDPE2KX020T8      | 2 TB   | DE7DA4D8BF60 | 419   | 0     | 1.15   |
| Intel     | SSDPE2KX020T8      | 2 TB   | E2B88F84F008 | 419   | 0     | 1.15   |
| Intel     | SSDPE2KX020T8      | 2 TB   | F3B764A82B33 | 419   | 0     | 1.15   |
| Intel     | SSDPE2KX020T8      | 2 TB   | FBA7532AC16A | 419   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 1A011BE07AB2 | 419   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 27BF42A5ED4B | 419   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 393D5EB206C7 | 419   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 43EC86AEE21B | 419   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 6E5941FEB4BF | 419   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | B7C31CA3F236 | 419   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | C3C32CED9CB8 | 419   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | E27B0597BD24 | 419   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | EB381FD7E4C7 | 419   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | F5FC077A02A9 | 419   | 0     | 1.15   |
| Intel     | SSDPE2KX010T8      | 1 TB   | 2786126E2526 | 419   | 0     | 1.15   |
| Intel     | SSDPE2KX010T8      | 1 TB   | 3B0BC7B12391 | 419   | 0     | 1.15   |
| Intel     | SSDPED1K375GA      | 375 GB | ADE891FE4FF2 | 419   | 0     | 1.15   |
| Intel     | SSDPED1K750GA      | 752 GB | 0B8AFD42F216 | 419   | 0     | 1.15   |
| Samsung   | MZVLB256HAHQ-00000 | 256 GB | 7FBABDEEFE25 | 419   | 0     | 1.15   |
| Samsung   | MZWLL1T6HAJQ-00005 | 1.6 TB | 05BAC84D4836 | 419   | 0     | 1.15   |
| Samsung   | MZWLL1T6HAJQ-00005 | 1.6 TB | 2438722AA1BE | 419   | 0     | 1.15   |
| Samsung   | MZWLL1T6HAJQ-00005 | 1.6 TB | 3DFCE5287146 | 419   | 0     | 1.15   |
| Samsung   | MZWLL1T6HAJQ-00005 | 1.6 TB | 65D06A7A2466 | 419   | 0     | 1.15   |
| Samsung   | MZWLL1T6HAJQ-00005 | 1.6 TB | 797C760DED67 | 419   | 0     | 1.15   |
| Samsung   | MZWLL1T6HAJQ-00005 | 1.6 TB | 7D0831B17BE8 | 419   | 0     | 1.15   |
| Samsung   | MZWLL1T6HAJQ-00005 | 1.6 TB | E0A8DE3E08F3 | 419   | 0     | 1.15   |
| Samsung   | MZWLL1T6HAJQ-00005 | 1.6 TB | E4C5325D2228 | 419   | 0     | 1.15   |
| WDC       | WUS3BA196C7P3E3    | 960 GB | D2E04DDE9E80 | 419   | 0     | 1.15   |
| Wester... | WUS3BA196C7P3E3    | 960 GB | D2E04DDE9E80 | 419   | 0     | 1.15   |
| Intel     | SSDPE21K750GA      | 752 GB | 8B37352BF0AA | 419   | 0     | 1.15   |
| Intel     | SSDPE21K750GA      | 752 GB | A577BA7250BC | 419   | 0     | 1.15   |
| Intel     | SSDPE21K750GA      | 752 GB | B7826AB8AF06 | 419   | 0     | 1.15   |
| Intel     | SSDPE21K750GA      | 752 GB | C8C9B840DECA | 419   | 0     | 1.15   |
| Samsung   | MZWLL1T6HAJQ-00005 | 1.6 TB | FAF86820DB0A | 419   | 0     | 1.15   |
| Intel     | SSDPED1K750GA      | 752 GB | 7E202887426A | 419   | 0     | 1.15   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 2E0C49B9EAF9 | 419   | 0     | 1.15   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 712C21EA0731 | 419   | 0     | 1.15   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 075ABF7A5E9B | 419   | 0     | 1.15   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 2E0C49B9EAF9 | 419   | 0     | 1.15   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 436FF10CE709 | 419   | 0     | 1.15   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 5ED05AAE9304 | 419   | 0     | 1.15   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 712C21EA0731 | 419   | 0     | 1.15   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 84154DC382B0 | 419   | 0     | 1.15   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | C6092E986231 | 419   | 0     | 1.15   |
| Intel     | SSDPED1K750GA      | 752 GB | 20A906D52511 | 419   | 0     | 1.15   |
| Intel     | SSDPED1K750GA      | 752 GB | 4CD3B7FB4D44 | 419   | 0     | 1.15   |
| Intel     | SSDPED1K750GA      | 752 GB | 581983D8A91A | 419   | 0     | 1.15   |
| WDC       | WUS3BA196C7P3E3    | 960 GB | DC6852F61097 | 419   | 0     | 1.15   |
| Wester... | WUS3BA196C7P3E3    | 960 GB | DC6852F61097 | 419   | 0     | 1.15   |
| Intel     | SSDPE2MX012T7      | 1.2 TB | 8FFE22C1A42F | 419   | 0     | 1.15   |
| Intel     | SSDPEDMW800G4      | 800 GB | 592C0AC49993 | 419   | 0     | 1.15   |
| WDC       | WUS3BA196C7P3E3    | 960 GB | 8D028EA17C3A | 419   | 0     | 1.15   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | AA3EED7B9867 | 419   | 0     | 1.15   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | BC021D77C6D8 | 419   | 0     | 1.15   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | CC9B819DCE93 | 419   | 0     | 1.15   |
| Wester... | WUS3BA196C7P3E3    | 960 GB | 8D028EA17C3A | 419   | 0     | 1.15   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 1460B77861C7 | 419   | 0     | 1.15   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 1F6F29BE546B | 419   | 0     | 1.15   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 24F2101EF07C | 419   | 0     | 1.15   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 91FD54A1A913 | 419   | 0     | 1.15   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | A5F88F729A3D | 419   | 0     | 1.15   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | AA3EED7B9867 | 419   | 0     | 1.15   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | AE81ECF18F46 | 419   | 0     | 1.15   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | BC021D77C6D8 | 419   | 0     | 1.15   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | CC9B819DCE93 | 419   | 0     | 1.15   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | DA255AA4C754 | 419   | 0     | 1.15   |
| Intel     | SSDPE21K750GA      | 752 GB | 6FD37F23DCC5 | 419   | 0     | 1.15   |
| Intel     | SSDPE21K750GA      | 752 GB | 7C2510FBCC94 | 419   | 0     | 1.15   |
| Intel     | SSDPE21K750GA      | 752 GB | 7D1909752C5C | 419   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 048C0C2AC1DB | 419   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 242371598770 | 419   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 53A63C5EDE2E | 419   | 0     | 1.15   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 665E0CF1113A | 419   | 0     | 1.15   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 70F417EADB1F | 419   | 0     | 1.15   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 7870FAC82EB1 | 419   | 0     | 1.15   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 7A3CD370279C | 419   | 0     | 1.15   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | CC690654371E | 419   | 0     | 1.15   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | FAC72FAF3D1A | 419   | 0     | 1.15   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | FE85FE3E74E0 | 419   | 0     | 1.15   |
| HP        | SSD EX950          | 2 TB   | 2A919208802A | 419   | 0     | 1.15   |
| Samsung   | MZSLW1T0HMLH-000L1 | 1 TB   | AC5D5D15CCFC | 419   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 6096BA100C71 | 419   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | E76DEC01490B | 419   | 0     | 1.15   |
| Intel     | SSDPED1K750GA      | 752 GB | 0C0F8466C783 | 419   | 0     | 1.15   |
| Samsung   | SSD 970 PRO        | 512 GB | 88DD34E24C54 | 419   | 0     | 1.15   |
| Samsung   | MZ1LB960HAJQ-00007 | 960 GB | 4B453DAF019A | 419   | 0     | 1.15   |
| Samsung   | SSD 970 PRO        | 1 TB   | 708C93E2B2D1 | 418   | 0     | 1.15   |
| Intel     | SSDPE2MX450G7      | 450 GB | 4C8A133E7F14 | 418   | 0     | 1.15   |
| Intel     | SSDPEKKW512G8      | 512 GB | AEFA02BCA018 | 418   | 0     | 1.15   |
| Intel     | SSDPED1K375GA      | 375 GB | 6005F4C51953 | 418   | 0     | 1.15   |
| Intel     | SSDPED1K375GA      | 375 GB | 77399542835B | 418   | 0     | 1.15   |
| Intel     | SSDPED1K750GA      | 752 GB | B1DA119B099C | 418   | 0     | 1.15   |
| Intel     | MT0800KEXUU        | 800 GB | ECBEEB2BB7CE | 418   | 0     | 1.15   |
| Intel     | SSDPED1K750GA      | 752 GB | D5CC35322904 | 418   | 0     | 1.15   |
| Micron    | 9300_MTFDHAL3T2TDR | 3.2 TB | 2FA38420268E | 418   | 0     | 1.15   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 8C06E94E75AE | 418   | 0     | 1.15   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | CE74C3F7F6F7 | 418   | 0     | 1.15   |
| Intel     | SSDPE21K750GA      | 752 GB | D115422D36D2 | 418   | 0     | 1.15   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 1247DF5F8EC1 | 418   | 0     | 1.15   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 5DAC39FA4FA9 | 418   | 0     | 1.15   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 601C46B5B498 | 418   | 0     | 1.15   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 8DA594DD3E64 | 418   | 0     | 1.15   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | A27EC57428C3 | 418   | 0     | 1.15   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 077D4151C3CB | 418   | 0     | 1.15   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 222B9A0FF5CC | 418   | 0     | 1.15   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 28CC2D709E35 | 418   | 0     | 1.15   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 5EAC92949870 | 418   | 0     | 1.15   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 8E860F20F9E6 | 418   | 0     | 1.15   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | A9B687970F78 | 418   | 0     | 1.15   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | D0F27114BDBE | 418   | 0     | 1.15   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | DFA8D471B7F2 | 418   | 0     | 1.15   |
| Corsair   | Force MP510 1.9TB  | 1.9 TB | C41DD9793A0F | 418   | 0     | 1.15   |
| Intel     | SSDPED1K375GA      | 375 GB | 866F6A3E7EDF | 418   | 0     | 1.15   |
| Intel     | SSDPEKNW020T8      | 2 TB   | 5EF56DEF80B3 | 418   | 0     | 1.15   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 8E8D860F0412 | 418   | 0     | 1.15   |
| WDC       | CL SN720 SDAQNT... | 512 GB | DB732FBEF961 | 418   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 16D399C5B92D | 418   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | A74218856079 | 418   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | D85CD9881AAA | 418   | 0     | 1.15   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 28864024A9B0 | 418   | 0     | 1.15   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 340C574A4AEB | 418   | 0     | 1.15   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | F22747ED4DE5 | 418   | 0     | 1.15   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | EDBAB1F03535 | 418   | 0     | 1.15   |
| Intel     | SSDPED1K750GA      | 752 GB | 8C9760714DD0 | 418   | 0     | 1.15   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 3D8C44F61A83 | 418   | 0     | 1.15   |
| WDC       | CL SN720 SDAQNT... | 1 TB   | A671C01D662A | 418   | 0     | 1.15   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 497F12B4E5F9 | 418   | 0     | 1.15   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | DA23DF970FB7 | 418   | 0     | 1.15   |
| Intel     | SSDPED1K750GA      | 752 GB | 31C0822C2456 | 418   | 0     | 1.15   |
| WDC       | WDS500G3XHC-00SJG0 | 500 GB | A20A01F65378 | 418   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 09D2C8E91EFE | 418   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 55E99CC20835 | 418   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 72A870877AD9 | 418   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 8DD9A264FE71 | 418   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | A0140E3EB292 | 418   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | B15795D2025C | 418   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | B724BE046D76 | 418   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | BA527628DFFA | 418   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | BBCB8DA416F1 | 418   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | C733B21B490A | 418   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | D7B9110B7A69 | 418   | 0     | 1.15   |
| Intel     | SSDPE2KX010T8      | 1 TB   | D1ABC8042B55 | 418   | 0     | 1.15   |
| Intel     | SSDPEDMW400G4      | 400 GB | 8AF8867543AD | 418   | 0     | 1.15   |
| Dell      | Express Flash N... | 1.6 TB | 9DB14F79717F | 418   | 0     | 1.15   |
| Dell      | Express Flash P... | 800 GB | 120ADF387FB6 | 418   | 0     | 1.15   |
| Intel     | SSDPEKKA512G8      | 512 GB | 6AB1BD8A4EB9 | 418   | 0     | 1.15   |
| Intel     | SSDPE2KE032T8      | 3.2 TB | 32AF35EFED4F | 418   | 0     | 1.15   |
| Intel     | SSDPE2KE032T8      | 3.2 TB | 6C214F37CF01 | 418   | 0     | 1.15   |
| WDC       | WUS3BA196C7P3E3    | 960 GB | 3FBFD3321121 | 418   | 0     | 1.15   |
| WDC       | WUS3BA196C7P3E3    | 960 GB | B755359CBCCE | 418   | 0     | 1.15   |
| WDC       | WUS3BA196C7P3E3    | 960 GB | BB7584F40B63 | 418   | 0     | 1.15   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 63F0449D9175 | 418   | 0     | 1.15   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | BA9E21AE9BB9 | 418   | 0     | 1.15   |
| Wester... | WUS3BA196C7P3E3    | 960 GB | 3FBFD3321121 | 418   | 0     | 1.15   |
| Wester... | WUS3BA196C7P3E3    | 960 GB | B755359CBCCE | 418   | 0     | 1.15   |
| Wester... | WUS3BA196C7P3E3    | 960 GB | BB7584F40B63 | 418   | 0     | 1.15   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 285A2B20584A | 418   | 0     | 1.15   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 63F0449D9175 | 418   | 0     | 1.15   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 66B5B62FE721 | 418   | 0     | 1.15   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 6D7EDFE4252A | 418   | 0     | 1.15   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | BA9E21AE9BB9 | 418   | 0     | 1.15   |
| Intel     | SSDPE2KE032T8      | 3.2 TB | 316511D55437 | 418   | 0     | 1.15   |
| Samsung   | MZVLB1T0HALR-00000 | 1 TB   | C78BE207A1C9 | 418   | 0     | 1.15   |
| Intel     | SSDPEDKE020T7      | 2 TB   | 16133DC6FF8B | 418   | 0     | 1.15   |
| Intel     | SSDPEKKA512G8      | 512 GB | 4E390E1C0034 | 418   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 4F584F434370 | 418   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 628DB5A337C9 | 418   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 65511FDC426A | 418   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 6ED199426335 | 418   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 751E3A96E150 | 418   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 7AB41E5DA75B | 418   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 87E28A83B4BF | 418   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | A20834E0D705 | 418   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | CB3D4ADDC6AE | 418   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | F77B79E565A1 | 418   | 0     | 1.15   |
| Intel     | SSDPE2KX010T8      | 1 TB   | 420207DAB58E | 418   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 204878757FF9 | 418   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 6564393C2089 | 418   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | C60784DE9BE5 | 418   | 0     | 1.15   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | F385FA59C0F0 | 418   | 0     | 1.15   |
| Intel     | SSDPED1K750GA      | 752 GB | 2CCF4729D99B | 418   | 0     | 1.15   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 3CDB3595D8C5 | 417   | 0     | 1.15   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | CF02AE7A3426 | 417   | 0     | 1.15   |
| WDC       | CL SN720 SDAQNT... | 1 TB   | FD1068BA2C9E | 417   | 0     | 1.15   |
| Intel     | SSDPE21K750GA      | 752 GB | D00B9F312A64 | 417   | 0     | 1.14   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 20E42686070C | 417   | 0     | 1.14   |
| Intel     | SSDPE2KX020T8      | 2 TB   | D6360F2CF521 | 417   | 0     | 1.14   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 034F0D5A4CFA | 417   | 0     | 1.14   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 20BE3BF4A15C | 417   | 0     | 1.14   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 79C655C17A21 | 417   | 0     | 1.14   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 874793C9458D | 417   | 0     | 1.14   |
| Intel     | SSDPE2KX020T8      | 2 TB   | B63552B03647 | 417   | 0     | 1.14   |
| WDC       | WDS500G3XHC-00SJG0 | 500 GB | 5B9BB5B0F7A6 | 417   | 0     | 1.14   |
| Intel     | SSDPE2KX020T8      | 2 TB   | A6DF316845AC | 417   | 0     | 1.14   |
| Intel     | SSDPE2KX020T8      | 2 TB   | CE11AD941CB3 | 417   | 0     | 1.14   |
| Intel     | SSDPE2KX020T8      | 2 TB   | DB79B51C78C2 | 417   | 0     | 1.14   |
| Intel     | SSDPEKKW512G8      | 512 GB | 7B0D8BE22D95 | 417   | 0     | 1.14   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 446F5CCFE9AB | 417   | 0     | 1.14   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | DC93E4D365BA | 417   | 0     | 1.14   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 0D8469903BB4 | 417   | 0     | 1.14   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 1F23170339C2 | 417   | 0     | 1.14   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 27C3F853813B | 417   | 0     | 1.14   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 4F224AC3F0A1 | 417   | 0     | 1.14   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 97AEA8094B2E | 417   | 0     | 1.14   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | CEEDA589A891 | 417   | 0     | 1.14   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | FC7557F326D2 | 417   | 0     | 1.14   |
| Intel     | SSDPE2KX010T8      | 1 TB   | 1313E4059B14 | 417   | 0     | 1.14   |
| Phison    | Viper M.2 VPN100   | 256 GB | 71392CCEC5A0 | 417   | 0     | 1.14   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | D481BF407A4D | 417   | 0     | 1.14   |
| Intel     | SSDPE2KX010T8      | 1 TB   | 3DED588F2299 | 417   | 0     | 1.14   |
| Intel     | SSDPE2KX010T8      | 1 TB   | 77923EF801D6 | 417   | 0     | 1.14   |
| Intel     | SSDPE2MX012T7      | 1.2 TB | DC73B0DD4197 | 417   | 0     | 1.14   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 7EFE1EBC49EA | 417   | 0     | 1.14   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | D2AC48C9FACF | 417   | 0     | 1.14   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 197C39CB702B | 417   | 0     | 1.14   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 33756960D9A1 | 417   | 0     | 1.14   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 37D9E54532C4 | 417   | 0     | 1.14   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 514218951F14 | 417   | 0     | 1.14   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | DBAF6B567B35 | 417   | 0     | 1.14   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | F185D7D29C1B | 417   | 0     | 1.14   |
| Intel     | SSDPE2KX010T8      | 1 TB   | 0447F11CD206 | 417   | 0     | 1.14   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 4CDAF1087834 | 417   | 0     | 1.14   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | B6B196867D25 | 417   | 0     | 1.14   |
| Intel     | SSDPE2KE032T8      | 3.2 TB | 7ABF586D57C6 | 417   | 0     | 1.14   |
| Intel     | SSDPE2KE032T8      | 3.2 TB | BD5540515DEA | 417   | 0     | 1.14   |
| WDC       | CL SN720 SDAQNT... | 1 TB   | 23DC5154C335 | 417   | 0     | 1.14   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 80CDE7256C92 | 417   | 0     | 1.14   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | A12E51974D5F | 417   | 0     | 1.14   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | A7C378592E4F | 417   | 0     | 1.14   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 05060DED7C5D | 417   | 0     | 1.14   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 76AA4BCC36D7 | 417   | 0     | 1.14   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 899A0CC92ADB | 417   | 0     | 1.14   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 95636F15951F | 417   | 0     | 1.14   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | B5F03E34981F | 417   | 0     | 1.14   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | E8F929F3E26B | 417   | 0     | 1.14   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | F9D4EF23CB9A | 417   | 0     | 1.14   |
| Intel     | SSDPE2KX080T8      | 8 TB   | D818BD20EAE5 | 417   | 0     | 1.14   |
| Intel     | MT0800KEXUU        | 800 GB | F02E12560EF8 | 417   | 0     | 1.14   |
| Intel     | SSDPE2KX010T8      | 1 TB   | D054434A968F | 417   | 0     | 1.14   |
| Samsung   | MZPJB480HMGC-0BW07 | 480 GB | D0A95ED148C7 | 417   | 0     | 1.14   |
| Samsung   | MZVLB512HAJQ-00000 | 512 GB | 193BC0764C03 | 417   | 0     | 1.14   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 1874B33728D6 | 417   | 0     | 1.14   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 21E0B6CBB1B4 | 417   | 0     | 1.14   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 547CBB1EE33A | 417   | 0     | 1.14   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 8C6C50CF27E5 | 417   | 0     | 1.14   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 993852742777 | 417   | 0     | 1.14   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 9B5303E45EDB | 417   | 0     | 1.14   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | E81272C5253F | 417   | 0     | 1.14   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | F6FCDCB935D6 | 417   | 0     | 1.14   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 8C5E0C356D32 | 417   | 0     | 1.14   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 8593A629DCAA | 417   | 0     | 1.14   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | EB6655DC803B | 417   | 0     | 1.14   |
| Intel     | SSDPE2KX010T8      | 1 TB   | 2376B9555CCE | 417   | 0     | 1.14   |
| Dell      | Express Flash P... | 1.6 TB | 983DEAEC61C8 | 417   | 0     | 1.14   |
| Intel     | SSDPED1K750GA      | 752 GB | D8E76A23D1D8 | 417   | 0     | 1.14   |
| Samsung   | MZVLB1T0HALR-00000 | 1 TB   | FF3091065157 | 417   | 0     | 1.14   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | A3A3B77116F3 | 417   | 0     | 1.14   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | D29DF71742E5 | 417   | 0     | 1.14   |
| Toshiba   | KXG50ZNV512G       | 512 GB | 1146481936E8 | 417   | 0     | 1.14   |
| Toshiba   | KXG50ZNV512G       | 512 GB | 60846BEE04BB | 417   | 0     | 1.14   |
| Dell      | Express Flash P... | 800 GB | 45962210EC13 | 417   | 0     | 1.14   |
| WDC       | WDS250G3X0C-00SJG0 | 250 GB | 5C6503754DDB | 417   | 0     | 1.14   |
| WDC       | WUS3BA196C7P3E3    | 960 GB | 2A2A5376642F | 417   | 0     | 1.14   |
| WDC       | WUS3BA196C7P3E3    | 960 GB | AAB0A74C662D | 417   | 0     | 1.14   |
| WDC       | WUS3BA196C7P3E3    | 960 GB | BDBEA03A2176 | 417   | 0     | 1.14   |
| WDC       | WUS3BA196C7P3E3    | 960 GB | DECE36AF3246 | 417   | 0     | 1.14   |
| WDC       | WUS3BA196C7P3E3    | 960 GB | E721F483600D | 417   | 0     | 1.14   |
| Wester... | WUS3BA196C7P3E3    | 960 GB | 0983012B5560 | 417   | 0     | 1.14   |
| Wester... | WUS3BA196C7P3E3    | 960 GB | 2278F4545132 | 417   | 0     | 1.14   |
| Wester... | WUS3BA196C7P3E3    | 960 GB | 2A2A5376642F | 417   | 0     | 1.14   |
| Wester... | WUS3BA196C7P3E3    | 960 GB | 942C90A72531 | 417   | 0     | 1.14   |
| Wester... | WUS3BA196C7P3E3    | 960 GB | AAB0A74C662D | 417   | 0     | 1.14   |
| Wester... | WUS3BA196C7P3E3    | 960 GB | BB20F93A7283 | 417   | 0     | 1.14   |
| Wester... | WUS3BA196C7P3E3    | 960 GB | BDBEA03A2176 | 417   | 0     | 1.14   |
| Wester... | WUS3BA196C7P3E3    | 960 GB | BEB6D917DBD0 | 417   | 0     | 1.14   |
| Wester... | WUS3BA196C7P3E3    | 960 GB | DECE36AF3246 | 417   | 0     | 1.14   |
| Wester... | WUS3BA196C7P3E3    | 960 GB | E721F483600D | 417   | 0     | 1.14   |
| Dell      | Express Flash P... | 1.6 TB | C2F48F891A85 | 417   | 0     | 1.14   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 1F0724034CEF | 417   | 0     | 1.14   |
| WDC       | CL SN720 SDAQNT... | 1 TB   | 64C6E71236D5 | 417   | 0     | 1.14   |
| Intel     | SSDPE2KX020T8      | 2 TB   | B66A3C4B3597 | 417   | 0     | 1.14   |
| Intel     | SSDPED1K375GA      | 375 GB | 09A910C20938 | 417   | 0     | 1.14   |
| Intel     | SSDPED1K375GA      | 375 GB | 89808F81185E | 417   | 0     | 1.14   |
| Intel     | SSDPEKNW020T8      | 2 TB   | 81742D73FD93 | 417   | 0     | 1.14   |
| WDC       | WUS3BA196C7P3E3    | 960 GB | B84C79E9755E | 417   | 0     | 1.14   |
| WDC       | WUS3BA196C7P3E3    | 960 GB | CBA3D03380A6 | 417   | 0     | 1.14   |
| WDC       | WUS3BA196C7P3E3    | 960 GB | EB620D47540C | 417   | 0     | 1.14   |
| Wester... | WUS3BA196C7P3E3    | 960 GB | B84C79E9755E | 417   | 0     | 1.14   |
| Wester... | WUS3BA196C7P3E3    | 960 GB | CBA3D03380A6 | 417   | 0     | 1.14   |
| Wester... | WUS3BA196C7P3E3    | 960 GB | EB620D47540C | 417   | 0     | 1.14   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 03A78299A75B | 417   | 0     | 1.14   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 0D1F538E3527 | 417   | 0     | 1.14   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 24B3FB820D89 | 417   | 0     | 1.14   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 8693CC786A55 | 417   | 0     | 1.14   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | FCDBBA09892D | 417   | 0     | 1.14   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 0BDBC94C7689 | 417   | 0     | 1.14   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 15E5D0348CD7 | 417   | 0     | 1.14   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 1673FA310CC4 | 417   | 0     | 1.14   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 181562F10CAF | 417   | 0     | 1.14   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 2B97569677F7 | 417   | 0     | 1.14   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 47C87133D0F6 | 417   | 0     | 1.14   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 4979480EFA7B | 417   | 0     | 1.14   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 6EA73323CDD8 | 417   | 0     | 1.14   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 8E5DEC71ACB5 | 417   | 0     | 1.14   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 985A0E1B0DD1 | 417   | 0     | 1.14   |
| Intel     | SSDPE2KX020T8      | 2 TB   | A30CDCCAE047 | 417   | 0     | 1.14   |
| Intel     | SSDPE2KX020T8      | 2 TB   | ACC1B617F0C2 | 417   | 0     | 1.14   |
| Intel     | SSDPE2KX020T8      | 2 TB   | D430BC23B136 | 417   | 0     | 1.14   |
| Intel     | SSDPE2KX020T8      | 2 TB   | DD949EF93E66 | 417   | 0     | 1.14   |
| Intel     | SSDPE2KX020T8      | 2 TB   | E4C3901C3F60 | 417   | 0     | 1.14   |
| Intel     | SSDPE2KX020T8      | 2 TB   | E6AD6BA3B27F | 417   | 0     | 1.14   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 1DA62CB16B30 | 417   | 0     | 1.14   |
| Wester... | WUS3BA138C7P3E3    | 3.8 TB | 30BF0C8326C1 | 417   | 0     | 1.14   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 77B6EBDC5B5A | 417   | 0     | 1.14   |
| Intel     | SSDPE2KX020T8      | 2 TB   | A494B2F760D6 | 417   | 0     | 1.14   |
| Intel     | SSDPE2KX020T8      | 2 TB   | F11D57B65D14 | 417   | 0     | 1.14   |
| Phison    | Viper M.2 VPN100   | 512 GB | 7A10DE5F380C | 417   | 0     | 1.14   |
| Intel     | SSDPE2KX010T8      | 1 TB   | 9C91C467A076 | 417   | 0     | 1.14   |
| Intel     | SSDPE2KX010T8      | 1 TB   | DC4D17B1AB3B | 417   | 0     | 1.14   |
| Intel     | SSDPE2KE032T8      | 3.2 TB | 23FAC3EFA051 | 416   | 0     | 1.14   |
| Intel     | SSDPE2KE032T8      | 3.2 TB | 2B8E5552B6CE | 416   | 0     | 1.14   |
| Intel     | SSDPE21K750GA      | 752 GB | 160048FED171 | 416   | 0     | 1.14   |
| Intel     | SSDPE21K750GA      | 752 GB | A51DFDC0F750 | 416   | 0     | 1.14   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | F76F2357E4BC | 416   | 0     | 1.14   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 210288C4106C | 416   | 0     | 1.14   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 2279CA476454 | 416   | 0     | 1.14   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 7A6AB274B4FB | 416   | 0     | 1.14   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 7CA36C092BC6 | 416   | 0     | 1.14   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | A82F75BF62A0 | 416   | 0     | 1.14   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | E83968622B99 | 416   | 0     | 1.14   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | F76F2357E4BC | 416   | 0     | 1.14   |
| Intel     | SSDPE2KX010T8      | 1 TB   | AF007727936D | 416   | 0     | 1.14   |
| Intel     | SSDPE2KX010T8      | 1 TB   | B6C400E65450 | 416   | 0     | 1.14   |
| Intel     | SSDPE2KX010T8      | 1 TB   | ED2CBCBF24FF | 416   | 0     | 1.14   |
| Intel     | SSDPED1K375GA      | 375 GB | D037EA7B82D2 | 416   | 0     | 1.14   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 71E612F600E9 | 416   | 0     | 1.14   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 8321A6C9C5AE | 416   | 0     | 1.14   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | A613F04E5F05 | 416   | 0     | 1.14   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 0647414C4B73 | 416   | 0     | 1.14   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 3E855DB9D250 | 416   | 0     | 1.14   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 4CFCAC58462A | 416   | 0     | 1.14   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 6B6ABE255D7A | 416   | 0     | 1.14   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 746C5DFB3B56 | 416   | 0     | 1.14   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | BE26D8C36222 | 416   | 0     | 1.14   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | C6E455E15124 | 416   | 0     | 1.14   |
| Intel     | SSDPE2KX010T8      | 1 TB   | 559FB13B3041 | 416   | 0     | 1.14   |
| Samsung   | MZPLL6T4HMLS-00003 | 6.4 TB | 801184EDC7BB | 416   | 0     | 1.14   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 39A1912E04C7 | 416   | 0     | 1.14   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | DA702183E2CE | 416   | 0     | 1.14   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 023E1F4B4EEC | 416   | 0     | 1.14   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 11FE6552B989 | 416   | 0     | 1.14   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 2001DDE71C43 | 416   | 0     | 1.14   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 24FB96758FEE | 416   | 0     | 1.14   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 702C2A86CBC3 | 416   | 0     | 1.14   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 9DFC148B98D8 | 416   | 0     | 1.14   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | A29DD028B7D1 | 416   | 0     | 1.14   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | A664708ACEAF | 416   | 0     | 1.14   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | B1A6AE7BCC97 | 416   | 0     | 1.14   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | B9A1C5B9C4F7 | 416   | 0     | 1.14   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | BA90911B6925 | 416   | 0     | 1.14   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | BC5EAF4DE658 | 416   | 0     | 1.14   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | E4FCEEF4CA9D | 416   | 0     | 1.14   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 0517E0338B31 | 416   | 0     | 1.14   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 0FED56943720 | 416   | 0     | 1.14   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 2375FE630995 | 416   | 0     | 1.14   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 23C036C90334 | 416   | 0     | 1.14   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 44FC8F40ADBB | 416   | 0     | 1.14   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | BF4E0C832B6D | 416   | 0     | 1.14   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | BFB450979517 | 416   | 0     | 1.14   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 39A1912E04C7 | 416   | 0     | 1.14   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | DA702183E2CE | 416   | 0     | 1.14   |
| Intel     | SSDPE2KX010T8      | 1 TB   | 0096FCEF40B1 | 416   | 0     | 1.14   |
| Intel     | SSDPE2KX010T8      | 1 TB   | 7A849EBA1F8B | 416   | 0     | 1.14   |
| Intel     | SSDPE2KX010T8      | 1 TB   | D2A842D44B62 | 416   | 0     | 1.14   |
| Intel     | SSDPED1K750GA      | 752 GB | 911A28CA2712 | 416   | 0     | 1.14   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 3616983FA6D6 | 416   | 0     | 1.14   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 4A72F3DDE077 | 416   | 0     | 1.14   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 51EE85BA55BF | 416   | 0     | 1.14   |
| Samsung   | SSD 970 EVO Plus   | 1 TB   | C21FB6F049F9 | 416   | 0     | 1.14   |
| Intel     | SSDPE2KX010T8      | 1 TB   | 597347B36A99 | 416   | 0     | 1.14   |
| Intel     | SSDPE2KX010T8      | 1 TB   | 98E9997A151D | 416   | 0     | 1.14   |
| Intel     | SSDPE2KX010T8      | 1 TB   | 60CF4735D32A | 416   | 0     | 1.14   |
| Intel     | SSDPE2KX010T8      | 1 TB   | CC4A7081C7E6 | 416   | 0     | 1.14   |
| Intel     | SSDPEDMW800G4      | 800 GB | 1D064505C26F | 416   | 0     | 1.14   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 188BCB762301 | 416   | 0     | 1.14   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 3DA60821B716 | 416   | 0     | 1.14   |
| Samsung   | SSD 970 PRO        | 512 GB | 065F0C1B0615 | 416   | 0     | 1.14   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | B96B85100F52 | 414   | 0     | 1.14   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 74D067F2D71E | 411   | 0     | 1.13   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | DE323DB871EC | 411   | 0     | 1.13   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | B3CBCAB9A92A | 409   | 0     | 1.12   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | DA9B85D8A29D | 409   | 0     | 1.12   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 6BAB82675699 | 408   | 0     | 1.12   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | A95AD53E41A8 | 407   | 0     | 1.12   |
| Intel     | SSDPE2KX080T8      | 8 TB   | 489A78950AEB | 407   | 0     | 1.12   |
| Wester... | WUS3BA196C7P3E3    | 960 GB | 70D5A0C9C33B | 407   | 0     | 1.12   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 2A82FD13ADE6 | 406   | 0     | 1.11   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 96E52B17DDEE | 406   | 0     | 1.11   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | D0C692BE0844 | 406   | 0     | 1.11   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | FD9FEB4B2CC6 | 406   | 0     | 1.11   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 33756960D9A1 | 405   | 0     | 1.11   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 8517A408DFB8 | 405   | 0     | 1.11   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | D918F51FB993 | 405   | 0     | 1.11   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 6F143D204B7A | 405   | 0     | 1.11   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 79CF3AB002FD | 405   | 0     | 1.11   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | CE15F19B5150 | 405   | 0     | 1.11   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | DE41CAD9DDB2 | 405   | 0     | 1.11   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 5D45FB9D249C | 405   | 0     | 1.11   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | C277168EA530 | 405   | 0     | 1.11   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | EB221258EC38 | 405   | 0     | 1.11   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 5EFCC548ACB9 | 405   | 0     | 1.11   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | AB59A93C0070 | 405   | 0     | 1.11   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 4DA529412016 | 404   | 0     | 1.11   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 6C3FB4698E42 | 404   | 0     | 1.11   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 9B1AA63F58C3 | 404   | 0     | 1.11   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | C4F2DA5E3912 | 404   | 0     | 1.11   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 1C0D26E7E6E5 | 404   | 0     | 1.11   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 6B986FAC2ABD | 404   | 0     | 1.11   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 063C4EA0D05A | 403   | 0     | 1.11   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 3EC9BBF3CADB | 403   | 0     | 1.11   |
| Dell      | Express Flash C... | 960 GB | CB644976F3CC | 403   | 0     | 1.11   |
| Dell      | Express Flash C... | 960 GB | B3919D72B2D0 | 403   | 0     | 1.11   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 0AF1FB133217 | 400   | 0     | 1.10   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 3601449C946D | 400   | 0     | 1.10   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 4BEE4840D7DA | 400   | 0     | 1.10   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | AA92C28A36E5 | 400   | 0     | 1.10   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | C15C2AB0EC1D | 400   | 0     | 1.10   |
| Intel     | SSDPE2KX080T8      | 8 TB   | 5C806A3FE863 | 400   | 0     | 1.10   |
| Intel     | MT0800KEXUU        | 800 GB | AF396C029379 | 398   | 0     | 1.09   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 63F0698CF4FD | 397   | 0     | 1.09   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 8280B9FE5C0C | 397   | 0     | 1.09   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 230F2D5B9760 | 397   | 0     | 1.09   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 45D9AB875455 | 397   | 0     | 1.09   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 1519A60F85A6 | 397   | 0     | 1.09   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | F2F372E8BFB9 | 397   | 0     | 1.09   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 388B2AD4A366 | 397   | 0     | 1.09   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | DBAF6B567B35 | 397   | 0     | 1.09   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 1F91A5BFB3A0 | 396   | 0     | 1.09   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 06DB750EBB29 | 395   | 0     | 1.08   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 311CAB552038 | 395   | 0     | 1.08   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 5EC9F558AD90 | 395   | 0     | 1.08   |
| Toshiba   | KXG60ZNV512G       | 512 GB | DFBCE0BDC749 | 395   | 0     | 1.08   |
| Samsung   | MZVLB512HAJQ-00000 | 512 GB | EC42530E647E | 395   | 0     | 1.08   |
| WDC       | WUS3BA138C7P3E3    | 3.8 TB | 65A2435D46D1 | 395   | 0     | 1.08   |
| Wester... | WUS3BA138C7P3E3    | 3.8 TB | 65A2435D46D1 | 395   | 0     | 1.08   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 698E47948DEF | 395   | 0     | 1.08   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 55C7DF1F4FD3 | 394   | 0     | 1.08   |
| Wester... | WUS3BA196C7P3E3    | 960 GB | 5E1327AFC457 | 393   | 0     | 1.08   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 542A52718391 | 393   | 0     | 1.08   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 5E6C62A7CA81 | 393   | 0     | 1.08   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | A7F57E4FCD40 | 393   | 0     | 1.08   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | E1E1818CC54B | 393   | 0     | 1.08   |
| Samsung   | MZQLW1T9HMJP-000AZ | 1.9 TB | 054D16C8F56C | 392   | 0     | 1.08   |
| Samsung   | MZVLB1T0HBLR-00000 | 1 TB   | 372C3EE649E3 | 392   | 0     | 1.08   |
| Samsung   | MZVLB1T0HBLR-00000 | 1 TB   | ABDF212F0B96 | 392   | 0     | 1.07   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 353DB8ABA6B6 | 392   | 0     | 1.07   |
| Intel     | SSDPEDMW800G4      | 800 GB | 0401FC215504 | 391   | 0     | 1.07   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 289ED0EAFA24 | 390   | 0     | 1.07   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | BF76B5710BF7 | 390   | 0     | 1.07   |
| Toshiba   | KXG60ZNV1T02       | 1 TB   | 876FB676E26A | 389   | 0     | 1.07   |
| Toshiba   | KXG60ZNV1T02       | 1 TB   | CDF1DE5E6A2A | 389   | 0     | 1.07   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 381BDA2C444A | 389   | 0     | 1.07   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | F35A21E25AA7 | 389   | 0     | 1.07   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 5044D72DEBC2 | 389   | 0     | 1.07   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | C2486D7619BE | 389   | 0     | 1.07   |
| Gigabyte  | GP-GSM2NE3100TNTD  | 1 TB   | 867598DB238C | 389   | 0     | 1.07   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 7D483CA0F397 | 388   | 0     | 1.07   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | B3882BFB92A1 | 388   | 0     | 1.07   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | B895FAC763B1 | 388   | 0     | 1.07   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | E7D769F68519 | 388   | 0     | 1.07   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | F81F735235FE | 388   | 0     | 1.06   |
| Toshiba   | KXG60ZNV1T02       | 1 TB   | 76936760C236 | 388   | 0     | 1.06   |
| Dell      | Express Flash N... | 1.6 TB | 0B19E7218585 | 387   | 0     | 1.06   |
| Wester... | WUS3BA196C7P3E3    | 960 GB | 26F88B2D884F | 387   | 0     | 1.06   |
| Wester... | WUS3BA196C7P3E3    | 960 GB | 77D7920C1E21 | 387   | 0     | 1.06   |
| Wester... | WUS3BA196C7P3E3    | 960 GB | 93FDFA50DBAA | 387   | 0     | 1.06   |
| Wester... | WUS3BA196C7P3E3    | 960 GB | 9AE9B362ECC4 | 387   | 0     | 1.06   |
| Wester... | WUS3BA196C7P3E3    | 960 GB | A06BD6DFE2ED | 387   | 0     | 1.06   |
| Wester... | WUS3BA196C7P3E3    | 960 GB | CBB0D491614B | 387   | 0     | 1.06   |
| Wester... | WUS3BA196C7P3E3    | 960 GB | D0E34B544D01 | 387   | 0     | 1.06   |
| Wester... | WUS3BA196C7P3E3    | 960 GB | D3B5A272DE1E | 387   | 0     | 1.06   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 544410E43956 | 386   | 0     | 1.06   |
| Micron    | 9300_MTFDHAL3T2TDR | 3.2 TB | CCB32215EB72 | 385   | 0     | 1.06   |
| Intel     | SSDPE2KX010T8      | 1 TB   | 5E47D45AEDD6 | 385   | 0     | 1.06   |
| Gigabyte  | GP-GSM2NE3100TNTD  | 1 TB   | 6A640BEA8979 | 385   | 0     | 1.06   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 59EEEAF5AA56 | 384   | 0     | 1.05   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 797AF10B6887 | 384   | 0     | 1.05   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 1A7AE880E228 | 384   | 0     | 1.05   |
| Dell      | Express Flash C... | 960 GB | 2A4A52F0E1E7 | 383   | 0     | 1.05   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 97AEA8094B2E | 383   | 0     | 1.05   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | C15458E6A8DB | 383   | 0     | 1.05   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | DDA94E177C93 | 383   | 0     | 1.05   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 39901CC07FB8 | 382   | 0     | 1.05   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | CBAA3FF46A46 | 382   | 0     | 1.05   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 9F7AC7A62FD1 | 382   | 0     | 1.05   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 20FB21DBEC28 | 382   | 0     | 1.05   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 11C3630EA3B2 | 382   | 0     | 1.05   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 2FCF4E52EB08 | 382   | 0     | 1.05   |
| Intel     | SSDPE21K100GA      | 100 GB | C62F580A30D9 | 382   | 0     | 1.05   |
| Intel     | SSDPE2KX010T8      | 1 TB   | 039DAD7F248E | 381   | 0     | 1.05   |
| Intel     | SSDPE2KX010T8      | 1 TB   | E591337C1914 | 381   | 0     | 1.05   |
| Toshiba   | KXG60ZNV1T02       | 1 TB   | 6F7756F1191B | 381   | 0     | 1.05   |
| Toshiba   | KXG60ZNV512G       | 512 GB | 05D311AFEE66 | 380   | 0     | 1.04   |
| Samsung   | SSD 970 PRO        | 512 GB | ED67AEE3B735 | 380   | 0     | 1.04   |
| Toshiba   | KXG60ZNV1T02       | 1 TB   | 7E67843451FE | 380   | 0     | 1.04   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | A18BF34DCE08 | 380   | 0     | 1.04   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | A5B41C70713B | 380   | 0     | 1.04   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | FC3ECC83E917 | 380   | 0     | 1.04   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 49C89E725502 | 379   | 0     | 1.04   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 96AC78C0F135 | 378   | 0     | 1.04   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | D8BCE0B84750 | 378   | 0     | 1.04   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 26E207B725C4 | 377   | 0     | 1.04   |
| Samsung   | MZPLL3T2HAJQ-00005 | 3.2 TB | 18FDB1A49513 | 377   | 0     | 1.03   |
| Dell      | Express Flash N... | 1.6 TB | B82DFD5083F4 | 377   | 0     | 1.03   |
| Dell      | Express Flash N... | 1.6 TB | 74BA86A86E88 | 376   | 0     | 1.03   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 45ED1E7A66BF | 376   | 0     | 1.03   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 4F224AC3F0A1 | 376   | 0     | 1.03   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | FC7557F326D2 | 376   | 0     | 1.03   |
| Toshiba   | KXG60ZNV512G       | 512 GB | ED772AA5DAE9 | 376   | 0     | 1.03   |
| Samsung   | MZPLL3T2HAJQ-00005 | 3.2 TB | AE599E1A85E0 | 376   | 0     | 1.03   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 9B36EF0F5A34 | 376   | 0     | 1.03   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 5A54A19B7D43 | 375   | 0     | 1.03   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 3680979CCD66 | 375   | 0     | 1.03   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | D30335EFDC11 | 375   | 0     | 1.03   |
| Toshiba   | KXG60ZNV512G       | 512 GB | 7EF393DF6723 | 375   | 0     | 1.03   |
| Samsung   | SSD 970 PRO        | 512 GB | 776141F6D95B | 375   | 0     | 1.03   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 3DE48FFDF707 | 375   | 0     | 1.03   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | FC02D4E95153 | 374   | 0     | 1.03   |
| Intel     | SSDPEL1K100GA      | 100 GB | 7979FBB53B7B | 373   | 0     | 1.02   |
| Toshiba   | KXG60ZNV512G       | 512 GB | D9DFF0899DAA | 373   | 0     | 1.02   |
| ADATA     | SX8200PNP          | 512 GB | 462265DE2E28 | 373   | 0     | 1.02   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | F011BF10A623 | 373   | 0     | 1.02   |
| Toshiba   | KXG60ZNV512G       | 512 GB | 459B30212380 | 373   | 0     | 1.02   |
| Intel     | SSDPE21K100GA      | 100 GB | 24C81FA6E608 | 372   | 0     | 1.02   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 31805C57DE85 | 372   | 0     | 1.02   |
| HP        | SSD EX950          | 2 TB   | 2155677D5D93 | 371   | 0     | 1.02   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | A050A4DCACE6 | 371   | 0     | 1.02   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | B927D473A2C7 | 371   | 0     | 1.02   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 5C937332C0A1 | 370   | 0     | 1.02   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | EE59B189A73C | 370   | 0     | 1.02   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | C1AC0F3F0379 | 370   | 0     | 1.01   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 5CF737DDFC7E | 370   | 0     | 1.01   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 0EFF3EE16370 | 370   | 0     | 1.01   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 28C9DCC095BE | 370   | 0     | 1.01   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 4B59E6962FEC | 370   | 0     | 1.01   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | D538A467FA02 | 370   | 0     | 1.01   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | DCB6867C2AC6 | 370   | 0     | 1.01   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 4E7562AFDD08 | 370   | 0     | 1.01   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 9F8B614F4753 | 369   | 0     | 1.01   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | A99383253CF1 | 369   | 0     | 1.01   |
| WDC       | WUS4BB038D7P3E3    | 3.8 TB | 06321A12B335 | 369   | 0     | 1.01   |
| WDC       | WUS4BB038D7P3E3    | 3.8 TB | 306F30F871B5 | 369   | 0     | 1.01   |
| WDC       | WUS4BB038D7P3E3    | 3.8 TB | E5D9C1CCA153 | 369   | 0     | 1.01   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | A5DC5FEA7420 | 369   | 0     | 1.01   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 286F35219D25 | 369   | 0     | 1.01   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 4EE72B1D0770 | 369   | 0     | 1.01   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | F598A9F54DF6 | 369   | 0     | 1.01   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | C5B91D160C53 | 369   | 0     | 1.01   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 26F8F16924EE | 369   | 0     | 1.01   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 3E57EC9A72A2 | 369   | 0     | 1.01   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 4B1C3614383A | 369   | 0     | 1.01   |
| Toshiba   | KXG60ZNV1T02       | 1 TB   | 24BBBE8C5596 | 368   | 0     | 1.01   |
| Toshiba   | KXG60ZNV1T02       | 1 TB   | 74BF6FE74C8B | 368   | 0     | 1.01   |
| Intel     | SSDPE21K100GA      | 100 GB | AF6E66FC58E2 | 368   | 0     | 1.01   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 9F92A2FBD06C | 368   | 0     | 1.01   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 8D2ABFE71D83 | 368   | 0     | 1.01   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 63F2886E17EA | 368   | 0     | 1.01   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | BC486ED833E6 | 368   | 0     | 1.01   |
| Intel     | SSDPE21K100GA      | 100 GB | E297D61D42F7 | 368   | 0     | 1.01   |
| Intel     | SSDPEDMW800G4      | 800 GB | E77254ED3838 | 368   | 0     | 1.01   |
| Intel     | SSDPEKKA512G8      | 512 GB | 862CA9F2D066 | 368   | 0     | 1.01   |
| Intel     | SSDPEKKW256G7      | 256 GB | 00D05E9F9691 | 367   | 0     | 1.01   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 0CC1F3372400 | 367   | 0     | 1.01   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 182E33256E4C | 367   | 0     | 1.01   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 3CEC2629939C | 367   | 0     | 1.01   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 714B70A00BFC | 367   | 0     | 1.01   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 7AEA354E6C73 | 367   | 0     | 1.01   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 7C154D6CB8F6 | 367   | 0     | 1.01   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | E7AE7087C5E8 | 367   | 0     | 1.01   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 3AB46CEF4C55 | 367   | 0     | 1.01   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 5D047D168C80 | 367   | 0     | 1.01   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 61AE2362061A | 367   | 0     | 1.01   |
| Kingston  | SA2000M81000G      | 1 TB   | 13D4D669B291 | 367   | 0     | 1.01   |
| Samsung   | SSD 970 PRO        | 1 TB   | 1D61F5234DFA | 367   | 0     | 1.01   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 004A12AE3542 | 366   | 0     | 1.01   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 71656536A16A | 366   | 0     | 1.01   |
| Toshiba   | KXG60ZNV1T02       | 1 TB   | D8289A7CDC1E | 366   | 0     | 1.00   |
| HP        | SSD EX950          | 2 TB   | 4A930056FE71 | 366   | 0     | 1.00   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | CAD03E889551 | 366   | 0     | 1.00   |
| Intel     | SSDPE2MX012T7      | 1.2 TB | 2EA5E9743706 | 366   | 0     | 1.00   |
| Samsung   | MZVLB1T0HALR-00000 | 1 TB   | EFD3350E41D7 | 366   | 0     | 1.00   |
| Intel     | SSDPEKKA512G8      | 512 GB | 589609FD842A | 366   | 0     | 1.00   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 7581C528FF5D | 366   | 0     | 1.00   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | F5E1C5F8924A | 366   | 0     | 1.00   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 3BAC0E8D0D02 | 366   | 0     | 1.00   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | C2608685EBEF | 366   | 0     | 1.00   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 9D6CDBC70DDA | 365   | 0     | 1.00   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | E649C6F9E375 | 365   | 0     | 1.00   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 110E3864D94C | 365   | 0     | 1.00   |
| Samsung   | MZVLB512HAJQ-00000 | 512 GB | 7B7910245173 | 365   | 0     | 1.00   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 6A0087C82DDA | 365   | 0     | 1.00   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 98B6F1160637 | 365   | 0     | 1.00   |
| WDC       | WUS4BB038D7P3E3    | 3.8 TB | 00B59D49CD3F | 365   | 0     | 1.00   |
| WDC       | WUS4BB038D7P3E3    | 3.8 TB | BCF1EA02D906 | 365   | 0     | 1.00   |
| WDC       | WUS4BB038D7P3E3    | 3.8 TB | C7453B4AFF9E | 365   | 0     | 1.00   |
| Intel     | SSDPEKKA512G8      | 512 GB | C934D4FCED14 | 364   | 0     | 1.00   |
| Intel     | SSDPED1D480GA      | 480 GB | F1A5ADE52CF7 | 364   | 0     | 1.00   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 7DA478101722 | 364   | 0     | 1.00   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 8FC24A6656F8 | 364   | 0     | 1.00   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 11FE6552B989 | 364   | 0     | 1.00   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 702C2A86CBC3 | 364   | 0     | 1.00   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | A29DD028B7D1 | 364   | 0     | 1.00   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | BC5EAF4DE658 | 364   | 0     | 1.00   |
| Dell      | Express Flash N... | 1.6 TB | 2946FA2DA184 | 363   | 0     | 1.00   |
| Dell      | Express Flash N... | 1.6 TB | BD8D4884543A | 363   | 0     | 1.00   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 4EAB8F2CB57E | 363   | 0     | 1.00   |
| Samsung   | SSD 970 PRO        | 512 GB | D6343B5D2245 | 363   | 0     | 1.00   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 44C1BBE765F6 | 363   | 0     | 1.00   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 0D8469903BB4 | 363   | 0     | 1.00   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 1F23170339C2 | 363   | 0     | 1.00   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 44B20C61AD9E | 363   | 0     | 1.00   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | CEEDA589A891 | 363   | 0     | 1.00   |
| WDC       | WUS4BB038D7P3E3    | 3.8 TB | 73D380659333 | 363   | 0     | 1.00   |
| WDC       | WUS4BB038D7P3E3    | 3.8 TB | 7B45EE03F6AB | 363   | 0     | 1.00   |
| WDC       | WUS4BB038D7P3E3    | 3.8 TB | 91FE8A838D8D | 363   | 0     | 1.00   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 0FE8CBDAD65B | 363   | 0     | 1.00   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | D17D3A438138 | 363   | 0     | 1.00   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | E2B9B634A1EB | 362   | 0     | 0.99   |
| Samsung   | SSD 970 EVO Plus   | 1 TB   | CAA91AB4ECCD | 362   | 0     | 0.99   |
| Samsung   | SSD 970 EVO Plus   | 1 TB   | AF0A169A4A1A | 362   | 0     | 0.99   |
| Phison    | PCIe SSD           | 256 GB | FBDC614FEBF3 | 362   | 0     | 0.99   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | AF9B5F839DE8 | 362   | 0     | 0.99   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | CF0C1C8C3CC3 | 361   | 0     | 0.99   |
| Samsung   | MZVLB1T0HALR-00000 | 1 TB   | 195D68E367DD | 361   | 0     | 0.99   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 48B67E5112DF | 361   | 0     | 0.99   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | E06E7C7BD502 | 361   | 0     | 0.99   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 357FB70E9400 | 360   | 0     | 0.99   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 964844F76413 | 360   | 0     | 0.99   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | C41F19610AE9 | 360   | 0     | 0.99   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 174FCD7CF6E4 | 360   | 0     | 0.99   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 20A862C9BE4C | 360   | 0     | 0.99   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 2BABBE6071CE | 360   | 0     | 0.99   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 49E8EBECD9A3 | 360   | 0     | 0.99   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 6CF6D7BC2EC9 | 359   | 0     | 0.99   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 55E99CC20835 | 359   | 0     | 0.99   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | B15795D2025C | 359   | 0     | 0.99   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 022196A90EE7 | 359   | 0     | 0.98   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 2A862431AEDA | 359   | 0     | 0.98   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 8C12D87AB7FE | 359   | 0     | 0.98   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | FA003F230B06 | 359   | 0     | 0.98   |
| Intel     | SSDPEL1K100GA      | 100 GB | 40025C94AE93 | 358   | 0     | 0.98   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 2107F6B1278E | 358   | 0     | 0.98   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 9CC336C6AAFC | 358   | 0     | 0.98   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 0B4937643A99 | 358   | 0     | 0.98   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 5BBDFC193B93 | 358   | 0     | 0.98   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 6096BA100C71 | 358   | 0     | 0.98   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 66B65502F307 | 358   | 0     | 0.98   |
| Samsung   | MZPJB480HMGC-0BW07 | 480 GB | 83FBCB7045FE | 357   | 0     | 0.98   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 5617C7D6F8EB | 357   | 0     | 0.98   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | FBEF775C2F6E | 357   | 0     | 0.98   |
| Dell      | Express Flash N... | 1.6 TB | 714C52EF97EE | 357   | 0     | 0.98   |
| Dell      | Express Flash N... | 1.6 TB | B30AFB667E3F | 357   | 0     | 0.98   |
| Intel     | SSDPEKKA256G7      | 256 GB | B56BDF8234EB | 356   | 0     | 0.98   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 37D9E54532C4 | 356   | 0     | 0.98   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 53E76C34EE3D | 356   | 0     | 0.98   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 263A0CECEB2C | 356   | 0     | 0.98   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 31CFFF2A7AD1 | 356   | 0     | 0.98   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 263A0CECEB2C | 356   | 0     | 0.98   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 31CFFF2A7AD1 | 356   | 0     | 0.98   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 2549C818780E | 356   | 0     | 0.98   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | B929CCF1B7D7 | 356   | 0     | 0.98   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 5E4061D53D30 | 355   | 0     | 0.98   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 99728A4EAA94 | 355   | 0     | 0.98   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | B1A6AE7BCC97 | 355   | 0     | 0.97   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | E4FCEEF4CA9D | 355   | 0     | 0.97   |
| Gigabyte  | GP-GSM2NE3100TNTD  | 1 TB   | 221E0D9059CA | 355   | 0     | 0.97   |
| Micron    | 9300_MTFDHAL7T6TDP | 7.6 TB | 1CCC0F26FF15 | 355   | 0     | 0.97   |
| Micron    | 9300_MTFDHAL7T6TDP | 7.6 TB | C7E9F6EAB64A | 355   | 0     | 0.97   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 4276F393B7FE | 355   | 0     | 0.97   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 7175E2FD8D5B | 355   | 0     | 0.97   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | A7562FA74D89 | 355   | 0     | 0.97   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | A8745A052DA0 | 355   | 0     | 0.97   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | C871CE121A2B | 355   | 0     | 0.97   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | FB3AB8FE5E4F | 355   | 0     | 0.97   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 2001DDE71C43 | 355   | 0     | 0.97   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | BA90911B6925 | 355   | 0     | 0.97   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 0517E0338B31 | 355   | 0     | 0.97   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | BFB450979517 | 355   | 0     | 0.97   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 92713146A39B | 355   | 0     | 0.97   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 1195277A74AB | 355   | 0     | 0.97   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | D55829A57889 | 355   | 0     | 0.97   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 6B716A05761F | 355   | 0     | 0.97   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 89D773C11E41 | 355   | 0     | 0.97   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | CABD8C4635BF | 355   | 0     | 0.97   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | BF68C80D9A79 | 354   | 0     | 0.97   |
| Samsung   | VO001920KWVMT 1... | 1.9 TB | 5BF04FB8FF35 | 354   | 0     | 0.97   |
| Samsung   | VO001920KWVMT 1... | 1.9 TB | 5B2B5B2C3170 | 354   | 0     | 0.97   |
| Samsung   | VO001920KWVMT 1... | 1.9 TB | 6AEB84F7756F | 354   | 0     | 0.97   |
| Micron    | 2200_MTFDHBA512TCK | 512 GB | 38E62F61143A | 353   | 0     | 0.97   |
| Intel     | SSDPEKKA512G8      | 512 GB | 840DC4ACC09F | 353   | 0     | 0.97   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 0AE4D4733DA8 | 353   | 0     | 0.97   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 24B156874470 | 353   | 0     | 0.97   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 5EE91A58DD7D | 353   | 0     | 0.97   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 80B1310F939D | 353   | 0     | 0.97   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 8DAF26117CFA | 353   | 0     | 0.97   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | B34686B79E9E | 353   | 0     | 0.97   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | B82E87ACBDC7 | 353   | 0     | 0.97   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | BD3B1B467B90 | 353   | 0     | 0.97   |
| Intel     | SSDPE2MD016T4      | 1.6 TB | D79C4F71EB13 | 353   | 0     | 0.97   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 0EDB55E17455 | 353   | 0     | 0.97   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 5F1F105E7979 | 353   | 0     | 0.97   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 1E329730A21F | 352   | 0     | 0.97   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | A95AD53E41A8 | 352   | 0     | 0.97   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | B96B85100F52 | 352   | 0     | 0.97   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | F1DE45A1A14E | 352   | 0     | 0.97   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 619B62413D17 | 352   | 0     | 0.97   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | F19816341149 | 352   | 0     | 0.97   |
| Intel     | SSDPE2KE032T8      | 3.2 TB | 3C1B4294E18F | 352   | 0     | 0.96   |
| Intel     | SSDPE2ME400G4      | 400 GB | 430D636DC8AB | 351   | 0     | 0.96   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | CAFD09DC8B03 | 351   | 0     | 0.96   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 2D794AC12C1E | 351   | 0     | 0.96   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | A35D3DEA5355 | 351   | 0     | 0.96   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | AACCB8DF0C23 | 351   | 0     | 0.96   |
| WDC       | CL SN720 SDAQNT... | 512 GB | 8F70FA13F7F5 | 350   | 0     | 0.96   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 889E1CFFA3EB | 350   | 0     | 0.96   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 0F5AD9D8DB6F | 350   | 0     | 0.96   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 3D8EAC436351 | 350   | 0     | 0.96   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | D3BD96F8660C | 350   | 0     | 0.96   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | D605D0490F1D | 350   | 0     | 0.96   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 4F584F434370 | 350   | 0     | 0.96   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 628DB5A337C9 | 350   | 0     | 0.96   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 6ED199426335 | 350   | 0     | 0.96   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 751E3A96E150 | 350   | 0     | 0.96   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 7AB41E5DA75B | 350   | 0     | 0.96   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 87E28A83B4BF | 350   | 0     | 0.96   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | A20834E0D705 | 350   | 0     | 0.96   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | CB3D4ADDC6AE | 350   | 0     | 0.96   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 88833CB34D14 | 349   | 0     | 0.96   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 96DC1E8A59E7 | 349   | 0     | 0.96   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | CC4B10BD8CF1 | 349   | 0     | 0.96   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | F1FBA528D8B4 | 349   | 0     | 0.96   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 1854CB75A64F | 349   | 0     | 0.96   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 1874B33728D6 | 349   | 0     | 0.96   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 210288C4106C | 349   | 0     | 0.96   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 21E0B6CBB1B4 | 349   | 0     | 0.96   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 547CBB1EE33A | 349   | 0     | 0.96   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 7A6AB274B4FB | 349   | 0     | 0.96   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 7CA36C092BC6 | 349   | 0     | 0.96   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 993852742777 | 349   | 0     | 0.96   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 9B5303E45EDB | 349   | 0     | 0.96   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | B2503C760509 | 349   | 0     | 0.96   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | CB45F8ECF27A | 349   | 0     | 0.96   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | E81272C5253F | 349   | 0     | 0.96   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | F6FCDCB935D6 | 349   | 0     | 0.96   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 8593A629DCAA | 349   | 0     | 0.96   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 9139E0470B02 | 349   | 0     | 0.96   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 01719C788FB4 | 349   | 0     | 0.96   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 462324121322 | 349   | 0     | 0.96   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | CB97E97E025F | 349   | 0     | 0.96   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | A664708ACEAF | 348   | 0     | 0.96   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | B9A1C5B9C4F7 | 348   | 0     | 0.96   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | F45B9691A8D6 | 348   | 0     | 0.96   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 19138FFEF40E | 348   | 0     | 0.96   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 5B718689BEBD | 348   | 0     | 0.96   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 7C4E46CF092C | 348   | 0     | 0.96   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 93616BA39091 | 348   | 0     | 0.96   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 9D2EB8966459 | 348   | 0     | 0.96   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | A9B60E8029AD | 348   | 0     | 0.96   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | CA69F5D31F17 | 348   | 0     | 0.96   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | CAB0CD668989 | 348   | 0     | 0.96   |
| Intel     | SSDPE2MD016T4      | 1.6 TB | 51FC48A2333E | 348   | 0     | 0.96   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 23C036C90334 | 348   | 0     | 0.95   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 55FAB81B5D6A | 348   | 0     | 0.95   |
| Toshiba   | KXD51RUE1T92       | 1.9 TB | E288516A9846 | 348   | 0     | 0.95   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 53F07AAAF9CC | 348   | 0     | 0.95   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | AA794F51AAC5 | 348   | 0     | 0.95   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | DC35136D1DC7 | 348   | 0     | 0.95   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 16493ACBC3A2 | 348   | 0     | 0.95   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | A375898F2144 | 348   | 0     | 0.95   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | B313155D40D4 | 348   | 0     | 0.95   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | EA280F1BE102 | 348   | 0     | 0.95   |
| Intel     | SSDPE2MD016T4      | 1.6 TB | E36CA3E43D41 | 348   | 0     | 0.95   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 01F7146DBB0D | 348   | 0     | 0.95   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 348BB7725191 | 348   | 0     | 0.95   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | DA9B85D8A29D | 348   | 0     | 0.95   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | F87E90F02CAF | 348   | 0     | 0.95   |
| Intel     | SSDPED1K750GA      | 752 GB | 388B58124E49 | 348   | 0     | 0.95   |
| Intel     | SSDPED1K750GA      | 752 GB | 412CAF701BCA | 348   | 0     | 0.95   |
| Intel     | SSDPED1K750GA      | 752 GB | 7751CE609802 | 348   | 0     | 0.95   |
| Intel     | SSDPED1K750GA      | 752 GB | A350C2828CEC | 348   | 0     | 0.95   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 15002F258BC3 | 347   | 0     | 0.95   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 5C2733DC6EB2 | 347   | 0     | 0.95   |
| Intel     | SSDPE21K750GA      | 752 GB | 20A119EF933B | 347   | 0     | 0.95   |
| Intel     | SSDPE21K750GA      | 752 GB | 64EB83C9BB18 | 347   | 0     | 0.95   |
| Intel     | SSDPE21K750GA      | 752 GB | AD33AD7969CA | 347   | 0     | 0.95   |
| Intel     | SSDPE2MD016T4      | 1.6 TB | C2AB6E559D43 | 347   | 0     | 0.95   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 09BFB1C00DDF | 346   | 0     | 0.95   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 3FF1678849AF | 346   | 0     | 0.95   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | FF19299BD402 | 346   | 0     | 0.95   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | F57E80203D31 | 345   | 0     | 0.95   |
| Micron    | 9300_MTFDHAL7T6TDP | 7.6 TB | 083A106977F3 | 345   | 0     | 0.95   |
| Micron    | 9300_MTFDHAL7T6TDP | 7.6 TB | 90688B6ED8AB | 345   | 0     | 0.95   |
| Intel     | SSDPE2KX010T8      | 1 TB   | 28332A1722D9 | 345   | 0     | 0.95   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 96A42A88FC07 | 345   | 0     | 0.95   |
| Samsung   | MZPLL6T4HMLA-00005 | 6.4 TB | EC26D2CCEACE | 345   | 0     | 0.95   |
| Intel     | SSDPED1K750GA      | 752 GB | A0CD17527178 | 345   | 0     | 0.95   |
| Intel     | SSDPED1K750GA      | 752 GB | A5DA000898B1 | 345   | 0     | 0.95   |
| Intel     | SSDPED1K750GA      | 752 GB | C3E5C6A2F15A | 345   | 0     | 0.95   |
| Intel     | SSDPED1K750GA      | 752 GB | DB12C2EB1844 | 345   | 0     | 0.95   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | B86A650DAD20 | 345   | 0     | 0.95   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 11241F9F731B | 345   | 0     | 0.95   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 25CC1DDAD7A0 | 345   | 0     | 0.95   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 5C45D1F6F418 | 345   | 0     | 0.95   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 794C077E562F | 345   | 0     | 0.95   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 9336E1122BA8 | 345   | 0     | 0.95   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | DAA5DBB85AE5 | 345   | 0     | 0.95   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 659C0DD3C6CD | 345   | 0     | 0.95   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 5E847EF5996E | 344   | 0     | 0.94   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 0B163D6FEB13 | 344   | 0     | 0.94   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 96EE216B8066 | 344   | 0     | 0.94   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | A4032131D4BE | 344   | 0     | 0.94   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | DB4566F57E96 | 344   | 0     | 0.94   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | CDC4C6E02CC9 | 344   | 0     | 0.94   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 0C81C76A7B1A | 344   | 0     | 0.94   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | D85CD9881AAA | 344   | 0     | 0.94   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 0725E2F78CA8 | 343   | 0     | 0.94   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 26844A869D59 | 343   | 0     | 0.94   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 56F7E5A5A295 | 343   | 0     | 0.94   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | C21ACC39FB72 | 343   | 0     | 0.94   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | D56F8DF3A85C | 343   | 0     | 0.94   |
| Samsung   | VO001920KWVMT 1... | 1.9 TB | 6CB9BF1C2E5D | 343   | 0     | 0.94   |
| Samsung   | VO001920KWVMT 1... | 1.9 TB | 9FE03D9F3A09 | 343   | 0     | 0.94   |
| WDC       | WDS500G3X0C-00SJG0 | 500 GB | A013A96F38DC | 343   | 0     | 0.94   |
| Samsung   | MZPLL3T2HAJQ-00005 | 3.2 TB | E604233A079D | 343   | 0     | 0.94   |
| Samsung   | VO001920KWVMT 1... | 1.9 TB | 7BAB5B1757CF | 343   | 0     | 0.94   |
| Samsung   | VO001920KWVMT 1... | 1.9 TB | D5C144A446F4 | 343   | 0     | 0.94   |
| HP        | SSD EX950          | 2 TB   | 23D3B887A1E0 | 343   | 0     | 0.94   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 514218951F14 | 343   | 0     | 0.94   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 6FE516A47DB2 | 342   | 0     | 0.94   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 7DBF40DCC544 | 342   | 0     | 0.94   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 938DCCF9E0B2 | 342   | 0     | 0.94   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | A368953AADBC | 342   | 0     | 0.94   |
| Samsung   | VO001920KWVMT 1... | 1.9 TB | 5A9A057F790D | 342   | 0     | 0.94   |
| Samsung   | VO001920KWVMT 1... | 1.9 TB | D10C44942201 | 342   | 0     | 0.94   |
| Intel     | SSDPE21K750GA      | 752 GB | 6F1E4792049E | 342   | 0     | 0.94   |
| Intel     | SSDPE21K750GA      | 752 GB | 9B2616C439ED | 342   | 0     | 0.94   |
| Intel     | SSDPE21K750GA      | 752 GB | FE2BB18F3CA9 | 342   | 0     | 0.94   |
| Samsung   | VO001920KWVMT 1... | 1.9 TB | A98EF94EC187 | 342   | 0     | 0.94   |
| Samsung   | VO001920KWVMT 1... | 1.9 TB | F80286229C94 | 342   | 0     | 0.94   |
| Corsair   | Force MP510 1.9TB  | 1.9 TB | 62677571A75A | 342   | 0     | 0.94   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 2279CA476454 | 342   | 0     | 0.94   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 95ABDC5D0E4F | 342   | 0     | 0.94   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 9C3928A6A89D | 342   | 0     | 0.94   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | F3A9CED3E645 | 342   | 0     | 0.94   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | F26B9990BC15 | 342   | 0     | 0.94   |
| Dell      | Express Flash N... | 1.6 TB | 9411D98EAE62 | 342   | 0     | 0.94   |
| Micron    | 9300_MTFDHAL7T6TDP | 7.6 TB | EDD3CBDC9CDC | 342   | 0     | 0.94   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 615C56AD9540 | 341   | 0     | 0.94   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | E30567DE6A7F | 341   | 0     | 0.94   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | FFC2E0AECC55 | 341   | 0     | 0.94   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 60A74BF57F20 | 341   | 0     | 0.94   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | FA0E1B965BDB | 341   | 0     | 0.94   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 71E612F600E9 | 341   | 0     | 0.94   |
| ADATA     | SX8200PNP          | 1 TB   | E7EBCC3971E3 | 341   | 0     | 0.94   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 745A5E749C30 | 341   | 0     | 0.94   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 77AD152EF3FB | 341   | 0     | 0.94   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 007ACA613336 | 341   | 0     | 0.94   |
| Intel     | SSDPED1D480GA      | 480 GB | BA104BADAB6C | 341   | 0     | 0.93   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | B1D504C84F62 | 341   | 0     | 0.93   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 02535824B9BF | 340   | 0     | 0.93   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 5297BA7083AF | 340   | 0     | 0.93   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | B79C26FAF8C9 | 340   | 0     | 0.93   |
| Samsung   | VO001920KWVMT 1... | 1.9 TB | 537D1E6E6527 | 340   | 0     | 0.93   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 2313B208DBAF | 340   | 0     | 0.93   |
| Samsung   | SSD 970 PRO        | 1 TB   | E5E3A0999771 | 340   | 0     | 0.93   |
| Samsung   | VO001920KWVMT 1... | 1.9 TB | 2101301A873C | 340   | 0     | 0.93   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 1B33F83E9B4D | 340   | 0     | 0.93   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 036D818EB27A | 340   | 0     | 0.93   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 59DCD5A1B3C8 | 340   | 0     | 0.93   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | A606C0436897 | 340   | 0     | 0.93   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | B6B16726D626 | 340   | 0     | 0.93   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | E368265FDF30 | 340   | 0     | 0.93   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | EB6C7A04DD7F | 340   | 0     | 0.93   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | C23232BBF45B | 339   | 0     | 0.93   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | F881463C0B19 | 339   | 0     | 0.93   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 1933B5CF5D7C | 339   | 0     | 0.93   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 359F35111DB2 | 339   | 0     | 0.93   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 52199F05F4ED | 339   | 0     | 0.93   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 5FE02B31148E | 339   | 0     | 0.93   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 73F61372FB7D | 339   | 0     | 0.93   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 77E4E75AA416 | 339   | 0     | 0.93   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 0AE13B39B8CE | 339   | 0     | 0.93   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | E63535231A0E | 339   | 0     | 0.93   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | E1A60F035D70 | 339   | 0     | 0.93   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 77468BA81BCA | 339   | 0     | 0.93   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 84838FA94794 | 339   | 0     | 0.93   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 168848062747 | 338   | 0     | 0.93   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 856DE1FB4B26 | 338   | 0     | 0.93   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 8FFBADF23981 | 338   | 0     | 0.93   |
| Micron    | 9300_MTFDHAL3T2TDR | 3.2 TB | 4C0D35B1FFF7 | 338   | 0     | 0.93   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | FBCB6B495609 | 338   | 0     | 0.93   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 4095BEF4872D | 338   | 0     | 0.93   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 7706AF7A1F1A | 338   | 0     | 0.93   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | C7626E6DAAE8 | 338   | 0     | 0.93   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | BB77F1BD960A | 338   | 0     | 0.93   |
| WDC       | PC SN720 SDAPNT... | 256 GB | B53871DB24CB | 337   | 0     | 0.93   |
| Intel     | SSDPEKKA512G8      | 512 GB | 9E5348CD41F7 | 337   | 0     | 0.93   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 101FA47E19D2 | 337   | 0     | 0.93   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 3A11CB9719DF | 337   | 0     | 0.93   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | EBEDA1CB99ED | 337   | 0     | 0.93   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | A66D759B7FA4 | 337   | 0     | 0.93   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 3DB041A02F8B | 337   | 0     | 0.92   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | EF6ECA36C098 | 337   | 0     | 0.92   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 13CDE2D1815A | 337   | 0     | 0.92   |
| HP        | SSD EX950          | 2 TB   | 15814B5CE9A0 | 337   | 0     | 0.92   |
| Intel     | SSDPE2KX010T8      | 1 TB   | AE2E400E8148 | 337   | 0     | 0.92   |
| Intel     | SSDPE2KX010T8      | 1 TB   | BC9F7808B81F | 337   | 0     | 0.92   |
| Intel     | SSDPE2KX010T8      | 1 TB   | A7FCE2143310 | 337   | 0     | 0.92   |
| Intel     | SSDPE2KX010T8      | 1 TB   | D639FA31DFFB | 337   | 0     | 0.92   |
| Intel     | SSDPE2KX010T8      | 1 TB   | 4A73E8B56141 | 336   | 0     | 0.92   |
| Intel     | SSDPE2KX010T8      | 1 TB   | C75F524EE419 | 336   | 0     | 0.92   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 09D2C8E91EFE | 336   | 0     | 0.92   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 72A870877AD9 | 336   | 0     | 0.92   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | B724BE046D76 | 336   | 0     | 0.92   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | B201C5C0C7CE | 336   | 0     | 0.92   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | DE1766084B15 | 336   | 0     | 0.92   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 05060DED7C5D | 336   | 0     | 0.92   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 95636F15951F | 336   | 0     | 0.92   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | B5F03E34981F | 336   | 0     | 0.92   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | E8F929F3E26B | 336   | 0     | 0.92   |
| Intel     | SSDPE2MX450G7      | 450 GB | 136281B63BAB | 335   | 0     | 0.92   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | D08C164F9143 | 335   | 0     | 0.92   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | FA85A52F8906 | 335   | 0     | 0.92   |
| Seagate   | FireCuda 520 SS... | 500 GB | 4ED24ABB4318 | 335   | 0     | 0.92   |
| Seagate   | FireCuda 520 SS... | 500 GB | CE26BDCC35ED | 335   | 0     | 0.92   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | A8DD544F9B09 | 334   | 0     | 0.92   |
| Kingston  | SEDC1000M1920G     | 1.9 TB | 0B8ABB19AA57 | 334   | 0     | 0.92   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 0F9D8097E85A | 334   | 0     | 0.92   |
| Intel     | SSDPE2KX010T8      | 1 TB   | BD9FF4230637 | 334   | 0     | 0.92   |
| Samsung   | MZVLB1T0HBLR-00000 | 1 TB   | 675A56C23B22 | 334   | 0     | 0.92   |
| Samsung   | SSD 970 PRO        | 512 GB | 0662B7CAFB4A | 334   | 0     | 0.92   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 5B3D4446BBBD | 333   | 0     | 0.91   |
| Seagate   | FireCuda 520 SS... | 500 GB | 3FF7C7B45DCF | 333   | 0     | 0.91   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 6F8706E56FFA | 333   | 0     | 0.91   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 89FEE09F9233 | 333   | 0     | 0.91   |
| Samsung   | MZQLW960HMJP-00003 | 960 GB | 3475FD03037A | 333   | 0     | 0.91   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | FA124DAF7D29 | 333   | 0     | 0.91   |
| Intel     | SSDPED1K750GA      | 752 GB | B1C6E3E518C6 | 333   | 0     | 0.91   |
| Micron    | 2200S NVMe         | 256 GB | 17FB702995FE | 333   | 0     | 0.91   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 3B8A482C0107 | 332   | 0     | 0.91   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 95950873A46A | 332   | 0     | 0.91   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | CB302F6827A8 | 332   | 0     | 0.91   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 25853525BFED | 332   | 0     | 0.91   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 4C910CC17052 | 332   | 0     | 0.91   |
| Seagate   | FireCuda 520 SS... | 500 GB | 590E8ADDB4D5 | 332   | 0     | 0.91   |
| Intel     | SSDPE2ME400G4      | 400 GB | 6F483A555BE7 | 331   | 0     | 0.91   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 922D407168F1 | 331   | 0     | 0.91   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 437979363F67 | 331   | 0     | 0.91   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 64D671DD4625 | 331   | 0     | 0.91   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 72290BFA70C3 | 331   | 0     | 0.91   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 05D37C127466 | 331   | 0     | 0.91   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 1081AF79C2B3 | 331   | 0     | 0.91   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 2C25BB56D0C8 | 331   | 0     | 0.91   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 3AF549A3A9E3 | 331   | 0     | 0.91   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 4B9B93BABE47 | 331   | 0     | 0.91   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 20D925FB5502 | 331   | 0     | 0.91   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 7798FEC4EE4A | 331   | 0     | 0.91   |
| Intel     | SSDPEL1K100GA      | 100 GB | 5E03AD83E1E8 | 331   | 0     | 0.91   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | AD4C8C6D62D5 | 331   | 0     | 0.91   |
| Intel     | SSDPE21K750GA      | 752 GB | 985901F64546 | 330   | 0     | 0.91   |
| Intel     | SSDPE21K750GA      | 752 GB | BEFDE7FE4D0E | 330   | 0     | 0.91   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 1EEEB9909B85 | 330   | 0     | 0.91   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | AC6C4B2B0B6D | 330   | 0     | 0.91   |
| Intel     | SSDPE2ME400G4      | 400 GB | 4783E541C30F | 330   | 0     | 0.91   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 2CB34E6FC255 | 330   | 0     | 0.91   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 340C574A4AEB | 330   | 0     | 0.91   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | E7CF90A55AC8 | 330   | 0     | 0.91   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | B46AC096920E | 330   | 0     | 0.91   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 3B7FB319C276 | 330   | 0     | 0.91   |
| Samsung   | SSD 970 PRO        | 512 GB | 06D09D80F9FA | 330   | 0     | 0.91   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 9CAD9A1027C6 | 330   | 0     | 0.90   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | C53AD171615E | 330   | 0     | 0.90   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | E8C4C88648FB | 330   | 0     | 0.90   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 65485B9C9AAA | 330   | 0     | 0.90   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 96AD5879436C | 330   | 0     | 0.90   |
| Samsung   | VO001920KWVMT 1... | 1.9 TB | 1F5302CE71E9 | 329   | 0     | 0.90   |
| Samsung   | VO001920KWVMT 1... | 1.9 TB | D3F4D6788E4E | 329   | 0     | 0.90   |
| Samsung   | VO001920KWVMT 1... | 1.9 TB | A5D1F4CEB6F8 | 329   | 0     | 0.90   |
| Samsung   | VO001920KWVMT 1... | 1.9 TB | FCB4FB683EA6 | 329   | 0     | 0.90   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 0D8ED37027BC | 329   | 0     | 0.90   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 286CD831B882 | 329   | 0     | 0.90   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 2D2305E28CD3 | 329   | 0     | 0.90   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 13308F3B96B7 | 329   | 0     | 0.90   |
| Samsung   | MZVLB1T0HALR-00000 | 1 TB   | 9646796483D8 | 329   | 0     | 0.90   |
| Intel     | SSDPEKKW256G7      | 256 GB | 95B03B22FEEF | 657   | 1     | 0.90   |
| Intel     | SSDPEL1K100GA      | 100 GB | DDDD4440AF67 | 328   | 0     | 0.90   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | D27BE45C322B | 328   | 0     | 0.90   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 2D262A89E756 | 328   | 0     | 0.90   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 6E3C17566A53 | 328   | 0     | 0.90   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 1AE15BFE19F6 | 328   | 0     | 0.90   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 0FED56943720 | 328   | 0     | 0.90   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | BF4E0C832B6D | 328   | 0     | 0.90   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | DDCBC3152A47 | 328   | 0     | 0.90   |
| Samsung   | SSD 970 EVO Plus   | 1 TB   | EDD9655447AB | 328   | 0     | 0.90   |
| Samsung   | SSD 970 EVO Plus   | 1 TB   | 3AC93D8D3D01 | 327   | 0     | 0.90   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 7B548139ABEF | 327   | 0     | 0.90   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | C9F72B51B939 | 327   | 0     | 0.90   |
| Samsung   | MZWLL1T6HEHP-00003 | 1.6 TB | 31CBF97EA98C | 327   | 0     | 0.90   |
| Samsung   | MZWLL1T6HEHP-00003 | 1.6 TB | 60602868322F | 327   | 0     | 0.90   |
| Samsung   | MZWLL1T6HEHP-00003 | 1.6 TB | 6550B5A19E18 | 327   | 0     | 0.90   |
| Samsung   | MZWLL1T6HEHP-00003 | 1.6 TB | A5A170CF81BC | 327   | 0     | 0.90   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 43CEFD452B18 | 327   | 0     | 0.90   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | A6B9220DE934 | 327   | 0     | 0.90   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 91D4075F2F1E | 326   | 0     | 0.90   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 350B9FCF285E | 326   | 0     | 0.90   |
| Crucial   | CT250P2SSD8        | 250 GB | B1AFB978E10F | 326   | 0     | 0.89   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 1B68E57B80B4 | 326   | 0     | 0.89   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 5F6F930712F7 | 326   | 0     | 0.89   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 3D4EDFE22F72 | 326   | 0     | 0.89   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | B2461F9A6291 | 326   | 0     | 0.89   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 0BE7BBC4FF2A | 326   | 0     | 0.89   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 297E9C35E30E | 326   | 0     | 0.89   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 6910A32B80B5 | 326   | 0     | 0.89   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | DCC3F123DEB8 | 326   | 0     | 0.89   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 858C086EEABA | 326   | 0     | 0.89   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 6A9519508DD0 | 325   | 0     | 0.89   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 70CE6831B662 | 325   | 0     | 0.89   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 5F5D090B8542 | 651   | 1     | 0.89   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 0A6F7B8E9F79 | 325   | 0     | 0.89   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 6F1EFC9BC4D6 | 325   | 0     | 0.89   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 7BE62BC8C507 | 325   | 0     | 0.89   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 984CDF288CB0 | 325   | 0     | 0.89   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 9EBBA68694B8 | 325   | 0     | 0.89   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | CE3BF7B1BC03 | 325   | 0     | 0.89   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | C35988D42A4E | 325   | 0     | 0.89   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 044A9F6AA891 | 324   | 0     | 0.89   |
| Phison    | Viper M.2 VPN100   | 2 TB   | 7D1D19E525D3 | 324   | 0     | 0.89   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 1FC2B40FE31A | 324   | 0     | 0.89   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 5529F50419FC | 324   | 0     | 0.89   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 805CBEDCA1BB | 324   | 0     | 0.89   |
| Samsung   | SSD 970 PRO        | 1 TB   | AC9D99315F83 | 324   | 0     | 0.89   |
| WDC       | PC SN720 SDAPNT... | 256 GB | E6037603BF3F | 324   | 0     | 0.89   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 1A6B8FCE57C2 | 323   | 0     | 0.89   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 56BF46060789 | 323   | 0     | 0.89   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 16D399C5B92D | 323   | 0     | 0.89   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 28864024A9B0 | 323   | 0     | 0.89   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 334A481BE6D6 | 323   | 0     | 0.89   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 8245278067C7 | 323   | 0     | 0.89   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | A414269227E9 | 323   | 0     | 0.89   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | F22747ED4DE5 | 323   | 0     | 0.89   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 1EEBC327A00C | 323   | 0     | 0.89   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | FD89758BD4CC | 323   | 0     | 0.89   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | BA527628DFFA | 323   | 0     | 0.89   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | BBCB8DA416F1 | 323   | 0     | 0.89   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 32B0A829DDFF | 323   | 0     | 0.89   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 82A2904BCE17 | 323   | 0     | 0.89   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 8F42F7806AE9 | 323   | 0     | 0.89   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | B23411E03008 | 323   | 0     | 0.89   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 5B129EA07E40 | 323   | 0     | 0.89   |
| Intel     | SSDPE2MX450G7      | 450 GB | 19D7FEB0BA46 | 322   | 0     | 0.88   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | A1B764986DF9 | 322   | 0     | 0.88   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | A12E51974D5F | 322   | 0     | 0.88   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 76AA4BCC36D7 | 322   | 0     | 0.88   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 1D182FEBAF6B | 322   | 0     | 0.88   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | FF00ABE0F776 | 322   | 0     | 0.88   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | B630A10AAD8B | 321   | 0     | 0.88   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 6722492AD906 | 321   | 0     | 0.88   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 2375FE630995 | 321   | 0     | 0.88   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 44FC8F40ADBB | 321   | 0     | 0.88   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 8321A6C9C5AE | 321   | 0     | 0.88   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | C2188FA362F1 | 320   | 0     | 0.88   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 02EA4E795375 | 320   | 0     | 0.88   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | DAB0B5A297B7 | 320   | 0     | 0.88   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | 8A52C4D65961 | 320   | 0     | 0.88   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | E5D4E7914B38 | 320   | 0     | 0.88   |
| Samsung   | VO001920KWVMT 1... | 1.9 TB | 9A1CC01C64D2 | 320   | 0     | 0.88   |
| Samsung   | VO001920KWVMT 1... | 1.9 TB | F500EE52199E | 320   | 0     | 0.88   |
| Samsung   | VO001920KWVMT 1... | 1.9 TB | 987D85D56ADB | 320   | 0     | 0.88   |
| Samsung   | VO001920KWVMT 1... | 1.9 TB | D29E913F383B | 320   | 0     | 0.88   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 6A1D1B66A329 | 320   | 0     | 0.88   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 83B370624F85 | 320   | 0     | 0.88   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | BF2011752BC5 | 320   | 0     | 0.88   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | C0B6177DD801 | 320   | 0     | 0.88   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | C14B52CDDDF7 | 320   | 0     | 0.88   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | DF9FBD6378B0 | 320   | 0     | 0.88   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 55172D9ADB42 | 320   | 0     | 0.88   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 61B92499A6E1 | 319   | 0     | 0.88   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 822DEAE50894 | 319   | 0     | 0.88   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 74DFEED97B29 | 319   | 0     | 0.88   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | E5ACC8C0EE26 | 319   | 0     | 0.88   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | EC7A0CB10E67 | 319   | 0     | 0.88   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 3A5484CFCE1C | 319   | 0     | 0.88   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 5CC131769F08 | 319   | 0     | 0.88   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 6A2608BFCF2E | 319   | 0     | 0.87   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | BE8182EA1287 | 319   | 0     | 0.87   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 3792FBA0B4E7 | 319   | 0     | 0.87   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 7C8CCFF584DE | 319   | 0     | 0.87   |
| Samsung   | SSD 970 PRO        | 512 GB | CAD5616E84A1 | 319   | 0     | 0.87   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 43C9AF9B6CD1 | 318   | 0     | 0.87   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 45D77A9C9B11 | 318   | 0     | 0.87   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 6F86FA523E54 | 318   | 0     | 0.87   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 897DD3D66245 | 318   | 0     | 0.87   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 96AEFAA9AF7A | 318   | 0     | 0.87   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | B8D0CA3324BD | 318   | 0     | 0.87   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | FF4F1E152B13 | 318   | 0     | 0.87   |
| Intel     | SSDPE2ME012T4      | 1.2 TB | 1F51E1D62E35 | 318   | 0     | 0.87   |
| Samsung   | VO001920KWVMT 1... | 1.9 TB | A7183E1E31D7 | 318   | 0     | 0.87   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 8D8E5DC22B48 | 318   | 0     | 0.87   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | C99DAF085E8D | 318   | 0     | 0.87   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 20D713B027A2 | 318   | 0     | 0.87   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 3EFD6C0A7A34 | 318   | 0     | 0.87   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 48CC800105B4 | 318   | 0     | 0.87   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | C3B6835DC846 | 318   | 0     | 0.87   |
| HP        | SSD EX950          | 2 TB   | A8C2551F927D | 318   | 0     | 0.87   |
| Samsung   | MZ1LB3T8HMLA-00007 | 3.8 TB | F7A4A50966E1 | 317   | 0     | 0.87   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 3345E07E271D | 317   | 0     | 0.87   |
| HGST      | HUSMR7638BDP3Y1    | 3.8 TB | 3E96661E228E | 317   | 0     | 0.87   |
| HGST      | HUSMR7638BDP3Y1    | 3.8 TB | 58A57498762D | 317   | 0     | 0.87   |
| HGST      | HUSMR7638BDP3Y1    | 3.8 TB | 6D9E093AD011 | 317   | 0     | 0.87   |
| HGST      | HUSMR7638BDP3Y1    | 3.8 TB | B4E8B21D61B6 | 317   | 0     | 0.87   |
| HGST      | HUSMR7638BDP3Y1    | 3.8 TB | B530217A7F0C | 317   | 0     | 0.87   |
| HGST      | HUSMR7638BDP3Y1    | 3.8 TB | DE5DD3FEC91A | 317   | 0     | 0.87   |
| Intel     | SSDPED1K375GA      | 375 GB | 873DD553BE91 | 317   | 0     | 0.87   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 2B5FBF09C940 | 317   | 0     | 0.87   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 4EC244322D39 | 317   | 0     | 0.87   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 8195363EFDA8 | 317   | 0     | 0.87   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | D0D7EC33EE3C | 317   | 0     | 0.87   |
| HGST      | HUSMR7638BDP3Y1    | 3.8 TB | 66A61D7DF830 | 317   | 0     | 0.87   |
| HGST      | HUSMR7638BDP3Y1    | 3.8 TB | 6A52E500F456 | 317   | 0     | 0.87   |
| HGST      | HUSMR7638BDP3Y1    | 3.8 TB | A4A1059D2687 | 317   | 0     | 0.87   |
| HGST      | HUSMR7638BDP3Y1    | 3.8 TB | 68360CE8CFE2 | 317   | 0     | 0.87   |
| HGST      | HUSMR7638BDP3Y1    | 3.8 TB | A0BAA4D0FCCF | 317   | 0     | 0.87   |
| HGST      | HUSMR7638BDP3Y1    | 3.8 TB | F91D7124CF8A | 317   | 0     | 0.87   |
| HP        | SSD EX950          | 2 TB   | FF724EB960E9 | 317   | 0     | 0.87   |
| Micron    | 9300_MTFDHAL3T2TDR | 3.2 TB | EEB66A9E3A36 | 316   | 0     | 0.87   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 7C1FE8FFACD4 | 316   | 0     | 0.87   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 253912976FFD | 316   | 0     | 0.87   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 380526E487DD | 316   | 0     | 0.87   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 41CE42317BC2 | 316   | 0     | 0.87   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 6D2D0441F156 | 316   | 0     | 0.87   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 959A72490011 | 316   | 0     | 0.87   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | B42EF80ADC34 | 316   | 0     | 0.87   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | BF171897D803 | 316   | 0     | 0.87   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | EFF535E288AF | 316   | 0     | 0.87   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | F185D7D29C1B | 316   | 0     | 0.87   |
| Intel     | SSDPE2KX010T8      | 1 TB   | FD06DDE8A841 | 316   | 0     | 0.87   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 80CDE7256C92 | 315   | 0     | 0.87   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 899A0CC92ADB | 315   | 0     | 0.87   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | F9D4EF23CB9A | 315   | 0     | 0.87   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | A5863B4FA030 | 315   | 0     | 0.86   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | E5938F39D0B3 | 314   | 0     | 0.86   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | BE2F08E611E0 | 314   | 0     | 0.86   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 096F9ABBB3AB | 314   | 0     | 0.86   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 2EC54A0CA123 | 314   | 0     | 0.86   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 027DBD958AEA | 314   | 0     | 0.86   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 0B5C169B30B1 | 314   | 0     | 0.86   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 89AD76504AB3 | 314   | 0     | 0.86   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | C35C95028507 | 314   | 0     | 0.86   |
| Samsung   | MZWLL1T6HEHP-00003 | 1.6 TB | 98A6A4B2BB44 | 314   | 0     | 0.86   |
| Samsung   | VO001920KWVMT 1... | 1.9 TB | 2639591A4DA9 | 313   | 0     | 0.86   |
| Samsung   | VO001920KWVMT 1... | 1.9 TB | BAA9CDFD35EB | 313   | 0     | 0.86   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 22342AB3545A | 313   | 0     | 0.86   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | D33A2757C387 | 313   | 0     | 0.86   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | D7BDD6580DBA | 313   | 0     | 0.86   |
| Samsung   | SSD 970 PRO        | 512 GB | CF222FF393C5 | 313   | 0     | 0.86   |
| Dell      | Express Flash N... | 1.6 TB | CC35C61616F9 | 313   | 0     | 0.86   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 1972C47BF9E6 | 313   | 0     | 0.86   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | C8D2CF5C4E23 | 313   | 0     | 0.86   |
| Intel     | SSDPE2KX010T8      | 1 TB   | AC0A7D766277 | 313   | 0     | 0.86   |
| WDC       | WDS100T3X0C-00SJG0 | 1 TB   | B02C9F97129F | 312   | 0     | 0.86   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 9F698E554C80 | 312   | 0     | 0.86   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 665EFEA9FAC5 | 312   | 0     | 0.86   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | D0F0E409CAAC | 312   | 0     | 0.86   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | D2F43949BAFC | 312   | 0     | 0.86   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | A253F528FC3D | 312   | 0     | 0.86   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | B461E9011564 | 312   | 0     | 0.86   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 4D25F8AF6D57 | 311   | 0     | 0.85   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 507669984B4F | 311   | 0     | 0.85   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | C36B78CC69B3 | 311   | 0     | 0.85   |
| Micron    | 9300_MTFDHAL3T2TDR | 3.2 TB | 8AA4C71BD207 | 311   | 0     | 0.85   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 6AB05CBA13FA | 311   | 0     | 0.85   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | C30B7FA6AA37 | 311   | 0     | 0.85   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 9526B8AA6F5C | 311   | 0     | 0.85   |
| WDC       | WDS100T3X0C-00SJG0 | 1 TB   | 3C68E2614968 | 310   | 0     | 0.85   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 2C7406C742E3 | 309   | 0     | 0.85   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 8687909E94A4 | 309   | 0     | 0.85   |
| Samsung   | MZVLB512HAJQ-00000 | 512 GB | 6A3CEF10E489 | 309   | 0     | 0.85   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | CF1F53488AF0 | 309   | 0     | 0.85   |
| WDC       | WDS500G3X0C-00SJG0 | 500 GB | 1DBFB1CCE981 | 308   | 0     | 0.85   |
| Intel     | SSDPED1K375GA      | 375 GB | 22094F59D5D0 | 308   | 0     | 0.85   |
| Micron    | 2200_MTFDHBA512TCK | 512 GB | E10F7E9AF2A2 | 308   | 0     | 0.84   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 3FE414C271DD | 308   | 0     | 0.84   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 80E4BA1A5A20 | 308   | 0     | 0.84   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 10A833454246 | 308   | 0     | 0.84   |
| Samsung   | MZVLB256HAHQ-00000 | 256 GB | 689D87BEB3A6 | 308   | 0     | 0.84   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 0E711749B2BD | 307   | 0     | 0.84   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 51FD14060B91 | 307   | 0     | 0.84   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 5BE595FC21EC | 307   | 0     | 0.84   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 911BBDD70B4D | 307   | 0     | 0.84   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | AA47BB9B29E5 | 307   | 0     | 0.84   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | BAF7FBD5AE7B | 307   | 0     | 0.84   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | CD579E4C6622 | 307   | 0     | 0.84   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 0E2B2DAA456E | 307   | 0     | 0.84   |
| Samsung   | MZWLL1T6HEHP-00003 | 1.6 TB | A9AEADF63CAF | 307   | 0     | 0.84   |
| Samsung   | MZWLL1T6HEHP-00003 | 1.6 TB | 5A78451717A1 | 307   | 0     | 0.84   |
| Samsung   | MZWLL1T6HEHP-00003 | 1.6 TB | E0DC3BE28294 | 307   | 0     | 0.84   |
| Samsung   | VO001920KWVMT 1... | 1.9 TB | 0064BBF3FE6A | 307   | 0     | 0.84   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 54C651D9AAC8 | 307   | 0     | 0.84   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 4D56803959D9 | 306   | 0     | 0.84   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | F130D8410DF7 | 306   | 0     | 0.84   |
| Intel     | SSDPE2KX010T8      | 1 TB   | E0AAA9617573 | 306   | 0     | 0.84   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 206D35C62D1C | 306   | 0     | 0.84   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 2D8A1F49EC1C | 306   | 0     | 0.84   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 6FA03F65F67A | 306   | 0     | 0.84   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | E6B89D9B7C1F | 306   | 0     | 0.84   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 193EB6A27433 | 305   | 0     | 0.84   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 4EA50E07BC36 | 305   | 0     | 0.84   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 84CE73EC84FD | 305   | 0     | 0.84   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 09EC9688AF48 | 305   | 0     | 0.84   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 3DFB72F10878 | 305   | 0     | 0.84   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | B409E794CB19 | 305   | 0     | 0.84   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | D5D6466582DD | 305   | 0     | 0.84   |
| Seagate   | FireCuda 520 SS... | 500 GB | 6932888DFB63 | 305   | 0     | 0.84   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | B8B32BE6F9E8 | 305   | 0     | 0.84   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | F2B01B99CFDA | 305   | 0     | 0.84   |
| Samsung   | VO001920KWVMT 1... | 1.9 TB | 114F3D4C925A | 304   | 0     | 0.84   |
| Samsung   | VO001920KWVMT 1... | 1.9 TB | 47856077D39B | 304   | 0     | 0.84   |
| Samsung   | VO001920KWVMT 1... | 1.9 TB | B26FFC5C9D93 | 304   | 0     | 0.84   |
| Intel     | SSDPE2KX010T8      | 1 TB   | 49717201BB02 | 304   | 0     | 0.83   |
| Phison    | Viper M.2 VPN100   | 256 GB | 99FA25FB8A66 | 304   | 0     | 0.83   |
| Samsung   | MZQLW960HMJP-00003 | 960 GB | 80DB92DE63B2 | 303   | 0     | 0.83   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 79673BD14011 | 303   | 0     | 0.83   |
| Intel     | SSDPE2KX010T8      | 1 TB   | 8547EA851FE6 | 303   | 0     | 0.83   |
| Samsung   | SSD 970 EVO Plus   | 1 TB   | D246314E8B39 | 303   | 0     | 0.83   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 8D69FEC52889 | 303   | 0     | 0.83   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | B951D6A3CD46 | 303   | 0     | 0.83   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | CB3AF644ACF9 | 303   | 0     | 0.83   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 7EC0371E56FE | 303   | 0     | 0.83   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 048A1730C173 | 303   | 0     | 0.83   |
| Samsung   | MZPLL6T4HMLA-00005 | 6.4 TB | CEBE1DA2C4E6 | 302   | 0     | 0.83   |
| Micron    | 2200_MTFDHBA512TCK | 512 GB | 6FC81D6C9A63 | 302   | 0     | 0.83   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 0080A046C0FB | 301   | 0     | 0.83   |
| Intel     | SSDPE2KX010T8      | 1 TB   | 11F55CFE9E20 | 301   | 0     | 0.82   |
| Samsung   | VO001920KWVMT 1... | 1.9 TB | 88CFA8B1528E | 300   | 0     | 0.82   |
| Samsung   | VO001920KWVMT 1... | 1.9 TB | 3D3177A5083F | 300   | 0     | 0.82   |
| Samsung   | MZ1LB3T8HMLA-00007 | 3.8 TB | BE0A9540E4D1 | 300   | 0     | 0.82   |
| WDC       | WUS4BB096D7P3E1    | 720 GB | C426126608AD | 300   | 0     | 0.82   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 145844129538 | 300   | 0     | 0.82   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | BEF882CE5632 | 300   | 0     | 0.82   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | FEED9289D3DC | 300   | 0     | 0.82   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | DD49E16628BB | 299   | 0     | 0.82   |
| Samsung   | SSD 970 PRO        | 512 GB | EF8BED995020 | 299   | 0     | 0.82   |
| WDC       | CL SN720 SDAQNT... | 512 GB | 72FCD2649EC6 | 299   | 0     | 0.82   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 5FD77AD54CF8 | 299   | 0     | 0.82   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 6320A4C90486 | 299   | 0     | 0.82   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | BB38134AA7E3 | 299   | 0     | 0.82   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | BB38134AA7E3 | 299   | 0     | 0.82   |
| Samsung   | MZPJB480HMGC-0BW07 | 480 GB | 4135DA10EF35 | 298   | 0     | 0.82   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 34D8248DBD5B | 298   | 0     | 0.82   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 3BA37672ED33 | 298   | 0     | 0.82   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 47A2B61EF776 | 298   | 0     | 0.82   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 5D0E27280B4C | 298   | 0     | 0.82   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | BF9642EAA131 | 298   | 0     | 0.82   |
| WDC       | WUS4BB038D7P3E1    | 3.8 TB | 3DBDE9A9EE86 | 298   | 0     | 0.82   |
| WDC       | WUS4BB038D7P3E1    | 3.8 TB | 9A053E19F66E | 298   | 0     | 0.82   |
| WDC       | WUS4BB038D7P3E1    | 3.8 TB | F5E29E0EAD61 | 298   | 0     | 0.82   |
| Intel     | SSDPECKE064T7ES    | 3.2 TB | 351554FD9390 | 297   | 0     | 0.82   |
| WDC       | CL SN720 SDAQNT... | 512 GB | 04552F4DD8E2 | 297   | 0     | 0.82   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 7818EFF980B1 | 297   | 0     | 0.82   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | E76DEC01490B | 297   | 0     | 0.81   |
| Samsung   | MZ1LB3T8HMLA-00007 | 3.8 TB | 4949D5D8440E | 297   | 0     | 0.81   |
| Samsung   | MZQLW960HMJP-00003 | 960 GB | D45E9F698011 | 296   | 0     | 0.81   |
| Gigabyte  | GP-GSM2NE3100TNTD  | 1 TB   | 1F1F0CEA01BD | 296   | 0     | 0.81   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 20BEDF8D3A76 | 296   | 0     | 0.81   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 7363FC5D8C62 | 296   | 0     | 0.81   |
| Micron    | 9300_MTFDHAL7T6TDP | 7.6 TB | 12BDAE23E400 | 296   | 0     | 0.81   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 13F7770DEC68 | 296   | 0     | 0.81   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 54342A54034D | 296   | 0     | 0.81   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 011F41CAE762 | 296   | 0     | 0.81   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 2D2585895556 | 296   | 0     | 0.81   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 46F8AE06F120 | 296   | 0     | 0.81   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 551E032D7922 | 296   | 0     | 0.81   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 5A0638527CC2 | 296   | 0     | 0.81   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 625EC9B3A9FC | 296   | 0     | 0.81   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | E782FA426B39 | 296   | 0     | 0.81   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | F9FCCA6D64B8 | 296   | 0     | 0.81   |
| Samsung   | MZQLW960HMJP-00003 | 960 GB | 74D7C084338B | 296   | 0     | 0.81   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 27C3F853813B | 296   | 0     | 0.81   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 197C39CB702B | 296   | 0     | 0.81   |
| Intel     | SSDPE2KX010T8      | 1 TB   | 335145FBD558 | 295   | 0     | 0.81   |
| Samsung   | MZPLJ6T4HALA-00007 | 6.4 TB | 47B838FCEFA9 | 295   | 0     | 0.81   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 11BB9A62CD40 | 295   | 0     | 0.81   |
| Samsung   | MZQLB3T8HALS-000AZ | 3.8 TB | 28D7031B7867 | 294   | 0     | 0.81   |
| Samsung   | MZQLB3T8HALS-000AZ | 3.8 TB | 9F400A2AED25 | 294   | 0     | 0.81   |
| Samsung   | MZQLB3T8HALS-000AZ | 3.8 TB | D99FBBCFEBB2 | 294   | 0     | 0.81   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 55B9C3B96373 | 294   | 0     | 0.81   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 7CDEC683A2A9 | 294   | 0     | 0.81   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 28793506B4B2 | 294   | 0     | 0.81   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | E0059E2247BF | 294   | 0     | 0.81   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | C81A5C2F3BB0 | 294   | 0     | 0.81   |
| Samsung   | MZPLJ6T4HALA-00007 | 6.4 TB | 807CF386CB84 | 293   | 0     | 0.81   |
| Samsung   | VO001920KWVMT 1... | 1.9 TB | 5B20C4C996CA | 293   | 0     | 0.81   |
| Samsung   | VO001920KWVMT 1... | 1.9 TB | C627E0231756 | 293   | 0     | 0.81   |
| Corsair   | Force MP510 1.9TB  | 1.9 TB | 745FE35ECE31 | 293   | 0     | 0.80   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 0030281DF78E | 293   | 0     | 0.80   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 6E4606563847 | 293   | 0     | 0.80   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 5D9BBFB59242 | 293   | 0     | 0.80   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | F93D3F6F71D9 | 293   | 0     | 0.80   |
| Samsung   | MZVLB512HAJQ-00000 | 512 GB | 426381B129A6 | 293   | 0     | 0.80   |
| Samsung   | MZVLB512HAJQ-00000 | 512 GB | 57BCDAAAE181 | 293   | 0     | 0.80   |
| Kingston  | SKC2500M81000G     | 1 TB   | 49E5C84B449F | 292   | 0     | 0.80   |
| Samsung   | MZVLB512HAJQ-00000 | 512 GB | 2FF631535C0A | 292   | 0     | 0.80   |
| Samsung   | SSD 960 EVO        | 250 GB | 7AD93D79CDEF | 292   | 0     | 0.80   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | D9A0517CEE14 | 292   | 0     | 0.80   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 115405C6DB2B | 292   | 0     | 0.80   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 4BB14A472C09 | 292   | 0     | 0.80   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 9553708C8E6E | 292   | 0     | 0.80   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | C40C0199293D | 292   | 0     | 0.80   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | C78345156938 | 292   | 0     | 0.80   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | EE7A5F3ABB30 | 292   | 0     | 0.80   |
| Seagate   | FireCuda 520 SS... | 500 GB | BB6923ECD6B5 | 292   | 0     | 0.80   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | B4C7062E25AA | 291   | 0     | 0.80   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | D23906F95383 | 291   | 0     | 0.80   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | D278CE46F776 | 291   | 0     | 0.80   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | DEB95BACB10A | 291   | 0     | 0.80   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 222B89B7CFCF | 291   | 0     | 0.80   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 63355E05CBEB | 291   | 0     | 0.80   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | D88E7C67686E | 291   | 0     | 0.80   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | E78082FC9A58 | 291   | 0     | 0.80   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | D91EBBE1F5E6 | 291   | 0     | 0.80   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | B23C3243D680 | 291   | 0     | 0.80   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | C9E99EBBDAC7 | 291   | 0     | 0.80   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 238DBFBFC38A | 291   | 0     | 0.80   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | BC82F956DE74 | 291   | 0     | 0.80   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 29DDAA46AF14 | 290   | 0     | 0.80   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 71D25D963843 | 290   | 0     | 0.80   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 9EBBCA82B115 | 290   | 0     | 0.80   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 841ADB8CBA16 | 290   | 0     | 0.80   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 946831120F05 | 290   | 0     | 0.80   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 99ACFEBAC10B | 290   | 0     | 0.80   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | B32552C424F5 | 290   | 0     | 0.80   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 41FB30C7CD87 | 290   | 0     | 0.79   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 12F1EE9CCD76 | 289   | 0     | 0.79   |
| Phison    | Sabrent Rocket Q   | 1 TB   | A7CB223C1006 | 289   | 0     | 0.79   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | A7CDBB5ADBA2 | 289   | 0     | 0.79   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | CB5E1D1E9537 | 289   | 0     | 0.79   |
| Phison    | Sabrent Rocket Q   | 1 TB   | 891B33562C11 | 289   | 0     | 0.79   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | C60784DE9BE5 | 289   | 0     | 0.79   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | F385FA59C0F0 | 289   | 0     | 0.79   |
| Intel     | SSDPE2MX450G7      | 450 GB | F6415DFDDAB1 | 288   | 0     | 0.79   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | C4E249D3225F | 288   | 0     | 0.79   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | DAA593F5254D | 288   | 0     | 0.79   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | B74C4921B659 | 288   | 0     | 0.79   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 0F6CA4361354 | 288   | 0     | 0.79   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | E96EAD27B183 | 288   | 0     | 0.79   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 871B5AEEF9DF | 288   | 0     | 0.79   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 8B9A16A97D01 | 288   | 0     | 0.79   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 183F233FC9E3 | 287   | 0     | 0.79   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 49FF100AD5DF | 287   | 0     | 0.79   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 6B2E6E06EA38 | 287   | 0     | 0.79   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 85178ED0457D | 287   | 0     | 0.79   |
| Micron    | 9300_MTFDHAL3T2TDR | 3.2 TB | 44F57CAF0D48 | 287   | 0     | 0.79   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | E989E3B67A73 | 287   | 0     | 0.79   |
| Samsung   | MZQLB3T8HALS-000AZ | 3.8 TB | BF55F241A78F | 287   | 0     | 0.79   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | FC82E270BED3 | 287   | 0     | 0.79   |
| WDC       | WUS4BB096D7P3E1    | 720 GB | 711A73C0E9AE | 287   | 0     | 0.79   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | B3CBCAB9A92A | 287   | 0     | 0.79   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | C23D2C6C8549 | 287   | 0     | 0.79   |
| Gigabyte  | GP-GSM2NE3100TNTD  | 1 TB   | DD8FD9BB01FF | 287   | 0     | 0.79   |
| Gigabyte  | GP-GSM2NE3100TNTD  | 1 TB   | 51F326344DAB | 287   | 0     | 0.79   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | 19D06E9FFFF0 | 287   | 0     | 0.79   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | EF593FFA327E | 287   | 0     | 0.79   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | F4B6E99A2EF6 | 287   | 0     | 0.79   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 3680979CCD66 | 287   | 0     | 0.79   |
| Intel     | SSDPE21K750GA      | 752 GB | ABF12D38158B | 286   | 0     | 0.79   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 3FD0EB78C1F7 | 286   | 0     | 0.79   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | E6B50064E0BF | 286   | 0     | 0.79   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 9F3BEC33E29F | 286   | 0     | 0.79   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 0B677493662C | 286   | 0     | 0.79   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 152FF9304FFF | 286   | 0     | 0.78   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | CFBC13446BA0 | 286   | 0     | 0.78   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | FB3C26CA77DA | 286   | 0     | 0.78   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | FE86CEDB1C2B | 286   | 0     | 0.78   |
| Intel     | SSDPE2KE032T8      | 3.2 TB | 5FDC791438C5 | 286   | 0     | 0.78   |
| Kingston  | SKC2500M8250G      | 250 GB | 7AE9F102BF45 | 286   | 0     | 0.78   |
| Micron    | 9300_MTFDHAL7T6TDP | 7.6 TB | 0B0C7D304623 | 285   | 0     | 0.78   |
| Micron    | 9300_MTFDHAL7T6TDP | 7.6 TB | BAA31A4AF81D | 285   | 0     | 0.78   |
| WDC       | CL SN720 SDAQNT... | 512 GB | B9DA6683E958 | 285   | 0     | 0.78   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | AD5A175E8DB5 | 285   | 0     | 0.78   |
| WDC       | WUS4BB076D7P3E1    | 7.6 TB | DEE8C3920760 | 285   | 0     | 0.78   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | F6EA8119CC05 | 284   | 0     | 0.78   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | DDF838A7D45D | 284   | 0     | 0.78   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | F9663808445B | 284   | 0     | 0.78   |
| Micron    | 2200_MTFDHBA512TCK | 512 GB | E81E2D7D2981 | 284   | 0     | 0.78   |
| WDC       | WUS4BB096D7P3E1    | 720 GB | DBE2D65C6844 | 284   | 0     | 0.78   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 03BC5048B7CB | 284   | 0     | 0.78   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | BB8A6BA7CB07 | 284   | 0     | 0.78   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | 3481851C68A6 | 284   | 0     | 0.78   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 17D13FA0BCBB | 284   | 0     | 0.78   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 4B91AC82CC2A | 284   | 0     | 0.78   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 75512DC70FF4 | 284   | 0     | 0.78   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 02A4ACAEB5DD | 284   | 0     | 0.78   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 2539CBAA93FE | 284   | 0     | 0.78   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 3FBF5E194F64 | 284   | 0     | 0.78   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 6A416E154D85 | 284   | 0     | 0.78   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 921BE6AF33E5 | 284   | 0     | 0.78   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 980CEB7F8EEB | 284   | 0     | 0.78   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 9BD38B395038 | 284   | 0     | 0.78   |
| Phison    | Viper M.2 VPN100   | 2 TB   | 5C41974EA625 | 284   | 0     | 0.78   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 1A7C38AE4599 | 283   | 0     | 0.78   |
| Samsung   | SSD 970 PRO        | 512 GB | 9BF6F72DA1C1 | 283   | 0     | 0.78   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 2E5B141CE407 | 283   | 0     | 0.78   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 1FBD8F0B7DBF | 283   | 0     | 0.78   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | A74218856079 | 283   | 0     | 0.78   |
| Intel     | SSDPEKKA512G8      | 512 GB | 4946729AEA1D | 283   | 0     | 0.78   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 15CDC6FDA1D9 | 283   | 0     | 0.78   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | B0633D6DA296 | 283   | 0     | 0.78   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 0F2180E5C4D1 | 282   | 0     | 0.78   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 1A0AE6B9B7D0 | 282   | 0     | 0.78   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 8C7A3B94F533 | 282   | 0     | 0.78   |
| Intel     | SSDPE2ME400G4      | 400 GB | FEC0BCA148A6 | 282   | 0     | 0.77   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 9343FF594FAA | 282   | 0     | 0.77   |
| Samsung   | MZVLW128HEGR-000L1 | 128 GB | 19F6265C67A6 | 282   | 0     | 0.77   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 585BCEFB1C6B | 282   | 0     | 0.77   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | D340D9236645 | 282   | 0     | 0.77   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | FDEB188B3ED9 | 282   | 0     | 0.77   |
| Intel     | SSDPE2KX010T8      | 1 TB   | E8D35765693B | 282   | 0     | 0.77   |
| Micron    | 9300_MTFDHAL3T2TDR | 3.2 TB | 2280187B2912 | 282   | 0     | 0.77   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | A7C378592E4F | 282   | 0     | 0.77   |
| WDC       | CL SN720 SDAQNT... | 512 GB | 1A2AC9610B0B | 281   | 0     | 0.77   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | AF32A9799879 | 281   | 0     | 0.77   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | E0D4D89F6F51 | 281   | 0     | 0.77   |
| Intel     | SSDPE2KX010T8      | 1 TB   | 1559AEA0A95B | 281   | 0     | 0.77   |
| Intel     | SSDPE2MX450G7      | 450 GB | 6A73ECDF69A9 | 281   | 0     | 0.77   |
| WDC       | WUS4BB096D7P3E1    | 720 GB | 666FD2B76B0E | 281   | 0     | 0.77   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | B567E5C82C36 | 281   | 0     | 0.77   |
| Intel     | SSDPECME016T4      | 800 GB | CAC780FE9166 | 281   | 0     | 0.77   |
| Intel     | SSDPECME016T4      | 800 GB | D03F99AA4684 | 281   | 0     | 0.77   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 3E855DB9D250 | 280   | 0     | 0.77   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 746C5DFB3B56 | 280   | 0     | 0.77   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | BE26D8C36222 | 280   | 0     | 0.77   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | C6E455E15124 | 280   | 0     | 0.77   |
| Kingston  | SEDC1000M1920G     | 1.9 TB | BCECE0B480AB | 280   | 0     | 0.77   |
| Samsung   | MZPLJ6T4HALA-00007 | 6.4 TB | B7C092BC7D66 | 280   | 0     | 0.77   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 3F1FDC025661 | 280   | 0     | 0.77   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | 1E960DA5C017 | 280   | 0     | 0.77   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | 87DF4078A6EF | 280   | 0     | 0.77   |
| WDC       | WUS4BB096D7P3E1    | 720 GB | E8E64D4136C9 | 280   | 0     | 0.77   |
| Samsung   | SSD 970 EVO Plus   | 500 GB | 44F2109A2357 | 280   | 0     | 0.77   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 073CA3C59994 | 280   | 0     | 0.77   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 1494E6829A09 | 280   | 0     | 0.77   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 491A32E4BF0A | 279   | 0     | 0.77   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 504914BD775C | 279   | 0     | 0.77   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 51CDE42F4EB6 | 279   | 0     | 0.77   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 7D9BB143E8F8 | 279   | 0     | 0.77   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 98B12467B7A1 | 279   | 0     | 0.77   |
| WDC       | WUS3CA116C7P3E3    | 1.6 TB | 221394740BAD | 278   | 0     | 0.76   |
| Wester... | WUS3CA116C7P3E3    | 1.6 TB | 221394740BAD | 278   | 0     | 0.76   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 004A12AE3542 | 278   | 0     | 0.76   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 45E1B5019DDD | 278   | 0     | 0.76   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 71656536A16A | 278   | 0     | 0.76   |
| Intel     | SSDPE21K750GA      | 752 GB | BC0140BBA813 | 277   | 0     | 0.76   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 1C73408FB3D3 | 277   | 0     | 0.76   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 8F1BE237E213 | 277   | 0     | 0.76   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 07A5ECE344BD | 277   | 0     | 0.76   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | 7DB94337904E | 277   | 0     | 0.76   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | EAB902DDEEE7 | 277   | 0     | 0.76   |
| Samsung   | VO001920KWVMT 1... | 1.9 TB | 8290BB4AB920 | 277   | 0     | 0.76   |
| Samsung   | VO001920KWVMT 1... | 1.9 TB | B737EDF65810 | 277   | 0     | 0.76   |
| Samsung   | MZ1LB3T8HMLA-00007 | 3.8 TB | 178BAB1BAECF | 277   | 0     | 0.76   |
| WDC       | WUS4BB038D7P3E1    | 3.8 TB | B8E91BD02A65 | 277   | 0     | 0.76   |
| Intel     | SSDPED1K375GA      | 375 GB | 46265597831C | 277   | 0     | 0.76   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 1BA564C8B2B2 | 277   | 0     | 0.76   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 203531414DBE | 277   | 0     | 0.76   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 278A5FEA93EF | 277   | 0     | 0.76   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | C82958836495 | 277   | 0     | 0.76   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | C19B66F8E02B | 276   | 0     | 0.76   |
| WDC       | CL SN720 SDAQNT... | 512 GB | D89F2566D406 | 276   | 0     | 0.76   |
| Samsung   | SSD 970 PRO        | 512 GB | 3B9960E0ECEA | 276   | 0     | 0.76   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 2CB34E6FC255 | 276   | 0     | 0.76   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | E52A62938978 | 276   | 0     | 0.76   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 2BD2945083B2 | 276   | 0     | 0.76   |
| Intel     | SSDPED1K750GA      | 752 GB | E87C819A83F2 | 275   | 0     | 0.76   |
| Seagate   | FireCuda 520 SS... | 500 GB | B69B65AEADAB | 275   | 0     | 0.75   |
| Intel     | SSDPED1K375GA      | 375 GB | 9DB7D44D9F7A | 275   | 0     | 0.75   |
| ADATA     | SX8200PNP          | 256 GB | 7B6CEE3C4B79 | 274   | 0     | 0.75   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | EF6CEDA67C12 | 274   | 0     | 0.75   |
| Samsung   | MZPLJ6T4HALA-00007 | 6.4 TB | 84DEC1AC7D38 | 274   | 0     | 0.75   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 7FB6076AE15C | 274   | 0     | 0.75   |
| Intel     | SSDPE2KX010T8      | 1 TB   | CE66C8582F19 | 273   | 0     | 0.75   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 9B0674F4BC93 | 273   | 0     | 0.75   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | A0BB04ECE427 | 273   | 0     | 0.75   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 309D32EBE80D | 273   | 0     | 0.75   |
| Intel     | SSDPE2KX010T8      | 1 TB   | 626673511A7C | 273   | 0     | 0.75   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 061B5B450DE7 | 273   | 0     | 0.75   |
| Intel     | SSDPE2KX010T8      | 1 TB   | 0B9821CD2808 | 272   | 0     | 0.75   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | AEE3D8A090BE | 272   | 0     | 0.75   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 03CE5072F904 | 271   | 0     | 0.74   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | BFBF8F5A1B8B | 271   | 0     | 0.74   |
| Samsung   | MZPJB480HMGC-0BW07 | 480 GB | 0373271A5BE7 | 271   | 0     | 0.74   |
| HGST      | HUSMR7638BDP3Y1    | 3.8 TB | 27D8D3F909BC | 271   | 0     | 0.74   |
| HGST      | HUSMR7638BDP3Y1    | 3.8 TB | B4DCD7436235 | 271   | 0     | 0.74   |
| HGST      | HUSMR7638BDP3Y1    | 3.8 TB | D5B64E594076 | 271   | 0     | 0.74   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | D73A9E8E267D | 271   | 0     | 0.74   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | ADDA19D3F4F9 | 271   | 0     | 0.74   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 0F69C8D74BB6 | 270   | 0     | 0.74   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 50BC11F84600 | 270   | 0     | 0.74   |
| Samsung   | VO001920KWVMT 1... | 1.9 TB | 5B9FD4DDE0BD | 270   | 0     | 0.74   |
| Samsung   | VO001920KWVMT 1... | 1.9 TB | A4C561402709 | 270   | 0     | 0.74   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 20F1E52FD067 | 270   | 0     | 0.74   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 5406FD7086BD | 270   | 0     | 0.74   |
| HGST      | HUSMR7638BDP3Y1    | 3.8 TB | 4A1153BB9869 | 269   | 0     | 0.74   |
| HGST      | HUSMR7638BDP3Y1    | 3.8 TB | 70C2D6049D59 | 269   | 0     | 0.74   |
| HGST      | HUSMR7638BDP3Y1    | 3.8 TB | 9B5664C44926 | 269   | 0     | 0.74   |
| HGST      | HUSMR7638BDP3Y1    | 3.8 TB | CCE7879D51B6 | 269   | 0     | 0.74   |
| HGST      | HUSMR7638BDP3Y1    | 3.8 TB | D0D28F3A5D20 | 269   | 0     | 0.74   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | 37DBCC090F49 | 269   | 0     | 0.74   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | 8E6E76645987 | 269   | 0     | 0.74   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | EF8E35B4417E | 269   | 0     | 0.74   |
| Intel     | SSDPE21K375GA      | 375 GB | B18721349199 | 269   | 0     | 0.74   |
| WDC       | CL SN720 SDAQNT... | 512 GB | BB6B4FE8DD21 | 269   | 0     | 0.74   |
| Intel     | SSDPED1K750GA      | 752 GB | C2B0961F8505 | 269   | 0     | 0.74   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | D98654664287 | 269   | 0     | 0.74   |
| Samsung   | SSD 970 EVO        | 1 TB   | 609A5459EADA | 1077  | 3     | 0.74   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 5303BF42472E | 269   | 0     | 0.74   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | C7C91E78B863 | 269   | 0     | 0.74   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 2CF3A52A466B | 269   | 0     | 0.74   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | D143B89DB9A3 | 269   | 0     | 0.74   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 4091604B3E6D | 269   | 0     | 0.74   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | FE9ABA25C744 | 269   | 0     | 0.74   |
| Intel     | SSDPE2KE032T8      | 3.2 TB | A61C17A94F1A | 268   | 0     | 0.74   |
| WDC       | CL SN720 SDAQNT... | 512 GB | 78FB8FD27651 | 268   | 0     | 0.74   |
| Intel     | SSDPE2KE032T8      | 3.2 TB | 2CE887682CC8 | 268   | 0     | 0.74   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | E758002B5DDD | 268   | 0     | 0.73   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 9F8D36B857C4 | 268   | 0     | 0.73   |
| WDC       | CL SN720 SDAQNT... | 512 GB | 67FB97EE8A23 | 268   | 0     | 0.73   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | C850C1C0A92C | 267   | 0     | 0.73   |
| Seagate   | FireCuda 520 SS... | 500 GB | 5D0E11142A1F | 267   | 0     | 0.73   |
| Micron    | 9300_MTFDHAL3T2TDR | 3.2 TB | DAA8A215BD9A | 267   | 0     | 0.73   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | F4B2EF24D516 | 267   | 0     | 0.73   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 907A8ADB29DF | 267   | 0     | 0.73   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | C858455DB74F | 267   | 0     | 0.73   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 49CB5EB62924 | 267   | 0     | 0.73   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 172CE5182431 | 267   | 0     | 0.73   |
| HGST      | HUSMR7638BDP3Y1    | 3.8 TB | 9067C275F628 | 267   | 0     | 0.73   |
| Micron    | 2200_MTFDHBA512TCK | 512 GB | C526399FC36B | 267   | 0     | 0.73   |
| Intel     | SSDPE2KX010T8      | 1 TB   | D0FE2071FC59 | 267   | 0     | 0.73   |
| Micron    | 2200_MTFDHBA512TCK | 512 GB | 69C13EEDC0B8 | 266   | 0     | 0.73   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 5CDA649D913E | 266   | 0     | 0.73   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | 13B6768CB7FE | 266   | 0     | 0.73   |
| Samsung   | MZPLJ6T4HALA-00007 | 6.4 TB | F4690EBF53B2 | 266   | 0     | 0.73   |
| Intel     | SSDPED1K750GA      | 752 GB | 21F0D6996FDC | 266   | 0     | 0.73   |
| Micron    | 2200_MTFDHBA512TCK | 512 GB | 7AB7C8E9C6B8 | 266   | 0     | 0.73   |
| Micron    | 2200_MTFDHBA512TCK | 512 GB | EE2C33B5F113 | 265   | 0     | 0.73   |
| Samsung   | SSD 970 EVO        | 250 GB | 3ACA136FD0EA | 531   | 1     | 0.73   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 49CA984D8D89 | 265   | 0     | 0.73   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | B7463DFBED2A | 265   | 0     | 0.73   |
| Micron    | 2200_MTFDHBA512TCK | 512 GB | 2160F9D05B95 | 265   | 0     | 0.73   |
| HGST      | HUSMR7638BDP3Y1    | 3.8 TB | 13499E1217CD | 265   | 0     | 0.73   |
| Intel     | SSDPE2KX010T8      | 1 TB   | 66B51BABE7A3 | 265   | 0     | 0.73   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | 1C30A160AD31 | 265   | 0     | 0.73   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | CC2413990B94 | 265   | 0     | 0.73   |
| Samsung   | MZVLB1T0HALR-00000 | 1 TB   | 4A80093A5697 | 264   | 0     | 0.73   |
| Samsung   | MZVLB1T0HALR-00000 | 1 TB   | B9F668B2BEE2 | 264   | 0     | 0.72   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 094958A5ABDC | 263   | 0     | 0.72   |
| Samsung   | MZVLB1T0HALR-00000 | 1 TB   | A9982AD2200F | 263   | 0     | 0.72   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | A0421CA3D5A4 | 263   | 0     | 0.72   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 78A1E55C5751 | 263   | 0     | 0.72   |
| Dell      | Express Flash N... | 1.6 TB | FDDA8603FE14 | 263   | 0     | 0.72   |
| Micron    | 2200_MTFDHBA512TCK | 512 GB | 93FA596351E2 | 263   | 0     | 0.72   |
| Samsung   | MZVLB1T0HALR-00000 | 1 TB   | 4AC3F6C27F4A | 263   | 0     | 0.72   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | DC1C2FE11015 | 263   | 0     | 0.72   |
| WDC       | CL SN720 SDAQNT... | 512 GB | 2154E14BA6C3 | 263   | 0     | 0.72   |
| HP        | SSD EX950          | 512 GB | 4480B034869B | 263   | 0     | 0.72   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 7706B5BC9EFB | 263   | 0     | 0.72   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | B6DE5C9CFA46 | 263   | 0     | 0.72   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 3A5BA68F5808 | 262   | 0     | 0.72   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | DD0855965300 | 262   | 0     | 0.72   |
| Intel     | EO000375KWJUC      | 375 GB | FD5687E4E1FC | 262   | 0     | 0.72   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 7C4B3B179B12 | 262   | 0     | 0.72   |
| Micron    | 2200_MTFDHBA512TCK | 512 GB | 14B36D270AD6 | 262   | 0     | 0.72   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | A4240F317103 | 262   | 0     | 0.72   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 056C56B7D3CE | 261   | 0     | 0.72   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | DEF3A60A1CF1 | 261   | 0     | 0.72   |
| Samsung   | MZPLJ6T4HALA-00007 | 6.4 TB | AF79586141AF | 261   | 0     | 0.72   |
| Micron    | 9300_MTFDHAL3T2TDR | 3.2 TB | 17770C9580B0 | 261   | 0     | 0.72   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 801E56FC9876 | 261   | 0     | 0.72   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 1FDD256D8BF8 | 261   | 0     | 0.72   |
| Kingston  | SKC2500M81000G     | 1 TB   | 7AC5542FE29C | 260   | 0     | 0.71   |
| Micron    | 2200_MTFDHBA512TCK | 512 GB | 3CDECBC25159 | 260   | 0     | 0.71   |
| Samsung   | MZPLJ6T4HALA-00007 | 6.4 TB | 80EBC25AADD7 | 260   | 0     | 0.71   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 309D32EBE80D | 260   | 0     | 0.71   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 7E2B10FE5537 | 260   | 0     | 0.71   |
| WDC       | WUS4BB038D7P3E1    | 3.8 TB | AC099C036056 | 260   | 0     | 0.71   |
| WDC       | WUS4BB038D7P3E1    | 3.8 TB | FA899B27C750 | 260   | 0     | 0.71   |
| Micron    | 2200_MTFDHBA512TCK | 512 GB | 29E142EFA074 | 260   | 0     | 0.71   |
| Dell      | Express Flash N... | 1.6 TB | 503A0EDBF202 | 260   | 0     | 0.71   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 978C81820B62 | 260   | 0     | 0.71   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 6DD57628A180 | 259   | 0     | 0.71   |
| Dell      | Express Flash P... | 1.6 TB | 4721043E755C | 259   | 0     | 0.71   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 2322C0D32589 | 259   | 0     | 0.71   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 4F4AF95125D0 | 259   | 0     | 0.71   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 59448C5B9052 | 259   | 0     | 0.71   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 7DDAAF0D24B4 | 259   | 0     | 0.71   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 242371598770 | 259   | 0     | 0.71   |
| Samsung   | MZPLJ6T4HALA-00007 | 6.4 TB | 46ED5DBF6CD8 | 259   | 0     | 0.71   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 319C69241D1A | 259   | 0     | 0.71   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | E081266A6EA6 | 259   | 0     | 0.71   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 5B24EEBC12EA | 259   | 0     | 0.71   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 4BF1DA5106E0 | 259   | 0     | 0.71   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | B8636643BCF0 | 259   | 0     | 0.71   |
| Micron    | 2200_MTFDHBA512TCK | 512 GB | 3BC0291FD216 | 259   | 0     | 0.71   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 21BA42AEAF12 | 259   | 0     | 0.71   |
| WDC       | WDS250G2B0C-00PXH0 | 250 GB | 0F3C3609D90E | 258   | 0     | 0.71   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 69BFAD101A05 | 258   | 0     | 0.71   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 95E1E1ED834D | 258   | 0     | 0.71   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | D28676489616 | 258   | 0     | 0.71   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | DE6DFFAAD59F | 258   | 0     | 0.71   |
| Intel     | SSDPE21K375GA      | 375 GB | 1E8564B56C99 | 258   | 0     | 0.71   |
| Dell      | Express Flash N... | 1.6 TB | E137578E8B03 | 258   | 0     | 0.71   |
| Samsung   | MZPJB480HMGC-0BW07 | 480 GB | 20CF42765EAC | 258   | 0     | 0.71   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 726C720B4F8A | 258   | 0     | 0.71   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 6986ABC8638B | 258   | 0     | 0.71   |
| WDC       | CL SN720 SDAQNT... | 512 GB | AC810CF48049 | 258   | 0     | 0.71   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 953D74E4A5C9 | 258   | 0     | 0.71   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 1C73408FB3D3 | 257   | 0     | 0.71   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 27D675535BFB | 257   | 0     | 0.71   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 652E277D2253 | 257   | 0     | 0.71   |
| Samsung   | SSD 970 EVO Plus   | 1 TB   | EAB755B6B575 | 257   | 0     | 0.71   |
| Samsung   | MZPJB480HMGC-0BW07 | 480 GB | FF7BE86DBAB5 | 257   | 0     | 0.71   |
| Intel     | SSDPE2ME400G4      | 400 GB | 272E84564EFE | 257   | 0     | 0.71   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 094958A5ABDC | 257   | 0     | 0.71   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | AB59A93C0070 | 257   | 0     | 0.71   |
| Samsung   | MZ1LB3T8HMLA-00007 | 3.8 TB | 41F4D89CB31E | 257   | 0     | 0.70   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 7AFF81956B78 | 257   | 0     | 0.70   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | EB989B16F4DA | 257   | 0     | 0.70   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | F95900DBD8A3 | 257   | 0     | 0.70   |
| Samsung   | SSD 970 PRO        | 512 GB | 8BD8E0190AAD | 257   | 0     | 0.70   |
| Samsung   | SSD 970 EVO Plus   | 1 TB   | 125D7A088A7B | 257   | 0     | 0.70   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | 6C4ECDC34730 | 257   | 0     | 0.70   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | BD39D2315189 | 256   | 0     | 0.70   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | D30335EFDC11 | 256   | 0     | 0.70   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | FB9529C788C3 | 256   | 0     | 0.70   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 1FB5DD27EEE9 | 256   | 0     | 0.70   |
| WDC       | WUS4BB038D7P3E1    | 3.8 TB | 421DAAB41807 | 256   | 0     | 0.70   |
| WDC       | WUS4BB038D7P3E3    | 3.8 TB | 01A88B4A6899 | 256   | 0     | 0.70   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 59D1C93143EB | 256   | 0     | 0.70   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 23898AED4B61 | 256   | 0     | 0.70   |
| Micron    | 2200_MTFDHBA512TCK | 512 GB | 2975A98B886E | 256   | 0     | 0.70   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 54EE46BBED69 | 256   | 0     | 0.70   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 16FA7CC62177 | 256   | 0     | 0.70   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 231897C1F6C4 | 256   | 0     | 0.70   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | C9DBA494046A | 256   | 0     | 0.70   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | CF45827114B0 | 256   | 0     | 0.70   |
| Dell      | Express Flash N... | 1.6 TB | 0EE60FE7C0D8 | 256   | 0     | 0.70   |
| WDC       | WDS250G3X0C-00SJG0 | 250 GB | F09C714D0F6E | 255   | 0     | 0.70   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 835E08BFCCB4 | 255   | 0     | 0.70   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | D8BC2051D921 | 255   | 0     | 0.70   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 65511FDC426A | 255   | 0     | 0.70   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | A787E6CA4491 | 255   | 0     | 0.70   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 9322BFF1DAA7 | 255   | 0     | 0.70   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 0EFF3EE16370 | 255   | 0     | 0.70   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 28C9DCC095BE | 255   | 0     | 0.70   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 4B59E6962FEC | 255   | 0     | 0.70   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 5CF737DDFC7E | 255   | 0     | 0.70   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 9322BFF1DAA7 | 255   | 0     | 0.70   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | D538A467FA02 | 255   | 0     | 0.70   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | DCB6867C2AC6 | 255   | 0     | 0.70   |
| Intel     | SSDPED1D280GA      | 280 GB | EBCDDF9F35F4 | 766   | 2     | 0.70   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 204878757FF9 | 255   | 0     | 0.70   |
| HGST      | HUSMR7638BDP3Y1    | 3.8 TB | AD41A3C3D1CD | 255   | 0     | 0.70   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | EB6655DC803B | 254   | 0     | 0.70   |
| Micron    | 2200_MTFDHBA512TCK | 512 GB | CFD7C7E07071 | 254   | 0     | 0.70   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 17D546DA5636 | 254   | 0     | 0.70   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 7D06FE107A54 | 254   | 0     | 0.70   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | B2F0CE1EB78E | 254   | 0     | 0.70   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 4E5EE8111D40 | 254   | 0     | 0.70   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | EDF2CCC9A730 | 254   | 0     | 0.70   |
| Intel     | SSDPE2KE032T8      | 3.2 TB | B2D4E8CBF10C | 254   | 0     | 0.70   |
| Intel     | SSDPE2KE032T8      | 3.2 TB | 23C069AB245C | 254   | 0     | 0.70   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 24FB96758FEE | 254   | 0     | 0.70   |
| Corsair   | Force MP510 1.9TB  | 1.9 TB | 36F0F4EFE459 | 254   | 0     | 0.70   |
| ADATA     | SX8200PNP          | 256 GB | 06BA3BB257CB | 254   | 0     | 0.70   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | 1AAB9EB92023 | 254   | 0     | 0.70   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 03F2FCA4929A | 254   | 0     | 0.70   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 023E1F4B4EEC | 254   | 0     | 0.70   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 1D89DDFB2680 | 254   | 0     | 0.70   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 333E5C6E2446 | 253   | 0     | 0.70   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 353820196378 | 253   | 0     | 0.70   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 6588152B5470 | 253   | 0     | 0.70   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 6C424254457A | 253   | 0     | 0.70   |
| Samsung   | MZQLB3T8HALS-000AZ | 3.8 TB | 4CABAFFBDC37 | 253   | 0     | 0.69   |
| Samsung   | MZQLB3T8HALS-000AZ | 3.8 TB | 5FCC3C274FA7 | 253   | 0     | 0.69   |
| Samsung   | MZQLB3T8HALS-000AZ | 3.8 TB | ACB7C2A819F0 | 253   | 0     | 0.69   |
| Samsung   | MZQLB3T8HALS-000AZ | 3.8 TB | C0E16CB06EF8 | 253   | 0     | 0.69   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 1D7A66E7E9D3 | 253   | 0     | 0.69   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 784DFD991C90 | 253   | 0     | 0.69   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | ABD5AA249FF9 | 253   | 0     | 0.69   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | A2C51B88B3BA | 253   | 0     | 0.69   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | F451A2AAB927 | 253   | 0     | 0.69   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 644382597E7C | 253   | 0     | 0.69   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | E2EC210233BD | 253   | 0     | 0.69   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | F739E213E85D | 253   | 0     | 0.69   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | FF0CF4490946 | 253   | 0     | 0.69   |
| Micron    | 2200_MTFDHBA512TCK | 512 GB | 247767E3FA74 | 253   | 0     | 0.69   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | FF013B8EA4CB | 253   | 0     | 0.69   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 061B5B450DE7 | 253   | 0     | 0.69   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 3601449C946D | 253   | 0     | 0.69   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 5F3DF42509D2 | 252   | 0     | 0.69   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 61C8D1CCEE5C | 252   | 0     | 0.69   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | CD2E9549D03D | 252   | 0     | 0.69   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 51CDE42F4EB6 | 252   | 0     | 0.69   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 15D480FF5ACC | 252   | 0     | 0.69   |
| WDC       | WUS4BB038D7P3E3    | 3.8 TB | 5892C9E060F9 | 252   | 0     | 0.69   |
| WDC       | WUS4BB038D7P3E3    | 3.8 TB | B2ECA2CA49B5 | 252   | 0     | 0.69   |
| WDC       | WUS4BB038D7P3E3    | 3.8 TB | FB6EB597D0F5 | 252   | 0     | 0.69   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | BE539568069F | 252   | 0     | 0.69   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 3BAC0E8D0D02 | 251   | 0     | 0.69   |
| WDC       | WUS4BB038D7P3E3    | 3.8 TB | 355FFB3226DC | 251   | 0     | 0.69   |
| WDC       | WUS4BB038D7P3E3    | 3.8 TB | 7821D2D3886E | 251   | 0     | 0.69   |
| WDC       | WUS4BB038D7P3E3    | 3.8 TB | F367032F53F6 | 251   | 0     | 0.69   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | E25D4F19D530 | 251   | 0     | 0.69   |
| Intel     | SSDPE21K750GA      | 752 GB | 3F3B73AF42AB | 251   | 0     | 0.69   |
| Intel     | SSDPE21K750GA      | 752 GB | 7DE6EE1D3C04 | 251   | 0     | 0.69   |
| Intel     | SSDPE21K750GA      | 752 GB | 9B04AB9E60E0 | 251   | 0     | 0.69   |
| Intel     | SSDPE21K750GA      | 752 GB | B8A483BEC315 | 251   | 0     | 0.69   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 5069CCA85F84 | 251   | 0     | 0.69   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | BDD20AC2DFBD | 251   | 0     | 0.69   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 3C60156F48E9 | 251   | 0     | 0.69   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 5D6896E1A250 | 251   | 0     | 0.69   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 978D2A115D0D | 251   | 0     | 0.69   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | B3EF4C3AB851 | 251   | 0     | 0.69   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | D7D7BFAD0CCE | 251   | 0     | 0.69   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | E4B7801254B3 | 251   | 0     | 0.69   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 31AFA7559FAF | 250   | 0     | 0.69   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 43536792CDE1 | 250   | 0     | 0.69   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | EDD7262C1806 | 250   | 0     | 0.69   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 9CC336C6AAFC | 250   | 0     | 0.69   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | E8939C939B8A | 250   | 0     | 0.69   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 5EFCC548ACB9 | 250   | 0     | 0.69   |
| Samsung   | MZWLJ7T6HALA-00007 | 7.6 TB | 422B1224E2C8 | 250   | 0     | 0.69   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 1CC9F6D2444E | 250   | 0     | 0.69   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 9C6CCCA36728 | 250   | 0     | 0.69   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | A67E90093BA1 | 250   | 0     | 0.69   |
| Toshiba   | KXG60ZNV256G       | 256 GB | 863E742464C0 | 250   | 0     | 0.69   |
| Samsung   | MZVLB1T0HALR-00000 | 1 TB   | 71D0A457A6E9 | 250   | 0     | 0.68   |
| Samsung   | SSD 970 PRO        | 1 TB   | FE161159491B | 249   | 0     | 0.68   |
| Samsung   | MZVLB1T0HALR-00000 | 1 TB   | D212FFCB6366 | 249   | 0     | 0.68   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | AE361EC42847 | 249   | 0     | 0.68   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 10904DA0A782 | 249   | 0     | 0.68   |
| Intel     | SSDPE2KX010T8      | 1 TB   | 727D0B41B0D3 | 249   | 0     | 0.68   |
| Dell      | Express Flash N... | 1.6 TB | 2CD9DC12A2AD | 249   | 0     | 0.68   |
| Dell      | Express Flash N... | 1.6 TB | 470B84E2A6F2 | 249   | 0     | 0.68   |
| Dell      | Express Flash N... | 1.6 TB | 48DD4BEA2DE5 | 249   | 0     | 0.68   |
| Dell      | Express Flash N... | 1.6 TB | C9023A00D827 | 249   | 0     | 0.68   |
| HP        | SSD EX950          | 512 GB | 40D21ECD0FA8 | 249   | 0     | 0.68   |
| Intel     | SSDPECKE064T7ES    | 3.2 TB | 2B2E0887AAE6 | 249   | 0     | 0.68   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 6E925EF1E2FB | 248   | 0     | 0.68   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | DD728F2063D7 | 248   | 0     | 0.68   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | 6FF7228D0048 | 248   | 0     | 0.68   |
| Toshiba   | KXG60ZNV256G       | 256 GB | 66D585D6F63C | 248   | 0     | 0.68   |
| WDC       | WUS4BB038D7P3E1    | 3.8 TB | B022A70EF1BF | 247   | 0     | 0.68   |
| WDC       | WUS4BB038D7P3E1    | 3.8 TB | BEBD83CC0369 | 247   | 0     | 0.68   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 0205D60252FE | 247   | 0     | 0.68   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 8E1931016CD1 | 247   | 0     | 0.68   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 5FE4968E1554 | 247   | 0     | 0.68   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 1FA53A847BCF | 247   | 0     | 0.68   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 5863A6DB64B5 | 247   | 0     | 0.68   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | B90B31564B9B | 247   | 0     | 0.68   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | BC16180C7CEB | 247   | 0     | 0.68   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | D7D4BFC541AE | 247   | 0     | 0.68   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 3864FB0691AE | 247   | 0     | 0.68   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | BF9B7A9F102A | 247   | 0     | 0.68   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | D069FC5234FE | 247   | 0     | 0.68   |
| Samsung   | MZPLJ6T4HALA-00007 | 6.4 TB | 68E6B05FB34B | 247   | 0     | 0.68   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | AC112AF52682 | 247   | 0     | 0.68   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | B4BBEFD1945C | 246   | 0     | 0.68   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 91F575526B3B | 246   | 0     | 0.68   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 767E07DFF1E7 | 246   | 0     | 0.68   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 72BAA401DFEA | 246   | 0     | 0.68   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 64E73FDC998A | 246   | 0     | 0.68   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | F00F62E00AEC | 246   | 0     | 0.68   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | A7F57E4FCD40 | 246   | 0     | 0.68   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | AA92C28A36E5 | 246   | 0     | 0.68   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | C15C2AB0EC1D | 246   | 0     | 0.68   |
| Samsung   | SSD 970 PRO        | 512 GB | 10FF09969621 | 246   | 0     | 0.68   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 253FB08073C9 | 246   | 0     | 0.68   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 5F54DF56BD4C | 246   | 0     | 0.68   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | A6D662C08DC4 | 246   | 0     | 0.68   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | F64985C8C3E3 | 246   | 0     | 0.68   |
| WDC       | CL SN720 SDAQNT... | 512 GB | 58CEA544662B | 246   | 0     | 0.68   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 88F8726D8AA9 | 246   | 0     | 0.67   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | F15FBBA612EE | 246   | 0     | 0.67   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 84C586D96394 | 246   | 0     | 0.67   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 991010FE78F0 | 246   | 0     | 0.67   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | B1DF320AD09A | 246   | 0     | 0.67   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | BA117E568FF8 | 246   | 0     | 0.67   |
| Dell      | Express Flash N... | 1.6 TB | 16D5BCAD3FE7 | 245   | 0     | 0.67   |
| Dell      | Express Flash N... | 1.6 TB | 9F1A91B1E103 | 245   | 0     | 0.67   |
| Dell      | Express Flash N... | 1.6 TB | B825C2D1BEED | 245   | 0     | 0.67   |
| Dell      | Express Flash N... | 1.6 TB | ED53DA113FB9 | 245   | 0     | 0.67   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 96750158B9E5 | 245   | 0     | 0.67   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | FC2336F2584F | 245   | 0     | 0.67   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 0104E78B083F | 245   | 0     | 0.67   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 8A8030EC67D9 | 245   | 0     | 0.67   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | A0EDD928C370 | 245   | 0     | 0.67   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | AB922323CF2A | 245   | 0     | 0.67   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | B2CE8F6FABD5 | 245   | 0     | 0.67   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | C7F61F956BD0 | 245   | 0     | 0.67   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | DEB4A496BDA7 | 245   | 0     | 0.67   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | EA032A184992 | 245   | 0     | 0.67   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 60C3D3DCD04C | 245   | 0     | 0.67   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | ED3E608E22AC | 244   | 0     | 0.67   |
| Micron    | 2200_MTFDHBA512TCK | 512 GB | 4A99F89C4E8D | 244   | 0     | 0.67   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 869BD0DCF28D | 244   | 0     | 0.67   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | E759FE6C42C5 | 244   | 0     | 0.67   |
| Samsung   | MZQLW960HMJP-00003 | 960 GB | 86B8E33DAA4A | 244   | 0     | 0.67   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 0DFE53418296 | 244   | 0     | 0.67   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 676EFCE83011 | 244   | 0     | 0.67   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | AE0CE3190A87 | 244   | 0     | 0.67   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 73F2C76CA792 | 244   | 0     | 0.67   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 0BFC36A0B7ED | 244   | 0     | 0.67   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 2DFFF759014F | 244   | 0     | 0.67   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | BAB7173BF7C1 | 244   | 0     | 0.67   |
| Micron    | 2200_MTFDHBA256TCK | 256 GB | 5E30FF86D3E1 | 244   | 0     | 0.67   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 53AE9721D045 | 243   | 0     | 0.67   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 74D067F2D71E | 243   | 0     | 0.67   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 3D2E9BA24457 | 243   | 0     | 0.67   |
| Intel     | SSDPE21D480GA      | 480 GB | 70440EDFAAA1 | 243   | 0     | 0.67   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 088CE2F75B44 | 243   | 0     | 0.67   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 4D07B4A5027D | 243   | 0     | 0.67   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 4DC9F0A02954 | 243   | 0     | 0.67   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 5549F603236F | 243   | 0     | 0.67   |
| WDC       | WUS4BB038D7P3E1    | 3.8 TB | 8960883D703E | 243   | 0     | 0.67   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | 35D7A54F2A42 | 243   | 0     | 0.67   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 1004ED5C3096 | 243   | 0     | 0.67   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 36C6F183A29D | 243   | 0     | 0.67   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 7B0E0F15D3CA | 243   | 0     | 0.67   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 844D74DD0800 | 243   | 0     | 0.67   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | C6AB3A3FFCD8 | 243   | 0     | 0.67   |
| Samsung   | SSD 970 PRO        | 512 GB | 5E603713D47C | 243   | 0     | 0.67   |
| WDC       | CL SN720 SDAQNT... | 512 GB | 5938272CDD34 | 486   | 1     | 0.67   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 015B306E9F3A | 242   | 0     | 0.67   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 112B29B70A3C | 242   | 0     | 0.67   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | C08FDD2D6A67 | 242   | 0     | 0.67   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | C0DA54CCC4DE | 242   | 0     | 0.67   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | C992FCD20960 | 242   | 0     | 0.67   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | DABD9425709E | 242   | 0     | 0.67   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | DB431177F1F8 | 242   | 0     | 0.67   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 821D43A8F98E | 242   | 0     | 0.67   |
| Intel     | SSDPE2KX010T7      | 1 TB   | 3DE87E2E124F | 242   | 0     | 0.66   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | BD4150C6928E | 242   | 0     | 0.66   |
| Samsung   | MZ4LB3T8HALS-00003 | 3.8 TB | 8737961D3F73 | 242   | 0     | 0.66   |
| Intel     | SSDPE21D480GA      | 480 GB | 6148BB911989 | 241   | 0     | 0.66   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 69CDF882C4ED | 241   | 0     | 0.66   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | 1C8E53528DF2 | 241   | 0     | 0.66   |
| Samsung   | MZVLB1T0HBLR-00000 | 1 TB   | CB1FE1E8C8D7 | 241   | 0     | 0.66   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 74E1972517B1 | 241   | 0     | 0.66   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 276FD2089D59 | 241   | 0     | 0.66   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 614D6C56B1E9 | 241   | 0     | 0.66   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 9D9A150DD452 | 241   | 0     | 0.66   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | AF7F011E1463 | 241   | 0     | 0.66   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | B0E128FC0368 | 241   | 0     | 0.66   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | D2818888347C | 241   | 0     | 0.66   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | D41410EFE7A0 | 241   | 0     | 0.66   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | EE2100AC2D68 | 241   | 0     | 0.66   |
| Dell      | Express Flash N... | 1.6 TB | ADABF365B44F | 241   | 0     | 0.66   |
| Dell      | Express Flash N... | 1.6 TB | AED3FAFDC6D7 | 241   | 0     | 0.66   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 8C6C50CF27E5 | 241   | 0     | 0.66   |
| WDC       | WUS4BB038D7P3E1    | 3.8 TB | 99ACC4AB8CCF | 241   | 0     | 0.66   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | A613F04E5F05 | 241   | 0     | 0.66   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 0647414C4B73 | 241   | 0     | 0.66   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 59637CD9FCE3 | 240   | 0     | 0.66   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 7B3FAB701279 | 240   | 0     | 0.66   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | 40ECA006C4CB | 240   | 0     | 0.66   |
| Toshiba   | KXD51RUE1T92       | 1.9 TB | DEDADDD62988 | 240   | 0     | 0.66   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 103573840F45 | 240   | 0     | 0.66   |
| Toshiba   | KXG60ZNV256G       | 256 GB | 9AAE72178EF1 | 240   | 0     | 0.66   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 93543A7E4E57 | 240   | 0     | 0.66   |
| Micron    | 9300_MTFDHAL3T2TDR | 3.2 TB | A46399EDD385 | 240   | 0     | 0.66   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 4EE0AC490A7D | 240   | 0     | 0.66   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | BB8093629CC9 | 240   | 0     | 0.66   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | AFDCD495DEA3 | 240   | 0     | 0.66   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | E572915E13D1 | 240   | 0     | 0.66   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | FFC965A68979 | 240   | 0     | 0.66   |
| Kingston  | SEDC1000M3840G     | 3.8 TB | 6B392331B67A | 240   | 0     | 0.66   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 53F991C1DF5C | 239   | 0     | 0.66   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | 33CAE3DFB777 | 239   | 0     | 0.66   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 0AF1FB133217 | 239   | 0     | 0.66   |
| WDC       | WDS500G3XHC-00SJG0 | 500 GB | B36D744A8666 | 239   | 0     | 0.66   |
| Dell      | Express Flash N... | 1.6 TB | B4D6B428649D | 239   | 0     | 0.66   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 100B6E7D89E0 | 239   | 0     | 0.66   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 76FC4B5C98D6 | 239   | 0     | 0.66   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 8CEE54D953F6 | 239   | 0     | 0.66   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | E3A65CB6EE70 | 239   | 0     | 0.66   |
| Samsung   | MZPLJ6T4HALA-00007 | 6.4 TB | DBE9B28D83D8 | 239   | 0     | 0.66   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 145D470EEAC2 | 238   | 0     | 0.65   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 22C2AA02A37B | 238   | 0     | 0.65   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 58CC3C9D2D51 | 238   | 0     | 0.65   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | F23F350EB250 | 238   | 0     | 0.65   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 75B3BBD411DA | 238   | 0     | 0.65   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | BC544FCCB157 | 238   | 0     | 0.65   |
| WDC       | CL SN720 SDAQNT... | 512 GB | E0D193B5F537 | 238   | 0     | 0.65   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | DE323DB871EC | 237   | 0     | 0.65   |
| Toshiba   | KXG60ZNV256G       | 256 GB | 16A3836D6E7F | 237   | 0     | 0.65   |
| WDC       | WUS4BB038D7P3E3    | 3.8 TB | 05181ED19198 | 237   | 0     | 0.65   |
| Samsung   | MZPLJ6T4HALA-00007 | 6.4 TB | D837A300859C | 237   | 0     | 0.65   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 7D4B2564E458 | 237   | 0     | 0.65   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 29DB75D89C24 | 237   | 0     | 0.65   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 2A0874F2BF31 | 237   | 0     | 0.65   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 6A1950E2FB99 | 237   | 0     | 0.65   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | BD9D9F573B7B | 237   | 0     | 0.65   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | B40703A64183 | 237   | 0     | 0.65   |
| Samsung   | MZVLQ512HALU-00000 | 512 GB | 7D6808B55043 | 236   | 0     | 0.65   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | F79E4B63A292 | 236   | 0     | 0.65   |
| Intel     | SSDPE2NU076T8      | 7.6 TB | 148151F160B5 | 236   | 0     | 0.65   |
| Intel     | SSDPE2NU076T8      | 7.6 TB | 5AAEDC1B441D | 236   | 0     | 0.65   |
| Intel     | SSDPE2NU076T8      | 7.6 TB | C052E0B12D3E | 236   | 0     | 0.65   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 31B181728E48 | 236   | 0     | 0.65   |
| Samsung   | MZPLJ6T4HALA-00007 | 6.4 TB | E8D6943FD279 | 236   | 0     | 0.65   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 41F892235B01 | 236   | 0     | 0.65   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 061C2D8A88FB | 236   | 0     | 0.65   |
| Kingston  | SEDC1000M3840G     | 3.8 TB | C7E1DB7ED00C | 236   | 0     | 0.65   |
| Dell      | Express Flash N... | 1.6 TB | 35EC5691E254 | 236   | 0     | 0.65   |
| Dell      | Express Flash N... | 1.6 TB | 52DB06934C54 | 236   | 0     | 0.65   |
| Intel     | SSDPED1D480GA      | 480 GB | E1E3D12EFFDE | 236   | 0     | 0.65   |
| Samsung   | SSD 960 PRO        | 512 GB | 0831312FD967 | 235   | 0     | 0.65   |
| WDC       | WUS4CB016D7P3E3    | 1.9 TB | F8AE00472664 | 235   | 0     | 0.64   |
| Samsung   | SSD 970 PRO        | 1 TB   | 7245A576FDDB | 235   | 0     | 0.64   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | D6FDDF479783 | 235   | 0     | 0.64   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 8CF164245AF5 | 235   | 0     | 0.64   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 6564393C2089 | 234   | 0     | 0.64   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | F6A3F7361D1B | 234   | 0     | 0.64   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | C91A2196017D | 234   | 0     | 0.64   |
| Dell      | Express Flash N... | 1.6 TB | 868B94D961F4 | 234   | 0     | 0.64   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 01E65361CB21 | 234   | 0     | 0.64   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 39D347B99125 | 234   | 0     | 0.64   |
| WDC       | WUS4BB038D7P3E3    | 3.8 TB | 83F13596B0CA | 234   | 0     | 0.64   |
| Toshiba   | KXD51RUE3T84       | 3.8 TB | 4F98C0FAAC2E | 234   | 0     | 0.64   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | 6A177A70AFAA | 234   | 0     | 0.64   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | FE187D28451F | 233   | 0     | 0.64   |
| Toshiba   | KXD51RUE1T92       | 1.9 TB | 0642CB6A1229 | 233   | 0     | 0.64   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 41B6928D2C7E | 233   | 0     | 0.64   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 4446DFDFF2C0 | 233   | 0     | 0.64   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 9C3CE3C1696D | 233   | 0     | 0.64   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 4BEE4840D7DA | 233   | 0     | 0.64   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 77AAEBD55754 | 233   | 0     | 0.64   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 4CFCAC58462A | 233   | 0     | 0.64   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 6B6ABE255D7A | 233   | 0     | 0.64   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | DC1B623B0900 | 233   | 0     | 0.64   |
| Samsung   | MZPLJ6T4HALA-00007 | 6.4 TB | 53ACE90FB9A4 | 233   | 0     | 0.64   |
| Samsung   | SSD 970 PRO        | 1 TB   | 71862E6D22FE | 233   | 0     | 0.64   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 636FAAB99F1E | 232   | 0     | 0.64   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 80FC6B0B24F3 | 232   | 0     | 0.64   |
| Samsung   | SSD 970 EVO Plus   | 1 TB   | B21DED5ABE99 | 232   | 0     | 0.64   |
| Samsung   | MZPLJ3T2HBJR-00007 | 3.2 TB | FC81A0091F5D | 232   | 0     | 0.64   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 2A6E172DAA85 | 232   | 0     | 0.64   |
| Samsung   | MZPLJ6T4HALA-00007 | 6.4 TB | 3611834D9BFB | 232   | 0     | 0.64   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 46435A5354CF | 232   | 0     | 0.64   |
| Intel     | SSDPED1K375GA      | 375 GB | 9DFEDABBE544 | 231   | 0     | 0.64   |
| Intel     | SSDPED1K375GA      | 375 GB | E68565183C22 | 231   | 0     | 0.64   |
| HP        | SSD EX950          | 2 TB   | 5E642651EAF6 | 231   | 0     | 0.63   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | E2A74B97FBA7 | 231   | 0     | 0.63   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 15CCB719C42A | 231   | 0     | 0.63   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | D820A0863299 | 231   | 0     | 0.63   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | C5CFC057FE32 | 231   | 0     | 0.63   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | E87C433CEAA7 | 231   | 0     | 0.63   |
| Samsung   | SSD 970 PRO        | 1 TB   | FD606A296A60 | 230   | 0     | 0.63   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 8349CC8F5B1F | 230   | 0     | 0.63   |
| Kingston  | SEDC1000M3840G     | 3.8 TB | 5914E69D9119 | 230   | 0     | 0.63   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 0F69C8D74BB6 | 230   | 0     | 0.63   |
| WDC       | WUS4BB038D7P3E3    | 3.8 TB | 61D5C59C041D | 230   | 0     | 0.63   |
| WDC       | WUS4BB038D7P3E3    | 3.8 TB | F185637FE118 | 230   | 0     | 0.63   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 18719DF1CB24 | 230   | 0     | 0.63   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 3395BEFF0F20 | 230   | 0     | 0.63   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 37BFC85E738B | 230   | 0     | 0.63   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 383557F9644F | 230   | 0     | 0.63   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 989F43DF72DC | 230   | 0     | 0.63   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | D572531E549B | 230   | 0     | 0.63   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | EEBAF6C5BF2C | 230   | 0     | 0.63   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | BB8A6BA7CB07 | 230   | 0     | 0.63   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 07A5ECE344BD | 230   | 0     | 0.63   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 45ED1E7A66BF | 230   | 0     | 0.63   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 65485B9C9AAA | 230   | 0     | 0.63   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 9F7AC7A62FD1 | 230   | 0     | 0.63   |
| WDC       | WUS4BB076D7P3E1    | 7.6 TB | 4E5181609E7C | 230   | 0     | 0.63   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 47D4B67041C1 | 229   | 0     | 0.63   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 4351809CD49E | 229   | 0     | 0.63   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 9FA3633824CB | 229   | 0     | 0.63   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 3226CC0F535C | 229   | 0     | 0.63   |
| Dell      | Express Flash N... | 1.6 TB | 5CDAB9AE907D | 229   | 0     | 0.63   |
| Intel     | SSDPEDKX040T7      | 4 TB   | 20CBF3DF584F | 916   | 3     | 0.63   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 79CF3AB002FD | 228   | 0     | 0.63   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | CE15F19B5150 | 228   | 0     | 0.63   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | EB221258EC38 | 228   | 0     | 0.63   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 222730842FBE | 228   | 0     | 0.63   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | 2FBE6500CA02 | 1143  | 4     | 0.63   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 9A469F836E08 | 228   | 0     | 0.63   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 328A3E8A86CA | 228   | 0     | 0.63   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 72BCBA292CBD | 228   | 0     | 0.63   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 11257CD75DF7 | 228   | 0     | 0.63   |
| WDC       | WUS4BB038D7P3E3    | 3.8 TB | AE3D3695873A | 227   | 0     | 0.62   |
| WDC       | WUS4BB038D7P3E3    | 3.8 TB | C31A247DB903 | 227   | 0     | 0.62   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 39226E45DA6D | 227   | 0     | 0.62   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | B7B7FA4528BC | 227   | 0     | 0.62   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | E9D55E00338A | 227   | 0     | 0.62   |
| Micron    | 9300_MTFDHAL3T2TDR | 3.2 TB | 0F093BC18D6D | 227   | 0     | 0.62   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 90F6F810FF10 | 227   | 0     | 0.62   |
| WDC       | CL SN720 SDAQNT... | 512 GB | 753BDE60E758 | 227   | 0     | 0.62   |
| Samsung   | SSD 970 EVO Plus   | 250 GB | 4384EE24FFF8 | 226   | 0     | 0.62   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 0836E65F5A0D | 226   | 0     | 0.62   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 95A2DE2B4244 | 226   | 0     | 0.62   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | B9B2CEAF5F8A | 226   | 0     | 0.62   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | BDD8C4603805 | 226   | 0     | 0.62   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | E7BD6C1EAC14 | 226   | 0     | 0.62   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | F41A2BE7AAE5 | 226   | 0     | 0.62   |
| Micron    | 2200_MTFDHBA256TCK | 256 GB | 87A64509A259 | 226   | 0     | 0.62   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 34E4A5DF202E | 226   | 0     | 0.62   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | BEDBED037F95 | 226   | 0     | 0.62   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | E3DFAF917AC0 | 226   | 0     | 0.62   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 120FEE1324C9 | 226   | 0     | 0.62   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | BCFB674CB025 | 226   | 0     | 0.62   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | E9BA74C4DDE1 | 226   | 0     | 0.62   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 3A1C23212646 | 226   | 0     | 0.62   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 8684B21D4CB0 | 226   | 0     | 0.62   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | CC0DD2014951 | 226   | 0     | 0.62   |
| Micron    | 2200_MTFDHBA512TCK | 512 GB | 57F204BCFD1C | 226   | 0     | 0.62   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 0030281DF78E | 226   | 0     | 0.62   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 096F9ABBB3AB | 226   | 0     | 0.62   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 1A7AE880E228 | 226   | 0     | 0.62   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 491A32E4BF0A | 226   | 0     | 0.62   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 5F6F930712F7 | 226   | 0     | 0.62   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | E5D4E7914B38 | 226   | 0     | 0.62   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | 62BB3CAE1BF2 | 226   | 0     | 0.62   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 36438D3F951E | 226   | 0     | 0.62   |
| Intel     | SSDPE2ME400G4      | 400 GB | 19438D4AA7E1 | 226   | 0     | 0.62   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | 57D7B6B3AB42 | 225   | 0     | 0.62   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 5D00E6F737C3 | 225   | 0     | 0.62   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 76382C91CC3A | 225   | 0     | 0.62   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | D1BB57873953 | 225   | 0     | 0.62   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 048C0C2AC1DB | 225   | 0     | 0.62   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 53A63C5EDE2E | 225   | 0     | 0.62   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 665E0CF1113A | 225   | 0     | 0.62   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 70F417EADB1F | 225   | 0     | 0.62   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 7870FAC82EB1 | 225   | 0     | 0.62   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 7A3CD370279C | 225   | 0     | 0.62   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | CC690654371E | 225   | 0     | 0.62   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | FAC72FAF3D1A | 225   | 0     | 0.62   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | FE85FE3E74E0 | 225   | 0     | 0.62   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | C6DEE07A37EE | 225   | 0     | 0.62   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 0AC92167311D | 225   | 0     | 0.62   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 2AD7ACB39CCB | 224   | 0     | 0.61   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 693497ED0733 | 224   | 0     | 0.61   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | AA2C2F12A999 | 224   | 0     | 0.61   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | AE9935AB7A38 | 224   | 0     | 0.61   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | D7C4BC45EB88 | 224   | 0     | 0.61   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 1516E7BED175 | 224   | 0     | 0.61   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | C7A184E45567 | 224   | 0     | 0.61   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 1B68E57B80B4 | 224   | 0     | 0.61   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | BB77F1BD960A | 224   | 0     | 0.61   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | C2188FA362F1 | 224   | 0     | 0.61   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | D73A9E8E267D | 224   | 0     | 0.61   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | E06E7C7BD502 | 224   | 0     | 0.61   |
| Samsung   | SSD 970 PRO        | 1 TB   | C56A536034B7 | 223   | 0     | 0.61   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 0A04DA1025D9 | 223   | 0     | 0.61   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | D747853DD4EF | 223   | 0     | 0.61   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | E58B5FC32C36 | 223   | 0     | 0.61   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | 731551F6A8C6 | 223   | 0     | 0.61   |
| Intel     | SSDPE2NV076T8      | 7.6 TB | 6C84CCF58AF9 | 223   | 0     | 0.61   |
| Intel     | SSDPE2NV076T8      | 7.6 TB | 830732B4F317 | 223   | 0     | 0.61   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 4530F38E0BE5 | 222   | 0     | 0.61   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 5BC442368743 | 222   | 0     | 0.61   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 60B30D1D9B1F | 222   | 0     | 0.61   |
| Intel     | SSDPEL1K100GA      | 100 GB | 81F163D47062 | 222   | 0     | 0.61   |
| Dell      | Express Flash N... | 1.6 TB | E5B76F93ED88 | 222   | 0     | 0.61   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 6E925EF1E2FB | 222   | 0     | 0.61   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 9F8B614F4753 | 222   | 0     | 0.61   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 048ED7086A13 | 222   | 0     | 0.61   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 12B907134747 | 222   | 0     | 0.61   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 12BFCF41CFFF | 222   | 0     | 0.61   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 3FDEF70D5C1F | 222   | 0     | 0.61   |
| Toshiba   | KXG60ZNV256G       | 256 GB | F67D0F02064A | 221   | 0     | 0.61   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 675FFB36EE34 | 221   | 0     | 0.61   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | F77B79E565A1 | 221   | 0     | 0.61   |
| Intel     | EO000375KWJUC      | 375 GB | AFC77C74A89E | 221   | 0     | 0.61   |
| Micron    | 2200_MTFDHBA256TCK | 256 GB | EB0ED6F34263 | 221   | 0     | 0.61   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | F598A9F54DF6 | 221   | 0     | 0.61   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | AAE452091275 | 221   | 0     | 0.61   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | C4F653AD921A | 221   | 0     | 0.61   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | E9F77E6079C9 | 221   | 0     | 0.61   |
| WDC       | WUS4BB038D7P3E1    | 3.8 TB | 3DC9F8C9B908 | 221   | 0     | 0.61   |
| WDC       | WUS4BB038D7P3E1    | 3.8 TB | F2E745470DF9 | 221   | 0     | 0.61   |
| Wester... | WUS4BB038D7P3E1    | 3.8 TB | 3DC9F8C9B908 | 221   | 0     | 0.61   |
| Wester... | WUS4BB038D7P3E1    | 3.8 TB | F2E745470DF9 | 221   | 0     | 0.61   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 2D1457CED687 | 221   | 0     | 0.61   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 96C119713627 | 221   | 0     | 0.61   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 9CC590A1E4B0 | 221   | 0     | 0.61   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 1238A14356FF | 221   | 0     | 0.61   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 777591FBB125 | 221   | 0     | 0.61   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 787753ACC7BD | 221   | 0     | 0.61   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | FC3F35BCA5C0 | 221   | 0     | 0.61   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 1238A14356FF | 221   | 0     | 0.61   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 44FC2A4664D1 | 221   | 0     | 0.61   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 777591FBB125 | 221   | 0     | 0.61   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 787753ACC7BD | 221   | 0     | 0.61   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 7F02C8F88C2E | 221   | 0     | 0.61   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | B09C917C6428 | 221   | 0     | 0.61   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | D36B49B90449 | 221   | 0     | 0.61   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | FC3F35BCA5C0 | 221   | 0     | 0.61   |
| Samsung   | MZPJB480HMGC-0BW07 | 480 GB | 5CB78740C9BB | 221   | 0     | 0.61   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | 4497059BDA4C | 220   | 0     | 0.61   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 042338AC7134 | 220   | 0     | 0.61   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 4D9650971BD1 | 220   | 0     | 0.61   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | A02573311598 | 220   | 0     | 0.61   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | E803CC9C47A6 | 220   | 0     | 0.61   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | 29EDB57282E2 | 220   | 0     | 0.60   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 08D0B831543C | 220   | 0     | 0.60   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 6A34008A8769 | 220   | 0     | 0.60   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | A5FA67997EBE | 220   | 0     | 0.60   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | D069FC5234FE | 220   | 0     | 0.60   |
| Samsung   | MZPLJ6T4HALA-00007 | 6.4 TB | A83B0F840B7A | 220   | 0     | 0.60   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 116511CD808B | 220   | 0     | 0.60   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 2FC4908A7267 | 220   | 0     | 0.60   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 38B8876903D0 | 220   | 0     | 0.60   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 513CBC096F70 | 220   | 0     | 0.60   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 5B1E9EBAF3A2 | 220   | 0     | 0.60   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 6B799681D88F | 220   | 0     | 0.60   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | BAC54F0D8927 | 220   | 0     | 0.60   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | D47E0528C6B4 | 220   | 0     | 0.60   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | A45C213786DA | 220   | 0     | 0.60   |
| Kingston  | SKC2500M8250G      | 250 GB | E3DF3D3E8805 | 220   | 0     | 0.60   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 08D5C342AEFA | 220   | 0     | 0.60   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 2C1B47EB5433 | 220   | 0     | 0.60   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | E87E09439861 | 220   | 0     | 0.60   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | ED9EACD40186 | 220   | 0     | 0.60   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 53D54329E159 | 219   | 0     | 0.60   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 971C34AC29F1 | 219   | 0     | 0.60   |
| Intel     | SSDPE2ME400G4      | 400 GB | 4DF8D7E9D2BC | 219   | 0     | 0.60   |
| Toshiba   | KXD51RUE1T92       | 1.9 TB | 51E98776C865 | 219   | 0     | 0.60   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 33BD56EBAE51 | 219   | 0     | 0.60   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 6094A16817F7 | 218   | 0     | 0.60   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 689B12BBCA36 | 218   | 0     | 0.60   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | D0DB15D1FB1B | 218   | 0     | 0.60   |
| Samsung   | MZPJB480HMGC-0BW07 | 480 GB | FDCB8F5B2D92 | 218   | 0     | 0.60   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | B8A0F9E957C9 | 218   | 0     | 0.60   |
| Toshiba   | KXG60PNV2T04       | 2 TB   | 9F0ECAFF50F4 | 218   | 0     | 0.60   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | E952D742B569 | 218   | 0     | 0.60   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 029218B1F466 | 218   | 0     | 0.60   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 03A3AE01ECB7 | 218   | 0     | 0.60   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 6235CB9CFD65 | 218   | 0     | 0.60   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | B4ADE2746552 | 218   | 0     | 0.60   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | C1D0944C783B | 218   | 0     | 0.60   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | DE644285BB6B | 218   | 0     | 0.60   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | F4DBE89AC6CB | 218   | 0     | 0.60   |
| WDC       | CL SN720 SDAQNT... | 512 GB | B1EA3E0B1D51 | 218   | 0     | 0.60   |
| Crucial   | CT500P1SSD8        | 500 GB | 1AA37AE5B55C | 217   | 0     | 0.60   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | 63EBD7BF716D | 217   | 0     | 0.60   |
| Intel     | SSDPED1D480GA      | 480 GB | 428853D43532 | 217   | 0     | 0.60   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 1DE9808AA91C | 217   | 0     | 0.60   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 267526C188A6 | 217   | 0     | 0.60   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | EA01462CE39D | 217   | 0     | 0.60   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 9A75C02FC323 | 217   | 0     | 0.60   |
| Toshiba   | KXD51RUE1T92       | 1.9 TB | 719B0D670081 | 217   | 0     | 0.60   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 0ACF690D08DD | 217   | 0     | 0.60   |
| Toshiba   | KXG60ZNV256G       | 256 GB | AE3A4CE7F334 | 217   | 0     | 0.60   |
| WDC       | CL SN720 SDAQNT... | 512 GB | 9586DD3D8657 | 217   | 0     | 0.59   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | 140D09820700 | 217   | 0     | 0.59   |
| Samsung   | MZPLJ6T4HALA-00007 | 6.4 TB | 71DA85C3640B | 217   | 0     | 0.59   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | C0092D546979 | 217   | 0     | 0.59   |
| Samsung   | SSD 970 EVO Plus   | 1 TB   | BBE8D511DB2B | 217   | 0     | 0.59   |
| Wester... | WUS4BB038D7P3E3    | 3.8 TB | F185637FE118 | 217   | 0     | 0.59   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | AD8D2576B3F9 | 216   | 0     | 0.59   |
| Dell      | Express Flash N... | 1.6 TB | 2428D6B6BB35 | 216   | 0     | 0.59   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 499FA4E8B6C8 | 216   | 0     | 0.59   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | ACD8A073B3BA | 216   | 0     | 0.59   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | D4522BBA19FA | 216   | 0     | 0.59   |
| Wester... | WUS4BB038D7P3E1    | 3.8 TB | 421DAAB41807 | 216   | 0     | 0.59   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 03DBBF6970AD | 216   | 0     | 0.59   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 0E8C10DDC17A | 216   | 0     | 0.59   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 93015F0DC8B8 | 215   | 0     | 0.59   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 20B71EFBAABF | 215   | 0     | 0.59   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 28F1CA6021B4 | 215   | 0     | 0.59   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | EE9A65813269 | 215   | 0     | 0.59   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 6E5941FEB4BF | 215   | 0     | 0.59   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | A99383253CF1 | 215   | 0     | 0.59   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | C733B21B490A | 215   | 0     | 0.59   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | D7B9110B7A69 | 215   | 0     | 0.59   |
| WDC       | CL SN720 SDAQNT... | 512 GB | 05039F08419C | 214   | 0     | 0.59   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 8F81C8374FB0 | 214   | 0     | 0.59   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 8F81C8374FB0 | 214   | 0     | 0.59   |
| WDC       | CL SN720 SDAQNT... | 512 GB | 0272408EF6AB | 214   | 0     | 0.59   |
| WDC       | WUS4BB038D7P3E1    | 3.8 TB | 1B6AD60A03F2 | 214   | 0     | 0.59   |
| WDC       | WUS4BB038D7P3E1    | 3.8 TB | 2F976807EE11 | 214   | 0     | 0.59   |
| WDC       | WUS4BB038D7P3E1    | 3.8 TB | 79B30584708E | 214   | 0     | 0.59   |
| WDC       | WUS4BB038D7P3E1    | 3.8 TB | ACE99FA0D987 | 214   | 0     | 0.59   |
| WDC       | WUS4BB038D7P3E1    | 3.8 TB | B9210CFB84B9 | 214   | 0     | 0.59   |
| WDC       | WUS4BB038D7P3E1    | 3.8 TB | FA0EC141EA7D | 214   | 0     | 0.59   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | D908AB1D9BC3 | 214   | 0     | 0.59   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 47681E4937C9 | 214   | 0     | 0.59   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 66E45723EBB6 | 214   | 0     | 0.59   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | 1862ACE408A3 | 213   | 0     | 0.59   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 3864FB0691AE | 213   | 0     | 0.59   |
| Samsung   | SSD 970 EVO Plus   | 500 GB | B9A69B88C1B9 | 213   | 0     | 0.59   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 9DFC148B98D8 | 213   | 0     | 0.59   |
| Intel     | SSDPED1K375GA      | 375 GB | E04BB5D04852 | 213   | 0     | 0.59   |
| Intel     | SSDPEKNW010T8      | 1 TB   | F08D9DDB5E94 | 213   | 0     | 0.58   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | ED6F9F2A153D | 213   | 0     | 0.58   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | DD49E16628BB | 212   | 0     | 0.58   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 0C36CCA1279F | 212   | 0     | 0.58   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | CB6282FE5807 | 212   | 0     | 0.58   |
| Wester... | WUS4BB038D7P3E3    | 3.8 TB | 5892C9E060F9 | 212   | 0     | 0.58   |
| Wester... | WUS4BB038D7P3E3    | 3.8 TB | FB6EB597D0F5 | 212   | 0     | 0.58   |
| Samsung   | MZ1LB3T8HMLA-00007 | 3.8 TB | E7C6FA4C3847 | 212   | 0     | 0.58   |
| Samsung   | SSD 970 PRO        | 512 GB | 4F1618464CF0 | 211   | 0     | 0.58   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 1ACE21FBEB19 | 211   | 0     | 0.58   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 2C754690E106 | 211   | 0     | 0.58   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 32AA9F771A86 | 211   | 0     | 0.58   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 4B521A47308D | 211   | 0     | 0.58   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | A54DC18C92A0 | 211   | 0     | 0.58   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | D8486F771051 | 211   | 0     | 0.58   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | F37FF1387F79 | 211   | 0     | 0.58   |
| Micron    | 2200_MTFDHBA512TCK | 512 GB | 2D8F5A9A7501 | 211   | 0     | 0.58   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 309ADF7C54D5 | 211   | 0     | 0.58   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | BB643535DF3F | 211   | 0     | 0.58   |
| Samsung   | MZQLW1T9HMJP-000AZ | 1.9 TB | 4D7409C5DD0C | 422   | 1     | 0.58   |
| HGST      | HUSMR7638BDP3Y1    | 3.8 TB | 42ADDC0D242A | 211   | 0     | 0.58   |
| WDC       | CL SN720 SDAQNT... | 512 GB | EEFA642AEADF | 211   | 0     | 0.58   |
| WDC       | WUS4BB038D7P3E1    | 3.8 TB | 5047C1E5D366 | 211   | 0     | 0.58   |
| WDC       | WUS4BB038D7P3E1    | 3.8 TB | 790A5C88F673 | 211   | 0     | 0.58   |
| Samsung   | SSD 970 EVO Plus   | 1 TB   | C5D58A0D8275 | 211   | 0     | 0.58   |
| Samsung   | SSD 970 PRO        | 512 GB | EDFDA08EEF77 | 210   | 0     | 0.58   |
| WDC       | WUS4BB038D7P3E1    | 3.8 TB | CCD2FE45E8AD | 210   | 0     | 0.58   |
| Wester... | WUS4BB038D7P3E1    | 3.8 TB | CCD2FE45E8AD | 210   | 0     | 0.58   |
| Intel     | SSDPED1K375GA      | 375 GB | EEA28D7A637E | 210   | 0     | 0.58   |
| Dell      | Express Flash N... | 1.6 TB | 9787EA5D65A3 | 210   | 0     | 0.58   |
| Dell      | Express Flash N... | 1.6 TB | F7AC5B23D256 | 210   | 0     | 0.58   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | F1743FBA8282 | 210   | 0     | 0.58   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | B5597A5CFD9E | 210   | 0     | 0.58   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 93DB387C280F | 209   | 0     | 0.57   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | C9DA5A54574B | 209   | 0     | 0.57   |
| Intel     | SSDPE2KX010T8      | 1 TB   | 9527022F139E | 209   | 0     | 0.57   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 0F64EA3A9A53 | 209   | 0     | 0.57   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | E15F9FD42E0A | 209   | 0     | 0.57   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 7A6208F687E6 | 209   | 0     | 0.57   |
| Intel     | SSDPED1K375GA      | 375 GB | C4DDC69DE7EF | 209   | 0     | 0.57   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 78C54602A914 | 209   | 0     | 0.57   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 7604EAF00012 | 209   | 0     | 0.57   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | ACB20218CD05 | 209   | 0     | 0.57   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | A8860E1E6EFC | 209   | 0     | 0.57   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | BAAB8814964D | 208   | 0     | 0.57   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | EAF0079A3975 | 208   | 0     | 0.57   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | CBB8775AAE5D | 208   | 0     | 0.57   |
| Wester... | WUS4BB038D7P3E3    | 3.8 TB | AE3D3695873A | 208   | 0     | 0.57   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 793F03D5BF59 | 208   | 0     | 0.57   |
| WDC       | WUS4BB038D7P3E1    | 3.8 TB | 1C5A8C93AB06 | 208   | 0     | 0.57   |
| WDC       | WUS4BB038D7P3E1    | 3.8 TB | 7F06A4BADF4B | 208   | 0     | 0.57   |
| WDC       | WUS4BB038D7P3E1    | 3.8 TB | CAA6D351E3C0 | 208   | 0     | 0.57   |
| WDC       | WUS4BB038D7P3E1    | 3.8 TB | EE4DCF60C596 | 208   | 0     | 0.57   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 286F35219D25 | 208   | 0     | 0.57   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 4EE72B1D0770 | 208   | 0     | 0.57   |
| Micron    | 9300_MTFDHAL7T6TDP | 7.6 TB | 20FB949028C7 | 208   | 0     | 0.57   |
| Micron    | 9300_MTFDHAL7T6TDP | 7.6 TB | C43E048B3346 | 208   | 0     | 0.57   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | AF8EAA0832B2 | 207   | 0     | 0.57   |
| Wester... | WUS4BB038D7P3E1    | 3.8 TB | 99ACC4AB8CCF | 207   | 0     | 0.57   |
| Dell      | Express Flash C... | 960 GB | 76DCEA269312 | 207   | 0     | 0.57   |
| Dell      | Express Flash C... | 960 GB | 89CA3A779258 | 207   | 0     | 0.57   |
| Dell      | Express Flash C... | 960 GB | C1D51F286CB4 | 207   | 0     | 0.57   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | 632CC6E96DCD | 207   | 0     | 0.57   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | E83968622B99 | 207   | 0     | 0.57   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 35A6841CF443 | 207   | 0     | 0.57   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | DE3F0B977AAE | 207   | 0     | 0.57   |
| Toshiba   | KXG60ZNV1T02       | 1 TB   | 92F511F1293F | 206   | 0     | 0.57   |
| WDC       | WUS4BB038D7P3E1    | 3.8 TB | 27C05E35819E | 206   | 0     | 0.57   |
| WDC       | WUS4BB038D7P3E1    | 3.8 TB | 321AF59AEBFE | 206   | 0     | 0.57   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | D27D2ACA4F7E | 206   | 0     | 0.57   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | D4CB5AADD9DD | 206   | 0     | 0.57   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | D27D2ACA4F7E | 206   | 0     | 0.57   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | D4CB5AADD9DD | 206   | 0     | 0.57   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 879CBE21931F | 206   | 0     | 0.57   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 292152A17CE8 | 206   | 0     | 0.57   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 2D29E2980363 | 206   | 0     | 0.57   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 84FDFA004198 | 206   | 0     | 0.57   |
| WDC       | WUS4BB076D7P3E1    | 7.6 TB | 6F546BE0AC37 | 206   | 0     | 0.57   |
| WDC       | WUS4BB076D7P3E1    | 7.6 TB | FDB17160CAFF | 206   | 0     | 0.57   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | C04987287B69 | 206   | 0     | 0.57   |
| Dell      | Express Flash P... | 1.6 TB | 310A0FEEDC55 | 206   | 0     | 0.57   |
| Dell      | Express Flash P... | 1.6 TB | 0BDFB295FE71 | 206   | 0     | 0.57   |
| Dell      | Express Flash P... | 1.6 TB | 2B2EB60DD031 | 206   | 0     | 0.57   |
| Dell      | Express Flash P... | 1.6 TB | 8AAD1535C6EC | 206   | 0     | 0.57   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 7F2D94D03034 | 206   | 0     | 0.57   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | A267E1B8F72F | 206   | 0     | 0.57   |
| Dell      | Express Flash P... | 1.6 TB | 07235FA1AA9C | 206   | 0     | 0.57   |
| Dell      | Express Flash P... | 1.6 TB | 9D1183979C9A | 206   | 0     | 0.57   |
| Dell      | Express Flash P... | 1.6 TB | AD26E5CD07A1 | 206   | 0     | 0.57   |
| Dell      | Express Flash P... | 1.6 TB | E6091FCA5EE2 | 206   | 0     | 0.57   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 63FA41C19D85 | 206   | 0     | 0.56   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | FBACABEB8A43 | 206   | 0     | 0.56   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 63FA41C19D85 | 206   | 0     | 0.56   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | FBACABEB8A43 | 206   | 0     | 0.56   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 5953E11026C9 | 206   | 0     | 0.56   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | F130D8410DF7 | 205   | 0     | 0.56   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 7D9BB143E8F8 | 205   | 0     | 0.56   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | B0AEBEED647B | 205   | 0     | 0.56   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 24604F77A207 | 205   | 0     | 0.56   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 7581C528FF5D | 205   | 0     | 0.56   |
| Samsung   | MZSLW1T0HMLH-000L1 | 1 TB   | A90D91DC0A90 | 205   | 0     | 0.56   |
| Seagate   | FireCuda 520 SS... | 500 GB | 2FE6F2481A83 | 204   | 0     | 0.56   |
| Wester... | WUS4BB038D7P3E3    | 3.8 TB | F367032F53F6 | 204   | 0     | 0.56   |
| Samsung   | SSD 970 PRO        | 1 TB   | 7BF62ABDBAC8 | 204   | 0     | 0.56   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 14FCBE9F523C | 203   | 0     | 0.56   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 0A6B41EB859B | 203   | 0     | 0.56   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | FB1CE00C006C | 203   | 0     | 0.56   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 50BC11F84600 | 203   | 0     | 0.56   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 5529F50419FC | 203   | 0     | 0.56   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 55C7DF1F4FD3 | 203   | 0     | 0.56   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 021F55CCF94F | 203   | 0     | 0.56   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 7BC2903CA150 | 203   | 0     | 0.56   |
| Samsung   | MZVLB1T0HBLR-00000 | 1 TB   | 5E74F62B6FD1 | 203   | 0     | 0.56   |
| Samsung   | SSD 970 EVO Plus   | 1 TB   | 507D86EC263B | 203   | 0     | 0.56   |
| Samsung   | MZWLJ7T6HALA-00007 | 7.6 TB | 9CCF589CADD2 | 203   | 0     | 0.56   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 1FC2B40FE31A | 203   | 0     | 0.56   |
| Wester... | WUS4BB038D7P3E1    | 3.8 TB | 8960883D703E | 203   | 0     | 0.56   |
| Toshiba   | KXG60PNV2T04       | 2 TB   | B71A29C2A638 | 203   | 0     | 0.56   |
| Samsung   | MZVLB1T0HBLR-00000 | 1 TB   | 82879CD1EE41 | 202   | 0     | 0.56   |
| Samsung   | MZVLB512HBJQ-00000 | 512 GB | 1A4D9AC0B158 | 202   | 0     | 0.56   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 2CB516666CCB | 202   | 0     | 0.55   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 692EA4C761F9 | 202   | 0     | 0.55   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | A88E30338303 | 202   | 0     | 0.55   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | CBB3D70A1944 | 202   | 0     | 0.55   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | D4DBF5EE1BC0 | 202   | 0     | 0.55   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | E5141D6D4716 | 202   | 0     | 0.55   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | BB8F385E7C40 | 202   | 0     | 0.55   |
| Samsung   | SSD 970 PRO        | 512 GB | B581906DBAA0 | 202   | 0     | 0.55   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | B9D2B817E5DC | 202   | 0     | 0.55   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | D9AF608BAE43 | 202   | 0     | 0.55   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 5EAAE21B531F | 202   | 0     | 0.55   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | CF4CB666C2D8 | 202   | 0     | 0.55   |
| WDC       | WUS4BB038D7P3E1    | 3.8 TB | 4E671E3F2956 | 201   | 0     | 0.55   |
| WDC       | WUS4BB038D7P3E1    | 3.8 TB | 8007868DED14 | 201   | 0     | 0.55   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 3B2F34018A31 | 201   | 0     | 0.55   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | E0B13009EF77 | 201   | 0     | 0.55   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 544410E43956 | 201   | 0     | 0.55   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | C277168EA530 | 201   | 0     | 0.55   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 1A011BE07AB2 | 201   | 0     | 0.55   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 27BF42A5ED4B | 201   | 0     | 0.55   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 393D5EB206C7 | 201   | 0     | 0.55   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 43EC86AEE21B | 201   | 0     | 0.55   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | B7C31CA3F236 | 201   | 0     | 0.55   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | C3C32CED9CB8 | 201   | 0     | 0.55   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | E27B0597BD24 | 201   | 0     | 0.55   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | EB381FD7E4C7 | 201   | 0     | 0.55   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | F5FC077A02A9 | 201   | 0     | 0.55   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 490AEFAF3B54 | 201   | 0     | 0.55   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | AA15589C64D3 | 201   | 0     | 0.55   |
| Samsung   | MZPLL3T2HAJQ-00005 | 3.2 TB | E7B02BD460DA | 201   | 0     | 0.55   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 7D1DF9363340 | 201   | 0     | 0.55   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | DC879367918E | 201   | 0     | 0.55   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 7D1DF9363340 | 201   | 0     | 0.55   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 90642055B4DC | 201   | 0     | 0.55   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | DC879367918E | 201   | 0     | 0.55   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 3954B1CDEDA9 | 201   | 0     | 0.55   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | ED6A61CF1CF4 | 201   | 0     | 0.55   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 3954B1CDEDA9 | 201   | 0     | 0.55   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | ED6A61CF1CF4 | 201   | 0     | 0.55   |
| WDC       | WUS4BB038D7P3E1    | 3.8 TB | 34BCA8546216 | 201   | 0     | 0.55   |
| WDC       | WUS4BB038D7P3E1    | 3.8 TB | 358E7C6E43F4 | 201   | 0     | 0.55   |
| WDC       | CL SN720 SDAQNT... | 512 GB | 2E28BAA0EAAE | 201   | 0     | 0.55   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 07D53C57E54D | 201   | 0     | 0.55   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 71A153B8D899 | 201   | 0     | 0.55   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 71A153B8D899 | 201   | 0     | 0.55   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | D9A0517CEE14 | 201   | 0     | 0.55   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | D54B6BE7BB92 | 201   | 0     | 0.55   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | D54B6BE7BB92 | 201   | 0     | 0.55   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | E15F9FD42E0A | 201   | 0     | 0.55   |
| Dell      | Express Flash C... | 960 GB | 33E4006FA3B7 | 201   | 0     | 0.55   |
| Dell      | Express Flash C... | 960 GB | CCC1A505A268 | 201   | 0     | 0.55   |
| Samsung   | MZVLB512HBJQ-00000 | 512 GB | 85D4EB74BE3C | 200   | 0     | 0.55   |
| Intel     | SSDPE2MX450G7      | 450 GB | 20010E0533C2 | 200   | 0     | 0.55   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | AF346B0705F9 | 200   | 0     | 0.55   |
| WDC       | WDS500G3XHC-00SJG0 | 500 GB | 6B16D32C46F3 | 200   | 0     | 0.55   |
| WDC       | WUS4BB038D7P3E1    | 3.8 TB | 46A7A2B47EA5 | 200   | 0     | 0.55   |
| WDC       | WUS4BB038D7P3E1    | 3.8 TB | 895018F49C23 | 200   | 0     | 0.55   |
| WDC       | WUS4BB038D7P3E1    | 3.8 TB | CB8E6A36656F | 200   | 0     | 0.55   |
| WDC       | WUS4BB038D7P3E1    | 3.8 TB | E80ED46E91AA | 200   | 0     | 0.55   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | C0AA66A1AA14 | 200   | 0     | 0.55   |
| HP        | SSD EX950          | 2 TB   | A429417D3475 | 200   | 0     | 0.55   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 168B436E5445 | 200   | 0     | 0.55   |
| WDC       | CL SN720 SDAQNT... | 512 GB | 09C66C3F3B9F | 200   | 0     | 0.55   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 7432FEB7269A | 199   | 0     | 0.55   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | FD955F1C789C | 199   | 0     | 0.55   |
| Samsung   | SSD 970 PRO        | 512 GB | E9A106B9A414 | 199   | 0     | 0.55   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 1524385016CE | 199   | 0     | 0.55   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 49F6A0799F4E | 199   | 0     | 0.55   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 8AA837164BDA | 199   | 0     | 0.55   |
| Samsung   | MZPJB960HMGC-0BW07 | 960 GB | 00F82A211E46 | 199   | 0     | 0.55   |
| Intel     | SSDPE2KX010T7      | 1 TB   | E6261FFF9237 | 198   | 0     | 0.55   |
| Dell      | Express Flash C... | 960 GB | AD9957161045 | 198   | 0     | 0.54   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 6F143D204B7A | 198   | 0     | 0.54   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | DE41CAD9DDB2 | 198   | 0     | 0.54   |
| Gigabyte  | GP-GSM2NE3100TNTD  | 1 TB   | 5D606D1FB250 | 198   | 0     | 0.54   |
| WDC       | WUS4BB076D7P3E1    | 7.6 TB | 4A6B953EBA6F | 198   | 0     | 0.54   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 509BBECD05C4 | 197   | 0     | 0.54   |
| Gigabyte  | GP-GSM2NE3100TNTD  | 1 TB   | 10E9F7CA8C03 | 197   | 0     | 0.54   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 83BD6B2D66D9 | 197   | 0     | 0.54   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 1BC872BD0248 | 197   | 0     | 0.54   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | DB1D4A612C4A | 197   | 0     | 0.54   |
| Patriot   | P300               | 256 GB | 5D48442403B8 | 197   | 0     | 0.54   |
| Samsung   | SSD 970 EVO Plus   | 250 GB | 3F4E8F139D2F | 197   | 0     | 0.54   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 7818EFF980B1 | 196   | 0     | 0.54   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | F81F735235FE | 196   | 0     | 0.54   |
| Patriot   | P300               | 256 GB | 6C8E510A9622 | 196   | 0     | 0.54   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 05E608D624AD | 196   | 0     | 0.54   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | B77C6BA8414F | 196   | 0     | 0.54   |
| WDC       | WDS100T3X0C-00SJG0 | 1 TB   | 0AF76D10D81A | 196   | 0     | 0.54   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 75B02F5739D3 | 196   | 0     | 0.54   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | 027426393810 | 196   | 0     | 0.54   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | CE1DB06CC537 | 196   | 0     | 0.54   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 1A6B8FCE57C2 | 196   | 0     | 0.54   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 49E8EBECD9A3 | 196   | 0     | 0.54   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 9B36EF0F5A34 | 196   | 0     | 0.54   |
| Kingston  | SA2000M8500G       | 500 GB | 9D501BD9046E | 195   | 0     | 0.54   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | CD3E7D2EB903 | 195   | 0     | 0.54   |
| Samsung   | MZPJB960HMGC-0BW07 | 960 GB | 885B6B89F33C | 195   | 0     | 0.54   |
| Toshiba   | KXD51RUE1T92       | 1.9 TB | 6FEAB407D7AD | 195   | 0     | 0.54   |
| Wester... | WUS4BB038D7P3E1    | 3.8 TB | F5E29E0EAD61 | 195   | 0     | 0.54   |
| Dell      | Express Flash C... | 960 GB | 5114ED6A2697 | 195   | 0     | 0.54   |
| Samsung   | MZVLQ512HALU-00000 | 512 GB | E50A763CA0DF | 195   | 0     | 0.54   |
| HP        | SSD EX950          | 512 GB | C4E676354F43 | 195   | 0     | 0.54   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | A5DC5FEA7420 | 195   | 0     | 0.53   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 4766FF003BC6 | 195   | 0     | 0.53   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 9A53A7177277 | 195   | 0     | 0.53   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 9B9BCCAEA6E2 | 195   | 0     | 0.53   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 5D45FB9D249C | 195   | 0     | 0.53   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 5DFD004543D7 | 194   | 0     | 0.53   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 0A48425B17AF | 194   | 0     | 0.53   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | DD4B52B39B3A | 194   | 0     | 0.53   |
| Samsung   | SSD 970 PRO        | 1 TB   | F8CB1F3BBE56 | 194   | 0     | 0.53   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 739A3624B1E1 | 194   | 0     | 0.53   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | EF0DEE465873 | 194   | 0     | 0.53   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 81C3D451FA15 | 193   | 0     | 0.53   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | DDB11D6D1D78 | 193   | 0     | 0.53   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | C9191A767798 | 193   | 0     | 0.53   |
| Samsung   | MZ1LB3T8HMLA-00007 | 3.8 TB | 1F5623294DD2 | 193   | 0     | 0.53   |
| Samsung   | MZ1LB3T8HMLA-00007 | 3.8 TB | A5A119B7D60A | 193   | 0     | 0.53   |
| Seagate   | FireCuda 520 SS... | 500 GB | A04CE67CF8B7 | 193   | 0     | 0.53   |
| Samsung   | MZPJB960HMGC-0BW07 | 960 GB | 01D4384DA290 | 193   | 0     | 0.53   |
| SK hynix  | PC601 NVMe         | 256 GB | F50C0CDE82D7 | 192   | 0     | 0.53   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 4D56803959D9 | 192   | 0     | 0.53   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 3E9E2186B962 | 192   | 0     | 0.53   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 4EE0AC490A7D | 192   | 0     | 0.53   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | BB8093629CC9 | 192   | 0     | 0.53   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 2EC54A0CA123 | 192   | 0     | 0.53   |
| WDC       | WUS4BB038D7P3E1    | 3.8 TB | 1498C3712BAE | 192   | 0     | 0.53   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 2B98B4627E71 | 191   | 0     | 0.53   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 0DE52BC6413A | 191   | 0     | 0.53   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 124806F43F10 | 191   | 0     | 0.53   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | B6DAC663D81B | 191   | 0     | 0.53   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | E32CFC0F7E9F | 191   | 0     | 0.53   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 04818E61BF6C | 191   | 0     | 0.53   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 26E207B725C4 | 191   | 0     | 0.53   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 48B67E5112DF | 191   | 0     | 0.53   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | F5E1C5F8924A | 191   | 0     | 0.53   |
| Samsung   | SSD 970 EVO Plus   | 500 GB | 434FFDDFA698 | 191   | 0     | 0.52   |
| Micron    | 9300_MTFDHAL3T2TDR | 3.2 TB | 88F519C3A724 | 191   | 0     | 0.52   |
| Samsung   | MZVLW256HEHP-000H1 | 256 GB | 7981654A5941 | 191   | 0     | 0.52   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 43E9CD3BA4D8 | 191   | 0     | 0.52   |
| Samsung   | SSD 970 PRO        | 512 GB | 53144C5567CD | 191   | 0     | 0.52   |
| Samsung   | MZPJB480HMGC-0BW07 | 480 GB | D6C38EEC6BE1 | 191   | 0     | 0.52   |
| Micron    | 2200_MTFDHBA256TCK | 256 GB | 5D2D67ADE2C4 | 191   | 0     | 0.52   |
| WDC       | WUS4BB038D7P3E1    | 3.8 TB | 063FF7E4B88D | 190   | 0     | 0.52   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 36A63262EBEE | 190   | 0     | 0.52   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 2DFD42D7E81C | 190   | 0     | 0.52   |
| Samsung   | SSD 970 PRO        | 1 TB   | 984317E7FBC3 | 190   | 0     | 0.52   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 771CF39C97D2 | 190   | 0     | 0.52   |
| Toshiba   | KXD51RUE1T92       | 1.9 TB | 19590F7ECFA0 | 190   | 0     | 0.52   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 4966539B2924 | 190   | 0     | 0.52   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | CAA932DCFAAE | 190   | 0     | 0.52   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 1494E6829A09 | 189   | 0     | 0.52   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 56BF46060789 | 189   | 0     | 0.52   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 0F64EA3A9A53 | 189   | 0     | 0.52   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | B40703A64183 | 189   | 0     | 0.52   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | FBCB6B495609 | 189   | 0     | 0.52   |
| Wester... | WUS4BB038D7P3E1    | 3.8 TB | 3DBDE9A9EE86 | 189   | 0     | 0.52   |
| Wester... | WUS4BB038D7P3E1    | 3.8 TB | 9A053E19F66E | 189   | 0     | 0.52   |
| Dell      | Express Flash C... | 960 GB | 96AC5A27FB35 | 189   | 0     | 0.52   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 509A6E17391F | 188   | 0     | 0.52   |
| Samsung   | SSD 970 EVO        | 250 GB | E212609AA1D3 | 188   | 0     | 0.52   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 5ADE1D9C9040 | 188   | 0     | 0.52   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | B3DD2E1D8914 | 188   | 0     | 0.52   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | F9EC89E6BF5A | 188   | 0     | 0.52   |
| Samsung   | SSD 970 EVO Plus   | 250 GB | C2F2279BAF0A | 188   | 0     | 0.52   |
| WDC       | CL SN720 SDAQNT... | 512 GB | 43622E5805EA | 188   | 0     | 0.52   |
| Dell      | Express Flash C... | 960 GB | CA47F95B8285 | 188   | 0     | 0.52   |
| WDC       | CL SN720 SDAQNT... | 512 GB | DC9BB26FC391 | 187   | 0     | 0.51   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 218538ABB067 | 187   | 0     | 0.51   |
| Dell      | Express Flash C... | 960 GB | 2B2015FBB0A3 | 187   | 0     | 0.51   |
| WDC       | WUS4CB016D7P3E3    | 1.6 TB | C394BBC7647B | 187   | 0     | 0.51   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 8132E4689B8B | 187   | 0     | 0.51   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | CA90AA57D0A4 | 187   | 0     | 0.51   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | C0D890EE9903 | 187   | 0     | 0.51   |
| Samsung   | SSD 970 EVO Plus   | 1 TB   | B94A360C26F2 | 187   | 0     | 0.51   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | A82F75BF62A0 | 186   | 0     | 0.51   |
| Samsung   | SSD 970 EVO Plus   | 1 TB   | 788BAA94DAEE | 186   | 0     | 0.51   |
| WDC       | WUS4BB038D7P3E1    | 3.8 TB | A418C4F28FB1 | 186   | 0     | 0.51   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 7085B3B0003B | 186   | 0     | 0.51   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 77F6A18E01E6 | 186   | 0     | 0.51   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 923DFF20B039 | 186   | 0     | 0.51   |
| Samsung   | SSD 970 PRO        | 1 TB   | 1F7523D68046 | 186   | 0     | 0.51   |
| Toshiba   | KXG60ZNV1T02       | 1 TB   | E641357C0DAB | 186   | 0     | 0.51   |
| WDC       | WUS4BB038D7P3E1    | 3.8 TB | 67E1FE010D09 | 186   | 0     | 0.51   |
| Samsung   | SSD 970 EVO Plus   | 250 GB | 7ECB61B92D5A | 186   | 0     | 0.51   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 3A1F110BD4EF | 186   | 0     | 0.51   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 3A1F110BD4EF | 186   | 0     | 0.51   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 59637CD9FCE3 | 186   | 0     | 0.51   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 7B3FAB701279 | 186   | 0     | 0.51   |
| Kingston  | SKC2500M81000G     | 1 TB   | 35B6420FDEB1 | 186   | 0     | 0.51   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 369974DB3BF4 | 186   | 0     | 0.51   |
| Intel     | SSDPE2KX010T8      | 1 TB   | 9069407BF82B | 185   | 0     | 0.51   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 98B12467B7A1 | 185   | 0     | 0.51   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 7347E1387AEF | 185   | 0     | 0.51   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 542A52718391 | 185   | 0     | 0.51   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 5E6C62A7CA81 | 185   | 0     | 0.51   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 6BAB82675699 | 185   | 0     | 0.51   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | E1E1818CC54B | 185   | 0     | 0.51   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 44CB24C8DD4D | 185   | 0     | 0.51   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | DF884F1F3B9A | 185   | 0     | 0.51   |
| Dell      | Express Flash P... | 1.6 TB | 08AC832320A8 | 185   | 0     | 0.51   |
| Samsung   | MZ1LB960HAJQ-00007 | 960 GB | FF2B9C4DA498 | 185   | 0     | 0.51   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 392F06C38725 | 185   | 0     | 0.51   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 9A0FB3ED8F59 | 185   | 0     | 0.51   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | A6511974A5CF | 184   | 0     | 0.51   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | C253CF064277 | 184   | 0     | 0.51   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 41CAD381C4E3 | 184   | 0     | 0.51   |
| Samsung   | SSD 970 EVO Plus   | 2 TB   | 43A7F1F54BA5 | 184   | 0     | 0.51   |
| Samsung   | SSD 970 EVO Plus   | 2 TB   | C746AFE6BC09 | 184   | 0     | 0.51   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | B33BFE3F426D | 184   | 0     | 0.51   |
| Toshiba   | KXG60PNV2T04       | 2 TB   | D4B1683384EF | 184   | 0     | 0.51   |
| HP        | SSD EX950          | 512 GB | 6664A4881132 | 184   | 0     | 0.50   |
| Toshiba   | KXG60ZNV256G       | 256 GB | 61E27425367C | 183   | 0     | 0.50   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 4BE72EDCF81C | 183   | 0     | 0.50   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 740056E6BF84 | 183   | 0     | 0.50   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 81B9248F3051 | 183   | 0     | 0.50   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 986C2F2B1F69 | 183   | 0     | 0.50   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 1496AB90A734 | 183   | 0     | 0.50   |
| Wester... | WUS4BB038D7P3E1    | 3.8 TB | FA899B27C750 | 182   | 0     | 0.50   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 03AD8D8C6C81 | 182   | 0     | 0.50   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | FF0994FFA498 | 182   | 0     | 0.50   |
| Intel     | SSDPE2KX010T8      | 1 TB   | 565F8B836839 | 182   | 0     | 0.50   |
| Toshiba   | KXG60ZNV256G       | 256 GB | 1E2C2BB5F17C | 182   | 0     | 0.50   |
| Intel     | SSDPE2MX450G7      | 450 GB | 81DDFB04B01E | 182   | 0     | 0.50   |
| Toshiba   | KXG60ZNV256G       | 256 GB | 61728BAE327F | 182   | 0     | 0.50   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | 1439905C1360 | 182   | 0     | 0.50   |
| Intel     | SSDPE2KX010T7      | 1 TB   | 02E65367700B | 182   | 0     | 0.50   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | A1288E27B3CA | 182   | 0     | 0.50   |
| WDC       | WUS4BB038D7P3E1    | 3.8 TB | 4399D3467901 | 182   | 0     | 0.50   |
| WDC       | WUS4BB038D7P3E1    | 3.8 TB | 69642526623D | 182   | 0     | 0.50   |
| WDC       | WUS4BB038D7P3E1    | 3.8 TB | 6E9A74B75868 | 182   | 0     | 0.50   |
| WDC       | WUS4BB038D7P3E1    | 3.8 TB | 7CCE91ACB15D | 182   | 0     | 0.50   |
| WDC       | WUS4BB038D7P3E1    | 3.8 TB | D6811633124A | 182   | 0     | 0.50   |
| WDC       | WUS4BB038D7P3E1    | 3.8 TB | EE97F259102D | 182   | 0     | 0.50   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 993DABD45881 | 182   | 0     | 0.50   |
| WDC       | CL SN720 SDAQNT... | 512 GB | 888A0E53B0AE | 182   | 0     | 0.50   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | B529697CB937 | 182   | 0     | 0.50   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 1F5B5EBC6210 | 181   | 0     | 0.50   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 6FA01B872AA1 | 181   | 0     | 0.50   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 0A247E0502A4 | 181   | 0     | 0.50   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 9C428B25D088 | 181   | 0     | 0.50   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | A350ECBFB812 | 181   | 0     | 0.50   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | F80769B17DEB | 181   | 0     | 0.50   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 8DD9A264FE71 | 181   | 0     | 0.50   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 04580AB2630D | 181   | 0     | 0.50   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 6EBD5C1CFC60 | 181   | 0     | 0.50   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 768D3DCC04E3 | 181   | 0     | 0.50   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 93C6E9F9F7D6 | 181   | 0     | 0.50   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 9400AAE1875B | 181   | 0     | 0.50   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 9F5A1E819E33 | 181   | 0     | 0.50   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | A5789C50CAD5 | 181   | 0     | 0.50   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | C2C95D052C54 | 181   | 0     | 0.50   |
| Dell      | Express Flash C... | 960 GB | 671E254AD383 | 181   | 0     | 0.50   |
| Dell      | Express Flash C... | 960 GB | B877DEC7B5C6 | 181   | 0     | 0.50   |
| WDC       | WUS4BB038D7P3E1    | 3.8 TB | 742D52542FD6 | 181   | 0     | 0.50   |
| WDC       | WUS4BB038D7P3E1    | 3.8 TB | F37F49781045 | 181   | 0     | 0.50   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 9D91AB646E1F | 181   | 0     | 0.50   |
| WDC       | WUS4BB076D7P3E1    | 7.6 TB | B422EC0507D8 | 181   | 0     | 0.50   |
| Samsung   | SSD 970 EVO Plus   | 500 GB | EF3D2AAC7A0D | 181   | 0     | 0.50   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 05AD7F36087A | 180   | 0     | 0.50   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 4EE79E10FCF3 | 180   | 0     | 0.50   |
| Dell      | Express Flash C... | 960 GB | 30711FC75451 | 180   | 0     | 0.50   |
| WDC       | WUS4BB076D7P3E1    | 7.6 TB | 0362282AE506 | 180   | 0     | 0.50   |
| Samsung   | SSD 970 PRO        | 512 GB | E233FAA4556F | 180   | 0     | 0.49   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 55C1BE9DD0E4 | 180   | 0     | 0.49   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 56C822E4061B | 180   | 0     | 0.49   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 7BFD3E951156 | 180   | 0     | 0.49   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | B75DDC251152 | 180   | 0     | 0.49   |
| WDC       | WUS4BB038D7P3E1    | 3.8 TB | 3B8785F4A90F | 180   | 0     | 0.49   |
| WDC       | WUS4BB038D7P3E1    | 3.8 TB | 7CE1CB9575F3 | 180   | 0     | 0.49   |
| Intel     | SSDPE2KX010T7      | 1 TB   | AB3E5277CA74 | 180   | 0     | 0.49   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 0C22E60FDD27 | 180   | 0     | 0.49   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 2E772111A242 | 180   | 0     | 0.49   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 870373AACE17 | 180   | 0     | 0.49   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | A56A18EF81F9 | 180   | 0     | 0.49   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | B3129FB2F73B | 180   | 0     | 0.49   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | BA5AE6EBE8FE | 180   | 0     | 0.49   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 854939268E75 | 180   | 0     | 0.49   |
| WDC       | WUS4BB076D7P3E1    | 7.6 TB | DEC89BEF6FDC | 180   | 0     | 0.49   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 73EB78229C11 | 180   | 0     | 0.49   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | CB40545815CE | 179   | 0     | 0.49   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | F100EAA87BE3 | 179   | 0     | 0.49   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | F3948F129641 | 179   | 0     | 0.49   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | F3948F129641 | 179   | 0     | 0.49   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 2E43D975A7BD | 179   | 0     | 0.49   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 7682AF2E00F1 | 179   | 0     | 0.49   |
| Micron    | 9300_MTFDHAL7T6TDP | 7.6 TB | 5537710EE469 | 179   | 0     | 0.49   |
| Micron    | 9300_MTFDHAL7T6TDP | 7.6 TB | 5B6A75543D9E | 179   | 0     | 0.49   |
| Micron    | 9300_MTFDHAL7T6TDP | 7.6 TB | D43408ADCF8A | 179   | 0     | 0.49   |
| Toshiba   | KXG60ZNV256G       | 256 GB | 1042840202AC | 179   | 0     | 0.49   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 5CC1C1FCD9B9 | 179   | 0     | 0.49   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | A5B2434FBEBF | 179   | 0     | 0.49   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 5CC1C1FCD9B9 | 179   | 0     | 0.49   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | A5B2434FBEBF | 179   | 0     | 0.49   |
| WDC       | WUS4BB038D7P3E1    | 3.8 TB | DAE7C426FC89 | 179   | 0     | 0.49   |
| WDC       | WUS4BB038D7P3E1    | 3.8 TB | F60E969DEEC8 | 179   | 0     | 0.49   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | D89FCEFFD457 | 178   | 0     | 0.49   |
| Samsung   | MZPJB960HMGC-0BW07 | 960 GB | 7DBA10D31114 | 178   | 0     | 0.49   |
| WDC       | WUS4BB038D7P3E1    | 3.8 TB | 51A0A4D1E583 | 178   | 0     | 0.49   |
| WDC       | WUS4BB038D7P3E1    | 3.8 TB | A15EC2D534AF | 178   | 0     | 0.49   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 5A80229F8534 | 178   | 0     | 0.49   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 1EEEB9909B85 | 178   | 0     | 0.49   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 731551F6A8C6 | 178   | 0     | 0.49   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | AC6C4B2B0B6D | 178   | 0     | 0.49   |
| Intel     | SSDPE2KX010T8      | 1 TB   | 695C0A081E5C | 178   | 0     | 0.49   |
| Intel     | SSDPE2KX010T8      | 1 TB   | D716A2CE4BCD | 178   | 0     | 0.49   |
| Samsung   | MZ1LB960HAJQ-00007 | 960 GB | F456F90569E8 | 178   | 0     | 0.49   |
| Samsung   | MZVLB1T0HALR-00000 | 1 TB   | BB1480ACB6D9 | 178   | 0     | 0.49   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 2F6F38F36B3C | 178   | 0     | 0.49   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | CE2EAD3EA867 | 178   | 0     | 0.49   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | BEE7EEC7BF47 | 178   | 0     | 0.49   |
| Silico... | NVME SSD           | 512 GB | 651BDB47CC19 | 177   | 0     | 0.49   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 67C66E4A0955 | 177   | 0     | 0.49   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 8615C64F492E | 177   | 0     | 0.49   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 1939741F618B | 177   | 0     | 0.49   |
| Samsung   | MZPJB480HMGC-0BW07 | 480 GB | 4906901B8772 | 177   | 0     | 0.49   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 748FFEDDC88E | 177   | 0     | 0.49   |
| Samsung   | MZPLJ3T2HBJR-00007 | 3.2 TB | B0796A413765 | 177   | 0     | 0.49   |
| Intel     | SSDPE2KX010T7      | 1 TB   | D7661B9AAAC6 | 177   | 0     | 0.49   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 45358D2BD031 | 176   | 0     | 0.48   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 4DF314631488 | 176   | 0     | 0.48   |
| HP        | SSD EX950          | 512 GB | ADE1ACF3156F | 176   | 0     | 0.48   |
| Intel     | SSDPE2KX010T7      | 1 TB   | 0351B97A02AE | 176   | 0     | 0.48   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | CC64E4EDCE16 | 176   | 0     | 0.48   |
| Toshiba   | KXG60PNV2T04       | 2 TB   | 7BE41E96E35B | 176   | 0     | 0.48   |
| Wester... | WUS4BB038D7P3E3    | 3.8 TB | 01A88B4A6899 | 176   | 0     | 0.48   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 522A720D7A44 | 176   | 0     | 0.48   |
| WDC       | CL SN720 SDAQNT... | 512 GB | C26EFDD3A616 | 176   | 0     | 0.48   |
| Intel     | SSDPE2KX040T8      | 4 TB   | FA12A6CD96B4 | 175   | 0     | 0.48   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 3B0B5214CFFE | 175   | 0     | 0.48   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 03435433A6E5 | 175   | 0     | 0.48   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 6181AE5DA4BF | 175   | 0     | 0.48   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 61C075E2C538 | 175   | 0     | 0.48   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 7CB836929C9C | 175   | 0     | 0.48   |
| Intel     | SSDPE2KX010T8      | 1 TB   | 0CCC753FB140 | 175   | 0     | 0.48   |
| Toshiba   | KXG60PNV2T04       | 2 TB   | E03D0BB740D1 | 175   | 0     | 0.48   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | EA073EFBA224 | 175   | 0     | 0.48   |
| Samsung   | SSD 970 EVO Plus   | 250 GB | BD949623C438 | 175   | 0     | 0.48   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 38247A051510 | 175   | 0     | 0.48   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | DA6622F0FC08 | 175   | 0     | 0.48   |
| WDC       | WUS4BB038D7P3E1    | 3.8 TB | 4D130371EB24 | 175   | 0     | 0.48   |
| WDC       | WUS4BB038D7P3E1    | 3.8 TB | 7CBF0C8A7902 | 175   | 0     | 0.48   |
| WDC       | WUS4BB038D7P3E1    | 3.8 TB | A7EE929F4E67 | 175   | 0     | 0.48   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | C12E965E3C93 | 175   | 0     | 0.48   |
| WDC       | WUS4BB038D7P3E1    | 3.8 TB | 2640A79A104C | 175   | 0     | 0.48   |
| WDC       | WUS4BB038D7P3E1    | 3.8 TB | 983C4EA31EA6 | 175   | 0     | 0.48   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | D8E5FDB42992 | 175   | 0     | 0.48   |
| Samsung   | MZVLB512HAJQ-00000 | 512 GB | 8F2B8B6D9C41 | 175   | 0     | 0.48   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 13308F3B96B7 | 174   | 0     | 0.48   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 8F1177251B95 | 174   | 0     | 0.48   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 1326F33A83F3 | 174   | 0     | 0.48   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | DFA5245AB425 | 174   | 0     | 0.48   |
| Micron    | 9300_MTFDHAL7T6TDP | 7.6 TB | B14BD21D0BA7 | 174   | 0     | 0.48   |
| Micron    | 9300_MTFDHAL7T6TDP | 7.6 TB | B13D7498CA05 | 174   | 0     | 0.48   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | A0140E3EB292 | 174   | 0     | 0.48   |
| Samsung   | SSD 970 EVO Plus   | 1 TB   | 4ADBB7D198CD | 174   | 0     | 0.48   |
| Wester... | WUS4BB038D7P3E1    | 3.8 TB | B022A70EF1BF | 174   | 0     | 0.48   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 60F9A85D2013 | 174   | 0     | 0.48   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 66DC32915CB5 | 174   | 0     | 0.48   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 3CC0FDFC43CE | 174   | 0     | 0.48   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 3DD2ED3DAEC7 | 174   | 0     | 0.48   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 7617A2427AD5 | 174   | 0     | 0.48   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 8F3A9FA52E26 | 174   | 0     | 0.48   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | AB920F1C3075 | 174   | 0     | 0.48   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | EB22ECA75633 | 174   | 0     | 0.48   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 08783FBCC52C | 173   | 0     | 0.48   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 1EB7764DCC73 | 173   | 0     | 0.48   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 57629AEBD223 | 173   | 0     | 0.48   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 631BFE626846 | 173   | 0     | 0.48   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | D72A47BCD18E | 173   | 0     | 0.48   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | E4D3B577B30E | 173   | 0     | 0.48   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 7B5360EAFD36 | 173   | 0     | 0.48   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | AEFF53460039 | 173   | 0     | 0.48   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | E816B3A199F8 | 173   | 0     | 0.48   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | EFF3473D7193 | 173   | 0     | 0.48   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | F9B6F319496A | 173   | 0     | 0.48   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 9CE1FC6BD12E | 173   | 0     | 0.48   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | F741B3278AB6 | 173   | 0     | 0.48   |
| WDC       | WUS4BB038D7P3E1    | 3.8 TB | 668D1BB5F70E | 173   | 0     | 0.47   |
| WDC       | WUS4BB038D7P3E1    | 3.8 TB | D06A1436E6B4 | 173   | 0     | 0.47   |
| Samsung   | MZPLJ6T4HALA-00007 | 6.4 TB | B41654F9D36A | 173   | 0     | 0.47   |
| Intel     | SSDPEL1K100GA      | 100 GB | D99FF697F8B2 | 172   | 0     | 0.47   |
| WDC       | WUS4BB038D7P3E1    | 3.8 TB | E81AE5292DD7 | 172   | 0     | 0.47   |
| Samsung   | MZPJB960HMGC-0BW07 | 960 GB | 776F55D86A22 | 172   | 0     | 0.47   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 2A73D02D916D | 172   | 0     | 0.47   |
| Samsung   | MZPLJ6T4HALA-00007 | 6.4 TB | 18AAC43F7F46 | 172   | 0     | 0.47   |
| Samsung   | MZVLB1T0HBLR-00000 | 1 TB   | 545966F66142 | 172   | 0     | 0.47   |
| Intel     | SSDPE2KX040T8      | 4 TB   | 53A29B91A069 | 172   | 0     | 0.47   |
| Wester... | WUS4BB038D7P3E3    | 3.8 TB | B2ECA2CA49B5 | 172   | 0     | 0.47   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 58974B2425D2 | 171   | 0     | 0.47   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 5097E1EA912D | 171   | 0     | 0.47   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | C9FA070EA3AF | 171   | 0     | 0.47   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | A33B584447C0 | 171   | 0     | 0.47   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 253FB08073C9 | 171   | 0     | 0.47   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 3F58DE683A8B | 171   | 0     | 0.47   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 402DAB20F8B6 | 171   | 0     | 0.47   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 4A478A3FA080 | 171   | 0     | 0.47   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 77D4F9CC0E5E | 171   | 0     | 0.47   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 7AD3A09B7F7C | 171   | 0     | 0.47   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | DA337243335D | 171   | 0     | 0.47   |
| Samsung   | MZ1LB3T8HMLA-00007 | 3.8 TB | 1D79AE8A7B73 | 171   | 0     | 0.47   |
| WDC       | WDS500G3X0C-00SJG0 | 500 GB | D2BB74CA76F5 | 171   | 0     | 0.47   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | F60C22798A74 | 171   | 0     | 0.47   |
| Samsung   | SSD 970 PRO        | 512 GB | EFA58FA5BFF9 | 171   | 0     | 0.47   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | AFB77A4F7549 | 170   | 0     | 0.47   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 8A11B05ECC44 | 170   | 0     | 0.47   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | A14DA1D5F2C2 | 170   | 0     | 0.47   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | E63D7BAEFC08 | 170   | 0     | 0.47   |
| Samsung   | MZVPV512HDGL-00000 | 512 GB | 5D9ED1E5B856 | 170   | 0     | 0.47   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 6157E0D89DFC | 170   | 0     | 0.47   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 75710A2D82F0 | 170   | 0     | 0.47   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 8998814DCA3F | 170   | 0     | 0.47   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | D675A8D19FC5 | 170   | 0     | 0.47   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | DC077334B828 | 170   | 0     | 0.47   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | DF9D11F36123 | 170   | 0     | 0.47   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | E2B879FAF8CF | 170   | 0     | 0.47   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | E74A1C415317 | 170   | 0     | 0.47   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 809FC615E552 | 170   | 0     | 0.47   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | DC9805989BD2 | 170   | 0     | 0.47   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 1DF3F046DE95 | 169   | 0     | 0.47   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | B9E5FFA15C61 | 169   | 0     | 0.46   |
| WDC       | WUS4BB038D7P3E1    | 3.8 TB | 9BA44887EB20 | 169   | 0     | 0.46   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 04BBDAFCFB34 | 169   | 0     | 0.46   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 21848508E847 | 169   | 0     | 0.46   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 4BC6A6777A60 | 169   | 0     | 0.46   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 607AFAC5A85B | 169   | 0     | 0.46   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 72A354148275 | 169   | 0     | 0.46   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 951B9D6CDA5C | 169   | 0     | 0.46   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | BDDB911208F7 | 169   | 0     | 0.46   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | E7DB73320244 | 169   | 0     | 0.46   |
| WDC       | WUS4BB076D7P3E1    | 7.6 TB | 90A80060E5A8 | 169   | 0     | 0.46   |
| WDC       | WUS4BB038D7P3E1    | 3.8 TB | BB979B2C17DB | 169   | 0     | 0.46   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 6CD0498B3E46 | 169   | 0     | 0.46   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 6CD0498B3E46 | 169   | 0     | 0.46   |
| Kingston  | SA2000M81000G      | 1 TB   | 45AB92B7959D | 169   | 0     | 0.46   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 1DAB61D05C67 | 168   | 0     | 0.46   |
| Samsung   | MZPLJ6T4HALA-00007 | 6.4 TB | 8B5A44A1E64E | 168   | 0     | 0.46   |
| Intel     | SSDPE2KX010T8      | 1 TB   | DE2E5240BF51 | 168   | 0     | 0.46   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | A1B764986DF9 | 168   | 0     | 0.46   |
| XPG       | SPECTRIX S40G      | 1 TB   | E8368B7E6777 | 168   | 0     | 0.46   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 8B67421D0779 | 167   | 0     | 0.46   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | B3F89BBDD507 | 167   | 0     | 0.46   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | EA7B36580EF9 | 167   | 0     | 0.46   |
| Dell      | Express Flash C... | 960 GB | AFAB43B00C31 | 167   | 0     | 0.46   |
| Dell      | Express Flash C... | 960 GB | CDB63E45F678 | 167   | 0     | 0.46   |
| WDC       | WUS4BB038D7P3E1    | 3.8 TB | D29D6144B386 | 167   | 0     | 0.46   |
| Wester... | WUS4BB038D7P3E1    | 3.8 TB | BEBD83CC0369 | 167   | 0     | 0.46   |
| Intel     | SSDPE2KX010T7      | 1 TB   | 63C975B2211D | 167   | 0     | 0.46   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | C6A4F475F5A6 | 167   | 0     | 0.46   |
| Samsung   | MZVLB1T0HALR-00000 | 1 TB   | B5F3FC45FF3F | 334   | 1     | 0.46   |
| Dell      | Express Flash C... | 960 GB | 08CE10D3970C | 167   | 0     | 0.46   |
| Samsung   | MZVLB1T0HBLR-00000 | 1 TB   | DCB569DFB3F1 | 167   | 0     | 0.46   |
| Micron    | 9300_MTFDHAL3T2TDR | 3.2 TB | 9E3ADD404EA6 | 166   | 0     | 0.46   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | C611FB326675 | 166   | 0     | 0.46   |
| WDC       | WUS4BB038D7P3E1    | 3.8 TB | 94C7D65D446E | 166   | 0     | 0.46   |
| Samsung   | MZVLB1T0HBLR-00000 | 1 TB   | 12502883F0F8 | 166   | 0     | 0.46   |
| Kingston  | SEDC1000M3840G     | 3.8 TB | 49AF0F08B4A5 | 166   | 0     | 0.46   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 2E6AB5BBF874 | 165   | 0     | 0.45   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 94CE26130153 | 165   | 0     | 0.45   |
| Intel     | SSDPE2KX010T7      | 1 TB   | C1FDC03C4F41 | 165   | 0     | 0.45   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 1915A3179B30 | 165   | 0     | 0.45   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 06856B7D66F5 | 165   | 0     | 0.45   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 24B3100ED50C | 165   | 0     | 0.45   |
| Samsung   | SSD 970 PRO        | 512 GB | 1F4A2879BEF0 | 165   | 0     | 0.45   |
| Toshiba   | KXG60ZNV1T02       | 1 TB   | BC74A619E832 | 164   | 0     | 0.45   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | B161B3A6E202 | 164   | 0     | 0.45   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 15002F258BC3 | 164   | 0     | 0.45   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 5C2733DC6EB2 | 164   | 0     | 0.45   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 5F3DF42509D2 | 164   | 0     | 0.45   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 5F54DF56BD4C | 164   | 0     | 0.45   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 61C8D1CCEE5C | 164   | 0     | 0.45   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 644382597E7C | 164   | 0     | 0.45   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 84C586D96394 | 164   | 0     | 0.45   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 991010FE78F0 | 164   | 0     | 0.45   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | A6D662C08DC4 | 164   | 0     | 0.45   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | B1DF320AD09A | 164   | 0     | 0.45   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | BA117E568FF8 | 164   | 0     | 0.45   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | E2EC210233BD | 164   | 0     | 0.45   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | F64985C8C3E3 | 164   | 0     | 0.45   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | F739E213E85D | 164   | 0     | 0.45   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | FF0CF4490946 | 164   | 0     | 0.45   |
| Patriot   | P300               | 128 GB | BB03FA929658 | 164   | 0     | 0.45   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 5A74513727F4 | 164   | 0     | 0.45   |
| WDC       | WDS500G3X0C-00SJG0 | 500 GB | 1D603C3B25EE | 163   | 0     | 0.45   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 3D4EDFE22F72 | 163   | 0     | 0.45   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 74DFEED97B29 | 163   | 0     | 0.45   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | B2461F9A6291 | 163   | 0     | 0.45   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | C23232BBF45B | 163   | 0     | 0.45   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | D2F43949BAFC | 163   | 0     | 0.45   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | E5ACC8C0EE26 | 163   | 0     | 0.45   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | EC7A0CB10E67 | 163   | 0     | 0.45   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | F881463C0B19 | 163   | 0     | 0.45   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | D96083BB4D77 | 163   | 0     | 0.45   |
| Patriot   | P300               | 128 GB | 96113185A8B4 | 163   | 0     | 0.45   |
| Micron    | 9300_MTFDHAL3T2TDR | 3.2 TB | F23FA6A98C30 | 163   | 0     | 0.45   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 052F7DCB4D0F | 163   | 0     | 0.45   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | CE2B5811ED94 | 163   | 0     | 0.45   |
| Intel     | SSDPED1D280GA      | 280 GB | 8878C8EB3EF1 | 163   | 0     | 0.45   |
| Samsung   | MZPLJ6T4HALA-00007 | 6.4 TB | B5947CC35D50 | 163   | 0     | 0.45   |
| Samsung   | MZPJB480HMGC-0BW07 | 480 GB | D0C7A74AE194 | 163   | 0     | 0.45   |
| Dell      | Express Flash C... | 960 GB | 7FC8FD67B8B5 | 163   | 0     | 0.45   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | DBB46BC23C00 | 163   | 0     | 0.45   |
| Dell      | Express Flash C... | 960 GB | 5AA4BD0A321A | 163   | 0     | 0.45   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 748FC5806C90 | 163   | 0     | 0.45   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 34B5246C799B | 163   | 0     | 0.45   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | E5B15315A928 | 163   | 0     | 0.45   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 97FF54DC2B3A | 163   | 0     | 0.45   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 32C5E49440F9 | 163   | 0     | 0.45   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 60819134B151 | 163   | 0     | 0.45   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 7DBB0C203DAC | 163   | 0     | 0.45   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 0C38E08621DF | 162   | 0     | 0.45   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 23D268247B7B | 162   | 0     | 0.45   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | F2663AE47931 | 162   | 0     | 0.45   |
| WDC       | WUS4BB076D7P3E1    | 7.6 TB | 81C43BB7398D | 162   | 0     | 0.45   |
| WDC       | WDS500G3X0C-00SJG0 | 500 GB | E09F9F276E4C | 162   | 0     | 0.45   |
| WDC       | WDS500G3X0C-00SJG0 | 500 GB | 74A20FC3003D | 162   | 0     | 0.45   |
| Intel     | SSDPEKKW512G8      | 512 GB | 07C317986BB4 | 162   | 0     | 0.45   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 775FD26C006A | 162   | 0     | 0.45   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 80ED12DA423E | 162   | 0     | 0.45   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 808F3FD7E9B6 | 162   | 0     | 0.45   |
| Patriot   | P300               | 128 GB | 88B48E0BB33B | 162   | 0     | 0.45   |
| Samsung   | SSD 970 PRO        | 512 GB | E13189A5CFF5 | 162   | 0     | 0.44   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 9DBD5C4DD039 | 162   | 0     | 0.44   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | DFBAA0286BAA | 162   | 0     | 0.44   |
| Patriot   | P300               | 128 GB | 512BBEFC4213 | 162   | 0     | 0.44   |
| WDC       | WDS500G3X0C-00SJG0 | 500 GB | F571CD2D339A | 162   | 0     | 0.44   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 1E32D7DC2761 | 162   | 0     | 0.44   |
| Dell      | Express Flash C... | 960 GB | 819B41D7CB3C | 162   | 0     | 0.44   |
| Dell      | Express Flash C... | 960 GB | BDBF0E325C29 | 162   | 0     | 0.44   |
| Dell      | Express Flash C... | 960 GB | C9FE8DD90546 | 162   | 0     | 0.44   |
| Dell      | Express Flash C... | 960 GB | D7E5E21715B7 | 162   | 0     | 0.44   |
| Samsung   | MZPLJ6T4HALA-00007 | 6.4 TB | 1D37430661FC | 161   | 0     | 0.44   |
| WDC       | WDS500G3X0C-00SJG0 | 500 GB | 06BD61F6B8BC | 161   | 0     | 0.44   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 1C8DF65F3091 | 161   | 0     | 0.44   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 9701DA723C0A | 161   | 0     | 0.44   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 9C230D8DF0E7 | 161   | 0     | 0.44   |
| Samsung   | MZVLB512HBJQ-00000 | 512 GB | 7DE76F6C1014 | 161   | 0     | 0.44   |
| Samsung   | MZPJB960HMGC-0BW07 | 960 GB | 9D6E215578C1 | 161   | 0     | 0.44   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 7B009D3C7536 | 161   | 0     | 0.44   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | B4F5CAC0D8C2 | 161   | 0     | 0.44   |
| Samsung   | MZ4LB3T8HALS-00003 | 3.8 TB | 684C8A28B4FC | 161   | 0     | 0.44   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 3BADA762C78F | 161   | 0     | 0.44   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 4D379C2B6B87 | 161   | 0     | 0.44   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 6D76199E1927 | 161   | 0     | 0.44   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 920BFEA5D5CC | 161   | 0     | 0.44   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | A853F34D6A01 | 161   | 0     | 0.44   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | AA7992754834 | 161   | 0     | 0.44   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | DB38F7FFE8A6 | 161   | 0     | 0.44   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | FC5658EC9F3E | 161   | 0     | 0.44   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 19A573F99421 | 160   | 0     | 0.44   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 47EC2BB94CC1 | 160   | 0     | 0.44   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | A809B65B4EB0 | 160   | 0     | 0.44   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | D942F14DDC69 | 160   | 0     | 0.44   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 49F6377E97D7 | 160   | 0     | 0.44   |
| Samsung   | SSD 970 EVO Plus   | 250 GB | D28EE1789C48 | 160   | 0     | 0.44   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 0E7390236933 | 160   | 0     | 0.44   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | D8CF6B58B57B | 159   | 0     | 0.44   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | A953EB4FC1BC | 159   | 0     | 0.44   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 433D7FA794A3 | 159   | 0     | 0.44   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 4949FF279A94 | 159   | 0     | 0.44   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 65F83ADBF8E4 | 159   | 0     | 0.44   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 6DCF0543D739 | 159   | 0     | 0.44   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 92A830AD26F5 | 159   | 0     | 0.44   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 94250C6B761C | 159   | 0     | 0.44   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | D9F384B63ED8 | 159   | 0     | 0.44   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | FDBB53869B62 | 159   | 0     | 0.44   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 7FB6076AE15C | 159   | 0     | 0.44   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | A5863B4FA030 | 159   | 0     | 0.44   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | F26B9990BC15 | 159   | 0     | 0.44   |
| Samsung   | MZ1LB960HAJQ-00007 | 960 GB | D6F7F7BF3663 | 159   | 0     | 0.44   |
| Samsung   | MZ1LB960HAJQ-00007 | 960 GB | 21F70684B41C | 159   | 0     | 0.44   |
| Samsung   | SSD 970 PRO        | 512 GB | 23FB585F337A | 159   | 0     | 0.44   |
| WDC       | CL SN720 SDAQNT... | 512 GB | 84E6661E72DA | 159   | 0     | 0.44   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 3182404CEB47 | 159   | 0     | 0.44   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 7FB120FF64B6 | 159   | 0     | 0.44   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | F6D3B5A5F6D3 | 159   | 0     | 0.44   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 77C31D8960AA | 158   | 0     | 0.44   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 3EFE2F3E6C9C | 158   | 0     | 0.44   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 4B5793800134 | 158   | 0     | 0.44   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 81C745AFD0E7 | 158   | 0     | 0.44   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | B5AFB5C9C430 | 158   | 0     | 0.44   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | B752B379A6A7 | 158   | 0     | 0.44   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | C1AB07CBA62E | 158   | 0     | 0.44   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | CFF6895C7EEF | 158   | 0     | 0.44   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | E958B8D5883D | 158   | 0     | 0.44   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 31DF5FD5FB56 | 158   | 0     | 0.44   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 3DD0FE3B5DF9 | 158   | 0     | 0.44   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 467677EA1210 | 158   | 0     | 0.44   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 5C290EA51A46 | 158   | 0     | 0.44   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 934BE33CCACC | 158   | 0     | 0.44   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | A4C013D1DF66 | 158   | 0     | 0.44   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | C9BFA74F1467 | 158   | 0     | 0.44   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | F97A79B57C6B | 158   | 0     | 0.44   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 13FB5EAA696D | 158   | 0     | 0.43   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | ED3C7992DF93 | 158   | 0     | 0.43   |
| Samsung   | MZ1LB960HAJQ-00007 | 960 GB | 71A64F4BF883 | 158   | 0     | 0.43   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 3866E89C371D | 158   | 0     | 0.43   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | AD6EA223BA22 | 158   | 0     | 0.43   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | B78331DEF1D1 | 158   | 0     | 0.43   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | C55746C57F81 | 158   | 0     | 0.43   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | D79A92FF424C | 158   | 0     | 0.43   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | F0330782A2B6 | 158   | 0     | 0.43   |
| WDC       | WUS4BB038D7P3E1    | 3.8 TB | 222380954453 | 158   | 0     | 0.43   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 2A17B23A496D | 158   | 0     | 0.43   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 24387343F84C | 157   | 0     | 0.43   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 3A3D800273B5 | 157   | 0     | 0.43   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 467AAA6EFC80 | 157   | 0     | 0.43   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 99713676E102 | 157   | 0     | 0.43   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | B9F7036E3BB3 | 157   | 0     | 0.43   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | DADC816EF494 | 157   | 0     | 0.43   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | ECDDC3C1EBB6 | 157   | 0     | 0.43   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | F298EA42A07E | 157   | 0     | 0.43   |
| WDC       | WUS4BB076D7P3E1    | 7.6 TB | 59FE2E84DBB7 | 157   | 0     | 0.43   |
| Samsung   | MZPLJ6T4HALA-00007 | 6.4 TB | 28F599A9C48F | 157   | 0     | 0.43   |
| Samsung   | SSD 970 PRO        | 512 GB | 5267A8DDEAB5 | 157   | 0     | 0.43   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 2BB09E6CB5EB | 157   | 0     | 0.43   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 3769348839B1 | 157   | 0     | 0.43   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 504CFE021728 | 157   | 0     | 0.43   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 574F6192E9A1 | 157   | 0     | 0.43   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 6745F015EA39 | 157   | 0     | 0.43   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 684653D8BC4C | 157   | 0     | 0.43   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 9201150B8F16 | 157   | 0     | 0.43   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 9CBCF9F3C7A6 | 157   | 0     | 0.43   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 9FC189B51393 | 157   | 0     | 0.43   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | B20C0095A7B0 | 157   | 0     | 0.43   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | CBA0233DFBFF | 157   | 0     | 0.43   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | DAB0DA4865B0 | 157   | 0     | 0.43   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | E1ED30786662 | 157   | 0     | 0.43   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | E5A7E11EC8C3 | 157   | 0     | 0.43   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 319DACF85B27 | 157   | 0     | 0.43   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 63FDC270E09B | 156   | 0     | 0.43   |
| Samsung   | MZVLB1T0HBLR-00000 | 1 TB   | 2A5B9155D05E | 156   | 0     | 0.43   |
| Samsung   | SSD 970 EVO Plus   | 1 TB   | C56DDD4A776D | 156   | 0     | 0.43   |
| Dell      | Express Flash C... | 960 GB | 8FB074BA6CA5 | 156   | 0     | 0.43   |
| Dell      | Express Flash C... | 960 GB | EFB6EBE0F96B | 156   | 0     | 0.43   |
| Dell      | Express Flash C... | 960 GB | FA93CFA2FF64 | 156   | 0     | 0.43   |
| Samsung   | SSD 970 EVO Plus   | 1 TB   | 096E7E45C04E | 156   | 0     | 0.43   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 38EE3A6D4F33 | 156   | 0     | 0.43   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 88F77153A298 | 156   | 0     | 0.43   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | C8B5A2DFA3A2 | 156   | 0     | 0.43   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 303C5EA45430 | 156   | 0     | 0.43   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 31231AFA578E | 156   | 0     | 0.43   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 48E1C5887A93 | 156   | 0     | 0.43   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 649003CDD1C6 | 156   | 0     | 0.43   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | A4E8C74ED341 | 156   | 0     | 0.43   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | B700DD1356F2 | 156   | 0     | 0.43   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | CAD60F331E61 | 156   | 0     | 0.43   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | EB9E1751FE03 | 156   | 0     | 0.43   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | F655B4564382 | 156   | 0     | 0.43   |
| Dell      | Express Flash C... | 960 GB | 0E3AF85F527F | 156   | 0     | 0.43   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | B951D6A3CD46 | 156   | 0     | 0.43   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 8066CA1AF977 | 156   | 0     | 0.43   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | B50E937B0ED3 | 156   | 0     | 0.43   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 06454D3C2FEC | 155   | 0     | 0.43   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | C2B54E7AE04A | 155   | 0     | 0.43   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 2C7BD304AA0F | 155   | 0     | 0.43   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 2C97E6C3057B | 155   | 0     | 0.43   |
| Samsung   | SSD 970 EVO Plus   | 1 TB   | 72F42EF3A663 | 155   | 0     | 0.43   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 6873A7E3B98B | 154   | 0     | 0.42   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 807DD0CD2A97 | 154   | 0     | 0.42   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | 694F25044A9E | 154   | 0     | 0.42   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | AABE12E4CC1E | 154   | 0     | 0.42   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | B522440B5ECE | 154   | 0     | 0.42   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | D0F26AF6725D | 154   | 0     | 0.42   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 11C88571D978 | 154   | 0     | 0.42   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 2BA8FC39FAC3 | 154   | 0     | 0.42   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 53363624CE31 | 154   | 0     | 0.42   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | B43DCF8B47C3 | 154   | 0     | 0.42   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | F570095B1878 | 154   | 0     | 0.42   |
| Samsung   | MZPLJ6T4HALA-00007 | 6.4 TB | 9D1D4E6FCF4A | 154   | 0     | 0.42   |
| Corsair   | Force MP510 1.9TB  | 1.9 TB | CCD58EA18164 | 153   | 0     | 0.42   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | C55B25386AB9 | 153   | 0     | 0.42   |
| Samsung   | SSD 970 EVO Plus   | 250 GB | 5BC7B028D45A | 153   | 0     | 0.42   |
| Intel     | SSDPED1D280GA      | 280 GB | 69FDD5BC9FC0 | 767   | 4     | 0.42   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 15C13D283828 | 153   | 0     | 0.42   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 8088C66B8217 | 153   | 0     | 0.42   |
| Samsung   | MZ4LB3T8HALS-00003 | 3.8 TB | F0437AD4FED9 | 152   | 0     | 0.42   |
| Samsung   | MZVLB1T0HBLR-00000 | 1 TB   | 5239F719097C | 152   | 0     | 0.42   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | BDB9151CDC68 | 152   | 0     | 0.42   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | A362A45B6320 | 151   | 0     | 0.42   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 8B925B4D6588 | 151   | 0     | 0.42   |
| Dell      | Express Flash C... | 960 GB | 816C4F4EFD6E | 151   | 0     | 0.41   |
| Samsung   | MZVLB1T0HBLR-00000 | 1 TB   | 1558BB06654F | 151   | 0     | 0.41   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 6D55FAD17913 | 151   | 0     | 0.41   |
| WDC       | CL SN720 SDAQNT... | 512 GB | A56CECE7387E | 150   | 0     | 0.41   |
| Micron    | 2200_MTFDHBA512TCK | 512 GB | 58F504A4A7DB | 150   | 0     | 0.41   |
| Toshiba   | KXG60ZNV256G       | 256 GB | 37E27E0BE79C | 150   | 0     | 0.41   |
| Dell      | Express Flash C... | 960 GB | DDAA07C27AF5 | 149   | 0     | 0.41   |
| Micron    | 2200_MTFDHBA512TCK | 512 GB | C45A79A5C364 | 149   | 0     | 0.41   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 127CE45C8189 | 149   | 0     | 0.41   |
| Wester... | WUS4BB038D7P3E1    | 3.8 TB | B8E91BD02A65 | 149   | 0     | 0.41   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | CB3AF644ACF9 | 149   | 0     | 0.41   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 18D167A610E7 | 149   | 0     | 0.41   |
| Intel     | SSDPECME016T4      | 800 GB | 2E2F1E27BEBA | 149   | 0     | 0.41   |
| Intel     | SSDPECME016T4      | 800 GB | AAC91B596135 | 149   | 0     | 0.41   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 907A2B319C1F | 149   | 0     | 0.41   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | A8EFAD68832F | 148   | 0     | 0.41   |
| Samsung   | SSD 970 PRO        | 512 GB | 7E6DAE3C2F64 | 148   | 0     | 0.41   |
| Samsung   | SSD 970 EVO Plus   | 500 GB | 39CB5B6D3E56 | 148   | 0     | 0.41   |
| Samsung   | MZVLB1T0HBLR-00000 | 1 TB   | 8D685025616D | 148   | 0     | 0.41   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 14C9BAAC60AA | 148   | 0     | 0.41   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 906DE683D0CE | 148   | 0     | 0.41   |
| Samsung   | SSD 970 PRO        | 1 TB   | B8B94A57B921 | 147   | 0     | 0.40   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 61E06B6A451E | 147   | 0     | 0.40   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | C5AC5BE4CC4D | 147   | 0     | 0.40   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | C8130D98D3FD | 147   | 0     | 0.40   |
| Samsung   | MZPJB960HMGC-0BW07 | 960 GB | F6EE73E337C7 | 147   | 0     | 0.40   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | FBA0EB3C83FC | 146   | 0     | 0.40   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | 6C3A1BD6A91A | 146   | 0     | 0.40   |
| Samsung   | SSD 970 EVO Plus   | 1 TB   | 59B64F6C99EC | 146   | 0     | 0.40   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | A928A4D157C3 | 146   | 0     | 0.40   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 7D321AC4405B | 146   | 0     | 0.40   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 55B9C3B96373 | 146   | 0     | 0.40   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 7CDEC683A2A9 | 146   | 0     | 0.40   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | C394BBC7647B | 146   | 0     | 0.40   |
| Intel     | SSDPE2KX080T8      | 8 TB   | 063BE087B0E9 | 145   | 0     | 0.40   |
| Samsung   | MZPLJ6T4HALA-00007 | 6.4 TB | 380E80836023 | 145   | 0     | 0.40   |
| WDC       | WUS4BB076D7P3E1    | 7.6 TB | CA302A5BC9EE | 145   | 0     | 0.40   |
| Wester... | WUS4BB076D7P3E1    | 7.6 TB | CA302A5BC9EE | 145   | 0     | 0.40   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 3B5BC6002A72 | 145   | 0     | 0.40   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | AE6B7C6D71A7 | 145   | 0     | 0.40   |
| WDC       | WUS4BB038D7P3E1    | 3.8 TB | 1208096BEDC4 | 145   | 0     | 0.40   |
| Phison    | Viper M.2 VPN100   | 256 GB | FF5398912F8B | 145   | 0     | 0.40   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 899F33C20106 | 145   | 0     | 0.40   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | BC76BEBAD218 | 145   | 0     | 0.40   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 84317303C5F1 | 144   | 0     | 0.40   |
| Samsung   | MZPJB960HMGC-0BW07 | 960 GB | 763F072F283F | 144   | 0     | 0.40   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | EC9F645008D5 | 144   | 0     | 0.40   |
| Micron    | 9300_MTFDHAL3T2TDR | 3.2 TB | BC8A95C81634 | 144   | 0     | 0.40   |
| Micron    | 9300_MTFDHAL3T2TDR | 3.2 TB | 8522B229E312 | 143   | 0     | 0.39   |
| Samsung   | SSD 970 EVO        | 1 TB   | FE7EA598986B | 143   | 0     | 0.39   |
| Intel     | SSDPECME016T4      | 800 GB | 52C8BB8B6F10 | 143   | 0     | 0.39   |
| Intel     | SSDPECME016T4      | 800 GB | 5AE6C02BF786 | 143   | 0     | 0.39   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | D23906F95383 | 143   | 0     | 0.39   |
| Samsung   | MZ1LB3T8HMLA-00007 | 3.8 TB | 19FA87CEF7DB | 143   | 0     | 0.39   |
| Samsung   | MZ1LB3T8HMLA-00007 | 3.8 TB | CFBB5C2D41FF | 143   | 0     | 0.39   |
| Wester... | WUS4BB038D7P3E3    | 3.8 TB | 05181ED19198 | 143   | 0     | 0.39   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 0FC04E3FF1B2 | 142   | 0     | 0.39   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 0FC04E3FF1B2 | 142   | 0     | 0.39   |
| Intel     | SSDPE2KX010T8      | 1 TB   | 2831A1EF2901 | 142   | 0     | 0.39   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 7A4071293EB7 | 142   | 0     | 0.39   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 29DDAA46AF14 | 142   | 0     | 0.39   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 71D25D963843 | 142   | 0     | 0.39   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | F1743FBA8282 | 142   | 0     | 0.39   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 895EC753D090 | 142   | 0     | 0.39   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 3EFD6C0A7A34 | 142   | 0     | 0.39   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 48CC800105B4 | 142   | 0     | 0.39   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | C3B6835DC846 | 142   | 0     | 0.39   |
| Samsung   | SSD 960 EVO        | 500 GB | 31D1652867A2 | 141   | 0     | 0.39   |
| Micron    | 9300_MTFDHAL3T2TDR | 3.2 TB | 00394CC7CDB1 | 141   | 0     | 0.39   |
| Samsung   | MZVLB1T0HBLR-00000 | 1 TB   | 82847C942482 | 141   | 0     | 0.39   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | DD728F2063D7 | 141   | 0     | 0.39   |
| Samsung   | SSD 970 EVO Plus   | 2 TB   | 2EDB1C97A436 | 141   | 0     | 0.39   |
| Samsung   | SSD 970 EVO Plus   | 2 TB   | B5C2E73932C3 | 141   | 0     | 0.39   |
| Samsung   | MZPJB960HMGC-0BW07 | 960 GB | FBF9584D458E | 140   | 0     | 0.39   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 38D9F2685BD0 | 140   | 0     | 0.38   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 3E61A098A933 | 140   | 0     | 0.38   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 6C89006F3632 | 140   | 0     | 0.38   |
| Samsung   | SSD 970 PRO        | 512 GB | 44A3ADF9574C | 139   | 0     | 0.38   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 0D943062122F | 139   | 0     | 0.38   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | D6CC0FB3F2E2 | 139   | 0     | 0.38   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | F1E6ECCBA35F | 139   | 0     | 0.38   |
| Intel     | SSDPE2KX080T8      | 8 TB   | 3531F0A593F4 | 139   | 0     | 0.38   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 05BA3FF3399B | 139   | 0     | 0.38   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 67F5E6C78303 | 139   | 0     | 0.38   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | BCB451AD261F | 139   | 0     | 0.38   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | BDBD7E6A4FCC | 139   | 0     | 0.38   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | D865A87894BF | 139   | 0     | 0.38   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | DF8ECD6ACBE9 | 139   | 0     | 0.38   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | E7547E8BCF0A | 139   | 0     | 0.38   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | FACBDCA61636 | 139   | 0     | 0.38   |
| WDC       | WUS4BB019D7P3E3    | 1.9 TB | 715CB3AD7588 | 139   | 0     | 0.38   |
| Samsung   | MZVLB1T0HALR-00000 | 1 TB   | 630386E00F6E | 138   | 0     | 0.38   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 68E87177F30A | 138   | 0     | 0.38   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 3FD0EB78C1F7 | 138   | 0     | 0.38   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 9F3BEC33E29F | 138   | 0     | 0.38   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | E6B50064E0BF | 138   | 0     | 0.38   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 6D70A9561DA0 | 138   | 0     | 0.38   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | F8AA0ECABA3B | 138   | 0     | 0.38   |
| Samsung   | SSD 970 PRO        | 1 TB   | CE2C0D23D13E | 137   | 0     | 0.38   |
| Intel     | SSDPECME016T4      | 800 GB | A3B547AF14EA | 137   | 0     | 0.38   |
| Samsung   | MZPLJ3T2HBJR-00007 | 3.2 TB | B80050098A9B | 136   | 0     | 0.37   |
| Dell      | Express Flash C... | 960 GB | B63B4137CF41 | 136   | 0     | 0.37   |
| Kingston  | SKC2500M8250G      | 250 GB | B44E1F7029FB | 136   | 0     | 0.37   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 351BF5286A8F | 136   | 0     | 0.37   |
| Intel     | SSDPE21D480GA      | 480 GB | 085BA76EE2C4 | 136   | 0     | 0.37   |
| WDC       | PC SN520 SDAPNU... | 128 GB | 6A7DD3484C41 | 136   | 0     | 0.37   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 2B559F3CD41F | 136   | 0     | 0.37   |
| Micron    | 9300_MTFDHAL3T2TDR | 3.2 TB | FB8B4757E43A | 136   | 0     | 0.37   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | D278CE46F776 | 136   | 0     | 0.37   |
| Samsung   | MZ4LB3T8HALS-00003 | 3.8 TB | 4CBA648E3739 | 136   | 0     | 0.37   |
| Wester... | WUS4BB038D7P3E1    | 3.8 TB | AC099C036056 | 136   | 0     | 0.37   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 1CA53AFF3B33 | 135   | 0     | 0.37   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 206FABC8B692 | 135   | 0     | 0.37   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 8BD64948050C | 135   | 0     | 0.37   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | ADBB5D6A23FF | 135   | 0     | 0.37   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | D59F917F2A52 | 135   | 0     | 0.37   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | DF798323621D | 135   | 0     | 0.37   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | F7A7E1422074 | 135   | 0     | 0.37   |
| Intel     | SSDPE2KX010T8      | 1 TB   | 7A3C83CA7BD8 | 135   | 0     | 0.37   |
| Samsung   | MZVLB1T0HBLR-00000 | 1 TB   | EEB82474E226 | 135   | 0     | 0.37   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 3A4D3D9CEC42 | 135   | 0     | 0.37   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | B8D7DE64A11F | 135   | 0     | 0.37   |
| Intel     | SSDPE2KX010T8      | 1 TB   | 1DD1AB78F167 | 135   | 0     | 0.37   |
| Samsung   | SSD 970 PRO        | 512 GB | 0111DFE743ED | 134   | 0     | 0.37   |
| Micron    | 9300_MTFDHAL7T6TDP | 7.6 TB | 3A4DCA941298 | 134   | 0     | 0.37   |
| Micron    | 9300_MTFDHAL7T6TDP | 7.6 TB | 8A1B90A91A39 | 134   | 0     | 0.37   |
| Micron    | 9300_MTFDHAL7T6TDP | 7.6 TB | 94C69DE7C6BD | 134   | 0     | 0.37   |
| Intel     | SSDPE21D480GA      | 480 GB | 0073786B5BF1 | 134   | 0     | 0.37   |
| Intel     | SSDPE21D480GA      | 480 GB | 02B7F533EC65 | 134   | 0     | 0.37   |
| Intel     | SSDPE21D480GA      | 480 GB | 0689D7F4E46A | 134   | 0     | 0.37   |
| Intel     | SSDPE21D480GA      | 480 GB | 208BE5BE7EE5 | 134   | 0     | 0.37   |
| Intel     | SSDPE21D480GA      | 480 GB | 25379CBBA59E | 134   | 0     | 0.37   |
| Intel     | SSDPE21D480GA      | 480 GB | 285920886313 | 134   | 0     | 0.37   |
| Intel     | SSDPE21D480GA      | 480 GB | 2FED527A2E49 | 134   | 0     | 0.37   |
| Intel     | SSDPE21D480GA      | 480 GB | 32F034447BF7 | 134   | 0     | 0.37   |
| Intel     | SSDPE21D480GA      | 480 GB | 6C4E4DC4F473 | 134   | 0     | 0.37   |
| Intel     | SSDPE21D480GA      | 480 GB | 6F607A9789AB | 134   | 0     | 0.37   |
| Intel     | SSDPE21D480GA      | 480 GB | 741C3DE9FAB0 | 134   | 0     | 0.37   |
| Intel     | SSDPE21D480GA      | 480 GB | 771D71B51CA3 | 134   | 0     | 0.37   |
| Intel     | SSDPE21D480GA      | 480 GB | 79472BA94FEF | 134   | 0     | 0.37   |
| Intel     | SSDPE21D480GA      | 480 GB | 91877AC10811 | 134   | 0     | 0.37   |
| Intel     | SSDPE21D480GA      | 480 GB | 92B03A267971 | 134   | 0     | 0.37   |
| Intel     | SSDPE21D480GA      | 480 GB | A68133DA0102 | 134   | 0     | 0.37   |
| Intel     | SSDPE21D480GA      | 480 GB | AEA666F54C8E | 134   | 0     | 0.37   |
| Intel     | SSDPE21D480GA      | 480 GB | BE30F489DA3C | 134   | 0     | 0.37   |
| Intel     | SSDPE21D480GA      | 480 GB | C053E8C85BAB | 134   | 0     | 0.37   |
| Intel     | SSDPE21D480GA      | 480 GB | FE7DE529F4AE | 134   | 0     | 0.37   |
| Intel     | SSDPE21D480GA      | 480 GB | 2BDD803E505E | 134   | 0     | 0.37   |
| Intel     | SSDPE21D480GA      | 480 GB | 3E270C81566A | 134   | 0     | 0.37   |
| Intel     | SSDPE21D480GA      | 480 GB | 672DD0883EC0 | 134   | 0     | 0.37   |
| Intel     | SSDPE21D480GA      | 480 GB | 83E657CA741D | 134   | 0     | 0.37   |
| Intel     | SSDPE21D480GA      | 480 GB | E372036D27CC | 134   | 0     | 0.37   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 2459679C5423 | 134   | 0     | 0.37   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | E592336CD23C | 134   | 0     | 0.37   |
| Samsung   | MZVLB256HBHQ-00000 | 256 GB | 843F7FF86EF9 | 134   | 0     | 0.37   |
| Samsung   | MZPJB960HMGC-0BW07 | 960 GB | 781B1C455CB5 | 134   | 0     | 0.37   |
| Micron    | 2200_MTFDHBA512TCK | 512 GB | 9439461EDE27 | 133   | 0     | 0.37   |
| Intel     | SSDPEKKA512G8      | 512 GB | EBF6091DB845 | 133   | 0     | 0.37   |
| Samsung   | SSD 970 PRO        | 512 GB | 17BB8BF45F7B | 133   | 0     | 0.37   |
| Samsung   | SSD 970 EVO Plus   | 250 GB | 5E684A68EC73 | 133   | 0     | 0.36   |
| WDC       | WDS100T3X0C-00SJG0 | 1 TB   | 5E9C43921354 | 132   | 0     | 0.36   |
| WDC       | WDS100T3X0C-00SJG0 | 1 TB   | 53C7554B6974 | 132   | 0     | 0.36   |
| Micron    | 2200_MTFDHBA512TCK | 512 GB | B242A6916E08 | 132   | 0     | 0.36   |
| Samsung   | SSD 970 PRO        | 512 GB | C87400DAAC7B | 132   | 0     | 0.36   |
| Wester... | WUS4BB076D7P3E1    | 7.6 TB | 0362282AE506 | 132   | 0     | 0.36   |
| Wester... | WUS4BB076D7P3E1    | 7.6 TB | 4A6B953EBA6F | 132   | 0     | 0.36   |
| Wester... | WUS4BB076D7P3E1    | 7.6 TB | 4E5181609E7C | 132   | 0     | 0.36   |
| Intel     | SSDPE21D480GA      | 480 GB | 29F8EF71027A | 131   | 0     | 0.36   |
| Intel     | SSDPE21D480GA      | 480 GB | 7B9C1DADA176 | 131   | 0     | 0.36   |
| Intel     | SSDPE21D480GA      | 480 GB | 9AADA19FC60D | 131   | 0     | 0.36   |
| Intel     | SSDPE21D480GA      | 480 GB | E0B593250F28 | 131   | 0     | 0.36   |
| WDC       | WDS100T3X0C-00SJG0 | 1 TB   | 52B884F8D24D | 131   | 0     | 0.36   |
| WDC       | WDS100T3X0C-00SJG0 | 1 TB   | 47C305ADEC57 | 131   | 0     | 0.36   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 708ED2584873 | 131   | 0     | 0.36   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | F628CE6ACEAC | 131   | 0     | 0.36   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 8C10EE69B253 | 131   | 0     | 0.36   |
| Intel     | SSDPE21D480GA      | 480 GB | B4DD6D133F06 | 130   | 0     | 0.36   |
| Samsung   | MZPLJ6T4HALA-00007 | 6.4 TB | 6D6EBCE3DB66 | 130   | 0     | 0.36   |
| Intel     | SSDPE2MD800G4      | 800 GB | 8AFE26CDD3F9 | 130   | 0     | 0.36   |
| WDC       | WDS100T3X0C-00SJG0 | 1 TB   | FB148756EAEF | 130   | 0     | 0.36   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 66856A60C627 | 130   | 0     | 0.36   |
| WDC       | WDS100T3X0C-00SJG0 | 1 TB   | D79C1820D003 | 130   | 0     | 0.36   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 1CC9F6D2444E | 130   | 0     | 0.36   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | BC16180C7CEB | 130   | 0     | 0.36   |
| Toshiba   | KXG60ZNV256G       | 256 GB | 6FCCE0753DDE | 130   | 0     | 0.36   |
| Toshiba   | KXG60ZNV256G       | 256 GB | DEB2DD247841 | 129   | 0     | 0.35   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 2C13CFD087E5 | 129   | 0     | 0.35   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 7706B5BC9EFB | 128   | 0     | 0.35   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | 77920735D776 | 128   | 0     | 0.35   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 5069CCA85F84 | 128   | 0     | 0.35   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | B6DE5C9CFA46 | 128   | 0     | 0.35   |
| WDC       | WDS250G3X0C-00SJG0 | 250 GB | 78C7833EE3A0 | 128   | 0     | 0.35   |
| Dell      | Express Flash P... | 1.6 TB | 512D89B6D542 | 128   | 0     | 0.35   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 32E630BA1103 | 128   | 0     | 0.35   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 45AA6B5C0F74 | 128   | 0     | 0.35   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 8143A2B6153E | 128   | 0     | 0.35   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 8ED305BAD046 | 128   | 0     | 0.35   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | A1EA3B804A6D | 128   | 0     | 0.35   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | B3E64C379057 | 128   | 0     | 0.35   |
| WDC       | CL SN720 SDAQNT... | 512 GB | DE896F91EAB9 | 128   | 0     | 0.35   |
| Toshiba   | KXG60ZNV256G       | 256 GB | 299EEAC1E487 | 128   | 0     | 0.35   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 11257CD75DF7 | 128   | 0     | 0.35   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | D6FDDF479783 | 128   | 0     | 0.35   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 4E7E11038BC8 | 128   | 0     | 0.35   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 1D89DDFB2680 | 127   | 0     | 0.35   |
| Samsung   | MZVLB512HAJQ-00000 | 512 GB | 8C99E5D77A41 | 127   | 0     | 0.35   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 2322C0D32589 | 127   | 0     | 0.35   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 333E5C6E2446 | 127   | 0     | 0.35   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 4F4AF95125D0 | 127   | 0     | 0.35   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 59448C5B9052 | 127   | 0     | 0.35   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 6588152B5470 | 127   | 0     | 0.35   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 7DDAAF0D24B4 | 127   | 0     | 0.35   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 0A41F3FA7BD4 | 127   | 0     | 0.35   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 640D1EE12F87 | 127   | 0     | 0.35   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 6DCF4B6E12FF | 127   | 0     | 0.35   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 0D2579CA7115 | 127   | 0     | 0.35   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | C071F3A22EB4 | 127   | 0     | 0.35   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | F6B417C48DE3 | 127   | 0     | 0.35   |
| Dell      | Ent NVMe P5600 ... | 1.6 TB | A56435BA7DE9 | 127   | 0     | 0.35   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 25DA16D9642C | 127   | 0     | 0.35   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | ED3CD45B7D23 | 127   | 0     | 0.35   |
| Dell      | Ent NVMe P5600 ... | 1.6 TB | A7E9B94D4669 | 127   | 0     | 0.35   |
| Samsung   | SSD 970 PRO        | 512 GB | 1346C1DEEB31 | 127   | 0     | 0.35   |
| Samsung   | SSD 970 EVO Plus   | 1 TB   | D52A736F2069 | 127   | 0     | 0.35   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | BF9B7A9F102A | 126   | 0     | 0.35   |
| WDC       | WUS4BB019D7P3E3    | 1.9 TB | 5E74EFA513F5 | 126   | 0     | 0.35   |
| Samsung   | MZVLB256HBHQ-00000 | 256 GB | E5A3E510BE4A | 126   | 0     | 0.35   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | DEB95BACB10A | 126   | 0     | 0.35   |
| WDC       | WUS4BB019D7P3E3    | 1.9 TB | 94C188EAA630 | 125   | 0     | 0.34   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 9F397828E9FE | 125   | 0     | 0.34   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 4E462A083BDD | 125   | 0     | 0.34   |
| WDC       | CL SN720 SDAQNT... | 1 TB   | F9464CB78999 | 125   | 0     | 0.34   |
| Samsung   | MZPLJ6T4HALA-00007 | 6.4 TB | 0D7CFE1EBF29 | 125   | 0     | 0.34   |
| Samsung   | SSD 970 PRO        | 512 GB | 909F892C64A3 | 125   | 0     | 0.34   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 441E84F70322 | 125   | 0     | 0.34   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 1CFA53E75A42 | 125   | 0     | 0.34   |
| Samsung   | MZVLB1T0HBLR-00000 | 1 TB   | 973B21941582 | 125   | 0     | 0.34   |
| Samsung   | SSD 970 PRO        | 512 GB | B822B24D2E0F | 125   | 0     | 0.34   |
| Toshiba   | KXG60PNV2T04       | 2 TB   | BE48AEDD3F4C | 125   | 0     | 0.34   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 5B24EEBC12EA | 124   | 0     | 0.34   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | ABD5AA249FF9 | 124   | 0     | 0.34   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 1DA2922E2F4A | 124   | 0     | 0.34   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | FA7A55B0F1A0 | 124   | 0     | 0.34   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | F451A2AAB927 | 124   | 0     | 0.34   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 078FC9CD16B0 | 124   | 0     | 0.34   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 7B4A02A6AD86 | 124   | 0     | 0.34   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 810308F3770F | 124   | 0     | 0.34   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 99653C3EFDC2 | 124   | 0     | 0.34   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | A08D98E265DE | 124   | 0     | 0.34   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | A85D783488F7 | 124   | 0     | 0.34   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | C4D851EFF926 | 124   | 0     | 0.34   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | FF33A96A8327 | 124   | 0     | 0.34   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 009545034B01 | 124   | 0     | 0.34   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | BA752E27CD6C | 124   | 0     | 0.34   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 1018CAB17D9E | 124   | 0     | 0.34   |
| Samsung   | SSD 970 PRO        | 512 GB | 1A9F4C13A4A4 | 124   | 0     | 0.34   |
| Samsung   | SSD 970 EVO        | 500 GB | DFBF87CED60C | 123   | 0     | 0.34   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 7494EC39ECB1 | 123   | 0     | 0.34   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | A4240F317103 | 123   | 0     | 0.34   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 409DDE1A8844 | 123   | 0     | 0.34   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 53395B646543 | 123   | 0     | 0.34   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | A2655F8372A1 | 123   | 0     | 0.34   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | F35972C723C6 | 123   | 0     | 0.34   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | FC44F49526B3 | 123   | 0     | 0.34   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 056C56B7D3CE | 123   | 0     | 0.34   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 49CB5EB62924 | 123   | 0     | 0.34   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 54EE46BBED69 | 123   | 0     | 0.34   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 9C6CCCA36728 | 123   | 0     | 0.34   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | A67E90093BA1 | 123   | 0     | 0.34   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | DEF3A60A1CF1 | 123   | 0     | 0.34   |
| Samsung   | MZVLB256HAHQ-00000 | 256 GB | 28865690C326 | 122   | 0     | 0.34   |
| Dell      | Ent NVMe P5600 ... | 1.6 TB | 4A480D98D3D6 | 122   | 0     | 0.34   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | 5109B6086473 | 122   | 0     | 0.34   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | 64A879F5DE5D | 122   | 0     | 0.34   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 0E594E5663C9 | 122   | 0     | 0.34   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | D152FD81EF63 | 122   | 0     | 0.34   |
| Toshiba   | KXG60ZNV256G       | 256 GB | B202D57E068B | 122   | 0     | 0.34   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 061C2D8A88FB | 122   | 0     | 0.34   |
| Micron    | 9300_MTFDHAL3T2TDR | 3.2 TB | BBEC64F78B9D | 122   | 0     | 0.34   |
| Samsung   | MZVLB256HAHQ-00000 | 256 GB | A9536BD4265E | 122   | 0     | 0.33   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 5863A6DB64B5 | 122   | 0     | 0.33   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 7AFF81956B78 | 122   | 0     | 0.33   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | B90B31564B9B | 122   | 0     | 0.33   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | BDD20AC2DFBD | 122   | 0     | 0.33   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | D7D4BFC541AE | 122   | 0     | 0.33   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | EB989B16F4DA | 122   | 0     | 0.33   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 3C60156F48E9 | 121   | 0     | 0.33   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 59D1C93143EB | 121   | 0     | 0.33   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 23898AED4B61 | 121   | 0     | 0.33   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 5D6896E1A250 | 121   | 0     | 0.33   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 60C3D3DCD04C | 121   | 0     | 0.33   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 978D2A115D0D | 121   | 0     | 0.33   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | B3EF4C3AB851 | 121   | 0     | 0.33   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | D7D7BFAD0CCE | 121   | 0     | 0.33   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | E4B7801254B3 | 121   | 0     | 0.33   |
| Wester... | WUS4BB076D7P3E1    | 7.6 TB | 90A80060E5A8 | 121   | 0     | 0.33   |
| Wester... | WUS4BB076D7P3E1    | 7.6 TB | B422EC0507D8 | 121   | 0     | 0.33   |
| Kingston  | SEDC1000M3840G     | 3.8 TB | 7DC0460F2E7D | 121   | 0     | 0.33   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 9A469F836E08 | 121   | 0     | 0.33   |
| Wester... | WUS4CB016D7P3E3    | 1.9 TB | F8AE00472664 | 121   | 0     | 0.33   |
| WDC       | WUS4BB019D7P3E3    | 1.9 TB | F9E88CBE6F44 | 121   | 0     | 0.33   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 8CF164245AF5 | 121   | 0     | 0.33   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 3ADC83FEBCC7 | 121   | 0     | 0.33   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 1FA53A847BCF | 121   | 0     | 0.33   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 353820196378 | 121   | 0     | 0.33   |
| Toshiba   | KXD51RUE1T92       | 1.9 TB | 452E76AA469E | 120   | 0     | 0.33   |
| WDC       | WUS4BB019D7P3E3    | 1.9 TB | E2303BFF4F07 | 120   | 0     | 0.33   |
| Samsung   | MZPLJ6T4HALA-00007 | 6.4 TB | 43478AD61B9A | 120   | 0     | 0.33   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 93161FF85C26 | 120   | 0     | 0.33   |
| Dell      | Express Flash C... | 960 GB | 42B1C25B8F40 | 120   | 0     | 0.33   |
| Dell      | Express Flash P... | 1.6 TB | 725990366E79 | 119   | 0     | 0.33   |
| Intel     | SSDPE21D480GA      | 480 GB | 80449F2BA4BD | 119   | 0     | 0.33   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 0A1EC0C2C3F8 | 119   | 0     | 0.33   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 23AB30D8A727 | 119   | 0     | 0.33   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 4B1809C69FF5 | 119   | 0     | 0.33   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | FB29FCBFB4F0 | 119   | 0     | 0.33   |
| Samsung   | SSD 970 EVO Plus   | 250 GB | 682C369EE2BC | 119   | 0     | 0.33   |
| Micron    | 7300_MTFDHBE960TDF | 960 GB | 7438B35D4C26 | 119   | 0     | 0.33   |
| Micron    | 7300_MTFDHBE960TDF | 960 GB | 88F1B48EDE4B | 118   | 0     | 0.33   |
| Micron    | 7300_MTFDHBE960TDF | 960 GB | DA348E06B916 | 118   | 0     | 0.33   |
| Dell      | Express Flash C... | 960 GB | 76EB1711BAEC | 118   | 0     | 0.32   |
| Dell      | Express Flash C... | 960 GB | F7326E856681 | 118   | 0     | 0.32   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 2E7860663889 | 118   | 0     | 0.32   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 90F5A07A328B | 118   | 0     | 0.32   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 1D7A66E7E9D3 | 118   | 0     | 0.32   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 784DFD991C90 | 118   | 0     | 0.32   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 0205D60252FE | 118   | 0     | 0.32   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 4BF1DA5106E0 | 118   | 0     | 0.32   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 8E1931016CD1 | 118   | 0     | 0.32   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | A2C51B88B3BA | 118   | 0     | 0.32   |
| Dell      | Express Flash P... | 1.6 TB | D21C7D7C6160 | 117   | 0     | 0.32   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 485BB2386667 | 117   | 0     | 0.32   |
| WDC       | WUS4BB019D7P3E3    | 1.9 TB | 4373465365B5 | 117   | 0     | 0.32   |
| WDC       | CL SN720 SDAQNT... | 1 TB   | FEED3B2967E4 | 117   | 0     | 0.32   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 3AB46CEF4C55 | 117   | 0     | 0.32   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 5D047D168C80 | 117   | 0     | 0.32   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 61AE2362061A | 117   | 0     | 0.32   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 7A25EC15CA4A | 117   | 0     | 0.32   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | B9FE2B800E2D | 117   | 0     | 0.32   |
| Intel     | SSDPECME016T4      | 800 GB | 89638043B889 | 117   | 0     | 0.32   |
| Samsung   | SSD 970 PRO        | 512 GB | E067C9B7A65B | 117   | 0     | 0.32   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | B929750313B0 | 117   | 0     | 0.32   |
| WDC       | WUS4BB019D7P3E3    | 1.9 TB | 806A92E6E2CD | 117   | 0     | 0.32   |
| Samsung   | SSD 970 PRO        | 512 GB | 45F5B30EC598 | 116   | 0     | 0.32   |
| Samsung   | SSD 970 PRO        | 512 GB | F5DBF1E52820 | 116   | 0     | 0.32   |
| WDC       | PC SN520 SDAPNU... | 128 GB | D89405ED30F1 | 116   | 0     | 0.32   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 03420E40566C | 116   | 0     | 0.32   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | BFA5BDBAF863 | 116   | 0     | 0.32   |
| Micron    | 9300_MTFDHAL3T2TDR | 3.2 TB | F0E371B5D267 | 116   | 0     | 0.32   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | EC070C415F65 | 116   | 0     | 0.32   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 055475263601 | 116   | 0     | 0.32   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | CFF7A9C89910 | 116   | 0     | 0.32   |
| WDC       | CL SN720 SDAQNT... | 512 GB | 2935AE672BAE | 116   | 0     | 0.32   |
| Wester... | WUS4BB038D7P3E3    | 3.8 TB | 61D5C59C041D | 116   | 0     | 0.32   |
| Dell      | Express Flash N... | 1.6 TB | 585795562D16 | 116   | 0     | 0.32   |
| Samsung   | SSD 970 PRO        | 512 GB | 133CFEFD75F9 | 115   | 0     | 0.32   |
| HP        | SSD EX950          | 512 GB | E469C0F0D545 | 115   | 0     | 0.32   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 61144ED35E61 | 115   | 0     | 0.32   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | B8898E31B14D | 115   | 0     | 0.32   |
| Kingston  | SEDC1000M960G      | 960 GB | 4F07938D8957 | 115   | 0     | 0.32   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 91EB4F67406A | 115   | 0     | 0.32   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 08698959DE8A | 115   | 0     | 0.32   |
| Dell      | Ent NVMe P5600 ... | 1.6 TB | E76A305790DC | 115   | 0     | 0.32   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 41016EDBBF5D | 115   | 0     | 0.32   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 59EA4FE09956 | 115   | 0     | 0.32   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | 8F58743BC9AD | 115   | 0     | 0.32   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | A4EC9D9CD6F5 | 115   | 0     | 0.32   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | C2608685EBEF | 115   | 0     | 0.32   |
| Samsung   | SSD 970 EVO Plus   | 500 GB | 91361900B5E9 | 115   | 0     | 0.32   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | B7D7261B6A0C | 114   | 0     | 0.31   |
| Wester... | WUS4BB076D7P3E1    | 7.6 TB | 59FE2E84DBB7 | 114   | 0     | 0.31   |
| Kingston  | SKC2500M8250G      | 250 GB | 3F044C7B2A05 | 114   | 0     | 0.31   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 44C3C8D2FBDF | 114   | 0     | 0.31   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 8A1DCA713079 | 114   | 0     | 0.31   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | EF1424CB65E3 | 114   | 0     | 0.31   |
| Samsung   | MZPJB960HMGC-0BW07 | 960 GB | B2CD714DF85F | 114   | 0     | 0.31   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 6428D84AC52A | 114   | 0     | 0.31   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | FAD48CF94B05 | 114   | 0     | 0.31   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 328A3E8A86CA | 114   | 0     | 0.31   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 72BCBA292CBD | 114   | 0     | 0.31   |
| Wester... | WUS4BB038D7P3E3    | 3.8 TB | 355FFB3226DC | 114   | 0     | 0.31   |
| Wester... | WUS4BB038D7P3E3    | 3.8 TB | 7821D2D3886E | 114   | 0     | 0.31   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 4CC3CB54060B | 114   | 0     | 0.31   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | F8FD12CB44AC | 114   | 0     | 0.31   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | FAC99B2C99E3 | 114   | 0     | 0.31   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | AE4F9B334BFC | 113   | 0     | 0.31   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | F0EB2A04E326 | 113   | 0     | 0.31   |
| ADATA     | SX8200PNP          | 512 GB | 8D7CCDEFCCCF | 113   | 0     | 0.31   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 5B168E4F8DE6 | 113   | 0     | 0.31   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 89A23F14BD29 | 113   | 0     | 0.31   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 8AC3862C1A2E | 113   | 0     | 0.31   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 77E9B60511FC | 113   | 0     | 0.31   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | BDA7557233DB | 113   | 0     | 0.31   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | E48F6017A163 | 112   | 0     | 0.31   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 35B5A1F05B5C | 112   | 0     | 0.31   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 7A0F67312D54 | 112   | 0     | 0.31   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | BD96DE9A4137 | 112   | 0     | 0.31   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | DC5F968B1F28 | 112   | 0     | 0.31   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 42071D0AD365 | 111   | 0     | 0.31   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 56BED58EB169 | 111   | 0     | 0.31   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | C9A0F289E154 | 111   | 0     | 0.31   |
| Samsung   | SSD 970 PRO        | 512 GB | 7E516DECA5CD | 111   | 0     | 0.31   |
| Samsung   | SSD 970 EVO Plus   | 250 GB | 99E505C842B4 | 111   | 0     | 0.31   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 3847D5BB30D5 | 111   | 0     | 0.31   |
| Intel     | SSDPE2KX020T8      | 2 TB   | 3DB07178A358 | 111   | 0     | 0.31   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | DC501A65C512 | 111   | 0     | 0.31   |
| Wester... | WUS4BB038D7P3E3    | 3.8 TB | 83F13596B0CA | 111   | 0     | 0.31   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 287647691108 | 111   | 0     | 0.31   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 2E2AAC3D02FB | 111   | 0     | 0.31   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 58974B2425D2 | 111   | 0     | 0.31   |
| WDC       | CL SN720 SDAQNT... | 512 GB | 8C976B16A1E4 | 111   | 0     | 0.30   |
| WDC       | CL SN720 SDAQNT... | 1 TB   | 68E49A07C6AA | 111   | 0     | 0.30   |
| Kingston  | SA2000M8500G       | 500 GB | 1E858975C571 | 111   | 0     | 0.30   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 0F67DF457F8B | 110   | 0     | 0.30   |
| Toshiba   | KXG60ZNV256G       | 256 GB | 0AC7D2A6AA12 | 110   | 0     | 0.30   |
| Samsung   | SSD 970 EVO Plus   | 500 GB | A656B52E46F3 | 109   | 0     | 0.30   |
| WDC       | CL SN720 SDAQNT... | 1 TB   | 332730B6569B | 109   | 0     | 0.30   |
| Wester... | WUS4BB096D7P3E1    | 720 GB | DBE2D65C6844 | 109   | 0     | 0.30   |
| KIOXIA    | KCD6XLUL1T92       | 1.9 TB | 4844F47D55F3 | 109   | 0     | 0.30   |
| KIOXIA    | KCD6XLUL1T92       | 1.9 TB | 4A6CEBF6F705 | 109   | 0     | 0.30   |
| KIOXIA    | KCD6XLUL1T92       | 1.9 TB | 69A312A3B4E5 | 109   | 0     | 0.30   |
| KIOXIA    | KCD6XLUL1T92       | 1.9 TB | 7828B0C8172B | 109   | 0     | 0.30   |
| KIOXIA    | KCD6XLUL1T92       | 1.9 TB | 7FD35E4386F4 | 109   | 0     | 0.30   |
| KIOXIA    | KCD6XLUL1T92       | 1.9 TB | 883C3F1D1FB7 | 109   | 0     | 0.30   |
| KIOXIA    | KCD6XLUL1T92       | 1.9 TB | 9B78C0ED7D67 | 109   | 0     | 0.30   |
| KIOXIA    | KCD6XLUL1T92       | 1.9 TB | 9FA143B37932 | 109   | 0     | 0.30   |
| KIOXIA    | KCD6XLUL1T92       | 1.9 TB | E6948163ACC0 | 109   | 0     | 0.30   |
| KIOXIA    | KCD6XLUL1T92       | 1.9 TB | FA5C22511462 | 109   | 0     | 0.30   |
| Samsung   | MZPLL1T6HAJQ-00005 | 1.6 TB | 846A02773DCE | 109   | 0     | 0.30   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 5822377A171D | 109   | 0     | 0.30   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 2121BC6F8CB9 | 109   | 0     | 0.30   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 81BD50D94D72 | 109   | 0     | 0.30   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | E3A3F92F4086 | 109   | 0     | 0.30   |
| Toshiba   | KXG60ZNV256G       | 256 GB | 0A3CEB2C9E52 | 109   | 0     | 0.30   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 1B62BAC4880F | 109   | 0     | 0.30   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 6F9EAC5C68A3 | 109   | 0     | 0.30   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | AA03A9024EDC | 109   | 0     | 0.30   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 0DD7A3FCBCFA | 109   | 0     | 0.30   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | FF13877CFB85 | 109   | 0     | 0.30   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | ACB20218CD05 | 109   | 0     | 0.30   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 821D43A8F98E | 108   | 0     | 0.30   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 1501BD206141 | 108   | 0     | 0.30   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 6B666E803171 | 108   | 0     | 0.30   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 1501BD206141 | 108   | 0     | 0.30   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 6B666E803171 | 108   | 0     | 0.30   |
| Huawei    | HWE52P431T6M002N   | 1.6 TB | 6747A495F05E | 108   | 0     | 0.30   |
| Huawei    | HWE52P431T6M002N   | 1.6 TB | B286640940E5 | 108   | 0     | 0.30   |
| WDC       | WUS4BB019D7P3E3    | 1.9 TB | C5C1C2DF48F4 | 108   | 0     | 0.30   |
| Samsung   | SSD 970 PRO        | 512 GB | 0CF96366385D | 108   | 0     | 0.30   |
| Samsung   | SSD 970 PRO        | 512 GB | C288E954B8CC | 108   | 0     | 0.30   |
| Crucial   | CT500P1SSD8        | 500 GB | CC94C64D92E7 | 108   | 0     | 0.30   |
| Samsung   | SSD 970 EVO        | 1 TB   | 80ACF9C2745F | 108   | 0     | 0.30   |
| Intel     | SSDPEKNW512G8      | 512 GB | 48FDADB5D397 | 108   | 0     | 0.30   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | EFA9957588B2 | 108   | 0     | 0.30   |
| Intel     | SSDPEL1K100GA      | 100 GB | 72773C71516F | 108   | 0     | 0.30   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 4308DF406AA5 | 107   | 0     | 0.30   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | FC03B7F56B7A | 107   | 0     | 0.30   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 69CDF882C4ED | 107   | 0     | 0.30   |
| WDC       | CL SN720 SDAQNT... | 1 TB   | C48B67F1BFC1 | 107   | 0     | 0.30   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 878A0C1B5A64 | 107   | 0     | 0.29   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 97F4493C247D | 107   | 0     | 0.29   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 546ACC525658 | 107   | 0     | 0.29   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | DFDA755CD171 | 107   | 0     | 0.29   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 6873A7E3B98B | 106   | 0     | 0.29   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 807DD0CD2A97 | 106   | 0     | 0.29   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | B529697CB937 | 106   | 0     | 0.29   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | EA073EFBA224 | 106   | 0     | 0.29   |
| Wester... | WUS4BB096D7P3E1    | 720 GB | 666FD2B76B0E | 106   | 0     | 0.29   |
| Samsung   | SSD 970 PRO        | 512 GB | E4E68643D3A4 | 106   | 0     | 0.29   |
| Dell      | Ent NVMe P5600 ... | 1.6 TB | 64B4BF79B7FC | 106   | 0     | 0.29   |
| Micron    | 2200_MTFDHBA512TCK | 512 GB | 6D9973163EAA | 106   | 0     | 0.29   |
| Micron    | 7300_MTFDHBE960TDF | 960 GB | 69287A11DF22 | 106   | 0     | 0.29   |
| Samsung   | SSD 970 EVO        | 1 TB   | 4B0AC633EBFE | 105   | 0     | 0.29   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 7435334224D9 | 105   | 0     | 0.29   |
| Wester... | WUS4BB096D7P3E1    | 720 GB | 711A73C0E9AE | 105   | 0     | 0.29   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 7435334224D9 | 105   | 0     | 0.29   |
| Intel     | SSDPEKNW010T8      | 1 TB   | 71AFF92DBDD6 | 105   | 0     | 0.29   |
| Wester... | WUS4BB096D7P3E1    | 720 GB | E8E64D4136C9 | 105   | 0     | 0.29   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | FFC965A68979 | 105   | 0     | 0.29   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 25BBCC678905 | 105   | 0     | 0.29   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | BC848A49CAB9 | 105   | 0     | 0.29   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 8FC2B787AA70 | 105   | 0     | 0.29   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | FD428012F080 | 105   | 0     | 0.29   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 7E9103996900 | 105   | 0     | 0.29   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | C88B20B62DB0 | 105   | 0     | 0.29   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | D2178F4B3237 | 105   | 0     | 0.29   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 11B6942193E3 | 105   | 0     | 0.29   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 5AC8BFA1E648 | 105   | 0     | 0.29   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | F037994F01D1 | 105   | 0     | 0.29   |
| WDC       | WDS250G2B0C-00PXH0 | 250 GB | 3DA1311D3771 | 105   | 0     | 0.29   |
| Dell      | Express Flash P... | 1.6 TB | F84825B5CEAC | 105   | 0     | 0.29   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 256B17C8D0ED | 104   | 0     | 0.29   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 24B3100ED50C | 104   | 0     | 0.29   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 256B17C8D0ED | 104   | 0     | 0.29   |
| WDC       | WUS4BB038D7P3E1    | 3.8 TB | 966EE7F47F57 | 104   | 0     | 0.29   |
| Wester... | WUS4BB038D7P3E3    | 3.8 TB | C31A247DB903 | 104   | 0     | 0.29   |
| Kingston  | SEDC1000M3840G     | 3.8 TB | 5D68B40E1CFB | 104   | 0     | 0.29   |
| WDC       | WUS4BB038D7P3E3    | 3.8 TB | 2366670FE987 | 104   | 0     | 0.29   |
| WDC       | WUS4BB038D7P3E3    | 3.8 TB | 7FD350685A8D | 104   | 0     | 0.29   |
| WDC       | WUS4BB038D7P3E3    | 3.8 TB | D325AEB7D2DD | 104   | 0     | 0.29   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 3C2C18F35B69 | 104   | 0     | 0.29   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 4EEF721B2774 | 104   | 0     | 0.29   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 86D0336DBDD2 | 104   | 0     | 0.29   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 4CB33CC8D894 | 104   | 0     | 0.28   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | BE6FC7C4DCC8 | 103   | 0     | 0.28   |
| Kingston  | SKC2500M81000G     | 1 TB   | BA01AB56F195 | 103   | 0     | 0.28   |
| Samsung   | SSD 970 PRO        | 512 GB | 24203E093575 | 103   | 0     | 0.28   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 052F7DCB4D0F | 103   | 0     | 0.28   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 0A6B41EB859B | 103   | 0     | 0.28   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 319DACF85B27 | 103   | 0     | 0.28   |
| Intel     | SSDPE21K375GA      | 375 GB | 07B4FFA582CA | 103   | 0     | 0.28   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 2380218E8638 | 103   | 0     | 0.28   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 2CCC45881FEA | 103   | 0     | 0.28   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 43BB2A9BD2FB | 103   | 0     | 0.28   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 57D2C0CF65E9 | 103   | 0     | 0.28   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | C7412047B98E | 103   | 0     | 0.28   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 2383CA737597 | 103   | 0     | 0.28   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 4696C13BC22C | 103   | 0     | 0.28   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | BA9A7307F58A | 103   | 0     | 0.28   |
| WDC       | PC SN530 NVMe      | 512 GB | 20D4BB9CACEE | 103   | 0     | 0.28   |
| Intel     | SSDPE21K375GA      | 375 GB | B1295CC68B98 | 102   | 0     | 0.28   |
| Toshiba   | KXG60ZNV256G       | 256 GB | AB4FA55CEAAA | 102   | 0     | 0.28   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 588CA4643645 | 102   | 0     | 0.28   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 588CA4643645 | 102   | 0     | 0.28   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 12164B757DCB | 102   | 0     | 0.28   |
| Intel     | SSDPEKNW010T8      | 1 TB   | 2787850BF7EF | 102   | 0     | 0.28   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | 0AE4C5BC8E08 | 102   | 0     | 0.28   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | 18A5D9C6D05B | 102   | 0     | 0.28   |
| Toshiba   | KXG60ZNV256G       | 256 GB | 309733D28D11 | 102   | 0     | 0.28   |
| Samsung   | MZVLB1T0HBLR-00000 | 1 TB   | 87D0FBB2B322 | 102   | 0     | 0.28   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 75B02F5739D3 | 102   | 0     | 0.28   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 775FD26C006A | 102   | 0     | 0.28   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | 10904DA0A782 | 102   | 0     | 0.28   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 95C8609ED5B1 | 102   | 0     | 0.28   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 95C8609ED5B1 | 102   | 0     | 0.28   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 9DBD5C4DD039 | 102   | 0     | 0.28   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | BEEC969A67BE | 101   | 0     | 0.28   |
| Samsung   | SSD 970 PRO        | 512 GB | A66059E9A1EE | 101   | 0     | 0.28   |
| Kingston  | SA2000M8250G       | 250 GB | D407BF2A22E4 | 101   | 0     | 0.28   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 261B3185ECC5 | 101   | 0     | 0.28   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 862CFAD5E8A1 | 101   | 0     | 0.28   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | A6B18569D9F5 | 101   | 0     | 0.28   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 58AED049F1AB | 101   | 0     | 0.28   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 9D70E90F851B | 101   | 0     | 0.28   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | D19355E29D3D | 101   | 0     | 0.28   |
| Samsung   | SSD 970 PRO        | 512 GB | 072DAA81D5B9 | 100   | 0     | 0.28   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 0A589E97DE86 | 100   | 0     | 0.28   |
| Samsung   | MZVLB512HAJQ-00000 | 512 GB | B223C8D13ADE | 302   | 2     | 0.28   |
| Samsung   | SSD 970 PRO        | 512 GB | 514D60498F84 | 100   | 0     | 0.28   |
| Samsung   | MZVLB512HBJQ-00000 | 512 GB | 05446E8E85DE | 100   | 0     | 0.27   |
| Toshiba   | KXG60ZNV1T02       | 1 TB   | C91323B50070 | 99    | 0     | 0.27   |
| Samsung   | SSD 970 PRO        | 512 GB | A189DB42FA86 | 99    | 0     | 0.27   |
| Samsung   | SSD 970 PRO        | 512 GB | 847B84D1A07F | 99    | 0     | 0.27   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 3F6D7854876E | 98    | 0     | 0.27   |
| Dell      | Express Flash N... | 6.4 TB | 52226F18767B | 98    | 0     | 0.27   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | D8F117E92911 | 98    | 0     | 0.27   |
| Samsung   | SSD 970 EVO Plus   | 250 GB | 6F8885DF04FB | 98    | 0     | 0.27   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 3A7579F282DF | 98    | 0     | 0.27   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 63B47CAFFB9A | 98    | 0     | 0.27   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 9B98F7CD6738 | 98    | 0     | 0.27   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | D83D8D540E01 | 98    | 0     | 0.27   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | DDB013511E23 | 98    | 0     | 0.27   |
| Toshiba   | KXG60ZNV256G       | 256 GB | 39931D9B2CC1 | 98    | 0     | 0.27   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | F96C3DE40505 | 98    | 0     | 0.27   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | B8A0F9E957C9 | 98    | 0     | 0.27   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | C6DEE07A37EE | 98    | 0     | 0.27   |
| Samsung   | SSD 970 EVO Plus   | 250 GB | 1DD4535ED40A | 98    | 0     | 0.27   |
| Samsung   | SSD 970 EVO Plus   | 250 GB | 9C5E8FD19328 | 98    | 0     | 0.27   |
| Samsung   | SSD 970 EVO Plus   | 500 GB | B1D348ABB379 | 97    | 0     | 0.27   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 0F813B7DCAAD | 97    | 0     | 0.27   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 2BE57600130A | 97    | 0     | 0.27   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 5373E778C4A8 | 97    | 0     | 0.27   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 979BB29073C5 | 97    | 0     | 0.27   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | A1D50B1E2CBD | 97    | 0     | 0.27   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | C14A344E1914 | 97    | 0     | 0.27   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | DCC5C6CF8042 | 97    | 0     | 0.27   |
| Intel     | SSDPE21D480GA      | 480 GB | A70AE67FDA05 | 97    | 0     | 0.27   |
| HP        | SSD EX950          | 512 GB | D480CE8A1146 | 97    | 0     | 0.27   |
| Samsung   | SSD 970 PRO        | 512 GB | 9411F9235275 | 97    | 0     | 0.27   |
| Samsung   | SSD 970 PRO        | 1 TB   | B502C9B2E300 | 97    | 0     | 0.27   |
| Samsung   | MZQLB1T9HAJR-000AZ | 1.9 TB | 7A13BF6553DC | 96    | 0     | 0.26   |
| Samsung   | MZQL2960HCJR-00A07 | 960 GB | 8C29D73B0B7B | 96    | 0     | 0.26   |
| Samsung   | MZQL2960HCJR-00A07 | 960 GB | 9C2A2702067C | 96    | 0     | 0.26   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | C7909198ED39 | 96    | 0     | 0.26   |
| Wester... | WUS4BB076D7P3E1    | 7.6 TB | 6F546BE0AC37 | 96    | 0     | 0.26   |
| Wester... | WUS4BB076D7P3E1    | 7.6 TB | DEE8C3920760 | 96    | 0     | 0.26   |
| Wester... | WUS4BB076D7P3E1    | 7.6 TB | FDB17160CAFF | 96    | 0     | 0.26   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 8D9246FFAABE | 96    | 0     | 0.26   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | B836C120059F | 96    | 0     | 0.26   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 0656F876C770 | 96    | 0     | 0.26   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 49365F53A6ED | 96    | 0     | 0.26   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 8A002255E77C | 96    | 0     | 0.26   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | AD15CCCAFA61 | 96    | 0     | 0.26   |
| Samsung   | SSD 970 PRO        | 512 GB | 490223AD4ADB | 96    | 0     | 0.26   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 912E43B56063 | 96    | 0     | 0.26   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 92B78F1459F9 | 96    | 0     | 0.26   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 96E945E86995 | 96    | 0     | 0.26   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | BEC8E26C007D | 96    | 0     | 0.26   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 13269A57F9C3 | 95    | 0     | 0.26   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 32C6D26E1853 | 95    | 0     | 0.26   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 91A13BCC91DA | 95    | 0     | 0.26   |
| Samsung   | SSD 970 PRO        | 512 GB | 1C8B55EBC501 | 95    | 0     | 0.26   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | B50E937B0ED3 | 95    | 0     | 0.26   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 7604EAF00012 | 95    | 0     | 0.26   |
| WDC       | WDS500G3X0C-00SJG0 | 500 GB | F59FBBC908DF | 95    | 0     | 0.26   |
| WDC       | WDS250G2B0C-00PXH0 | 250 GB | 2DB6520CA03C | 95    | 0     | 0.26   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | ABC9BF716C83 | 95    | 0     | 0.26   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | ABC9BF716C83 | 95    | 0     | 0.26   |
| Micron    | 9300_MTFDHAL7T6TDP | 7.6 TB | 0DCDBD1AACB3 | 95    | 0     | 0.26   |
| Micron    | 9300_MTFDHAL7T6TDP | 7.6 TB | 7CA46F433477 | 95    | 0     | 0.26   |
| Micron    | 9300_MTFDHAL7T6TDP | 7.6 TB | B0CAD0446CB9 | 95    | 0     | 0.26   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 0FE356A0AA80 | 94    | 0     | 0.26   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 4E43E3CA7C33 | 94    | 0     | 0.26   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 528D2892192F | 94    | 0     | 0.26   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 79F977C44C39 | 94    | 0     | 0.26   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 7DD64693E82C | 94    | 0     | 0.26   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 929BB502AEBB | 94    | 0     | 0.26   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | D3EBD55D9FD0 | 94    | 0     | 0.26   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | EB94784A5A32 | 94    | 0     | 0.26   |
| Wester... | WUS4BB076D7P3E1    | 7.6 TB | 81C43BB7398D | 94    | 0     | 0.26   |
| Wester... | WUS4BB076D7P3E1    | 7.6 TB | DEC89BEF6FDC | 94    | 0     | 0.26   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 1FB9EFC23B33 | 94    | 0     | 0.26   |
| Micron    | 2200_MTFDHBA512TCK | 512 GB | DB6623075474 | 93    | 0     | 0.26   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 6F934D596923 | 93    | 0     | 0.26   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 825A954F2ACF | 93    | 0     | 0.26   |
| Dell      | Express Flash C... | 960 GB | 2177B7001656 | 93    | 0     | 0.26   |
| Intel     | SSDPE2KX010T8      | 1 TB   | 96E6A9169756 | 93    | 0     | 0.26   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 3FD4B3B385D1 | 93    | 0     | 0.26   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 4D9650971BD1 | 93    | 0     | 0.26   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 11BB9A62CD40 | 93    | 0     | 0.26   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 9F8D36B857C4 | 93    | 0     | 0.26   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 3F51A080C5A1 | 93    | 0     | 0.26   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 66E45723EBB6 | 93    | 0     | 0.26   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | A4CE09F600F9 | 93    | 0     | 0.26   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | CCEE374B32A9 | 93    | 0     | 0.26   |
| Samsung   | MZPLJ6T4HALA-00007 | 6.4 TB | 2596DFF5A8C9 | 93    | 0     | 0.26   |
| WDC       | WUS4BB019D7P3E3    | 1.9 TB | 2000AA1595ED | 93    | 0     | 0.26   |
| Micron    | 2200_MTFDHBA512TCK | 512 GB | D32C62773A14 | 93    | 0     | 0.26   |
| Micron    | 9300_MTFDHAL7T6TDP | 7.6 TB | 0D0F6D2BC50A | 93    | 0     | 0.26   |
| Micron    | 9300_MTFDHAL7T6TDP | 7.6 TB | A0AD621FAB99 | 93    | 0     | 0.26   |
| Micron    | 9300_MTFDHAL7T6TDP | 7.6 TB | EA579A539325 | 93    | 0     | 0.26   |
| Samsung   | SSD 970 EVO Plus   | 250 GB | A454C1261D40 | 93    | 0     | 0.26   |
| WDC       | WDS500G3XHC-00SJG0 | 500 GB | 67427C32915F | 93    | 0     | 0.26   |
| Dell      | Ent NVMe P5600 ... | 1.6 TB | 306B25C8C3CD | 93    | 0     | 0.26   |
| Micron    | 2200_MTFDHBA512TCK | 512 GB | 139266DD69BA | 93    | 0     | 0.26   |
| WDC       | CL SN720 SDAQNT... | 512 GB | 77F097A3CE81 | 92    | 0     | 0.25   |
| Intel     | SSDPEKKW512G8      | 512 GB | 7806DC3B51ED | 92    | 0     | 0.25   |
| Samsung   | SSD 970 PRO        | 512 GB | 519F5D1B01D9 | 92    | 0     | 0.25   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 25EF19BBD464 | 92    | 0     | 0.25   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 2E5AA843983A | 92    | 0     | 0.25   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | CB41C39EC34C | 92    | 0     | 0.25   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | D7F4CFEB8319 | 92    | 0     | 0.25   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | D953F008B43E | 92    | 0     | 0.25   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | D9CBCF92E46C | 92    | 0     | 0.25   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | DCCD7759D4A2 | 92    | 0     | 0.25   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | F663EEBA3F82 | 92    | 0     | 0.25   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 20E97E76E594 | 92    | 0     | 0.25   |
| Samsung   | MZVLB512HBJQ-00000 | 512 GB | 3795F9A987FD | 92    | 0     | 0.25   |
| Samsung   | SSD 970 PRO        | 512 GB | E272D55A3B26 | 92    | 0     | 0.25   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | B69DA6CA8CED | 92    | 0     | 0.25   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 28793506B4B2 | 92    | 0     | 0.25   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | B69DA6CA8CED | 92    | 0     | 0.25   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | A0BB04ECE427 | 92    | 0     | 0.25   |
| Samsung   | MZ4LB3T8HALS-00003 | 3.8 TB | 0CB47A6395C6 | 92    | 0     | 0.25   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 8684B21D4CB0 | 92    | 0     | 0.25   |
| Wester... | WUS4BB096D7P3E1    | 720 GB | C426126608AD | 92    | 0     | 0.25   |
| Micron    | 9300_MTFDHAL7T6TDP | 7.6 TB | 01007010E86D | 91    | 0     | 0.25   |
| Micron    | 9300_MTFDHAL7T6TDP | 7.6 TB | 4CD607E70AFF | 91    | 0     | 0.25   |
| Micron    | 9300_MTFDHAL7T6TDP | 7.6 TB | BEA2C16CC7B5 | 91    | 0     | 0.25   |
| Intel     | SSDPED1K375GA      | 375 GB | C94D94181DF7 | 91    | 0     | 0.25   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | BE006FBE8B65 | 91    | 0     | 0.25   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 3CC5E8B06983 | 91    | 0     | 0.25   |
| Samsung   | MZVLB512HBJQ-00000 | 512 GB | 086FAD82D33D | 91    | 0     | 0.25   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 24604F77A207 | 91    | 0     | 0.25   |
| Samsung   | SSD 970 PRO        | 512 GB | EF4D809CBD1F | 91    | 0     | 0.25   |
| Samsung   | MZQLB1T9HAJR-000AZ | 1.9 TB | 30F29F853195 | 91    | 0     | 0.25   |
| Intel     | SSDPEKNW512G8      | 512 GB | 56DB1B709658 | 91    | 0     | 0.25   |
| WDC       | WUS4BB019D7P3E3    | 1.9 TB | 9AEED3C15B22 | 91    | 0     | 0.25   |
| Micron    | 9300_MTFDHAL7T6TDP | 7.6 TB | 2039B87569A0 | 91    | 0     | 0.25   |
| Micron    | 9300_MTFDHAL7T6TDP | 7.6 TB | 41C6D56C015F | 91    | 0     | 0.25   |
| Micron    | 9300_MTFDHAL7T6TDP | 7.6 TB | 50CE08082550 | 91    | 0     | 0.25   |
| Samsung   | MZPLJ6T4HALA-00007 | 6.4 TB | 9D7FF3996A72 | 90    | 0     | 0.25   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 7A86C9A16964 | 90    | 0     | 0.25   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 5E287083C61A | 90    | 0     | 0.25   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 07740EFF30E4 | 90    | 0     | 0.25   |
| Samsung   | MZPLJ3T2HBJR-000H3 | 3.2 TB | 1D3FBAAC53AE | 90    | 0     | 0.25   |
| Samsung   | MZPLJ3T2HBJR-000H3 | 3.2 TB | 1D7323BB9B75 | 90    | 0     | 0.25   |
| WDC       | WUS4BB038D7P3E1    | 3.8 TB | 14FFFA4C5677 | 90    | 0     | 0.25   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | B23C3243D680 | 89    | 0     | 0.25   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | C9E99EBBDAC7 | 89    | 0     | 0.25   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 57EB60086468 | 89    | 0     | 0.24   |
| Dell      | Ent NVMe P5600 ... | 1.6 TB | 192FC381DEF9 | 89    | 0     | 0.24   |
| WDC       | WUS4BB019D7P3E3    | 1.9 TB | 899A42EB4DE4 | 88    | 0     | 0.24   |
| Toshiba   | KXD51RUE1T92       | 1.9 TB | 079B9DC8EA80 | 88    | 0     | 0.24   |
| Toshiba   | KXD51RUE1T92       | 1.9 TB | DE6ACBE3A6D0 | 88    | 0     | 0.24   |
| Samsung   | MZPLJ3T2HBJR-000H3 | 3.2 TB | 0559E3447122 | 87    | 0     | 0.24   |
| Samsung   | MZPLJ3T2HBJR-000H3 | 3.2 TB | 307FBA3AB951 | 87    | 0     | 0.24   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 5EAAE21B531F | 87    | 0     | 0.24   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | CF4CB666C2D8 | 87    | 0     | 0.24   |
| Samsung   | SSD 970 PRO        | 512 GB | 64AE59C80978 | 87    | 0     | 0.24   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 52CC165F2944 | 87    | 0     | 0.24   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | BAA1C06CCA03 | 87    | 0     | 0.24   |
| Samsung   | MZQLB1T9HAJR-000AZ | 1.9 TB | 9DAF785CE4FE | 87    | 0     | 0.24   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 5E274511528F | 87    | 0     | 0.24   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 108490F4BCF4 | 86    | 0     | 0.24   |
| Samsung   | MZPLJ6T4HALA-00007 | 6.4 TB | 78C9DC814890 | 86    | 0     | 0.24   |
| Micron    | 2200_MTFDHBA512TCK | 512 GB | DC0870737752 | 86    | 0     | 0.24   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | D908AB1D9BC3 | 86    | 0     | 0.24   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | BEF2617DBA3E | 86    | 0     | 0.24   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | FA659DF363CE | 86    | 0     | 0.24   |
| Samsung   | SSD 970 EVO Plus   | 250 GB | 455396201D95 | 85    | 0     | 0.24   |
| Dell      | Ent NVMe P5600 ... | 1.6 TB | 6E83941FE7C5 | 85    | 0     | 0.24   |
| Samsung   | MZPLJ3T2HBJR-000H3 | 3.2 TB | DA46480DFA61 | 85    | 0     | 0.24   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | C04987287B69 | 85    | 0     | 0.24   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 2FB44B5AA145 | 85    | 0     | 0.23   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | D18917D8196E | 85    | 0     | 0.23   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 033E4266DAE1 | 85    | 0     | 0.23   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 03A271A13F20 | 85    | 0     | 0.23   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 080B6FC5116E | 85    | 0     | 0.23   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 16997E62D42C | 85    | 0     | 0.23   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 1CE20FCF437C | 85    | 0     | 0.23   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 2DE09B0A0F61 | 85    | 0     | 0.23   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 6498AC3486D3 | 85    | 0     | 0.23   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 80AC1222BF00 | 85    | 0     | 0.23   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 8950E100A660 | 85    | 0     | 0.23   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 8E8E71E6BCC1 | 85    | 0     | 0.23   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | DAE5F94B3F37 | 85    | 0     | 0.23   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | E19F4959A5C9 | 85    | 0     | 0.23   |
| Toshiba   | KXG60ZNV512G       | 512 GB | 4BDFE23AD54E | 85    | 0     | 0.23   |
| Toshiba   | KXG60ZNV512G       | 512 GB | AE30F4F3DE11 | 85    | 0     | 0.23   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 5CDA649D913E | 85    | 0     | 0.23   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | CC0DD2014951 | 85    | 0     | 0.23   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | BDE3A7A866F7 | 85    | 0     | 0.23   |
| Samsung   | MZVLB512HAJQ-00000 | 512 GB | 5AB64AA64032 | 85    | 0     | 0.23   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 79B8084D1356 | 85    | 0     | 0.23   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | CF374560824C | 85    | 0     | 0.23   |
| Samsung   | MZVLB1T0HALR-00000 | 1 TB   | BAC99E75833F | 85    | 0     | 0.23   |
| Kingston  | SA2000M8500G       | 500 GB | 6AB6EF09CB97 | 84    | 0     | 0.23   |
| Kingston  | SEDC1000M960G      | 960 GB | A68289B4066C | 84    | 0     | 0.23   |
| Samsung   | SSD 970 PRO        | 512 GB | 53853B2A6567 | 84    | 0     | 0.23   |
| Samsung   | MZPLJ3T2HBJR-000H3 | 3.2 TB | 11B3D4536BFD | 84    | 0     | 0.23   |
| Samsung   | MZVLB1T0HALR-00000 | 1 TB   | F1292FA431DB | 84    | 0     | 0.23   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | AFDB136BB81B | 84    | 0     | 0.23   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | A64D9AF65C42 | 84    | 0     | 0.23   |
| Dell      | Express Flash P... | 1.6 TB | 8ECDD40B09AC | 83    | 0     | 0.23   |
| Intel     | SSDPE21K375GA      | 375 GB | B1FAA31448C1 | 83    | 0     | 0.23   |
| SK hynix  | VO000960KXAVL      | 960 GB | 02E4DDFD987D | 83    | 0     | 0.23   |
| Samsung   | SSD 970 EVO Plus   | 2 TB   | 1CF764A1791A | 83    | 0     | 0.23   |
| Samsung   | SSD 970 EVO Plus   | 2 TB   | A01D87294114 | 83    | 0     | 0.23   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 45A01AED5708 | 83    | 0     | 0.23   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 5EB95B8413D8 | 83    | 0     | 0.23   |
| Toshiba   | KXG60ZNV512G       | 512 GB | 597710CF4A29 | 83    | 0     | 0.23   |
| Toshiba   | KXG60ZNV512G       | 512 GB | F492333D14EB | 83    | 0     | 0.23   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 16687DC21BF2 | 83    | 0     | 0.23   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 35A4672D63B6 | 83    | 0     | 0.23   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 52A2D5837DC7 | 83    | 0     | 0.23   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 6E7B4C306BC7 | 83    | 0     | 0.23   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 74A9A29805F8 | 83    | 0     | 0.23   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 7971A81B789A | 83    | 0     | 0.23   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | C554CC95A924 | 83    | 0     | 0.23   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | C56B352F6B98 | 83    | 0     | 0.23   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | D0EE36814DE7 | 83    | 0     | 0.23   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | E029E36D16E3 | 83    | 0     | 0.23   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | FCC268E496FD | 83    | 0     | 0.23   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | FF4DA25D76A5 | 83    | 0     | 0.23   |
| Toshiba   | KXG60ZNV512G       | 512 GB | 48D01C1B980C | 83    | 0     | 0.23   |
| Toshiba   | KXG60ZNV512G       | 512 GB | E552DB7DF4FE | 83    | 0     | 0.23   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 03EC8E0C2DEA | 83    | 0     | 0.23   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 353D5EAE87EE | 83    | 0     | 0.23   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 3C63B00B852C | 83    | 0     | 0.23   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 4A88620401C9 | 83    | 0     | 0.23   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 55F023F5198D | 83    | 0     | 0.23   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 56FC5D3E38FB | 83    | 0     | 0.23   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 59FEFF25D759 | 83    | 0     | 0.23   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 667BEF10F7A5 | 83    | 0     | 0.23   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 926B33DD9641 | 83    | 0     | 0.23   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | A2FF8E6C4A7B | 83    | 0     | 0.23   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | D9F9481A5001 | 83    | 0     | 0.23   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | FDF7608845CA | 83    | 0     | 0.23   |
| Toshiba   | KXG60ZNV512G       | 512 GB | 22110D7A085A | 83    | 0     | 0.23   |
| Toshiba   | KXG60ZNV512G       | 512 GB | 751EDC5D067F | 83    | 0     | 0.23   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 0C52EC2B1093 | 83    | 0     | 0.23   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 35E617E7BCC7 | 83    | 0     | 0.23   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 5606336DC4BB | 83    | 0     | 0.23   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 5848C0E66094 | 83    | 0     | 0.23   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 772F64368A17 | 83    | 0     | 0.23   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 80283755D5E9 | 83    | 0     | 0.23   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 884E6E740DE6 | 83    | 0     | 0.23   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | AEF8DF2BBE2C | 83    | 0     | 0.23   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | C54CB661CBCA | 83    | 0     | 0.23   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | D771917845DC | 83    | 0     | 0.23   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 0F691DEC28B6 | 82    | 0     | 0.23   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 42297CCC97C0 | 82    | 0     | 0.23   |
| Samsung   | MZVLB1T0HALR-00000 | 1 TB   | 3B696E1127CD | 82    | 0     | 0.23   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 0836ECA0B4EF | 82    | 0     | 0.23   |
| Samsung   | MZVLB1T0HALR-00000 | 1 TB   | 484CA30E4943 | 82    | 0     | 0.23   |
| Samsung   | MZWLL800HEHP-00003 | 800 GB | B816C65A0661 | 905   | 10    | 0.23   |
| Samsung   | MZPLJ3T2HBJR-00007 | 3.2 TB | B8A7B982776F | 82    | 0     | 0.23   |
| Intel     | SSDPE21K375GA      | 375 GB | 64C846B65846 | 82    | 0     | 0.22   |
| Samsung   | MZWLL800HEHP-00003 | 800 GB | F55C28434055 | 902   | 10    | 0.22   |
| Intel     | SSDPED1K375GA      | 375 GB | 383C21F4B5FC | 82    | 0     | 0.22   |
| Micron    | 7300_MTFDHBE1T6TDG | 1.6 TB | 9177D0C12909 | 81    | 0     | 0.22   |
| Micron    | 9300_MTFDHAL3T2TDR | 3.2 TB | 0688D21476E2 | 81    | 0     | 0.22   |
| Micron    | 9300_MTFDHAL3T2TDR | 3.2 TB | B474CFD34348 | 81    | 0     | 0.22   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 509A6E17391F | 81    | 0     | 0.22   |
| Samsung   | MZPLJ6T4HALA-00007 | 6.4 TB | D42F9A7B7232 | 81    | 0     | 0.22   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 8938E8F39B4E | 81    | 0     | 0.22   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | E29B5AFAB6E6 | 81    | 0     | 0.22   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 838650774A4E | 80    | 0     | 0.22   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | D4BEE93989E4 | 80    | 0     | 0.22   |
| Samsung   | SSD 970 EVO Plus   | 250 GB | 31DFAE64B0D6 | 80    | 0     | 0.22   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | A721283BCB2D | 80    | 0     | 0.22   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | BC9E6E080381 | 80    | 0     | 0.22   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 6A44F7934C5B | 80    | 0     | 0.22   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 8EFC18981387 | 80    | 0     | 0.22   |
| Corsair   | MP400              | 1 TB   | AB440B4AF305 | 80    | 0     | 0.22   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 35E1700A95E8 | 80    | 0     | 0.22   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | C57678B4915F | 80    | 0     | 0.22   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 35E1700A95E8 | 80    | 0     | 0.22   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | C57678B4915F | 80    | 0     | 0.22   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | EAF0079A3975 | 80    | 0     | 0.22   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | CBB8775AAE5D | 80    | 0     | 0.22   |
| Samsung   | MZPLJ1T6HBJR-000H3 | 1.6 TB | DBDA15C4859C | 79    | 0     | 0.22   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | C64590614AEA | 79    | 0     | 0.22   |
| Toshiba   | KXG60ZNV512G       | 512 GB | 335277C9DF9B | 79    | 0     | 0.22   |
| Toshiba   | KXG60ZNV512G       | 512 GB | 7E6ED71CC401 | 79    | 0     | 0.22   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 01B60F779FAF | 79    | 0     | 0.22   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 430FA0BB0402 | 79    | 0     | 0.22   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 59414644336A | 79    | 0     | 0.22   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 5ACA202A3A67 | 79    | 0     | 0.22   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 65978677E05E | 79    | 0     | 0.22   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 741F3F09B6A9 | 79    | 0     | 0.22   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 834AF14D49CD | 79    | 0     | 0.22   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 89FFA57CE964 | 79    | 0     | 0.22   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 8A50AAB6F432 | 79    | 0     | 0.22   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | A73954527EC7 | 79    | 0     | 0.22   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | BD8623CC06BF | 79    | 0     | 0.22   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | D32006C6879E | 79    | 0     | 0.22   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 7A6F0EC9B029 | 78    | 0     | 0.22   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 7E6C2901E074 | 78    | 0     | 0.22   |
| Toshiba   | KXG60ZNV512G       | 512 GB | D25D51329AFC | 78    | 0     | 0.22   |
| Toshiba   | KXG60ZNV512G       | 512 GB | F90C90292F20 | 78    | 0     | 0.22   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 05F87302F937 | 78    | 0     | 0.22   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 1025C3E3123E | 78    | 0     | 0.22   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 1344F76FA470 | 78    | 0     | 0.22   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 27C756BD5C5F | 78    | 0     | 0.22   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 45B97508F2DF | 78    | 0     | 0.22   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 4BF619D1672C | 78    | 0     | 0.22   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 52A129C0B1DF | 78    | 0     | 0.22   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 616D829E837D | 78    | 0     | 0.22   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 623B1B1A4AA3 | 78    | 0     | 0.22   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 9E3C6D798497 | 78    | 0     | 0.22   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | DF7324091C7C | 78    | 0     | 0.22   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | FC198772F110 | 78    | 0     | 0.22   |
| Samsung   | MZPLJ6T4HALA-00007 | 6.4 TB | 064679C91B3F | 78    | 0     | 0.22   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 8A41EA260582 | 78    | 0     | 0.22   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 1A6D08081BA1 | 78    | 0     | 0.21   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 2A3D88416C02 | 78    | 0     | 0.21   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 6DFD8481D642 | 78    | 0     | 0.21   |
| Samsung   | MZPLJ1T6HBJR-000H3 | 1.6 TB | 574C49C909C3 | 78    | 0     | 0.21   |
| Samsung   | MZVLB1T0HALR-00000 | 1 TB   | 5A901E203A6F | 78    | 0     | 0.21   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 90380C998669 | 78    | 0     | 0.21   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | B888778FD8DE | 78    | 0     | 0.21   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | C401AA3303B9 | 78    | 0     | 0.21   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 4D065516CF9F | 77    | 0     | 0.21   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 60C30539F67E | 77    | 0     | 0.21   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 831C5B899E64 | 77    | 0     | 0.21   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | A9F8179F4DB4 | 77    | 0     | 0.21   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | AF8BFD733D58 | 77    | 0     | 0.21   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | BA9659C11A21 | 77    | 0     | 0.21   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | CEB9A1705300 | 77    | 0     | 0.21   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | EA526F1F5AB7 | 77    | 0     | 0.21   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | EA63748316CA | 77    | 0     | 0.21   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | EEA162F20305 | 77    | 0     | 0.21   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | F8693889972B | 77    | 0     | 0.21   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | F8C249BBB5EE | 77    | 0     | 0.21   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 11F0BB8A6044 | 77    | 0     | 0.21   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 1AE65C7BBCA9 | 77    | 0     | 0.21   |
| Toshiba   | KXG60ZNV512G       | 512 GB | 8941CAE9F380 | 77    | 0     | 0.21   |
| Toshiba   | KXG60ZNV512G       | 512 GB | C0266C7C8EA8 | 77    | 0     | 0.21   |
| Samsung   | SSD 970 PRO        | 512 GB | 406B5A74CEAF | 77    | 0     | 0.21   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 31E860B623E7 | 77    | 0     | 0.21   |
| Intel     | SSDPE21K375GA      | 375 GB | 1F64910706B6 | 77    | 0     | 0.21   |
| Samsung   | MT001600KWHAC 1... | 1.6 TB | F10057B85F2B | 77    | 0     | 0.21   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 3B3297975E65 | 77    | 0     | 0.21   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | AF12C5785333 | 77    | 0     | 0.21   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | C1B912679AED | 77    | 0     | 0.21   |
| Samsung   | MZVLB1T0HBLR-00000 | 1 TB   | 7AAA19CD6899 | 77    | 0     | 0.21   |
| Samsung   | MZWLL800HEHP-00003 | 800 GB | 5DA674A99F18 | 846   | 10    | 0.21   |
| SK hynix  | VO000960KXAVL      | 960 GB | A61ED2738A59 | 76    | 0     | 0.21   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 122C36358B91 | 76    | 0     | 0.21   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 4801331FA3A5 | 76    | 0     | 0.21   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 7FBBEBFEB00D | 76    | 0     | 0.21   |
| Samsung   | MZWLL800HEHP-00003 | 800 GB | 89CA0A6E5F56 | 843   | 10    | 0.21   |
| Samsung   | MZVLB1T0HBLR-00000 | 1 TB   | B7ACDF9909BD | 76    | 0     | 0.21   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | E09C5B882288 | 76    | 0     | 0.21   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | EA4FD5098F7A | 76    | 0     | 0.21   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | FB23AEBD886E | 76    | 0     | 0.21   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 85ECEC98F8F7 | 76    | 0     | 0.21   |
| Intel     | SSDPE2KX010T8      | 1 TB   | 01D64D5D3EA6 | 76    | 0     | 0.21   |
| Samsung   | MZVLB1T0HBLR-00000 | 1 TB   | 85230988D7A7 | 76    | 0     | 0.21   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 005399F56047 | 76    | 0     | 0.21   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | E43030E083F1 | 76    | 0     | 0.21   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | ADDE1E557AA5 | 76    | 0     | 0.21   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | B67CFF9925E5 | 76    | 0     | 0.21   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | E4B301ECD0B5 | 76    | 0     | 0.21   |
| Samsung   | SSD 970 PRO        | 1 TB   | A3F5CC092F19 | 76    | 0     | 0.21   |
| Wester... | WUS4CB016D7P3E3    | 1.6 TB | DC1C2FE11015 | 76    | 0     | 0.21   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 91EB4F67406A | 75    | 0     | 0.21   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 1B59598E8BC8 | 75    | 0     | 0.21   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 1D4566CDC4C2 | 75    | 0     | 0.21   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | A58DB787D7ED | 75    | 0     | 0.21   |
| WDC       | WUS3BA119C7P3E3    | 1.9 TB | EB3BDAC118FA | 75    | 0     | 0.21   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 8CC554F99A56 | 75    | 0     | 0.21   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | DFBAA0286BAA | 75    | 0     | 0.21   |
| Intel     | SSDPED1K375GA      | 375 GB | 43EA705CE85F | 75    | 0     | 0.21   |
| Intel     | SSDPED1K375GA      | 375 GB | B86CAE0F2D01 | 75    | 0     | 0.21   |
| Samsung   | SSD 970 EVO Plus   | 250 GB | 07D205CB71B8 | 75    | 0     | 0.21   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | 1CC1BB2C96E8 | 75    | 0     | 0.21   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | DE7A4AA9F724 | 75    | 0     | 0.21   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | FE1EBC4AF77F | 75    | 0     | 0.21   |
| WDC       | WUS4BB019D7P3E3    | 1.9 TB | 5736AB2332ED | 75    | 0     | 0.21   |
| Samsung   | MZVLB1T0HBLR-00000 | 1 TB   | 22AD0675208B | 75    | 0     | 0.21   |
| Intel     | SSDPEKNW010T8      | 1 TB   | EBF41F5BF789 | 74    | 0     | 0.21   |
| Intel     | SSDPE2KX010T8      | 1 TB   | 1BD49C9855EE | 74    | 0     | 0.20   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 61FE2F329D4D | 74    | 0     | 0.20   |
| Toshiba   | KXG60ZNV256G       | 256 GB | 15EC8A5BBF1B | 74    | 0     | 0.20   |
| Toshiba   | KXG60ZNV256G       | 256 GB | 287F7C51FAD2 | 74    | 0     | 0.20   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | CDA3831A4EFB | 74    | 0     | 0.20   |
| Intel     | SSDPEKNW512G8      | 512 GB | 9AE50BD64A5C | 73    | 0     | 0.20   |
| SK hynix  | BC501 HFM512GDJ... | 512 GB | 0912CF32F42F | 73    | 0     | 0.20   |
| Samsung   | MZPLJ6T4HALA-00007 | 6.4 TB | 6C06413CC18C | 73    | 0     | 0.20   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | C0AA66A1AA14 | 73    | 0     | 0.20   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | F2CEAD1C812E | 73    | 0     | 0.20   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | CD9BDE36F521 | 73    | 0     | 0.20   |
| Gigabyte  | GP-GSM2NE3100TNTD  | 1 TB   | 0D3B2C13D377 | 73    | 0     | 0.20   |
| Gigabyte  | GP-GSM2NE3100TNTD  | 1 TB   | 43FFC49660BE | 73    | 0     | 0.20   |
| Samsung   | MZVLB512HBJQ-00000 | 512 GB | 5A4D9C1D8AE9 | 73    | 0     | 0.20   |
| Corsair   | MP400              | 1 TB   | 788EC978A7B9 | 72    | 0     | 0.20   |
| Intel     | SSDPED1K750GA      | 752 GB | 8F0C04567502 | 72    | 0     | 0.20   |
| Micron    | 2200_MTFDHBA256TCK | 256 GB | 00AC9B24262F | 72    | 0     | 0.20   |
| WDC       | WDS100T3X0C-00SJG0 | 1 TB   | 47FC82A56766 | 72    | 0     | 0.20   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 7DA380D655C6 | 72    | 0     | 0.20   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 0F22A76AD8F5 | 72    | 0     | 0.20   |
| Samsung   | SSD 980 PRO        | 250 GB | CF3E9C6BB05F | 72    | 0     | 0.20   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 06856B7D66F5 | 72    | 0     | 0.20   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 77C31D8960AA | 72    | 0     | 0.20   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | BDB9151CDC68 | 72    | 0     | 0.20   |
| Micron    | 2200_MTFDHBA512TCK | 512 GB | AC6BB9B179AF | 71    | 0     | 0.20   |
| WDC       | CL SN720 SDAQNT... | 512 GB | B0B915A28146 | 71    | 0     | 0.20   |
| WDC       | WDS100T3X0C-00SJG0 | 1 TB   | 04AEA46D72E8 | 71    | 0     | 0.20   |
| WDC       | WDS100T3X0C-00SJG0 | 1 TB   | 3D140911C1A4 | 71    | 0     | 0.20   |
| WDC       | WDS100T3X0C-00SJG0 | 1 TB   | 2A6F4B383BCB | 71    | 0     | 0.20   |
| WDC       | WDS100T3X0C-00SJG0 | 1 TB   | 658BC63C91CB | 71    | 0     | 0.20   |
| WDC       | WUS4BB038D7P3E1    | 3.8 TB | EC3E9FEDFD2D | 71    | 0     | 0.20   |
| Samsung   | SSD 970 PRO        | 512 GB | 2DC4C58419A1 | 71    | 0     | 0.20   |
| WDC       | WDS100T3X0C-00SJG0 | 1 TB   | CA2EE9922EC3 | 71    | 0     | 0.20   |
| WDC       | WUS4BB019D7P3E3    | 1.9 TB | 8F1895B4C237 | 71    | 0     | 0.20   |
| WDC       | WUS4BB019D7P3E3    | 1.9 TB | D18080A90D38 | 71    | 0     | 0.20   |
| WDC       | WUS4BB019D7P3E3    | 1.9 TB | FFC429B90DBE | 71    | 0     | 0.20   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 8A7900138118 | 71    | 0     | 0.19   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | E4F6EB0DF3DC | 71    | 0     | 0.19   |
| Samsung   | MZVLB1T0HBLR-00000 | 1 TB   | CB68C31DF6E8 | 70    | 0     | 0.19   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | B5DFE7B3DA7D | 70    | 0     | 0.19   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | EC0E6A3AB16E | 70    | 0     | 0.19   |
| Corsair   | MP400              | 1 TB   | 7593CCACBE8E | 70    | 0     | 0.19   |
| Micron    | 2200_MTFDHBA256TCK | 256 GB | EC4A850CF5C0 | 70    | 0     | 0.19   |
| Corsair   | MP400              | 1 TB   | 041185F165B6 | 70    | 0     | 0.19   |
| Intel     | SSDPE2KX010T8      | 1 TB   | 8CC706392E72 | 70    | 0     | 0.19   |
| Intel     | SSDPE2KX010T8      | 1 TB   | D832B6578112 | 70    | 0     | 0.19   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 67C66E4A0955 | 70    | 0     | 0.19   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 33074E0DBFBD | 70    | 0     | 0.19   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | E6B4EC4BF209 | 70    | 0     | 0.19   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 331252486F72 | 70    | 0     | 0.19   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | D36F85BD8508 | 70    | 0     | 0.19   |
| Corsair   | MP400              | 1 TB   | 30CCCDF6B572 | 70    | 0     | 0.19   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 9B89F86E6FF2 | 70    | 0     | 0.19   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | D1ACA5BE485B | 70    | 0     | 0.19   |
| Intel     | SSDPE2KX010T8      | 1 TB   | E11264E3DF36 | 70    | 0     | 0.19   |
| Micron    | 2200_MTFDHBA256TCK | 256 GB | AE63B989076E | 70    | 0     | 0.19   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 4E2D6BA89A69 | 69    | 0     | 0.19   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 71EAE54A37C5 | 69    | 0     | 0.19   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | B9C4933FD6A1 | 69    | 0     | 0.19   |
| Corsair   | MP400              | 1 TB   | AEFE1816C8C9 | 69    | 0     | 0.19   |
| WDC       | WDS100T3X0C-00SJG0 | 1 TB   | 4D714786F082 | 69    | 0     | 0.19   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 6B39D51C24F9 | 69    | 0     | 0.19   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | EFE496E00868 | 69    | 0     | 0.19   |
| WDC       | WDS100T3X0C-00SJG0 | 1 TB   | E01A0C1CF663 | 68    | 0     | 0.19   |
| Samsung   | SSD 970 PRO        | 512 GB | A2EB125F1B90 | 68    | 0     | 0.19   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 859D278C748B | 68    | 0     | 0.19   |
| Intel     | MDTPED1K750GA      | 752 GB | 27F66D2D2CE1 | 68    | 0     | 0.19   |
| Micron    | 2200_MTFDHBA256TCK | 256 GB | F0696DE030CF | 68    | 0     | 0.19   |
| Samsung   | MZPLJ6T4HALA-00007 | 6.4 TB | 5DCBF7B66D12 | 68    | 0     | 0.19   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 5BEA040B5010 | 68    | 0     | 0.19   |
| Corsair   | MP400              | 1 TB   | D25248E0683C | 68    | 0     | 0.19   |
| Samsung   | SSD 970 EVO        | 1 TB   | E58D80C2DB65 | 819   | 11    | 0.19   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 65F2435C58FE | 68    | 0     | 0.19   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | E9CBA15AC2A3 | 68    | 0     | 0.19   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 53C4693DE8E2 | 67    | 0     | 0.19   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | BBD63F2C5147 | 67    | 0     | 0.19   |
| WDC       | WDS500G3XHC-00SJG0 | 500 GB | 01DC76EE2D3A | 67    | 0     | 0.19   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | BAE9D37717E0 | 67    | 0     | 0.19   |
| Micron    | 9300_MTFDHAL3T2TDR | 3.2 TB | 29761F19583F | 67    | 0     | 0.18   |
| Micron    | 2200_MTFDHBA256TCK | 256 GB | FDC961FC322D | 66    | 0     | 0.18   |
| Micron    | 2200_MTFDHBA256TCK | 256 GB | 8CDCE7A5C8C8 | 66    | 0     | 0.18   |
| Samsung   | MZVLB256HAHQ-000L7 | 256 GB | 94F88DEC1444 | 66    | 0     | 0.18   |
| Corsair   | MP400              | 1 TB   | 03234BF47787 | 66    | 0     | 0.18   |
| Samsung   | MZVLB512HAJQ-00000 | 512 GB | 67896D994B13 | 395   | 5     | 0.18   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 75DB4140EE27 | 65    | 0     | 0.18   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | D72BFB931031 | 65    | 0     | 0.18   |
| Micron    | 2200_MTFDHBA256TCK | 256 GB | 4336C7EF94C4 | 65    | 0     | 0.18   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 45123170C789 | 65    | 0     | 0.18   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 66A72290ADDB | 65    | 0     | 0.18   |
| WDC       | WUS4BB019D7P3E3    | 1.9 TB | 48370171384E | 65    | 0     | 0.18   |
| WDC       | WUS4BB019D7P3E3    | 1.9 TB | 9730E1429DF2 | 65    | 0     | 0.18   |
| WDC       | WUS4BB019D7P3E3    | 1.9 TB | 9A486D991C55 | 65    | 0     | 0.18   |
| WDC       | WUS4BB019D7P3E3    | 1.9 TB | D36B26153C87 | 65    | 0     | 0.18   |
| Samsung   | SSD 970 PRO        | 512 GB | B2B18535AB58 | 65    | 0     | 0.18   |
| Micron    | 2200_MTFDHBA256TCK | 256 GB | 0E6FB58E3908 | 65    | 0     | 0.18   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 1C8BC92051B2 | 65    | 0     | 0.18   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | BFEACBC67B97 | 65    | 0     | 0.18   |
| Micron    | 2200_MTFDHBA256TCK | 256 GB | 2B819A91481A | 65    | 0     | 0.18   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 19E2B73FD6ED | 65    | 0     | 0.18   |
| Wester... | WUS4BB038D7P3E1    | 3.8 TB | 1B6AD60A03F2 | 64    | 0     | 0.18   |
| Toshiba   | KXG60ZNV256G       | 256 GB | 71129B183F4E | 64    | 0     | 0.18   |
| WDC       | CL SN720 SDAQNT... | 512 GB | 4D1ECEA53C38 | 64    | 0     | 0.18   |
| Dell      | Express Flash P... | 1.6 TB | 0AA4464B4C21 | 64    | 0     | 0.18   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | C5C1D33D4B57 | 64    | 0     | 0.18   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | A07308C8BB72 | 64    | 0     | 0.18   |
| Corsair   | MP400              | 1 TB   | 47D198C3B457 | 64    | 0     | 0.18   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 2486ECD6CDC5 | 64    | 0     | 0.18   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 30CE4CF543AC | 64    | 0     | 0.18   |
| WDC       | WUS4BB019D7P3E3    | 1.9 TB | 5504939C110B | 64    | 0     | 0.18   |
| WDC       | WUS4BB019D7P3E3    | 1.9 TB | A7F2D4993C43 | 64    | 0     | 0.18   |
| WDC       | WUS4BB019D7P3E3    | 1.9 TB | C70577B4823E | 64    | 0     | 0.18   |
| WDC       | WUS4BB019D7P3E3    | 1.9 TB | DAF5193303ED | 64    | 0     | 0.18   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 47D2B51DB3BD | 63    | 0     | 0.17   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 8615C64F492E | 63    | 0     | 0.17   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | F60C22798A74 | 63    | 0     | 0.17   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 7D07DC013E77 | 63    | 0     | 0.17   |
| Kingston  | SKC2500M8500G      | 500 GB | F49A8CE55FDE | 63    | 0     | 0.17   |
| Samsung   | MZPLJ6T4HALA-00007 | 6.4 TB | 60CE6C9863F5 | 63    | 0     | 0.17   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | EA1601F0DB74 | 63    | 0     | 0.17   |
| Micron    | 9300_MTFDHAL3T2TDR | 3.2 TB | 39734338FC49 | 63    | 0     | 0.17   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | D26986AF2F24 | 63    | 0     | 0.17   |
| Intel     | SSDPEKNW010T8      | 1 TB   | E68979185D5A | 62    | 0     | 0.17   |
| WDC       | WUS4BB019D7P3E3    | 1.9 TB | 29960782E711 | 62    | 0     | 0.17   |
| WDC       | WUS4BB019D7P3E3    | 1.9 TB | A5995EACCFA3 | 62    | 0     | 0.17   |
| WDC       | WUS4BB019D7P3E3    | 1.9 TB | BD11A213B96E | 62    | 0     | 0.17   |
| WDC       | WUS4BB019D7P3E3    | 1.9 TB | C02219912E5F | 62    | 0     | 0.17   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | DB5DAE835CAF | 62    | 0     | 0.17   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 61AB4C516699 | 62    | 0     | 0.17   |
| Micron    | 9300_MTFDHAL3T2TDR | 3.2 TB | 7CF82422EBE4 | 62    | 0     | 0.17   |
| WDC       | CL SN720 SDAQNT... | 1 TB   | 416B9A8F4812 | 62    | 0     | 0.17   |
| WDC       | CL SN720 SDAQNT... | 512 GB | E3EACE032AC8 | 62    | 0     | 0.17   |
| Samsung   | SSD 970 EVO        | 1 TB   | 4F09EE3C5B12 | 62    | 0     | 0.17   |
| Corsair   | MP400              | 1 TB   | E71D22B4B7CB | 62    | 0     | 0.17   |
| Samsung   | SSD 970 EVO        | 1 TB   | D682BB0AACBF | 62    | 0     | 0.17   |
| Wester... | WUS4BB038D7P3E1    | 3.8 TB | BB979B2C17DB | 61    | 0     | 0.17   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 10C9354F393A | 61    | 0     | 0.17   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | D9557E53F14D | 61    | 0     | 0.17   |
| WDC       | CL SN720 SDAQNT... | 1 TB   | C68A8EA8EC14 | 61    | 0     | 0.17   |
| Wester... | WUS4BB038D7P3E1    | 3.8 TB | 3B8785F4A90F | 61    | 0     | 0.17   |
| Wester... | WUS4BB038D7P3E1    | 3.8 TB | 7CE1CB9575F3 | 61    | 0     | 0.17   |
| Wester... | WUS4BB038D7P3E1    | 3.8 TB | F60E969DEEC8 | 61    | 0     | 0.17   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 68F169DE80E7 | 426   | 6     | 0.17   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 7163EBB63505 | 60    | 0     | 0.17   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | D4DE6F4CC130 | 60    | 0     | 0.17   |
| Samsung   | MZVL21T0HCLR-00B00 | 1 TB   | 2CDCDFB33F57 | 60    | 0     | 0.17   |
| WDC       | WUS3BA119C7P3E3    | 1.9 TB | 2CFCBECF8C49 | 60    | 0     | 0.17   |
| Samsung   | MZVL21T0HCLR-00B00 | 1 TB   | 5431B39DB2E6 | 60    | 0     | 0.17   |
| WDC       | WUS4BB019D7P3E3    | 1.9 TB | 2AEC965A36F9 | 60    | 0     | 0.17   |
| WDC       | WUS4BB019D7P3E3    | 1.9 TB | 7ED2D78BDC88 | 60    | 0     | 0.17   |
| WDC       | WUS4BB019D7P3E3    | 1.9 TB | ADE0CDBE558B | 60    | 0     | 0.17   |
| WDC       | WUS4BB019D7P3E3    | 1.9 TB | 1243300DE7E7 | 60    | 0     | 0.17   |
| WDC       | WUS4BB019D7P3E3    | 1.9 TB | 5D8C6DA053F6 | 60    | 0     | 0.17   |
| WDC       | WUS4BB019D7P3E3    | 1.9 TB | B90AC5538E14 | 60    | 0     | 0.17   |
| Intel     | SSDPED1K375GA      | 375 GB | 3F3A19B4A640 | 60    | 0     | 0.17   |
| Wester... | WUS4BB038D7P3E1    | 3.8 TB | 9BA44887EB20 | 60    | 0     | 0.17   |
| Intel     | SSDPED1K375GA      | 375 GB | A81F97B4A22F | 60    | 0     | 0.16   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 188FE3CE1B5A | 60    | 0     | 0.16   |
| WDC       | WDS500G3X0C-00SJG0 | 500 GB | 03CAB8A236B0 | 59    | 0     | 0.16   |
| Intel     | SSDPED1K375GA      | 375 GB | E349594480B2 | 59    | 0     | 0.16   |
| WDC       | WDS500G3X0C-00SJG0 | 500 GB | 65588DF8EF38 | 59    | 0     | 0.16   |
| WDC       | WUS4BB019D7P3E3    | 1.9 TB | AE08B8EBD4BF | 59    | 0     | 0.16   |
| WDC       | CL SN720 SDAQNT... | 512 GB | 8C5CFB320BB3 | 59    | 0     | 0.16   |
| WDC       | WUS4BB038D7P3E3    | 3.8 TB | 4D45D57AF124 | 59    | 0     | 0.16   |
| Intel     | SSDPED1K375GA      | 375 GB | C51ED1636FE7 | 59    | 0     | 0.16   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 39226E45DA6D | 59    | 0     | 0.16   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | B7B7FA4528BC | 59    | 0     | 0.16   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | E9D55E00338A | 59    | 0     | 0.16   |
| WDC       | WUS4BB019D7P3E3    | 1.9 TB | 5369B4A9DC7E | 59    | 0     | 0.16   |
| WDC       | WUS4BB019D7P3E3    | 1.9 TB | 60F5F7F596C2 | 59    | 0     | 0.16   |
| WDC       | WUS4BB019D7P3E3    | 1.9 TB | 912D14AD08FF | 59    | 0     | 0.16   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 2B61E03995CD | 59    | 0     | 0.16   |
| Samsung   | MZWLL800HEHP-00003 | 800 GB | 00CF1256FA12 | 826   | 13    | 0.16   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 7B2956D6B4A3 | 59    | 0     | 0.16   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | B184F0B33F79 | 59    | 0     | 0.16   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | C8B6DE235005 | 59    | 0     | 0.16   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | EF37F1640A03 | 59    | 0     | 0.16   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | F5F25C31F92B | 59    | 0     | 0.16   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | F8F84F351B68 | 59    | 0     | 0.16   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | FEC9F13623CB | 59    | 0     | 0.16   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 83756F487561 | 58    | 0     | 0.16   |
| WDC       | CL SN720 SDAQNT... | 1 TB   | 173793071209 | 58    | 0     | 0.16   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | C75396681947 | 58    | 0     | 0.16   |
| Samsung   | SSD 980            | 500 GB | 0186B1FD7943 | 58    | 0     | 0.16   |
| WDC       | WDS500G3X0C-00SJG0 | 500 GB | D7CA9EEB89A5 | 58    | 0     | 0.16   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 1510E2541F28 | 58    | 0     | 0.16   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 5D2B7A449256 | 58    | 0     | 0.16   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 5E74BC2C08BB | 58    | 0     | 0.16   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | F9FC517F9FBD | 58    | 0     | 0.16   |
| WDC       | CL SN720 SDAQNT... | 1 TB   | 2F2A63A90CD3 | 58    | 0     | 0.16   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 25F0582F02A1 | 58    | 0     | 0.16   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 0BFC5B97B396 | 58    | 0     | 0.16   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 25DAE8A30BDC | 58    | 0     | 0.16   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | E60AC80D9C91 | 58    | 0     | 0.16   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | EAA0A7367EED | 58    | 0     | 0.16   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | F1B0496D52B4 | 58    | 0     | 0.16   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | F1C298E46944 | 58    | 0     | 0.16   |
| Dell      | Express Flash C... | 960 GB | AE8A0F8BC95B | 58    | 0     | 0.16   |
| Toshiba   | KXG60ZNV1T02       | 1 TB   | E4819AE8206B | 58    | 0     | 0.16   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 47FD491A114F | 58    | 0     | 0.16   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 5E4A35FB9ACA | 58    | 0     | 0.16   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 616A154A7BD7 | 58    | 0     | 0.16   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | AA2B3D2C9256 | 58    | 0     | 0.16   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | C4D209924DE5 | 58    | 0     | 0.16   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | D610009D205E | 58    | 0     | 0.16   |
| Dell      | Express Flash C... | 960 GB | 7B97779CCCE7 | 58    | 0     | 0.16   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | CF16CEB0DBC7 | 58    | 0     | 0.16   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 34222B4725C2 | 58    | 0     | 0.16   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 8053A9BC24FA | 58    | 0     | 0.16   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | DEF66AC09F07 | 58    | 0     | 0.16   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 948134B068FB | 57    | 0     | 0.16   |
| Samsung   | SSD 970 PRO        | 512 GB | 1F1E5D17AC2F | 57    | 0     | 0.16   |
| Intel     | SSDPED1K375GA      | 375 GB | F2230AEBE290 | 57    | 0     | 0.16   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 0EDAC70A06F6 | 57    | 0     | 0.16   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 83083B4C44F9 | 57    | 0     | 0.16   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | D069C09BDFEF | 57    | 0     | 0.16   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | F2FFB9B27DFE | 57    | 0     | 0.16   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | AB961C43FBDA | 57    | 0     | 0.16   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 78A05E264BD0 | 57    | 0     | 0.16   |
| WDC       | WUS4BB019D7P3E3    | 1.9 TB | 08FD296C00AF | 57    | 0     | 0.16   |
| WDC       | WUS4BB019D7P3E3    | 1.9 TB | 4872E8989C02 | 57    | 0     | 0.16   |
| WDC       | WUS4BB019D7P3E3    | 1.9 TB | 9A7898DF7DC4 | 57    | 0     | 0.16   |
| Wester... | WUS4BB038D7P3E1    | 3.8 TB | A418C4F28FB1 | 57    | 0     | 0.16   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 77F6A18E01E6 | 57    | 0     | 0.16   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 522AEBAEC102 | 57    | 0     | 0.16   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 6094A16817F7 | 57    | 0     | 0.16   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 689B12BBCA36 | 57    | 0     | 0.16   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | D0DB15D1FB1B | 57    | 0     | 0.16   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 14CA26EC2EBD | 57    | 0     | 0.16   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 91AC54C481F2 | 57    | 0     | 0.16   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | AA38CA49629F | 57    | 0     | 0.16   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | CD6534E5A0FE | 57    | 0     | 0.16   |
| Wester... | WUS4BB038D7P3E1    | 3.8 TB | 7CCE91ACB15D | 57    | 0     | 0.16   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 522A720D7A44 | 57    | 0     | 0.16   |
| Samsung   | MZ4LB3T8HALS-00003 | 3.8 TB | 625346FAF202 | 57    | 0     | 0.16   |
| WDC       | WDS500G3X0C-00SJG0 | 500 GB | FE6E1B616E38 | 57    | 0     | 0.16   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 0CBCB66ECD87 | 57    | 0     | 0.16   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 1B7AAC14CBB4 | 57    | 0     | 0.16   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 2773F1F063AA | 57    | 0     | 0.16   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 60B5257ECEB7 | 57    | 0     | 0.16   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 82C2C2A4909A | 57    | 0     | 0.16   |
| Dell      | Express Flash C... | 960 GB | 60C3ACBD32A3 | 57    | 0     | 0.16   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 3B50A5513694 | 57    | 0     | 0.16   |
| Intel     | SSDPE2KX010T8      | 1 TB   | 5CF3A7C910D2 | 56    | 0     | 0.16   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 1F7067E132D3 | 56    | 0     | 0.16   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 27DE0F96564D | 56    | 0     | 0.16   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 2EEBBBA057F2 | 56    | 0     | 0.16   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 47CB879C002F | 56    | 0     | 0.16   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 62697D1883A4 | 56    | 0     | 0.16   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 6543F93F0483 | 56    | 0     | 0.16   |
| WDC       | WUS4BB019D7P3E3    | 1.9 TB | 694CB1A07E1E | 56    | 0     | 0.16   |
| WDC       | WUS4BB019D7P3E3    | 1.9 TB | 8F2AFC0A4D97 | 56    | 0     | 0.16   |
| WDC       | WUS4BB019D7P3E3    | 1.9 TB | DD3309D49FED | 56    | 0     | 0.16   |
| WDC       | WUS4BB019D7P3E3    | 1.9 TB | 16D24F224ABE | 56    | 0     | 0.15   |
| WDC       | WUS4BB019D7P3E3    | 1.9 TB | 1B0A8237F63D | 56    | 0     | 0.15   |
| WDC       | WUS4BB019D7P3E3    | 1.9 TB | 5E3A63740E37 | 56    | 0     | 0.15   |
| Intel     | SSDPE2KX010T8      | 1 TB   | D60762468764 | 56    | 0     | 0.15   |
| Samsung   | SSD 970 EVO Plus   | 500 GB | AB9E066453FA | 56    | 0     | 0.15   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 809FC615E552 | 56    | 0     | 0.15   |
| Samsung   | MZPLJ1T6HBJR-000H3 | 1.6 TB | 53C390A7C94D | 55    | 0     | 0.15   |
| Samsung   | SSD 970 PRO        | 512 GB | B5E103485087 | 55    | 0     | 0.15   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 9627D264F41A | 55    | 0     | 0.15   |
| Wester... | WUS4BB038D7P3E1    | 3.8 TB | D29D6144B386 | 55    | 0     | 0.15   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | A6511974A5CF | 55    | 0     | 0.15   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 39F880C020F4 | 55    | 0     | 0.15   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 8EB3789E1C20 | 55    | 0     | 0.15   |
| Samsung   | SSD 970 PRO        | 512 GB | B4ED1F82C71C | 55    | 0     | 0.15   |
| Kingston  | SA2000M8500G       | 500 GB | D5312E772122 | 55    | 0     | 0.15   |
| Samsung   | MZPLJ6T4HALA-00007 | 6.4 TB | E8CE0F30B49C | 55    | 0     | 0.15   |
| Wester... | WUS4BB038D7P3E1    | 3.8 TB | D06A1436E6B4 | 55    | 0     | 0.15   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 0A04DA1025D9 | 55    | 0     | 0.15   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | D747853DD4EF | 55    | 0     | 0.15   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | E58B5FC32C36 | 55    | 0     | 0.15   |
| Samsung   | MZVLB1T0HBLR-00000 | 1 TB   | 9259F2A75AC3 | 55    | 0     | 0.15   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 61C075E2C538 | 54    | 0     | 0.15   |
| Wester... | WUS4BB038D7P3E1    | 3.8 TB | 790A5C88F673 | 54    | 0     | 0.15   |
| WDC       | WDS500G3X0C-00SJG0 | 500 GB | 21B59A57BEF4 | 54    | 0     | 0.15   |
| WDC       | WUS3BA119C7P3E3    | 1.9 TB | D7AECB533C91 | 54    | 0     | 0.15   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 806B14EF95BE | 54    | 0     | 0.15   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 4530F38E0BE5 | 54    | 0     | 0.15   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 5BC442368743 | 54    | 0     | 0.15   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 60B30D1D9B1F | 54    | 0     | 0.15   |
| WDC       | WUS4BB019D7P3E3    | 1.9 TB | 5CE7B4AD0D99 | 54    | 0     | 0.15   |
| WDC       | WUS4BB019D7P3E3    | 1.9 TB | 8AC9FE632463 | 54    | 0     | 0.15   |
| WDC       | WUS4BB019D7P3E3    | 1.9 TB | EF0C27199082 | 54    | 0     | 0.15   |
| Samsung   | MZPLJ1T6HBJR-000H3 | 1.6 TB | 9AFC6DAFA25B | 54    | 0     | 0.15   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 116E7DBE28BA | 53    | 0     | 0.15   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 34461311C73C | 53    | 0     | 0.15   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 359E82E3458A | 53    | 0     | 0.15   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 681A7537515F | 53    | 0     | 0.15   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 8454E94F15B6 | 53    | 0     | 0.15   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | D153912E0C8A | 53    | 0     | 0.15   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | E70FBFF6FA66 | 53    | 0     | 0.15   |
| WDC       | WUS4BB038D7P3E3    | 3.8 TB | D846145408B8 | 53    | 0     | 0.15   |
| Intel     | SSDPE2KX010T8      | 1 TB   | 5E89709CB746 | 53    | 0     | 0.15   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 5185D612DDA9 | 53    | 0     | 0.15   |
| Wester... | WUS4BB038D7P3E1    | 3.8 TB | 94C7D65D446E | 53    | 0     | 0.15   |
| Wester... | WUS4BB038D7P3E1    | 3.8 TB | ACE99FA0D987 | 53    | 0     | 0.15   |
| ADATA     | SX8200PNP          | 512 GB | FF3246ABF464 | 53    | 0     | 0.15   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 39F0C02DF7E5 | 53    | 0     | 0.15   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 5329FA18A8FD | 53    | 0     | 0.15   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 67C1233FC75D | 53    | 0     | 0.15   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 8B8A28399845 | 53    | 0     | 0.15   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | A3B97DB9E931 | 53    | 0     | 0.15   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | ECEDF32134B6 | 53    | 0     | 0.15   |
| Samsung   | SSD 980            | 500 GB | E90AFD09AF56 | 53    | 0     | 0.15   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 620207125B5B | 52    | 0     | 0.14   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 80400326A76E | 52    | 0     | 0.14   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 8774ABF93310 | 52    | 0     | 0.14   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 90299E6E69AA | 52    | 0     | 0.14   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | A29A35BF8578 | 52    | 0     | 0.14   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | B9629919EB32 | 52    | 0     | 0.14   |
| Huawei    | HWE56P43800M005N   | 800 GB | 61CF7B1896EF | 52    | 0     | 0.14   |
| WDC       | WUS4BB019D7P3E3    | 1.9 TB | 20A925820445 | 52    | 0     | 0.14   |
| WDC       | WUS4BB019D7P3E3    | 1.9 TB | B5904E75851D | 52    | 0     | 0.14   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | AAE452091275 | 52    | 0     | 0.14   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | C4F653AD921A | 52    | 0     | 0.14   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | E9F77E6079C9 | 52    | 0     | 0.14   |
| WDC       | PC SN720 SDAPNT... | 256 GB | 8A8CB42FFA02 | 52    | 0     | 0.14   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 631963312E4B | 52    | 0     | 0.14   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 7BAA0586B53C | 52    | 0     | 0.14   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 7FDF23850F41 | 52    | 0     | 0.14   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | EE59C841504D | 52    | 0     | 0.14   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | F82B4DCE7C43 | 52    | 0     | 0.14   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | F9EB2516EB55 | 52    | 0     | 0.14   |
| Wester... | WUS4BB038D7P3E1    | 3.8 TB | 46A7A2B47EA5 | 52    | 0     | 0.14   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 06EBCC6558D5 | 52    | 0     | 0.14   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 1EE5E1D089B8 | 52    | 0     | 0.14   |
| Intel     | SSDPE2KX010T8      | 1 TB   | EADD3BC52F52 | 52    | 0     | 0.14   |
| Intel     | SSDPE2MX450G7      | 450 GB | 0336DDEAE88A | 52    | 0     | 0.14   |
| Intel     | SSDPE2MX450G7      | 450 GB | 0F4A277DFE05 | 52    | 0     | 0.14   |
| Intel     | SSDPE2KX010T8      | 1 TB   | D99ED4A1DDF7 | 52    | 0     | 0.14   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 3FC42A41641E | 52    | 0     | 0.14   |
| Toshiba   | KXG60ZNV256G       | 256 GB | 36C3F933DD43 | 52    | 0     | 0.14   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | AE6B7C6D71A7 | 52    | 0     | 0.14   |
| Intel     | SSDPE2KX010T8      | 1 TB   | 03983F4CDD37 | 52    | 0     | 0.14   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 0A02EEBE2091 | 52    | 0     | 0.14   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 9195477991D5 | 52    | 0     | 0.14   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | B8D330AA0878 | 52    | 0     | 0.14   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | C559985985CC | 52    | 0     | 0.14   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | C97C099DC247 | 52    | 0     | 0.14   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | F1E81CF43A6C | 52    | 0     | 0.14   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | C1D5C347E0FA | 51    | 0     | 0.14   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 4F38D0319C61 | 51    | 0     | 0.14   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 0F643624DED3 | 51    | 0     | 0.14   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 3D1BCDE62D46 | 51    | 0     | 0.14   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 55A33EBFA908 | 51    | 0     | 0.14   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 79897F7DEB19 | 51    | 0     | 0.14   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | A969C2165D93 | 51    | 0     | 0.14   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | CC722F4F3A14 | 51    | 0     | 0.14   |
| Kingston  | SKC2500M81000G     | 1 TB   | 036F5BA02562 | 51    | 0     | 0.14   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 815CBEFA00A6 | 51    | 0     | 0.14   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 86E8324EC38F | 51    | 0     | 0.14   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | CB1A40F0691F | 51    | 0     | 0.14   |
| Wester... | WUS4BB038D7P3E1    | 3.8 TB | 966EE7F47F57 | 51    | 0     | 0.14   |
| Kingston  | SKC2500M8250G      | 250 GB | CC2CDA1F2650 | 51    | 0     | 0.14   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 2FEF9F352E60 | 51    | 0     | 0.14   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 398CFFC7F013 | 51    | 0     | 0.14   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 4E4CFA6FB09A | 51    | 0     | 0.14   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 87A44EC44A21 | 51    | 0     | 0.14   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | B33422BCAB6B | 51    | 0     | 0.14   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | EA64F851F189 | 51    | 0     | 0.14   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | BB1C9DB5E6A5 | 51    | 0     | 0.14   |
| Kingston  | SKC2500M81000G     | 1 TB   | 9E92048081FC | 51    | 0     | 0.14   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 4D4BD85CCD8B | 51    | 0     | 0.14   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | F18E18E1FA96 | 51    | 0     | 0.14   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 44CB24C8DD4D | 51    | 0     | 0.14   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 5A80229F8534 | 51    | 0     | 0.14   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 5D00E6F737C3 | 51    | 0     | 0.14   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 76382C91CC3A | 51    | 0     | 0.14   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | D1BB57873953 | 51    | 0     | 0.14   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | DF884F1F3B9A | 51    | 0     | 0.14   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 70221761E5C9 | 51    | 0     | 0.14   |
| Huawei    | HWE56P43800M005N   | 800 GB | 9B514E17FD3B | 51    | 0     | 0.14   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 07115410087A | 51    | 0     | 0.14   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 13B129CFD8DD | 51    | 0     | 0.14   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 3ED0E23A1BC9 | 51    | 0     | 0.14   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | E179D3EA3FDA | 51    | 0     | 0.14   |
| Intel     | SSDPEKKA512G8      | 512 GB | 74496095E2E9 | 51    | 0     | 0.14   |
| Samsung   | MZVLB1T0HBLR-00000 | 1 TB   | 86F4B1FC9E12 | 50    | 0     | 0.14   |
| Dell      | Express Flash N... | 6.4 TB | C7C39603E0DD | 50    | 0     | 0.14   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | EC061A1856CE | 50    | 0     | 0.14   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 5097E1EA912D | 50    | 0     | 0.14   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | BEE7EEC7BF47 | 50    | 0     | 0.14   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | C9FA070EA3AF | 50    | 0     | 0.14   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 26FA4F818C8B | 50    | 0     | 0.14   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 9F0B38BEAE37 | 50    | 0     | 0.14   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | E9997EAC2866 | 50    | 0     | 0.14   |
| Intel     | SSDPEKKA512G8      | 512 GB | 45C3363B39DF | 50    | 0     | 0.14   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | E3096A7C2BE7 | 50    | 0     | 0.14   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 7A1DD52A9EDF | 50    | 0     | 0.14   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | DB970926BC2E | 50    | 0     | 0.14   |
| Intel     | SSDPEKKA512G8      | 512 GB | 820B37082679 | 50    | 0     | 0.14   |
| Samsung   | MZ1LB3T8HMLA-00007 | 3.8 TB | 71A072153A76 | 202   | 3     | 0.14   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 215D8CF5DC36 | 50    | 0     | 0.14   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 50EEFE319E11 | 50    | 0     | 0.14   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 6BF765C48541 | 50    | 0     | 0.14   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 82BE2B266639 | 50    | 0     | 0.14   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | A6FA4C712826 | 50    | 0     | 0.14   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | A9809813C857 | 50    | 0     | 0.14   |
| Wester... | WUS4BB038D7P3E1    | 3.8 TB | 4D130371EB24 | 50    | 0     | 0.14   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 6B08C5769B64 | 50    | 0     | 0.14   |
| Wester... | WUS4BB038D7P3E1    | 3.8 TB | 1498C3712BAE | 50    | 0     | 0.14   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 83BD6B2D66D9 | 50    | 0     | 0.14   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 5399EE7ABBCB | 50    | 0     | 0.14   |
| WDC       | WUS3BA119C7P3E3    | 1.9 TB | CEB3749372FA | 50    | 0     | 0.14   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 36A63262EBEE | 50    | 0     | 0.14   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | C5CFC057FE32 | 50    | 0     | 0.14   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | E87C433CEAA7 | 50    | 0     | 0.14   |
| Intel     | SSDPE2MX450G7      | 450 GB | F723A7886DBF | 49    | 0     | 0.14   |
| Micron    | 7300_MTFDHBE6T4TDG | 6.4 TB | B383685B7BB6 | 49    | 0     | 0.14   |
| Micron    | 7300_MTFDHBE6T4TDG | 6.4 TB | E2EE817234A6 | 49    | 0     | 0.14   |
| Wester... | WUS4BB038D7P3E1    | 3.8 TB | 14FFFA4C5677 | 49    | 0     | 0.14   |
| Wester... | WUS4BB038D7P3E1    | 3.8 TB | DAE7C426FC89 | 49    | 0     | 0.14   |
| Samsung   | MZVLB1T0HBLR-00000 | 1 TB   | 2A0080AE700F | 49    | 0     | 0.14   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 06677EEB0D48 | 49    | 0     | 0.14   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 37B816A29C1D | 49    | 0     | 0.14   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 390EE97C100B | 49    | 0     | 0.14   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 93E3010A007B | 49    | 0     | 0.14   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | C377E7238BF7 | 49    | 0     | 0.14   |
| Intel     | SSDPE2KX010T8      | 1 TB   | 21D0F4DAC649 | 49    | 0     | 0.14   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 45358D2BD031 | 49    | 0     | 0.14   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | DC9805989BD2 | 49    | 0     | 0.14   |
| Dell      | Express Flash C... | 960 GB | 8EB52630EC40 | 48    | 0     | 0.13   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | F6FDE4DC5780 | 48    | 0     | 0.13   |
| Micron    | 7300_MTFDHBE6T4TDG | 6.4 TB | 58ABB70C79F5 | 48    | 0     | 0.13   |
| Kingston  | SA2000M81000G      | 1 TB   | 40780A431BC5 | 48    | 0     | 0.13   |
| Kingston  | SA2000M81000G      | 1 TB   | F06B66F781F7 | 48    | 0     | 0.13   |
| Micron    | 7300_MTFDHBE6T4TDG | 6.4 TB | 2A5556232EA7 | 48    | 0     | 0.13   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 20B71EFBAABF | 48    | 0     | 0.13   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | EE9A65813269 | 48    | 0     | 0.13   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 3D79B88F8B11 | 48    | 0     | 0.13   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 4E2DAD7A82EB | 48    | 0     | 0.13   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 75A3048EA3A7 | 48    | 0     | 0.13   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 9DBC5FF3B05C | 48    | 0     | 0.13   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | C97523220B23 | 48    | 0     | 0.13   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | CCD34FD3ECAA | 48    | 0     | 0.13   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | DD32A80FECE6 | 48    | 0     | 0.13   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | E2CDC10BDFCD | 48    | 0     | 0.13   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 28F1CA6021B4 | 48    | 0     | 0.13   |
| Wester... | WUS4BB038D7P3E1    | 3.8 TB | 063FF7E4B88D | 48    | 0     | 0.13   |
| Wester... | WUS4BB038D7P3E1    | 3.8 TB | EC3E9FEDFD2D | 48    | 0     | 0.13   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 05E608D624AD | 48    | 0     | 0.13   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 49CEE990EDF9 | 48    | 0     | 0.13   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 03435433A6E5 | 48    | 0     | 0.13   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 1DAB61D05C67 | 48    | 0     | 0.13   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 6181AE5DA4BF | 48    | 0     | 0.13   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 7CB836929C9C | 48    | 0     | 0.13   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | A1288E27B3CA | 48    | 0     | 0.13   |
| Micron    | 7300_MTFDHBE6T4TDG | 6.4 TB | DF147F45757F | 48    | 0     | 0.13   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 83793252A42D | 48    | 0     | 0.13   |
| Micron    | 7300_MTFDHBE1T6TDG | 1.6 TB | 22B90BE3D402 | 48    | 0     | 0.13   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | C356FE9DD001 | 48    | 0     | 0.13   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 222730842FBE | 47    | 0     | 0.13   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 38247A051510 | 47    | 0     | 0.13   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | DA6622F0FC08 | 47    | 0     | 0.13   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 17D2E9898F8A | 47    | 0     | 0.13   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 7E1CA36F0D1D | 47    | 0     | 0.13   |
| Micron    | 7300_MTFDHBE6T4TDG | 6.4 TB | 4296CFDB6610 | 47    | 0     | 0.13   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 035C0B78B255 | 47    | 0     | 0.13   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 107A58443265 | 47    | 0     | 0.13   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 1D1C20137872 | 47    | 0     | 0.13   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 279D60574A2F | 47    | 0     | 0.13   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 75DC484EA59E | 47    | 0     | 0.13   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 9C254DF01ED1 | 47    | 0     | 0.13   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | E06361D0F33E | 47    | 0     | 0.13   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | F72674BD13B5 | 47    | 0     | 0.13   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | F8F3533015B6 | 47    | 0     | 0.13   |
| WDC       | CL SN720 SDAQNT... | 1 TB   | A6B781208068 | 47    | 0     | 0.13   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 0356ADE5E5B8 | 47    | 0     | 0.13   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 39C34F26A9D7 | 47    | 0     | 0.13   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | A996F69B4B61 | 47    | 0     | 0.13   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | AA8B40D565D6 | 47    | 0     | 0.13   |
| Toshiba   | KXG60ZNV256G       | 256 GB | B399393997CD | 47    | 0     | 0.13   |
| Toshiba   | KXG60ZNV256G       | 256 GB | FF0FD400DDDF | 47    | 0     | 0.13   |
| WDC       | PC SN720 SDAPNT... | 256 GB | 74E0FC5A3F66 | 47    | 0     | 0.13   |
| WDC       | PC SN520 SDAPNU... | 256 GB | 5821BC84E521 | 47    | 0     | 0.13   |
| WDC       | WUS4BB038D7P3E3    | 3.8 TB | 4FDEE0FBB2A6 | 47    | 0     | 0.13   |
| WDC       | WUS4BB038D7P3E3    | 3.8 TB | 6F1AEB3F4CAB | 47    | 0     | 0.13   |
| WDC       | WUS4BB038D7P3E3    | 3.8 TB | DEC05CE92AA8 | 47    | 0     | 0.13   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 1D22BB8FABE7 | 47    | 0     | 0.13   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 242CF1E181AB | 47    | 0     | 0.13   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 372E3FD58F37 | 47    | 0     | 0.13   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 762C73794E5C | 47    | 0     | 0.13   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 7C4C3CA1F5A8 | 47    | 0     | 0.13   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | C51B98774DA1 | 47    | 0     | 0.13   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | D60B8E2BA6A9 | 47    | 0     | 0.13   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | EB19B14482B7 | 47    | 0     | 0.13   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | F01293BF6812 | 47    | 0     | 0.13   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | FC15E226B565 | 47    | 0     | 0.13   |
| Wester... | WUS4BB096D7P3E3    | 960 GB | FC15E226B565 | 47    | 0     | 0.13   |
| Micron    | 2200_MTFDHBA512TCK | 512 GB | 0817C685A016 | 47    | 0     | 0.13   |
| Dell      | Express Flash C... | 960 GB | 1D3FBAD04E0D | 47    | 0     | 0.13   |
| Samsung   | MZPLJ1T6HBJR-000H3 | 1.6 TB | 88E996DBB5BB | 47    | 0     | 0.13   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 0291EA4EA165 | 47    | 0     | 0.13   |
| Samsung   | MZVLB256HBHQ-00000 | 256 GB | 892FBCAE8AD8 | 47    | 0     | 0.13   |
| WDC       | CL SN720 SDAQNT... | 1 TB   | 85DA11B7E070 | 47    | 0     | 0.13   |
| Wester... | WUS4BB038D7P3E1    | 3.8 TB | 1C5A8C93AB06 | 47    | 0     | 0.13   |
| Wester... | WUS4BB038D7P3E1    | 3.8 TB | 7F06A4BADF4B | 47    | 0     | 0.13   |
| Wester... | WUS4BB038D7P3E1    | 3.8 TB | 895018F49C23 | 47    | 0     | 0.13   |
| Wester... | WUS4BB038D7P3E1    | 3.8 TB | CAA6D351E3C0 | 47    | 0     | 0.13   |
| Wester... | WUS4BB038D7P3E1    | 3.8 TB | CB8E6A36656F | 47    | 0     | 0.13   |
| Wester... | WUS4BB038D7P3E1    | 3.8 TB | E80ED46E91AA | 47    | 0     | 0.13   |
| Wester... | WUS4BB038D7P3E1    | 3.8 TB | EE4DCF60C596 | 47    | 0     | 0.13   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 1939741F618B | 47    | 0     | 0.13   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 66856A60C627 | 47    | 0     | 0.13   |
| Corsair   | MP400              | 1 TB   | F2005A3EF7C1 | 46    | 0     | 0.13   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 85438651FA2B | 46    | 0     | 0.13   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | C56749963CD3 | 46    | 0     | 0.13   |
| Toshiba   | KXG60ZNV256G       | 256 GB | 858D4D97DF98 | 46    | 0     | 0.13   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 11F7F090EE7A | 46    | 0     | 0.13   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 54A3800A66FB | 46    | 0     | 0.13   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 73567723030F | 46    | 0     | 0.13   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 7B9CBF43ABE9 | 46    | 0     | 0.13   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 81D8E60C65E0 | 46    | 0     | 0.13   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | EFE14CD2A018 | 46    | 0     | 0.13   |
| Dell      | Express Flash C... | 960 GB | 79323CBB69D9 | 46    | 0     | 0.13   |
| Dell      | Express Flash C... | 960 GB | F77059E20190 | 46    | 0     | 0.13   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 2D1457CED687 | 46    | 0     | 0.13   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 96C119713627 | 46    | 0     | 0.13   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 9CC590A1E4B0 | 46    | 0     | 0.13   |
| WDC       | CL SN720 SDAQNT... | 512 GB | 18D67394BFD2 | 46    | 0     | 0.13   |
| WDC       | CL SN720 SDAQNT... | 512 GB | 7FE59A9C7998 | 46    | 0     | 0.13   |
| WDC       | PC SN720 SDAPNT... | 256 GB | 63147B4D73D0 | 46    | 0     | 0.13   |
| ADATA     | SX8200PNP          | 2 TB   | CF8898BDE20B | 46    | 0     | 0.13   |
| Micron    | 2200_MTFDHBA512TCK | 512 GB | D1B0F19328E5 | 46    | 0     | 0.13   |
| SK hynix  | PC601 HFS256GD9... | 256 GB | 7C55AAB4B400 | 46    | 0     | 0.13   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 474DA5517656 | 46    | 0     | 0.13   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 042338AC7134 | 46    | 0     | 0.13   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | A02573311598 | 46    | 0     | 0.13   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | E803CC9C47A6 | 46    | 0     | 0.13   |
| Corsair   | MP400              | 1 TB   | EBAE44E61E0C | 46    | 0     | 0.13   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | 20FD25DAD03B | 46    | 0     | 0.13   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | 260E9604ECB2 | 46    | 0     | 0.13   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | AE06C8BC9228 | 46    | 0     | 0.13   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | DD4C198886AF | 46    | 0     | 0.13   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | F2C1EE2E7BE4 | 46    | 0     | 0.13   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | FDC8A3853E24 | 46    | 0     | 0.13   |
| Toshiba   | KXG60ZNV256G       | 256 GB | 4BBDBC590A54 | 46    | 0     | 0.13   |
| Toshiba   | KXG60ZNV256G       | 256 GB | EFEA20C51065 | 46    | 0     | 0.13   |
| Wester... | WUS4BB038D7P3E1    | 3.8 TB | 4E671E3F2956 | 46    | 0     | 0.13   |
| Wester... | WUS4BB038D7P3E1    | 3.8 TB | 8007868DED14 | 46    | 0     | 0.13   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | D89FCEFFD457 | 46    | 0     | 0.13   |
| Dell      | Express Flash C... | 960 GB | E56C15D2E65C | 46    | 0     | 0.13   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 49A7DCD2BED6 | 46    | 0     | 0.13   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 4C48718BDF0B | 46    | 0     | 0.13   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 71F23BC08D32 | 46    | 0     | 0.13   |
| Samsung   | MZVLB1T0HBLR-00000 | 1 TB   | 8E3B3846463A | 46    | 0     | 0.13   |
| Corsair   | MP400              | 1 TB   | FCBEF748D2CC | 46    | 0     | 0.13   |
| Intel     | SSDPE2KX010T8      | 1 TB   | E0CF669B666C | 46    | 0     | 0.13   |
| Samsung   | MZPLJ1T6HBJR-000H3 | 1.6 TB | 1792784D0579 | 46    | 0     | 0.13   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 0B87C274A7D8 | 46    | 0     | 0.13   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 617EE7563F1A | 46    | 0     | 0.13   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 6EFDB2B28CB9 | 46    | 0     | 0.13   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 713AEA8FAA3A | 46    | 0     | 0.13   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 916B94D98F4E | 46    | 0     | 0.13   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | DBC11AE34E70 | 46    | 0     | 0.13   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | ECD316E50D47 | 46    | 0     | 0.13   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 3A4D7BD84C20 | 46    | 0     | 0.13   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 15C13D283828 | 46    | 0     | 0.13   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 8088C66B8217 | 46    | 0     | 0.13   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | DDB11D6D1D78 | 46    | 0     | 0.13   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | 14477C1DFB73 | 46    | 0     | 0.13   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | 25B318284CC5 | 46    | 0     | 0.13   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | 39C7DD5A73B8 | 46    | 0     | 0.13   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | 4E17DA563539 | 46    | 0     | 0.13   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | F48D1B2B9D73 | 46    | 0     | 0.13   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | F5ED1298263E | 46    | 0     | 0.13   |
| Toshiba   | KXG60ZNV256G       | 256 GB | A7D85CC902F9 | 46    | 0     | 0.13   |
| Toshiba   | KXG60ZNV256G       | 256 GB | D6FD0265D5AB | 46    | 0     | 0.13   |
| Intel     | SSDPED1K375GA      | 375 GB | 050C74CF0952 | 46    | 0     | 0.13   |
| Intel     | SSDPED1K375GA      | 375 GB | 488FF14DA844 | 46    | 0     | 0.13   |
| Intel     | SSDPEKNW020T8      | 2 TB   | CEEC98771CE8 | 46    | 0     | 0.13   |
| Micron    | 2200_MTFDHBA512TCK | 512 GB | 309CA2234962 | 46    | 0     | 0.13   |
| Micron    | 2200_MTFDHBA512TCK | 512 GB | 00B9112AEC9E | 46    | 0     | 0.13   |
| Dell      | Express Flash C... | 960 GB | 47A4D7316060 | 46    | 0     | 0.13   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | 93660D69F4B3 | 46    | 0     | 0.13   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | BE36DFF5F3B9 | 46    | 0     | 0.13   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | E11BD3E929CA | 46    | 0     | 0.13   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | E7D6A6D0536F | 46    | 0     | 0.13   |
| Toshiba   | KXG60ZNV256G       | 256 GB | 7002D627B40C | 46    | 0     | 0.13   |
| Dell      | Express Flash C... | 960 GB | 04F70A60EC22 | 45    | 0     | 0.13   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 7085B3B0003B | 45    | 0     | 0.13   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | D7345F11440C | 45    | 0     | 0.13   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | C831107E3FD7 | 45    | 0     | 0.13   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | DF5535AC8B8A | 45    | 0     | 0.13   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 34E4A5DF202E | 45    | 0     | 0.13   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | BEDBED037F95 | 45    | 0     | 0.13   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | E3DFAF917AC0 | 45    | 0     | 0.13   |
| Wester... | WUS4BB038D7P3E1    | 3.8 TB | 4399D3467901 | 45    | 0     | 0.13   |
| Wester... | WUS4BB038D7P3E1    | 3.8 TB | 69642526623D | 45    | 0     | 0.13   |
| Wester... | WUS4BB038D7P3E1    | 3.8 TB | 6E9A74B75868 | 45    | 0     | 0.13   |
| Wester... | WUS4BB038D7P3E1    | 3.8 TB | D6811633124A | 45    | 0     | 0.13   |
| Wester... | WUS4BB038D7P3E1    | 3.8 TB | EE97F259102D | 45    | 0     | 0.13   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 993DABD45881 | 45    | 0     | 0.13   |
| Team      | TM8FP6128G         | 128 GB | AFB50DFD3FB7 | 45    | 0     | 0.13   |
| Toshiba   | KXG60PNV2T04       | 2 TB   | 0CB176B65812 | 45    | 0     | 0.13   |
| WDC       | CL SN720 SDAQNT... | 1 TB   | 5932FEA7DC76 | 45    | 0     | 0.13   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 8E1510A68B5C | 45    | 0     | 0.13   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 9D7AEDFE502E | 45    | 0     | 0.13   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 9F119A5DAEF8 | 45    | 0     | 0.13   |
| Dell      | Express Flash C... | 960 GB | 3B5832DE2270 | 45    | 0     | 0.13   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 033B4DD6E0ED | 45    | 0     | 0.12   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 07833850AEA0 | 45    | 0     | 0.12   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 27C791E2BF0F | 45    | 0     | 0.12   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 4A3A0FD5DDFE | 45    | 0     | 0.12   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 546DC9D4B148 | 45    | 0     | 0.12   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 5CFA7B7C57EC | 45    | 0     | 0.12   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 9E7ADEE9CFC8 | 45    | 0     | 0.12   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | EFAA32E797D3 | 45    | 0     | 0.12   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 28608A1E2702 | 45    | 0     | 0.12   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | CA9CC49D8762 | 45    | 0     | 0.12   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | DC4FEF1FFC6F | 45    | 0     | 0.12   |
| Toshiba   | KXG60ZNV256G       | 256 GB | F4F5C11BF27D | 45    | 0     | 0.12   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 90DFC0386D7F | 45    | 0     | 0.12   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | EA94AD0A1180 | 45    | 0     | 0.12   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | EC57D05D4C05 | 45    | 0     | 0.12   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 332D6979B698 | 45    | 0     | 0.12   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | C84C5BAD7997 | 45    | 0     | 0.12   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 54C69E483D70 | 45    | 0     | 0.12   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 5C64E60A42DC | 45    | 0     | 0.12   |
| Toshiba   | KXG60ZNV256G       | 256 GB | 3464092DC707 | 45    | 0     | 0.12   |
| WDC       | CL SN720 SDAQNT... | 1 TB   | DF197608077B | 45    | 0     | 0.12   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 06D518EC2325 | 45    | 0     | 0.12   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 9FA67F016682 | 45    | 0     | 0.12   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | A51993C55985 | 45    | 0     | 0.12   |
| Toshiba   | KXG60ZNV256G       | 256 GB | 19FF1C76695F | 45    | 0     | 0.12   |
| Toshiba   | KXG60ZNV256G       | 256 GB | 3B6A2D6E191E | 45    | 0     | 0.12   |
| Toshiba   | KXG60ZNV256G       | 256 GB | 8051D17C2DA8 | 45    | 0     | 0.12   |
| Wester... | WUS4BB038D7P3E1    | 3.8 TB | 67E1FE010D09 | 45    | 0     | 0.12   |
| Dell      | Express Flash C... | 960 GB | 0641827BEFAC | 45    | 0     | 0.12   |
| Dell      | Express Flash C... | 960 GB | 25562FAF41BD | 45    | 0     | 0.12   |
| Dell      | Express Flash C... | 960 GB | 54150571149D | 45    | 0     | 0.12   |
| Dell      | Express Flash C... | 960 GB | 64AEAF151426 | 45    | 0     | 0.12   |
| Dell      | Express Flash C... | 960 GB | 7A6A05E57EFB | 45    | 0     | 0.12   |
| Dell      | Express Flash C... | 960 GB | 7D62057406CF | 45    | 0     | 0.12   |
| Dell      | Express Flash C... | 960 GB | CE05904692C8 | 45    | 0     | 0.12   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | EEB5DE5CB041 | 45    | 0     | 0.12   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 3F39EE14BE62 | 45    | 0     | 0.12   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 56860EBE66D0 | 45    | 0     | 0.12   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 6E0F474CC038 | 45    | 0     | 0.12   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 8DC62C333913 | 45    | 0     | 0.12   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | C3BDE5D97D47 | 271   | 5     | 0.12   |
| WDC       | WUS4BB096D7P3E4    | 960 GB | B920D111C4B2 | 45    | 0     | 0.12   |
| WDC       | WUS4BB096D7P3E4    | 960 GB | BA89193FACA8 | 45    | 0     | 0.12   |
| Samsung   | MZQLB1T9HAJR-000AZ | 1.9 TB | 50BA49B34CF5 | 45    | 0     | 0.12   |
| Dell      | Express Flash C... | 960 GB | F50E385A28A3 | 45    | 0     | 0.12   |
| Toshiba   | KXG60ZNV256G       | 256 GB | AEC05E15F50F | 45    | 0     | 0.12   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 2C10EDFC7830 | 45    | 0     | 0.12   |
| ADATA     | SX8200PNP          | 2 TB   | F836DFB74421 | 44    | 0     | 0.12   |
| Toshiba   | KXG60ZNV256G       | 256 GB | 64F96544B30F | 44    | 0     | 0.12   |
| Dell      | Express Flash C... | 960 GB | BC6A8347288E | 44    | 0     | 0.12   |
| Corsair   | MP400              | 1 TB   | 44775F3D01CD | 44    | 0     | 0.12   |
| Dell      | Express Flash C... | 960 GB | 6F8918583868 | 44    | 0     | 0.12   |
| WDC       | PC SN720 SDAPNT... | 256 GB | ADB21AC179D4 | 44    | 0     | 0.12   |
| Dell      | Express Flash C... | 960 GB | 94B3C2326928 | 44    | 0     | 0.12   |
| Samsung   | MZPLJ1T6HBJR-000H3 | 1.6 TB | C698BDE51D27 | 44    | 0     | 0.12   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 184876DCF6B3 | 44    | 0     | 0.12   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 27D342EDCA03 | 44    | 0     | 0.12   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 2EF972E4C5AA | 44    | 0     | 0.12   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 3E7538073991 | 44    | 0     | 0.12   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 7CBCF997F2B6 | 44    | 0     | 0.12   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 8225B1225276 | 44    | 0     | 0.12   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | BE2CCC3EF3F8 | 44    | 0     | 0.12   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | D09FDF103078 | 44    | 0     | 0.12   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | DF67913821F6 | 44    | 0     | 0.12   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | E5581624FE6D | 44    | 0     | 0.12   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 2B98B4627E71 | 44    | 0     | 0.12   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 3B5BC6002A72 | 44    | 0     | 0.12   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | DC501A65C512 | 44    | 0     | 0.12   |
| WDC       | WDS500G3X0C-00SJG0 | 500 GB | 81D4E62B9910 | 44    | 0     | 0.12   |
| WDC       | CL SN720 SDAQNT... | 512 GB | 3173A219976D | 44    | 0     | 0.12   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 8075C1ECD022 | 44    | 0     | 0.12   |
| Samsung   | MZVLB512HAJQ-00000 | 512 GB | 95544377518D | 44    | 0     | 0.12   |
| Wester... | WUS4BB038D7P3E1    | 3.8 TB | 2640A79A104C | 44    | 0     | 0.12   |
| Wester... | WUS4BB038D7P3E1    | 3.8 TB | 7CBF0C8A7902 | 44    | 0     | 0.12   |
| Wester... | WUS4BB038D7P3E1    | 3.8 TB | 983C4EA31EA6 | 44    | 0     | 0.12   |
| Wester... | WUS4BB038D7P3E1    | 3.8 TB | A7EE929F4E67 | 44    | 0     | 0.12   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 124806F43F10 | 44    | 0     | 0.12   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 8B925B4D6588 | 44    | 0     | 0.12   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | E32CFC0F7E9F | 44    | 0     | 0.12   |
| Dell      | Express Flash C... | 960 GB | 5124DA913B62 | 44    | 0     | 0.12   |
| Intel     | SSDPE2KX010T8      | 1 TB   | 313B8DE0FBA3 | 44    | 0     | 0.12   |
| Intel     | SSDPE2KX010T8      | 1 TB   | B040770C2005 | 44    | 0     | 0.12   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 899F33C20106 | 44    | 0     | 0.12   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | B6DAC663D81B | 44    | 0     | 0.12   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | BC76BEBAD218 | 44    | 0     | 0.12   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 83DB8D1984A5 | 44    | 0     | 0.12   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | A761CAD772EF | 44    | 0     | 0.12   |
| WDC       | CL SN720 SDAQNT... | 512 GB | D03A1CAEFB4F | 44    | 0     | 0.12   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 17D081008467 | 44    | 0     | 0.12   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 6319A13EA44C | 44    | 0     | 0.12   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 85EB747FE176 | 44    | 0     | 0.12   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 95FFAB612D90 | 44    | 0     | 0.12   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | BBC689BFAB9F | 44    | 0     | 0.12   |
| Samsung   | MZPLJ6T4HALA-00007 | 6.4 TB | AEBE8BBB2067 | 44    | 0     | 0.12   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | F4AFD50EC016 | 44    | 0     | 0.12   |
| WDC       | WUS3BA119C7P3E3    | 1.9 TB | EF68D07C7E43 | 44    | 0     | 0.12   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 42F84932FE59 | 44    | 0     | 0.12   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 576B96CE84E1 | 44    | 0     | 0.12   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 6717C4393021 | 44    | 0     | 0.12   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 6E542557E4B7 | 44    | 0     | 0.12   |
| Toshiba   | KXG60PNV2T04       | 2 TB   | 9C02A35A3A26 | 44    | 0     | 0.12   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 55C1BE9DD0E4 | 44    | 0     | 0.12   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 56C822E4061B | 44    | 0     | 0.12   |
| Dell      | Express Flash C... | 960 GB | C2B2C925C049 | 44    | 0     | 0.12   |
| Micron    | 7300_MTFDHBE6T4TDG | 6.4 TB | 9D6FACE3C691 | 44    | 0     | 0.12   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 2A17B23A496D | 44    | 0     | 0.12   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 309ADF7C54D5 | 44    | 0     | 0.12   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | B161B3A6E202 | 44    | 0     | 0.12   |
| WDC       | WUS4BB038D7P3E3    | 3.8 TB | 2E7DB537AA57 | 44    | 0     | 0.12   |
| WDC       | WUS4BB038D7P3E3    | 3.8 TB | 57CAB9492EBD | 44    | 0     | 0.12   |
| WDC       | WUS4BB038D7P3E3    | 3.8 TB | CB2E4CE6A1AF | 44    | 0     | 0.12   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 7682AF2E00F1 | 44    | 0     | 0.12   |
| Samsung   | SSD 970 EVO Plus   | 2 TB   | E01BB0BB044F | 44    | 0     | 0.12   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 748FC5806C90 | 44    | 0     | 0.12   |
| Dell      | Express Flash C... | 960 GB | 57C0F16C9F7C | 44    | 0     | 0.12   |
| Dell      | Express Flash C... | 960 GB | F301C988BB67 | 43    | 0     | 0.12   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 1C5BABA70B43 | 43    | 0     | 0.12   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 84DF3A00AF14 | 43    | 0     | 0.12   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | C9FEB816BE15 | 43    | 0     | 0.12   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | F26DDBF54DEC | 43    | 0     | 0.12   |
| Toshiba   | KXG60ZNV256G       | 256 GB | 03090CE31BFF | 43    | 0     | 0.12   |
| Toshiba   | KXG60ZNV256G       | 256 GB | 0C0E9706AF01 | 43    | 0     | 0.12   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 41CAD381C4E3 | 43    | 0     | 0.12   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | F96C3DE40505 | 43    | 0     | 0.12   |
| Micron    | 7300_MTFDHBE6T4TDG | 6.4 TB | 75C63BC713D1 | 43    | 0     | 0.12   |
| Samsung   | MZVLB512HAJQ-00000 | 512 GB | AAB28F3CB726 | 43    | 0     | 0.12   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 479E581E702F | 43    | 0     | 0.12   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | A453C67E015F | 43    | 0     | 0.12   |
| Dell      | Express Flash C... | 960 GB | C244A11399A6 | 43    | 0     | 0.12   |
| Toshiba   | KXG60ZNV256G       | 256 GB | 6ED0A05BE18A | 43    | 0     | 0.12   |
| WDC       | CL SN720 SDAQNT... | 512 GB | 0D794C25AB17 | 43    | 0     | 0.12   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | A99D2F0EBC95 | 43    | 0     | 0.12   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | A99D2F0EBC95 | 43    | 0     | 0.12   |
| Micron    | 7300_MTFDHBE6T4TDG | 6.4 TB | 0EE731283A73 | 43    | 0     | 0.12   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 12C579D44929 | 43    | 0     | 0.12   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 2C20A682FC2C | 43    | 0     | 0.12   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 3751876F0FF1 | 43    | 0     | 0.12   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 38B01FB4D3E9 | 43    | 0     | 0.12   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | A8F0B8FB4C39 | 43    | 0     | 0.12   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | CB89FB166F0A | 43    | 0     | 0.12   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | CF958C5E65FB | 43    | 0     | 0.12   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | EE34FBBE3962 | 43    | 0     | 0.12   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | EF9150F8B7C2 | 43    | 0     | 0.12   |
| Wester... | WUS4BB038D7P3E1    | 3.8 TB | 668D1BB5F70E | 43    | 0     | 0.12   |
| Micron    | 7300_MTFDHBE6T4TDG | 6.4 TB | 21BBAF2CCA9A | 43    | 0     | 0.12   |
| Corsair   | MP400              | 1 TB   | 4B1D2F2397A9 | 43    | 0     | 0.12   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 307A325BD0CE | 43    | 0     | 0.12   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 3753A3643EB5 | 43    | 0     | 0.12   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 7EEEDA259B72 | 43    | 0     | 0.12   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | E355E476D0A1 | 43    | 0     | 0.12   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 307A325BD0CE | 43    | 0     | 0.12   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 3753A3643EB5 | 43    | 0     | 0.12   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 7EEEDA259B72 | 43    | 0     | 0.12   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | E355E476D0A1 | 43    | 0     | 0.12   |
| Dell      | Express Flash C... | 960 GB | 51D78239186E | 43    | 0     | 0.12   |
| Samsung   | SSD 970 EVO Plus   | 1 TB   | E016CF80BF56 | 43    | 0     | 0.12   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | 01CDBF08646A | 43    | 0     | 0.12   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | 2BDF1F2D8A41 | 43    | 0     | 0.12   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | 302B2BA2597E | 43    | 0     | 0.12   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | 5915F33DCD5E | 43    | 0     | 0.12   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | AF7068058CDB | 43    | 0     | 0.12   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | F309FE5F62AD | 43    | 0     | 0.12   |
| Toshiba   | KXG60ZNV256G       | 256 GB | D423B74649BD | 43    | 0     | 0.12   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 56E296C2A949 | 43    | 0     | 0.12   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 6C607DE6F2C3 | 43    | 0     | 0.12   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 6D55FAD17913 | 43    | 0     | 0.12   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 9A0FB3ED8F59 | 43    | 0     | 0.12   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 0512680ACB5E | 43    | 0     | 0.12   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 2C946587187F | 43    | 0     | 0.12   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 4648A81DAF0F | 43    | 0     | 0.12   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 5023084A0867 | 43    | 0     | 0.12   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 605D27A89984 | 43    | 0     | 0.12   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 7384663BC960 | 43    | 0     | 0.12   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 9103E09F854E | 43    | 0     | 0.12   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 92C8287048A2 | 43    | 0     | 0.12   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | C90ADF8F99AF | 43    | 0     | 0.12   |
| Samsung   | SSD 980            | 500 GB | 604F89477E69 | 43    | 0     | 0.12   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 053F07A1CA72 | 43    | 0     | 0.12   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 315C9D74C9D6 | 43    | 0     | 0.12   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 35FF215BCE9B | 43    | 0     | 0.12   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 5298A54EE84E | 43    | 0     | 0.12   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | AFA51BD03703 | 43    | 0     | 0.12   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | C533609C251D | 43    | 0     | 0.12   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 35FEC793F84E | 43    | 0     | 0.12   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 444223B4052D | 43    | 0     | 0.12   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | EAE515E374C0 | 43    | 0     | 0.12   |
| Dell      | Express Flash C... | 960 GB | 85D97875BE56 | 43    | 0     | 0.12   |
| Micron    | 7300_MTFDHBE6T4TDG | 6.4 TB | CF5A7069552F | 43    | 0     | 0.12   |
| Dell      | Express Flash N... | 1.6 TB | E6ED2A716A1C | 43    | 0     | 0.12   |
| Toshiba   | KXG60ZNV1T02       | 1 TB   | 0B7AF6A382B9 | 43    | 0     | 0.12   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 008D318CCA1C | 43    | 0     | 0.12   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 4BA2C4AA4AC9 | 43    | 0     | 0.12   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 5019B67A5BA2 | 43    | 0     | 0.12   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 51791C91AE97 | 43    | 0     | 0.12   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 6547CD2B77F1 | 43    | 0     | 0.12   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | 932E35C9DB4B | 43    | 0     | 0.12   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | B9D099F7940A | 43    | 0     | 0.12   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | C76D012BD999 | 43    | 0     | 0.12   |
| WDC       | WUS4BB096D7P3E3    | 960 GB | F01AB6E306BF | 43    | 0     | 0.12   |
| Micron    | 2200_MTFDHBA512TCK | 512 GB | FD42394927BB | 43    | 0     | 0.12   |
| Samsung   | SSD 970 EVO Plus   | 2 TB   | EACA5E659E20 | 43    | 0     | 0.12   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 14FCBE9F523C | 43    | 0     | 0.12   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 63FDC270E09B | 43    | 0     | 0.12   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | CE2B5811ED94 | 43    | 0     | 0.12   |
| Dell      | Express Flash C... | 960 GB | 4C87A1C1E037 | 43    | 0     | 0.12   |
| Samsung   | SSD 970 EVO Plus   | 1 TB   | E988AF57FD3C | 43    | 0     | 0.12   |
| Samsung   | MZQL23T8HCLS-00... | 3.8 TB | 3AC7545FEBD7 | 43    | 0     | 0.12   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 18CE481CE31B | 43    | 0     | 0.12   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 5DEF113ED515 | 43    | 0     | 0.12   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 614708824B2B | 43    | 0     | 0.12   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 62184A23DD61 | 43    | 0     | 0.12   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 6FB4C29A718C | 43    | 0     | 0.12   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 9C9DB87AD299 | 43    | 0     | 0.12   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | D36F5767C970 | 43    | 0     | 0.12   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | F1F4A53DA2DA | 43    | 0     | 0.12   |
| Samsung   | SSD 970 PRO        | 1 TB   | E9342CFD5A96 | 43    | 0     | 0.12   |
| Wester... | WUS4BB038D7P3E1    | 3.8 TB | 222380954453 | 43    | 0     | 0.12   |
| Wester... | WUS4BB038D7P3E1    | 3.8 TB | 5047C1E5D366 | 43    | 0     | 0.12   |
| Wester... | WUS4BB038D7P3E1    | 3.8 TB | 742D52542FD6 | 43    | 0     | 0.12   |
| Wester... | WUS4BB038D7P3E1    | 3.8 TB | F37F49781045 | 43    | 0     | 0.12   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 9D91AB646E1F | 43    | 0     | 0.12   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 07C651B6CDFC | 43    | 0     | 0.12   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 180661D6BFFA | 43    | 0     | 0.12   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 2C8C4DBB1504 | 43    | 0     | 0.12   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 4DF66437EE14 | 43    | 0     | 0.12   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 9589C5E2FA41 | 43    | 0     | 0.12   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | AD44740423C3 | 43    | 0     | 0.12   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | BCDBA6232A64 | 43    | 0     | 0.12   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 015016B4837E | 42    | 0     | 0.12   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 3651260072AC | 42    | 0     | 0.12   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 8027E10C8EB3 | 42    | 0     | 0.12   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 9E6F6DF86057 | 42    | 0     | 0.12   |
| Toshiba   | KXG60ZNV256G       | 256 GB | 4CCD5D72A7C3 | 42    | 0     | 0.12   |
| Toshiba   | KXG60ZNV256G       | 256 GB | 82A4B0EFEE5D | 42    | 0     | 0.12   |
| Micron    | 2200_MTFDHBA512TCK | 512 GB | 935ABB1ACF27 | 42    | 0     | 0.12   |
| WDC       | WUS4BB096D7P3E4    | 960 GB | 69E5531015C5 | 42    | 0     | 0.12   |
| Corsair   | MP400              | 1 TB   | 21C6682AF8DD | 42    | 0     | 0.12   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 3C5FDEE030E1 | 42    | 0     | 0.12   |
| Dell      | Express Flash C... | 960 GB | AE1D08143873 | 42    | 0     | 0.12   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 021F55CCF94F | 42    | 0     | 0.12   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 127CE45C8189 | 42    | 0     | 0.12   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 7BC2903CA150 | 42    | 0     | 0.12   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 0193588564EC | 42    | 0     | 0.12   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | A6EF0EBC19C4 | 42    | 0     | 0.12   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | AE47A0A47F32 | 42    | 0     | 0.12   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | C4EA3A895651 | 42    | 0     | 0.12   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 06054FA14FE8 | 42    | 0     | 0.12   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 1BCD6E51E2BC | 42    | 0     | 0.12   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 2003DE77C34B | 42    | 0     | 0.12   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 29B7516FBA6A | 42    | 0     | 0.12   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 45655C92E643 | 42    | 0     | 0.12   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 5B7973FF83BA | 42    | 0     | 0.12   |
| Samsung   | MZVLB512HBJQ-00000 | 512 GB | 51078F1ED69C | 42    | 0     | 0.12   |
| Micron    | 2200_MTFDHBA256TCK | 256 GB | FEDACA97AC95 | 42    | 0     | 0.12   |
| Corsair   | MP400              | 1 TB   | 07BA9E3434D9 | 42    | 0     | 0.12   |
| Dell      | Express Flash C... | 960 GB | 2FCB8D5548D8 | 42    | 0     | 0.12   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 32C5E49440F9 | 42    | 0     | 0.12   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 60819134B151 | 42    | 0     | 0.12   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 7DBB0C203DAC | 42    | 0     | 0.12   |
| Wester... | WUS4BB038D7P3E1    | 3.8 TB | 51A0A4D1E583 | 42    | 0     | 0.12   |
| Wester... | WUS4BB038D7P3E1    | 3.8 TB | A15EC2D534AF | 42    | 0     | 0.12   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | B77C6BA8414F | 42    | 0     | 0.12   |
| Intel     | SSDPED1D480GA      | 480 GB | CCB82F33AA84 | 42    | 0     | 0.12   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 1DF3F046DE95 | 42    | 0     | 0.12   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | AD8D2576B3F9 | 42    | 0     | 0.12   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | CC64E4EDCE16 | 42    | 0     | 0.12   |
| Micron    | 2200_MTFDHBA512TCK | 512 GB | 4CBCA2D7363F | 42    | 0     | 0.12   |
| WDC       | WUS4BB038D7P3E3    | 3.8 TB | F78721FC0059 | 42    | 0     | 0.12   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | DFA5245AB425 | 42    | 0     | 0.12   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | 3FCE3C65B5E3 | 42    | 0     | 0.12   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | 5096FE6DAC21 | 42    | 0     | 0.12   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | 7317F2DBD1D8 | 42    | 0     | 0.12   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | 8C4153A1D8BE | 42    | 0     | 0.12   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | ADC85763C2E5 | 42    | 0     | 0.12   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | DB51CDB38A00 | 42    | 0     | 0.12   |
| WDC       | WUS4BB038D7P3E3    | 3.8 TB | 0C2A4264DA0A | 42    | 0     | 0.12   |
| WDC       | WUS4BB038D7P3E3    | 3.8 TB | 5009C2C1990C | 42    | 0     | 0.12   |
| WDC       | WUS4BB038D7P3E3    | 3.8 TB | EA26FDB68F8C | 42    | 0     | 0.12   |
| Samsung   | MZPLJ1T6HBJR-000H3 | 1.6 TB | AEDAAB85A830 | 42    | 0     | 0.12   |
| Micron    | 7300_MTFDHBE6T4TDG | 6.4 TB | 8CA1EB222C61 | 42    | 0     | 0.12   |
| Micron    | 2200_MTFDHBA512TCK | 512 GB | 0C4294BB8B9D | 42    | 0     | 0.12   |
| WDC       | WUS3BA119C7P3E3    | 1.9 TB | 21F2B10AB3F0 | 42    | 0     | 0.12   |
| Wester... | WUS4BB038D7P3E1    | 3.8 TB | 1208096BEDC4 | 42    | 0     | 0.12   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 60B6946D6339 | 42    | 0     | 0.12   |
| Samsung   | SSD 970 PRO        | 1 TB   | EA604E4685B9 | 42    | 0     | 0.12   |
| WDC       | WUS4BB096D7P3E4    | 960 GB | 17724482F44F | 42    | 0     | 0.12   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 2CD78C6E16EC | 42    | 0     | 0.12   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | B32CB503361C | 42    | 0     | 0.12   |
| Wester... | WUS4BB038D7P3E1    | 3.8 TB | 34BCA8546216 | 42    | 0     | 0.12   |
| Wester... | WUS4BB038D7P3E1    | 3.8 TB | 358E7C6E43F4 | 42    | 0     | 0.12   |
| Wester... | WUS4BB038D7P3E1    | 3.8 TB | E81AE5292DD7 | 42    | 0     | 0.12   |
| Huawei    | HWE56P43800M005N   | 800 GB | 4C670DCF9938 | 41    | 0     | 0.11   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 72B93084C480 | 41    | 0     | 0.11   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | B2C9667E6A03 | 41    | 0     | 0.11   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | CD7DC4299DD7 | 41    | 0     | 0.11   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | E7A116CA42F9 | 41    | 0     | 0.11   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | F74CC4EBCC8A | 41    | 0     | 0.11   |
| Dell      | Express Flash C... | 960 GB | 214EA8D29BAC | 41    | 0     | 0.11   |
| Dell      | Express Flash C... | 960 GB | 8293280CF598 | 41    | 0     | 0.11   |
| Micron    | 2200_MTFDHBA512TCK | 512 GB | FC50664362F8 | 41    | 0     | 0.11   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 33C4B75695A7 | 41    | 0     | 0.11   |
| Wester... | WUS4BB038D7P3E1    | 3.8 TB | 2F976807EE11 | 41    | 0     | 0.11   |
| Wester... | WUS4BB038D7P3E1    | 3.8 TB | 79B30584708E | 41    | 0     | 0.11   |
| Wester... | WUS4BB038D7P3E1    | 3.8 TB | B9210CFB84B9 | 41    | 0     | 0.11   |
| Wester... | WUS4BB038D7P3E1    | 3.8 TB | FA0EC141EA7D | 41    | 0     | 0.11   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 2F6F38F36B3C | 41    | 0     | 0.11   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | CE2EAD3EA867 | 41    | 0     | 0.11   |
| Micron    | 2200_MTFDHBA512TCK | 512 GB | F28E892FF650 | 41    | 0     | 0.11   |
| Intel     | SSDPE2KX040T8      | 4 TB   | 23A8AD9A7AA3 | 41    | 0     | 0.11   |
| Intel     | SSDPE2KX040T8      | 4 TB   | A6A826F4FEA9 | 41    | 0     | 0.11   |
| Micron    | 2200_MTFDHBA512TCK | 512 GB | B7BA923ADC71 | 41    | 0     | 0.11   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 2C7BD304AA0F | 41    | 0     | 0.11   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 2C97E6C3057B | 41    | 0     | 0.11   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | BB8F385E7C40 | 41    | 0     | 0.11   |
| Kingston  | SKC2500M8500G      | 500 GB | E62C7DE71CE6 | 41    | 0     | 0.11   |
| Micron    | 2200_MTFDHBA512TCK | 512 GB | 46626679B8A1 | 41    | 0     | 0.11   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 06454D3C2FEC | 41    | 0     | 0.11   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | C2B54E7AE04A | 41    | 0     | 0.11   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | CE1DB06CC537 | 41    | 0     | 0.11   |
| Intel     | SSDPEDMD400G4      | 400 GB | DBFAE38161D1 | 41    | 0     | 0.11   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | B2BB555D32A1 | 41    | 0     | 0.11   |
| WDC       | PC SN720 SDAPNT... | 256 GB | 73E36759E4B6 | 41    | 0     | 0.11   |
| Corsair   | MP400              | 1 TB   | 668DAF60BC6D | 41    | 0     | 0.11   |
| Intel     | SSDPEDMD400G4      | 400 GB | 91ACF144181C | 41    | 0     | 0.11   |
| Wester... | WUS4BB038D7P3E1    | 3.8 TB | 27C05E35819E | 41    | 0     | 0.11   |
| Wester... | WUS4BB038D7P3E1    | 3.8 TB | 321AF59AEBFE | 41    | 0     | 0.11   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | E950646807D8 | 41    | 0     | 0.11   |
| Dell      | Express Flash C... | 960 GB | 237FC192731B | 41    | 0     | 0.11   |
| Dell      | Express Flash C... | 960 GB | 290F4CBA0141 | 41    | 0     | 0.11   |
| Dell      | Express Flash C... | 960 GB | 7A160A4853A2 | 41    | 0     | 0.11   |
| Dell      | Express Flash C... | 960 GB | EC51752EF405 | 41    | 0     | 0.11   |
| Intel     | SSDPE2KX010T8      | 1 TB   | 924650303EDF | 41    | 0     | 0.11   |
| Toshiba   | KXG60ZNV256G       | 256 GB | D05061FF7B2E | 41    | 0     | 0.11   |
| Samsung   | SSD 970 PRO        | 512 GB | 22AB68ABAEA7 | 40    | 0     | 0.11   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 880FB24AA280 | 38    | 0     | 0.11   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | FF6751344908 | 38    | 0     | 0.11   |
| Samsung   | SSD 970 EVO Plus   | 250 GB | 2F24E4A90003 | 38    | 0     | 0.10   |
| Micron    | 7300_MTFDHBE6T4TDG | 6.4 TB | E30327FE8089 | 37    | 0     | 0.10   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 1F8F32B5D229 | 37    | 0     | 0.10   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 6213B02F7022 | 37    | 0     | 0.10   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 6B3D4742852A | 37    | 0     | 0.10   |
| Micron    | 2200_MTFDHBA256TCK | 256 GB | 5F945EBE456C | 37    | 0     | 0.10   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 0BCF29E7A9D4 | 36    | 0     | 0.10   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 40A17290E873 | 36    | 0     | 0.10   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | A977735C796C | 36    | 0     | 0.10   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | E810BE7AA126 | 36    | 0     | 0.10   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | F3A87C83BD4B | 36    | 0     | 0.10   |
| Micron    | 2200_MTFDHBA256TCK | 256 GB | 04A556CBDF7F | 36    | 0     | 0.10   |
| Toshiba   | KXG60ZNV1T02       | 1 TB   | 517ABD69E8B2 | 36    | 0     | 0.10   |
| Toshiba   | KXG60ZNV1T02       | 1 TB   | EBB439A4142B | 36    | 0     | 0.10   |
| Toshiba   | KXG60ZNV1T02       | 1 TB   | 44889B2DDE31 | 35    | 0     | 0.10   |
| Dell      | Express Flash C... | 960 GB | A02113EF144B | 35    | 0     | 0.10   |
| Samsung   | MZVLB256HAHQ-000L7 | 256 GB | 4FA012D498BB | 35    | 0     | 0.10   |
| WDC       | WDS500G3X0C-00SJG0 | 500 GB | FC3F147783FE | 35    | 0     | 0.10   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 4E462A083BDD | 34    | 0     | 0.09   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 854939268E75 | 34    | 0     | 0.09   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | CD9BDE36F521 | 34    | 0     | 0.09   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | ED3CD45B7D23 | 34    | 0     | 0.09   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 93161FF85C26 | 34    | 0     | 0.09   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | D0F26AF6725D | 34    | 0     | 0.09   |
| Samsung   | SSD 970 EVO Plus   | 500 GB | 31EB946CA562 | 34    | 0     | 0.09   |
| Toshiba   | KXG60ZNV256G       | 256 GB | 8B0C1A94CBAF | 34    | 0     | 0.09   |
| ADATA     | SX6000PNP          | 256 GB | EB2D6D61A3B6 | 34    | 0     | 0.09   |
| Toshiba   | KXG6AZNV512G       | 512 GB | 4356DA06AB44 | 34    | 0     | 0.09   |
| Toshiba   | KXG6AZNV512G       | 512 GB | F288DCDE2E8A | 34    | 0     | 0.09   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 1119186A86D0 | 34    | 0     | 0.09   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 2846A6AE465C | 34    | 0     | 0.09   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 663AEC37738F | 34    | 0     | 0.09   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 8B17B458A647 | 34    | 0     | 0.09   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 9240FBD53BFA | 34    | 0     | 0.09   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 9A1AB12B4FDB | 34    | 0     | 0.09   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | B1D06E560CFA | 34    | 0     | 0.09   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | ED9D579CFACF | 34    | 0     | 0.09   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | F523A3300387 | 34    | 0     | 0.09   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | F707269502F3 | 34    | 0     | 0.09   |
| Kingston  | SA2000M81000G      | 1 TB   | C5AE650E768E | 33    | 0     | 0.09   |
| Kingston  | SA2000M81000G      | 1 TB   | E4A06EB6C4F0 | 33    | 0     | 0.09   |
| Toshiba   | KXG6AZNV512G       | 512 GB | 71AB88CC274C | 33    | 0     | 0.09   |
| Toshiba   | KXG6AZNV512G       | 512 GB | 7511C076AA18 | 33    | 0     | 0.09   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 0B09E31E0520 | 33    | 0     | 0.09   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 213E9E800469 | 33    | 0     | 0.09   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 4A36DE97405D | 33    | 0     | 0.09   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 4DA5A23B37F5 | 33    | 0     | 0.09   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 53CF783E0C40 | 33    | 0     | 0.09   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 6373E5D96F7A | 33    | 0     | 0.09   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 77BBB5254B4D | 33    | 0     | 0.09   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 92C41576F4E5 | 33    | 0     | 0.09   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | BF463D7A26B4 | 33    | 0     | 0.09   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | D01E38B4E339 | 33    | 0     | 0.09   |
| Toshiba   | KXG6AZNV512G       | 512 GB | 1DF127E42A11 | 33    | 0     | 0.09   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | AAD841E3AC33 | 33    | 0     | 0.09   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | DFDC23123880 | 33    | 0     | 0.09   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | F92DBBFD6AB3 | 33    | 0     | 0.09   |
| WDC       | WUS4BB096D7P3E1    | 800 GB | 2BAC27EE0A8A | 33    | 0     | 0.09   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 27249E25F5DA | 32    | 0     | 0.09   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 40D676DD5F52 | 32    | 0     | 0.09   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 692B59DD53F2 | 32    | 0     | 0.09   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 7BD3E4663768 | 32    | 0     | 0.09   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 011F6CD1118D | 32    | 0     | 0.09   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 0FA9FDA17B1F | 32    | 0     | 0.09   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 1C56A3F47C67 | 32    | 0     | 0.09   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 527ECA782C7A | 32    | 0     | 0.09   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | BA1EEAA79C27 | 32    | 0     | 0.09   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | CD7ADF7C6850 | 32    | 0     | 0.09   |
| Dell      | Express Flash C... | 960 GB | 4323CB12A62C | 32    | 0     | 0.09   |
| Micron    | 2200_MTFDHBA512TCK | 512 GB | 334A5F2E6882 | 32    | 0     | 0.09   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 2D21D527DD33 | 32    | 0     | 0.09   |
| Samsung   | MZVLB256HAHQ-00000 | 256 GB | 263E36CA6EB9 | 32    | 0     | 0.09   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | CF393709A5B0 | 32    | 0     | 0.09   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 5A7BA04C1360 | 32    | 0     | 0.09   |
| Samsung   | MZVLB512HAJQ-00000 | 512 GB | 2383AF5F2D48 | 32    | 0     | 0.09   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 1B9012AFA08A | 31    | 0     | 0.09   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 5B54A13CA91B | 31    | 0     | 0.09   |
| Toshiba   | KXG6AZNV512G       | 512 GB | 0EC06419796D | 31    | 0     | 0.09   |
| Toshiba   | KXG6AZNV512G       | 512 GB | F5B6EEB68EDC | 31    | 0     | 0.09   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | E84F87E34BBD | 31    | 0     | 0.09   |
| Micron    | 2200_MTFDHBA512TCK | 512 GB | 0CF9C29CB856 | 31    | 0     | 0.09   |
| Samsung   | MZVLB512HBJQ-00000 | 512 GB | DF9D70B061D1 | 31    | 0     | 0.09   |
| Toshiba   | KXG6AZNV512G       | 512 GB | A587082AE74D | 30    | 0     | 0.08   |
| Toshiba   | KXG6AZNV512G       | 512 GB | E270849DC057 | 30    | 0     | 0.08   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 317235CF9B3F | 30    | 0     | 0.08   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 3F0F308B6379 | 30    | 0     | 0.08   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 4E523B594E56 | 30    | 0     | 0.08   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 749780A66E6E | 30    | 0     | 0.08   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 8AF2D6AE4CB8 | 30    | 0     | 0.08   |
| Samsung   | SSD 970 EVO Plus   | 500 GB | C1E613DDD7B5 | 30    | 0     | 0.08   |
| Crucial   | CT1000P5SSD8       | 1 TB   | AFE44C919814 | 30    | 0     | 0.08   |
| Micron    | 2200_MTFDHBA512TCK | 512 GB | BD9A72C0B38B | 30    | 0     | 0.08   |
| Silico... | NV900-1T           | 1 TB   | 357F7CFC7093 | 30    | 0     | 0.08   |
| ADATA     | SX6000LNP          | 512 GB | E87FE29996C3 | 30    | 0     | 0.08   |
| Micron    | 2200_MTFDHBA512TCK | 512 GB | A055C60E2469 | 29    | 0     | 0.08   |
| Micron    | 2200_MTFDHBA512TCK | 512 GB | BA5FAC556DA2 | 29    | 0     | 0.08   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 79A5654D431A | 29    | 0     | 0.08   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | AB507F446483 | 29    | 0     | 0.08   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | C1552FC6DF7C | 29    | 0     | 0.08   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | D90F8AD419B4 | 29    | 0     | 0.08   |
| WDC       | WUS4BB096D7P3E1    | 800 GB | C0C2C46941D1 | 29    | 0     | 0.08   |
| Micron    | 2200_MTFDHBA512TCK | 512 GB | C4F69DBC5B8F | 28    | 0     | 0.08   |
| Intel     | SSDPF2KX076TZ      | 7.6 TB | 53A280E7C730 | 28    | 0     | 0.08   |
| Micron    | 2200_MTFDHBA512TCK | 512 GB | 28C94D8664EB | 28    | 0     | 0.08   |
| Micron    | 2200_MTFDHBA512TCK | 512 GB | 863C5EB44D39 | 28    | 0     | 0.08   |
| Samsung   | MZVLB1T0HBLR-00000 | 1 TB   | BA78635844EF | 28    | 0     | 0.08   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 1A4D6B393404 | 27    | 0     | 0.08   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 2594F8826CF8 | 27    | 0     | 0.08   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 3F5D55B8BFE9 | 27    | 0     | 0.08   |
| Dell      | Express Flash C... | 960 GB | BDD50621A6B6 | 27    | 0     | 0.08   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 15F25A558AA4 | 27    | 0     | 0.08   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | D20A28CED788 | 27    | 0     | 0.08   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | DBD66B315FB7 | 27    | 0     | 0.08   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 198F5A475430 | 27    | 0     | 0.08   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 379D2A94CA45 | 27    | 0     | 0.08   |
| WDC       | WUS4BB038D7P3E4    | 3.8 TB | 0764D47F53A2 | 27    | 0     | 0.08   |
| WDC       | WUS4BB038D7P3E4    | 3.8 TB | 553C79FF8EC3 | 27    | 0     | 0.08   |
| WDC       | WUS4BB038D7P3E4    | 3.8 TB | CFF04C1D95A0 | 27    | 0     | 0.08   |
| WDC       | WUS4BB096D7P3E1    | 800 GB | 9487AF6FAEE9 | 27    | 0     | 0.08   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | D497441E830B | 27    | 0     | 0.08   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | D717F7522F22 | 27    | 0     | 0.08   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | CBAF04C077B3 | 27    | 0     | 0.08   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 4C1638C0C2B0 | 27    | 0     | 0.08   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 97A21DD98F38 | 27    | 0     | 0.08   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | B29E9105F788 | 27    | 0     | 0.08   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | BBF6EB2505C0 | 27    | 0     | 0.08   |
| Toshiba   | KXG60ZNV256G       | 256 GB | 231915FFAC37 | 27    | 0     | 0.08   |
| Toshiba   | KXG60ZNV256G       | 256 GB | F3A90874428C | 27    | 0     | 0.08   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 5F7F7FB8CE83 | 27    | 0     | 0.08   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 6B2E9C15B45B | 27    | 0     | 0.08   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 7928600AED23 | 27    | 0     | 0.08   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 7CF799DA95F3 | 27    | 0     | 0.08   |
| Samsung   | MZVLB256HAHQ-000L7 | 256 GB | 8F5F9C160274 | 27    | 0     | 0.08   |
| Toshiba   | KXG60ZNV256G       | 256 GB | A3FF6F8BC3BF | 27    | 0     | 0.08   |
| WDC       | WUS4BB019D7P3E3    | 1.9 TB | 3CF785EE7350 | 27    | 0     | 0.08   |
| WDC       | WUS4BB019D7P3E3    | 1.9 TB | 7AE331EC540C | 27    | 0     | 0.08   |
| WDC       | WUS4BB019D7P3E3    | 1.9 TB | EBC2832FE541 | 27    | 0     | 0.08   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 2459679C5423 | 27    | 0     | 0.07   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 8F1177251B95 | 27    | 0     | 0.07   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | E592336CD23C | 27    | 0     | 0.07   |
| WDC       | WUS4BB038D7P3E4    | 3.8 TB | A35079C45C0C | 27    | 0     | 0.07   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | E5FE7CE421AA | 27    | 0     | 0.07   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 4CC3CB54060B | 26    | 0     | 0.07   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 60F9A85D2013 | 26    | 0     | 0.07   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 66DC32915CB5 | 26    | 0     | 0.07   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 428215B4D8F0 | 26    | 0     | 0.07   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 81BE3BE25D1D | 26    | 0     | 0.07   |
| WDC       | WUS4BB096D7P3E1    | 800 GB | 247025EC8353 | 26    | 0     | 0.07   |
| WDC       | WUS4BB096D7P3E1    | 800 GB | DEC4DB9C3E9F | 26    | 0     | 0.07   |
| Dell      | Express Flash C... | 960 GB | 9B75A23E898C | 26    | 0     | 0.07   |
| Samsung   | SSD 970 EVO Plus   | 250 GB | 362D1AD2519D | 26    | 0     | 0.07   |
| Samsung   | SSD 970 EVO Plus   | 250 GB | 9F5E1F881340 | 26    | 0     | 0.07   |
| Samsung   | MZVLB256HBHQ-00000 | 256 GB | 1853FE8AD3BC | 25    | 0     | 0.07   |
| Samsung   | MZVLB1T0HBLR-00000 | 1 TB   | C209A11427E0 | 25    | 0     | 0.07   |
| Samsung   | MZVLB256HAHQ-00000 | 256 GB | 4C88DC4A8261 | 25    | 0     | 0.07   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 7366BB7DD03D | 25    | 0     | 0.07   |
| Micron    | 7300_MTFDHBE6T4TDG | 6.4 TB | F51B05C06DB4 | 25    | 0     | 0.07   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 50764DE5C5CE | 24    | 0     | 0.07   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | D3DCEDC05D20 | 24    | 0     | 0.07   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | F0FB29AA36E9 | 24    | 0     | 0.07   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | A0EBC092B483 | 24    | 0     | 0.07   |
| Micron    | 7300_MTFDHBE6T4TDG | 6.4 TB | 500CE020065A | 24    | 0     | 0.07   |
| Micron    | 7300_MTFDHBE6T4TDG | 6.4 TB | 60772B3D8004 | 24    | 0     | 0.07   |
| Micron    | 7300_MTFDHBE6T4TDG | 6.4 TB | 94F7F10407CE | 24    | 0     | 0.07   |
| Micron    | 7300_MTFDHBE6T4TDG | 6.4 TB | CC354B16C012 | 24    | 0     | 0.07   |
| Micron    | 7300_MTFDHBE6T4TDG | 6.4 TB | D7ED5DC83DBD | 24    | 0     | 0.07   |
| WDC       | WDS500G2B0C-00PXH0 | 500 GB | D638825B9993 | 24    | 0     | 0.07   |
| Samsung   | SSD 970 EVO Plus   | 500 GB | 5C4F62A04EFB | 24    | 0     | 0.07   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 1018CAB17D9E | 23    | 0     | 0.07   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 708ED2584873 | 23    | 0     | 0.07   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | A33B584447C0 | 23    | 0     | 0.07   |
| Micron    | 2200_MTFDHBA512TCK | 512 GB | 6ADDF1A8CABD | 23    | 0     | 0.06   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 2C65D43A6F24 | 23    | 0     | 0.06   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 305BE0CE6564 | 23    | 0     | 0.06   |
| WDC       | WUS4BB096D7P3E1    | 800 GB | 3445F9DE91FE | 23    | 0     | 0.06   |
| Micron    | 2200_MTFDHBA512TCK | 512 GB | 28A8DAD5B4E9 | 23    | 0     | 0.06   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | 8A11B05ECC44 | 23    | 0     | 0.06   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | A14DA1D5F2C2 | 23    | 0     | 0.06   |
| Wester... | WUS4BB019D7P3E1    | 1.9 TB | E63D7BAEFC08 | 23    | 0     | 0.06   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 2B9609125D19 | 22    | 0     | 0.06   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | BB689AFA648B | 22    | 0     | 0.06   |
| Samsung   | MZVLB1T0HBLR-00000 | 1 TB   | 986BD1E5E8D3 | 22    | 0     | 0.06   |
| Samsung   | MZVLB256HAHQ-00000 | 256 GB | 20DC63AD5F72 | 22    | 0     | 0.06   |
| Micron    | MTFDHBA1T0TDV-1... | 1 TB   | 2653A9DC97E4 | 21    | 0     | 0.06   |
| Samsung   | MZVL21T0HCLR-00B00 | 1 TB   | 9535491588B1 | 21    | 0     | 0.06   |
| Samsung   | MZVLB1T0HBLR-00000 | 1 TB   | 1F242AE51ED4 | 21    | 0     | 0.06   |
| Samsung   | SSD 970 PRO        | 512 GB | 3D377A7EEFF5 | 21    | 0     | 0.06   |
| Samsung   | SSD 970 PRO        | 512 GB | 644B09C9E5B1 | 21    | 0     | 0.06   |
| Samsung   | MZVLB1T0HBLR-00000 | 1 TB   | 0826997B06CE | 21    | 0     | 0.06   |
| Samsung   | SSD 970 EVO Plus   | 1 TB   | 8C6C2D7BA7F3 | 21    | 0     | 0.06   |
| Samsung   | MZVLB256HBHQ-00000 | 256 GB | 78EF6087A42B | 21    | 0     | 0.06   |
| Samsung   | MZVLB256HBHQ-00000 | 256 GB | F810FAACFC63 | 21    | 0     | 0.06   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 2BFDCDD9A807 | 21    | 0     | 0.06   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 7A9353CE7796 | 21    | 0     | 0.06   |
| Samsung   | MZVLB1T0HBLR-00000 | 1 TB   | 1F0FF21ACCED | 20    | 0     | 0.06   |
| WDC       | WUS4BB038D7P3E4    | 3.8 TB | 89FC092F17BF | 20    | 0     | 0.06   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 10C2B5B87465 | 20    | 0     | 0.06   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 0C0D98A0CB81 | 20    | 0     | 0.06   |
| Kingston  | SKC2500M81000G     | 1 TB   | CA7027974704 | 20    | 0     | 0.06   |
| Toshiba   | KXG60ZNV1T02       | 1 TB   | FEFB1F9D0359 | 20    | 0     | 0.06   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 08698959DE8A | 20    | 0     | 0.06   |
| Samsung   | MZVLB256HAHQ-00000 | 256 GB | 5F7983557BB6 | 19    | 0     | 0.05   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 92630DBB1641 | 19    | 0     | 0.05   |
| Samsung   | MZVLB1T0HBLR-00000 | 1 TB   | 724D4AB5D60A | 19    | 0     | 0.05   |
| WDC       | WUS4BB096D7P3E1    | 800 GB | 6CA49652012E | 19    | 0     | 0.05   |
| WDC       | WUS4BB096D7P3E1    | 800 GB | 6E87E6C674CA | 19    | 0     | 0.05   |
| Crucial   | CT1000P5SSD8       | 1 TB   | 5D1E754B7730 | 19    | 0     | 0.05   |
| WDC       | CL SN720 SDAQNT... | 512 GB | 2E2DFB2F6398 | 19    | 0     | 0.05   |
| WDC       | CL SN720 SDAQNT... | 512 GB | D293FF7995C1 | 19    | 0     | 0.05   |
| Samsung   | MZQLB7T6HMLA-00007 | 7.6 TB | 0C7E4E0D513C | 18    | 0     | 0.05   |
| Samsung   | MZVLB256HAHQ-00000 | 256 GB | 7D86F8043548 | 18    | 0     | 0.05   |
| Samsung   | MZVLB256HAHQ-00000 | 256 GB | E9D669197DED | 18    | 0     | 0.05   |
| Samsung   | MZVLB256HAHQ-00000 | 256 GB | 09FA92A16EBC | 18    | 0     | 0.05   |
| Samsung   | MZVLB256HAHQ-00000 | 256 GB | 0CB8DC280519 | 18    | 0     | 0.05   |
| Samsung   | MZVLB256HAHQ-00000 | 256 GB | E1AF3FE861C5 | 18    | 0     | 0.05   |
| Samsung   | MZVLB1T0HBLR-00000 | 1 TB   | 395AF47CD9A7 | 18    | 0     | 0.05   |
| WDC       | CL SN720 SDAQNT... | 512 GB | C085CB0D7C52 | 18    | 0     | 0.05   |
| Toshiba   | KXG60ZNV512G       | 512 GB | 12B0274335E5 | 18    | 0     | 0.05   |
| Toshiba   | KXG60ZNV512G       | 512 GB | D8341739E91B | 18    | 0     | 0.05   |
| Samsung   | MZVL21T0HCLR-00B00 | 1 TB   | E4FDF1AB14D6 | 18    | 0     | 0.05   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 1809053B1ED5 | 18    | 0     | 0.05   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 7FDC6431A146 | 18    | 0     | 0.05   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | ADD6B8258F8B | 18    | 0     | 0.05   |
| Intel     | SSDPF2KX076TZ      | 7.6 TB | 80B58A0BB385 | 18    | 0     | 0.05   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 2BC9AC0D20C1 | 17    | 0     | 0.05   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 3CE102D885F1 | 17    | 0     | 0.05   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | C1E264A9B755 | 17    | 0     | 0.05   |
| Samsung   | MZVLB512HAJQ-00000 | 512 GB | 84D6C161101B | 17    | 0     | 0.05   |
| Toshiba   | KXG60ZNV512G       | 512 GB | 98790BD570D0 | 17    | 0     | 0.05   |
| Toshiba   | KXG60ZNV512G       | 512 GB | BD942CD43371 | 17    | 0     | 0.05   |
| Micron    | 7300_MTFDHBE6T4TDG | 6.4 TB | 080B8C4C5EC3 | 17    | 0     | 0.05   |
| Toshiba   | KXG60ZNV512G       | 512 GB | 54CBC90CBA0F | 17    | 0     | 0.05   |
| Micron    | 7300_MTFDHBE6T4TDG | 6.4 TB | 2F7F739243C4 | 17    | 0     | 0.05   |
| Samsung   | MZVLB1T0HBLR-00000 | 1 TB   | 8AAC88C03679 | 17    | 0     | 0.05   |
| Micron    | 7300_MTFDHBE6T4TDG | 6.4 TB | E6FE0A589A7F | 17    | 0     | 0.05   |
| Samsung   | MZVLB1T0HBLR-00000 | 1 TB   | 2116FDD74674 | 17    | 0     | 0.05   |
| Wester... | WUS4BB096D7P3E4    | 960 GB | 17724482F44F | 17    | 0     | 0.05   |
| Wester... | WUS4BB096D7P3E4    | 960 GB | 69E5531015C5 | 17    | 0     | 0.05   |
| Micron    | 7300_MTFDHBE6T4TDG | 6.4 TB | 38CAF49E0F4C | 17    | 0     | 0.05   |
| Micron    | 7300_MTFDHBE6T4TDG | 6.4 TB | A136D155E394 | 17    | 0     | 0.05   |
| Toshiba   | KXG60ZNV512G       | 512 GB | 04EBC622DCEE | 17    | 0     | 0.05   |
| Toshiba   | KXG60ZNV512G       | 512 GB | C1A1615F7D63 | 17    | 0     | 0.05   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 75788DA7E453 | 17    | 0     | 0.05   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 831506E7440B | 17    | 0     | 0.05   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | DACEC1942B90 | 17    | 0     | 0.05   |
| Samsung   | MZVLB1T0HBLR-00000 | 1 TB   | AF62E5D6450A | 16    | 0     | 0.05   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 16735348D71D | 16    | 0     | 0.05   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 18F842985B1D | 16    | 0     | 0.05   |
| Samsung   | MZQLB3T8HALS-00007 | 3.8 TB | 7C5576A97979 | 16    | 0     | 0.05   |
| Samsung   | SSD 970 EVO Plus   | 250 GB | B69A1B0D0B96 | 16    | 0     | 0.04   |
| Micron    | 9300_MTFDHAL7T6TDP | 7.6 TB | 2DA1DF8ADB66 | 15    | 0     | 0.04   |
| Micron    | 9300_MTFDHAL7T6TDP | 7.6 TB | 74CA7CA25999 | 15    | 0     | 0.04   |
| Micron    | 9300_MTFDHAL7T6TDP | 7.6 TB | 8ACF4BFC1F92 | 15    | 0     | 0.04   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 2C2698223BE1 | 15    | 0     | 0.04   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 792F322A5E33 | 15    | 0     | 0.04   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | A7CD790D9A19 | 15    | 0     | 0.04   |
| Dell      | Express Flash C... | 960 GB | 1658807A7640 | 15    | 0     | 0.04   |
| Dell      | Express Flash C... | 960 GB | 20221C1472AF | 15    | 0     | 0.04   |
| Dell      | Express Flash C... | 960 GB | 4A114B781AA9 | 15    | 0     | 0.04   |
| Dell      | Express Flash C... | 960 GB | 9E1C21BDAC06 | 15    | 0     | 0.04   |
| Dell      | Express Flash C... | 960 GB | E319D99DD1BC | 15    | 0     | 0.04   |
| Dell      | Express Flash C... | 960 GB | E7D5137FFCF0 | 15    | 0     | 0.04   |
| Samsung   | MZVLB256HAHQ-00000 | 256 GB | 4EBAAA5D36C2 | 15    | 0     | 0.04   |
| Samsung   | SSD 970 PRO        | 1 TB   | 3796FE2288B9 | 15    | 0     | 0.04   |
| Samsung   | SSD 970 PRO        | 512 GB | 5268059B45D2 | 15    | 0     | 0.04   |
| Toshiba   | KXG60ZNV512G       | 512 GB | 4F5212317DF5 | 15    | 0     | 0.04   |
| Toshiba   | KXG60ZNV512G       | 512 GB | B8CB3F378075 | 15    | 0     | 0.04   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 997E392D0E9D | 15    | 0     | 0.04   |
| Toshiba   | KXG60ZNV256G       | 256 GB | 06D1B5ADCDEA | 15    | 0     | 0.04   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 11C88571D978 | 15    | 0     | 0.04   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 25DA16D9642C | 15    | 0     | 0.04   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 2BA8FC39FAC3 | 15    | 0     | 0.04   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 53363624CE31 | 15    | 0     | 0.04   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 5B168E4F8DE6 | 15    | 0     | 0.04   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 8B67421D0779 | 15    | 0     | 0.04   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | B3F89BBDD507 | 15    | 0     | 0.04   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | B43DCF8B47C3 | 15    | 0     | 0.04   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | EA7B36580EF9 | 15    | 0     | 0.04   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | F570095B1878 | 15    | 0     | 0.04   |
| Toshiba   | KXG60ZNV1T02       | 1 TB   | C2DD1E6DF2E2 | 14    | 0     | 0.04   |
| Toshiba   | KXG60ZNV1T02       | 1 TB   | DA072E22C66A | 14    | 0     | 0.04   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 04BBDAFCFB34 | 14    | 0     | 0.04   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 21848508E847 | 14    | 0     | 0.04   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 2C13CFD087E5 | 14    | 0     | 0.04   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 4BC6A6777A60 | 14    | 0     | 0.04   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 607AFAC5A85B | 14    | 0     | 0.04   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 72A354148275 | 14    | 0     | 0.04   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 951B9D6CDA5C | 14    | 0     | 0.04   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | BDDB911208F7 | 14    | 0     | 0.04   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | E7DB73320244 | 14    | 0     | 0.04   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 0A3E4C98048E | 14    | 0     | 0.04   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | D12A7EE04FCE | 14    | 0     | 0.04   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | D758DB218FBA | 14    | 0     | 0.04   |
| WDC       | WUS4BB038D7P3E4    | 3.8 TB | 217BEBDA31AD | 14    | 0     | 0.04   |
| Micron    | 2200_MTFDHBA512TCK | 512 GB | C65B47A16064 | 14    | 0     | 0.04   |
| Intel     | MDTPED1K750GA      | 752 GB | 5834F7A91C90 | 13    | 0     | 0.04   |
| Intel     | SSDPEDMD400G4      | 400 GB | AA20BA91AA40 | 13    | 0     | 0.04   |
| Micron    | 2200_MTFDHBA512TCK | 512 GB | 91869E33D6D1 | 13    | 0     | 0.04   |
| Toshiba   | KXG60ZNV512G       | 512 GB | 08E596662165 | 13    | 0     | 0.04   |
| Toshiba   | KXG60ZNV512G       | 512 GB | A60E26D19E60 | 13    | 0     | 0.04   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 1E203E92C3F0 | 13    | 0     | 0.04   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 52119BEA4FEE | 13    | 0     | 0.04   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | A21D7B56D1D3 | 13    | 0     | 0.04   |
| Crucial   | CT1000P5SSD8       | 1 TB   | 86F173274941 | 13    | 0     | 0.04   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 20C7998AAB3C | 13    | 0     | 0.04   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 639F2B4514EB | 13    | 0     | 0.04   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 720D7002AA7D | 13    | 0     | 0.04   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 7B797E221C9F | 13    | 0     | 0.04   |
| Samsung   | SSD 970 PRO        | 1 TB   | 49408EC33FA7 | 13    | 0     | 0.04   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 31D2D7A06DA0 | 13    | 0     | 0.04   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 3B7DF89068B3 | 13    | 0     | 0.04   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 7688C555940C | 13    | 0     | 0.04   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 664D96F439B4 | 12    | 0     | 0.04   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 7C5AAF17A507 | 12    | 0     | 0.04   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | EA090DD63BB4 | 12    | 0     | 0.04   |
| Toshiba   | KXG60ZNV512G       | 512 GB | 8572E71DAD57 | 12    | 0     | 0.04   |
| Toshiba   | KXG60ZNV512G       | 512 GB | 95A261DB5D2A | 12    | 0     | 0.04   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | F86AC67771CF | 12    | 0     | 0.04   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | F86AC67771CF | 12    | 0     | 0.04   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 524CB7F68A6E | 12    | 0     | 0.04   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 6F5063C2E4B6 | 12    | 0     | 0.04   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 8390300E8FDC | 12    | 0     | 0.04   |
| WDC       | WUS4CB032D7P3E4    | 3.2 TB | 248541010AE3 | 12    | 0     | 0.03   |
| WDC       | WUS4CB032D7P3E4    | 3.2 TB | 24C7C989651C | 12    | 0     | 0.03   |
| WDC       | WUS4CB032D7P3E4    | 3.2 TB | FF3AA6080B74 | 12    | 0     | 0.03   |
| Wester... | WUS4CB032D7P3E4    | 3.2 TB | 248541010AE3 | 12    | 0     | 0.03   |
| Wester... | WUS4CB032D7P3E4    | 3.2 TB | 24C7C989651C | 12    | 0     | 0.03   |
| Wester... | WUS4CB032D7P3E4    | 3.2 TB | FF3AA6080B74 | 12    | 0     | 0.03   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 0CDCFB01C209 | 12    | 0     | 0.03   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | 7DC7D481B486 | 12    | 0     | 0.03   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | BF5649B4A890 | 12    | 0     | 0.03   |
| Dell      | Ent NVMe AGN RI... | 3.8 TB | E5CCC9FEC46F | 12    | 0     | 0.03   |
| Dell      | Express Flash C... | 960 GB | 64BC083B8A9A | 12    | 0     | 0.03   |
| Kingston  | SKC2500M81000G     | 1 TB   | 92B4541F06FC | 12    | 0     | 0.03   |
| Toshiba   | KXG60ZNV512G       | 512 GB | 69791169AFF1 | 12    | 0     | 0.03   |
| Samsung   | SSD 970 EVO Plus   | 500 GB | 34EE65286F59 | 12    | 0     | 0.03   |
| Toshiba   | KXG6AZNV512G       | 512 GB | 489A643903F7 | 12    | 0     | 0.03   |
| Toshiba   | KXG6AZNV512G       | 512 GB | C471003EFF0E | 12    | 0     | 0.03   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 82B07040B8FD | 12    | 0     | 0.03   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | CB9B8728407D | 12    | 0     | 0.03   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | E646EBAEC0FA | 12    | 0     | 0.03   |
| Intel     | SSDPEKNW010T8      | 1 TB   | 3C8A1EBD453B | 12    | 0     | 0.03   |
| SK hynix  | BC501A NVMe        | 128 GB | 1D8E8498ED7B | 11    | 0     | 0.03   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 8163D2B59EAF | 11    | 0     | 0.03   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | A99098B0374F | 11    | 0     | 0.03   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | B0D32BA233BD | 11    | 0     | 0.03   |
| Intel     | SSDPF2KX076TZ      | 7.6 TB | 7A328B16B73B | 11    | 0     | 0.03   |
| Samsung   | MZVLB512HBJQ-00000 | 512 GB | D68DFEA43DA6 | 11    | 0     | 0.03   |
| Samsung   | SSD 970 PRO        | 1 TB   | 29AADC714EC3 | 11    | 0     | 0.03   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | EA5280055B1D | 10    | 0     | 0.03   |
| ADATA     | SX8200PNP          | 512 GB | 28BA9BC0F213 | 10    | 0     | 0.03   |
| Samsung   | MZVLB512HBJQ-00000 | 512 GB | 950FCA2D9CDA | 10    | 0     | 0.03   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | E1111B65B639 | 10    | 0     | 0.03   |
| Intel     | SSDPF2KX076TZ      | 7.6 TB | C59564588618 | 10    | 0     | 0.03   |
| WDC       | CL SN720 SDAQNT... | 512 GB | D659B32F8086 | 10    | 0     | 0.03   |
| WDC       | WUS4CB032D7P3E4    | 3.2 TB | 67B2646DAD8E | 10    | 0     | 0.03   |
| Wester... | WUS4CB032D7P3E4    | 3.2 TB | 67B2646DAD8E | 10    | 0     | 0.03   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 71A73FC7B45E | 10    | 0     | 0.03   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 71D2D40D4EC4 | 10    | 0     | 0.03   |
| Toshiba   | KXG60ZNV512G       | 512 GB | 80101C94100B | 10    | 0     | 0.03   |
| ADATA     | SX6000PNP          | 256 GB | F8FBB8ACDB65 | 9     | 0     | 0.03   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | A27ABE064695 | 9     | 0     | 0.03   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 54E8DF709558 | 9     | 0     | 0.03   |
| Toshiba   | KXG60ZNV1T02       | 1 TB   | B1D7895D05E3 | 9     | 0     | 0.03   |
| Toshiba   | KXG60ZNV1T02       | 1 TB   | CFD2DFFFE389 | 9     | 0     | 0.03   |
| Intel     | SSDPELKX010T8      | 1 TB   | 753409E0DC75 | 9     | 0     | 0.03   |
| Intel     | SSDPELKX010T8      | 1 TB   | AB42A563ED1A | 9     | 0     | 0.03   |
| Wester... | WUS4BB096D7P3E4    | 960 GB | B920D111C4B2 | 9     | 0     | 0.02   |
| Wester... | WUS4BB096D7P3E4    | 960 GB | BA89193FACA8 | 9     | 0     | 0.02   |
| Intel     | SSDPE2KE016T8      | 1.6 TB | 01AE891C68E3 | 9     | 0     | 0.02   |
| Intel     | SSDPE2KE016T8      | 1.6 TB | 0E89B2B6DA67 | 9     | 0     | 0.02   |
| Intel     | SSDPE2KE016T8      | 1.6 TB | 1609E279AE03 | 9     | 0     | 0.02   |
| Intel     | SSDPE2KE016T8      | 1.6 TB | 2C27936113EA | 9     | 0     | 0.02   |
| Intel     | SSDPE2KE016T8      | 1.6 TB | 38EDD57E1B8F | 9     | 0     | 0.02   |
| Intel     | SSDPE2KE016T8      | 1.6 TB | 3A6DE830576F | 9     | 0     | 0.02   |
| Intel     | SSDPE2KE016T8      | 1.6 TB | 44CECFF4C3B4 | 9     | 0     | 0.02   |
| Intel     | SSDPE2KE016T8      | 1.6 TB | 54F2404397F4 | 9     | 0     | 0.02   |
| Intel     | SSDPE2KE016T8      | 1.6 TB | 7000ACB768F2 | 9     | 0     | 0.02   |
| Intel     | SSDPE2KE016T8      | 1.6 TB | 797C1F4AEBEC | 9     | 0     | 0.02   |
| Intel     | SSDPE2KE016T8      | 1.6 TB | 8150246A1A5D | 9     | 0     | 0.02   |
| Intel     | SSDPE2KE016T8      | 1.6 TB | 8A91A99714DE | 9     | 0     | 0.02   |
| Intel     | SSDPE2KE016T8      | 1.6 TB | 8ADBA4FE21FC | 9     | 0     | 0.02   |
| Intel     | SSDPE2KE016T8      | 1.6 TB | 8B2D67359189 | 9     | 0     | 0.02   |
| Intel     | SSDPE2KE016T8      | 1.6 TB | 8FC486E1CBED | 9     | 0     | 0.02   |
| Intel     | SSDPE2KE016T8      | 1.6 TB | A6FB1F12EEFC | 9     | 0     | 0.02   |
| Intel     | SSDPE2KE016T8      | 1.6 TB | D72752E70BDC | 9     | 0     | 0.02   |
| Intel     | SSDPE2KE016T8      | 1.6 TB | 2D745828D4B1 | 9     | 0     | 0.02   |
| Intel     | SSDPE2KE016T8      | 1.6 TB | CFD38AA728FB | 9     | 0     | 0.02   |
| Intel     | SSDPE2KE016T8      | 1.6 TB | D0D244639B5D | 9     | 0     | 0.02   |
| Toshiba   | KXG60ZNV512G       | 512 GB | 4013DF8151E0 | 8     | 0     | 0.02   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 6938E0BE9B1D | 8     | 0     | 0.02   |
| Toshiba   | KXG60ZNV256G       | 256 GB | 235C9C73BE11 | 8     | 0     | 0.02   |
| Corsair   | MP400              | 1 TB   | 08BD9A8512CE | 8     | 0     | 0.02   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | DD05B5CC49D5 | 16    | 1     | 0.02   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | 7B1382D758DD | 8     | 0     | 0.02   |
| Intel     | SSDPF2KX076TZ      | 7.6 TB | 1A0003D4741A | 8     | 0     | 0.02   |
| Crucial   | CT500P5SSD8        | 500 GB | 163DE232A7B3 | 7     | 0     | 0.02   |
| Micron    | 2200_MTFDHBA512TCK | 512 GB | A34EF394D30A | 7     | 0     | 0.02   |
| Crucial   | CT500P5SSD8        | 500 GB | 29CEE504796F | 7     | 0     | 0.02   |
| Dell      | Express Flash C... | 960 GB | 728A1DBC7B28 | 7     | 0     | 0.02   |
| Dell      | Express Flash C... | 960 GB | BF28DF2E0C9C | 7     | 0     | 0.02   |
| Samsung   | MZVLB512HBJQ-00000 | 512 GB | BEAED1895517 | 7     | 0     | 0.02   |
| Samsung   | MZVLB1T0HALR-00000 | 1 TB   | 712E8C86C7C7 | 7     | 0     | 0.02   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 433D7FA794A3 | 7     | 0     | 0.02   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 4949FF279A94 | 7     | 0     | 0.02   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 65F83ADBF8E4 | 7     | 0     | 0.02   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 6DCF0543D739 | 7     | 0     | 0.02   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 92A830AD26F5 | 7     | 0     | 0.02   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 94250C6B761C | 7     | 0     | 0.02   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | D9F384B63ED8 | 7     | 0     | 0.02   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | FDBB53869B62 | 7     | 0     | 0.02   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 3CC0FDFC43CE | 7     | 0     | 0.02   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 3DD2ED3DAEC7 | 7     | 0     | 0.02   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 49F6377E97D7 | 7     | 0     | 0.02   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 7617A2427AD5 | 7     | 0     | 0.02   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 8F3A9FA52E26 | 7     | 0     | 0.02   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | AB920F1C3075 | 7     | 0     | 0.02   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | C6A4F475F5A6 | 7     | 0     | 0.02   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | EB22ECA75633 | 7     | 0     | 0.02   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 3BADA762C78F | 7     | 0     | 0.02   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 4D379C2B6B87 | 7     | 0     | 0.02   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 6D76199E1927 | 7     | 0     | 0.02   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 920BFEA5D5CC | 7     | 0     | 0.02   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | A853F34D6A01 | 7     | 0     | 0.02   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | AA7992754834 | 7     | 0     | 0.02   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | DB38F7FFE8A6 | 7     | 0     | 0.02   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | FC5658EC9F3E | 7     | 0     | 0.02   |
| Samsung   | MZVLB512HBJQ-00000 | 512 GB | F454FB565897 | 7     | 0     | 0.02   |
| Samsung   | SSD 970 EVO Plus   | 1 TB   | 39741BC3ADD4 | 7     | 0     | 0.02   |
| Samsung   | SSD 970 EVO Plus   | 1 TB   | 7CEA1A8B5BFB | 7     | 0     | 0.02   |
| Crucial   | CT1000P5SSD8       | 1 TB   | 1700B01C279C | 6     | 0     | 0.02   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 7189B72E6FA9 | 6     | 0     | 0.02   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | BC0F000A719E | 6     | 0     | 0.02   |
| Intel     | SSDPED1K750GA      | 752 GB | 04303340FF77 | 6     | 0     | 0.02   |
| Intel     | SSDPED1K750GA      | 752 GB | 9EB62BD9B8CE | 6     | 0     | 0.02   |
| Intel     | SSDPED1K750GA      | 752 GB | CB4F469EA779 | 6     | 0     | 0.02   |
| Intel     | SSDPED1K750GA      | 752 GB | F6D5D7384739 | 6     | 0     | 0.02   |
| Samsung   | MZVLB1T0HBLR-00000 | 1 TB   | 8446067B1DEC | 6     | 0     | 0.02   |
| Samsung   | SSD 970 EVO Plus   | 1 TB   | 3E4D3961388B | 6     | 0     | 0.02   |
| Toshiba   | KXG6AZNV512G       | 512 GB | 73562E5130BD | 6     | 0     | 0.02   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 0A705E63D70A | 6     | 0     | 0.02   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 0DD306247CDE | 6     | 0     | 0.02   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 12C002486079 | 6     | 0     | 0.02   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 4752D8024991 | 6     | 0     | 0.02   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 49F532D0B98A | 6     | 0     | 0.02   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 5ABFF6635B7E | 6     | 0     | 0.02   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 77FA15A1A9C2 | 6     | 0     | 0.02   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | 7F593CF0FFCE | 6     | 0     | 0.02   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | B2904F885815 | 6     | 0     | 0.02   |
| WDC       | WUS4BB019D7P3E1    | 1.9 TB | F249DBD715A2 | 6     | 0     | 0.02   |
| Toshiba   | KXG60ZNV256G       | 256 GB | 0BB42CA3C6E4 | 6     | 0     | 0.02   |
| Toshiba   | KXG60ZNV256G       | 256 GB | EE73EAE97C38 | 6     | 0     | 0.02   |
| Intel     | SSDPEDMX020T7      | 2 TB   | E7549926C343 | 1195  | 196   | 0.02   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 123694FC3B0A | 6     | 0     | 0.02   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | DA0E7673695A | 6     | 0     | 0.02   |
| Kingston  | SKC2500M82000G     | 2 TB   | 21970BB0CD59 | 5     | 0     | 0.02   |
| Micron    | 2200_MTFDHBA256TCK | 256 GB | 64B0FF891FBE | 5     | 0     | 0.02   |
| Samsung   | SSD 970 EVO Plus   | 500 GB | 22031AD4F9B7 | 5     | 0     | 0.02   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | 8167F50E3C66 | 5     | 0     | 0.01   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | 8167F50E3C66 | 5     | 0     | 0.01   |
| Corsair   | MP400              | 1 TB   | EC799245ADAC | 5     | 0     | 0.01   |
| SK hynix  | BC501A NVMe        | 128 GB | 7F7CBDEF1043 | 5     | 0     | 0.01   |
| Samsung   | SSD 970 EVO Plus   | 250 GB | A03025AC8C30 | 5     | 0     | 0.01   |
| KIOXIA    | KCD61LUL3T84       | 3.8 TB | 1EF63C34F40F | 5     | 0     | 0.01   |
| KIOXIA    | KCD61LUL3T84       | 3.8 TB | 28A0E021A0C9 | 5     | 0     | 0.01   |
| KIOXIA    | KCD61LUL3T84       | 3.8 TB | 419F8A77B992 | 5     | 0     | 0.01   |
| KIOXIA    | KCD61LUL3T84       | 3.8 TB | 46AD308813E0 | 5     | 0     | 0.01   |
| KIOXIA    | KCD61LUL3T84       | 3.8 TB | 47797102888F | 5     | 0     | 0.01   |
| KIOXIA    | KCD61LUL3T84       | 3.8 TB | 52D390901422 | 5     | 0     | 0.01   |
| KIOXIA    | KCD61LUL3T84       | 3.8 TB | 627638F47692 | 5     | 0     | 0.01   |
| KIOXIA    | KCD61LUL3T84       | 3.8 TB | 69242CF2E292 | 5     | 0     | 0.01   |
| KIOXIA    | KCD61LUL3T84       | 3.8 TB | 7608CFA6A3A2 | 5     | 0     | 0.01   |
| KIOXIA    | KCD61LUL3T84       | 3.8 TB | 8D00684E647A | 5     | 0     | 0.01   |
| KIOXIA    | KCD61LUL3T84       | 3.8 TB | 94F341FDB87C | 5     | 0     | 0.01   |
| KIOXIA    | KCD61LUL3T84       | 3.8 TB | 9E2135905CC1 | 5     | 0     | 0.01   |
| KIOXIA    | KCD61LUL3T84       | 3.8 TB | A291924B42D1 | 5     | 0     | 0.01   |
| KIOXIA    | KCD61LUL3T84       | 3.8 TB | BF822457DD84 | 5     | 0     | 0.01   |
| KIOXIA    | KCD61LUL3T84       | 3.8 TB | C70C342962DC | 5     | 0     | 0.01   |
| KIOXIA    | KCD61LUL3T84       | 3.8 TB | E8F37C109C2E | 5     | 0     | 0.01   |
| KIOXIA    | KCD61LUL3T84       | 3.8 TB | F5EBAE7E2254 | 5     | 0     | 0.01   |
| KIOXIA    | KCD61LUL3T84       | 3.8 TB | F756978014FA | 5     | 0     | 0.01   |
| KIOXIA    | KCD61LUL3T84       | 3.8 TB | FBB521D78595 | 5     | 0     | 0.01   |
| KIOXIA    | KCD61LUL3T84       | 3.8 TB | 15A4F429475F | 5     | 0     | 0.01   |
| Samsung   | MZQLB1T9HAJR-00007 | 1.9 TB | EF78AD79C003 | 5     | 0     | 0.01   |
| Micron    | 2200_MTFDHBA512TCK | 512 GB | 00AF3CE91C5D | 4     | 0     | 0.01   |
| Micron    | 7300_MTFDHBG1T9TDF | 1.9 TB | 102D33C144A2 | 4     | 0     | 0.01   |
| Micron    | 7300_MTFDHBG1T9TDF | 1.9 TB | 948D9892B65A | 4     | 0     | 0.01   |
| WDC       | WUS4BB096D7P3E1    | 960 GB | AF724472CDA8 | 4     | 0     | 0.01   |
| Wester... | WUS4BB096D7P3E1    | 960 GB | AF724472CDA8 | 4     | 0     | 0.01   |
| Samsung   | MZVL21T0HCLR-00B00 | 1 TB   | 86B2C9FBF642 | 4     | 0     | 0.01   |
| Micron    | 2200_MTFDHBA512TCK | 512 GB | C5AFE717921F | 4     | 0     | 0.01   |
| Kingston  | SA2000M81000G      | 1 TB   | B39DA17EABA9 | 4     | 0     | 0.01   |
| Samsung   | MZVL21T0HCLR-00B00 | 1 TB   | 8D40DEEA24C6 | 4     | 0     | 0.01   |
| Samsung   | MZVLB1T0HBLR-00000 | 1 TB   | 4AAC10366C23 | 4     | 0     | 0.01   |
| Micron    | 7300_MTFDHBG1T9TDF | 1.9 TB | 422B322975C0 | 4     | 0     | 0.01   |
| Micron    | 7300_MTFDHBG1T9TDF | 1.9 TB | D833B2D5EAEE | 4     | 0     | 0.01   |
| Toshiba   | KXG60ZNV1T02       | 1 TB   | 3C35A7AC3C68 | 4     | 0     | 0.01   |
| Intel     | SSDPED1K750GA      | 752 GB | 0A0EED037CD3 | 4     | 0     | 0.01   |
| Intel     | SSDPED1K750GA      | 752 GB | 0B0A75482C9B | 4     | 0     | 0.01   |
| Intel     | SSDPED1K750GA      | 752 GB | 783175203410 | 4     | 0     | 0.01   |
| Intel     | SSDPED1K750GA      | 752 GB | 967B4E943A9B | 4     | 0     | 0.01   |
| Intel     | SSDPEKNW512G8      | 512 GB | E05A56AD462B | 4     | 0     | 0.01   |
| ADATA     | SX6000NP           | 128 GB | 12D632E07035 | 615   | 142   | 0.01   |
| SK hynix  | BC501A NVMe        | 128 GB | 1A2FDD080778 | 4     | 0     | 0.01   |
| Samsung   | MZVLB1T0HBLR-00000 | 1 TB   | 4BB309E384B6 | 4     | 0     | 0.01   |
| Toshiba   | KXG60ZNV512G       | 512 GB | F2FB3E4372D7 | 4     | 0     | 0.01   |
| WDC       | PC SN730 SDBQNT... | 256 GB | C73BB3CBE553 | 4     | 0     | 0.01   |
| ADATA     | SX8200PNP          | 512 GB | FBB0C5EC18F1 | 4     | 0     | 0.01   |
| Samsung   | MZ1LB1T9HALS-00007 | 1.9 TB | 0B123AC51968 | 4     | 0     | 0.01   |
| Samsung   | MZ1LB1T9HALS-00007 | 1.9 TB | 9D968AB4D76C | 4     | 0     | 0.01   |
| Intel     | SSDPE2KE016T8      | 1.6 TB | C2FBA071C8BE | 4     | 0     | 0.01   |
| Micron    | 7300_MTFDHBG1T9TDF | 1.9 TB | 9315508A7DF5 | 4     | 0     | 0.01   |
| Micron    | 7300_MTFDHBG1T9TDF | 1.9 TB | B0FD041204E3 | 4     | 0     | 0.01   |
| Dell      | Express Flash N... | 6.4 TB | 3FF9DC2C20F1 | 4     | 0     | 0.01   |
| Dell      | Express Flash N... | 6.4 TB | F5A3E900CA16 | 4     | 0     | 0.01   |
| Samsung   | MZVLB1T0HBLR-00000 | 1 TB   | 3A6D58C8B22C | 3     | 0     | 0.01   |
| Micron    | 2200_MTFDHBA512TCK | 512 GB | 48DB4CB0140D | 3     | 0     | 0.01   |
| Crucial   | CT500P5SSD8        | 500 GB | D14947E20E56 | 3     | 0     | 0.01   |
| Intel     | SSDPE2KX040T7      | 4 TB   | 77188C568EAF | 3     | 0     | 0.01   |
| Samsung   | SSD 970 EVO Plus   | 1 TB   | ADE88E23CB9B | 3     | 0     | 0.01   |
| Samsung   | MZVLB256HBHQ-000L7 | 256 GB | 4DCFBBA37A71 | 2     | 0     | 0.01   |
| Samsung   | MZQLW960HMJP-00003 | 960 GB | 674BDBBA15D6 | 1675  | 570   | 0.01   |
| Samsung   | MZVLB256HBHQ-000L7 | 256 GB | 9AB8408D7943 | 2     | 0     | 0.01   |
| Samsung   | MZVLB512HBJQ-00000 | 512 GB | 4C5BF8BCE565 | 2     | 0     | 0.01   |
| Samsung   | MZVLB1T0HBLR-00000 | 1 TB   | A8A579F799FF | 2     | 0     | 0.01   |
| Samsung   | MZVLB512HBJQ-00000 | 512 GB | C4851D7E1D56 | 2     | 0     | 0.01   |
| Samsung   | SSD 980 PRO        | 250 GB | CFBE6334FF85 | 2     | 0     | 0.01   |
| Toshiba   | KXG60ZNV256G       | 256 GB | 7CDD05458796 | 1     | 0     | 0.01   |
| Crucial   | CT500P5SSD8        | 500 GB | 201D37CE1DDC | 1     | 0     | 0.01   |
| Crucial   | CT500P5SSD8        | 500 GB | 59D8EC889628 | 1     | 0     | 0.01   |
| Micron    | 2200_MTFDHBA256TCK | 256 GB | D7ED15D7C4F0 | 1     | 0     | 0.01   |
| Samsung   | SSD 970 EVO Plus   | 1 TB   | C895F97B9631 | 1     | 0     | 0.01   |
| Samsung   | SSD 980            | 250 GB | 8490843F811A | 1     | 0     | 0.00   |
| Samsung   | MZQLW960HMJP-00003 | 960 GB | C24870A7D706 | 1720  | 1006  | 0.00   |
| Samsung   | MZVLB256HBHQ-000L7 | 256 GB | 10C7BDA5123C | 1     | 0     | 0.00   |
| Samsung   | SSD 970 PRO        | 1 TB   | 5DA3814D28E8 | 1     | 0     | 0.00   |
| Samsung   | MZQLW960HMJP-00003 | 960 GB | 8A4B6FB6F4B7 | 1471  | 1008  | 0.00   |
| Kingston  | SKC2500M8500G      | 500 GB | 69FC33EEAEBB | 1     | 0     | 0.00   |
| WDC       | CL SN720 SDAQNT... | 512 GB | 558E598CE38A | 490   | 384   | 0.00   |
| Micron    | 7300_MTFDHBG1T9TDF | 1.9 TB | 5A05748D7698 | 1     | 0     | 0.00   |
| Micron    | 7300_MTFDHBG1T9TDF | 1.9 TB | 9E48F20F5A79 | 1     | 0     | 0.00   |
| Kingston  | SKC2500M82000G     | 2 TB   | 3D49A800E532 | 1     | 0     | 0.00   |
| Samsung   | MZVLW128HEGR-00000 | 128 GB | 744D606A27F2 | 1     | 0     | 0.00   |
| WDC       | CL SN720 SDAQNT... | 512 GB | 70AD13C42DE3 | 431   | 384   | 0.00   |
| Samsung   | MZVLW128HEGR-00000 | 128 GB | F62EEA6FD71A | 1     | 0     | 0.00   |
| Dell      | Express Flash N... | 1.6 TB | 23A840B7671B | 1058  | 1008  | 0.00   |
| Samsung   | SSD 970 EVO Plus   | 1 TB   | 8E95C4FF7121 | 1     | 0     | 0.00   |
| Samsung   | MZPLL1T6HEHP-00003 | 1.6 TB | 610237FEB63E | 1048  | 1015  | 0.00   |
| Intel     | SSDPEDKX040T7      | 4 TB   | BFBAC84A16B9 | 1028  | 1011  | 0.00   |
| Samsung   | MZVLW1T0HMLH-000L7 | 1 TB   | 9ADE15853D63 | 0     | 0     | 0.00   |
| Samsung   | MZVKW512HMJP-00000 | 512 GB | 07694E190B49 | 821   | 1010  | 0.00   |
| Intel     | SSDPED1K750GA      | 752 GB | 45BE75154965 | 0     | 0     | 0.00   |
| Intel     | SSDPED1K750GA      | 752 GB | 8FFDFBCA5F27 | 0     | 0     | 0.00   |
| Intel     | SSDPED1K750GA      | 752 GB | 988498AD1BA3 | 0     | 0     | 0.00   |
| Intel     | SSDPED1K750GA      | 752 GB | F8761AB11F16 | 0     | 0     | 0.00   |
| Samsung   | SSD 970 EVO Plus   | 500 GB | 41EF3E9F578B | 0     | 0     | 0.00   |
| Intel     | SSDPE2KX040T8      | 4 TB   | EC0F09CCE7F2 | 779   | 1023  | 0.00   |
| Samsung   | MZVLB1T0HBLR-00000 | 1 TB   | 4D42BD73FD9B | 0     | 0     | 0.00   |
| Samsung   | SSD 980            | 250 GB | 961C23779ED4 | 0     | 0     | 0.00   |
| Intel     | SSDPED1K750GA      | 752 GB | 035B6E55FD95 | 0     | 0     | 0.00   |
| Intel     | SSDPED1K750GA      | 752 GB | 14F3F6F51561 | 0     | 0     | 0.00   |
| Intel     | SSDPED1K750GA      | 752 GB | 277A90C98FBB | 0     | 0     | 0.00   |
| Intel     | SSDPED1K750GA      | 752 GB | 58FC99D0AF36 | 0     | 0     | 0.00   |
| Intel     | SSDPED1K750GA      | 752 GB | AC9A7AF8CA9D | 0     | 0     | 0.00   |
| Intel     | SSDPED1K750GA      | 752 GB | BD2CC539B0D8 | 0     | 0     | 0.00   |
| Intel     | SSDPED1K750GA      | 752 GB | EC698043FCFE | 0     | 0     | 0.00   |
| Intel     | SSDPED1K750GA      | 752 GB | F735F4093937 | 0     | 0     | 0.00   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 878CA3D681E3 | 0     | 0     | 0.00   |
| Samsung   | SSD 980            | 250 GB | BB4442113967 | 0     | 0     | 0.00   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | B70E35445087 | 0     | 0     | 0.00   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 29DF80C4CBD0 | 0     | 0     | 0.00   |
| Samsung   | MZQLB960HAJR-00007 | 960 GB | 62A86EF6FD22 | 0     | 0     | 0.00   |
| Samsung   | SSD 970 EVO Plus   | 1 TB   | 885ED1E02D4F | 0     | 0     | 0.00   |
| Intel     | SSDPEKKW010T8      | 1 TB   | D59B3D46DA2A | 0     | 0     | 0.00   |
| Samsung   | MZ1LB960HAJQ-00007 | 960 GB | 1762E7178471 | 0     | 0     | 0.00   |
| Samsung   | MZ1LB960HAJQ-00007 | 960 GB | F14945C19F54 | 0     | 0     | 0.00   |
| Samsung   | MZVLB1T0HBLR-00000 | 1 TB   | 622C40B6FDDA | 0     | 0     | 0.00   |
| Samsung   | SSD 970 EVO Plus   | 500 GB | CE78F1B27452 | 0     | 0     | 0.00   |
| Samsung   | SSD 980            | 250 GB | 0B5C6E1864EA | 0     | 0     | 0.00   |
| Samsung   | SSD 980            | 1 TB   | 5D3D470E6862 | 0     | 0     | 0.00   |
| Samsung   | SSD 980            | 1 TB   | 937FAD2B01B2 | 0     | 0     | 0.00   |
| WDC       | WUS4CB032D7P3E4    | 3.8 TB | 1F58890BFCBA | 0     | 0     | 0.00   |
